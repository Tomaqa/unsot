\documentclass[a4paper,11pt]{article}

\usepackage[utf8]{inputenc} % LaTeX source encoded as UTF-8

\usepackage[margin=1in]{geometry}

\usepackage{lang}

% \Finaltrue
% \Blindtrue

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

\author{\open{Kol{\' a}rik Tom{\' a}{\v s}}}
\title{\open{UN/SOT }Core Input Language Specification}
\date{\open{\today}}

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

\begin{document}

\maketitle

\open{%
\begin{abstract}
This paper fully specifies core input language
of~UN/SOT solver,
including used SMT-LIB constructs
and few ODE-related commands.
It yet provides full functionality,
language extensions only enhance file structure etc.
\end{abstract}
}

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

%%%%%%%%%%%%%%%%%%%%%%%%%%%%

% Sun 04 Nov 2018 07:39:55 PM CET
\begin{section}{General specification}\label{s:spec}
Core input language supports usage of~SMT formulas
along with~ODEs.
It is strongly based on~SMT-LIB standard
and inspired by~other similar tools
such as~dReal.
The~terminology follows the~SMT-LIB's.

Despite the~similarity, the~language
is not compatible (\hl{not conform}) with~SMT-LIB standard,
which is relatively robust.
Our tool supports subset of~SMT-LIB
and ortogonally adds few ODE commands.
Both layers are described in~individual sections.

The~language behaves like a~logic,
that is derived from a~\hl{theory of~Reals},
and which supports usage of~ODEs.
However, it is not a~logic nor a~theory in~all,
because it works with~floating point variables
with~approximation errors, which bounds are not exact.

%%%

% Mon 09 Dec 2019 02:11:45 AM CET
\begin{paragraph}{Terminology.}\label{p:spec:term}
All ODEs share the~same independent variable
denoted as~$t$ (which usually stands for~time).
Each ODE possesses a~dependent variable
with~the~same name as~the~ODE itself.

Whole evaluation consists of~deciding satisfiability of~set of~formulas,
which can contain, in~addition to~Boolean and real terms,
also so~called \name{flows},
as~special cases of~real functions,
which encapsulate ODE schemes.
ODE solver is being active only during the~evaluation of~particular flows,
as~long as~violation of~an~\name{invariant} occurs---%
a~\name{timer condition} (related to~$t$),
a~\name{local invariant} (related to~concrete ODE),
or a~\name{global invariant}.
Flow instances are typically connected into~cascades
and separated by~dicrete \name{jumps}.

In~addition to~$t$, $\tau$ symbol is used as~local timer,
which evolves the~same way as~$t$,
but always starts from zero for~each flow instance.
The~division of~invariants into~different categories
is reserved mainly for~performance reasons,
where the~most simple ones (starting with timer conditions)
can be implemented more efficiently than the~more general ones.
\end{paragraph} %%Terminology.

%%%

% Mon 09 Dec 2019 03:00:56 AM CET
\begin{paragraph}{Marking.}\label{p:spec:sign}
Brace characters \id{<>} are not parts of~the~syntax
as~they only stand for~arguments that are to~be substituted,
or they border or group arguments;
postfix \id{*} denotes that the~string
can appear arbitrarily many times including zero times;
postfix \id{+} is like \id{*} but requires at~least one occurance
($n$ \id{+}s require at~least $n$ occurances);
infix \id{|} denotes more options for~the~position
and it should be enclosed within~\id{<>} angle brackets;
the~same applies for~\id{-}, which stands for~ranges.
\end{paragraph} %%Marking.

%%%

%%%%%%%%%%%%%

% Thu 25 Oct 2018 01:18:10 PM CEST
\begin{subsection}{Syntax}\label{ss:spec:syntax}
The~language uses fully parenthesized prefix notation%
\footnote{This syntax is widely known from the~\name{Lisp} language.}.
It is sequence of~\name{tokens}, \name{expressions},
white characters and comments.
It is case sensitive.

%%%

% Thu 25 Oct 2018 01:21:54 PM CEST
\begin{paragraph}{White characters.}\label{p:spec:syntax:ws}
Allowed white characters are:
\begin{center}
\begin{tabular}{r|ccc}
   Name  &  space & tabulator &      newline \\ \hline
   ASCII &   $32$ &       $9$ &  $(13+{})10$ \\
\end{tabular}
\end{center}

With~the~only exception of~separation of~tokens,
white characters are ignored in~all other cases.
\end{paragraph} %%White characters.

%%%

% Thu 25 Oct 2018 01:24:31 PM CEST
\begin{paragraph}{Comment.}\label{p:spec:syntax:cmt}
Any segment of~a~line that starts with \hll{\id{;}}
is treated as~a comment to~the~end of~the~line.
Its content is ignored.
\end{paragraph} %%Comment.

%%%

% Mon 09 Dec 2019 02:22:08 AM CET
\begin{paragraph}{Token}\label{p:spec:syntax:token}
is a~sequence of~non-white characters
(which act as~separators).
Allowed characters depend on~the~type of~token,
which is either \name{identifier} or \name{literal}.

\hll{Identifiers} consist of~alphanumeric characters and characters
\begin{center}
\id{+ \  - \  * \  / \  \% \  \^{}
\   = \  < \  >
\  \_ \  . \  '
\  ? \  ! \  \&}
\end{center}
They only must not begin with~a~digit%
\footnote{Due to~only minimal restrictions,
one should avoid confusing names of~identifiers,
like~containing operator symbols
(\id{a<b} etc.),
and should separate tokens by~white characters or to~expressions thoroughly.}.
All listed symbols, except of~\id{'}, are subset of~SMT-LIB alphabet.
Identifiers have to~be declared, defined or reserved before using.
They represent either \name{commands}
(then it's always a~reserved token;
they are interpreted purely internally in~the~tool),
or \name{functions}
(which can be user defined),
or \name{sorts} of~elements, expressions, etc.
\name{Constants}%
\footnote{Term ``variable" is avoided here
because all elements are static and it is not possible
to~reassign new values to~them dynamically---
with~the~only exception of~flow variables,
which on~the~other hand do not propagate outside of~the~flow.}
are special cases of~functions with no arguments.

\hll{Literals} are nameless constants of~some sort.
Numerical ones contain digits and eventually decimal point
(\id{\hll{.}}) or negative sign (\id{-}),
and semilogarithmic form of~reals is also supported%
\footnote{These features are not allowed in~SMT-LIB
or are more restricted.}.
Literals must not begin with~positive sign and with~additional zeros.
Only decimal system is supported.
Boolean literals are \id{true} and \id{false}, or also 1 and 0.
\end{paragraph} %%Token

%%%

% Sun 04 Nov 2018 10:02:43 PM CET
\begin{paragraph}{Expression}\label{p:spec:syntax:expr}
is always enclosed within~parenthesis:
\begin{center}
\id{( <{}<token> | <expr>{}>* )}
\end{center}
where \id{<expr>} is nested expression
and \id{<token>} is a~token.

If an~expression appears at~root level of~the~input,
then it has to~be a~command.
Command must begin with an~identifier with~the~name of~the~command.
Common expressions have not this restriction
as~long as~they are not functions.
If the~first element is a~token which is not an~identifier
of~a~command or function (not constant),
the~expression is treated as~a~list.
Commands do not have to~have a~sort,
but functions always do.

(Possible additional white characters and comments
are omited from~the~description;
they are ignored during the~evaluation.)
\end{paragraph} %%Expression

%%%

\end{subsection} %%Syntax

%%%%%%%%%%%%%

\end{section} %%General specification

%%%%%%%%%%%%%%%%%%%%%%%%%%%%

% Sun 04 Nov 2018 07:53:38 PM CET
\begin{section}{SMT constructs}\label{s:smt}
Only a~subset of~all SMT-LIB constructs is used,
and some nonlinear functions,
which real logics in~SMT do not support, are added.

%%%

% Sun 04 Nov 2018 08:01:52 PM CET
\begin{paragraph}{Sorts:}\label{p:smt:sorts}
\begin{itemize}
\item \id{Bool}~---~logical type,
\item \id{Real}~---~type of~real numbers%
   \footnote{More precisely: of~floating point numbers with~rounding errors.}.
\end{itemize}
Integers are not accepted (resp. they are treated as~reals).
All~discrete or truly integer values
have to~be built from~\id{Bool} constants
(e.g.~locations of~an~automaton can be modelled
as~conjunctions of~\id{Bool}s),
and thus have to~be finite.
\end{paragraph} %%Sorts:

%%%

% Mon 09 Dec 2019 02:27:51 AM CET
\begin{paragraph}{Reserved functions.}\label{p:smt:reserved}
The~language contain these functions (resp. operators)
with~standard semantics:
\begin{itemize}
\item Unary:  \quad \id{not}
\item Binary: \quad \id{/}
\item $n$-ary:
   \begin{itemize}
   \item with~left associativity:
      \begin{itemize}
      \item ${n \geq 1}$: \quad \id{- \  and \  or}
      \item ${n \geq 2}$: \quad \id{xor \  + \  *}
      \end{itemize}
   \item with~right associativity, ${n \geq 2}$: \quad \id{=>}
   \item chainable, ${n \geq 2}$:  \quad \id{= \  < \  > \  <= \  >=}
   \end{itemize}
\end{itemize}
and specifically these functions:
\begin{itemize}
\item \id{distinct}~---~$n$-ary function with~${n \geq 2}$
   which returns true iff all pairs from~the~$n$ arguments are distinct,
\item \id{ite}~---~ternary function
   with~the~first argument of~sort \id{Bool} (condition);
   it returns the~second argument if the~condition is true,
   or the~third otherwise.
\end{itemize}

All functions above are supported by~SMT solvers.
In~addition, these \hl{unary} functions,
which are not supported by~SMT, are added:
\begin{center}
\id{abs \  sqrt \  cbrt \  sin \  cos \  tan \  exp \  ln}
\end{center}
and these \hl{binary} functions:
\begin{center}
\id{\^{} \  min \  max}
\end{center}
All these functions have same semantics as~the~ones
from~standard (resp. POSIX) C/C++ libraries
(\id{ln} stands for \id{log} and \id{\^{}} stands for \id{pow}).
\end{paragraph} %%Reserved functions.

%%%

%%%%%%%%%%%%%

% Sun 04 Nov 2018 08:47:45 PM CET
\begin{subsection}{Commands}\label{ss:smt:commands}
Several SMT reserved commands are understood by~the~language.
All of~them are listed in~this section.

% % Mon 07 May 2018 04:35:22 PM CEST
% \begin{paragraph}{\id{set-logic}}\label{p:design:spec:ilang:smt:logic}
% nastavuje logiku použitou v~\acr{smt} řešiči:
% \begin{center}
% \id{(set-logic <logic\_\-name>)}
% \end{center}
% kde \id{<logic\_\-name>} je jedna z~následujících logik
% teorií reálných čísel s~volnými funkčními symboly,
% podle \acr{smt}-LIB:
% \begin{itemize}
% \item \id{QF\_\-UFLRA}~---~lineární bez~kvantifikátorů,
% \item \id{QF\_\-UFNRA}~---~nelineární bez~kvantifikátorů,
% \item \id{UFLRA}~---~lineární s~kvantifikátory.
% \end{itemize}
% Vliv použití kvantifikátorů jsme však dosud nezkoumali.

% Příkaz smí být volán nejvýše jednou
% a musí předcházet všem ostatním uvedeným příkazům.
% Není-li příkaz uveden, je jako výchozí logika zvolena \id{QF\_\-UFLRA}.

% Pokud to implementace umožňuje,
% smí být také podporovány zmíněné logiky
% bez~volných funkčních symbolů (názvy jsou bez~znaků \id{UF}).
% Pak je jako výchozí logika volena \id{QF\_\-LRA}.
% \end{paragraph} %%\id{set-logic}

%%%

% Wed 01 May 2019 11:39:04 PM CEST
\begin{paragraph}{\id{declare-fun}}\label{p:smt:commands:declfun}
serves to~declare new functions (resp., in~most cases, constants),
with~sort and signature, but without interpretation.
The~form is
\Spec{(declare-fun <fun\_\-id> (<arg\_\-sort>*) <sort>)}
with~arguments:
\begin{itemize}
\item \id{<fun\_\-id>}~---~identifier of~the~introduced function (constant),
\item \id{<arg\_\-sort>*}~---~list of~identifiers of~input arguments' sorts
   (empty in~case of~constant),
\item \id{<sort>}~---~identifier of~function's return value sort
   (resp. of~the~constant itself).
\end{itemize}
Few examples:
\begin{center}
\id{(declare-fun y (Real) Real)}\\
\id{(declare-fun empty? () Bool)}
\end{center}

All these functions (constants) are introduced into~global scope,
i.e.~they can be used anywhere after its declaration
(local arguments of~functions have higher priority,
but shadowing global identifiers should be avoided).

After their declaration/definition, constants must be used as~tokens
(e.g.~\id{a}, not \id{(a)}),
whereas functions with~at~least one argument
must be used as~expressions (e.g.~\id{(foo 5)}).
\end{paragraph} %%\id{declare-fun}

%%%

% Mon 09 Dec 2019 02:33:12 AM CET
\begin{paragraph}{\id{declare-const}}\label{p:smt:commands:declconst}
is a~syntactic abbreviation of~\id{declare-fun} for~constants:
\Spec{(declare-const <const\_\-id> <sort>)}
\end{paragraph} %%\id{declare-const}

%%%

% Wed 01 May 2019 11:40:45 PM CEST
\begin{paragraph}{\id{define-fun}}\label{p:smt:commands:defun}
extends \id{declare\--fun} of~function interpretation:
\Spec{(define-fun <fun\_\-id> ((<arg> <arg\_\-sort>)*) <sort> <expr>)}
with~these different arguments:
\begin{itemize}
\item \id{(<arg> <arg\_\-sort>)*}~---~list of~identifier pairs
   of~input arguments' names and sorts;
   it is recommended to~prefix \id{<arg>}s
   with~e.g.~\id{\&}
   to~clearly distinguish between local and global arguments,
\item \id{<expr>}~---~expression (or token)
   which defines function's behaviour---prescription for~the~return value
   of~sort \id{<sort>};
   it can (not necessarily) contain particular input arguments (\id{<arg>}s)
   and other (global) functions.
\end{itemize}
Example:
\begin{center}
\id{(define-fun v ((\&s Real) (\&t Real)) Real (/ \&s \&t))}
\end{center}

Every function is either declared or defined, not both.
\end{paragraph} %%\id{define-fun}

%%%

% Wed 01 May 2019 11:49:44 PM CEST
\begin{paragraph}{\id{assert}}\label{p:smt:commands:assert}
introduces conditional formula (\name{assertion}),
which is required to~be satisfied:
\Spec{(assert <expr>)}
where \id{<expr>} is expression (or token) of~sort \id{Bool}.
\\Few examples:
\begin{center}
\id{(assert (or (= x 5) (= x 6) ))}\\
\id{(assert (and (=> empty? (= k 0)) (=> (not empty?) (> k 0)) ))}
\end{center}

Whole input is treated satisfiable iff every assertion can be satisfied,
i.e.~they are put into~logical conjunction.
(Thus, \id{(assert (and a b))} is functionally equivalent
to~\id{(assert a) (assert b)}).
\end{paragraph} %%\id{assert}

%%%

% Thu 08 Nov 2018 04:24:10 PM CET
\begin{paragraph}{\id{check-sat}}\label{p:smt:commands:check-sat}
performs evaluation of~whole input specification.
Its output is one of~these:
\begin{itemize}
\item \id{unsat}---there approximately%
   \footnote{The~decision is not exact (from~principle)
   due to~floating point rounding errors
   and inaccurate numerical integration of~ODEs.}
   is no assesment of~all undefined constants
   which would satisfy all assertions,
\item \id{sat}---an~assesment which approximately satisfy all assertions
   has been found; values of~all constants can be possibly displayed,
\item \id{unknown}---the~solver is not capable
   to~decide the~input specification for~some reason.
\end{itemize}
\end{paragraph} %%\id{check-sat}

%%%

% Mon 09 Dec 2019 02:37:02 AM CET
\begin{paragraph}{\id{get-value}}\label{p:smt:commands:get-value}
outputs model values of~given constants:
\Spec{(get-value (<const\_\-id>*))}
\end{paragraph} %%\id{get-value}

%%%

% Mon 09 Dec 2019 02:37:02 AM CET
\begin{paragraph}{\id{get-model}}\label{p:smt:commands:get-model}
outputs model values of~all declared constants.
\end{paragraph} %%\id{get-model}

%%%

% Mon 09 Dec 2019 02:37:02 AM CET
\begin{paragraph}{\id{echo}}\label{p:smt:commands:echo}
prints out given string on~standard output (with~a~newline):
\Spec{(echo < | "<token>*">)}
\end{paragraph} %%\id{echo}

%%%

% Mon 09 Dec 2019 02:37:02 AM CET
\begin{paragraph}{\id{exit}}\label{p:smt:commands:exit}
terminates the~solver on its call.
\end{paragraph} %%\id{exit}

%%%

% Mon 09 Dec 2019 02:49:00 AM CET
\begin{paragraph}{\id{set-option}}\label{p:smt:commands:set-option}
sets an~option and its value:
\Spec{(set-option <option> <value>)}
with~these possible options
(ODE-related ones are also shown already for~completeness):
\begin{itemize}
\item \id{:verbosity <0-3>}~---~the~level of~solver verbosity,
   where 0 means ``quiet'', 1 ``verbose''
   and 3 most verbose;
   generally, level 1 adds information about performed definitions,
   level 2 about instantiations,
   and level 3 even about the~evaluation process,
\item \id{:dt <float>}~---~defines global (initial) size of~$dt$
   of~all flows,
   i.e.~integration step size of~ODE solver;
   this value might change during each flow
   if stepping method of~ODE solver is adaptive,
   %% set for each flow separately
   %% -> inside `define-flow`:
   %% (= _dt 0.001)
\item \id{:keep-flow-invariant}~---~changes the~behaviour
   of~invariants s.t. after the~end of~evaluation of~a~flow
   the~invariant still holds (while the~default behaviour is
   that it stays violated in~the~last step of~integration)%
   \footnote{This can be useful for~example for~some bouncing effects.},
\item \id{:require-flow-progress}~---~each flow that already violates
   its invariant in~its first step is falsified.
\end{itemize}
\end{paragraph} %%\id{set-option}

%%%

% Mon 09 Dec 2019 02:48:56 AM CET
\begin{paragraph}{\id{get-option}}\label{p:smt:commands:get-option}
prints out the~value of~given option:
\Spec{(get-option <option>)}
\end{paragraph} %%\id{get-option}

%%%

\end{subsection} %%Commands

%%%%%%%%%%%%%

\end{section} %%SMT constructs

%%%%%%%%%%%%%%%%%%%%%%%%%%%%

% Mon 09 Dec 2019 03:12:28 AM CET
\begin{section}{ODE constructs}\label{s:ode}
Core language includes only fundamental ODE constructs.
Language extensions can provide many utilities
to~better handle e.g.~hybrid systems and automatons,
but can require some effort to~get familiar with~their syntax and semantics
(especially for~non-programmers).
However, all these tools do not add any new functionality
as~they are only wrappers above core constructs described below.

In~context of~ODEs one can simplify the~syntax as~follows:
\begin{itemize}
\item ODEs have one shared independent variable which name is always $t$,
\item sort of~functions, their derivatives, their arguments
   and of~$t$ is \id{Real}.
\end{itemize}
These observations will be supposed from now on.
Independent variable $t$ is denoted as~\id{\_t}
and timer $\tau$ is denoted as~\id{\_tau};
these identifiers are collectively named \name{special arguments}.

Input can contain number of~dependent or independent
ODEs, accordingly to~whether they appear in~the~same scheme.
Each single ODE is denoted as~\id{ode} within~this document
and its name is the~same with~its dependent variable.
Each \id{ode} can appear multiple times (with~different equations),
each possibly dependent on~arbitrary condition;
each such occurance is understood as~a~\name{variant} of~the~\id{ode}
(it can have various prescriptions).
A~variant (even single) is described by~an~equality
of~its dependent variable or its derivative
with an~expression (see below).

No discrete changes can happen during evalution of~a~flow.
Variants of~\id{ode}s can be switched typically during jumps,
which can be evaluated only after violation of~an~invariant.

%%%%%%%%%%%%%

% Thu 02 May 2019 12:10:13 AM CEST
\begin{subsection}{Flows usage}\label{ss:ode:flows}
This section describes constructs
that handle particular flows.
Each flow always contains all mutually dependent \id{ode}s together,
which are understood as an~ODE scheme,
and can appear only inside commands related to~their~flow.

%%%

% Mon 09 Dec 2019 03:35:52 AM CET
\begin{paragraph}{\id{define-flow}}\label{p:ode:flows:def}
is crucial command that provides majority
of~ODE functionality.
It defines an~ODE scheme as~standalone flow,
including ODE prescriptions and their invariants,
but it does not yet assign any input nor output values
(it is not instantiated yet).
Its form is
\Spec{(define-flow <flow\_\-id> (<ode\_\-id>+) ((<arg> <arg\_\-sort>)*) <expr>)}
with~arguments:
\begin{itemize}
\item \id{<flow\_\-id>}---identifier of~introduced flow structure
   which is later to~be evaluated in~particular flow instances
   with~various input and output values,
\item \id{<ode\_\-id>+}---list of~\id{ode} identifiers
   that represent whole ODE scheme (of~all their dependent variables),
\item \id{(<arg> <arg\_\-sort>)*}~---~list of~identifier pairs
   of~additional arguments' names and sorts;
   it is recommended to~prefix \id{<arg>}s with~e.g.~\id{\&}
   (like in~\id{define-fun}),
\item \id{<expr>}---(almost) \hl{arbitrary} conditional expression
   which can additionaly contain \id{ode} variants
   and invariants; it behaves as~a~wrapper that embeds \id{ode}s
   into a~common expression.
\end{itemize}

\id{ode} variants are in~the~form \id{(= <ode\_\-id>' <expr\_>)},
% (then it is treated as~derivative prescription),
% or in~the~form \id{(= <ode\_\-id> <expr>)}
% (then it stands for~common function),
where \id{<expr\_>} is expression or token of~sort \id{Real}.
No~derivatives (\id{<ode\_\-id>'}) can appear inside of~the~\id{expr\_},
and also anywhere else outside of~the~variant.

Invariants have to~be satisfied during whole flow.
Each particular invariant is conditional expression:
\begin{itemize}
\item an~equality or inequality in~the~form \id{(<op> <id> <expr\_>)},
   where \id{<op>} is one of~$\{\id{=}, \id{<}, \id{>}, \id{<=}, \id{>=}\}$,
   \id{<id>} is an~\id{ode} identifier (\id{<ode\_\-id>})
   or special argument, and \id{<expr\_>}
   is expression or token of~sort \id{Real},
\item conjunction, disjunction or negation
   of~an~invariant is also an~invariant.
\end{itemize}
However, each invariant's underlying equality/inequality
must contain an~\id{ode} or special argument,
otherwise it is treated as~common real predicate.
Timer conditions and local invariants have a~restriction
on~each its equality/inequality:
its left hand side must be always the~same \id{ode}
(then it is a~local invariant)
or special argument (then it is timer condition).

Each \id{ode} can also appear inside unary function \id{init} or \id{final},
which represent initial and final values of~the~\id{ode}, respectively%
\footnote{One can also alternatively use \id{\&<ode\_\-id>} token
instead of~\id{init} function, and \id{<ode\_\-id>*} token
instead of~\id{final} function;
also, \id{diff} unary function
is an~alternative to~\id{<ode\_\-id>'} notation.};
these can appear anywhere in~the~\id{<expr>},
except of~that final values cannot (obviously) appear
inside of~any \id{ode} variant or any invariant.

Example:
\begin{Verbatim}[samepage=true]
(define-flow flow1 (x y) ((&a  Bool) (&a* Bool)
                          (&_t* Real) (&x* Real) (&y* Real))
         ;; ODEs
   (and  (= x' (+ y 1))
         (ite &a  (= y' (sin _t))
                  (= y' (cos _t))
         )
         ;; Invariants
         (>= x 0) (<= x 3)
         (ite &a (and (>= y  0) (<= y 1))
                 (and (>= y -1) (<= y 1))
         )
         (<= _tau 10)
         (> (+ x y) 0)
         ;; Jumps
         (ite &a (not &a*) &a* )
         (= &_t* (final _t)) (= &x* (final x))
         (= &y* (- (final y)))
))
\end{Verbatim}

All conditions outside of~the~particular \id{ode} variants and invariants
are decided right \hl{before} the~flow is started,
purely by~SMT solver,
while particular variants and invariants
are being evaluated during whole flow purely by~ODE solver.

As you may noticed in~the~example,
the~flow can also contain jump conditions,
thanks to~the~\id{final} function.
The~output values do not have to~be the~same as~the~final values
(see the~negation in~the~jump with~\id{(final y)},
or also the~negation of~the~Boolean \id{\&a}).
The~output arguments are recommended to~be
both prefixed with~\id{\&} and suffixed with~\id{*}.
\end{paragraph} %%\id{define-flow}

%%%

% Mon 09 Dec 2019 04:05:51 AM CET
\begin{paragraph}{\id{flow}}\label{p:ode:flows:flow}
instantiates defined flow structure---%
assigns input values and arguments to~it:
\Spec{(flow <flow\_\-id> (<in\_\-val>++) (<arg\_\-val>*))}
with~arguments:
\begin{itemize}
\item \id{<flow\_\-id>}---identifier of~the~flow structure
   which is to~be instantiated (see \id{define-flow}),
\item \id{<in\_\-val>++}---list of~tokens
   with~initial values of~corresponding \id{ode}s of~the~\id{<flow\_\-id>};
   in~addition, first element of~the~list stands
   for~initial value of~$t$ (resp. \id{\_t}),
\item \id{<arg\_\-val>*}---similarly, list of~values
   of~arguments of~\id{<flow\_\-id>};
   tokens of~output constants must be placed here.
\end{itemize}
The~command substitutes each \id{ode} variant and invariant
by~a~Boolean constant, and, of~course, also all formal parameters
of~the~defined flow, including initial values (\id{<in\_\-val>}).
The~resulting expression is a~conditional,
which can be, typically, used inside an~\id{assert} command.
\\Example:
\begin{Verbatim}
(assert (and (flow flow1 (_t_0 x_0 y_0) (a_0 a_1 _t_1 x_1 y_1)) )
             (flow flow1 (_t_1 x_1 y_1) (a_1 a_2 _t_2 x_2 y_2)) )
))
\end{Verbatim}

As~shown in~the~example,
particular flows of~the~same ODE scheme (\id{<flow\_\-id>})
are typically connected into a~cascade via~input and output values.

Note that all input and argument values are constants,
i.e.~they do not change during the~whole flow.
\end{paragraph} %%\id{flow}

%%%

\end{subsection} %%Flows usage

%%%%%%%%%%%%%

% % Sun 04 Nov 2018 10:20:42 PM CET
% \begin{subsection}{ODE solver configuration}\label{ss:ode:conf}

% %%%

% %% ~~ set-interval-equidistance

% %% ~~ set-epsilon (pri porovnavani float cisel zejmena na rovnost)

% \end{subsection} %%ODE solver configuration

%%%%%%%%%%%%%

\end{section} %%ODE constructs

%%%%%%%%%%%%%%%%%%%%%%%%%%%%

% Thu 02 May 2019 01:58:10 PM CEST
\begin{section}{Language usage}\label{s:usage}
This section describes recommended form and structure
of~input system specifications.
\open[The tool]{UN/SOT} is aimed mainly for~hybrid systems modeling and verification,
but since the~input language is designed to~be quite general
(as~well as~the~whole idea of~satisfiability problem),
it is likely that it is suitable also for~other use cases.

The~input is \hl{static},
thus all features (e.g.~number of~constants)
must be known before the~procedure is started.
(However, this does not mean that the~input
must be deterministic---%
see section below for~details, where initialization is discussed.)
Each part of~evaluation, according to~flow instances,
requires its own set of~constants.
From~the~core language point of~view,
the~entire responsibility of~handling all constants
(their interconnection, initialization, etc.)
lays on~the~user.
Typical use case is to~connect constants
into~sequence of~successive flows.

Note that, from functional point of~view,
ordering of~assertions does not matter at~all,
due to~the~static property of~the input.
Again, all equality assertions of~certain constant
must hold at~the~same time,
there does not appear any progress of its value,
like it would be, for example, in~procedural programming language.

The~following subsections describe structures
that appears in~typical use case
and that should help beginners to~construct the~model of~their problem.
All subsections share the~same set of~used functions (and constants).

%%%%%%%%%%%%%

% Tue 06 Nov 2018 12:51:21 PM CET
\begin{subsection}{Definition of~\id{Bool}s and \id{Real}s}
\label{ss:usage:def}
Firstly, it is advisable to~declare or define
all needed functions and constants (except of~flows themselves),
which can be used globally anywhere thereafter.

It is recommended to~suffix all flow-related constants
by ordinal~number of~the~flow instance (as~it is more synoptical),
but the~solver ignores naming of~all~identifiers.

%%%

% Tue 06 Nov 2018 05:02:06 PM CET
\begin{paragraph}{\id{Bool}s.}\label{p:usage:def:bool}
Boolean constants can form the~discrete state of~a~modelled system.
One has to~declare constants for~every flow,
so that the~solver can select various variants of~\id{ode}s for~all flows.
But Booleans can be, of~course, used also in~other ordinary functions.

It is often suitable to~also define Boolean functions
that characterize concrete dicrete states
which are related to~\id{ode} variants (or other stuff).

When making decisions based on~discrete state,
be sure to~cover all possible combinations of~Boolean constants,
or to~forbid the~unused.

Example:
\begin{Verbatim}[samepage=true]
;; Boolean flow constants
(declare-const a_0 Bool) (declare-const a_1 Bool)
(declare-const b_0 Bool) (declare-const b_1 Bool)

;; Locations of representative automaton
(define-fun is_loc1    ((&a Bool) (&b Bool)) Bool  (and       &a  (not &b) ))
(define-fun is_loc2    ((&a Bool) (&b Bool)) Bool  (and  (not &a)      &b  ))
(define-fun is_loc3    ((&a Bool) (&b Bool)) Bool  (and  (not &a) (not &b) ))
(define-fun _forbidden ((&a Bool) (&b Bool)) Bool  (and       &a       &b  ))

;; Example of usage:
(assert (and (=> (is_loc1 a_0 b_0) ..) (=> (is_loc2 a_0 b_0) ..)
             (=> (is_loc3 a_0 b_0) ..)
             (=> (is_loc1 a_1 b_1) ..) (=> (is_loc2 a_1 b_1) ..)
             (=> (is_loc3 a_1 b_1) ..) ))
(assert (not (or (_forbidden a_0 b_0) (_forbidden a_1 b_1) )))
\end{Verbatim}
\end{paragraph} %%\id{Bool}s.

%%%

% Mon 09 Dec 2019 04:24:28 AM CET
\begin{paragraph}{\id{Real}s.}\label{p:usage:def:real}
It is necessary to~declare real constants
of~all input and output \id{ode} values of~all flows,
and also of~$t$.
A~constant%
\footnote{More generally speaking, it is a~value,
but it is unusual to~not use constants and use directly concrete literals.}
is necessary for~every \id{ode} initial value for~each flow,
and also for~$t$.
It is recommended to~suffix these with~the~same ordinal number
of~the~flow instance.

One might often want to~define ordinary real functions,
which can be used inside flows too.
This is often suitable for~decreasing redundancy and enhancing lucidity.

Also, real functions frequently depend on~constant coefficients,
which are better to~introduce under some descriptive identifier,
than to~directly use literals.

Example:
\begin{Verbatim}[samepage=true]
;; Real constants
(define-fun g  () Real 9.81)
(define-fun m1 () Real 10) (define-fun m2 () Real 12.5)

;; Real flow constants
(declare-const _t_0 Real) (declare-const _t_1 Real)
(declare-const  h_0 Real) (declare-const  h_1 Real)
(declare-const  v_0 Real) (declare-const  v_1 Real)

;; Potentional energy function
(define-fun U_g   ((&m Real) (&h Real)) Real  (* &m g &h))

;; Flow-related stuff - omited here ...

;; Example of usage:
(assert (and  (<= (U_g m1 h_0) 100) (<= (U_g m1 h_1) 100)
              (<= (U_g m2 h_0) 150) (<= (U_g m2 h_1) 150) ))
\end{Verbatim}
\end{paragraph} %%\id{Real}s.

%%%

\end{subsection} %%Definition of~\id{Bool}s and \id{Real}s

%%%%%%%%%%%%%

% Thu 08 Nov 2018 04:21:56 PM CET
\begin{subsection}{Initializations}\label{ss:usage:init}
Both Boolean and real constants typically
need to~be deterministically initialized,
unless they are intended to~be nondeterministic.
To~achieve initialization, one has to~simply
use conditional formulas on~constants related to~first flow
(or to~no flow at~all) inside \id{assert} commands.

%%%

% Tue 06 Nov 2018 09:24:25 PM CET
\begin{paragraph}{\id{Bool}s.}\label{p:usage:init:bool}
In~case of~Booleans, their initialization is optional, not required at~all.
\end{paragraph} %%\id{Bool}s.

%%%

% Thu 02 May 2019 02:38:05 PM CEST
\begin{paragraph}{\id{Real}s.}\label{p:usage:init:real}
In~case of~reals, for~each constant,
one has these options of~its initialization:
\begin{itemize}
\item deterministic: the~constant is asserted
   to~be equal to~exactly one value
   (more values would likely result in~conflict),
\item nondeterministic:
   \begin{itemize}
   \item finite: the~constant is asserted
      to~more than one value in~logical sum;
      the~number of options is finite and known,
   \item infinite: the~constant appears inside an~inequality,
      which would mean that there is infinite number of~options,
      which is not desired;
      to~avoid this, the~solver approximates this case
      like~the~finite case---by~logical sum
      of~particular values of~the~interval
      with~equidistance, which size is set globally;
      this can, however, generate huge number of~options.
   \end{itemize}
\end{itemize}

Every real constant must have both upper and lower bound,
otherwise it is an~error.

Initial value of~$t$ (in~first flow) must be deterministic.
\end{paragraph} %%\id{Real}s.

%%%

\bigskip

Example:
\begin{Verbatim}[samepage=true]
;; Initializations
(assert (and  (= t_0 0)
              (or (= h_0 5) (= h_0 5.5) (= h_0 6) (= h_0 6.5) (= h_0 7) )
              (and (>= v_0 -0.1) (<= v_0 0.1) )
              (not a_0)
))
\end{Verbatim}
\end{subsection} %%Initializations

%%%%%%%%%%%%%

% Thu 02 May 2019 02:44:13 PM CEST
\begin{subsection}{Definition and instantiation of~flows}\label{ss:usage:flows}
In~core language, definition of~flows
and selection of~their variants happens at~once
via~\id{define-flow} command.
Then, they are connected with~all input and output constants (resp. values)
via~\id{flow} commands, arbitrarily (but finitely) many times.
As~each \id{flow} command produces a~conjunction of~predicates,
they are typically used inside \id{assert} commands.

Each flow (\id{flow}) is understood as~continuous evaluation
of~real variables, until invariant of~the~flow is violated.
This is usually, but not necessarily, followed by~a~discrete jump
to~different discrete state.
For example, when used in~bounded model checking (BMC)
of~a~hybrid automaton,
each step of~BMC correspond to~one flow preceded by~a~jump.
This means that number of~\id{flow} calls
matches the~number of~desired steps.

Usage of~\id{define-flow} and \id{flow} commands with~examples
is described in~section~\rf{ss:ode:flows}.
\end{subsection} %%Definition of~flows

%%%%%%%%%%%%%

% Mon 09 Dec 2019 04:36:11 AM CET
\begin{subsection}{Jump conditions}\label{ss:usage:jumps}
A~jump models discrete change of~system state,
of~both Boolean and real constants.
Its most significant effects can be:
\begin{itemize}
\item discrete state change (i.e.~assignment of~successive Boolean constants)
   depending on~current system state;
   in~context of~(hybrid) automatons, discrete state can be modeled
   with~locations and state change as~a~transition between locations,
\item jump assignments of~real variables,
   e.g.~resetting a~timer (but the~basic one is included by~default),
   incrementing a~counter, flipping value of~a~variable
   (resp. of~its successive constant), etc.
\end{itemize}

The~important thing is that jump conditions are purely handled
by~SMT solver and are ignored by~ODE solver;
thus, no jump condition can interrupt any flow by~itself.
Flows always terminate only based on~their invariants.

An~example of~jumps is shown within the~\id{define-flow} command
in~section~\rf{ss:ode:flows}.
\end{subsection} %%Jump conditions

%%%%%%%%%%%%%

\end{section} %%Language usage

%%%%%%%%%%%%%%%%%%%%%%%%%%%%

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

\end{document}

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
