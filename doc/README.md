# UN/SOT documentation

Brief description of the contents:
* `articles`: free publications
    (only master thesis text in Czech languge is present so far),
* **`experiments`**: contains all necessary information, data and also scripts
    of performed experiments,
* `external`: documentation of external tools etc.,
* `img`: all project-related images (especially plots),
* `lang`: input language specification, including the preprocessing language.

Source codes are not properly documented yet.
