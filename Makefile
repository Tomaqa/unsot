C := gcc
CPP := g++
# CPP := clang++

LN := ln -rfs

###################################################

PROJ_NAME := unsot

ROOT_DIR   := $(abspath .)

INCL_DIR  := include
SRC_DIR   := src
BUILD_DIR := build
LIB_DIR   := lib
BIN_DIR   := bin
TEST_DIR  := test

INCL_LIB_DIR  := $(INCL_DIR)/$(PROJ_NAME)
SRC_LIB_DIR   := $(SRC_DIR)/$(PROJ_NAME)
BUILD_LIB_DIR := $(BUILD_DIR)/$(PROJ_NAME)

SRC_MAIN_DIR := $(SRC_DIR)/main
BUILD_MAIN_DIR := $(BUILD_DIR)/main

INCL_TEST_DIR := $(INCL_DIR)/$(TEST_DIR)
SRC_TEST_DIR := $(SRC_DIR)/$(TEST_DIR)
BUILD_TEST_DIR := $(BUILD_DIR)/$(TEST_DIR)
BIN_TEST_DIR := $(TEST_DIR)

INCL_TEST_LIB_DIR := $(INCL_TEST_DIR)/$(PROJ_NAME)
SRC_TEST_LIB_DIR := $(SRC_TEST_DIR)/$(PROJ_NAME)
BUILD_TEST_LIB_DIR := $(BUILD_TEST_DIR)/$(PROJ_NAME)
SRC_TEST_MAIN_DIR := $(SRC_TEST_DIR)/main
BUILD_TEST_MAIN_DIR := $(BUILD_TEST_DIR)/main

TOOLS_DIR := tools
DOC_DIR   := doc
DATA_DIR  := data
LOCAL_DIR  := local
EXTERNAL_DIR  := external

PROJ_CMD := $(BIN_DIR)/online_reals_minisat_odeint_solver $(BIN_DIR)/rail_minisat_odeint_solver $(BIN_DIR)/applet/online_reals_solver
PROJ_SCRIPT := $(BIN_DIR)/$(PROJ_NAME)

###################################################

override FLAGS += -Wall -Wextra -pedantic -Wshadow
override FLAGS += -isystem $(INCL_DIR) -I $(INCL_LIB_DIR) -I $(SRC_LIB_DIR)
override FLAGS += -O3
override FLAGS += -fopenmp
ifeq ($(CPP), g++)
override FLAGS += -pipe
endif
ifeq ($(CPP), clang++)
override FLAGS +=
endif

CPPFLAGS := $(FLAGS) -std=c++17
CFLAGS   := $(FLAGS) -std=gnu99
ifeq ($(CPP), g++)
CPPFLAGS +=
CFLAGS +=
endif
ifeq ($(CPP), clang++)
CPPFLAGS += -Wno-dtor-name -Wno-mismatched-tags -Wno-unused-lambda-capture -Wno-unqualified-std-cast-call
CFLAGS +=
endif

DBG_FLAGS := -g -DDEBUG -O0 -ftrapv
ifeq ($(CPP), g++)
DBG_FLAGS += -fmax-errors=3
DBG_FLAGS += -fno-inline
# DBG_FLAGS += -pg
endif
ifeq ($(CPP), clang++)
DBG_FLAGS += -ferror-limit=3
endif
DBG_CPPFLAGS := $(DBG_FLAGS) #-fvtable-verify=std
DBG_CFLAGS := $(DBG_FLAGS)

ifndef DEBUG
  ifeq ($(CPP), clang++)
  CPPFLAGS += -Wno-unused-parameter -Wno-unused-variable
  endif
endif

ifdef VERBOSE
  CPPFLAGS += -DVERBOSE=$(VERBOSE)
endif

ifdef QUIET
  CPPFLAGS += -DQUIET
endif

LIB_FLAGS :=
MAIN_FLAGS :=
TEST_FLAGS := -I $(INCL_TEST_LIB_DIR)
TEST_FLAGS += $(DBG_FLAGS)

LIB_CFLAGS := $(CFLAGS) $(LIB_FLAGS)
LIB_CPPFLAGS := $(CPPFLAGS) $(LIB_FLAGS)
LIB_CPPFLAGS += -Weffc++ -Wno-non-virtual-dtor -Wold-style-cast -Woverloaded-virtual -Wmissing-declarations
ifeq ($(CPP), g++)
LIB_CPPFLAGS +=
CPPFLAGS += -Wno-terminate -Wuseless-cast -Wsuggest-override
# CPPFLAGS += -Wsuggest-final-types -Wsuggest-final-methods
endif
ifeq ($(CPP), clang++)
LIB_CPPFLAGS +=
CPPFLAGS +=
endif
# LIB_CPPFLAGS += -Wnoexcept
MAIN_CPPFLAGS := $(CPPFLAGS) $(MAIN_FLAGS)
TEST_CPPFLAGS := $(CPPFLAGS) $(TEST_FLAGS)

LDFLAGS := -Wl,--no-undefined,-rpath,$(ROOT_DIR)/$(LIB_DIR)
LDFLAGS += -L $(ROOT_DIR)/$(LIB_DIR)
LDFLAGS += -lm -Wl,-Bstatic -lminisat -Wl,-Bdynamic

ifdef DEBUG
  LIB_CPPFLAGS += $(DBG_CPPFLAGS)
  LIB_CFLAGS += $(DBG_CFLAGS)
  MAIN_CPPFLAGS += $(DBG_CPPFLAGS)
endif

###################################################

DIRS_CMD := $(TOOLS_DIR)/dev/dirs.sh

MKDIR := mkdir -p
MKDIR_DIRS := $(LIB_DIR)/ $(LOCAL_DIR)/
MKDIR_DIRS += $(shell $(DIRS_CMD) $(SRC_LIB_DIR) $(BUILD_LIB_DIR))
MKDIR_DIRS += $(shell $(DIRS_CMD) $(SRC_MAIN_DIR) $(BUILD_MAIN_DIR))
MKDIR_DIRS += $(shell $(DIRS_CMD) $(SRC_MAIN_DIR) $(BIN_DIR))
MKDIR_DIRS += $(shell $(DIRS_CMD) $(SRC_TEST_LIB_DIR) $(BUILD_TEST_LIB_DIR))
MKDIR_DIRS += $(shell $(DIRS_CMD) $(SRC_TEST_MAIN_DIR) $(BUILD_TEST_MAIN_DIR))
MKDIR_DIRS += $(shell $(DIRS_CMD) $(SRC_TEST_MAIN_DIR) $(BIN_TEST_DIR))

###################################################

FIND_FLAGS := -not -path '*/\.*' -type f -name

LIB_HEADERS     := $(shell find $(INCL_LIB_DIR) $(FIND_FLAGS) *.hpp)
LIB_CPP_SOURCES := $(shell find $(SRC_LIB_DIR)  $(FIND_FLAGS) *.cpp)
LIB_C_SOURCES   := $(shell find $(SRC_LIB_DIR)  $(FIND_FLAGS) *.c)
LIB_SOURCES := $(LIB_CPP_SOURCES) $(LIB_C_SOURCES)

MAIN_SOURCES := $(shell find $(SRC_MAIN_DIR)  $(FIND_FLAGS) *.cpp)

TEST_LIB_SOURCES := $(shell find $(SRC_TEST_LIB_DIR)   $(FIND_FLAGS) *.cpp)
TEST_MAIN_SOURCES := $(shell find $(SRC_TEST_MAIN_DIR) $(FIND_FLAGS) *.cpp)

LIB_CPP_OBJECTS := $(patsubst $(SRC_LIB_DIR)/%, $(BUILD_LIB_DIR)/%, $(LIB_CPP_SOURCES:.cpp=.o))
LIB_C_OBJECTS := $(patsubst $(SRC_LIB_DIR)/%, $(BUILD_LIB_DIR)/%, $(LIB_C_SOURCES:.c=.o))
LIB_OBJECTS := $(LIB_CPP_OBJECTS) $(LIB_C_OBJECTS)

MAIN_OBJECTS := $(patsubst $(SRC_MAIN_DIR)/%, $(BUILD_MAIN_DIR)/%, $(MAIN_SOURCES:.cpp=.o))

TEST_LIB_OBJECTS := $(patsubst $(SRC_TEST_LIB_DIR)/%, $(BUILD_TEST_LIB_DIR)/%, $(TEST_LIB_SOURCES:.cpp=.o))
TEST_MAIN_OBJECTS := $(patsubst $(SRC_TEST_MAIN_DIR)/%, $(BUILD_TEST_MAIN_DIR)/%, $(TEST_MAIN_SOURCES:.cpp=.o))

OBJECTS := $(LIB_OBJECTS) $(MAIN_OBJECTS)
TEST_OBJECTS := $(TEST_LIB_OBJECTS) $(TEST_MAIN_OBJECTS)
ALL_OBJECTS := $(OBJECTS) $(TEST_OBJECTS)

CMDS := $(patsubst $(SRC_MAIN_DIR)/%, $(BIN_DIR)/%, $(MAIN_SOURCES:.cpp=))
TEST_CMDS := $(patsubst $(SRC_TEST_MAIN_DIR)/%, $(BIN_TEST_DIR)/%, $(TEST_MAIN_SOURCES:.cpp=))
ALL_CMDS := $(CMDS) $(TEST_CMDS)

###################################################

EXTERNALS := $(wildcard $(EXTERNAL_DIR)/*)

###################################################
###################################################

### Main rules

.PHONY: default all main clean print

default: external $(PROJ_SCRIPT) local tools

all: default main test

lib: external $(LIB_OBJECTS)

main: external $(MAIN_OBJECTS) $(CMDS)

test: external $(TEST_LIB_OBJECTS) $(TEST_MAIN_OBJECTS) $(TEST_CMDS)

local:

tools:

###################################################

### External rules

external: $(EXTERNALS)

###################################################

### Particular build files rules

$(BUILD_LIB_DIR)/%.o: $(SRC_LIB_DIR)/%.c
	$(C) -c $< $(LIB_CFLAGS) -o $@

$(BUILD_LIB_DIR)/%.o: $(SRC_LIB_DIR)/%.cpp
	$(CPP) -c $< $(LIB_CPPFLAGS) -o $@

$(BUILD_MAIN_DIR)/%.o: $(SRC_MAIN_DIR)/%.cpp
	$(CPP) -c $< $(MAIN_CPPFLAGS) -o $@

$(BUILD_TEST_LIB_DIR)/%.o: $(SRC_TEST_LIB_DIR)/%.cpp
	$(CPP) -c $< $(LIB_CPPFLAGS) $(TEST_CPPFLAGS) -o $@

$(BUILD_TEST_MAIN_DIR)/%.o: $(SRC_TEST_MAIN_DIR)/%.cpp
	$(CPP) -c $< $(TEST_CPPFLAGS) -o $@

$(BIN_DIR)/%: $(BUILD_MAIN_DIR)/%.o
	$(CPP) $^ $(MAIN_CPPFLAGS) $(LDFLAGS) -o $@

$(BIN_TEST_DIR)/%: $(BUILD_TEST_MAIN_DIR)/%.o
	$(CPP) $^ $(TEST_CPPFLAGS) $(LDFLAGS) -o $@

###################################################

### Symbolic link rules

$(PROJ_SCRIPT): | $(PROJ_CMD)
	@$(LN) $(TOOLS_DIR)/bin/$(PROJ_NAME).sh $@

###################################################

.SECONDEXPANSION:

### External rules

$(EXTERNALS): | $$@/build
$(EXTERNAL_DIR)/%/build:
	@$(MAKE) -C $(@D)
#++ what about libraries an binaries?
$(EXTERNALS): | $(INCL_DIR)/$$(@F)

## Minisat specific rules
$(INCL_DIR)/minisat: $(EXTERNAL_DIR)/minisat/config.mk
	@$(MAKE) -C $(<D) install
	@touch $(INCL_DIR)/minisat
$(EXTERNAL_DIR)/minisat/config.mk:
	@$(MAKE) -C $(@D) config prefix=$(ROOT_DIR)
$(INCL_DIR)/minisat: $(EXTERNAL_DIR)/minisat/build

###################################################

### Clean rules

clean: clean-lib clean-main

clean-lib:
	rm -fr $(BUILD_LIB_DIR)/*

clean-main:
	rm -fr $(BUILD_MAIN_DIR)/* $(BIN_DIR)/*

clean-test:
	rm -fr $(BUILD_TEST_DIR)/* $(BIN_TEST_DIR)/*

clean-all: clean clean-test

###################################################

## Print internal variables
print:
	@echo LIB_HEADERS $(LIB_HEADERS)
	@echo LIB_SOURCES $(LIB_SOURCES)
	@echo LIB_OBJECTS $(LIB_OBJECTS)
	@echo MAIN_SOURCES $(MAIN_SOURCES)
	@echo MAIN_OBJECTS $(MAIN_OBJECTS)
	@echo OBJECTS $(OBJECTS)
	@echo CMDS $(CMDS)
	@echo TEST_LIB_SOURCES $(TEST_LIB_SOURCES)
	@echo TEST_LIB_OBJECTS $(TEST_LIB_OBJECTS)
	@echo TEST_MAIN_SOURCES $(TEST_MAIN_SOURCES)
	@echo TEST_MAIN_OBJECTS $(TEST_MAIN_OBJECTS)
	@echo TEST_OBJECTS $(TEST_OBJECTS)
	@echo TEST_CMDS $(TEST_CMDS)
	@echo ALL_OBJECTS $(ALL_OBJECTS)
	@echo ALL_CMDS $(ALL_CMDS)
	@echo MKDIR_DIRS $(MKDIR_DIRS)
	@echo EXTERNALS $(EXTERNALS)

$(MKDIR_DIRS):
	@$(MKDIR) $@

## Create directory if necessary
$(ALL_OBJECTS) $(ALL_CMDS): | $$(@D)/
$(LIB_DIR) $(LOCAL_DIR): | $$@/

###################################################
###################################################

.depend: external
	$(TOOLS_DIR)/dev/deps.sh

include .depend
include .obj_depend
