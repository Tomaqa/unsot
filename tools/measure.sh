#!/bin/bash

source tools/lib

function usage {
  printf -- "Measures all combinations of input parameters and outputs CSV data.\n"
  printf -- "USAGE: %s <file> [<param>:<val1>,<val2>,... | <var>] ...\n" "$0"
  printf -- "\t<var> arguments are variables to be observed in the sat model.\n"
  printf -- "\nOptionally pass TIMEOUT env. variable (a number with time unit suffix s|m|h|d /s is default/).\n"
  exit 0
}

function shutdown {
  for p in ${PIDS[@]}; do
    (( $p )) && kill $p
  done
  for f in ${OUT_FS[@]}; do
    rm -f $f
  done
  exit $1
}

TIMEOUT_RET=124

PROFILE_IDS=(total_t prep_t solve_t)
SKIP_PROFILE_IDS=()
if [[ $CMD == $OFFLINE_CMD ]]; then
  PROFILE_IDS+=(isolve_t fsolve_t bools_t bools_c reals_t preds_t reals_c)
else
  PROFILE_IDS+=(solve_c isolve_t fsolve_t tback_t tback_c
                tprop_t tprop_c tprop_cp tsugg_t tsugg_c tsugg_cp
                texp_t texp_c texp_cp texp_l tinc_t tinc_c tinc_cp tinc_l)
  SKIP_PROFILE_IDS+=(solve_c texp_cp tinc_cp)
  [[ $CMD != $ONLINE_SMT2_CMD ]] && PROFILE_IDS+=(flow_t flow_c)
fi
declare -a TRIMMED_PROFILE_IDS

for id in ${PROFILE_IDS[@]}; do
  contains_elem SKIP_PROFILE_IDS $id || TRIMMED_PROFILE_IDS+=($id)
done

function run1 {
  local params
  local vals

  for i in ${!PARAM_IDS[@]}; do
    local id=${PARAM_IDS[$i]}
    local conf=${COUNTERS[$i]}
    local -n vals_i=PARAM_VALS_$i
    local val=${vals_i[$conf]}
    vals+=($val)
    params+=(-D${id}=${val})
  done

  (( ${#PARAMS[@]} )) && printf -- "%s;" ${PARAMS[@]/#-D/}
  [[ ${vals[0]} =~ ^[=+] ]] && vals[0]=\'${vals[0]}
  print_array vals ';'
  printf ';'
  vals=()

  local out_f=`mktemp`
  timeout $TIMEOUT "${CMD[@]}" "$DATA" ${params[@]} ${PARAMS[@]} -o $out_f &
  local pid=$!
  local i=${#PIDS[@]}
  PIDS[$i]=$pid
  OUT_FS[$i]=$out_f

  wait $pid
  local ret=$?
  (( $ret && $ret != $TIMEOUT_RET )) && shutdown $ret
  PIDS[$i]=0

  if (( $ret == $TIMEOUT_RET )); then
    vals+=(?)
    for obs in ${OBSERVES[@]}; do
      vals+=(?)
    done
    vals+=("> ${TIMEOUT}")
  else
    local out=`<$out_f`
    local lines
    split_string "$out" lines $'\n'
    local sat=${lines[0]}
    vals+=($sat)
    for obs in ${OBSERVES[@]}; do
      if [[ $sat == sat ]]; then
        local oval
        for l in "${lines[@]}"; do
          local a
          split_string "$l" a :=
          [[ ${a[0]} != $obs ]] && continue
          oval=${a[1]}
        done
        [[ -z $oval ]] && {
          printf -- "!! '%s' observe not found !!\n" $obs >&2
          shutdown 1
        }
        vals+=($oval)
      else
        vals+=(x)
      fi
    done

    local i
    local size=${#PROFILE_IDS[@]}
    for (( i=0; $i < $size; ++i )); do
      local id=${PROFILE_IDS[$i]}
      contains_elem SKIP_PROFILE_IDS $id && continue
      local line=${lines[$(( -$size +$i ))]}
      line=(${line#*: })
      line=${line[0]}
      vals+=($line)
    done
  fi

  print_array vals ';'
  printf -- "\n"

  return 0
}

function run {
  local level=$1
  (( $level == $N )) && {
    run1
    return 0
  }
  local -n counter=COUNTERS[$level]
  local -n len=LENGTHS[$level]
  for (( counter=0; $counter < $len; ++counter )); do
    run $(( $level + 1 ))
  done
}

unset PIDS OUT_FS
trap "shutdown" INT TERM

ROOT=$(realpath "`dirname "$0"`")
while [[ ! `basename "$ROOT"` =~ ^unsot ]]; do
  ROOT=`dirname "$ROOT"`
done

CMD=("$ROOT/bin/unsot" -P)

[[ -z $1 ]] && usage
DATA=`realpath -s "$1"`
shift 1

if [[ -n $TIMEOUT ]]; then
  [[ $TIMEOUT =~ ^[0-9]+(|\.[0-9]+)(|s|m|h|d)$ ]] || {
    printf -- "Invalid format of timeout: %s\n" $TIMEOUT >&2
    exit 2
  }
else
  TIMEOUT=0
fi

cd "$ROOT/bin"
ROOT=../

_PARAM_IDS=()
_PARAM_VALS=()
PARAMS=()
for arg in $@; do
  [[ $arg =~ ^[a-zA-Z] ]] || {
    printf -- "Unrecognized argument: %s\n" "$arg" >&2
    exit 2
  }

  [[ $arg =~ : ]] || {
    _PARAM_IDS+=($arg)
    _PARAM_VALS+=()
    continue
  }

  param_id=${arg%:*}
  param_vals=${arg#*:}
  [[ -z $param_vals ]] && {
    PARAMS+=(-D$param_id)
    continue
  }

  _PARAM_IDS+=($param_id)
  _PARAM_VALS+=($param_vals)
done

PARAM_IDS=()
OBSERVES=()
COUNTERS=()
LENGTHS=()
for i in ${!_PARAM_IDS[@]}; do
  param_id=${_PARAM_IDS[$i]}
  param_vals=${_PARAM_VALS[$i]}

  COUNTERS[$i]=0
  [[ -z $param_vals ]] && {
    OBSERVES+=($param_id)
    LENGTHS[$i]=1
    continue
  }

  declare -n vals=PARAM_VALS_$i
  split_string $param_vals vals ,
  PARAM_IDS[$i]=$param_id
  LENGTHS[$i]=${#vals[@]}
done

N=$(( ${#PARAM_IDS[@]} + ${#OBSERVES[@]} ))

if [[ -z $SAME ]] || (( ! $SAME )); then
  IDS=()
  (( ${#PARAMS[@]} )) && IDS+=(other)
  IDS+=(${PARAM_IDS[@]} result ${OBSERVES[@]} ${TRIMMED_PROFILE_IDS[@]})
  print_array IDS ';'
  printf -- "\n"
fi

run 0
