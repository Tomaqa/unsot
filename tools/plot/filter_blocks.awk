BEGIN {
    def_RS = RS
}

/^#/ {
    id = $2
    RS = "#"
    getline data[id]
    RS = def_RS
}

END {
    split(data[0], rows, RS)
    head = rows[1]
    split(head, cols, OFS)
    for (j in cols) {
        if (j == 1) mask[j] = 0
        else mask[j] = (cols[j] ~ /^_/)
    }
    for (id in data) {
        split(data[id], rows, RS)
        for (i in rows) {
            split(rows[i], cols, OFS)
            for (j in cols) {
                if (!mask[j]) printf("%s ", cols[j])
            }
            printf("\n")
        }
    }
}
