#!/bin/bash

GNUPLOT_CMD=(gnuplot)
AWK_CMD=(awk)

command -v "${GNUPLOT_CMD[@]}" &>/dev/null || {
    printf -- "Gnuplot not found.\n" >&2
    exit 1
}

command -v "${AWK_CMD[@]}" &>/dev/null || {
    printf -- "awk not found.\n" >&2
    exit 1
}

function usage {
    printf -- "USAGE: %s <data_file> [<plot_file>] [<gnuplot_script_file>]\n" "$0"
}

[[ -z $1 ]] && {
    usage
    exit 1
}

TOOLS_PLOT_DIR=`dirname "$0"`

DATA_FILE="$1"
TMP_DATA_FILE=`mktemp`

if [[ -n $2 ]]; then
    PLOT_FILE="$2"
else
    dname=`dirname "$DATA_FILE"`
    bname=`basename "$DATA_FILE"`
    PLOT_FILE="$dname/${bname%.*}_plot.svg"
fi

AWK_FILTER_SCRIPT="$TOOLS_PLOT_DIR/filter_blocks.awk"
if [[ -n $3 ]]; then
    GP_TRAJ_SCRIPT="$3"
else
    GP_TRAJ_SCRIPT="$TOOLS_PLOT_DIR/traject.gp"
fi

printf -- "Generating plot '%s' ..." "$PLOT_FILE"

[[ ! "$DATA_FILE" =~ _all$ ]] && grep -q '^#' "$DATA_FILE" && {
    "${AWK_CMD[@]}" -f "$AWK_FILTER_SCRIPT" <"$DATA_FILE" >"$TMP_DATA_FILE"
    mv "$DATA_FILE"{,_all}
    mv "$TMP_DATA_FILE" "$DATA_FILE"
}

"${GNUPLOT_CMD[@]}" -e "ifname='$DATA_FILE'; ofname='$PLOT_FILE'" \
                       "$GP_TRAJ_SCRIPT"

printf -- " done.\n"

exit 0
