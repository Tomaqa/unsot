set terminal svg size 800,480 noenhance

set output ofname

set style data lines

set xlabel "Time [s]"
set ylabel "State"

stats ifname using 1 nooutput
columns = STATS_columns
blocks = STATS_blocks-1

xmin = STATS_min
xmax = STATS_max

ymax = -1e30
do for [i=2:columns] {
    stats ifname using i nooutput
    ymax = (ymax > valid(STATS_max)) ? ymax : STATS_max
}

set key horizontal tmargin left

set xrange [xmin:xmax]
set y2range [0:ymax]

plot for [b=0:blocks-1] for [j=2:columns] ifname index b \
    using 1:j lt (j-1) title b==0 ? columnhead(j) : "", \
for [b=0:blocks-1] "" index b \
    using 1:(column(0) == 1 ? ymax : NaN) axes x1y2 with impulse notitle \
    lc black lw 0.5 dashtype 2
