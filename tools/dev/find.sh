#!/bin/bash

NAME_ARGS=(-name)
if [[ -z $NAME ]]; then
    NAME_ARGS+=('*')
else
    NAME_ARGS+=("$NAME")
fi

if [[ -z $DIRS ]]; then
    DIRS=(.)
else
    DIRS=($DIRS)
fi

GREP_CMD=(grep -E -H -I)
find "${DIRS[@]}" -type f "${NAME_ARGS[@]}" -exec "${GREP_CMD[@]}" "$@" {} \;
