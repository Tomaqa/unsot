#!/bin/bash

PLUS_PATT=0
GLOB_PATT=0

[[ $# == 1 ]] && {
    arg=$1
    if [[ $1 =~ ^[+]+$ ]]; then
        PLUS_PATT=${#arg}
        shift 1
    elif [[ $1 =~ ^[*]+$ ]]; then
        GLOB_PATT=${#arg}
        shift 1
    fi
}

if [[ -z $1 ]]; then
    if (( $PLUS_PATT || $GLOB_PATT )); then
       APPEND_PATT='['
       (( $GLOB_PATT )) && APPEND_PATT+=' '
       APPEND_PATT+='!+]'
       if (( $PLUS_PATT > 1 )); then
          APPEND_PATT+="$APPEND_PATT"
       else
          APPEND_PATT+='|/[*]|[*]/'
       fi
    else
       APPEND_PATT=' '
    fi
    ARGS=('(^[^/"]*(//'"$APPEND_PATT)|^[^#].*CDBG)")
else
    ARGS=("$@")
fi

[[ -z $DIRS ]] && {
    LDIRS=(src/{unsot,main,test} include/{unsot,test})
    DIRS="${LDIRS[@]}"
}

export NAME
export DIRS

exec `dirname "$0"`/find.sh "${ARGS[@]}"
