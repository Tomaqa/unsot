#!/bin/bash

make clean_all
./tools/dev/deps.sh
make init

[[ -z $1 ]] && exec make

exec "$@"
