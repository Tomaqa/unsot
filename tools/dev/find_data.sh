#!/bin/bash

if [[ -z $1 ]]; then
    ARGS=('^[ ]*; ')
else
    ARGS=("$@")
fi


[[ -z $DIRS ]] && {
    DIRS=data/smto
}

export NAME
export DIRS

exec `dirname "$0"`/find.sh "${ARGS[@]}"
