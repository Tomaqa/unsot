#!/bin/bash

find src/unsot/ include/unsot/ -type f ! -name *.gch -exec cat {} + | wc -l
