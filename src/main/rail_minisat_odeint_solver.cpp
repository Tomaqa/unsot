#include "solver/run.hpp"
#include "smt/solver/online/reals/run.hpp"
#include "solver/online/reals/rail/minisat/odeint.hpp"

using namespace unsot::solver;
using namespace unsot::ode::solver;
namespace solver = unsot::solver;

int main(int argc, const char* argv[])
{
    using Solver = solver::online::reals::rail::minisat::Solver<Odeint>;
    return Solver::Run(argc, argv).run();
}
