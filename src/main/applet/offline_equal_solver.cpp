#include "smt/solver/offline/bools/run.hpp"
#include "smt/solver/offline/reals/equal/minisat.hpp"

using namespace unsot::smt::solver;

int main(int argc, const char* argv[])
{
    return offline::reals::equal::minisat::Solver::Run(argc, argv).run();
}
