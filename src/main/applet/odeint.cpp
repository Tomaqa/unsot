#include "ode/solver/run.hpp"
#include "ode/solver/odeint.hpp"

using namespace unsot::ode::solver;

int main(int argc, const char* argv[])
{
    return Odeint::Run<Odeint>(argc, argv).run();
}
