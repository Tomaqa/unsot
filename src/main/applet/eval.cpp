#include "expr/fun/run.hpp"

#ifdef NO_EXPLICIT_TP_INST
#include "expr/fun_arith.hpp"
#endif

using namespace unsot::expr;

int main(int argc, const char* argv[])
{
    return fun::Tp<double>::Run(argc, argv).run();
}
