#include "smt/solver/online/reals/run.hpp"
#include "smt/solver/online/reals/rail/minisat.hpp"

using namespace unsot::smt::solver;

int main(int argc, const char* argv[])
{
    return online::reals::rail::minisat::Solver::Run(argc, argv).run();
}
