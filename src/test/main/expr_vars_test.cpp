#include "test.hpp"
#include "expr/var/vars_arith.hpp"
#include "expr/var/vars/alg.hpp"

#include "util/alg.hpp"
#include "util/numeric/alg.hpp"

namespace unsot::test {
    using namespace expr;
    using namespace expr::var;
    using namespace expr::var::vars;
    using namespace util::numeric;

    template <typename Sort, typename ArgSort = Sort, Type typeV = Type{}, typename ConfT = conf::Tp<typeV>>
    struct Vars_input : Static<Vars_input<Sort, ArgSort, typeV, ConfT>> {
        struct Unset_var : Object<Unset_var> {
            Unset_var(Key key_) : key(move(key_)) { }

            Key key;

            String to_string() const
            {
                using unsot::to_string;
                return to_string(key);
            }
        };
        struct Set_var : Object<Set_var> {
            Set_var(Key key_, Sort val = {})
                : key(move(key_)), value(move(val)) { }

            Key key;
            Sort value;

            String to_string() const
            {
                using unsot::to_string;
                return to_string(key) + " " + to_string(value);
            }
        };
        struct Unset_fun : Object<Unset_fun> {
            Unset_fun(Key key_, List ls_ = {})
                : key(move(key_)), ls(move(ls_)) { }

            Key key;
            List ls;

            String to_string() const
            {
                using unsot::to_string;
                return to_string(key) + " " + to_string(ls);
            }
        };
        struct Set_fun : Object<Set_fun> {
            Set_fun(Key key_, Sort val = {}, List ls_ = {})
                : key(move(key_)), value(move(val)), ls(move(ls_)) { }

            Key key;
            Sort value;
            List ls;

            String to_string() const
            {
                using unsot::to_string;
                return to_string(key) + " " + to_string(value) + " " + to_string(ls);
            }
        };

        Vector<Unset_var> unset_vars;
        Vector<Set_var> set_vars;
        Vector<Unset_fun> unset_funs;
        Vector<Set_fun> set_funs;

        Vars_input(Vector<Unset_var>&& unset_vars_ = {},
                   Vector<Set_var>&& set_vars_ = {},
                   Vector<Unset_fun>&& unset_funs_ = {},
                   Vector<Set_fun>&& set_funs_ = {})
            : unset_vars(move(unset_vars_)),
              set_vars(move(set_vars_)),
              unset_funs(move(unset_funs_)),
              set_funs(move(set_funs_))
        { }
        virtual ~Vars_input() = default;

        virtual size_t size() const
        {
            return unset_vars.size()
                 + set_vars.size()
                 + unset_funs.size()
                 + set_funs.size()
                 ;
        }

        virtual String to_string() const
        {
            using unsot::to_string;
            return to_string(unset_vars) + "\n"
                 + to_string(set_vars) + "\n"
                 + to_string(unset_funs) + "\n"
                 + to_string(set_funs);
        }

        using Vars = var::Vars<Sort, ArgSort, typeV, ConfT>;
        using Arg_vars = var::Vars<ArgSort, ArgSort, typeV, ConfT>;
        using Pred_vars = var::Vars<Bool, ArgSort, typeV, ConfT>;

        using Rets_base_link = Link_tp<typename Vars::Rets_base>;
        using Args_base_link = Link_tp<typename Vars::Args_base>;
        using Preds_base_link = Link_tp<typename Vars::Preds_base>;

        mutable Rets_base_link rets_base_l{};

        mutable Arg_vars arg_vars{};
        mutable Args_base_link args_base_l{};

        mutable Pred_vars pred_vars{};
        mutable Preds_base_link preds_base_l{};

        virtual void add_vars(Vars& vars) const
        {
            for (auto& v : unset_vars) {
                vars.add_var(v.key);
            }
            for (auto& v : set_vars) {
                vars.add_var(v.key, v.value);
            }
            try {
                for (auto& v : unset_funs) {
                    if constexpr (Vars::Types::has_funs_v) vars.add_fun(v.ls, v.key);
                    else throw dummy;
                }
                for (auto& v : set_funs) {
                    if constexpr (Vars::Types::has_funs_v) vars.add_fun(v.ls, v.key, v.value);
                    else throw dummy;
                }
            }
            catch (Dummy) {
                THROW("Cannot add functions!");
            }
        }

        virtual Vars cons_vars_base() const
        {
            Vars vars;
            if constexpr (!Vars::is_global_v) {
                if (rets_base_l) vars.connect(*rets_base_l);
                if (args_base_l) vars.connect_args(*args_base_l);
                else vars.connect_args(arg_vars);
            }
            return vars;
        }

        virtual Vars cons_vars() const
        {
            /// To also check move assignment
            Vars tmp(cons_vars_base());
            add_vars(tmp);
            Vars vars;
            vars = move(tmp);

            if constexpr (typeV == Type::auto_assignee) {
                if (preds_base_l) vars.connect_preds(*preds_base_l);
                else if constexpr (Vars::Types::is_bools_v)
                    vars.connect_preds(vars);
                else vars.connect_preds(pred_vars);
            }

            return vars;
        }
    };

    template <typename ArgSort = Bool, Type typeV = Type{}, typename ConfT = conf::Tp<typeV>>
    struct Bools_input : Inherit<Vars_input<Bool, ArgSort, typeV, ConfT>> {
        using Inherit = unsot::Inherit<Vars_input<Bool, ArgSort, typeV, ConfT>>;

        using typename Inherit::Unset_var;
        using typename Inherit::Set_var;
        using typename Inherit::Unset_fun;
        using typename Inherit::Set_fun;

        using Unset_pred = Unset_fun;
        using Set_pred = Set_fun;

        Vector<Unset_pred> unset_preds;
        Vector<Set_pred> set_preds;

        Bools_input(Vector<Unset_var>&& unset_vars_ = {},
                    Vector<Set_var>&& set_vars_ = {},
                    Vector<Unset_fun>&& unset_funs_ = {},
                    Vector<Set_fun>&& set_funs_ = {},
                    Vector<Unset_pred>&& unset_preds_ = {},
                    Vector<Set_pred>&& set_preds_ = {})
            : Inherit(move(unset_vars_),
                      move(set_vars_),
                      move(unset_funs_),
                      move(set_funs_)),
              unset_preds(move(unset_preds_)),
              set_preds(move(set_preds_))
        { }
        virtual ~Bools_input() = default;

        size_t size() const override
        {
            return Inherit::size()
                 + unset_preds.size()
                 + set_preds.size()
                 ;
        }

        String to_string() const override
        {
            using unsot::to_string;
            return Inherit::to_string() + "\n"
                 + to_string(unset_preds) + "\n"
                 + to_string(set_preds);
        }

        using typename Inherit::Vars;

        void add_vars(Vars& vars) const override
        {
            Inherit::add_vars(vars);

            for (auto& v : unset_preds) {
                vars.add_pred(v.ls, v.key);
            }
            for (auto& v : set_preds) {
                vars.add_pred(v.ls, v.key, v.value);
            }
        }
    };

    template <typename Sort, typename ArgSort = Sort, Type typeV = Type{}, typename ConfT = conf::Tp<typeV>>
        using Vars_input_ptr = Shared_ptr<Vars_input<Sort, ArgSort, typeV, ConfT>>;

    ////////////////////////////////////////////////////////////////

    template <typename Sort, typename ArgSort = Sort, Type typeV = Type{}, typename ConfT = conf::Tp<typeV>>
    struct Vars_case : Inherit<Void_case<Vars_input_ptr<Sort, ArgSort, typeV, ConfT>>> {
        using Inherit = unsot::Inherit<Void_case<Vars_input_ptr<Sort, ArgSort, typeV, ConfT>>>;

        using Inherit::Inherit;

        using typename Inherit::Params;

        using Vars_input = typename Params::element_type;
        using Vars = typename Vars_input::Vars;

        using Bools_input = test::Bools_input<ArgSort, typeV, ConfT>;

        static_assert(!Vars::Types::is_bools_v
                      || is_derived_from_v<Bools_input, Vars_input>);

        void do_stuff() override
        {
            Vars vars(this->params()->cons_vars());

            Keys var_keys;
            var_keys.reserve(vars.size());

            for (auto& v : this->cparams()->unset_vars) {
                var_keys.push_back(v.key);
            }
            for (auto& v : this->cparams()->set_vars) {
                var_keys.push_back(v.key);
            }
            for (auto& v : this->cparams()->unset_funs) {
                var_keys.push_back(v.key);
            }
            for (auto& v : this->cparams()->set_funs) {
                var_keys.push_back(v.key);
            }
            auto bools_ptr = dynamic_cast<Bools_input*>(this->params().get());
            if (bools_ptr) {
                for (auto& v : bools_ptr->unset_preds) {
                    var_keys.push_back(v.key);
                }
                for (auto& v : bools_ptr->set_preds) {
                    var_keys.push_back(v.key);
                }
            }

            expect(vars.ckeys() == var_keys,
                   "Resulting Var keys do not match the input keys: "s
                   + to_string(vars.ckeys()) + "\n!=\n"
                   + to_string(var_keys));

            if (!this->should_throw()) {
                cout << "[ " << var_keys << "]{ " << vars << "}" << endl;
            }

            const auto eit_var_unset = begin(vars)+this->cparams()->unset_vars.size();
            expect(std::all_of(begin(vars), eit_var_unset,
                               [](auto& vptr){ return vptr->is_unset(); }),
                   "Vars without values are not unset.");
            const auto eit_var_set = eit_var_unset+this->cparams()->set_vars.size();
            expect(std::all_of(eit_var_unset, eit_var_set,
                               [](auto& vptr){ return vptr->is_set(); }),
                   "Vars with values are not set.");
            const auto eit_fun_unset = eit_var_set+this->cparams()->unset_funs.size();
            expect(std::all_of(eit_var_set, eit_fun_unset,
                               [](auto& vptr){ return vptr->is_unset(); }),
                   "Funs without values are not unset.");
            const auto eit_fun_set = eit_fun_unset+this->cparams()->set_funs.size();
            expect(std::all_of(eit_fun_unset, eit_fun_set,
                               [](auto& vptr){ return vptr->is_set(); }),
                   "Funs with values are not set.");
            if (bools_ptr) {
                const auto eit_pred_unset = eit_fun_set+bools_ptr->unset_preds.size();
                expect(std::all_of(eit_fun_set, eit_pred_unset,
                                   [](auto& vptr){ return vptr->is_unset(); }),
                       "Preds without values are not unset.");
                const auto eit_pred_set = eit_pred_unset+bools_ptr->set_preds.size();
                expect(std::all_of(eit_pred_unset, eit_pred_set,
                                   [](auto& vptr){ return vptr->is_set(); }),
                       "Preds with values are not set.");
            }

            const Sort val{};
            set_all(vars, val);
            expect(all_set(vars, val), "Vars are not set after setting.");
            unset_all(vars);
            expect(all_unset(vars), "Vars are not set after setting.");
        }
    };

    ////////////////////////////////////////////////////////////////

    struct Vars_eval_params {};

    template <typename Sort, typename ArgSort = Sort, Type typeV = Type{}, typename ConfT = conf::Tp<typeV>>
    struct Vars_eval_case : Inherit<Cons_case<Vars<Sort, ArgSort, typeV>,
                                              Vars_input_ptr<Sort, ArgSort, typeV, ConfT>,
                                              Values<Optional<Sort>>,
                                              Vars_eval_params>> {
        using Inherit = unsot::Inherit<Cons_case<Vars<Sort, ArgSort, typeV>,
                                                 Vars_input_ptr<Sort, ArgSort, typeV, ConfT>,
                                                 Values<Optional<Sort>>,
                                                 Vars_eval_params>>;

        using typename Inherit::Input;
        using typename Inherit::Output;
        using typename Inherit::Cons;

        using Vars_input = typename Input::element_type;

        using Value = typename Output::value_type;

        using Inherit::Inherit;

        bool condition(const Output& expected_, const Output& result_) const override
        {
            return apx_equal(expected_, result_);
        }

        Cons ccons() const override
        {
            return this->cinput()->cons_vars();
        }

        Output result() override
        {
            Vars vars(ccons());

            if (!this->should_throw()) {
                cout << "[ " << vars.ckeys() << "]";
                cout.flush();
            }

            try_eval_all(vars);

            if (!this->should_throw()) {
                cout << " { " << vars << "}" << endl;
            }

            return try_get_all(vars);
        }
    };

    ////////////////////////////////////////////////////////////////

    template <typename Sort>
    using Vars_output = Pair<Keys, Values<Optional<Sort>>>;

    template <typename Sort, typename ArgSort = Sort, Type typeV = Type{}, typename ConfT = conf::Tp<typeV>>
    struct Vars_connect_case : Inherit<Case<Vars<Sort, ArgSort, typeV, ConfT>&, Vars_output<Sort>>> {
        using Inherit = unsot::Inherit<Case<Vars<Sort, ArgSort, typeV, ConfT>&, Vars_output<Sort>>>;

        using typename Inherit::Input;
        using typename Inherit::Output;

        using Inherit::Inherit;

        bool condition(const Output& expected_, const Output& result_) const override
        {
            return expected_.first == result_.first
                && apx_equal(expected_.second, result_.second);
        }

        Output result() override
        {
            Input& vars = this->input();

            if (!this->should_throw()) {
                cout << "[ " << vars.ckeys() << "]";
                cout.flush();
            }

            try_eval_all(vars);

            if (!this->should_throw()) {
                cout << " { " << vars << "}" << endl;
            }

            return {to_keys(as_const(vars)), try_get_all(vars)};
        }
    };
}


////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////

int main(int, const char*[])
try {
    using namespace unsot;
    using namespace unsot::test;
    using namespace std;

    using Br = Bools_input<>;
    using Ba = Bools_input<Bool, Type::assignee>;
    using Baa = Bools_input<Bool, Type::auto_assignee>;
    using Rr = Vars_input<Real>;
    using Ra = Vars_input<Real, Real, Type::assignee>;
    using Raa = Vars_input<Real, Real, Type::auto_assignee>;
    using PR = Bools_input<Real>;
    using PRfc = Bools_input<Real, Type::regular, Full_conf>;

    ////////////////////////////////////////////////////////////////

    const String vars_msg = "constructing of Vars-s";
    Suite(vars_msg + "<Bool>").test<Vars_case<Bool>>({
        { new Br{{{"a"}} }                                                  },
        { new Br{{{"a"}, {"b"}} }                                           },
        { new Br{{{"a"}, {"b"}}, {{"x", true}} }                            },
        { new Br{{{"a"}, {"b"}}, {{"x", true}, {"y", false}} }              },
        { new Br{{{"a"}}, {}, {{"f", "- a"}} }                              },
        { new Br{{{"a"}}, {}, {{"f1", "log a"}, {"f2", "abs 2"}} }          },
        { new Br{{{"a"}}, {}, {{"f1", "log a"}, {"f2", "abs f1"}} }         },
        { new Br{{{"a"}}, {}, {{"f1", "+ a"}}, {{"f2", true, "not f1"}} }   },
        { new Br{{{"a"}}, {}, {{"f1", "+ a"}}, {}, {{"p1", "not f1"}}, {{"p2", false, "and p1"}} } },
        { new Vars_input<Bool>{{{"a"}}, {{"x", true}}, {{"f", "+ x"}} }     },
        { new Br{{}, {}, {}, {}, {{"p1", "= 0 0"}, {"p2", "= 0 (- 1 1)"}, {"p3", "= (+ 1 3) 0"}, {"p4", "= (* 5 9) (and 1 0)"}} } },
    });

    Suite(vars_msg + "<Bool>", true).test<Vars_case<Bool>>({
        { new Br{{{"a"}}, {}, {{"f"}} }                                     },
        { new Br{{{"a"}}, {}, {{"f", "+ f"}} }                              },
        { new Br{{}, {}, {}, {}, {{"p1", "+ 1 2"}} }                        },
    });

    Suite(vars_msg + "<Bool,, assignee>").test<Vars_case<Bool, Bool, Type::assignee>>({
        { new Ba{{{"x"}, {"y"}, {"z"}}, {}, {}, {}, {{"p1", "or x z"}, {"p2", "and y z"}, {"e1", "= x y"}, {"e2", "= y z"}, {"e3", "= z x"}} } },
        { new Ba{{{"x"}, {"z"}, {"w"}}, {{"y", true}}, {}, {}, {{"p1", "or x z"}, {"p2", "and y z"}, {"p3", "not w"}, {"e1", "= x y"}, {"e2", "= y z"}, {"e3", "= z w"}, {"e4", "= w x"}} } },
    });

    Suite(vars_msg + "<Bool,, auto_assignee>").test<Vars_case<Bool, Bool, Type::auto_assignee>>({
        { new Baa{{{"x"}, {"y"}, {"z"}}, {}, {}, {}, {{"p1", "or x z"}, {"p2", "and y z"}, {"e1", "= x y"}, {"e2", "= y z"}, {"e3", "= z x"}} } },
        { new Baa{{{"x"}, {"z"}, {"w"}}, {{"y", true}}, {}, {}, {{"p1", "or x z"}, {"p2", "and y z"}, {"p3", "not w"}, {"e1", "= x y"}, {"e2", "= y z"}, {"e3", "= z w"}, {"e4", "= w x"}} } },
    });

    Suite(vars_msg + "<Real>").test<Vars_case<Real>>({
        { new Rr{{}, {{"x", 1.5}, {"y", 0.5}}, {{"f", "+ x y"}} },          },
    });

    Suite(vars_msg + "<Real>", true).test<Vars_case<Real>>({
        { new Rr{{}, {{"x", 1.5}, {"y", 0.5}}, {{"f", "+ x w"}} },          },
    });

    Suite(vars_msg + "<Real,, assignee>").test<Vars_case<Real, Real, Type::assignee>>({
        { new Ra{{}, {{"x", 1.5}, {"y", 0.5}}, {{"f", "+ x y"}} },          },
    });

    Suite(vars_msg + "<Real,, auto_assignee>").test<Vars_case<Real, Real, Type::auto_assignee>>({
        { new Raa{{}, {{"x", 1.5}, {"y", 0.5}}, {{"f", "+ x y"}} },          },
    });

    Suite(vars_msg + "<Bool, Real>").test<Vars_case<Bool, Real>>({
        { new PR{{{"a"}}, {{"b", true}}, {}, {}, {{"p1", "< 2 1"}} }        },
        { new PR{{{"a"}}, {{"b", true}}, {}, {}, {{"p1", "= 2 1"}} }        },
    });

    Suite(vars_msg + "<Bool, Real>", true).test<Vars_case<Bool, Real>>({
        { new PR{{{"a"}}, {{"b", true}}, {{"f1", "+ 1"}}, {}, {{"p1", "< 2 1"}} } },
        { new PR{{{"a"}}, {{"b", true}}, {}, {}, {{"p1", "< a 1"}} }        },
        { new PR{{{"a"}}, {{"b", true}}, {}, {}, {{"p1", "= b 0"}} }        },
    });

    ////////////////////////////////////////////////////////////////

    const String vars_eval_msg = "evaluation of Vars-s";
    Suite(vars_eval_msg + "<Bool>").test<Vars_eval_case<Bool>>({
        { new Br{{{"a"}, {"b"}}, {{"x", true}, {"y", false}} },                    {{}, {}, true, false}             },
        { new Br{{{"a"}}, {{"x", false}}, {{"f1", "+ a"}}, {{"f2", true, "not f1"}} },  {{}, false, {}, true}        },
        { new Br{{}, {{"x", false}, {"y", true}}, {{"f1", "or x y"}, {"f2", "not f1"}, {"f3", "and x f1"}} },  {false, true, true, false, false} },
        { new Br{{{"?"}}, {{"x", true}, {"y", false}}, {{"f", "or x y"}} },        {{}, true, false, true},          },
        { new Br{{}, {{"x", false}, {"y", true}}, {{"f1", "or x y"}}, {}, {{"p1", "= x f1"}}, {{"p2", true, "= x y"}} },  {false, true, true, false, true} },
    });

    Suite(vars_eval_msg + "<Bool>", true).test<Vars_eval_case<Bool>>({
        { new Br{{{"a"}} },                                                        {false}                           },
        { new Br{{{"a"}} },                                                        {true}                            },
        { new Br{{}, {{"x", true}} },                                              {false}                           },
        { new Br{{}, {{"x", false}} },                                             {true}                            },
    });

    Suite(vars_eval_msg + "<Bool,, assignee>").test<Vars_eval_case<Bool, Bool, Type::assignee>>({
        { new Ba{{{"x"}}, {{"y", true}}, {{"f1", "or x y"}}, {}, {{"p1", "= x f1"}}, {{"p2", false, "= x y"}} },  {{}, true, {}, {}, false} },
        { new Ba{{{"x"}}, {{"y", true}}, {{"f1", "or 0 y"}}, {}, {{"p1", "= x f1"}}, {{"p2", false, "= x y"}} },  {true, true, true, true, false} },
        { new Ba{{{"x"}}, {{"y", true}}, {{"f1", "or x y"}}, {}, {{"p1", "= x f1"}}, {{"p2", true, "= x y"}} },  {{}, true, {}, {}, true} },
        { new Ba{{{"x"}}, {{"y", true}}, {{"f1", "or x y"}}, {}, {{"p1", "= x f1"}, {"p2", "= x y"}} },  {true, true, {}, {}, true} },
        { new Ba{{}, {{"x", true}, {"y", false}}, {{"f", "* 0.1 10"}}, {}, {{"p", "= 5.5 5"}} },  {true, false, false, true}     },
        { new Ba{{{"x"}, {"z"}, {"w"}}, {{"y", true}}, {}, {}, {{"p1", "or x z"}, {"p2", "and y z"}, {"p3", "not w"}, {"e1", "= x y"}, {"e2", "= y z"}, {"e3", "= z w"}, {"e4", "= w x"}} },  {true, true, true, true, {}, {}, {}, true, true, true, true} },
        { new Ba{{{"x"}, {"z"}, {"w"}}, {{"y", true}}, {}, {}, {{"e1", "= x (and y 0)"}, {"e2", "= y z"}, {"e3", "= z (not w)"}, {"e4", "= w x"}} }, {false, true, false, true, true, true, {}, true} },
    });

    Suite(vars_eval_msg + "<Bool,, auto_assignee>").test<Vars_eval_case<Bool, Bool, Type::auto_assignee>>({
        { new Baa{{{"x"}}, {{"y", true}}, {{"f1", "or x y"}}, {}, {{"p1", "= x f1"}}, {{"p2", false, "= x y"}} },  {{}, true, {}, {}, false} },
        { new Baa{{{"x"}}, {{"y", true}}, {{"f1", "or 0 y"}}, {}, {{"p1", "= x f1"}}, {{"p2", false, "= x y"}} },  {true, true, true, true, false} },
        { new Baa{{{"x"}}, {{"y", true}}, {{"f1", "or x y"}}, {}, {{"p1", "= x f1"}}, {{"p2", true, "= x y"}} },  {true, true, true, true, true} },
        { new Baa{{{"x"}}, {{"y", true}}, {{"f1", "or x y"}}, {}, {{"p1", "= x f1"}, {"p2", "= x y"}} },  {true, true, true, true, true} },
        { new Baa{{{"x"}, {"z"}, {"w"}}, {{"y", true}}, {}, {}, {{"p1", "or x z"}, {"p2", "and y z"}, {"p3", "not w"}, {"e1", "= x y"}, {"e2", "= y z"}, {"e3", "= z w"}, {"e4", "= w x"}} },  {true, true, true, true, true, true, false, true, true, true, true} },
        { new Baa{{{"x"}, {"z"}, {"w"}}, {{"y", true}}, {}, {}, {{"e1", "= x (and y 0)"}, {"e2", "= y z"}, {"e3", "= z (not w)"}, {"e4", "= w x"}} }, {false, true, false, true, true, true, true, true} },
    });

    Suite(vars_eval_msg + "<Real>").test<Vars_eval_case<Real>>({
        { new Rr{{{"?"}}, {{"x", 1.5}, {"y", 0.5}}, {{"f1", "* 0.1 10"}, {"f2", "/ x y"}}, },  {{}, 1.5, 0.5, 1, 3}    },
    });

    Suite(vars_eval_msg + "<Bool, Real>").test<Vars_eval_case<Bool, Real>>({
        { new PR{{{"?"}}, {{"x", true}, {"y", false}}, {}, {}, {{"p", "= 5.5 5"}} },  {{}, true, false, false}    },
    });

    ////////////////////////////////////////////////////////////////

    const String vars_connect_msg = "connection of Vars-s";

    Br ibools1{{}, {{"x", false}, {"y", true}}, {{"f1", "and x y"}, {"f2", "or x y"}}  };

    Ra ireals1{{{"?"}}, {{"x", 3.14}, {"y", 1.}}, {{"f1", "+ x y"}, {"f2", "- f1"}}  };
    Raa iauto_reals1{{{"?"}}, {{"x", 3.14}, {"y", 1.}}, {{"f1", "+ x y"}, {"f2", "- f1"}}  };

    PR ipreds1{{}, {{"z", true}}, {}, {}, {{"p1", ">= x y"}, {"p2", "= f1 (- ?)"}, {"p3", "= (^ f2 2) ?"}} };
    PRfc iauto_preds1{{}, {{"z", true}}, {}, {}, {{"p1", ">= x y"}, {"p2", "= f1 (- ?)"}, {"p3", "= (^ f2 2) ?"}} };

    auto bools1(ibools1.cons_vars());
    Vars<Bool, Real> preds1;

    static_assert(is_same_v<decltype(bools1)::Types::Var, decltype(preds1)::Types::Var>);

    Suite(vars_connect_msg + "<Bool>").test<Vars_connect_case<Bool>>({
        { bools1,     {{"x", "y", "f1", "f2"}, {false, true, false, true}}  },
    });

    Suite(vars_connect_msg + "<Bool>", true).test<Vars_connect_case<Bool>>({
        { bools1,     {{"x", "y", "f1"}, {false, true, false, true}}        },
        { bools1,     {{"x", "y", "f1", "f2"}, {false, true, false}}        },
    });

    ireals1.preds_base_l = &preds1;
    auto reals1(ireals1.cons_vars());

    static_assert(is_derived_from_v<decltype(reals1)::Types::Var, decltype(preds1)::Types::Pred::Arg_var>);
    static_assert(is_same_v<decltype(reals1)::Types::Var, decltype(preds1)::Types::Equality::Assignee>);

    ipreds1.rets_base_l = &bools1;
    ipreds1.args_base_l = &reals1;
    preds1 = ipreds1.cons_vars();

    Suite(vars_connect_msg + "<Real,, assignee>").test<Vars_connect_case<Real, Real, Type::assignee>>({
        { reals1,     {{"?", "x", "y", "f1", "f2"}, {{}, 3.14, 1., 4.14, -4.14}}, },
    });

    Suite(vars_connect_msg + "<Bool, Real>").test<Vars_connect_case<Bool, Real>>({
        { preds1,     {{"z", "p1", "p2", "p3"}, {true, true, {}, true}}     },
    });

    Suite(vars_connect_msg + "<Real,, assignee>").test<Vars_connect_case<Real, Real, Type::assignee>>({
        { reals1,     {{"?", "x", "y", "f1", "f2"}, {4.14*4.14, 3.14, 1., 4.14, -4.14}}, },
    });

    bools1 = ibools1.cons_vars();
    Vars<Bool, Real, Type::regular, Full_conf> auto_preds1;
    iauto_reals1.preds_base_l = &auto_preds1;
    auto auto_reals1(iauto_reals1.cons_vars());

    static_assert(is_derived_from_v<decltype(auto_reals1)::Types::Var, decltype(auto_preds1)::Types::Pred::Arg_var>);
    static_assert(is_derived_from_v<decltype(auto_reals1)::Types::Var, decltype(auto_preds1)::Types::Equality::Assignee>);
    //+ they are effectively the same, but `Types::Fun_mixin`s differ since they come from `Types` classes with different tp. args
    //+ static_assert(is_derived_from_v<decltype(auto_reals1)::Types::Var::Assigner, decltype(auto_preds1)::Types::Equality>);

    iauto_preds1.args_base_l = &auto_reals1;
    auto_preds1 = iauto_preds1.cons_vars();

    //+ this will fail `assert` in `cast` due to the same reasons as the static assert above
    if constexpr (!_debug_) {
        Suite(vars_connect_msg + "<Real,, auto_assignee>").test<Vars_connect_case<Real, Real, Type::auto_assignee>>({
            { auto_reals1,  {{"?", "x", "y", "f1", "f2"}, {4.14*4.14, 3.14, 1., 4.14, -4.14}}, },
        });

        auto_reals1.ptr("?")->unset();
        auto_preds1 = iauto_preds1.cons_vars();

        Suite(vars_connect_msg + "<Bool, Real> (auto)").test<Vars_connect_case<Bool, Real, Type::regular, Full_conf>>({
            { auto_preds1,  {{"z", "p1", "p2", "p3"}, {true, true, false, true}} },
        });

        Suite(vars_connect_msg + "<Real,, auto_assignee>").test<Vars_connect_case<Real, Real, Type::auto_assignee>>({
            { auto_reals1,  {{"?", "x", "y", "f1", "f2"}, {4.14*4.14, 3.14, 1., 4.14, -4.14}}, },
        });
    }

    ////////////////////////////////////////////////////////////////

    ////////////////////////////////////////////////////////////////
    ////////////////////////////////////////////////////////////////

    cout << endl << "Success." << endl;
    return 0;
}
catch (const unsot::Error& e) {
    std::cout << e << std::endl;
    throw;
}
