#include "test.hpp"
#include "ode/flow.hpp"
#include "ode/flow/parse.hpp"

namespace unsot::test {
    using namespace ode;
    using namespace ode::flow;

    constexpr double step = 5e-1;

    ////////////////////////////////////////////////////////////////

    struct State_config {
        const State& state;
        const Config& config{};
    };

    struct Eval_init_case : Inherit<Input_case<const Flow&, State_config>> {
        using Inherit::Inherit;

        void do_stuff() override
        {
            cinput().check_input(params().state, params().config);
        }
    };

    ////////////////////////////////////////////////////////////////

    struct Eval_case : Inherit<Case<const Flow&, State, State_config>> {
        using Inherit::Inherit;

        Output result() override
        {
            State state(params().state);
            cinput().check_input(params().state, params().config);
            cinput().eval_step(params().state, state, params().config);
            state.advance_t(params().state, step);
            return state;
        }
    };

    struct Linv_case : Inherit<Case<const Flow&, bool, State_config>> {
        using Inherit::Inherit;

        Output result() override
        {
            cinput().check_input(params().state, params().config);
            return cinput().local_invariants(params().state, params().config);
        }
    };

    struct Time_case : Inherit<Case<const Flow&, bool, State_config>> {
        using Inherit::Inherit;

        Output result() override
        {
            cinput().check_input(params().state, params().config);
            return cinput().timer(params().state, params().config);
        }
    };
}


////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////

int main(int, const char*[])
try {
    using namespace unsot;
    using namespace unsot::test;
    using namespace std;

    const Flow ball = Parse("(x v) (g)", "(= x' v) (= v' (- g))", "(>= x (- (+ v 1) (+ 1 v))) (<= v 0) (>= v 0)").perform();
    const Flow flow1 = Parse("(x y z) (a b)", "(= x' (+ x (- 2 1))) (= x' (* x _tau)) (= y' (- x _t)) (= z' (+ a b)) (= z' (- a b))", "(>= x 0) (<= x 0) (> y 0) (< y _tau) (< _tau z) (< _t 1) (> _t (- (* x y z) (* a b)))").perform();

    const double g = 9.7;
    const State ball_st0(0, Reals{5, 0}, Reals{g});
    const State ball_st1(step, Reals{0, -g}, Reals{g}, step);
    const State ball_st2(2*step, Reals{-g, -g}, Reals{g}, 2*step);
    auto ball_st_def_f = [g](int steps){
        return State(steps*step, Reals{0, 0}, Reals{g}, steps*step);
    };
    const Config ball_conf_0{{1, 1}, {}, {1, 1}};
    const Config ball_conf_1{{1, 1}, {}, {1, 2}};

    const State flow1_st0(0, Reals{1, 2, 3}, Reals{4, 8});
    const State flow1_st1_00(step, Reals{2, 1, 12}, Reals{4, 8}, step);
    const State flow1_st1_11(step, Reals{0, 1, -4}, Reals{4, 8}, step);
    const State flow1_st2_00(2*step, Reals{3, 2-step, 12}, Reals{4, 8}, 2*step);
    const State flow1_st2_11(2*step, Reals{0, -step, -4}, Reals{4, 8}, 2*step);
    const auto flow1_st_def_f = [](int steps){
        return State(steps*step, Reals{0, 0, 0}, Reals{4, 8}, steps*step);
    };
    const Config flow1_conf_00_00_0{{1, 1, 1}, {1, 1}, {1, 1, 1}};
    const Config flow1_conf_11_00_0{{2, 1, 2}, {1, 1}, {1, 1, 1}};
    const Config flow1_conf_00_11_0{{1, 1, 1}, {1, 1}, {2, 2, 1}};
    const Config flow1_conf_11_11_0{{2, 1, 2}, {1, 1}, {2, 2, 1}};
    const Config flow1_conf_00_00_1{{1, 1, 1}, {1, 2}, {1, 1, 1}};
    const Config flow1_conf_11_00_1{{2, 1, 2}, {1, 2}, {1, 1, 1}};
    const Config flow1_conf_00_11_1{{1, 1, 1}, {1, 2}, {2, 2, 1}};
    const Config flow1_conf_11_11_1{{2, 1, 2}, {1, 2}, {2, 2, 1}};

    ////////////////////////////////////////////////////////////////

    const String eval_init_msg = "initializing evaluation of Flow";
    Suite(eval_init_msg).test<Eval_init_case>({
        { ball,                    {ball_st0, ball_conf_0}                       },
        { ball,                    {ball_st0, ball_conf_1}                       },
        { ball,                    {ball_st0, {}}                                },
        { ball,                    {ball_st0}                                    },
        { ball,                    {ball_st0, {{1, 1}, {10, 50}, {1, 2}}}        },
        { ball,                    {ball_st0, Config::parse("(1 1) (1 1) (1 2)")}},
        { flow1,                   {flow1_st0, flow1_conf_00_00_0}               },
        { flow1,                   {flow1_st0, flow1_conf_11_00_0}               },
        { flow1,                   {flow1_st0, flow1_conf_00_11_0}               },
        { flow1,                   {flow1_st0, flow1_conf_11_11_0}               },
        { flow1,                   {flow1_st0, flow1_conf_00_00_1}               },
        { flow1,                   {flow1_st0, flow1_conf_11_00_1}               },
        { flow1,                   {flow1_st0, flow1_conf_00_11_1}               },
        { flow1,                   {flow1_st0, flow1_conf_11_11_1}               },
    });

    Suite(eval_init_msg, true).test<Eval_init_case>({
        { ball,                    {ball_st0, {{2, 1}, {1, 1}, {1, 1}}}          },
        { ball,                    {ball_st0, {{1, 2}, {1, 1}, {1, 1}}}          },
        { ball,                    {ball_st0, {{1, 1}, {1, 1}, {2, 1}}}          },
        { ball,                    {ball_st0, {{1, 1}, {1, 1}, {1, 3}}}          },
        { ball,                    {ball_st0, {{-1, 1}, {1, 1}, {1, 1}}}         },
        //+ { ball,                    {ball_st0, Config::parse("(0 0) () (0)")}     },
        { ball,                    {ball_st0, Config::parse("1 1 1")}            },
        { ball,                    {ball_st0, Config::parse("(1 1) (1 1) (-1 1)")}  },
        { flow1,                   {flow1_st0, {{2, 1, 2}, {1, 3}, {2, 2, 1}}}   },
        { flow1,                   {flow1_st0, {{2, 1, 2}, Ids{2}, {2, 2, 1}}}   },
        { ball,                    {State::parse("0 (5 0 0) (9.7)"), ball_conf_0}},
        { ball,                    {State::parse("0 (5) (9.7)"), ball_conf_0}    },
        { ball,                    {State::parse("0 (5 0)"), ball_conf_0}        },
        { ball,                    {State::parse("0 (5 0) (9.7 0)"), ball_conf_0}},
    });

    ////////////////////////////////////////////////////////////////

    Suite("evaluating single steps of Flow").test<Eval_case>({
        { ball,                    ball_st1,                                  {ball_st0, ball_conf_0}                        },
        { ball,                    ball_st_def_f(1),                          {ball_st0}                                     },
        { ball,                    ball_st1,                                  {ball_st0, ball_conf_0}                        },
        { ball,                    ball_st1,                                  {ball_st0, ball_conf_1}                        },
        { ball,                    ball_st1,                                  {ball_st0, Config::parse("(1 1) (1 1) (1 2)")} },
        { ball,                    ball_st2,                                  {ball_st1, ball_conf_0}                        },
        { ball,                    ball_st_def_f(2),                          {ball_st1}                                     },
        { ball,                    ball_st_def_f(3),                          {ball_st2}                                     },
        { flow1,                   flow1_st1_00,                              {flow1_st0, flow1_conf_00_00_0}                },
        { flow1,                   flow1_st_def_f(1),                         {flow1_st0}                                    },
        { flow1,                   flow1_st1_00,                              {flow1_st0, flow1_conf_00_11_0}                },
        { flow1,                   flow1_st1_00,                              {flow1_st0, flow1_conf_00_00_1}                },
        { flow1,                   flow1_st1_00,                              {flow1_st0, flow1_conf_00_11_1}                },
        { flow1,                   flow1_st2_00,                              {flow1_st1_00, flow1_conf_00_00_0}             },
        { flow1,                   flow1_st_def_f(2),                         {flow1_st1_00}                                 },
        { flow1,                   flow1_st2_00,                              {flow1_st1_00, flow1_conf_00_11_0}             },
        { flow1,                   flow1_st2_00,                              {flow1_st1_00, flow1_conf_00_00_1}             },
        { flow1,                   flow1_st2_00,                              {flow1_st1_00, flow1_conf_00_11_1}             },
        { flow1,                   flow1_st1_11,                              {flow1_st0, flow1_conf_11_00_0}                },
        { flow1,                   flow1_st1_11,                              {flow1_st0, Config::parse("2 1 2")}            },
        { flow1,                   flow1_st1_11,                              {flow1_st0, flow1_conf_11_11_0}                },
        { flow1,                   flow1_st1_11,                              {flow1_st0, flow1_conf_11_00_1}                },
        { flow1,                   flow1_st1_11,                              {flow1_st0, flow1_conf_11_11_1}                },
        { flow1,                   flow1_st2_11,                              {flow1_st1_11, flow1_conf_11_00_0}             },
        { flow1,                   flow1_st_def_f(2),                         {flow1_st1_11}                                 },
        { flow1,                   flow1_st2_11,                              {flow1_st1_11, flow1_conf_11_11_0}             },
        { flow1,                   flow1_st2_11,                              {flow1_st1_11, flow1_conf_11_00_1}             },
        { flow1,                   flow1_st2_11,                              {flow1_st1_11, flow1_conf_11_11_1}             },
        { flow1,                   flow1_st_def_f(3),                         {flow1_st2_00}                                 },
        { flow1,                   flow1_st_def_f(3),                         {flow1_st2_11}                                 },
    });

    Suite("local invariants of single steps of Flow").test<Linv_case>({
        { ball,                    true,                                      {ball_st0, ball_conf_0}                        },
        { ball,                    true,                                      {ball_st0, ball_conf_1}                        },
        { ball,                    true,                                      {ball_st1, ball_conf_0}                        },
        { ball,                    false,                                     {ball_st1, ball_conf_1}                        },
        { ball,                    false,                                     {ball_st2, ball_conf_0}                        },
        { ball,                    false,                                     {ball_st2, ball_conf_1}                        },
        { flow1,                   true,                                      {flow1_st0, flow1_conf_00_00_0}                },
        { flow1,                   true,                                      {flow1_st0, flow1_conf_11_00_0}                },
        { flow1,                   true,                                      {flow1_st0, flow1_conf_00_00_1}                },
        { flow1,                   true,                                      {flow1_st0, flow1_conf_11_00_1}                },
        { flow1,                   false,                                     {flow1_st0, flow1_conf_00_11_0}                },
        { flow1,                   false,                                     {flow1_st0, flow1_conf_11_11_0}                },
        { flow1,                   false,                                     {flow1_st0, flow1_conf_00_11_1}                },
        { flow1,                   false,                                     {flow1_st0, flow1_conf_11_11_1}                },
        { flow1,                   true,                                      {flow1_st1_00, flow1_conf_00_00_0}             },
        { flow1,                   true,                                      {flow1_st1_00, flow1_conf_11_00_0}             },
        { flow1,                   true,                                      {flow1_st1_00, flow1_conf_00_00_1}             },
        { flow1,                   true,                                      {flow1_st1_00, flow1_conf_11_00_1}             },
        { flow1,                   false,                                     {flow1_st1_00, flow1_conf_00_11_0}             },
        { flow1,                   false,                                     {flow1_st1_00, flow1_conf_11_11_0}             },
        { flow1,                   false,                                     {flow1_st1_00, flow1_conf_00_11_1}             },
        { flow1,                   false,                                     {flow1_st1_00, flow1_conf_11_11_1}             },
        { flow1,                   true,                                      {flow1_st1_11, flow1_conf_00_00_0}             },
        { flow1,                   true,                                      {flow1_st1_11, flow1_conf_11_00_0}             },
        { flow1,                   true,                                      {flow1_st1_11, flow1_conf_00_00_1}             },
        { flow1,                   true,                                      {flow1_st1_11, flow1_conf_11_00_1}             },
        { flow1,                   false,                                     {flow1_st1_11, flow1_conf_00_11_0}             },
        { flow1,                   false,                                     {flow1_st1_11, flow1_conf_11_11_0}             },
        { flow1,                   false,                                     {flow1_st1_11, flow1_conf_00_11_1}             },
        { flow1,                   false,                                     {flow1_st1_11, flow1_conf_11_11_1}             },
        { flow1,                   true,                                      {flow1_st2_00, flow1_conf_00_00_0}             },
        { flow1,                   true,                                      {flow1_st2_00, flow1_conf_11_00_0}             },
        { flow1,                   true,                                      {flow1_st2_00, flow1_conf_00_00_1}             },
        { flow1,                   true,                                      {flow1_st2_00, flow1_conf_11_00_1}             },
        { flow1,                   false,                                     {flow1_st2_00, flow1_conf_00_11_0}             },
        { flow1,                   false,                                     {flow1_st2_00, flow1_conf_11_11_0}             },
        { flow1,                   false,                                     {flow1_st2_00, flow1_conf_00_11_1}             },
        { flow1,                   false,                                     {flow1_st2_00, flow1_conf_11_11_1}             },
        { flow1,                   false,                                     {flow1_st2_11, flow1_conf_00_00_0}             },
        { flow1,                   false,                                     {flow1_st2_11, flow1_conf_11_00_0}             },
        { flow1,                   false,                                     {flow1_st2_11, flow1_conf_00_00_1}             },
        { flow1,                   false,                                     {flow1_st2_11, flow1_conf_11_00_1}             },
        { flow1,                   true,                                      {flow1_st2_11, flow1_conf_00_11_0}             },
        { flow1,                   true,                                      {flow1_st2_11, flow1_conf_11_11_0}             },
        { flow1,                   true,                                      {flow1_st2_11, flow1_conf_00_11_1}             },
        { flow1,                   true,                                      {flow1_st2_11, flow1_conf_11_11_1}             },
    });

    Suite("timer conditions of single steps of Flow").test<Time_case>({
        { ball,                    true,                                      {ball_st0, ball_conf_0}                        },
        { ball,                    true,                                      {ball_st0, ball_conf_1}                        },
        { ball,                    true,                                      {ball_st1, ball_conf_0}                        },
        { ball,                    true,                                      {ball_st1, ball_conf_1}                        },
        { ball,                    true,                                      {ball_st2, ball_conf_0}                        },
        { ball,                    true,                                      {ball_st2, ball_conf_1}                        },
        { flow1,                   true,                                      {flow1_st0, flow1_conf_00_00_0}                },
        { flow1,                   true,                                      {flow1_st0, flow1_conf_11_00_0}                },
        { flow1,                   true,                                      {flow1_st0, flow1_conf_00_11_0}                },
        { flow1,                   true,                                      {flow1_st0, flow1_conf_11_11_0}                },
        { flow1,                   true,                                      {flow1_st0, flow1_conf_00_00_1}                },
        { flow1,                   true,                                      {flow1_st0, flow1_conf_11_00_1}                },
        { flow1,                   true,                                      {flow1_st0, flow1_conf_00_11_1}                },
        { flow1,                   true,                                      {flow1_st0, flow1_conf_11_11_1}                },
        { flow1,                   true,                                      {flow1_st1_00, flow1_conf_00_00_0}             },
        { flow1,                   true,                                      {flow1_st1_00, flow1_conf_11_00_0}             },
        { flow1,                   true,                                      {flow1_st1_00, flow1_conf_00_11_0}             },
        { flow1,                   true,                                      {flow1_st1_00, flow1_conf_11_11_0}             },
        { flow1,                   true,                                      {flow1_st1_00, flow1_conf_00_00_1}             },
        { flow1,                   true,                                      {flow1_st1_00, flow1_conf_11_00_1}             },
        { flow1,                   true,                                      {flow1_st1_00, flow1_conf_00_11_1}             },
        { flow1,                   true,                                      {flow1_st1_00, flow1_conf_11_11_1}             },
        { flow1,                   false,                                     {flow1_st1_11, flow1_conf_00_00_0}             },
        { flow1,                   false,                                     {flow1_st1_11, flow1_conf_11_00_0}             },
        { flow1,                   false,                                     {flow1_st1_11, flow1_conf_00_11_0}             },
        { flow1,                   false,                                     {flow1_st1_11, flow1_conf_11_11_0}             },
        { flow1,                   false,                                     {flow1_st1_11, flow1_conf_00_00_1}             },
        { flow1,                   false,                                     {flow1_st1_11, flow1_conf_11_00_1}             },
        { flow1,                   false,                                     {flow1_st1_11, flow1_conf_00_11_1}             },
        { flow1,                   false,                                     {flow1_st1_11, flow1_conf_11_11_1}             },
        { flow1,                   false,                                     {flow1_st2_00, flow1_conf_00_00_0}             },
        { flow1,                   false,                                     {flow1_st2_00, flow1_conf_11_00_0}             },
        { flow1,                   false,                                     {flow1_st2_00, flow1_conf_00_11_0}             },
        { flow1,                   false,                                     {flow1_st2_00, flow1_conf_11_11_0}             },
        { flow1,                   false,                                     {flow1_st2_00, flow1_conf_00_00_1}             },
        { flow1,                   false,                                     {flow1_st2_00, flow1_conf_11_00_1}             },
        { flow1,                   false,                                     {flow1_st2_00, flow1_conf_00_11_1}             },
        { flow1,                   false,                                     {flow1_st2_00, flow1_conf_11_11_1}             },
        { flow1,                   false,                                     {flow1_st2_11, flow1_conf_00_00_0}             },
        { flow1,                   false,                                     {flow1_st2_11, flow1_conf_11_00_0}             },
        { flow1,                   false,                                     {flow1_st2_11, flow1_conf_00_11_0}             },
        { flow1,                   false,                                     {flow1_st2_11, flow1_conf_11_11_0}             },
        { flow1,                   false,                                     {flow1_st2_11, flow1_conf_00_00_1}             },
        { flow1,                   false,                                     {flow1_st2_11, flow1_conf_11_00_1}             },
        { flow1,                   false,                                     {flow1_st2_11, flow1_conf_00_11_1}             },
        { flow1,                   false,                                     {flow1_st2_11, flow1_conf_11_11_1}             },
    });

    ////////////////////////////////////////////////////////////////

    ////////////////////////////////////////////////////////////////
    ////////////////////////////////////////////////////////////////

    cout << endl << "Success." << endl;
    return 0;
}
catch (const unsot::Error& e) {
    std::cout << e << std::endl;
    throw;
}
