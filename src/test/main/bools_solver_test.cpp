#include "smt/solver_test.hpp"

#include "smt/solver.tpp"
#include "smt/solver/bools.tpp"

////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////

int main(int, const char*[])
try {
    using namespace unsot;
    using namespace unsot::test;
    using namespace unsot::smt::solver::test;
    using namespace std;
    using unsot::ignore;

    ////////////////////////////////////////////////////////////////

    const String bools_msg = "decl./def. and expansion and assertions of bool vars/funs or real preds";
    Suite(bools_msg).test<Bools_case<>>({
        { {{"a"}, {"b"}},                        {{"a"}, {"b"}},            },
        { {{"a", true, true}, {"b"}},            {{"a", true}, {"b"}},      },
        { {{"a", true, true}, {"b", true, false}}, {{"a", true}, {"b", false}}, },
        { {{"a"}, {"F", false, {}, true, "and a 1"}}, {{"a"}, {"F_1", {}, "and a 1"}}, },
        { {{"a", true, true}, {"F", true, false, true, "and a 1"}}, {{"a", true}, {"F_1", false, "and a 1"}}, },
        { {{"a", true, false}, {"F", true, false, true, "and a 1"}}, {{"a", false}, {"F_1", false, "and a 1"}}, },
        { {{"a", true, true}, {"b", true, false}, {"F", false, {}, true, "and b a"}}, {{"a", true}, {"b", false}, {"F_1", {}, "and b a"}}, },
        { {{"a", true, true}, {"b", true, true}, {"F", false, {}, true, "and b a"}}, {{"a", true}, {"b", true}, {"F_1", {}, "and b a"}}, },
        { {{"a", true, true}, {"b", true, false}, {"F", true, true, true, "=> a b"}}, {{"a", true}, {"b", false}, {"F_1", true, "=> a b"}}, },
        //+ { {{"F", true, true, true, "or 0 &a", true, {"&a"}, {"1"}}}, {{"F_1", true, "or 0 1"}}, },
        { {{"a"}, {"F", true, true, true, "or 0 &a", true, {"&a"}, {"a"}}}, {{"a"}, {"F_1", true, "or 0 a"}}, },
        //+ { {{"a"}, {"F", true, true, true, "or &b &a", true, {"&a", "&b"}, {"a", "1"}}}, {{"a"}, {"F_1", true, "or 1 a"}}, },
        { {{"a"}, {"F", true, true, true, "or &b &a", true, {"&a", "&b"}, {"a", "a"}}}, {{"a"}, {"F_1", true, "or a a"}}, },
        { {{"a"}, {"F", true, true, true, "or &b &a", true, {"&a", "&b", "&c"}, {"a", "a", "a"}}}, {{"a"}, {"F_1", true, "or a a"}}, },
    });

    Suite(bools_msg, true).test<Bools_case<>>({
        { {{"F", false, {}, true, "+ 1 1"}},     ignore,                    },
        { {{"F", false, {}, true, "or 1 2"}},    ignore,                    },
        { {{"F", false, {}, true, "or a 1"}},    ignore,                    },
        { {{"F", false, {}, true, "1"}},         ignore,                    },
        { {{"F", true, true, true, "or 0 &a", true, {"&a"}, {"?"}}}, ignore, },
        { {{"a"}, {"F", true, true, true, "or 0 &a", true, {"&a"}, {"a", "a"}}}, ignore, },
        { {{"a"}, {"F", true, true, true, "or &b &a", true, {"&a", "&b"}, {"a"}}}, ignore, },
        //+ { {{"a"}, {"F", true, true, true, "or &a &a", true, {"&a", "&a"}, {"a", "a"}}}, ignore, },
        { {{"a"}, {"F", true, true, true, "or &? &a", true, {"&a"}, {"a"}}}, ignore, },
    });

    ////////////////////////////////////////////////////////////////

    using S = Test_solver;

    const String inst_msg = "instantiation of plain expressions";
    Suite(inst_msg).test<Inst_expr_case<>>({
        { {{}, "and 0 1"},                   {invoke([]{ S s; s.instantiate_bool("and 0 1"); return s; })} },
        { {{}, "and 0 1"},                   {invoke([]{ S s; s.declare_bool_var("_F_1"); return s; })} },
        { {{"a"}, "and a 1"},                {invoke([]{ S s; s.declare_bool_var("a"); s.declare_bool_var("_F_1"); return s; })} },
        { {{"a"}, "and a 1"},                {invoke([]{ S s; s.declare_bool_var("a"); s.define_bool_fun("_F", "and a 1"); s.instantiate_def_bool_fun("_F"); return s; })} },
        { {{"a"}, "= a 1"},                  {invoke([]{ S s; s.declare_bool_var("a"); s.instantiate_bool("= a 1"); return s; })} },
        { {{"a"}, "= (= a 1) (= a 0)"},      {invoke([]{ S s; s.declare_bool_var("a"); s.instantiate_bool("= (= a 1) (= a 0)"); return s; })} },
        { {{"a"}, "= (= a 1) (= a 0)"},      {invoke([]{ S s; s.declare_bool_var("a"); s.declare_bool_var("_F_1"); return s; })} },
        { {{"a"}, "= (= a 1) a"},            {invoke([]{ S s; s.declare_bool_var("a"); s.declare_bool_var("_F_1"); return s; })} },
    });

    Suite(inst_msg, true).test<Inst_expr_case<>>({
        { {{}, ""},                          ignore            },
        { {{}, "a"},                         ignore            },
        { {{}, "a b"},                       ignore            },
        { {{"x"}, "x"},                      ignore            },
        { {{}, "+ 1 1"},                     ignore            },
        { {{}, "+ 1 2"},                     ignore            },
        { {{}, "+ x 2"},                     ignore            },
        { {{}, "= 1 2"},                     ignore            },
        { {{"x"}, "+ x 2"},                  ignore            },
        { {{}, "and 0 1"},                   {invoke([]{ S s; return s; })} },
        { {{"a"}, "= (= a 1) (= a 0)"},      {invoke([]{ S s; s.declare_bool_var("a"); s.instantiate_bool("= a 1"); s.instantiate_bool("= a 0"); s.declare_bool_var("_F_3"); return s; })} },
        { {{"a"}, "= (= a 1) a"},            {invoke([]{ S s; s.declare_bool_var("a"); s.instantiate_bool("= a 1"); s.declare_bool_var("_F_2"); return s; })} },
        { {{"x", "y"}, "+ x (> y 0)"},       ignore            },
    });

    ////////////////////////////////////////////////////////////////

    const String assert_msg = "assertion of plain expressions";
    Suite(assert_msg).test<Assert_expr_case<>>({
        { {{"a"}, "not a"},                  {invoke([]{ S s; s.define_bool_var("a", false); return s; })}    },
        { {{"a"}, "and a"},                  {invoke([]{ S s; s.define_bool_var("a", true); return s; })}     },
        { {{}, "and 0"},                     {invoke([]{ S s; s.assert_elem("0"); return s; })} },
        { {{}, "and 1"},                     {invoke([]{ S s; s.assert_elem("1"); return s; })} },
        { {{"a", "b"}, "and a b"},           {invoke([]{ S s; s.define_bool_var("a", true); s.define_bool_var("b", true); return s; })} },
        { {{"a", "b"}, "and a (not b)"},     {invoke([]{ S s; s.define_bool_var("a", true); s.define_bool_var("b", false); return s; })} },
        { {{"a", "b"}, "and (not a) b"},     {invoke([]{ S s; s.define_bool_var("a", false); s.define_bool_var("b", true); return s; })} },
        { {{"a", "b"}, "and (not a) (not b)"}, {invoke([]{ S s; s.define_bool_var("a", false); s.define_bool_var("b", false); return s; })} },
    });

    Suite(assert_msg, true).test<Assert_expr_case<>>({
        { {{"a"}, "not a"},                  {invoke([]{ S s; s.define_bool_var("a", true); return s; })}    },
        { {{"a"}, "and a"},                  {invoke([]{ S s; s.assert_elem("1"); return s; })}    },
    });

    ////////////////////////////////////////////////////////////////

    const String parse_msg = "parsing";
    Suite(parse_msg).test<Parse_case<>>({
        { "",                                {invoke([]{ S s; return s; })} },
        { "(declare-fun a () Bool)",         {invoke([]{ S s; s.declare_bool_var("a"); return s; })} },
        { "(declare-const a Bool)",          {invoke([]{ S s; s.declare_bool_var("a"); return s; })} },
        { "(define-fun a () Bool 1)",        {invoke([]{ S s; s.define_bool_var("a", true); return s; })} },
        { "(define-fun a () Bool 0)",        {invoke([]{ S s; s.define_bool_var("a", false); return s; })} },
        { "(define-fun F () Bool (or 1 0))", {invoke([]{ S s; s.define_bool_fun("F", "or 0 1"); return s; })} },
        { "(define-fun F ((&a Bool)) Bool (or &a))", {invoke([]{ S s; s.define_bool_fun("F", "or &a", {"&a"}); return s; })} },
        { "(define-fun F ((&a Bool)(&b Bool)) Bool (or &b &a))", {invoke([]{ S s; s.define_bool_fun("F", "or &b &a", {"&a", "&b"}); return s; })} },
        { "(assert false)",                  {invoke([]{ S s; s.assert_elem("0"); return s; })} },
        { "(assert true)",                   {invoke([]{ S s; s.assert_elem("1"); return s; })} },
        { "(declare-const a Bool)(assert a)", {invoke([]{ S s; s.define_bool_var("a", true); return s; })} },
        { "(declare-const a Bool)(declare-const b Bool)(assert (and a (not b)))", {invoke([]{ S s; s.define_bool_var("a", true); s.define_bool_var("b", false); return s; })} },
    });

    Suite(parse_msg, true).test<Parse_case<>>({
        { "()",                              ignore                         },
        { "(())",                            ignore                         },
        { "(declare)",                       ignore                         },
        { "(declare-const)",                 ignore                         },
        { "(declare-const Bool)",            ignore                         },
        { "(declare-const ())",              ignore                         },
        { "(declare-const a Real)",          ignore                         },
        { "(declare-const a Bool 1)",        ignore                         },
        { "(declare-const a () Bool)",       ignore                         },
        { "(declare-fun a Bool)",            ignore                         },
        { "(declare-fun a (Bool) Bool)",     ignore                         },
        { "(define-fun F () Bool &a)",       ignore                         },
        { "(define-fun F ((&a Bool)) Bool)", ignore                         },
        { "(define-fun F ((&a Bool)) Bool &a)", ignore                      },
        { "(define-fun F ((&a Boolx)) Bool (or &a))", ignore                },
        { "(define-fun F ((&a)) Bool (or &a))", ignore                      },
        { "(define-fun F (((&a Bool))) Bool (or &a))", ignore               },
        { "(define-fun F (()) Bool (or &a))", ignore                        },
        { "(define-fun F (&a) Bool (or &a))", ignore                        },
        { "(define-fun F (&a Bool) Bool (or &a))", ignore                   },
        { "(define-fun F ((&a Bool) &b) Bool (or &a))", ignore              },
        { "(define-fun F ((&a Bool) (&b)) Bool (or &a))", ignore            },
        { "declare-fun a () Bool",           ignore                         },
        { "declare-const a Bool",            ignore                         },
        { "a",                               ignore                         },
        { "(assert)",                        ignore                         },
        { "(assert ())",                     ignore                         },
        { "(assert (and))",                  ignore                         },
        { "(assert (a))",                    ignore                         },
        { "(assert true false)",             ignore                         },
        { "(assert a)",                      ignore                         },
        { "assert false",                    ignore                         },
        { "",                                {invoke([]{ S s; s.declare_bool_var("a"); return s; })} },
        { "(declare-fun a () Bool)",         {invoke([]{ S s; s.declare_bool_var("b"); return s; })} },
        { "(assert false)",                  {invoke([]{ S s; s.assert_elem("1"); return s; })} },
    });

    ////////////////////////////////////////////////////////////////

    ////////////////////////////////////////////////////////////////
    ////////////////////////////////////////////////////////////////

    cout << endl << "Success." << endl;
    return 0;
}
catch (const unsot::Error& e) {
    std::cout << e << std::endl;
    throw;
}
