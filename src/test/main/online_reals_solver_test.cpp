#include "smt/solver_test.hpp"
#include "smt/solver/online/reals/minisat.hpp"

////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////

int main(int, const char*[])
try {
    using namespace unsot;
    using namespace unsot::test;
    using namespace unsot::smt::solver::test;
    using namespace std;
    using online::reals::minisat::Solver;

    ////////////////////////////////////////////////////////////////

    Solver s;
    s.parse(Bounce::input);

    Sat sat = s.solve();
    cout << endl;
    s.print_vars();

    check_smt(s, sat);

    ////////////////////////////////////////////////////////////////

    cout << endl << endl;
    Solver s2;
    s2.parse(Branch::input);

    sat = s2.solve();
    cout << endl;
    s2.print_vars();

    check_smt<Branch>(s2, sat);

    ////////////////////////////////////////////////////////////////

    ////////////////////////////////////////////////////////////////
    ////////////////////////////////////////////////////////////////

    cout << endl << "Success." << endl;
    return 0;
}
catch (const unsot::Error& e) {
    std::cout << e << std::endl;
    throw;
}
