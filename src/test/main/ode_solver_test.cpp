#include "test.hpp"
#include "ode/solver.hpp"
#include "ode/solver/euler.hpp"
#include "ode/solver/odeint.hpp"
#include "ode/flow/parse.hpp"

#include "util/numeric/alg.hpp"

namespace unsot::test {
    using namespace ode;
    using namespace ode::solver;
    using namespace ode::flow;
    using numeric::apx_equal;
    using numeric::def_rel_eps;
    using numeric::def_abs_eps;

    using Traject = Base::Traject;

    ////////////////////////////////////////////////////////////////

    auto& def_dt = Base::def_dt;
    template <typename SolveInput>
    struct Solver_params {
        const SolveInput& solve_input;
        const double step{def_dt};
        const double rel_eps{def_rel_eps};
        const double abs_eps{def_abs_eps};
        Shared_ptr<const Traject> traject_ptr{};
    };

    template <typename S, typename SolveInput>
    struct Solve_case_base : Inherit<Cons_case<S, Flow, State, Solver_params<SolveInput>>> {
        using Inherit = unsot::Inherit<Cons_case<S, Flow, State, Solver_params<SolveInput>>>;

        using typename Inherit::Output;

        using Inherit::Inherit;

        bool condition(const Output& expected_, const Output& result_) const override
        {
            return apx_equal<State>(expected_, result_,
                                    this->cparams().rel_eps, this->cparams().abs_eps);
        }

        virtual State solve(S& solver)                                    = 0;

        Output result() override
        {
            S tmp(this->cons());
            S solver;
            solver = move(tmp);

            solver.dt() = this->cparams().step;

            State res(solve(solver));
            Traject traj = solver.ctraject();
            State res2(solve(solver));
            Traject traj2 = solver.ctraject();
            expect(res == res2,
                   "Two consecutive solutions differ: '"s
                   + res.to_string() + "' != '" + res2.to_string() + "'");
            this->params().traject_ptr = solver.ctraject_ptr();
            expect(traj == traj2 && traj == *this->params().traject_ptr,
                   "Two consecutive trajectories differ: '"s
                   + traj.to_string() + "' != '" + traj2.to_string() + "'");

            return res;
        }
    };

    struct State_config {
        const State& state;
        const Config& config;
    };

    template <typename S>
    struct Solve_case : Inherit<Solve_case_base<S, State_config>> {
        using Inherit = unsot::Inherit<Solve_case_base<S, State_config>>;

        using Inherit::Inherit;

        State solve(S& solver) override
        {
            return solver.solve(this->cparams().solve_input.state,
                                this->cparams().solve_input.config);
        }
    };

    template <typename S>
    struct Solve_parse_case : Inherit<Solve_case_base<S, List>> {
        using Inherit = unsot::Inherit<Solve_case_base<S, List>>;

        using Inherit::Inherit;

        State solve(S& solver) override
        {
            return solver.solve(this->cparams().solve_input);
        }
    };
}

////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////

int main(int, const char*[])
try {
    using namespace unsot;
    using namespace unsot::test;
    using namespace std;
    using unsot::ignore;
    using unsot::to_string;

    ////////////////////////////////////////////////////////////////

    const List ball_ls("( (x v) (g) ) ( (= x' v) (= v' (- g)) ) ( (>= x 0) (<= v 0) (>= v 0) )");
    const Flow ball = Parse(ball_ls).perform();

    const double g = 9.7;
    const double x0 = 5;
    const auto next_dt_down = [g](double t0_, double x0_){ return t0_ + sqrt((2.*x0_)/g); };
    const auto next_dv_down = [g](double tau_){ return -g*tau_; };
    const auto next_dt_up = [g](double t0_, double v0_){ return t0_ + v0_/g; };
    const auto next_dx_up = [g](double tau_, double v0_){ return v0_*tau_ - 0.5*g*tau_*tau_; };

    const List ball_st0_ls("0 ("s + to_string(x0) + " 0) (" + to_string(g) + ")");
    const State ball_st0 = State::parse(ball_st0_ls);
    const double t1 = next_dt_down(0, x0);
    const double v1 = next_dv_down(t1);
    const State ball_st1(t1, Reals{0, v1}, Reals{g}, t1);
    const double v1_ = -v1;
    const State ball_st1_(t1, Reals{0, v1_}, Reals{g});
    const double t2 = next_dt_up(t1, v1_);
    const double tau2 = t2-t1;
    const double x2 = next_dx_up(tau2, v1_);
    const State ball_st2(t2, Reals{x2, 0}, Reals{g}, tau2);
    const State ball_st2_(t2, Reals{x2, 0}, Reals{g});
    const double t3 = next_dt_down(t2, x2);
    const double tau3 = t3-t2;
    const double v3 = next_dv_down(tau3);
    const State ball_st3(t3, Reals{0, v3}, Reals{g}, tau3);
    const double v3_ = -v3;
    const State ball_st3_(t3, Reals{0, v3_}, Reals{g});
    const double t4 = next_dt_up(t3, v3_);
    const double tau4 = t4-t3;
    const double x4 = next_dx_up(tau4, v3_);
    const State ball_st4(t4, Reals{x4, 0}, Reals{g}, tau4);

    const List ball_conf_0_ls("(1 1) () (1 1)");
    const Config ball_conf_0 = Config::parse(ball_conf_0_ls);
    const Config ball_conf_1{{1, 1}, {}, {1, 2}};

    ////////////////////////////////////////////////////////////////

    const Flow sincos = Parse("x y", "(= x' (- y _t)) (= y' (- _t x))", "< _tau 10").perform();

    const auto next_x = [](double t_){ return t_ - sin(t_) + cos(t_) - 1; };
    const auto next_y = [](double t_){ return t_ - sin(t_) - cos(t_) + 1; };

    const double sc_t1 = 10;
    const State sincos_st0(0, Reals{0, 0});
    const State sincos_st1(sc_t1, Reals{next_x(sc_t1), next_y(sc_t1)}, Reals{}, sc_t1);

    const Config sincos_conf{{1, 1}, 1};

    ////////////////////////////////////////////////////////////////

    const auto P = [](auto&&... args){
        return Parse(FORWARD(args)...).perform();
    };

    const String solve_msg = "solving Flow with ";

    const String euler_msg = solve_msg + "Euler";
    Suite(euler_msg).test<Solve_case<Euler>>({
        { ball,                        ball_st1,                              {{ball_st0, ball_conf_0}, 1e-5, 1e-5, 2e-7}      },
        { ball,                        ball_st1,                              {{ball_st0, ball_conf_0}, 1e-4, 1e-4, 2e-6}      },
        { ball,                        ball_st1,                              {{ball_st0, ball_conf_0}, 1e-3, 1e-3, 2e-5}      },
        { ball,                        ball_st1,                              {{ball_st0, ball_conf_0}, 1e-2, 1e-2, 2e-4}      },
        { ball,                        ball_st1,                              {{ball_st0, ball_conf_0}, 1e-1, 1e-1, 2e-3}      },
        { ball,                        ball_st1,                              {{ball_st0, ball_conf_0}, 5e-3, 5e-3, 1e-5}  },
        { ball,                        ball_st2,                              {{ball_st1_, ball_conf_1}, 5e-3, 5e-3, 1e-5} },
        { ball,                        ball_st3,                              {{ball_st2_, ball_conf_0}, 5e-3, 5e-3, 1e-5} },
        { ball,                        ball_st4,                              {{ball_st3_, ball_conf_1}, 5e-3, 5e-3, 1e-5} },
        { P(ball_ls),                  ball_st1,                              {{ball_st0, ball_conf_0}, 5e-3, 5e-3, 1e-5}  },
        { P("x", "= x' (- 1)", "= _tau 1"),  State::parse("0 (100)"),         {{State::parse("0 (100)"), Config::parse("(1) 1")}}   },
        { P("x", "= x' (- 1)", "<= _tau 10"),  State::parse("10 (90) 10"),    {{State::parse("0 (100)"), Config::parse("(1) 1")}}  },
        { P("x", "= x' (- 1)", "< _tau 15"),  State::parse("20 (85) 15"),     {{State::parse("5 (100)"), Config::parse("(1) 1")}, 1} },
        { P("x", "= x' (- (* 2 x) 100)", "< _tau 10"),  {10, {2.425e10}, Reals{}, 10}, {{State::parse("0 (100)"), Config::parse("(1) 1")}, 5e-3, 1e-1}       },
        { P("x", "(= x' (- (/ x 2) (/ _t 3))) (= x' (- (/ _t 3) x))", "< _tau 50"),  {50, {10}, Reals{}, 50}, {{State::parse("0 (10)"), Config::parse("(0) 1")}, 1e-3, 1e-2} },
        { P("x", "(= x' (- (/ x 2) (/ _t 3))) (= x' (- (/ _t 3) x))", "< _tau 50"),  {50, {6.24e11}, Reals{}, 50}, {{State::parse("0 (10)"), Config::parse("(1) 1")}, 1e-3, 1e-2} },
        { P("x", "(= x' (- (/ x 2) (/ _t 3))) (= x' (- (/ _t 3) x))", "< _tau 50"),  {50, {16.3267}, Reals{}, 50}, {{State::parse("0 (10)"), Config::parse("(2) 1")}, 1e-3, 1e-2}   },
        { P("x y", "(= x' (- 1)) (= y' 1)", "< _tau 20"), State::parse("20 (80 40) 20"), {{State::parse("0 (100 20)"), Config::parse("(1 1) 1")}}  },
        { sincos,                      sincos_st1,                            {{sincos_st0, sincos_conf}, 5e-3, 5e-2}  },
    });

    const String odeint_msg = solve_msg + "Odeint";
    Suite(odeint_msg).test<Solve_case<Odeint>>({
        { ball,                        ball_st1,                              {{ball_st0, ball_conf_0}, 1e-3, 1e-5, 2e-7}      },
        { ball,                        ball_st1,                              {{ball_st0, ball_conf_0}, 1e-2, 1e-4, 2e-6}      },
        { ball,                        ball_st1,                              {{ball_st0, ball_conf_0}, 1e-1, 1e-3, 2e-5}      },
        { ball,                        ball_st1,                              {{ball_st0, ball_conf_0}}  },
        { ball,                        ball_st2,                              {{ball_st1_, ball_conf_1}} },
        { ball,                        ball_st3,                              {{ball_st2_, ball_conf_0}} },
        { ball,                        ball_st4,                              {{ball_st3_, ball_conf_1}} },
        { P(ball_ls),                  ball_st1,                              {{ball_st0, ball_conf_0}}  },
        { P("x", "= x' (- 1)", "= _tau 1"),  State::parse("0 (100)"),         {{State::parse("0 (100)"), Config::parse("(1) 1")}}          },
        { P("x", "= x' (- 1)", "<= _tau 10"),  State::parse("10 (90) 10"),    {{State::parse("0 (100)"), Config::parse("(1) 1")}}         },
        { P("x", "= x' (- 1)", "< _tau 15"),  State::parse("20 (85) 15"),     {{State::parse("5 (100)"), Config::parse("(1) 1")}, 1}      },
        { P("x", "= x' (- (* 2 x) 100)", "< _tau 10"),  {10, {2.425e10}, Reals{}, 10}, {{State::parse("0 (100)"), Config::parse("(1) 1")},  def_dt, 1e-3}       },
        { P("x", "(= x' (- (/ x 2) (/ _t 3))) (= x' (- (/ _t 3) x))", "< _tau 50"),  {50, {10}, Reals{}, 50}, {{State::parse("0 (10)"), Config::parse("(0) 1")}, 1e-3, 1e-2} },
        { P("x", "(= x' (- (/ x 2) (/ _t 3))) (= x' (- (/ _t 3) x))", "< _tau 50"),  {50, {6.24e11}, Reals{}, 50}, {{State::parse("0 (10)"), Config::parse("(1) 1")}, 1e-2, 1e-3} },
        { P("x", "(= x' (- (/ x 2) (/ _t 3))) (= x' (- (/ _t 3) x))", "< _tau 50"),  {50, {16.3267}, Reals{}, 50}, {{State::parse("0 (10)"), Config::parse("(2) 1")}, 1e-2, 1e-3}   },
        { P("x y", "(= x' (- 1)) (= y' 1)", "< _tau 20"), State::parse("20 (80 40) 20"), {{State::parse("0 (100 20)"), Config::parse("(1 1) 1")}}                                     },
        { sincos,                      sincos_st1,                            {{sincos_st0, sincos_conf}, 5e-3, 1e-3}                       },
    });

    Suite(euler_msg, true).test<Solve_case<Euler>>({
        { ball,                        ball_st1,                              {{ball_st0, ball_conf_0}}                            },
        { ball,                        ball_st2,                              {{ball_st1, ball_conf_1}, def_dt, def_dt, 1e-5}  },
        { ball,                        ball_st2_,                             {{ball_st1_, ball_conf_1}, def_dt, def_dt, 1e-5} },
        { ball,                        ball_st2,                              {{ball_st1_, ball_conf_0}, def_dt, def_dt, 1e-5} },
    });

    const String euler_parse_msg = euler_msg + " from List";
    Suite(euler_parse_msg).test<Solve_parse_case<Euler>>({
        { ball,                        ball_st1,                              {List{ball_st0_ls.to_ptr(), ball_conf_0_ls.to_ptr()}, 1e-3, 1e-3, 2e-5}                    },
        { ball,                        ball_st1,                              {ball_st0_ls.to_string()+ball_conf_0_ls.to_string(), 1e-3, 1e-3, 2e-5} },
        { P("(p gam) (om r)", "(= p' (* (- r) gam)) (= gam' 0) (= gam' (- om)) (= gam' om)", "< _tau 1"),  State::parse("1 (-10 3) (2 5) 1"),  {"(0 (0.0 1.0) (2.0 5.0)) ((1 3) 1)", def_dt, 1e-2}   },
    });

    Suite(euler_parse_msg, true).test<Solve_parse_case<Euler>>({
        { ball,                        ignore,                                {"", 1e-3, 1e-3, 2e-5}                               },
        { ball,                        ignore,                                {ball_st0_ls.to_string() + "(0)", 1e-3, 1e-3, 2e-5} },
        { ball,                        ignore,                                {ball_st0_ls.to_string() + "(0 2)", 1e-3, 1e-3, 2e-5} },
    });

    ////////////////////////////////////////////////////////////////

    ////////////////////////////////////////////////////////////////
    ////////////////////////////////////////////////////////////////

    cout << endl << "Success." << endl;
    return 0;
}
catch (const unsot::Error& e) {
    std::cout << e << std::endl;
    throw;
}
