#include "test.hpp"
#include "expr/fun_arith.hpp"

#include "util/numeric.hpp"
#include "util/scope_guard.hpp"

namespace unsot::test {
    using namespace expr;
    using namespace expr::fun;

    ////////////////////////////////////////////////////////////////

    struct Base_params {
        Keys keys{};
    };

    String to_string(const Base_params& rhs)
    {
        return to_string(rhs.keys);
    }

    struct Base_case : Inherit<Cons_case<List, String, String, Base_params>> {
        using Inherit::Inherit;

        Output result() override
        {
            using unsot::to_string;

            Base fun = empty(cparams().keys)
                     ? Base::cons(ccons(), new_keys())
                     : Base::cons(ccons(), new_keys(cparams().keys));
            /// To also check move assignment
            Base tmp(fun);
            Base fun2;
            fun2 = move(tmp);
            Base* fun_l = new Base(fun);

            /// Destroy the original `fun`
            tmp = fun;
            fun = Base::cons(ccons(), new_keys());
            fun = move(tmp);

            auto sg = Scope_guard([&fun_l]{ delete fun_l; });

            Vector<Base> funs;
            funs.emplace_back(move(fun2));
            funs.emplace_back(*fun_l);
            funs.emplace_back(Base::cons(fun.coper(), fun,
                                         fun.ckeys_ptr(), fun.cids_map_ptr()));
            Formula phi = Formula::cons(ccons());
            funs.emplace_back(Base::cons(phi.coper(), phi,
                                         fun.ckeys_ptr(), fun.cids_map_ptr()));
            funs.emplace_back(Base::cons(phi, fun.ckeys_ptr(), fun.cids_map_ptr()));
            //+ this should be forbidden
            //+ Base fun_args = Base::cons(phi);
            Base fun_args(phi);
            fun_args.virtual_init_with_args(fun.ckeys_ptr(), fun.cids_map_ptr());
            funs.emplace_back(move(fun_args));
            Base fun_ls_args(ccons());
            fun_ls_args.virtual_init_with_args(fun.ckeys_ptr(), fun.cids_map_ptr());
            funs.emplace_back(move(fun_ls_args));

            Vector<String> msgs = {
                "copy of itself"s,
                "dynamic copy of itself"s,
                "Base constructed with oper and Base"s,
                "Base constructed with oper and Formula"s,
                "Base constructed with Formula wo/ oper"s,
                "Base constructed with shared pointers provided after construction"s,
                "Base constructed with List and with shared pointers provided after construction"s,
            };

            if (!should_throw()) {
                assert(size(funs) == size(msgs));
                for_each(funs, cbegin(msgs), [&fun](auto& f, auto& msg){
                    expect(fun == f,
                           "Fun differs from "s + msg + ": "
                           + to_string(fun) + " != " + to_string(f));
                });

                cout << cinput() << " -> " << fun << endl;
            }

            return fun.to_string();
        }
    };

    ////////////////////////////////////////////////////////////////

    using Key_ids = Vector<Key_id>;
    using All_key_ids = Vector<Key_ids>;

    Key_ids to_key_ids(const Base::Key_ids& kids)
    {
        auto vec = Key_ids(cbegin(kids), cend(kids));
        sort(vec);
        return vec;
    }

    struct Key_ids_case : Inherit<Cons_case<List, String, All_key_ids, Base_params>> {
        using Inherit = unsot::Inherit<Cons_case<List, String, All_key_ids, Base_params>>;

        using typename Inherit::Output;

        using Inherit::Inherit;

        Output result() override
        {
            Base fun = Base::cons(ccons(), new_keys(cparams().keys));

            Output all_kids;
            all_kids.push_back(to_key_ids(fun.ckey_ids()));
            for (Idx idx = 0; idx < fun.size(); ++idx)
                all_kids.push_back(to_key_ids(fun.cpart_key_ids(idx)));
            return all_kids;
        }
    };

    /////////////////////////////////////////////////////////////////

    template <typename FunT, typename Values = typename FunT::Values>
    struct Fun_params : Base_params {
        Values values{};
        bool quiet{false};
    };

    template <typename FunT, typename Values>
    String to_string(const Fun_params<FunT, Values>& rhs)
    {
        return to_string(static_cast<const Base_params&>(rhs));
             + " " + to_string(rhs.values);
    }

    template <typename FunT, typename Values = typename FunT::Values>
    struct Fun_case
        : Inherit<Cons_case<List, String, typename FunT::Value, Fun_params<FunT, Values>>> {
        using Inherit = unsot::Inherit<Cons_case<List, String, typename FunT::Value, Fun_params<FunT, Values>>>;

        using typename Inherit::Output;
        using Value = typename FunT::Value;
        static_assert(is_same_v<Output, Value>);

        using Inherit::Inherit;

        void print_eval_res(const FunT& fun, Value res)
        {
            if (this->cparams().quiet) return;
            this->params().quiet = true;
            cout << fun << endl << "  =? " << res << endl;
        }

        Output result() override
        {
            using unsot::to_string;

            List ls(this->ccons());
            auto fun = FunT::cons(ls, new_keys(this->cparams().keys));
            auto fun_base = Base::cons(ls, new_keys(this->cparams().keys));
            auto phi = Formula::cons(ls);
            /// To also check move assignment
            FunT tmp(fun);
            FunT fun2;
            fun2 = move(tmp);
            FunT* fun_l = new FunT(fun);

            /// Destroy the original `fun`
            tmp = fun;
            fun = FunT::cons(ls, new_keys());
            fun = move(tmp);

            auto sg = Scope_guard([&fun_l]{ delete fun_l; });

            //!? Assignment from init. list will cause "valueless_by_exception" error ...
            Vector<FunT> funs;
            funs.emplace_back(move(fun2));
            funs.emplace_back(*fun_l);
            funs.emplace_back(FunT::cons(ls, fun.ckeys_ptr()));
            funs.emplace_back(FunT::cons(ls, fun.ckeys_ptr(), fun.cvalues_ptr()));
            funs.emplace_back(FunT::cons(fun.coper(), fun, fun.ckeys_ptr(), fun.cvalues_ptr()));
            funs.emplace_back(FunT::cons(fun_base.coper(), fun_base, fun.ckeys_ptr(), fun.cvalues_ptr()));
            funs.emplace_back(FunT::cons(phi.coper(), phi, fun.ckeys_ptr(), fun.cvalues_ptr()));
            funs.emplace_back(FunT::cons(fun, fun.ckeys_ptr(), fun.cvalues_ptr()));
            funs.emplace_back(FunT::cons(fun_base, fun.ckeys_ptr(), fun.cvalues_ptr()));
            funs.emplace_back(FunT::cons(phi, fun.ckeys_ptr(), fun.cvalues_ptr()));
            //+ this should be forbidden
            //+ funs.emplace_back(FunT::cons(phi));
            FunT fun_args(phi);
            fun_args.virtual_init_with_args(fun.ckeys_ptr(), fun.cids_map_ptr());
            funs.emplace_back(move(fun_args));
            fun_args = phi;
            fun_args.virtual_init_with_args(fun.ckeys_ptr(), fun.cvalues_ptr(), fun.cids_map_ptr());
            funs.emplace_back(move(fun_args));
            FunT fun_ls_args(this->ccons());
            fun_ls_args.virtual_init_with_args(fun.ckeys_ptr(), fun.cids_map_ptr());
            funs.emplace_back(move(fun_ls_args));
            fun_ls_args = this->ccons();
            fun_ls_args.virtual_init_with_args(fun.ckeys_ptr(), fun.cvalues_ptr(), fun.cids_map_ptr());
            funs.emplace_back(move(fun_ls_args));

            Vector<String> msgs = {
                "copy of itself"s,
                "dynamic copy of itself"s,
                "copy of keys pointer"s,
                "copy of both keys and values pointers"s,
                "Fun constructed with oper and Fun"s,
                "Fun constructed with oper and Base"s,
                "Fun constructed with oper and Formula"s,
                "Fun constructed with Fun wo/ oper"s,
                "Fun constructed with Base wo/ oper"s,
                "Fun constructed with Formula wo/ oper"s,
                "Fun constructed with shared key pointers provided after construction"s,
                "Fun constructed with all shared pointers provided after construction"s,
                "Fun constructed with List and with shared key pointers provided after construction"s,
                "Fun constructed with List and with all shared pointers provided after construction"s,
            };

            Value res = FunT::ret_to_value(fun(this->cparams().values));
            Value res2 = FunT::ret_to_value(fun(this->cparams().values));

            Vector<Value> ress = to<Vector<Value>>(funs, [this](auto& f){
                return FunT::ret_to_value(f(this->cparams().values));
            });
            Vector<Value> res2s = to<Vector<Value>>(funs, [this](const auto& f){
                return FunT::ret_to_value(f(this->cparams().values));
            });

            if (!this->should_throw()) {
                assert(size(funs) == size(msgs));
                for_each(funs, cbegin(msgs), [&fun](auto& f, auto& msg){
                    expect(fun == f,
                           "Fun differs from "s + msg + ": "
                           + to_string(fun) + " != " + to_string(f));
                });
                for_each(ress, cbegin(msgs), [res](auto r, auto& msg){
                    expect(res == r,
                           "Result differ with "s + msg + ": "
                           + to_string(res) + " != " + to_string(r));
                });
                expect(res == res2,
                       "Results of two consecutive evaluations differ: "s
                       + to_string(res) + " != " + to_string(res2));
                for_each(res2s, cbegin(msgs), [res](auto r, auto& msg){
                    expect(res == r,
                           "Results of two consecutive evaluations differ at "s + msg + ": "
                           + to_string(res) + " != " + to_string(r));
                });

                this->print_eval_res(fun, res);
            }

            return res;
        }
    };
}

////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////

int main(int, const char*[])
try {
    using namespace unsot;
    using namespace unsot::test;
    using namespace std;
    using unsot::ignore;

    ////////////////////////////////////////////////////////////////

    const String formula_msg = "constructing of Bases and Formulas";
    Suite(formula_msg).test<Base_case>({
        {"+ 1",                                     "( + 1 )",                        {}                                             },
        {"+ 1 2 3",                                 "( + 1 2 3 )",                    {}                                             },
        {"a 1 2 3",                                 "( a 1 2 3 )",                    {}                                             },
        {"+ 1 (* 2 3)",                             "( + 1 ( * 2 3 ) )",              {}                                             },
        {"+ a",                                     "( + a )[ a ]",                   {}                                             },
        {"+ a",                                     "( + a )[ a ]",                   {{"a"}}                                        },
        {"+ a 1 b",                                 "( + a 1 b )[ a b ]",             {}                                             },
        {"+ a 1 b",                                 "( + a 1 b )[ a b ]",             {{"a"}}                                        },
        {"+ a 1 b",                                 "( + a 1 b )[ b a ]",             {{"b"}}                                        },
        {"+ a 1 b",                                 "( + a 1 b )[ a b ]",             {{"a", "b"}}                                   },
        {"+ a 1 b",                                 "( + a 1 b )[ b a ]",             {{"b", "a"}}                                   },
        {"+ x (* 2 a)",                             "( + x ( * 2 a ) )[ x a ]",       {}                                             },
        {"+ x (* 2 (- a))",                         "( + x ( * 2 ( - a ) ) )[ x a ]", {}                                             },
        {"+ x (* 2 (- a))",                         "( + x ( * 2 ( - a ) ) )[ x a ]", {{"x"}}                                        },
        {"+ x (* 2 (- a))",                         "( + x ( * 2 ( - a ) ) )[ a x ]", {{"a"}}                                        },
        {"+ x (* 2 (- a))",                         "( + x ( * 2 ( - a ) ) )[ x a ]", {{"x", "a"}}                                   },
        {"+ x (* 2 (- a))",                         "( + x ( * 2 ( - a ) ) )[ a x ]", {{"a", "x"}}                                   },
    });

    Suite(formula_msg, true).test<Base_case>({
        {"",                                        ignore,                                                                          },
        {"()",                                      ignore,                                                                          },
        {"-",                                       ignore                                                                           },
        {"+",                                       ignore                                                                           },
        {"fun",                                     ignore                                                                           },
        {"1",                                       ignore                                                                           },
        {"1 2",                                     ignore                                                                           },
        {"1 2 3",                                   ignore                                                                           },
        {"()",                                      ignore                                                                           },
        {"(+)",                                     ignore                                                                           },
        {"(+ 1)",                                   ignore                                                                           },
        {"+ ()",                                    ignore                                                                           },
        {"+ (1)",                                   ignore                                                                           },
        {"+ (a)",                                   ignore                                                                           },
        {"+ (+)",                                   ignore                                                                           },
    });

    ////////////////////////////////////////////////////////////////

    const String key_ids_msg = "stored argument ids";
    Suite(key_ids_msg).test<Key_ids_case>({
        { "+ 1 2",                              {{}, {}, {}},                         {},                                                           },
        { "+ 1 2",                              {{}, {}, {}},                         {{"x", "y"}},                                                 },
        { "+ 1 2 3",                            {{}, {}, {}, {}},                     {},                                                           },
        { "+ x 2",                              {{0}, {0}, {}},                       {},                                                           },
        { "+ x 2",                              {{0}, {0}, {}},                       {{"x", "y"}},                                                 },
        { "+ y 2",                              {{1}, {1}, {}},                       {{"x", "y"}},                                                 },
        { "+ x y",                              {{0, 1}, {0}, {1}},                   {},                                                           },
        { "+ x y",                              {{0, 1}, {0}, {1}},                   {{"x"}},                                                      },
        { "+ x y",                              {{0, 1}, {0}, {1}},                   {{"x", "y"}},                                                 },
        { "+ y x",                              {{0, 1}, {0}, {1}},                   {},                                                           },
        { "+ y x",                              {{0, 1}, {1}, {0}},                   {{"x"}},                                                      },
        { "+ y x",                              {{0, 1}, {1}, {0}},                   {{"x", "y"}},                                                 },
        { "+ x y z",                            {{0, 1, 2}, {0}, {1}, {2}},           {{"x", "y"}},                                                 },
        { "* (+ x 2) (- y 3)",                  {{0, 1}, {0}, {1}},                   {},                                                           },
        { "* (+ x 2) (- y 3)",                  {{0, 1}, {0}, {1}},                   {{"x", "y"}},                                                 },
        { "* (+ y 2) (- x 3)",                  {{0, 1}, {1}, {0}},                   {{"x", "y"}},                                                 },
        { "* (+ x 2) y (- z 3)",                {{0, 1, 2}, {0}, {1}, {2}},           {{"x", "y"}},                                                 },
        { "* (+ x (* x 2)) (- (* y 3) y )",     {{0, 1}, {0}, {1}},                   {},                                                           },
        { "* (+ x (* x 2)) (- (* y 3) y )",     {{0, 1}, {0}, {1}},                   {{"x", "y"}},                                                 },
        { "* (+ x (* z 2)) (- (* y 3) y )",     {{0, 1, 2}, {0, 1}, {2}},             {},                                                           },
        { "* (+ x (* z 2)) (- (* y 3) y )",     {{0, 1, 2}, {0, 2}, {1}},             {{"x", "y"}},                                                 },
        { "* (+ x (* z 2)) (- (* y 3) y )",     {{0, 1, 2}, {0, 2}, {1}},             {{"x", "y", "z"}},                                            },
    });

    Suite(key_ids_msg, true).test<Key_ids_case>({
        { "+ 1 2",                              All_key_ids{},                        {},                                                           },
        { "+ 1 2",                              {{}},                                 {},                                                           },
        { "+ 1 2",                              {{}, {}},                             {},                                                           },
        { "+ 1 2",                              {{0}, {0}, {0}},                      {},                                                           },
        { "+ x 2",                              {{}, {}, {}},                         {},                                                           },
        { "+ x 2",                              {{1}, {1}, {}},                       {},                                                           },
        { "+ x 2",                              {{0}, {0}, {0}},                      {},                                                           },
        { "+ y x",                              {{0}, {0}, {0}},                      {},                                                           },
        { "+ y x",                              {{0}, {0}, {1}},                      {},                                                           },
        { "+ y x",                              {{1, 2}, {2}, {1}},                   {{"x", "y"}},                                                 },
        { "+ y x",                              {{0, 2}, {2}, {0}},                   {{"x", "y"}},                                                 },
        { "* (+ x (* z 2)) (- (* y 3) y )",     {{0, 1}, {0}, {1}},                   {{"x", "y"}},                                                 },
        { "* (+ x (* z 2)) (- (* y 3) y )",     {{0, 1, 2}, {0}, {1}},                {{"x", "y"}},                                                 },
        { "* (+ x (* z 2)) (- (* y 3) y )",     {{0, 1}, {0, 2}, {1}},                {{"x", "y"}},                                                 },
    });

    ////////////////////////////////////////////////////////////////

    constexpr Real x = 10;

    const String eval_msg = "construction and evaluation of Funs :: ";
    const String eval_real_msg = eval_msg + "<Real>";
    Suite::Data<Fun_case<Fun_tp<Real>>> eval_data_real = {
        {"+ 1",                                                      1,                                  {},                                       },
        {"+ 1 2",                                                    1+2,                                {},                                       },
        {"+ 1 2 3",                                                  1+2+3,                              {},                                       },
        {"+ 1 2 3 4",                                                1+2+3+4,                            {},                                       },
        {"+ 1 (+ 2 3)",                                              1+2+3,                              {},                                       },
        {"+ 1 (+ 2 (+ 3 4))",                                        1+2+3+4,                            {},                                       },
        {"+ 10 x",                                                   10+x,                               {{}, {x}},                                },
        {"+ 10 (+ x x)",                                             10+x+x,                             {{}, {x}},                                },
        {"+ x y",                                                    13+17,                              {{}, {13, 17}},                           },
        {"+ x ( - 10 y)",                                            100+(10-50),                        {{}, {100, 50}},                          },
        {"+ x (- (* (/ 3 t) y) 2)",                                  50+(3./10)*20-2,                    {{}, {50, 10, 20}},                       },
        {"+ x (- (* (/ 3 t) y) 2)",                                  1+(3./2)*3-2,                       {{}, {1, 2, 3}},                          },
        {"+ x (- (* (/ 3 t) y) 2)",                                  x+(3./50)*20-2,                     {{}, {x, 50, 20}},                        },
        {"+ x (- (* (/ 3 t) y) 2)",                                  50+(3./10)*20-2,                    {{{"t", "x", "y"}}, {10, 50, 20}},        },
        {"- 0 (* y (* 5 (/ x (/ 3 2))))",                            -5*5*(60./(3./2)),                  {{{"x", "y"}}, {60, 5}},                  },
        {"+ 1 2",                                                    1+2,                                {{{"t"}}, {1}},                           },
        {"+ 1 x",                                                    1+5,                                {{{"t"}}, {1, 5}},                        },
        {"^ (exp 1) 2",                                              exp(1)*exp(1),                      {},                                       },
        {"exp (ln 4)",                                               4,                                  {},                                       },
        {"min (max 0 4.5) (max 2 5 1 0) 100",                        4.5,                                {},                                       },
        {"ite (< 0 2) -1 1",                                         (0 < 2) ? -1 : 1,                   {},                                       },
        {"ite (< a b) a b",                                          min(1, 2),                          {{{"a", "b"}}, {1, 2}},                   },
        {"ite (< a b) a b",                                          min(2, 1),                          {{{"a", "b"}}, {2, 1}},                   },
        {"ite 0.5 -1 1",                                             bool(0.5) ? -1 : 1,                 {},                                       },
        {"+ 1e5",                                                    1e5,                                {},                                       },
        {"= 1 1.5",                                                  0,                                  {},                                       },
        {"and 0.5",                                                  1,                                  {},                                       },
        {"not 0.5",                                                  0,                                  {},                                       },
        {"and 0.5 1",                                                1,                                  {},                                       },
        {"and 0.5 0",                                                0,                                  {},                                       },
        {"xor 1 1.5",                                                0,                                  {},                                       },
        {"and (> (- x 1) 5) (< (+ x 1) 10)",                         1,                                  {{{"x"}}, {7}},                           },
        {"- (/ x 2) (/ _t 3)",                                       x/2.-1./3.,                         {{{"x", "_t"}}, {x, 1}},                  },
    };
    Suite(eval_real_msg).test<Fun_case<Fun_tp<Real>>>(eval_data_real);

    //+ Suite::Data<Fun_case<Fun_tp<Real>, initializer_list<Real>>> eval_data_real_list = {
    //+     {"+ x ( - 10 y)",                                            100+(10-50),                        {{}, {100, 50}, true},                    },
    //+     {"(+ x (- (* (/ 3 t) y) 2))",                                50+(3./10)*20-2,                    {{"t", "x", "y"}, {10, 50, 20}, true},    },
    //+     {"(+ x (- (* (/ 3 t) y) 2))",                                50+(3./10)*20-2,                    {{"t", "x", "y"}, {10, 50, 20}},    },
    //+ };
    //+ Suite(eval_real_msg).test<Fun_case<Fun_tp<Real>, initializer_list<Real>>>(eval_data_real_list);

    Suite(eval_real_msg, true).test<Fun_case<Fun_tp<Real>>>({
        {"+ 1 2",                              ignore,                               {{}, {1}},                                                    },
        {"+ 1 2",                              ignore,                               {{{"x"}}, {1, 2}},                                            },
        {"+ 1 x",                              ignore,                               {},                                                           },
        {"+ 1 x",                              ignore,                               {{{"y"}}, {1, 2, 3}},                                         },
        {"+ 1 x",                              ignore,                               {{{"y"}}, {1, 2, 3}},                                         },
        {"/ 1",                                ignore,                               {},                                                           },
        {"/ 1 2 3",                            ignore,                               {},                                                           },
        {"ite 1",                              ignore,                               {},                                                           },
        {"ite 1 2",                            ignore,                               {},                                                           },
        {"% 1 2",                              ignore,                               {},                                                           },
        {"> (% 1 2) 5.5",                      ignore,                               {},                                                           },
        {"+",                                  ignore,                               {},                                                           },
        {"1",                                  ignore,                               {},                                                           },
        {"(+) 1 1",                            ignore,                               {},                                                           },
        {"(+ 1 1) 1 1",                        ignore,                               {},                                                           },
        {"1 1 1",                              ignore,                               {},                                                           },
        {"# 1 1",                              ignore,                               {},                                                           },
        {"+ 1 (# 1 1)",                        ignore,                               {},                                                           },
        {"- (/ x 2) (/ _t 3)",                 4.,                                   {{{"x", "_t"}}, {x, 1}},                                      },
        {"^ 2 2 2",                            ignore,                               {},                                                           },
    });

    const String eval_real_fun_msg = eval_msg + "Fun<Real>";
    Suite(eval_real_fun_msg).test<Fun_case<Fun<Real>>>({
        {"+ 1 2",                                                    1+2,                                {},                                       },
        {"+ 10 x",                                                   10+x,                               {{}, {x}},                                },
        {"^ (exp 1) 2",                                              exp(1)*exp(1),                      {},                                       },
        {"ite (< 0 2) -1 1",                                         (0 < 2) ? -1 : 1,                   {},                                       },
        {"+ 1e5",                                                    1e5,                                {},                                       },
        {"- (/ x 2) (/ _t 3)",                                       x/2.-1./3.,                         {{{"x", "_t"}}, {x, 1}},                  },
        {"- (/ (+ x 1) (- x 2)) (/ (- _t 1) (+ _t 3))",              (x+1.)/(x-2.) - (-1./3.),           {{{"x", "_t"}}, {10, 0}},                 },
    });

    Suite(eval_real_fun_msg, true).test<Fun_case<Fun<Real>>>({
        {"= 1 1.5",                            ignore,                               {},                                                           },
        {"= 1 1",                              ignore,                               {},                                                           },
        {"< 1 1",                              ignore,                               {},                                                           },
        {"and 0.5",                            ignore,                               {},                                                           },
        {"and 1 1",                            ignore,                               {},                                                           },
        {"% 1 2",                              ignore,                               {},                                                           },
        {"and (> (- x 1) 5) (< (+ x 1) 10)",   ignore,                               {{{"x"}}, {7}},                                               },
        {"^ 2 2 2",                            ignore,                               {},                                                           },
        //+ {"+ 1 (= 2 1)",                        ignore,                               {},                                                           },
    });

    const String eval_real_pred_msg = eval_msg + "Pred<Real>";
    Suite(eval_real_pred_msg).test<Fun_case<Pred<Real>>>({
        {"= 1 1.5",                                                  false,          {},                                                           },
        {"and 0.5",                                                  true,           {},                                                           },
        {"and (> (- x 1) 5) (< (+ x 1) 10)",                         true,           {{{"x"}}, {7}},                                               },
        {"and (and (< x 1) (> x 0))",                                false,          {{{"x"}}, {7}},                                               },
        {"> (/ x 2) (/ _t 3)",                                       true,           {{{"x", "_t"}}, {x, 1}},                                      },
    });

    Suite(eval_real_pred_msg, true).test<Fun_case<Pred<Real>>>({
        {"+ 1 2",                              ignore,                               {},                                                           },
        {"+ 10 x",                             ignore,                               {},                                                           },
        {"% 1 2",                              ignore,                               {},                                                           },
        {"> (% 1 2) 5.5",                      ignore,                               {},                                                           },
        {"^ (exp 1) 2",                        ignore,                               {},                                                           },
        {"ite (< 0 2) -1 1",                   ignore,                               {},                                                           },
        {"+ 1e5",                              ignore,                               {},                                                           },
        //+ {"and (+ 1 2) (- 3 2)",                ignore,                               {},                                                           },
    });

    const String eval_int_msg = eval_msg + "<Int>";
    Suite::Data<Fun_case<Fun_tp<Int>>> eval_data_int = {
        {"+ 1",                                                      1,                                  {},                                       },
        {"+ 1 2.5",                                                  1+2,                                {},                                       },
        {"+ 1 2.5 3",                                                1+2+3,                              {},                                       },
        {"+ 1 2.5 3 4",                                              1+2+3+4,                            {},                                       },
        {"+ x (- (* (/ 3 t) y) 2)",                                  1+(3/2)*3-2,                        {{}, {1, 2, 3}},                          },
        {"+ x (- (* (/ 3 t) y) 2)",                                  x+(3/50)*20-2,                      {{}, {Int(x), 50, 20}},                   },
        {"+ 1 x",                                                    1+5,                                {{{"t"}}, {1, 5}},                        },
        {"^ 1 2",                                                    1,                                  {},                                       },
        {"min (max 0 4.5) (max 2 5 1 0) 100",                        4,                                  {},                                       },
        {"and 0.5",                                                  0,                                  {},                                       },
        {"not 0.5",                                                  1,                                  {},                                       },
        {"and 0.5 1",                                                0,                                  {},                                       },
        {"and 0.5 0",                                                0,                                  {},                                       },
        {"xor 1 1.5",                                                0,                                  {},                                       },
        {"% 7 4",                                                    3,                                  {},                                       },
        {"> (% 7 4) 5",                                              0,                                  {},                                       },
    };
    Suite(eval_int_msg).test<Fun_case<Fun_tp<Int>>>(eval_data_int);

    Suite(eval_int_msg, true).test<Fun_case<Fun_tp<Int>>>({
        {"/ 1",                                ignore,                               {},                                                           },
        {"/ 1 2 3",                            ignore,                               {},                                                           },
        {"ite 1",                              ignore,                               {},                                                           },
        {"ite 1 2",                            ignore,                               {},                                                           },
        {"# 1 1",                              ignore,                               {},                                                           },
        {"+ 1 (# 1 1)",                        ignore,                               {},                                                           },
        {"- (/ x 2) (/ _t 3)",                 400,                                  {{{"x", "_t"}}, {Int(x), 1}},                                 },
        {"exp 4",                              ignore,                               {},                                                           },
        {"> (exp 4) 0",                        ignore,                               {},                                                           },
        {"ln 4",                               ignore,                               {},                                                           },
        {"^ 2 2 2",                            ignore,                               {},                                                           },
    });

    const String eval_int_fun_msg = eval_msg + "Fun<Int>";
    Suite(eval_int_fun_msg).test<Fun_case<Fun<Int>>>({
        {"+ 1 2",                                                    1+2,                                {},                                       },
        {"+ 10 x",                                                   10+x,                               {{}, {Int(x)}},                           },
        {"* 4 2.5",                                                  8,                                  {},                                       },
        {"/ 4.9 2.5",                                                2,                                  {},                                       },
        {"% 4.9 2.5",                                                0,                                  {},                                       },
    });

    Suite(eval_int_fun_msg, true).test<Fun_case<Fun<Int>>>({
        //+ {"= 1 1.5",                            ignore,                               {},                                                           },
        //+ {"= 1 1",                              ignore,                               {},                                                           },
        {"< 1 1",                              ignore,                               {},                                                           },
        //+ {"and 0.5",                            ignore,                               {},                                                           },
        //+ {"and 1 1",                            ignore,                               {},                                                           },
    });

    const String eval_int_pred_msg = eval_msg + "Pred<Int>";
    Suite(eval_int_pred_msg).test<Fun_case<Pred<Int>>>({
        {"= 1 1.5",                                                  true,           {},                                                           },
        {"and 0.5",                                                  false,          {},                                                           },
        {"= (% 7 3) 1",                                              true,           {},                                                           },
    });

    Suite(eval_int_pred_msg, true).test<Fun_case<Pred<Int>>>({
        {"+ 1",                                ignore,                               {},                                                           },
        {"+ 1 2",                              ignore,                               {},                                                           },
        {"<~ 1 2",                             ignore,                               {},                                                           },
        {"% 1 2",                              ignore,                               {},                                                           },
        {"exp 1",                              ignore,                               {},                                                           },
        {"> (exp 1) 0",                        ignore,                               {},                                                           },
    });

    ////////////////////////////////////////////////////////////////

    ////////////////////////////////////////////////////////////////
    ////////////////////////////////////////////////////////////////

    cout << endl << "Success." << endl;
    return 0;
}
catch (const unsot::Error& e) {
    std::cout << e << std::endl;
    throw;
}
