#include "smt/solver_test.hpp"
#include "smt/solver/reals.hpp"

#include "smt/solver.tpp"
#include "smt/solver/bools.tpp"
#include "smt/solver/reals.tpp"

namespace unsot::smt::solver::test {
    template <typename S, var::Type typeV>
    struct Test_reals_solver_crtp : Inherit<reals::Mixin<Test_solver_crtp<S>, typeV>, Test_reals_solver_crtp<S, typeV>> {
        using Inherit = unsot::Inherit<reals::Mixin<Test_solver_crtp<S>, typeV>, Test_reals_solver_crtp<S, typeV>>;

        using typename Inherit::This;

        using Inherit::Inherit;

        String to_string() const& override
        {
            return this->creals().to_string() + " & " + Inherit::to_string();
        }

        bool equals(const This& rhs) const noexcept
        {
            return this->creals() == rhs.creals()
                && Inherit::equals(rhs);
        }
    };

    struct Test_reals_solver : Test_reals_solver_crtp<Test_reals_solver, var::Type::assignee> {};
    struct Test_auto_reals_solver : Test_reals_solver_crtp<Test_auto_reals_solver, var::Type::auto_assignee> {};

    ////////////////////////////////////////////////////////////////

    template <typename S, typename InputsT, typename OutputsT>
    struct Reals_case_tp : Inherit<Vars_case<S, InputsT, OutputsT>> {
        using Inherit = unsot::Inherit<Vars_case<S, InputsT, OutputsT>>;

        using typename Inherit::Input;
        using typename Inherit::Output;

        using typename Inherit::Single_input;
        using typename Inherit::Value;

        static constexpr bool is_preds = is_bool_v<Value>;

        static_assert(is_preds || is_real_v<Value>);

        using Inherit::Inherit;

        static bool is_real_var(const Single_input& in, const Key& key) noexcept
        {
            if constexpr (!is_preds) return true;
            return !in.has_formula && key[0] >= 'r';
        }
        static bool is_real_var(const Single_input& in) noexcept
        {
            return is_real_var(in, in.key);
        }

        static bool is_real_formula(const Single_input& in, const Key& key) noexcept
        {
            if constexpr (!is_preds) return true;
            return in.has_formula && islower(key[0]);
        }
        static bool is_real_formula(const Single_input& in) noexcept
        {
            return is_real_formula(in, in.key);
        }

        bool contains_real_condition(const Key& key) const noexcept
        {
            auto& s = *this->sptr;
            return s.contains(key) && s.contains_real(key) && s.creals().contains(key);
        }

        bool contains_condition(const Single_input& in, const Key& key) const noexcept override
        {
            if (is_real_var(in, key))
                return contains_real_condition(key)
                    && !this->contains_bool_condition(key);

            return this->contains_bool_condition(key)
                && !contains_real_condition(key);
        }

        bool formula_contains_real_condition(const Single_input& in, const Key& key) const noexcept
        {
            auto& s = *this->sptr;
            if constexpr (is_preds) {
                if (!s.contains_real_pred(key) || !s.contains_bool(key)) return false;
            }
            else if (!s.contains_real_fun(key) || !s.contains_real(key)) return false;

            if (s.contains_bool_var(key) || s.contains_bool_fun(key) || s.contains_real_var(key)) return false;
            if constexpr (is_preds) {
                if (s.contains_real(key)) return false;
            }
            else if (s.contains_bool(key)) return false;

            if (s.contains(in.key) || s.contains_real(in.key)
                || s.contains_real_pred(in.key) || s.creal_preds().contains(in.key)
                || s.contains_real_fun(in.key) || s.creals().contains(in.key)
                || s.contains_var(in.key) || s.contains_bool_fun(in.key))
                return false;

            return true;
        }

        bool formula_contains_condition(const Single_input& in, const Key& key) const noexcept override
        {
            if (is_real_formula(in, key))
                return formula_contains_real_condition(in, key)
                    && !this->formula_contains_bool_condition(in, key);

            return this->formula_contains_bool_condition(in, key)
                && !formula_contains_real_condition(in, key);
        }

        bool var_contains_real_condition(const Single_input& in, const Key& key) const noexcept
        {
            auto& s = *this->sptr;
            if (!s.contains_var(key) || !s.contains_real_var(key))
                return false;
            if (!s.contains(in.key) || !s.contains_real(in.key) || !s.creals().contains(in.key)
                || !s.contains_var(in.key) || !s.contains_real_var(in.key))
                return false;
            if (s.contains_bool(key) || s.contains_bool_var(key)
                || s.contains_real_fun(key) || s.contains_real_pred(key))
                return false;

            return true;
        }

        bool var_contains_condition(const Single_input& in, const Key& key) const noexcept override
        {
            if (is_real_var(in, key))
                return var_contains_real_condition(in, key)
                    && !this->var_contains_bool_condition(in, key);

            return this->var_contains_bool_condition(in, key)
                && !var_contains_real_condition(in, key);
        }

        const var::Ptr& get_ptr(const Single_input& in, const Key& key) const override
        {
            if (!is_real_var(in, key)) return Inherit::get_ptr(in, key);

            auto& s = *this->sptr;
            s.check_contains(key);
            s.check_contains_real(key);
            solver::check_contains_free_arg_keys<Real>(s, Formula::cons("and", List(key)));

            return s.creals().cptr(key);
        }

        void add_var(const Single_input& in) override
        {
            if (!is_real_var(in)) return Inherit::add_var(in);
            auto& s = *this->sptr;
            if (in.has_value) s.define_real_var(in.key, in.value);
            else s.declare_real_var(in.key);
        }

        void add_fun(Key& key, Formula& phi, const Single_input& in) override
        {
            if (!is_real_formula(in)) return Inherit::add_fun(key, phi, in);
            auto& s = *this->sptr;
            if constexpr (is_preds) {
                s.define_real_pred(in.key, in.formula, in.arg_keys);
                key = s.instantiate_def_real_pred(in.key, in.arg_values);
                phi = s.expand_def_real_pred(in.key, in.arg_values);
                s.assert_def_real_pred(in.key, in.arg_values);
                s.assert_bool("not "s + key);
                s.assert_elem(key);
            }
            else {
                s.define_real_fun(in.key, in.formula, in.arg_keys);
                key = s.instantiate_def_real_fun(in.key, in.arg_values);
                phi = s.expand_def_real_fun(in.key, in.arg_values);
            }
            expect(phi.initialized(),
                   "Expanded formula is not initialized: "s + phi.to_string());
        }
    };

    template <typename S>
    using Reals_case = Reals_case_tp<S, Reals_inputs, Reals_outputs>;
    template <typename S>
    using Real_preds_case = Reals_case_tp<S, Bools_inputs, Bools_outputs>;

    template <typename S>
    Suite::Data<Reals_case<S>> reals_valid_data()
    {
        return {
            { {{"x"}, {"y"}},                        {{"x"}, {"y"}},            },
            { {{"x", true, 1.}, {"y"}},              {{"x", 1.}, {"y"}},        },
            { {{"x", true, 1.}, {"y", true, -2.5}},  {{"x", 1.}, {"y", -2.5}},  },
            { {{"x"}, {"f", false, {}, true, "+ 1 2"}}, {{"x"}, {"f_1", {}, "+ 1 2"}}, },
            { {{"x", true, 2.}, {"f", true, 1., true, "+ 1 2"}}, {{"x", 2.}, {"f_1", 1., "+ 1 2"}}, },
            { {{"x", true, 1.}, {"y", true, 2.}, {"f", false, {}, true, "+ y x"}}, {{"x", 1.}, {"y", 2.}, {"f_1", {}, "+ y x"}}, },
            //+ { {{"x", true, 1.}, {"y", true, 2.}, {"f", false, {}, true, "+ y &z", true, {"&z"}, {"3"}}}, {{"x", 1.}, {"y", 2.}, {"f_1", {}, "+ y 3"}}, },
            //+ { {{"x", true, 1.}, {"y", true, 2.}, {"f", false, {}, true, "+ y &z", true, {"&z", "&x"}, {"3", "inf"}}}, {{"x", 1.}, {"y", 2.}, {"f_1", {}, "+ y 3"}}, },
            { {{"x", true, 1.}, {"y", true, 2.}, {"f", false, {}, true, "+ &y &x", true, {"&y", "&x"}, {"y", "x"}}}, {{"x", 1.}, {"y", 2.}, {"f_1", {}, "+ y x"}}, },
            { {{"x", true, 1.}, {"y", true, 2.}, {"f", false, {}, true, "+ y x &z", true, {"&z"}, {"y"}}}, {{"x", 1.}, {"y", 2.}, {"f_1", {}, "+ y x y"}}, },
            //+ { {{"x", true, 1.}, {"y", true, 2.}, {"f", false, {}, true, "+ y x &z", true, {"&z"}, {"3"}}}, {{"x", 1.}, {"y", 2.}, {"f_1", {}, "+ y x 3"}}, },
        };
    }

    template <typename S>
    Suite::Data<Reals_case<S>> reals_invalid_data()
    {
        return {
            { {{"x", true, 1.}, {"y"}},              {{"x", 2.}, {"y"}},        },
            { {{"x"}, {"f", false, {}, true, "+ x y"}},          ignore         },
            { {{"x", true, 2.}, {"f", true, 1., true, "+ 1 2"}}, {{"x", 2.}, {"f_1", 1.5, "+ 1 2"}}, },
            { {{"x", true, 2.}, {"f", false, {}, true, "+ 1 2"}}, {{"x", 2.}, {"f_1", {}, "+ 1 x"}}, },
            { {{"x", true, 1.}, {"y", true, 2.}, {"f", false, {}, true, "+ y &z", true, {"&z"}, {"x", "y"}}}, ignore, },
            { {{"x", true, 1.}, {"y", true, 2.}, {"f", false, {}, true, "+ y &z", true, {"&z", "&x"}, {"y"}}}, ignore, },
            //+ { {{"x", true, 1.}, {"y", true, 2.}, {"f", false, {}, true, "+ y &z", true, {"&z"}, {"3", "2"}}}, {{"x", 1.}, {"y", 2.}, {"f_1", 5., "+ y 3"}}, },
            //+ { {{"x", true, 1.}, {"y", true, 2.}, {"f", false, {}, true, "+ y &z", true, {"&z", "&x"}, {"3"}}}, {{"x", 1.}, {"y", 2.}, {"f_1", 5., "+ y 3"}}, },
        };
    }

    template <typename S>
    Suite::Data<Real_preds_case<S>> real_preds_valid_data()
    {
        return {
            { {{"a"}, {"b"}},                        {{"a"}, {"b"}},            },
            { {{"a", true, true}, {"b", true, false}}, {{"a", true}, {"b", false}}, },
            { {{"a"}, {"p", false, {}, true, "and 1 2"}}, {{"a"}, {"p_1", {}, "and 1 2"}}, },
            { {{"x", true, true}, {"y", true, false}, {"p", false, {}, true, "and y x"}}, {{"x", true}, {"y", false}, {"p_1", {}, "and y x"}}, },
            { {{"a", true, true}, {"x", true, true}, {"p", false, {}, true, "and 0 x"}}, {{"a", true}, {"x", true}, {"p_1", {}, "and 0 x"}}, },
            { {{"a", true, true}, {"x", true, true}, {"p", false, {}, true, "> x &y", true, {"&y"}, {"x"}}}, {{"a", true}, {"x", true}, {"p_1", {}, "> x x"}}, },
            //+ { {{"a", true, true}, {"x", true, true}, {"p", false, {}, true, "> x &y", true, {"&y"}, {"0"}}}, {{"a", true}, {"x", true}, {"p_1", {}, "> x 0"}}, },
            //+ { {{"p", false, {}, true, "= &x &y", true, {"&x", "&y"}, {"0", "0"}}}, {{"p_1", {}, "= 0 0"}}, },
            //+ { {{"p", false, {}, true, "= &x &y", true, {"&x", "&y"}, {"1", "0"}}}, {{"p_1", {}, "= 1 0"}}, },
        };
    }

    template <typename S>
    Suite::Data<Real_preds_case<S>> real_preds_invalid_data()
    {
        return {
            { {{"a"}, {"p", false, {}, true, "and a 0"}},               ignore  },
            { {{"x"}, {"F", false, {}, true, "and x 0"}},               ignore  },
            { {{"a"}, {"b"}, {"p", false, {}, true, "> a b"}},          ignore, },
            { {{"a"}, {"p", false, {}, true, "and 1 2"}}, {{"a"}, {"p_1", {}, "and 2 1"}}, },
        };
    }

    ////////////////////////////////////////////////////////////////

    struct Expr_reals_input : Inherit<Expr_bools_input> {
        Keys rkeys;

        Expr_reals_input(Keys bkeys_, Keys rkeys_, Formula phi)
                   : Inherit(move(bkeys_), move(phi)), rkeys(move(rkeys_)) { }

        template <typename S>
        void declare(S& s) const
        {
            Inherit::declare(s);
            for (auto& k : rkeys) {
                s.declare_real_var(k);
            }
        }

        String keys_to_string() const& override
        {
            using unsot::to_string;
            return Inherit::keys_to_string() + " [ "s + to_string(rkeys) + "]";
        }
    };

    template <typename S, typename InputT = Expr_reals_input>
    struct Reals_inst_expr_case : Inherit<Inst_expr_case<S, InputT>> {
        using Inherit = unsot::Inherit<Inst_expr_case<S, InputT>>;

        using typename Inherit::Output;

        using Inherit::Inherit;

        bool valid_sort(const Output& s, const Key& k) const noexcept override
        {
            return Inherit::valid_sort(s, k) || s.contains_real(k);
        }

        String sort_msg(const Key& k) const override
        {
            return "The instantiated expression is not bool nor real: "s + k;
        }

        bool valid_fun(const Output& s, const Key& k) const noexcept override
        {
            if (!Inherit::valid_sort(s, k)) return s.contains_real_fun(k);
            return Inherit::valid_fun(s, k) || s.contains_real_pred(k);
        }

        String fun_msg(const Key& k) const override
        {
            return "The instantiated expression is not [bool function or real predicate] nor [real function]: "s + k;
        }

        bool valid_def_fun(const Output& s, const Key& k) const noexcept override
        {
            return Inherit::valid_def_fun(s, k)
                && !s.contains_def_real_fun(k) && !s.contains_def_real_pred(k);
        }

        void eval(Output& s) override
        {
            Inherit::eval(s);
            try_eval_all(s.reals());
            try_eval_all(this->expected().reals());
        }
    };

    template <typename S>
    Suite::Data<Reals_inst_expr_case<S>> reals_inst_expr_valid_data()
    {
        return {
            { {{}, {}, "+ 1 2"},                 {invoke([]{ S s; s.instantiate_real("+ 1 2"); return s; })} },
            { {{}, {}, "+ 1 2"},                 {invoke([]{ S s; s.define_real_fun("_f", "+ 1 2"); s.instantiate_def_real_fun("_f"); return s; })} },
            { {{"a"}, {"x"}, "+ 1 2"},           {invoke([]{ S s; s.declare_real_var("x"); s.declare_bool_var("a"); s.instantiate_real("+ 1 2"); return s; })} },
            { {{}, {"x"}, "+ x 2"},              {invoke([]{ S s; s.declare_real_var("x"); s.instantiate_real("+ x 2"); return s; })} },
            { {{}, {"x"}, "+ x 2"},              {invoke([]{ S s; s.declare_real_var("x"); s.define_real_fun("_f", "+ x 2"); s.instantiate_def_real_fun("_f"); return s; })} },
            { {{}, {"x"}, "> x 0"},              {invoke([]{ S s; s.declare_real_var("x"); s.instantiate_real("> x 0"); return s; })} },
            { {{}, {"x"}, "> x 0"},              {invoke([]{ S s; s.declare_real_var("x"); s.define_real_pred("_p", "> x 0"); s.instantiate_def_real_pred("_p"); return s; })} },
            { {{"a"}, {"x"}, "and 1 a"},         {invoke([]{ S s; s.declare_real_var("x"); s.declare_bool_var("a"); s.declare_bool_var("_F_1"); return s; })} },
            { {{}, {}, "+ (* (exp 5) 3) (sin 2)"}, {invoke([]{ S s; s.instantiate_real("+ (* (exp 5) 3) (sin 2)"); return s; })} },
            { {{}, {}, "+ (* (exp 5) 3) (sin 2)"}, {invoke([]{ S s; s.define_real_fun("_f", "+ (* (exp 5) 3) (sin 2)"); s.instantiate_def_real_fun("_f"); return s; })} },
            { {{}, {"x", "y"}, "+ (* (exp x) 3) (sin y)"}, {invoke([]{ S s; s.declare_real_var("x"); s.declare_real_var("y"); s.instantiate_real("+ (* (exp x) 3) (sin y)"); return s; })} },
            { {{}, {"x", "y"}, "+ (* (exp x) 3) (sin y)"}, {invoke([]{ S s; s.declare_real_var("x"); s.declare_real_var("y"); s.define_real_fun("_f", "+ (* (exp x) 3) (sin y)"); s.instantiate_def_real_fun("_f"); return s; })} },
            { {{}, {"x", "y"}, "> (sin x) (^ y 2)"}, {invoke([]{ S s; s.declare_real_var("x"); s.declare_real_var("y"); s.instantiate_real("> (sin x) (^ y 2)"); return s; })} },
            { {{}, {"x", "y"}, "> (sin x) (^ y 2)"}, {invoke([]{ S s; s.declare_real_var("x"); s.declare_real_var("y"); s.define_real_pred("_p", "> (sin x) (^ y 2)"); s.instantiate_def_real_pred("_p"); return s; })} },
            { {{"a"}, {"x", "y"}, "and a (> (sin x) (^ y 2))"}, {invoke([]{ S s; s.declare_real_var("x"); s.declare_real_var("y"); s.declare_bool_var("a"); s.define_real_pred("_p", "> (sin x) (^ y 2)"); s.instantiate_def_real_pred("_p"); s.declare_bool_var("_F_2"); return s; })} },
            { {{}, {"x"}, "= (< x 1) (= x 0)"}, {invoke([]{ S s; s.declare_real_var("x"); s.instantiate_real("< x 1"); s.instantiate_real("= x 0"); s.declare_bool_var("_F_3"); return s; })} },
            { {{"a"}, {"x"}, "= (= a 1) (= x 0)"}, {invoke([]{ S s; s.declare_real_var("x"); s.declare_bool_var("a"); s.instantiate_real("= x 0"); s.declare_bool_var("_F_2"); return s; })} },
            { {{}, {"x"}, "= (+ x 1) (^ x 2)"}, {invoke([]{ S s; s.declare_real_var("x"); s.instantiate_real("= (+ x 1) (^ x 2)"); return s; })} },
        };
    }

    template <typename S>
    Suite::Data<Reals_inst_expr_case<S>> reals_inst_expr_invalid_data()
    {
        return {
            { {{}, {}, ""},                      ignore            },
            { {{}, {}, "x"},                     ignore            },
            { {{}, {}, "x y"},                   ignore            },
            { {{}, {"x"}, "x"},                  ignore            },
            { {{}, {}, "+ x 2"},                 ignore            },
            { {{"x"}, {}, "+ x 2"},              ignore            },
            //+ { {{"a"}, {"x"}, "and x 1"},         ignore            },
            { {{}, {"x"}, "+ 1 2"},              {invoke([]{ S s; s.declare_bool_var("x"); s.instantiate_real("+ 1 2"); return s; })} },
            { {{"a"}, {"x"}, "+ 1 2"},           {invoke([]{ S s; s.declare_real_var("x"); s.instantiate_real("+ 1 2"); return s; })} },
            { {{}, {"x"}, "+ x 2"},              {invoke([]{ S s; s.declare_real_var("x"); s.instantiate_real("+ 2 x"); return s; })} },
            { {{"a"}, {"x"}, "and a x"},         ignore            },
            { {{"a"}, {"x"}, "and a (or x)"},    ignore            },
            { {{"a"}, {"x", "y"}, "and a (+ x y)"}, ignore         },
            //+ { {{}, {"x", "y"}, "+ x (> y 0)"},   ignore            },
            { {{"a"}, {"x"}, "+ x (and a 1)"},   ignore            },
        };
    }

    ////////////////////////////////////////////////////////////////

    template <typename S, typename InputT = Expr_reals_input>
    struct Reals_assert_expr_case : Inherit<Assert_expr_case<S, InputT>> {
        using Inherit = unsot::Inherit<Assert_expr_case<S, InputT>>;

        using typename Inherit::Output;

        using Inherit::Inherit;

        void eval(Output& s) override
        {
            Inherit::eval(s);
            try_eval_all(s.reals());
        }
    };

    template <typename S>
    Suite::Data<Reals_assert_expr_case<S>> reals_assert_expr_valid_data()
    {
        return {
            { {{"a"}, {"x"}, "and a (or (and a (> x 0)) (and (not a) (<= x 0)) )"}, {invoke([]{ S s; s.declare_real_var("x"); s.define_bool_var("a", true); s.assert_real("> x 0"); s.assert_expr("not (<= x 0)"); return s; })} },
            { {{"a"}, {"x"}, "and a (=> a (> x 0)) (=> (not a) (<= x 0) )"}, {invoke([]{ S s; s.declare_real_var("x"); s.define_bool_var("a", true); s.assert_real("> x 0"); s.assert_expr("not (<= x 0)"); return s; })} },
            { {{"a"}, {"x"}, "and a (= a (> x 0)) (= (not a) (<= x 0) )"}, {invoke([]{ S s; s.declare_real_var("x"); s.define_bool_var("a", true); s.assert_real("> x 0"); s.assert_expr("not (<= x 0)"); return s; })} },
            { {{"a"}, {"x"}, "and a (= a (= x 0) )"}, {invoke([]{ S s; s.declare_real_var("x"); s.define_bool_var("a", true); s.assert_real("= x 0"); return s; })} },
            { {{"a"}, {"x"}, "and a (= (= a 1) (= x 0) )"}, {invoke([]{ S s; s.declare_real_var("x"); s.define_bool_var("a", true); s.assert_real("= x 0"); return s; })} },
            { {{}, {"x", "y"}, "and (= x 0) (= x y)"}, {invoke([]{ S s; s.declare_real_var("x"); s.declare_real_var("y"); s.assert_real("= x 0"); s.assert_real("= x y"); return s; })} },
            { {{}, {"x", "y"}, "and (= x y) (= x 0)"}, {invoke([]{ S s; s.declare_real_var("x"); s.declare_real_var("y"); s.assert_real("= x y"); s.assert_real("= x 0"); return s; })} },
            { {{"a"}, {"x"}, "and a (=> a (< (- x 2) 0))"}, {invoke([]{ S s; s.define_bool_var("a", true); s.declare_real_var("x"); s.assert_real("< (- x 2) 0"); return s; })} },
            { {{"a"}, {"x"}, "and a (=> a (= (- x 2) 0))"}, {invoke([]{ S s; s.define_bool_var("a", true); s.declare_real_var("x"); s.assert_real("= (- x 2) 0"); return s; })} },
            { {{"a"}, {"x", "y"}, "and a (=> a (= (- x y) 0))"}, {invoke([]{ S s; s.define_bool_var("a", true); s.declare_real_var("x"); s.declare_real_var("y"); s.assert_real("= (- x y) 0"); return s; })} },
            { {{"a"}, {"x", "y"}, "and a (=> (not a) (= (- x y) 0))"}, {invoke([]{ S s; s.define_bool_var("a", true); s.declare_real_var("x"); s.declare_real_var("y"); s.assert_expr("not (= (- x y) 0)"); return s; })} },
            { {{"a", "b"}, {"x", "y"}, "and a b (=> (and (not a) b) (= (- x y) 0))"}, {invoke([]{ S s; s.define_bool_var("a", true); s.define_bool_var("b", true); s.declare_real_var("x"); s.declare_real_var("y"); s.assert_expr("not (= (- x y) 0)"); return s; })} },
            { {{"a", "b"}, {"x", "y"}, "and a b (=> (or (and (not a) b)) (= (- x y) 0))"}, {invoke([]{ S s; s.define_bool_var("a", true); s.define_bool_var("b", true); s.declare_real_var("x"); s.declare_real_var("y"); s.assert_expr("not (= (- x y) 0)"); return s; })} },
        };
    }

    template <typename S>
    Suite::Data<Reals_assert_expr_case<S>> reals_assert_expr_invalid_data()
    {
        return {
            { {{}, {"x"}, "= x 0"}, {invoke([]{ S s; s.define_real_var("x", 0); s.assert_real("= x 0"); s.assert_real("= x 0"); return s; })} },
        };
    }

    ////////////////////////////////////////////////////////////////

    template <typename S>
    struct Reals_parse_case : Inherit<Parse_case<S>> {
        using Inherit = unsot::Inherit<Parse_case<S>>;

        using typename Inherit::Output;

        using Inherit::Inherit;

        void eval(Output& s) override
        {
            Inherit::eval(s);
            try_eval_all(s.reals());
        }
    };

    template <typename S>
    Suite::Data<Reals_parse_case<S>> reals_parse_valid_data()
    {
        return {
            { "",                                {invoke([]{ S s; return s; })} },
            { "(declare-fun x () Real)",         {invoke([]{ S s; s.declare_real_var("x"); return s; })} },
            { "(declare-const x Real)",          {invoke([]{ S s; s.declare_real_var("x"); return s; })} },
            { "(define-fun x () Real 1)",        {invoke([]{ S s; s.define_real_var("x", 1); return s; })} },
            { "(define-fun x () Real 0)",        {invoke([]{ S s; s.define_real_var("x", 0); return s; })} },
            { "(define-fun f () Real (* 5 10))", {invoke([]{ S s; s.define_real_fun("f", "* 5 10"); return s; })} },
            { "(define-fun f ((&x Real)) Real (* &x 10))", {invoke([]{ S s; s.define_real_fun("f", "* &x 10", {"&x"}); return s; })} },
            { "(define-fun x () Real 10)(assert (> x 0))", {invoke([]{ S s; s.define_real_var("x", 10); s.assert_real("> x 0"); return s; })} },
        };
    }

    template <typename S>
    Suite::Data<Reals_parse_case<S>> reals_parse_invalid_data()
    {
        return {
            { "(declare-const x real)",          ignore                         },
            { "(declare-const x () real)",       ignore                         },
            { "(declare-const x Realx)",         ignore                         },
            { "(declare-const x () Realx)",      ignore                         },
        };
    }
}

////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////

int main(int, const char*[])
try {
    using namespace unsot;
    using namespace unsot::test;
    using namespace unsot::smt::solver::test;
    using namespace std;
    using unsot::ignore;

    using S = Test_reals_solver;
    using Sa = Test_auto_reals_solver;

    ////////////////////////////////////////////////////////////////

    auto vars_msg_f = [](auto& msg){ return "decl./def. and expansion "s + msg; };
    auto auto_msg_f = [](auto& msg){ return msg + " (auto assignees)"; };

    const String reals_msg = vars_msg_f("of real vars/funs");
    Suite(reals_msg).test<Reals_case<S>>(reals_valid_data<S>());
    Suite(reals_msg, true).test<Reals_case<S>>(reals_invalid_data<S>());

    Suite(auto_msg_f(reals_msg)).test<Reals_case<Sa>>(reals_valid_data<Sa>());
    Suite(auto_msg_f(reals_msg), true).test<Reals_case<Sa>>(reals_invalid_data<Sa>());

    const String preds_msg = vars_msg_f("and assertions of bools or real preds");
    Suite(preds_msg).test<Real_preds_case<S>>(real_preds_valid_data<S>());
    Suite(preds_msg, true).test<Real_preds_case<S>>(real_preds_invalid_data<S>());

    Suite(auto_msg_f(preds_msg)).test<Real_preds_case<Sa>>(real_preds_valid_data<Sa>());
    Suite(auto_msg_f(preds_msg), true).test<Real_preds_case<Sa>>(real_preds_invalid_data<Sa>());

    ////////////////////////////////////////////////////////////////

    const String inst_msg = "instantiation of plain expressions";
    Suite(inst_msg).test<Reals_inst_expr_case<S>>(reals_inst_expr_valid_data<S>());
    Suite(inst_msg, true).test<Reals_inst_expr_case<S>>(reals_inst_expr_invalid_data<S>());

    Suite(auto_msg_f(inst_msg)).test<Reals_inst_expr_case<Sa>>(reals_inst_expr_valid_data<Sa>());
    Suite(auto_msg_f(inst_msg), true).test<Reals_inst_expr_case<Sa>>(reals_inst_expr_invalid_data<Sa>());

    ////////////////////////////////////////////////////////////////

    const String assert_msg = "assertion of plain expressions";
    Suite(assert_msg).test<Reals_assert_expr_case<S>>(reals_assert_expr_valid_data<S>());
    Suite(assert_msg, true).test<Reals_assert_expr_case<S>>(reals_assert_expr_invalid_data<S>());

    Suite(auto_msg_f(assert_msg)).test<Reals_assert_expr_case<Sa>>(reals_assert_expr_valid_data<Sa>());
    Suite(auto_msg_f(assert_msg), true).test<Reals_assert_expr_case<Sa>>(reals_assert_expr_invalid_data<Sa>());

    ////////////////////////////////////////////////////////////////

    const String parse_msg = "parsing";
    Suite(parse_msg).test<Reals_parse_case<S>>(reals_parse_valid_data<S>());
    Suite(parse_msg, true).test<Reals_parse_case<S>>(reals_parse_invalid_data<S>());

    Suite(auto_msg_f(parse_msg)).test<Reals_parse_case<Sa>>(reals_parse_valid_data<Sa>());
    Suite(auto_msg_f(parse_msg), true).test<Reals_parse_case<Sa>>(reals_parse_invalid_data<Sa>());

    ////////////////////////////////////////////////////////////////

    ////////////////////////////////////////////////////////////////
    ////////////////////////////////////////////////////////////////

    cout << endl << "Success." << endl;
    return 0;
}
catch (const unsot::Error& e) {
    std::cout << e << std::endl;
    throw;
}
