#include "test.hpp"
#include "expr.hpp"
#include "expr/alg.hpp"

namespace unsot::test {
    using namespace expr;

    ////////////////////////////////////////////////////////////////

    struct List_params {
        bool nest;
        bool emerge;
    };

    String to_string(const List_params& rhs)
    {
        using unsot::to_string;
        return "nest:"s + to_string(rhs.nest) + " emerge:"s + to_string(rhs.emerge);
    }

    struct List_case : Inherit<Cons_case<List, String, String, List_params>> {
        using Inherit::Inherit;

        Output result() override
        {
            using unsot::to_string;

            List ls(ccons());
            /// To also check move assignment
            List tmp(ls);
            List ls2;
            ls2 = move(tmp);
            if (cparams().nest) {
                ls.nest();
                ls2 = move(ls2).to_nested();
                if (!should_throw()) {
                    List ls3 = ccons().to_nested();
                    expect(ls == ls3,
                           "Result differ with nested copy: "s
                           + to_string(ls) + " != " + to_string(ls3));
                }
            }
            if (cparams().emerge) {
                ls.emerge_rec();
                ls2.emerge_rec();
                if (!should_throw()) {
                    List ls3 = ccons().emerge_rec();
                    expect(ls == ls3,
                           "Result differ with emerged copy: "s
                           + to_string(ls) + " != " + to_string(ls3));
                }
            }
            if (!should_throw()) {
                expect(ls == ls2,
                       "Result differ with copy of itself: "s
                       + to_string(ls) + " != " + to_string(ls2));

                cout << cinput_cp() << " -> " << ls << endl;
            }

            return to_string(ls);
        }
    };

    ////////////////////////////////////////////////////////////////

    struct Flat_deep_case : Inherit<Case<const List&, Vector<bool>>> {
        using Inherit::Inherit;

        Output result() override
        {
            return {is_flat(cinput()), is_deep(cinput()), is_deep_flat(cinput()), is_deep_sparse(cinput()), is_deep_n(cinput(), 2)};
        }
    };

    struct Flatten_case : Inherit<Cons_case<List, String, String>> {
        using Inherit::Inherit;

        Output result() override
        {
            using unsot::to_string;

            List ls(cons());
            List ls2(ls);
            List ls3(ls.to_flat());
            if (!should_throw()) {
                expect(is_flat(ls.flatten()),
                       "'is_flat()' is false after 'flatten()': '"s
                       + to_string(ls) + "'");
                expect(is_flat(ls2.emerge_rec().flatten()),
                       "'is_flat()' is false after 'emerge_rec().flatten()': '"s
                       + to_string(ls2) + "'");
                expect(ls == ls2,
                       "Flatten versions with/without 'emerge_rec() differ: '"s
                       + to_string(ls) + "' != '"
                       + to_string(ls2) + "'");
                expect(ls == ls3,
                       "Result differ with flatten copy: '"s
                       + to_string(ls) + "' != '"
                       + to_string(ls3) + "'");
                cout << cinput_cp() << " -> " << ls << endl;
            }
            return to_string(ls);
        }
    };

    ////////////////////////////////////////////////////////////////

    template <typename Arg, bool cast = false>
    struct Flatten_transform_case : Inherit<Cons_case<List, String, Values<Arg>>> {
        using Inherit = unsot::Inherit<Cons_case<List, String, Values<Arg>>>;

        using typename Inherit::Output;

        using Inherit::Inherit;

        Output result() override
        {
            using unsot::to_string;

            List ls = this->cons();
            ls.flatten();
            const auto lsc = ls;
            Output values = invoke([&lsc]{
                if constexpr (cast) {
                    if constexpr (is_same_v<Arg, Key>)
                        return cast_to_keys_check(lsc);
                    else return cast_to_values_check<Arg>(lsc);
                }
                else return to_values_check<Arg>(lsc);
            });

            Output tmp = to_values_check<Arg>(move(ls));
            expect(values == tmp,
                   "Output differs with additional conversion: "s
                   + to_string(values) + " != " + to_string(tmp));

            cout << this->cinput_cp() << " -> " << lsc << " -> " << values << endl;

            return values;
        }
    };

    ////////////////////////////////////////////////////////////////

    struct Cast_lists_case : Inherit<Cons_case<List, String, Lists>> {
        using Inherit = unsot::Inherit<Cons_case<List, String, Lists>>;

        using typename Inherit::Output;

        using Inherit::Inherit;

        Output result() override
        {
            using unsot::to_string;

            List ls = this->cons();
            Output lists = cast_to_lists_check(ls);
            Output tmp = cast_to_lists_check(move(ls));
            expect(lists == tmp,
                   "Output differs with additional cast: "s
                   + to_string(lists) + " != " + to_string(tmp));

            return lists;
        }
    };

    ////////////////////////////////////////////////////////////////

    struct Replace_params {
        const Keys& from{};
        const Keys& to{};
    };

    String to_string(const Replace_params& rhs)
    {
        return to_string(rhs.from) + " -> " + to_string(rhs.to);
    }

    struct Replace_case : Inherit<Case<List, List, Replace_params>> {
        using Inherit::Inherit;

        Output result() override
        {
            return replace(move(input()), cparams().from, cparams().to);
        }
    };

    ////////////////////////////////////////////////////////////////
}

////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////

int main(int, const char*[])
try {
    using namespace unsot;
    using namespace unsot::test;
    using namespace std;
    using unsot::ignore;

    ////////////////////////////////////////////////////////////////

    const String ls_msg = "constructing of expr lists";
    Suite(ls_msg).test<List_case>({
        {"",                                         "( )",                                       {false, true},                                   },
        {"",                                         "( )",                                       {true, true},                                    },
        {"",                                         "( ( ) )",                                   {true, false},                                   },
        {"  ",                                       "( )",                                       {false, true},                                   },
        {"  ",                                       "( )",                                       {true, true},                                    },
        {"  ",                                       "( ( ) )",                                   {true, false},                                   },
        {"-",                                        "( - )",                                     {false, true},                                   },
        {"-",                                        "( - )",                                     {true, true},                                    },
        {"-",                                        "( ( - ) )",                                 {true, false},                                   },
        {" 1 ",                                      "( 1 )",                                     {false, false},                                  },
        {"1 2 3",                                    "( 1 2 3 )",                                 {false, false},                                  },
        {"1 2 3",                                    "( ( 1 2 3 ) )",                             {true, false},                                   },
        {"1 2 3",                                    "( 1 2 3 )",                                 {true, true},                                    },
        {"5  1  (-(+ abc 1) 1 2 (- 1))",             "( 5 1 ( - ( + abc 1 ) 1 2 ( - 1 ) ) )",     {false, true},                                   },
        {"()",                                       "( ( ) )",                                   {false, false},                                  },
        {"()",                                       "( )",                                       {false, true},                                   },
        {"()",                                       "( )",                                       {true, true},                                    },
        {"(())",                                     "( )",                                       {false, true},                                   },
        {"(())",                                     "( ( ( ) ) )",                               {false, false},                                  },
        {"((()))",                                   "( )",                                       {false, true},                                   },
        {"((()))",                                   "( ( ( ( ) ) ) )",                           {false, false},                                  },
        {"0(1(2(3)4)5)6",                            "( 0 ( 1 ( 2 3 4 ) 5 ) 6 )",                 {false, true},                                   },
        {"0(1(2(3)4)5)6",                            "( 0 ( 1 ( 2 ( 3 ) 4 ) 5 ) 6 )",             {false, false},                                  },
        {"1 (+ 2 (3))",                              "( 1 ( + 2 3 ) )",                           {false, true},                                   },
        {"(1) (+ 2 (3))",                            "( 1 ( + 2 3 ) )",                           {false, true},                                   },
        {"(1) (+ 2 (3))",                            "( ( 1 ) ( + 2 ( 3 ) ) )",                   {false, false},                                  },
        {" (1 2 3)",                                 "( ( 1 2 3 ) )",                             {false, false},                                  },
        {" (1 2 3)",                                 "( 1 2 3 )",                                 {false, true},                                   },
        {" ((1) 2) ( ( 3) )",                        "( ( 1 2 ) 3 )",                             {false, true},                                   },
        {" ((1) 2) ( ( 3) )",                        "( ( ( 1 ) 2 ) ( ( 3 ) ) )",                 {false, false},                                  },
        {"+ 1",                                      "( + 1 )",                                   {false, false},                                  },
        {"+ 1 \x00 21244",                           "( + 1 )",                                   {false, false},                                  },
        {" \x00 ",                                   "( )",                                       {false, false},                                  },
    });

    Suite(ls_msg, true).test<List_case>({
        {"(",                                       ignore,                                       {false, false},                                  },
        {")",                                       ignore,                                       {false, false},                                  },
        {"  ) ",                                    ignore,                                       {false, false},                                  },
        {" (  ",                                    ignore,                                       {false, false},                                  },
        {")(",                                      ignore,                                       {false, false},                                  },
        {" )  ( ",                                  ignore,                                       {false, false},                                  },
        {" ( ( ) ",                                 ignore,                                       {false, false},                                  },
        {"  ( ) ) ",                                ignore,                                       {false, false},                                  },
        {"  x ) ",                                  ignore,                                       {false, false},                                  },
        {" ( x  ",                                  ignore,                                       {false, false},                                  },
        {" ( x ) ( ",                               ignore,                                       {false, false},                                  },
        {" ( ( x )  ",                              ignore,                                       {false, false},                                  },
        {" ( ( x ) ( ",                             ignore,                                       {false, false},                                  },
        {" ( ( x ) ) ) ",                           ignore,                                       {false, false},                                  },
        {" ( ( x ) x) ) ",                          ignore,                                       {false, false},                                  },
        {" ( ( x ) x) 7) ",                         ignore,                                       {false, false},                                  },
        {" ( ( ( x ) x) 7 ",                        ignore,                                       {false, false},                                  },
    });

    ////////////////////////////////////////////////////////////////

    Suite("checking of flat and deep state").test<Flat_deep_case>({
        { {""},                                            {true, true, true, true, true}      },
        { {"+ 1 2"},                                       {true, false, false, false, false}  },
        { {"+ x 2 5 1 +"},                                 {true, false, false, false, false}  },
        { {"+ x (2) 5 1 +"},                               {false, false, false, false, false} },
        { {"+ (x 2) 5 1 +"},                               {false, false, false, false, false} },
        { {"(+ x 2) (5 1 +)"},                             {false, true, true, true, false}    },
        { {"(+ (x) 2) (5 1 +)"},                           {false, true, false, true, false}   },
        { {"((+ x 2) (5 1 +))"},                           {false, true, false, false, true}   },
    });

    ////////////////////////////////////////////////////////////////

    Suite("flattening expr lists").test<Flatten_case>({
        {"",                                                         "( )",                                                                        },
        {"+ 1 2",                                                    "( + 1 2 )",                                                                  },
        {"+ (x (2)) 5 1 +",                                          "( + x 2 5 1 + )",                                                            },
        {"5  1  (-(+ abc 1) 1 2 (- 1))",                             "( 5 1 - + abc 1 1 2 - 1 )",                                                  },
        {"0(1(2(3)4)5)6",                                            "( 0 1 2 3 4 5 6 )",                                                          },
        {"* (+ 1 2 3) (+ 4 5 6) (+ 7 8 9)",                          "( * + 1 2 3 + 4 5 6 + 7 8 9 )",                                              },
    });

    ////////////////////////////////////////////////////////////////

    auto flat_transform_msg_f = [](String type, bool cast = false){
        return "flattening and "s + (cast ? "casting" : "converting")
               + " expr lists into arrays of values<" + type + ">";
    };

    Suite(flat_transform_msg_f("int", true)).test<Flatten_transform_case<int, true>>({
        {"",                                                         Values<int>{},                                                                },
        {"1 2",                                                      {1, 2},                                                                       },
        {"0 (1 2 3) (4 5 6) (7 8 9)",                                {0, 1, 2, 3, 4, 5, 6, 7, 8, 9},                                               },
    });

    Suite(flat_transform_msg_f("int", true), true).test<Flatten_transform_case<int, true>>({
        {"1 x",                                                      ignore                                                                        },
        {"1 3.1415",                                                 ignore                                                                        },
    });

    Suite(flat_transform_msg_f("double", true)).test<Flatten_transform_case<double, true>>({
        {"1. 3.1415",                                                {1, 3.1415},                                                                  },
    });

    Suite(flat_transform_msg_f("double", true), true).test<Flatten_transform_case<double, true>>({
        {"1 2.5",                                                    ignore                                                                        },
    });

    Suite(flat_transform_msg_f("String", true)).test<Flatten_transform_case<String, true>>({
        {"a x",                                                      {"a", "x"},                                                                   },
    });

    Suite(flat_transform_msg_f("String", true), true).test<Flatten_transform_case<String, true>>({
        {"1 x",                                                      ignore                                                                        },
        {"1 2",                                                      ignore                                                                        },
    });

    Suite(flat_transform_msg_f("int")).test<Flatten_transform_case<int>>({
        {"",                                                         Values<int>{},                                                                },
        {"1 2",                                                      {1, 2},                                                                       },
        {"0 (1 2 3) (4 5 6) (7 8 9)",                                {0, 1, 2, 3, 4, 5, 6, 7, 8, 9},                                               },
        {"1. 3.1415",                                                {1, 3},                                                                       },
    });

    Suite(flat_transform_msg_f("int"), true).test<Flatten_transform_case<int>>({
        {"1 x",                                                      ignore                                                                        },
    });

    Suite(flat_transform_msg_f("String")).test<Flatten_transform_case<String>>({
        {"a x",                                                      {"a", "x"},                                                                   },
    });

    Suite(flat_transform_msg_f("String"), true).test<Flatten_transform_case<String>>({
        {"1 x",                                                      ignore                                                                        },
    });

    ////////////////////////////////////////////////////////////////

    const String to_lists_msg = "casting expr sublists into vector";
    Suite(to_lists_msg).test<Cast_lists_case>({
        {"",                                                         Lists{},                                                                      },
        {"(1 2 3) (4 5 6) (7 8 9)",                                  {{"1 2 3"}, {"4 5 6"}, {"7 8 9"}},                                            },
        {"(1 2 3) () (7 8 9)",                                       {{"1 2 3"}, {}, {"7 8 9"}},                                                   },
    });

    Suite(to_lists_msg, true).test<Cast_lists_case>({
        {"1",                                                        ignore                                                                        },
        {"1 2",                                                      ignore                                                                        },
        {"0 (1 2 3) (4 5 6) (7 8 9)",                                ignore                                                                        },
    });

    ////////////////////////////////////////////////////////////////

    const String replace_msg = "replacing particular keys";
    Suite(replace_msg).test<Replace_case>({
        { "and a",                                  "and a",                  Replace_params{},    },
        { "and a",                                  "and b",                  {{"a"}, {"b"}},      },
        { "and a a",                                "and b b",                {{"a"}, {"b"}},      },
        { "and a a",                                "and a a",                {{"b"}, {"a"}},      },
        { "and (or a b) (xor a c)",                 "and (or b b) (xor b c)", {{"a"}, {"b"}},      },
        { "and (or a b) (xor a c)",                 "and (or x1 x2) (xor x1 x3)", {{"a", "b", "c"}, {"x1", "x2", "x3"}},      },
        { "a (c a b) (f a c)",                      "x1 (x3 x1 x2) (f x1 x3)", {{"a", "b", "c"}, {"x1", "x2", "x3"}},      },
    });

    ////////////////////////////////////////////////////////////////

    ////////////////////////////////////////////////////////////////
    ////////////////////////////////////////////////////////////////

    cout << endl << "Success." << endl;
    return 0;
}
catch (const unsot::Error& e) {
    std::cout << e << std::endl;
    throw;
}
