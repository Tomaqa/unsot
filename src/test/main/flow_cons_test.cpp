#include "test.hpp"
#include "ode/flow.hpp"
#include "ode/flow/parse.hpp"

namespace unsot::test {
    using namespace ode;
    using namespace ode::flow;
    using Keys = flow::Keys;
    using Ekeys = expr::Keys;

    using Odes = Flow::Odes;
    using Invariants = Flow::Invariants;
    using Local_invariants = Flow::Local_invariants;
    using Timer = Flow::Timer;

    ////////////////////////////////////////////////////////////////

    template <typename T, typename O = T>
    struct Parse_case_base : Inherit<Cons_case<T, String, O>> {
        using Inherit = unsot::Inherit<Cons_case<T, String, O>>;

        using typename Inherit::Cons;

        using Inherit::Inherit;

        Cons cons() override
        {
            return Cons::parse(move(this->input()));
        }
    };

    ////////////////////////////////////////////////////////////////

    using Keys_parse_case = Parse_case_base<Keys>;
    using Ekeys_parse_case = Parse_case_base<Keys, Ekeys>;

    struct All_keys_case : Inherit<Ekeys_parse_case> {
        using Inherit::Inherit;

        Output result() override
        {
            return cons().ckeys();
        }
    };

    struct Ode_keys_case : Inherit<Ekeys_parse_case> {
        using Inherit::Inherit;

        Output result() override
        {
            return cons().ode_keys_slice();
        }
    };

    struct Arg_keys_case : Inherit<Ekeys_parse_case> {
        using Inherit::Inherit;

        Output result() override
        {
            return cons().arg_keys_slice();
        }
    };

    struct T_ode_keys_case : Inherit<Ekeys_parse_case> {
        using Inherit::Inherit;

        Output result() override
        {
            return cons().t_ode_keys_slice();
        }
    };

    ////////////////////////////////////////////////////////////////

    using State_parse_case = Parse_case_base<State>;

    ////////////////////////////////////////////////////////////////

    using Flow_parse_case = Parse_case_base<Flow>;

    struct Parse_input {
        String first;
        String second{};
        String third{};
    };

    String to_string(const Parse_input& rhs)
    {
        return rhs.first + " , " + rhs.second + " , " + rhs.third;
    }

    struct Flow_parser_case : Inherit<Cons_case<Flow, Parse_input>> {
        using typename Inherit::Cons;

        using Inherit::Inherit;

        Cons cons() override
        {
            if (this->cinput().third.empty()) {
                if (this->cinput().second.empty()) return Parse(move(input().first)).perform();
                return Parse(move(input().first), move(input().second)).perform();
            }
            return Parse(move(input().first), move(input().second), move(input().third)).perform();
        }
    };

    ////////////////////////////////////////////////////////////////

    using Config_parse_case = Parse_case_base<Config>;

    ////////////////////////////////////////////////////////////////
    ////////////////////////////////////////////////////////////////

    using Fs = Formulas;
    const auto Phi = [](auto&&... args){ return Formula::cons(FORWARD(args)...); };
    const auto Phis = [](auto&&... args){
        return Fs{Formula::cons(FORWARD(args))...};
    };

    template <typename C>
    Suite::Data<C> flow_parse_valid_data()
    {
        return {
            { {"((x) (a b)) (= x' (+ x 1))"},                                     {Keys{Ekeys{"x"}, Ekeys{"a", "b"}}, All_formulas{Phis("+ x 1")}, All_formulas_map{}}                                                                     },
            { {"((x) (a b)) ((= x' (+ x 1)))"},                                   {Keys{Ekeys{"x"}, Ekeys{"a", "b"}}, All_formulas{Phis("+ x 1")}, All_formulas_map{}}                                                                     },
            { {"((x) (a b)) (= x' (+ x 1)) ()"},                                  {Keys{Ekeys{"x"}, Ekeys{"a", "b"}}, All_formulas{Phis("+ x 1")}, All_formulas_map{}}                                                                     },
            { {"((x) (a b)) ((= x' (+ x 1))) ()"},                                {Keys{Ekeys{"x"}, Ekeys{"a", "b"}}, All_formulas{Phis("+ x 1")}, All_formulas_map{}}                                                                     },
            { {"((x) (a b)) ((= x' (+ x 1))) (())"},                              {Keys{Ekeys{"x"}, Ekeys{"a", "b"}}, All_formulas{Phis("+ x 1")}, All_formulas_map{}}                                                                     },
            { {"((x) (a b)) ((= x' (+ x 1))) ((> x 0))"},                         {Keys{Ekeys{"x"}, Ekeys{"a", "b"}}, All_formulas{Phis("+ x 1")}, All_formulas_map{{"x", Phis("> x 0")}}}                                                 },
            { {"((x) (a b)) (= x' (+ x 1)) (> x 0)"},                             {Keys{Ekeys{"x"}, Ekeys{"a", "b"}}, All_formulas{Phis("+ x 1")}, All_formulas_map{{"x", Phis("> x 0")}}}                                                 },
            { {"((x) (a b)) ((= x' x)) (()(> x 0)())"},                           {Keys{Ekeys{"x"}, Ekeys{"a", "b"}}, All_formulas{Phis("+ x")}, All_formulas_map{{"x", Phis("> x 0")}}}                                                   },
            { {"((x) (a b)) (= x' 1) (and (> x 0) (<= x 10))"},                   {Keys{Ekeys{"x"}, Ekeys{"a", "b"}}, All_formulas{Phis("+ 1")}, All_formulas_map{{"x", Phis("and (> x 0) (<= x 10)")}}}                                   },
            { {"((x) (a b)) (= x' 1) (and (> x (+ 1 2)) (<= x (- 10 5)))"},       {Keys{Ekeys{"x"}, Ekeys{"a", "b"}}, All_formulas{Phis("+ 1")}, All_formulas_map{{"x", Phis("and (> x (+ 1 2)) (<= x (- 10 5))")}}}                       },
            { {"((x) (a b)) (= x' 1) (or (> x 0) (<= x 10))"},                    {Keys{Ekeys{"x"}, Ekeys{"a", "b"}}, All_formulas{Phis("+ 1")}, All_formulas_map{{"x", Phis("or (> x 0) (<= x 10)")}}}                                    },
            { {"((x) (a b)) (= x' 1) (=> (> x 0) (<= x 10))"},                    {Keys{Ekeys{"x"}, Ekeys{"a", "b"}}, All_formulas{Phis("+ 1")}, All_formulas_map{{"x", Phis("=> (> x 0) (<= x 10)")}}}                                    },
            { {"((x) (a b)) (= x' (+ 1)) (and (or (> x 0)) (not (<= x 10)))"},    {Keys{Ekeys{"x"}, Ekeys{"a", "b"}}, All_formulas{Phis("+ 1")}, All_formulas_map{{"x", Phis("and (or (> x 0)) (not (<= x 10))")}}}                        },
            { {"((x y z) (a b)) ((= x' (+ x 1)) (= x' (* x _tau)) (= y' (- x _t)) (= z' (+ a b)) (= z' (- a b))) ((> x 0) (< x 0) (> y 0) (= y _tau) (< z _t) (= _tau 0) (< _t 0) (> _t (- (* x y z) (* a b))))"}, {Keys{Ekeys{"x", "y", "z"}, Ekeys{"a", "b"}}, All_formulas{Phis("+ x 1", "* x _tau"), Phis("- x _t"), Phis("+ a b", "- a b")}, All_formulas_map{{"x", Phis("> x 0", "< x 0")}, {"y", Phis("> y 0", "= y _tau")}, {"z", Phis("< z _t")}, {"_tau", Phis("= _tau 0")}, {"_t", Phis("< _t 0", "> _t (- (* x y z) (* a b))")}}}  },
            { {"((x y z) (a b)) ((= x' (+ x 1)) (= x' (* x _tau)) (= y' (- x _t)) (= z' (+ a b)) (= z' (- a b)) (> x 0) (< x 0) (> y 0) (= y _tau) (< z _t) (= _tau 0) (< _t 0) (> _t (- (* x y z) (* a b))))"}, {Keys{Ekeys{"x", "y", "z"}, Ekeys{"a", "b"}}, All_formulas{Phis("+ x 1", "* x _tau"), Phis("- x _t"), Phis("+ a b", "- a b")}, All_formulas_map{{"x", Phis("> x 0", "< x 0")}, {"y", Phis("> y 0", "= y _tau")}, {"z", Phis("< z _t")}, {"_tau", Phis("= _tau 0")}, {"_t", Phis("< _t 0", "> _t (- (* x y z) (* a b))")}}}  },
            { {"((x y z) (a b)) (= x' (+ x 1)) (= x' (* x _tau)) (= y' (- x _t)) (= z' (+ a b)) (= z' (- a b)) (> x 0) (< x 0) (> y 0) (= y _tau) (< z _t) (= _tau 0) (< _t 0) (> _t (- (* x y z) (* a b)))"}, {Keys{Ekeys{"x", "y", "z"}, Ekeys{"a", "b"}}, All_formulas{Phis("+ x 1", "* x _tau"), Phis("- x _t"), Phis("+ a b", "- a b")}, All_formulas_map{{"x", Phis("> x 0", "< x 0")}, {"y", Phis("> y 0", "= y _tau")}, {"z", Phis("< z _t")}, {"_tau", Phis("= _tau 0")}, {"_t", Phis("< _t 0", "> _t (- (* x y z) (* a b))")}}}  },
            { {"((x y z) (a b)) ((= _tau 0) (= x' (+ x 1)) (= x' (* x _tau)) (> x 0) (< x 0) (> y 0) (= y' (- x _t)) (= y _tau) (= z' (+ a b)) (= z' (- a b)) (< z _t) (< _t 0) (> _t (- (* x y z) (* a b))))"}, {Keys{Ekeys{"x", "y", "z"}, Ekeys{"a", "b"}}, All_formulas{Phis("+ x 1", "* x _tau"), Phis("- x _t"), Phis("+ a b", "- a b")}, All_formulas_map{{"x", Phis("> x 0", "< x 0")}, {"y", Phis("> y 0", "= y _tau")}, {"z", Phis("< z _t")}, {"_tau", Phis("= _tau 0")}, {"_t", Phis("< _t 0", "> _t (- (* x y z) (* a b))")}}}  },
            { {"((x y z) (a b)) ((= _tau 0) (= x' (+ x 1)) (= x' (* x _tau)) (> x 0) (< x 0) (> y 0) (= y' (- x _t)) (= y _tau) (= z' (+ a b)) (= z' (- a b)) (< z _t) (< _t 0) (> _t (- (* x y z) (* a b))))"}, {Keys{Ekeys{"x", "y", "z"}, Ekeys{"a", "b"}}, All_formulas{Phis("+ x 1", "* x _tau"), Phis("- x _t"), Phis("+ a b", "- a b")}, All_formulas_map{{"x", Phis("> x 0", "< x 0")}, {"y", Phis("> y 0", "= y _tau")}, {"z", Phis("< z _t")}, {"_tau", Phis("= _tau 0")}, {"_t", Phis("< _t 0", "> _t (- (* x y z) (* a b))")}}}  },
            { {"((x y z) (a b)) (= _tau 0) (= x' (+ x 1)) (= x' (* x _tau)) (> x 0) (< x 0) (> y 0) (= y' (- x _t)) (= y _tau) (= z' (+ a b)) (= z' (- a b)) (< z _t) (< _t 0) (> _t (- (* x y z) (* a b)))"}, {Keys{Ekeys{"x", "y", "z"}, Ekeys{"a", "b"}}, All_formulas{Phis("+ x 1", "* x _tau"), Phis("- x _t"), Phis("+ a b", "- a b")}, All_formulas_map{{"x", Phis("> x 0", "< x 0")}, {"y", Phis("> y 0", "= y _tau")}, {"z", Phis("< z _t")}, {"_tau", Phis("= _tau 0")}, {"_t", Phis("< _t 0", "> _t (- (* x y z) (* a b))")}}}  },
            { {"((x) (a b)) (= x' 1) (> x 0)()"},                                 ignore },
            { {"((x) (a b)) (= x' 1) (> x 0)(> x 0)"},                            ignore },
            { {"((x) (a b)) (= x' 1) ()(> x 0)"},                                 ignore },
            { {"((x) (a b)) (= x' 1) ()(> x 0)()"},                               ignore },
            { {"((x) (a b)) (= x' (+ 1)) (and (or (> (+ x 1) 0)) (not (<= x 10)))"}, {Keys{Ekeys{"x"}, Ekeys{"a", "b"}}, All_formulas{Phis("+ 1")}, All_formulas_map{{"", Phis("and (or (> (+ x 1) 0)) (not (<= x 10))")}}}                  },
            { {"((x y) (a b)) (= x' (+ 1)) (= y' 1) (and (or (> (+ x 1) 0)) (not (<= y 10)))"}, {Keys{Ekeys{"x", "y"}, Ekeys{"a", "b"}}, All_formulas{Phis("+ 1"), Phis(("+ 1"))}, All_formulas_map{{"", Phis("and (or (> (+ x 1) 0)) (not (<= y 10))")}}}       },
            { {"((x y) (a b)) (= x' (+ x 1)) (= y' (+ a b)) (not (and (> x 0) (> (+ x y) 0)))"},  {Keys{Ekeys{"x", "y"}, Ekeys{"a", "b"}}, All_formulas{Phis("+ x 1"), Phis("+ a b")}, All_formulas_map{{"", Phis("not (and (> x 0) (> (+ x y) 0))")}}}       },
        };
    }

    template <typename C>
    Suite::Data<C> flow_parse_invalid_data()
    {
        return {
            { {"(x)"},                                                    ignore },
            { {"(x) (a b)"},                                              ignore },
            { {"(x) (a b) (= x' (+ x 1))"},                               ignore },
            { {"((x) (a b)) (= x' (+ c 1))"},                             ignore },
            { {"((x) (a b)) ((= x' (+ x 1))) (> x 0)"},                   ignore },
            { {"((x) (a b)) (= x' (+ x 1)) ((> x 0))"},                   ignore },
            { {"((x) (a b)) (= x' 1) > x 0"},                             ignore },
            { {"((x) (a b)) (= x' 1) > a 0"},                             ignore },
            { {"((x) (a b)) (= x' 1) (>) x 0"},                           ignore },
            { {"((x) (a b)) (= x' 1) ((>) x 0)"},                         ignore },
            { {"((x) (a b)) (= x' 1) (()> x 0)"},                         ignore },
            { {"((x) (a b)) (= x' 1) () x 0"},                            ignore },
            { {"((x) (a b)) (= x' 1) (() x 0)"},                          ignore },
            { {"((x) (a b)) (= x' 1) (x) (> x 0)"},                       ignore },
            { {"((x) (a b)) (= x' 1) ((x) (> x 0))"},                     ignore },
            { {"((x) (a b)) (= x' 1) (> (x) 0)"},                         ignore },
            { {"((x) (a b)) (= x' 1) (x (> x 0))"},                       ignore },
            { {"((x) (a b)) (= x' 1) (> x 0 1)"},                         ignore },
            { {"((x) (a b)) (= x' 1) ((and) (> x 0) (<= x 10))"},         ignore },
            { {"((x) (a b)) (= x' 1) ((> x 0) <= x 10)"},                 ignore },
            { {"((x) (a b)) (= x' 1) (<= (> x 0) x 10)"},                 ignore },
            { {"((x) (a b)) (= x' x')"},                                  ignore },
            { {"((x y) (a b)) (= x' y')"},                                ignore },
            { {"((x) (a b)) ((= x' 1)) (> x 0)"},                         ignore },
            { {"((x) (a b)) (= x' 1) ((> x 0))"},                         ignore },
            { {"((x) (a b)) (= x' (+ 1)) (and (or (> (+ x 1) 0)) (not (<= a 10)))"}, ignore },
        };
    }
}


////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////

int main(int, const char*[])
try {
    using namespace unsot;
    using namespace unsot::test;
    using namespace std;
    using unsot::ignore;

    using test::Keys;

    ////////////////////////////////////////////////////////////////

    const String keys_msg = "constructing of Keys-s from direct arguments";
    Suite(keys_msg).test<Case<Keys>>({
        { {{"x"}, Ekeys{}},                                          {Ekeys{"x"}, Ekeys{}}                                  },
        { {Ekeys{"x"}},                                              {Ekeys{"x"}, Ekeys{}}                                  },
        { Ekeys{"x"},                                                {Ekeys{"x"}, Ekeys{}}                                  },
        { Ekeys{"x", "y"},                                           {Ekeys{"x", "y"}, Ekeys{}}                             },
        { Keys({"x"}, 1),                                            {Ekeys{"x"}, Ekeys{}}                                  },
        { {{"x"}, {"y"}},                                            {Ekeys{"x"}, Ekeys{"y"}}                               },
        { {{"x", "y"}},                                              {Ekeys{"x", "y"}, Ekeys{}}                             },
        { {{"x", "y"}, {"a"}},                                       {Ekeys{"x", "y"}, Ekeys{"a"}}                          },
        { Keys({"x", "y"}, 2),                                       {Ekeys{"x", "y"}, Ekeys{}}                             },
        { Keys({"x", "y"}, 1),                                       {Ekeys{"x"}, Ekeys{"y"}}                               },
        { {{"x"}, {"y'", "z"}},                                      {Ekeys{"x"}, Ekeys{"y'", "z"}}                         },
    });

    Suite(keys_msg, true).test<Case<Keys>>({
        //+ { {{}, Ekeys{"x"}},                                          ignore                                                 },
        { {{"x"}, {"y"}},                                            {Ekeys{"y"}, Ekeys{"x"}}                               },
        { {{"x", "y"}, {"a"}},                                       {Ekeys{"y", "x"}, Ekeys{"a"}}                          },
        { {{"x", "y"}, {"a", "b"}},                                  {Ekeys{"x", "y"}, Ekeys{"b", "a"}}                     },
        //+ { {{"x"}, {"x"}},                                            ignore                                                 },
        //+ { {{}, {"y"}},                                               ignore                                                 },
        //+ { Keys({"x", "y"}, 3),                                       ignore                                                 },
        //+ { Keys({"x"}, 0),                                            ignore                                                 },
        //+ { Keys({"x", "y"}, 0),                                       ignore                                                 },
        //+ { {{"x"}, {""}},                                             ignore                                                 },
        //+ { {{"x"}, Ekeys{""}},                                        ignore                                                 },
        //+ { {{"x", ""}},                                               ignore                                                 },
        //+ { {{"x"}, {""}},                                             ignore                                                 },
        //+ { {{"x y"}, Ekeys{}},                                        ignore                                                 },
        //+ { {{"x'"}, {"y", "z"}},                                      ignore                                                 },
        //+ { {{"x y'"}},                                                ignore                                                 },
        //+ { {{"x '"}},                                                 ignore                                                 },
    });

    const String keys_parse_msg = "parsing Keys-s";
    Suite(keys_parse_msg).test<Keys_parse_case>({
        { {"x"},                                                     {Ekeys{"x"}, Ekeys{}}                                  },
        { {"x y"},                                                   {Ekeys{"x", "y"}, Ekeys{}}                             },
        { {"(x)"},                                                   {Ekeys{"x"}, Ekeys{}}                                  },
        { {"(x y)"},                                                 {Ekeys{"x", "y"}, Ekeys{}}                             },
        { {"(x)()"},                                                 {Ekeys{"x"}, Ekeys{}}                                  },
        { {"(x y)()"},                                               {Ekeys{"x", "y"}, Ekeys{}}                             },
        { {"(x)(y)"},                                                {Ekeys{"x"}, Ekeys{"y"}}                               },
        { {"@v $w x* &y $z ~a +b c- *d e/ %f ^g h? i! 'k"},          {Ekeys{"@v", "$w", "x*", "&y", "$z", "~a", "+b", "c-", "*d", "e/", "%f", "^g", "h?", "i!", "'k"}, Ekeys{}}  },
        { {"(x*) (&y $z ~a +b c- *d e/ %f ^g h? i! 'k')"},           {Ekeys{"x*"}, Ekeys{"&y", "$z", "~a", "+b", "c-", "*d", "e/", "%f", "^g", "h?", "i!", "'k'"}}  },
    });

    Suite(keys_parse_msg, true).test<Keys_parse_case>({
        { {""},                                                      ignore                                                       },
        { {"()"},                                                    ignore                                                       },
        { {"() ()"},                                                 ignore                                                       },
        { {"() (x)"},                                                ignore                                                       },
        { {"x y ()"},                                                ignore                                                       },
        { {"x y (a b)"},                                             ignore                                                       },
        { {"(x y) a b"},                                             ignore                                                       },
        { {"(x y) () a b"},                                          ignore                                                       },
        { {"_t"},                                                    ignore                                                       },
        { {"_tau"},                                                  ignore                                                       },
        { {"_t _tau"},                                               ignore                                                       },
        { {"(_t _tau) (c)"},                                         ignore                                                       },
        { {"(x _t _tau) (c)"},                                       ignore                                                       },
        { {"(x) (_t)"},                                              ignore                                                       },
        { {"(x) (_tau)"},                                            ignore                                                       },
        { {"x x"},                                                   ignore                                                       },
        { {"x y x"},                                                 ignore                                                       },
        { {"(x y) (x)"},                                             ignore                                                       },
        { {"(y x) (x)"},                                             ignore                                                       },
    });

    const String arg_keys_msg = "Keys as array of all present flow keys";
    Suite(arg_keys_msg).test<All_keys_case>({
        { {"x y"},                                                {"x", "y", "_t", "_tau"}                                        },
        { {"(x y) (a b)"},                                        {"x", "y", "a", "b", "_t", "_tau"}                              },
    });

    Suite(arg_keys_msg, true).test<All_keys_case>({
        { {""},                                                   {"_t", "_tau"}                                                  },
        { {"x y"},                                                {"x", "y"}                                                      },
    });

    Suite("Keys as array of all present flow ODE keys").test<Ode_keys_case>({
        { {"x y"},                                                {"x", "y"}                                                      },
        { {"(x y) (a b)"},                                        {"x", "y"}                                                      },
    });

    Suite("Keys as array of all present flow const. keys").test<Arg_keys_case>({
        { {"x y"},                                                Ekeys{}                                                      },
        { {"(x y) (a b)"},                                        {"a", "b"}                                                      },
    });

    Suite("Keys as array of all present flow ODE keys + t key").test<T_ode_keys_case>({
        { {"x y"},                                                {"_t", "x", "y"}                                                },
        { {"(x y) (a b)"},                                        {"_t", "x", "y"}                                                },
    });

    ////////////////////////////////////////////////////////////////

    const String state_msg = "constructing of State-s from direct arguments";
    Suite(state_msg).test<Case<State>>({
        { {0, {10}, Reals{}},                                        {0, Reals{10}, Reals{}, 0}                                   },
        { {1, {10}, Reals{}, 0},                                     {1, Reals{10}, Reals{}, 0}                                   },
        { {0, {10}},                                                 {0, Reals{10}, Reals{}, 0}                                   },
        { State(0, {10}, 1),                                         {0, Reals{10}, Reals{}, 0}                                   },
        { State(0, {10}, 1, 0),                                      {0, Reals{10}, Reals{}, 0}                                   },
        { {0, {10, 2, 3}},                                           {0, Reals{10, 2, 3}, Reals{}, 0}                             },
        { State(0, {10, 2, 3}, 1),                                   {0, Reals{10}, Reals{2, 3}, 0}                               },
        { State(0, {10, 2, 3}, 2),                                   {0, Reals{10, 2}, Reals{3}, 0}                               },
        { State(0, {10, 2, 3}, 3),                                   {0, Reals{10, 2, 3}, Reals{}, 0}                             },
        { State(0, {10, 2, 3}, 3, 0),                                {0, Reals{10, 2, 3}, Reals{}, 0}                             },
        { {0, {10}, Reals{2}},                                       {0, Reals{10}, Reals{2}, 0}                                  },
        { {0, {10, 1}, Reals{2}},                                    {0, Reals{10, 1}, Reals{2}, 0}                               },
        { {0, {10, 1}, {2, 3}},                                      {0, Reals{10, 1}, Reals{2, 3}, 0}                            },
        { {5, {10, 1}, {2, 3}, 4},                                   {5, Reals{10, 1}, Reals{2, 3}, 4}                            },
    });

    Suite(state_msg, true).test<Case<State>>({
        { {1, {10}},                                                 {0, Reals{10}, Reals{}}                                      },
        { {0, {9}},                                                  {0, Reals{10}, Reals{}}                                      },
        { {0, {10}, Reals{1}},                                       {0, Reals{10}, Reals{}}                                      },
        { {0, {10}},                                                 {0, Reals{10}, Reals{1}}                                     },
        //+ { {0, {10}, Reals{}, 1},                                     ignore                                                       },
        //+ { State(0, {10}, 0),                                         ignore                                                       },
        //+ { State(0, {10}, 2),                                         ignore                                                       },
        //+ { {0, {}},                                                   ignore                                                       },
    });

    const String state_parse_msg = "parsing State-s";
    Suite(state_parse_msg).test<State_parse_case>({
        { {"0 (10) () "},                                            {0, {10}, Reals{}, 0}                                        },
        { {"1 (10) () 1"},                                           {1, {10}, Reals{}, 1}                                        },
        { {"0 (10) "},                                               {0, {10}, Reals{}, 0}                                        },
        { {"2 (10) 1"},                                              {2, {10}, Reals{}, 1}                                        },
        { {"0 (10) (1) "},                                           {0, {10}, Reals{1}, 0}                                       },
        { {"0 (10) (1 2 3) "},                                       {0, {10}, Reals{1, 2, 3}, 0}                                 },
        { {"2 (10) (1 2 3) 0"},                                      {2, {10}, Reals{1, 2, 3}, 0}                                 },
    });

    Suite(state_parse_msg, true).test<State_parse_case>({
        { {""},                                                      ignore                                                       },
        { {"0"},                                                     ignore                                                       },
        { {"0 10"},                                                  ignore                                                       },
        { {"0 10 0"},                                                ignore                                                       },
        { {"0 10 (0)"},                                              ignore                                                       },
        { {"0 (10) (0) ()"},                                         ignore                                                       },
        { {"(0 10 0)"},                                              ignore                                                       },
        { {"(0 10) 0"},                                              ignore                                                       },
        { {"(0 10) (0)"},                                            ignore                                                       },
        { {"( 0 (10) ())"},                                          ignore                                                       },
        { {"( 0 (10) (1))"},                                         ignore                                                       },
        { {"(0) (10)"},                                              ignore                                                       },
        { {"(0) (10) (0)"},                                          ignore                                                       },
        { {"(0) (10) (0) (0)"},                                      ignore                                                       },
        { {"() 0 (10) (0)"},                                         ignore                                                       },
        { {"() 0 (10) ()"},                                          ignore                                                       },
        { {"() 0 (10)"},                                             ignore                                                       },
        { {"()"},                                                    ignore                                                       },
        { {"() (10)"},                                               ignore                                                       },
        { {"0 ()"},                                                  ignore                                                       },
        { {"0 () ()"},                                               ignore                                                       },
        { {"0 () (5)"},                                              ignore                                                       },
        { {"0 () () ()"},                                            ignore                                                       },
        { {"0 (()) ()"},                                             ignore                                                       },
        { {"0 () (())"},                                             ignore                                                       },
        { {"0 (()) (())"},                                           ignore                                                       },
        { {"0 ((10)) ()"},                                           ignore                                                       },
        { {"0 ((10)) (0)"},                                          ignore                                                       },
        { {"0 (10) ((0))"},                                          ignore                                                       },
        { {"0 (10 ()) ()"},                                          ignore                                                       },
        { {"0 (10 (20)) ()"},                                        ignore                                                       },
        { {"0 (10) ((5))"},                                          ignore                                                       },
        { {"(0 10) (5)"},                                            ignore                                                       },
        { {"(0 10 5)"},                                              ignore                                                       },
        { {"(0 (10) (5)) 5"},                                        ignore                                                       },
        { {"(0 (10) (5)) 1 2"},                                      ignore                                                       },
        { {"(0 (10) (5)) (1) 2"},                                    ignore                                                       },
        { {"(0 (10) (5)) 1 (2)"},                                    ignore                                                       },
        { {"(0 (10) (5)) (1) (2)"},                                  ignore                                                       },
        { {"(0 (10) (5)) (1 2)"},                                    ignore                                                       },
        { {"x (10) (0)"},                                            ignore                                                       },
        { {"0 (x) (0)"},                                             ignore                                                       },
        { {"0 (1) (x)"},                                             ignore                                                       },
        { {"0 (10) (+ 1 2)"},                                        ignore                                                       },
        { {"0 (10) $(+ 1 2)"},                                       ignore                                                       },
        { {"0 (10) ($(+ 1 2))"},                                     ignore                                                       },
    });

    Suite("State as array of all real values").test<Parse_case_base<State, Reals>>({
        { {"0 (10) (1)"},                                            {10, 1, 0, 0}                                               },
        { {"2 (10)"},                                                {10, 2, 0}                                                  },
    });

    ////////////////////////////////////////////////////////////////

    const String ode_msg = "constructing of Ode-s from direct arguments";
    Suite(ode_msg).test<Cons_case<Odes>>({
        { {Keys::parse("(x y z) (a b)"), "x", "+ x 1"},                           {Keys{Ekeys{"x", "y", "z"}, Ekeys{"a", "b"}}, Key{"x"}, Phi("+ x 1")}            },
        { {Keys::parse("(x y z) (a b)"), "x", "+ 3 2 1"},                         {Keys{Ekeys{"x", "y", "z"}, Ekeys{"a", "b"}}, Key{"x"}, Phi("+ 3 2 1")}          },
        { {Keys::parse("(x y z) (a b)"), "x", "+ 1"},                             {Keys{Ekeys{"x", "y", "z"}, Ekeys{"a", "b"}}, Key{"x"}, Phi("+ 1")}              },
        { {Keys::parse("(x y z) (a b)"), "x", "+ x"},                             {Keys{Ekeys{"x", "y", "z"}, Ekeys{"a", "b"}}, Key{"x"}, Phi("+ x")}              },
        { {Keys::parse("(x y z) (a b)"), "x", "- x"},                             {Keys{Ekeys{"x", "y", "z"}, Ekeys{"a", "b"}}, Key{"x"}, Phi("- x")}              },
        { {Keys::parse("(x y z) (a b)"), "x", "- 1"},                             {Keys{Ekeys{"x", "y", "z"}, Ekeys{"a", "b"}}, Key{"x"}, Phi("- 1")}              },
        { {Keys::parse("(x y z) (a b)"), "x", "+ a 1"},                           {Keys{Ekeys{"x", "y", "z"}, Ekeys{"a", "b"}}, Key{"x"}, Phi("+ a 1")}            },
        { {Keys::parse("(x y z) (a b)"), "x", "- z b"},                           {Keys{Ekeys{"x", "y", "z"}, Ekeys{"a", "b"}}, Key{"x"}, Phi("- z b")}            },
        { {Keys::parse("(x _ta z) (&a &b)"), "_ta", "+ _ta 1"},                   {Keys{Ekeys{"x", "_ta", "z"}, Ekeys{"&a", "&b"}}, Key{"_ta"}, Phi("+ _ta 1")}    },
        { {Keys::parse("(x y z) (a b)"), "x", Fs{"+ x 1"}},                       {Keys{Ekeys{"x", "y", "z"}, Ekeys{"a", "b"}}, Key{"x"}, Phis("+ x 1")}           },
        { {Keys::parse("(x y z) (a b)"), "x", Fs{"+ x 1", "+ x 1"}},              {Keys{Ekeys{"x", "y", "z"}, Ekeys{"a", "b"}}, Key{"x"}, Phis("+ x 1", "+ x 1")} },
        { {Keys::parse("(x y z) (a b)"), "x", Fs{"+ x 1", "+ _t", "+ _tau"}},     {Keys{Ekeys{"x", "y", "z"}, Ekeys{"a", "b"}}, Key{"x"}, Phis("+ x 1", "+ _t", "+ _tau")} },
        { {Keys::parse("(x y z) (a b)"), "x", Fs{}},                              {Keys{Ekeys{"x", "y", "z"}, Ekeys{"a", "b"}}, Key{"x"}, Odes::def_formula}       },
    });

    Suite(ode_msg, true).test<Cons_case<Odes>>({
        { {Keys::parse("(x y z) (a b)"), "x", "+ x 1"},                           {Keys{Ekeys{"x", "y", "z"}, Ekeys{"a", "b"}}, Key{"x"}, Phi("+ a 1")}            },
        { {Keys::parse("(x y z) (a b)"), "x", "+ x 1"},                           {Keys{Ekeys{"x", "y", "z"}, Ekeys{"a", "b"}}, Key{"y"}, Phi("+ x 1")}            },
        { {Keys::parse("(x y z) (a b)"), "x", "1"},                               ignore                                                      },
        { {Keys::parse("(x y z) (a b)"), "x", "x"},                               ignore                                                      },
        { {Keys::parse("(x y z) (a b)"), "x", "= x 1"},                           ignore                                                      },
        { {Keys::parse("(x y z) (a b)"), "x", "= x' 1"},                          ignore                                                      },
        { {Keys::parse("(x y z) (a b)"), "x", "(- 1)"},                           ignore                                                      },
        { {Keys::parse("(x y z) (a b)"), "_t", "+ x 1"},                          ignore                                                      },
        { {Keys::parse("(x y z) (a b)"), "_t", "+ _t 1"},                         ignore                                                      },
        { {Keys::parse("(x y z) (a b)"), "_tau", "+ _tau 1"},                     ignore                                                      },
        { {Keys::parse("(x y z) (a b)"), "x'", "+ x 1"},                          ignore                                                      },
        { {Keys::parse("(x y z) (a b)"), "x'", "+ x' 1"},                         ignore                                                      },
        { {Keys::parse("(x y z) (a b)"), "x", "+ x' 1"},                          ignore                                                      },
        { {Keys::parse("(x y z) (a b)"), "x", "x 1"},                             ignore                                                      },
        { {Keys::parse("(x y z) (a b)"), "x", "(+) x 1"},                         ignore                                                      },
        { {Keys::parse("(x y z) (a b)"), "x", "(+ x) x 1"},                       ignore                                                      },
        { {Keys::parse("(x y z) (a b)"), "x", "(+ x 1) 5"},                       ignore                                                      },
        { {Keys::parse("(x y z) (a b)"), "x", Fs{"(+) x 1"}},                     ignore                                                      },
        { {Keys::parse("(x y z) (a b)"), "x", ""},                                ignore                                                      },
        { {Keys::parse("(x y z) (a b)"), "x", "()"},                              ignore                                                      },
        { {Keys::parse("(x y z) (a b)"), "x", "((+ x 1))"},                       {Keys{Ekeys{"x", "y", "z"}, Ekeys{"a", "b"}}, Key{"x"}, Phis("+ x 1")} },
        { {Keys::parse("(x y z) (a b)"), "x", Fs{""}},                            ignore                                                      },
        { {Keys::parse("(x y z) (a b)"), "x", Formula{}},                         ignore                                                      },
        { {Keys::parse("(x y z) (a b)"), "x", Fs{"+ x 1", ""}},                   ignore                                                      },
        { {Keys::parse("(x y z) (a b)"), "x", Fs{"+ x 1", "+ x 1", ""}},          ignore                                                      },
        { {Keys::parse("(x y z) (a b)"), "x", "+ c 1"},                           ignore                                                      },
        { {Keys::parse("(x y z) (a b)"), "x", "+ x 1"},                           {Keys{Ekeys{"x", "y", "z"}, Ekeys{"a", "b"}}, Key{"x"}, Phis("+ x 1")} },
    });

    ////////////////////////////////////////////////////////////////

    const String ginv_msg = "constructing of Invariants-s from direct arguments";
    Suite(ginv_msg).test<Cons_case<Invariants>>({
        { {Keys::parse("(x y z) (a b)"), "> x (+ y z)"},                      {Keys{Ekeys{"x", "y", "z"}, Ekeys{"a", "b"}}, Phi("> x (+ y z)")}                },
        { {Keys::parse("(x y z) (a b)"), "> x (+ y a)"},                      {Keys{Ekeys{"x", "y", "z"}, Ekeys{"a", "b"}}, Phi("> x (+ y a)")}                },
        { {Keys::parse("(x y z) (a b)"), "= x y z"},                          {Keys{Ekeys{"x", "y", "z"}, Ekeys{"a", "b"}}, Phi("= x y z")}                    },
        { {Keys::parse("(x y z) (a b)"), {"> x 0", "", "", "<= x 0", "> y x"}}, {Keys{Ekeys{"x", "y", "z"}, Ekeys{"a", "b"}}, Fs{Phi("> x 0"), "", "", Phi("<= x 0"), Phi("> y x")}}  },
        { {Keys::parse("(x y z) (a b)"), Fs{}},                               {Keys{Ekeys{"x", "y", "z"}, Ekeys{"a", "b"}}, Phis()}      },
        { {Keys::parse("(x y z) (a b)"), Formula{}},                          {Keys{Ekeys{"x", "y", "z"}, Ekeys{"a", "b"}}, Phis()}      },
        { {Keys::parse("(x y z) (a b)"), ""},                                 {Keys{Ekeys{"x", "y", "z"}, Ekeys{"a", "b"}}, Phis()}      },
        { {Keys::parse("(x y z) (a b)"), Fs{}},                               {Keys{Ekeys{"x", "y", "z"}, Ekeys{"a", "b"}}, Invariants::def_formula}  },
        { Invariants{Keys::parse("(x y z) (a b)")},                           ignore                                                     },
        { {},                                                                 ignore                                                     },
        { {Keys::parse("(x y z) (a b)"), "and (or (> x 0) (<= y 10)) (not (= z b))"},  {Keys{Ekeys{"x", "y", "z"}, Ekeys{"a", "b"}}, Phi("and (or (> x 0) (<= y 10)) (not (= z b))")}  },
        { {Keys::parse("(x y z) (a b)"), Fs{"or (and (> z 0))", "and (> x 0) (<= y 10)"}}, {Keys{Ekeys{"x", "y", "z"}, Ekeys{"a", "b"}}, Phis("or (and (> z 0))", "and (> x 0) (<= y 10)")}  },
        { {Keys::parse("(x y z) (a b)"), Fs{"> (+ a z) 0", "= (* 1 0) (* (/ x y) 0)"}}, ignore                                           },
        { {Keys::parse("(x y z) (a b)"), "> a x"},                            ignore                                                     },
        { {Keys::parse("(x y) (a b)"), "not (and (> x 0) (> (+ x y) 0))"},    {Keys{Ekeys{"x", "y"}, Ekeys{"a", "b"}}, Phi("not (and (> x 0) (> (+ x y) 0))")} },
    });

    Suite(ginv_msg, true).test<Cons_case<Invariants>>({
        { {Keys::parse("(x y z) (a b)"), "(>) x 0"},                          ignore                                                     },
        { {Keys::parse("(x y z) (a b)"), {"> x 0", "()", "<= x 0"}},          ignore                                                     },
        { {Keys::parse("(x y z) (a b)"), Fs{"()", "<= x 0"}},                 ignore                                                     },
        { {Keys::parse("(x y z) (a b)"), Fs{"> x 0", "()"}},                  ignore                                                     },
        { {Keys::parse("(x y z) (a b)"), Fs{"()"}},                           ignore                                                     },
        { {Keys::parse("(x y z) (a b)"), "()"},                               ignore                                                     },
        { {Keys::parse("(x y z) (a b)"), "0"},                                ignore                                                     },
        { {Keys::parse("(x y z) (a b)"), "= G 2"},                            ignore                                                     },
        { {Keys::parse("(x y z) (a b)"), "= x' 2"},                           ignore                                                     },
        { {Keys::parse("(x y z) (a b)"), "+ x 0"},                            ignore                                                     },
        { {Keys::parse("(x y z) (a b)"), "@ x 1"},                            ignore                                                     },
        { {Keys::parse("(x y z) (a b)"), "= x"},                              ignore                                                     },
        { {Keys::parse("(x y z) (a b)"), Fs{"<= x 0", "> y 0"}},              {Keys{Ekeys{"x", "y", "z"}, Ekeys{"a", "b"}}, Phis("> y 0", "<= x 0")} },
        { {Keys::parse("(x y z) (a b)"), "(> a b) (= x 0)"},                  ignore                                                     },
        { {Keys::parse("(x y z) (a b)"), "(and (> a b) (= x 0)) (and (= x 0))"}, ignore                                                  },
        { {Keys::parse("(x y z) (a b)"), "(and (or (> x 0) (<= y 10)) (not (= a b)))"},  ignore                                          },
        { {Keys::parse("(x y z) (a b)"), "> a b"},                            ignore                                                     },
        { {Keys::parse("(x y z) (a b)"), "= 1 0"},                            ignore                                                     },
        { {Keys::parse("(x y z) (a b)"), Fs{"> (+ a b) 0", "= (* 1 0) (* (/ x y) 0)"}}, ignore                                           },
    });

    ////////////////////////////////////////////////////////////////

    const String linv_msg = "constructing of Local_invariants-s from direct arguments";
    Suite(linv_msg).test<Cons_case<Local_invariants>>({
        { {Keys::parse("(x y z) (a b)"), "x", "> x 0"},                       {Keys{Ekeys{"x", "y", "z"}, Ekeys{"a", "b"}}, Key{"x"}, Phi("> x 0")}                     },
        { {Keys::parse("(x y z) (a b)"), "x", "> x a"},                       {Keys{Ekeys{"x", "y", "z"}, Ekeys{"a", "b"}}, Key{"x"}, Phi("> x a")}                     },
        { {Keys::parse("(x y z) (a b)"), "x", "> x y"},                       {Keys{Ekeys{"x", "y", "z"}, Ekeys{"a", "b"}}, Key{"x"}, Phi("> x y")}                     },
        { {Keys::parse("(x y z) (a b)"), "x", "> x (+ y z)"},                 {Keys{Ekeys{"x", "y", "z"}, Ekeys{"a", "b"}}, Key{"x"}, Phi("> x (+ y z)")}               },
        { {Keys::parse("(x y z) (a b)"), "x", "> x (+ y a)"},                 {Keys{Ekeys{"x", "y", "z"}, Ekeys{"a", "b"}}, Key{"x"}, Phi("> x (+ y a)")}               },
        { {Keys::parse("(x y z) (a b)"), "x", {"> x 0", "", "", "<= x 0"}},   {Keys{Ekeys{"x", "y", "z"}, Ekeys{"a", "b"}}, Key{"x"}, Fs{Phi("> x 0"), "", "", Phi("<= x 0")}}  },
        { {Keys::parse("(x y z) (a b)"), "x", {"> x 0", "", "", "<= x 0"}},   {Keys{Ekeys{"x", "y", "z"}, Ekeys{"a", "b"}}, Key{"x"}, Fs{Phi("> x 0"), "", "", Phi("<= x 0")}}  },
        { {Keys::parse("(x y z) (a b)"), "x", Fs{}},                          ignore                                                                                    },
        { {Keys::parse("(x y z) (a b)"), "y", Formula{}},                     ignore                                                                                    },
        { {Keys::parse("(x y z) (a b)"), "z", ""},                            ignore                                                                                    },
        { {Keys::parse("(x y z) (a b)"), "x", ">= x (+ y 2)"},                ignore                                                                                    },
        { {Keys::parse("(x y z) (a b)"), "x", "= x (^ y 2)"},                 ignore                                                                                    },
        { {Keys::parse("(x y z) (a b)"), "y", "< y y"},                       ignore                                                                                    },
        { Local_invariants{Keys::parse("(x y z) (a b)"), "x"},                {Keys{Ekeys{"x", "y", "z"}, Ekeys{"a", "b"}}, Key{"x"}, Phis()}                           },
        { {Keys::parse("(x y z) (a b)"), "x", "and (> x 0) (<= x 10)"},       {Keys{Ekeys{"x", "y", "z"}, Ekeys{"a", "b"}}, Key{"x"}, Phi("and (> x 0) (<= x 10)")}     },
        { {Keys::parse("(x y z) (a b)"), "x", "or (> x 0) (<= x 10)"},        ignore                                                                                    },
        { {Keys::parse("(x y z) (a b)"), "x", "not (> x 0)"},                 ignore                                                                                    },
        { {Keys::parse("(x y z) (a b)"), "x", "and (or (> x 0) (not (> x 10)))"},   ignore                                                                              },
        { {Keys::parse("(x y z) (a b)"), "x", Fs{"> x 0", "and (> x 0) (<= x 10)"}}, {Keys{Ekeys{"x", "y", "z"}, Ekeys{"a", "b"}}, Key{"x"}, Phis("> x 0", "and (> x 0) (<= x 10)")}  },
        { {Keys::parse("(x y z) (a b)"), "x", Fs{"and (> x 0)", "and (> x 0) (<= x 10)"}},     ignore                                                                   },
        { {Keys::parse("(x y z) (a b)"), "x", Fs{"and (and (> x 0))", "and (> x 0) (<= x 10)"}}, ignore                                                                 },
        { {Keys::parse("(x y z) (a b)"), "x", Fs{"not (and (> x 0))", "and (> x 0) (<= x 10)"}}, ignore                                                                 },
        { {Keys::parse("(x y z) (a b)"), "x", "and (> x 0) (and (> x 0) (<= x 10))"}, ignore                                                                            },
    });

    Suite(linv_msg, true).test<Cons_case<Local_invariants>>({
        { {Keys::parse("(x y z) (a b)"), "x", "(>) x 0"},                     ignore                                                          },
        { {Keys::parse("(x y z) (a b)"), "x", "() x 0"},                      ignore                                                          },
        { {Keys::parse("(x y z) (a b)"), "x", Fs{"(>) x 0"}},                 ignore                                                          },
        { {Keys::parse("(x y z) (a b)"), "a", "> a 0"},                       ignore                                                          },
        { {Keys::parse("(x y z) (a b)"), "x", "> a 0"},                       ignore                                                          },
        { {Keys::parse("(x y z) (a b)"), "x", "> y 0"},                       ignore                                                          },
        { {Keys::parse("(x y z) (a b)"), "x", "> a x"},                       ignore                                                          },
        { {Keys::parse("(x y z) (a b)"), "", "> x 0"},                        ignore                                                          },
        { Local_invariants{Keys::parse("(x y z) (a b)")},                     ignore                                                          },
        { {},                                                                 ignore                                                          },
        { {Keys::parse("(x y z) (a b)"), "(x)", "> x 0"},                     ignore                                                          },
        { {Keys::parse("(x y z) (a b)"), "x'", "= x' 2"},                     ignore                                                          },
        { {Keys::parse("(x y z) (a b)"), "x y", "> x 0"},                     ignore                                                          },
        { {Keys::parse("(x y z) (a b)"), "x", "+ x 1"},                       ignore                                                          },
        { {Keys::parse("(x y z) (a b)"), "x", "@ x 1"},                       ignore                                                          },
        { {Keys::parse("(x y z) (a b)"), "x", "> x"},                         ignore                                                          },
        { {Keys::parse("(x y z) (a b)"), "x", "> x 1 2"},                     ignore                                                          },
        { {Keys::parse("(x y z) (a b)"), "x", "x"},                           ignore                                                          },
        { {Keys::parse("(x y z) (a b)"), "x", Fs{"<= x 0", "> x 0"}},         {Keys{Ekeys{"x", "y", "z"}, Ekeys{"a", "b"}}, Key{"x"}, Phis("> x 0", "<= x 0")}          },
        { {Keys::parse("(x y z) (a b)"), "x", "(> x 0) (> x 1)"},             ignore                                                          },
        { {Keys::parse("(x y z) (a b)"), "x", "and (> x 0) (> y 1)"},         ignore                                                          },
        { {Keys::parse("(x y z) (a b)"), "x", "(and (> x 0)) (and (> x 0) (<= x 10))"}, ignore                                                },
    });

    ////////////////////////////////////////////////////////////////

    const String timer_msg = "constructing of Timer-s from direct arguments";
    Suite(timer_msg).test<Case<Timer>>({
        { Timer{Keys::parse("(x y z) (a b)")},                                    {Keys{Ekeys{"x", "y", "z"}, Ekeys{"a", "b"}}, Phis(), Phis()}               },
        { {Keys::parse("(x y z) (a b)"), Fs{}, Fs{"< _t 10"}},                    ignore                                                                      },
        { {Keys::parse("(x y z) (a b)"), Fs{"< _tau 10"}},                        ignore                                                                      },
        { {Keys::parse("(x y z) (a b)"), Fs{"< _tau 10"}, Fs{}},                  ignore                                                                      },
        { {Keys::parse("(x y z) (a b)"), Fs{">= _tau 10"}, Fs{"> _t 20"}},        ignore                                                                      },
        { {Keys::parse("(x y z) (a b)"), Fs{"< _tau 10", "< _tau 5"}, Fs{"> _t 20", "= _t 5"}}, ignore                                                        },
        { {Keys::parse("(x y z) (a b)"), Fs{"< _tau _t"}},                        ignore                                                                      },
        { {Keys::parse("(x y z) (a b)"), Fs{"< _tau _tau"}},                      ignore                                                                      },
        { {Keys::parse("(x y z) (a b)"), Fs{"< _tau (- _t _tau)"}},               ignore                                                                      },
        { {Keys::parse("(x y z) (a b)"), Fs{"< _tau (- _t x)"}},                  ignore                                                                      },
        { {Keys::parse("(x y z) (a b)"), Fs{"< _tau (+ _t x a)"}},                ignore                                                                      },
        { {Keys::parse("(x y z) (a b)"), ""},                                     {Keys{Ekeys{"x", "y", "z"}, Ekeys{"a", "b"}}, Phis(), Phis()}               },
        { {Keys::parse("(x y z) (a b)"), "", ""},                                 {Keys{Ekeys{"x", "y", "z"}, Ekeys{"a", "b"}}, Phis(), Phis()}               },
        { {Keys::parse("(x y z) (a b)"), "< _tau 10"},                            {Keys{Ekeys{"x", "y", "z"}, Ekeys{"a", "b"}}, Phi("< _tau 10"), Formula{}}  },
        { {Keys::parse("(x y z) (a b)"), "< _tau 10", "< _t 5"},                  {Keys{Ekeys{"x", "y", "z"}, Ekeys{"a", "b"}}, Phi("< _tau 10"), Phi("< _t 5")} },
        { {Keys::parse("(x y z) (a b)"), "", "< _t 5"},                           {Keys{Ekeys{"x", "y", "z"}, Ekeys{"a", "b"}}, Formula{}, Phi("< _t 5")}     },
    });

    Suite(timer_msg, true).test<Case<Timer>>({
        //+ { {Keys::parse("(x y z) (a b)"), Fs{}, Fs{"< _tau 10"}},                  ignore                                                                                    },
        //+ { {Keys::parse("(x y z) (a b)"), Fs{"< _t 10"}, Fs{}},                    ignore                                                                                    },
        //+ { {Keys::parse("(x y z) (a b)"), Fs{}, Fs{"> _t 0", "< _tau 10"}},        ignore                                                                                    },
        //+ { {Keys::parse("(x y z) (a b)"), Fs{"> _t 0", "< _tau 10"}},              ignore                                                                                    },
    });
    ////////////////////////////////////////////////////////////////

    using Afs  = All_formulas;
    using Afsm  = All_formulas_map;
    const String flow_msg = "constructing of Flow-s from direct arguments";
    Suite(flow_msg).test<Case<Flow>>({
        { {Keys::parse("x"), Fs{"+ 2 1", "- x 1"}, Fs{}},                     {Keys{Ekeys{"x"}, Ekeys{}}, All_formulas{Phis("+ 2 1", "- x 1")}, All_formulas_map{}}               },
        { {Keys::parse("(x) (a b)"), Fs{"+ 2 1", "- x 1", "+ a b"}, Fs{}},    {Keys{Ekeys{"x"}, Ekeys{"a", "b"}}, All_formulas{Phis("+ 2 1", "- x 1", "+ a b")}, All_formulas_map{}}               },
        { {Keys::parse("(x) (a b)"), Fs{"+ x 1", "- x 1", "+ a b"}, Afsm{}}, {Keys{Ekeys{"x"}, Ekeys{"a", "b"}}, All_formulas{Phis("+ x 1", "- x 1", "+ a b")}, All_formulas_map{}}               },
        { {Keys::parse("(x y) (a b)"), Afs{{"+ x 1", "- x 1"}, {"+ a b"}}, Fs{}},  {Keys{Ekeys{"x", "y"}, Ekeys{"a", "b"}}, All_formulas{Phis("+ x 1", "- x 1"), Phis("+ a b")}, All_formulas_map{}}  },
        { {Keys::parse("(x y) (a b)"), Afs{{"+ x 1", "- x 1"}, {"+ a b"}}, {{"y", Fs{"> y 0"}}}},  {Keys{Ekeys{"x", "y"}, Ekeys{"a", "b"}}, All_formulas{Phis("+ x 1", "- x 1"), Phis("+ a b")}, All_formulas_map{{"y", Phis("> y 0")}}}  },
        { {Keys::parse("(x y) (a b)"), Afs{{"+ x 1", "- x 1"}, {"+ a b"}}, {{"y", Fs{"> y 0"}}, {"x", Fs{"> x y", "< x 5"}}}},  {Keys{Ekeys{"x", "y"}, Ekeys{"a", "b"}}, All_formulas{Phis("+ x 1", "- x 1"), Phis("+ a b")}, All_formulas_map{{"x", Phis("> x y", "< x 5")}, {"y", Phis("> y 0")}}}  },
        { {Keys::parse("(x y z) (a b)"), Afs{{"+ x 1"}, {"- x 1"}, {"+ a b"}}, Fs{"> y 0", "> x 0", "< z _t"}},  {Keys{Ekeys{"x", "y", "z"}, Ekeys{"a", "b"}}, All_formulas{Phis("+ x 1"), Phis("- x 1"), Phis("+ a b")}, All_formulas_map{{"x", Phis("> x 0")}, {"y", Phis("> y 0")}, {"z", Phis("< z _t")}}}  },
        { {Keys::parse("(x) (a b)"), Fs{"+ x 1", "- x 1", "+ a b"}, Fs{"> _t 0", "> _tau 0"}}, {Keys{Ekeys{"x"}, Ekeys{"a", "b"}}, All_formulas{Phis("+ x 1", "- x 1", "+ a b")}, All_formulas_map{{"_tau", {"> _tau 0"}}, {"_t", {"> _t 0"}}}}               },
        { {Keys::parse("(x) (a b)"), Fs{"+ x 1", "- x 1", "+ a b"}, Fs{"= _t _tau", "> _tau _t"}}, {Keys{Ekeys{"x"}, Ekeys{"a", "b"}}, All_formulas{Phis("+ x 1", "- x 1", "+ a b")}, All_formulas_map{{"_tau", {"> _tau _t"}}, {"_t", {"= _t _tau"}}}}               },
        { {Keys::parse("(x y z) (a b)"), Afs{{"+ x 1", "* x _tau"}, {"- x 1"}, {"+ a b", "- a b"}}, Fs{"> _t (- (* x y z) (* a b))", "> x 0", "> y 0", "< x 0", "= _tau 0", "< z _t", "= y _tau", "< _t 0"}},  {Keys{Ekeys{"x", "y", "z"}, Ekeys{"a", "b"}}, All_formulas{Phis("+ x 1", "* x _tau"), Phis("- x 1"), Phis("+ a b", "- a b")}, All_formulas_map{{"x", Phis("> x 0", "< x 0")}, {"y", Phis("> y 0", "= y _tau")}, {"z", Phis("< z _t")}, {"_tau", Phis("= _tau 0")}, {"_t", Phis("> _t (- (* x y z) (* a b))", "< _t 0")}}}  },
        { {Keys::parse("(x y z) (a b)"), Afs{{"+ x 1", "* x _tau"}, {"- x 1"}, {"+ a b", "- a b"}}, Fs{"and (> y 0) (= y _tau)", "> _t (- (* x y z) (* a b))", "= _tau 0", "< z _t", "< _t 0", "and (> x 0) (not (> x 0))"}},  {Keys{Ekeys{"x", "y", "z"}, Ekeys{"a", "b"}}, All_formulas{Phis("+ x 1", "* x _tau"), Phis("- x 1"), Phis("+ a b", "- a b")}, All_formulas_map{{"x", Phis("and (> x 0) (not (> x 0))")}, {"y", Phis("and (> y 0) (= y _tau)")}, {"z", Phis("< z _t")}, {"_tau", Phis("= _tau 0")}, {"_t", Phis("> _t (- (* x y z) (* a b))", "< _t 0")}}}  },
        { {Keys::parse("(x y) (a b)"), Afs{{"+ x 1", "- x 1"}, {"+ a b"}}, {{"y", Fs{}}}},  {Keys{Ekeys{"x", "y"}, Ekeys{"a", "b"}}, All_formulas{Phis("+ x 1", "- x 1"), Phis("+ a b")}, All_formulas_map{{"y", {}}}}  },
        { {Keys::parse("(x) (a b)"), Fs{"+ 2 1", "- x 1", "+ a b"}, Fs{"> a x"}},    {Keys{Ekeys{"x"}, Ekeys{"a", "b"}}, All_formulas{Phis("+ 2 1", "- x 1", "+ a b")}, All_formulas_map{{"", Phis("> a x")}}}               },
        { {Keys::parse("(x) (a b)"), Fs{"+ 2 1", "- x 1", "+ a b"}, Fs{"> (+ a x) 0"}},    {Keys{Ekeys{"x"}, Ekeys{"a", "b"}}, All_formulas{Phis("+ 2 1", "- x 1", "+ a b")}, All_formulas_map{{"", Phis("> (+ a x) 0")}}}   },
        { {Keys::parse("(x) (a b)"), Fs{"+ 2 1", "- x 1", "+ a b"}, Fs{"> 0 x"}},    {Keys{Ekeys{"x"}, Ekeys{"a", "b"}}, All_formulas{Phis("+ 2 1", "- x 1", "+ a b")}, All_formulas_map{{"", Phis("> 0 x")}}}               },
        { {Keys::parse("(x) (a b)"), Fs{"+ 2 1", "- x 1", "+ a b"}, Fs{"> x 0", "> 0 x", "> a x", "> (+ a x) 0"}},    {Keys{Ekeys{"x"}, Ekeys{"a", "b"}}, All_formulas{Phis("+ 2 1", "- x 1", "+ a b")}, All_formulas_map{{"x", Phis("> x 0")}, {"", Phis("> 0 x", "> a x", "> (+ a x) 0")}}}  },
        { {Keys::parse("(x) (a)"), Fs{"+ 2 1"}, Fs{"= x a", "= a x", "= x _t", "= _t x", "= a _t", "= _t a"}},    {Keys{Ekeys{"x"}, Ekeys{"a"}}, All_formulas{Phis("+ 2 1")}, All_formulas_map{{"x", Phis("= x a", "= x _t")}, {"_t", Phis("= _t x", "= _t a")}, {"", Phis("= a x", "= a _t")}}}  },
        { {Keys::parse("(x) (a)"), Fs{"+ 2 1"}, Fs{"= x a", "= (- _t) x", "= x _t", "= _t x", "= (- x) _t", "= _t a"}},    {Keys{Ekeys{"x"}, Ekeys{"a"}}, All_formulas{Phis("+ 2 1")}, All_formulas_map{{"x", {"= x a", "= x _t"}}, {"_t", {"= _t x", "= _t a"}}, {"", Phis("= (- _t) x", "= (- x) _t")}}}  },
        { {Keys::parse("(x y) (a b)"), Afs{{"+ x 1", "- x 1"}, {"+ a b"}}, {{"", Fs{"< (+ a b) x"}}, {"y", Fs{"= y a"}}, {"_t", Fs{"< _t 10"}}}},  {Keys{Ekeys{"x", "y"}, Ekeys{"a", "b"}}, All_formulas{Phis("+ x 1", "- x 1"), Phis("+ a b")}, All_formulas_map{{"y", Phis("= y a")}, {"_t", Phis("< _t 10")}, {"", Phis("< (+ a b) x")}}}  },
        { {Keys::parse("(x y) (a b)"), Afs{{"+ x 1", "- x 1"}, {"+ a b"}}, {{"", Fs{"< x (+ a b)"}}}},  {Keys{Ekeys{"x", "y"}, Ekeys{"a", "b"}}, All_formulas{Phis("+ x 1", "- x 1"), Phis("+ a b")}, All_formulas_map{{"", Phis("< x (+ a b)")}}}  },
        { {Keys::parse("(x y) (a b)"), Afs{{"+ x 1", "- x 1"}, {"+ a b"}}, Fs{"and (< x (+ a b)) (> y (- a b))"}},  {Keys{Ekeys{"x", "y"}, Ekeys{"a", "b"}}, All_formulas{Phis("+ x 1", "- x 1"), Phis("+ a b")}, All_formulas_map{{"", Phis("and (< x (+ a b)) (> y (- a b))")}}}  },
        { {Keys::parse("(x y) (a b)"), Afs{{"+ x 1", "- x 1"}, {"+ a b"}}, Fs{"and (< a x) (> b y)"}},  {Keys{Ekeys{"x", "y"}, Ekeys{"a", "b"}}, All_formulas{Phis("+ x 1", "- x 1"), Phis("+ a b")}, All_formulas_map{{"", Phis("and (< a x) (> b y)")}}}  },
        { {Keys::parse("(x y) (a b)"), Afs{{"+ x 1", "- x 1"}, {"+ a b"}}, Fs{"and (< a (+ x y)) (> b (- x y))"}},  {Keys{Ekeys{"x", "y"}, Ekeys{"a", "b"}}, All_formulas{Phis("+ x 1", "- x 1"), Phis("+ a b")}, All_formulas_map{{"", Phis("and (< a (+ x y)) (> b (- x y))")}}}  },
        { {Keys::parse("(x y) (a b)"), Afs{{"+ x 1", "- x 1"}, {"+ a b"}}, Fs{"< x (+ a b)", "> a (- y b)"}},  {Keys{Ekeys{"x", "y"}, Ekeys{"a", "b"}}, All_formulas{Phis("+ x 1", "- x 1"), Phis("+ a b")}, All_formulas_map{{"x", Phis("< x (+ a b)")}, {"", Phis("> a (- y b)")}}}  },
        { {Keys::parse("(x y) (a b)"), Afs{{"+ x 1", "- x 1"}, {"+ a b"}}, Fs{"< a (+ x y)", "> b (- x y)"}},  {Keys{Ekeys{"x", "y"}, Ekeys{"a", "b"}}, All_formulas{Phis("+ x 1", "- x 1"), Phis("+ a b")}, All_formulas_map{{"", Phis("< a (+ x y)", "> b (- x y)")}}}  },
        { {Keys::parse("(x y) (a b)"), Afs{{"+ x 1", "- x 1"}, {"+ a b"}}, Fs{"and (< (+ x 1) 0) (not (> y b))"}},  {Keys{Ekeys{"x", "y"}, Ekeys{"a", "b"}}, All_formulas{Phis("+ x 1", "- x 1"), Phis("+ a b")}, All_formulas_map{{"", Phis("and (< (+ x 1) 0) (not (> y b))")}}}  },
        { {Keys::parse("(x y) (a b)"), Afs{{"+ x 1", "- x 1"}, {"+ a b"}}, Fs{"and (< (+ x 1) 0) (not (> (- x y) b))"}},  {Keys{Ekeys{"x", "y"}, Ekeys{"a", "b"}}, All_formulas{Phis("+ x 1", "- x 1"), Phis("+ a b")}, All_formulas_map{{"", Phis("and (< (+ x 1) 0) (not (> (- x y) b))")}}}  },
        { {Keys::parse("(x y) (a b)"), Afs{{"+ x 1"}, {"+ a b"}}, Fs{"not (and (> x 0) (> (+ x y) 0))"}},  {Keys{Ekeys{"x", "y"}, Ekeys{"a", "b"}}, All_formulas{Phis("+ x 1"), Phis("+ a b")}, All_formulas_map{{"", Phis("not (and (> x 0) (> (+ x y) 0))")}}}       },
    });

    Suite(flow_msg, true).test<Case<Flow>>({
        { {Keys::parse("(x) (a b)"), Fs{"+ x 1", "- x 1", "+ a b"}, Fs{}},    {Keys{Ekeys{"x"}, Ekeys{"a", "b"}}, All_formulas{Phis("- x 1", "+ x 1", "+ a b")}, All_formulas_map{}}               },
        { {Keys::parse("(x y z) (a b)"), Afs{{"+ x 1", "* x _tau"}, {"- x 1"}, {"+ a b", "- a b"}}, Fs{"> _t (- (* x y z) (* a b))", "> x 0", "> y 0", "< x 0", "= _tau 0", "< z _t", "= y _tau", "< _t 0"}},  {Keys{Ekeys{"x", "y", "z"}, Ekeys{"a", "b"}}, All_formulas{Phis("+ x 1", "* x _tau"), Phis("- x 1"), Phis("+ a b", "- a b")}, All_formulas_map{{"x", Phis("> x 0", "< x 0")}, {"y", Phis("> y 0", "= y _tau")}, {"z", Phis("< z _t")}, {"_tau", Phis("= _tau 0")}, {"_t", Phis("< _t 0", "> _t (- (* x y z) (* a b))")}}}  },
        { {Keys::parse("(x y z) (a b)"), Afs{{"+ x 1", "* x _tau"}, {"- x 1"}, {"+ a b", "- a b"}}, Fs{"> _t (- (* x y z) (* a b))", "> x 0", "> y 0", "< x 0", "= _tau 0", "< z _t", "= y _tau", "< _t 0"}},  {Keys{Ekeys{"x", "y", "z"}, Ekeys{"a", "b"}}, All_formulas{Phis("+ x 1", "* x _tau"), Phis("- x 1"), Phis("+ a b", "- a b")}, All_formulas_map{{"x", Phis("< x 0", "> x 0")}, {"y", Phis("> y 0", "= y _tau")}, {"z", Phis("< z _t")}, {"_tau", Phis("= _tau 0")}, {"_t", Phis("> _t (- (* x y z) (* a b))", "< _t 0")}}}  },
        //+ { {Keys::parse("(x y) (a b)"), Afs{{"+ x 1", "- x 1"}}, Fs{}},   ignore  },
        //+ { {Keys::parse("(x y) (a b)"), Afs{{"+ x 1", "- x 1"}, {}}, Fs{}},  ignore  },
        //+ { {Keys::parse("(x y) (a b)"), Afs{}, Fs{}},                         ignore  },
        //+ { {Keys::parse("(x y) (a b)"), Fs{}, Fs{}},                           ignore  },
        //+ { {Keys::parse("(x) (a b)"), Fs{}, Fs{}},                             ignore  },
        //+ { {Keys::parse("(x y z) (a b)"), Afs{{"+ x 1"}, {"- x 1"}, {"+ a b"}}, Fs{"> y 0", "", "< z _t"}},  ignore  },
        //+ { {Keys::parse("(x y z) (a b)"), Afs{{"+ x 1"}, {"- x 1"}, {"+ a b"}}, Fs{"> y 0", {}, "< z _t"}},  ignore  },
        //+ { {Keys::parse("(x y) (a b)"), Afs{{"+ x 1", "- x 1"}, {"+ a b"}}, {{"y", Fs{"> x 0"}}}},  ignore  },
        //+ { {Keys::parse("(x y) (a b)"), Afs{{"+ x 1", "- x 1"}, {"+ a b"}}, {{"y", Fs{"> _t 0"}}}},  ignore  },
        //+ { {Keys::parse("(x y) (a b)"), Afs{{"+ x 1", "- x 1"}, {"+ a b"}}, {{"y", Fs{"- y 1"}}}},  ignore  },
        //+ { {Keys::parse("(x) (a b)"), Fs{"+ 2 1", "- x 1", "+ a b"}, Fs{"> x (= x a)"}},    ignore               },
        //+ { {Keys::parse("(x) (a b)"), Fs{"+ 2 1", "- x 1", "+ a b"}, Fs{"> (= x a) 0"}},    ignore               },
        //+ { {Keys::parse("(x) (a b)"), Fs{"+ 2 1", "- x 1", "+ a b"}, Fs{"= (= x 0) (= x 0)"}},    ignore               },
        //+ { {Keys::parse("(x) (a b)"), Fs{"+ 2 1", "- x 1", "+ a b"}, Fs{"> 0 y"}},    ignore               },
        //+ { {Keys::parse("(x y) (a b)"), Afs{{"+ x 1", "- x 1"}, {"+ a b"}}, {{"y", Fs{"= a y"}}}},        ignore  },
        //+ { {Keys::parse("(x y) (a b)"), Afs{{"+ x 1", "- x 1"}, {"+ a b"}}, {{"_t", Fs{"< _tau 10"}}}},   ignore  },
        //+ { {Keys::parse("(x y) (a b)"), Afs{{"+ x 1", "- x 1"}, {"+ a b"}}, {{"y", Fs{"= (+ y 1) a"}}}},  ignore  },
        //+ { {Keys::parse("(x y) (a b)"), Afs{{"+ x 1", "- x 1"}, {"+ a b"}}, {{"a", Fs{"= a y"}}}},        ignore  },
        //+ { {Keys::parse("(x) (a b)"), Fs{"+ 2 1", "- x 1", "+ a b"}, Fs{"> a b"}},    ignore         },
        //+ { {Keys::parse("(x) (a b)"), Fs{"+ 2 1", "- x 1", "+ a b"}, Fs{"> (+ a b) 0"}},    ignore   },
        //+ { {Keys::parse("(x y) (a b)"), Afs{{"+ x 1", "- x 1"}, {"+ a b"}}, Fs{"and (< a (+ b 1)) (> b (- x y))"}},  ignore  },
        //+ { {Keys::parse("(x y) (a b)"), Afs{{"+ x 1", "- x 1"}, {"+ a b"}}, Fs{"< a (+ b 1)", "> b (- x y)"}},  ignore  },
    });
    auto flow_parse_msg_f = [](String str = ""){
        return "parsing Flow-s"s + str;
    };
    const String flow_parse_msg = flow_parse_msg_f(" from single List");
    Suite(flow_parse_msg).test<Flow_parse_case>(flow_parse_valid_data<Flow_parse_case>());

    Suite(flow_parse_msg, true).test<Flow_parse_case>(flow_parse_invalid_data<Flow_parse_case>());

    auto flow_parser_msg_f = [&](String str = ""){
        return flow_parse_msg_f(move(str)) + " with Base";
    };
    const String flow_parser_single_msg = flow_parser_msg_f(" from single List");
    const String flow_parser_msg = flow_parser_msg_f();
    Suite(flow_parser_single_msg).test<Flow_parser_case>(flow_parse_valid_data<Flow_parser_case>());
    Suite(flow_parser_msg).test<Flow_parser_case>({
        { {"(x) (a b)", "(= x' (+ x 1))", ""},                                {Keys{Ekeys{"x"}, Ekeys{"a", "b"}}, All_formulas{Phis("+ x 1")}, All_formulas_map{}}                                                                         },
        { {"(x) (a b)", "(= x' (+ x 1))", "()"},                              {Keys{Ekeys{"x"}, Ekeys{"a", "b"}}, All_formulas{Phis("+ x 1")}, All_formulas_map{}}                                                                         },
        { {"(x) (a b)", "= x' (+ x 1)", ""},                                  {Keys{Ekeys{"x"}, Ekeys{"a", "b"}}, All_formulas{Phis("+ x 1")}, All_formulas_map{}}                                                                         },
        { {"(x) (a b)", "(= x' (+ x 1)) ()"},                                 {Keys{Ekeys{"x"}, Ekeys{"a", "b"}}, All_formulas{Phis("+ x 1")}, All_formulas_map{}}                                                                         },
        { {"(x) (a b)", "= x' (+ x 1)"},                                      {Keys{Ekeys{"x"}, Ekeys{"a", "b"}}, All_formulas{Phis("+ x 1")}, All_formulas_map{}}                                                                         },
        { {"(x y z) (a b)", "(= x' (+ x 1)) (= y' (- x 1)) (= z' (+ a b))", ""}, {Keys{Ekeys{"x", "y", "z"}, Ekeys{"a", "b"}}, All_formulas{Phis("+ x 1"), Phis("- x 1"), Phis("+ a b")}, All_formulas_map{}}          },
        { {"(x y z) (a b)", "(= x' (+ x 1)) (= y' (- x 1)) (= z' (+ a b))"},  {Keys{Ekeys{"x", "y", "z"}, Ekeys{"a", "b"}}, All_formulas{Phis("+ x 1"), Phis("- x 1"), Phis("+ a b")}, All_formulas_map{}}               },
        { {"(x y z) (a b)", "(= x' (+ x 1)) (= y' (- x 1)) (= z' (+ a b))", "> y 0"},  {Keys{Ekeys{"x", "y", "z"}, Ekeys{"a", "b"}}, All_formulas{Phis("+ x 1"), Phis("- x 1"), Phis("+ a b")}, All_formulas_map{{"y", Phis("> y 0")}}}  },
        { {"(x y z) (a b)", "(= x' (+ x 1)) (= y' (- x 1)) (= z' (+ a b))", "(> y 0)"},  {Keys{Ekeys{"x", "y", "z"}, Ekeys{"a", "b"}}, All_formulas{Phis("+ x 1"), Phis("- x 1"), Phis("+ a b")}, All_formulas_map{{"y", Phis("> y 0")}}}  },
        { {"(x y z) (a b)", "(= x' (+ x 1)) (= y' (- x 1)) (= z' (+ a b)) (> y 0)"},  {Keys{Ekeys{"x", "y", "z"}, Ekeys{"a", "b"}}, All_formulas{Phis("+ x 1"), Phis("- x 1"), Phis("+ a b")}, All_formulas_map{{"y", Phis("> y 0")}}}  },
        { {"(x y z) (a b)", "(= x' (+ x 1)) (= y' (- x 1)) (= z' (+ a b))", "(> x 0) (> y 0) (< z _t)"}, {Keys{Ekeys{"x", "y", "z"}, Ekeys{"a", "b"}}, All_formulas{Phis("+ x 1"), Phis("- x 1"), Phis("+ a b")}, All_formulas_map{{"x", Phis("> x 0")}, {"y", Phis("> y 0")}, {"z", Phis("< z _t")}}}   },
        { {"(x y z) (a b)", "(= x' (+ x 1)) (= y' (- x 1)) (= z' (+ a b))", "> _tau 0"},  {Keys{Ekeys{"x", "y", "z"}, Ekeys{"a", "b"}}, All_formulas{Phis("+ x 1"), Phis("- x 1"), Phis("+ a b")}, All_formulas_map{{"_tau", Phis("> _tau 0")}}}  },
        { {"(x y z) (a b)", "(= x' (+ x 1)) (= y' (- x 1)) (= z' (+ a b))", "> _t 0"},  {Keys{Ekeys{"x", "y", "z"}, Ekeys{"a", "b"}}, All_formulas{Phis("+ x 1"), Phis("- x 1"), Phis("+ a b")}, All_formulas_map{{"_t", Phis("> _t 0")}}}  },
        { {"(x y z) (a b)", "(= x' (+ x 1)) (= y' (- x 1)) (= z' (+ a b))", "(> _tau _t) (= _t _tau)"},  {Keys{Ekeys{"x", "y", "z"}, Ekeys{"a", "b"}}, All_formulas{Phis("+ x 1"), Phis("- x 1"), Phis("+ a b")}, All_formulas_map{{"_tau", Phis("> _tau _t")}, {"_t", Phis("= _t _tau")}}}  },
        { {"(x y z) (a b)", "(= x' (+ x 1)) (= x' (* x _tau)) (= y' (- x _t)) (= z' (+ a b)) (= z' (- a b))", "(> x 0) (< x 0) (> y 0) (= y _tau) (< z _t) (= _tau 0) (< _t 0) (> _t (- (* x y z) (* a b)))"}, {Keys{Ekeys{"x", "y", "z"}, Ekeys{"a", "b"}}, All_formulas{Phis("+ x 1", "* x _tau"), Phis("- x _t"), Phis("+ a b", "- a b")}, All_formulas_map{{"x", Phis("> x 0", "< x 0")}, {"y", Phis("> y 0", "= y _tau")}, {"z", Phis("< z _t")}, {"_tau", Phis("= _tau 0")}, {"_t", Phis("< _t 0", "> _t (- (* x y z) (* a b))")}}}  },
        { {"(x y z) (a b)", "( (= x' (+ x 1)) (= x' (* x _tau)) (= y' (- x _t)) (= z' (+ a b)) (= z' (- a b))) ((> x 0) (< x 0) (> y 0) (= y _tau) (< z _t) (= _tau 0) (< _t 0) (> _t (- (* x y z) (* a b))))"}, {Keys{Ekeys{"x", "y", "z"}, Ekeys{"a", "b"}}, All_formulas{Phis("+ x 1", "* x _tau"), Phis("- x _t"), Phis("+ a b", "- a b")}, All_formulas_map{{"x", Phis("> x 0", "< x 0")}, {"y", Phis("> y 0", "= y _tau")}, {"z", Phis("< z _t")}, {"_tau", Phis("= _tau 0")}, {"_t", Phis("< _t 0", "> _t (- (* x y z) (* a b))")}}}  },
        { {"(x y z) (a b)", "(= _tau 0) (= x' (+ x 1)) (= x' (* x _tau)) (> x 0) (< x 0) (> y 0) (= y' (- x _t)) (= y _tau) (= z' (+ a b)) (= z' (- a b)) (< z _t) (< _t 0) (> _t (- (* x y z) (* a b)))"}, {Keys{Ekeys{"x", "y", "z"}, Ekeys{"a", "b"}}, All_formulas{Phis("+ x 1", "* x _tau"), Phis("- x _t"), Phis("+ a b", "- a b")}, All_formulas_map{{"x", Phis("> x 0", "< x 0")}, {"y", Phis("> y 0", "= y _tau")}, {"z", Phis("< z _t")}, {"_tau", Phis("= _tau 0")}, {"_t", Phis("< _t 0", "> _t (- (* x y z) (* a b))")}}}  },
    });

    Suite(flow_parser_single_msg, true).test<Flow_parser_case>(flow_parse_invalid_data<Flow_parser_case>());
    Suite(flow_parser_msg, true).test<Flow_parser_case>({
        { {"(x) (a b)", ""},                                                  ignore },
        { {"(x) (a b)", "()"},                                                ignore },
        { {"(x) (a b)", "(= x' (+ x 1))", "(())"},                            ignore },
        { {"(x) (a b)", "(= x' (+ x 1)) (())"},                               ignore },
        { {"(x y z) (a b)", "(= x' 1) (= y' 1) (= z' a) ((> y 0))"},          ignore },
    });

    ////////////////////////////////////////////////////////////////

    const String config_msg = "constructing of Config-s from direct arguments";
    Suite(config_msg).test<Case<Config>>({
        { {{0, 1}, 2, 3, {4, 5}, 6},      {Ids{0, 1}, 2, 3, Ids{4, 5}, 6}   },
        { {{0, 1}, 2, 3, {4, 5}},         {Ids{0, 1}, 2, 3, Ids{4, 5}, def_id} },
        { {{0, 1}, 2, 3, Ids{}},          {Ids{0, 1}, 2, 3, Ids{}, def_id}  },
        { {{0, 1}, 2, 3},                 {Ids{0, 1}, 2, 3, Ids{}, def_id}  },
        { {{0, 1}, 2},                    {Ids{0, 1}, 2, def_id, Ids{}, def_id} },
        { {{0, 1}},                       {Ids{0, 1}, def_id, def_id, Ids{}, def_id} },
        { {Ids{0}},                       {Ids{0}, def_id, def_id, Ids{}, def_id} },
        { {Ids{}},                        {Ids{}, def_id, def_id, Ids{}, def_id} },
        { {},                             {Ids{}, def_id, def_id, Ids{}, def_id} },
        { {{0, 1}, {2, 3}, {4, 5}, 6},    {Ids{0, 1}, 2, 3, Ids{4, 5}, 6}   },
        { {{0, 1}, {2, 3}, {4, 5}},       {Ids{0, 1}, 2, 3, Ids{4, 5}, def_id} },
        { {{0, 1}, {2, 3}, Ids{}},        {Ids{0, 1}, 2, 3, Ids{}, def_id}  },
        { {{0, 1}, {2, 3}},               {Ids{0, 1}, 2, 3, Ids{}, def_id}  },
        { {{0, 1}, Ids{2}},               {Ids{0, 1}, 2, def_id, Ids{}, def_id} },
        { {{0, 1}, Ids{}},                {Ids{0, 1}, def_id, def_id, Ids{}, def_id} },
        { {{0, 1}, Ids{2}, {4, 5}, 6},    {Ids{0, 1}, 2, def_id, Ids{4, 5}, 6} },
        { {{0, 1}, Ids{}, {4, 5}, 6},     {Ids{0, 1}, def_id, def_id, Ids{4, 5}, 6} },
        { {{0, 1}, {2, 3}, {4, 5, 6}},    {Ids{0, 1}, 2, 3, Ids{4, 5}, 6}   },
        { {{0, 1}, Ids{2}, {4, 5, 6}},    {Ids{0, 1}, 2, def_id, Ids{4, 5}, 6} },
        { {{0, 1}, Ids{}, {4, 5, 6}},     {Ids{0, 1}, def_id, def_id, Ids{4, 5}, 6} },
        { {Ids{0}, {2, 3}, {4, 5}},       {Ids{0}, 2, 3, Ids{4}, 5}         },
        { {Ids{0}, {2, 3}, Ids{4}},       {Ids{0}, 2, 3, Ids{4}, def_id}    },
        { {Ids{0}, {2, 3}},               {Ids{0}, 2, 3, Ids{}, def_id}     },
        { {Ids{0}, Ids{2}, {4, 5}},       {Ids{0}, 2, def_id, Ids{4}, 5}    },
        { {Ids{0}, Ids{}, {4, 5}},        {Ids{0}, def_id, def_id, Ids{4}, 5} },
        { {Ids{0}, Ids{2}, Ids{4}},       {Ids{0}, 2, def_id, Ids{4}, def_id} },
        { {Ids{0}, Ids{}, Ids{4}},        {Ids{0}, def_id, def_id, Ids{4}, def_id} },
        { {{0, 1}, {2, 3, 4, 5, 6}},      {Ids{0, 1}, 2, 3, Ids{4, 5}, 6}   },
        { {{0, 1}, {2, 3, 4, 5}},         {Ids{0, 1}, 2, 3, Ids{4, 5}, def_id} },
        { {Ids{0}, {2, 3, 4, 5}},         {Ids{0}, 2, 3, Ids{4}, 5}         },
        { {Ids{0}, {2, 3, 4}},            {Ids{0}, 2, 3, Ids{4}, def_id}    },
        { {Ids{}},                        ignore                            },
        { {Ids{}, 0, 1},                  ignore                            },
        { {Ids{}, 1},                     ignore                            },
        { {Ids{}, {0, 1}},                {Ids{}, 0, 1}                     },
        { {Ids{}, Ids{1}},                {Ids{}, 1}                        },
        { {Ids{}, 0, 1, Ids{}},           ignore                            },
        { {Ids{}, 0, 1, Ids{}, 2},        ignore                            },
    });

    Suite(config_msg, true).test<Case<Config>>({
        //+ { {{0, 1}, 2, 3, Ids{4}},         ignore                            },
        //+ { {{0, 1}, 2, 3, {4, 5, 6, 7}},   ignore                            },
        //+ { {Ids{0}, 2, 3, {4, 5, 6}},      ignore                            },
        //+ { {{0, 1}, {2, 3}, Ids{4}},       ignore                            },
        //+ { {{0, 1}, {2, 3}, {4, 5, 6, 7}}, ignore                            },
        //+ { {Ids{0}, {2, 3}, {4, 5, 6}},    ignore                            },
        //+ { {{0, 1}, {2, 3, 4, 5, 6, 7}},   ignore                            },
        //+ { {{0, 1}, {2, 3, 4}},            ignore                            },
        //+ { {Ids{0}, {2, 3, 4, 5, 6}},      ignore                            },
        //+ { {Ids{}, {0, 1, 2}},             ignore                            },
        //+ { {Ids{}, 0, 1, Ids{2}},          ignore                            },
        //+ { {Ids{}, 0, 1, {2, 3}},          ignore                            },
        //+ { {Ids{}, {0, 1}, Ids{2}},        ignore                            },
        //+ { {Ids{}, {0, 1}, {2, 3}},        ignore                            },
    });

    const String config_parse_msg = "parsing Config-s";
    Suite(config_parse_msg).test<Config_parse_case>({
        { {"(0 1) 2 3 (4 5) 6"},          {Ids{0, 1}, 2, 3, Ids{4, 5}, 6}   },
        { {"(0 1) 2 3 (4 5)"},            {Ids{0, 1}, 2, 3, Ids{4, 5}, def_id} },
        { {"(0 1) 2 3 ()"},               {Ids{0, 1}, 2, 3, Ids{}, def_id}  },
        { {"(0 1) 2 3"},                  {Ids{0, 1}, 2, 3, Ids{}, def_id}  },
        { {"(0 1) 2"},                    {Ids{0, 1}, 2, def_id, Ids{}, def_id} },
        { {"(0 1)"},                      {Ids{0, 1}, def_id, def_id, Ids{}, def_id} },
        { {"(0)"},                        {Ids{0}, def_id, def_id, Ids{}, def_id} },
        { {"()"},                         {Ids{}, def_id, def_id, Ids{}, def_id} },
        { {"((0 1) 2 3 (4 5) 6)"},        {Ids{0, 1}, 2, 3, Ids{4, 5}, 6}   },
        { {"((0 1) 2 3 (4 5))"},          {Ids{0, 1}, 2, 3, Ids{4, 5}, def_id} },
        { {"((0 1) 2 3 ())"},             {Ids{0, 1}, 2, 3, Ids{}, def_id}  },
        { {"((0 1) 2 3)"},                {Ids{0, 1}, 2, 3, Ids{}, def_id}  },
        { {"((0 1) 2)"},                  {Ids{0, 1}, 2, def_id, Ids{}, def_id} },
        { {"((0 1))"},                    {Ids{0, 1}, def_id, def_id, Ids{}, def_id} },
        { {"((0))"},                      {Ids{0}, def_id, def_id, Ids{}, def_id} },
        { {"(())"},                       {Ids{}, def_id, def_id, Ids{}, def_id} },
        { {"0 1"},                        {Ids{0, 1}, def_id, def_id, Ids{}, def_id} },
        { {"0"},                          {Ids{0}, def_id, def_id, Ids{}, def_id} },
        { {""},                           {Ids{}, def_id, def_id, Ids{}, def_id} },
        { {"(0 1) (2 3) (4 5) 6"},        {Ids{0, 1}, 2, 3, Ids{4, 5}, 6}   },
        { {"(0 1) (2 3) (4 5)"},          {Ids{0, 1}, 2, 3, Ids{4, 5}, def_id} },
        { {"(0 1) (2 3) ()"},             {Ids{0, 1}, 2, 3, Ids{}, def_id}  },
        { {"(0 1) (2 3)"},                {Ids{0, 1}, 2, 3, Ids{}, def_id}  },
        { {"(0 1) (2)"},                  {Ids{0, 1}, 2, def_id, Ids{}, def_id} },
        { {"(0 1) ()"},                   {Ids{0, 1}, def_id, def_id, Ids{}, def_id} },
        { {"(0 1) (2) (4 5) 6"},          {Ids{0, 1}, 2, def_id, Ids{4, 5}, 6} },
        { {"(0 1) ()  (4 5) 6"},          {Ids{0, 1}, def_id, def_id, Ids{4, 5}, 6} },
        { {"(0 1) (2 3) (4 5 6)"},        {Ids{0, 1}, 2, 3, Ids{4, 5}, 6}   },
        { {"(0 1) (2)   (4 5 6)"},        {Ids{0, 1}, 2, def_id, Ids{4, 5}, 6} },
        { {"(0 1) () (4 5 6)"},           {Ids{0, 1}, def_id, def_id, Ids{4, 5}, 6} },
        { {"(0) (2 3) (4 5)"},            {Ids{0}, 2, 3, Ids{4}, 5}         },
        { {"(0) (2 3) (4)"},              {Ids{0}, 2, 3, Ids{4}, def_id}    },
        { {"(0) (2 3)"},                  {Ids{0}, 2, 3, Ids{}, def_id}     },
        { {"(0) (2) (4 5)"},              {Ids{0}, 2, def_id, Ids{4}, 5}    },
        { {"(0) () (4 5)"},               {Ids{0}, def_id, def_id, Ids{4}, 5} },
        { {"(0) (2) (4)"},                {Ids{0}, 2, def_id, Ids{4}, def_id} },
        { {"(0) () (4)"},                 {Ids{0}, def_id, def_id, Ids{4}, def_id} },
        { {"(0 1) (2 3 4 5 6)"},          {Ids{0, 1}, 2, 3, Ids{4, 5}, 6}   },
        { {"(0 1) (2 3 4 5)"},            {Ids{0, 1}, 2, 3, Ids{4, 5}, def_id} },
        { {"(0)   (2 3 4 5)"},            {Ids{0}, 2, 3, Ids{4}, 5}         },
        { {"(0)   (2 3 4)"},              {Ids{0}, 2, 3, Ids{4}, def_id}    },
        { {"()"},                         {}                                },
        { {"() 0 1"},                     {Ids{}, 0, 1}                     },
        { {"() 1"},                       {Ids{}, 1}                        },
        { {"() (0 1)"},                   {Ids{}, 0, 1}                     },
        { {"() (1)"},                     {Ids{}, 1}                        },
        { {"() 0 1 ()"},                  {Ids{}, 0, 1}                     },
        //+ { {"() 0 1 () 2"},                {Ids{}, 0, 1, Ids{}, 2}           },
    });

    Suite(config_parse_msg, true).test<Config_parse_case>({
        { {"(0 1) 2 3 4 (4 5) 6"},        ignore                            },
        { {"(0 1) 2 3 4 (5) 6"},          ignore                            },
        { {"(0 1) 2 3 4 5 () 6"},         ignore                            },
        { {"(0 1) 2 3 4 5 6"},            ignore                            },
        { {"(0 1) 2 3 4 5"},              ignore                            },
        { {"(0 1) 2 3 4"},                ignore                            },
        { {"(0 1) 2 3 (4 5) ()"},         ignore                            },
        { {"(0 1) 2 3 (4 5) (6)"},        ignore                            },
        { {"(0 1) 2 3 (4 5) (6 7)"},      ignore                            },
        { {"(0 1) (2 3 4) (5) 6"},        ignore                            },
        { {"(0 1) (2 3 4) (4 5) 6"},      ignore                            },
        { {"(0 1) (2 3 4) (5 6)"},        ignore                            },
        { {"(0 1) (2 3) 4 5 6"},          ignore                            },
        { {"(0 1) (2 3) 4 5"},            ignore                            },
        { {"(0 1) (2 3) 4"},              ignore                            },
        { {"(0 1) 2 3 (4) (5 6)"},        ignore                            },
        { {"(0 1) 2 3 (4)"},              ignore                            },
        { {"(0 1) 2 3 (4 5 6 7)"},        ignore                            },
        { {"(0) 2 3 (4 5 6)"},            ignore                            },
        { {"(0 1) (2 3) (4)"},            ignore                            },
        { {"(0 1) (2 3) (4 5 6 7)"},      ignore                            },
        { {"(0) (2 3) (4 5 6)"},          ignore                            },
        { {"(0 1) (2 3 4 5 6 7)"},        ignore                            },
        { {"(0 1) (2 3 4)"},              ignore                            },
        { {"(0) (2 3 4 5 6)"},            ignore                            },
        { {"() (0 1 2)"},                 ignore                            },
        { {"() 0 1 (2)"},                 ignore                            },
        { {"() 0 1 (2 3)"},               ignore                            },
        { {"() (0 1) (2)"},               ignore                            },
        { {"() (0 1) (2 3)"},             ignore                            },
    });

    ////////////////////////////////////////////////////////////////

    ////////////////////////////////////////////////////////////////
    ////////////////////////////////////////////////////////////////

    cout << endl << "Success." << endl;
    return 0;
}
catch (const unsot::Error& e) {
    std::cout << e << std::endl;
    throw;
}
