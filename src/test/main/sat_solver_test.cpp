#include "test.hpp"
#include "smt/sat/solver.hpp"
#include "smt/sat/solver/offline/minisat.hpp"

namespace unsot::test {
    using namespace smt::sat::solver;
    using namespace smt::sat::solver::minisat;

    using offline::Minisat;

    ////////////////////////////////////////////////////////////////

}

////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////

int main(int, const char*[])
try {
    using namespace unsot;
    using namespace unsot::test;
    using namespace std;
    using test::Minisat;

    ////////////////////////////////////////////////////////////////

    auto s = Minisat::cons();
    auto x1 = s.new_var();
    Lit l1(x1, true);
    auto x2 = s.new_var();
    Lit l2(x2, false);
    auto x3 = s.new_var();
    Lit l3(x3, false);
    auto x4 = s.new_var();
    Lit l4(x4, true);
    s.add_clause(l1);
    s.add_clause(l2);
    s.add_clause(l3);
    bool sat = s.solve();
    cout << "Sat: " << sat << endl;
    expect(sat, "Expected 'sat'!");
    cout << "Model: " << s.cmodel() << endl;
    cout << "Value(x1): " << s.value(x1) << endl;
    cout << "Value(l1): " << s.lit_value(l1) << endl;
    cout << "Value(x2): " << s.value(x2) << endl;
    cout << "Value(l2): " << s.lit_value(l2) << endl;
    cout << "Value(x3): " << s.value(x3) << endl;
    cout << "Value(l3): " << s.lit_value(l3) << endl;
    cout << "Value(x4): " << s.value(x4) << endl;
    cout << "Value(l4): " << s.lit_value(l4) << endl;
    expect(s.value(x1), "Expected x1 == true!");
    expect(s.value(x2).is_false(), "Expected x2 == false!");
    expect(s.value(x3).is_false(), "Expected x3 == false!");
    expect(s.value(x4).unknown(), "Expected x4 == unknown!");

    cout << "Model value(x1): " << s.model_value(x1) << endl;
    cout << "Model value(l1): " << s.lit_model_value(l1) << endl;
    cout << "Model value(x2): " << s.model_value(x2) << endl;
    cout << "Model value(l2): " << s.lit_model_value(l2) << endl;
    cout << "Model value(x3): " << s.model_value(x3) << endl;
    cout << "Model value(l3): " << s.lit_model_value(l3) << endl;
    cout << "Model value(x4): " << s.model_value(x4) << endl;
    cout << "Model value(l4): " << s.lit_model_value(l4) << endl;
    expect(s.model_value(x1), "Expected x1 == true!");
    expect(!s.model_value(x2), "Expected x2 == false!");
    expect(!s.model_value(x3), "Expected x3 == false!");

    cout << endl;
    auto x5 = s.new_var();
    auto x6 = s.new_var();
    s.add_clause(Lit(x5, true), Lit(x6, false));
    cout << s.to_cnf() << endl;
    sat = s.solve();
    cout << "Sat: " << sat << endl;
    expect(sat, "Expected 'sat'!");
    if (sat) cout << "Model: " << s.cmodel() << endl;
    sat = s.solve(Lit(x5, true));
    cout << "Sat[x5]: " << sat << endl;
    expect(sat, "Expected 'sat' under [x5] assumptions!");
    if (sat) cout << "Model: " << s.cmodel() << endl;
    sat = s.solve(Lit(x6, true));
    cout << "Sat[x6]: " << sat << endl;
    expect(sat, "Expected 'sat' under [x6] assumptions!");
    if (sat) cout << "Model: " << s.cmodel() << endl;
    sat = s.solve(Lit(x5, false));
    cout << "Sat[~x5]: " << sat << endl;
    expect(sat, "Expected 'sat' under [~x5] assumptions!");
    if (sat) cout << "Model: " << s.cmodel() << endl;
    sat = s.solve(Lit(x6, false));
    cout << "Sat[~x6]: " << sat << endl;
    expect(sat, "Expected 'sat' under [~x6] assumptions!");
    if (sat) cout << "Model: " << s.cmodel() << endl;
    sat = s.solve(Lit(x5, false), Lit(x6, true));
    cout << "Sat[~x5, x6]: " << sat << endl;
    expect(!sat, "Expected 'unsat' under [~x5, x6] assumptions!");
    if (sat) cout << "Model: " << s.cmodel() << endl;
    cout << s.cconflict() << endl;

    ////////////////////////////////////////////////////////////////

    ////////////////////////////////////////////////////////////////
    ////////////////////////////////////////////////////////////////

    cout << endl << "Success." << endl;
    return 0;
}
catch (const unsot::Error& e) {
    std::cout << e << std::endl;
    throw;
}
