#include "expr.hpp"

#include "util/alg.hpp"
#include "util/hash.hpp"

namespace unsot::expr {
    void Place::check_is_elem(const Ptr& ptr)
    {
        expect(Elem_base::is_me(ptr), "Expected expr element, got: "s + ptr->to_string());
    }

    void Place::check_is_list(const Ptr& ptr)
    {
        expect(List::is_me(ptr), "Expected expr list, got: "s + ptr->to_string());
    }
}

////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////

namespace unsot::expr {
    template <typename K, Req<is_keys_v<K>>>
    Keys_ptr new_keys(K&& keys)
    {
        check_keys(keys);
        return MAKE_SHARED(FORWARD(keys));
    }

    template <typename K, Req<is_ids_map_v<K>>>
    Ids_map_ptr new_ids_map(K&& imap)
    {
        return MAKE_SHARED(FORWARD(imap));
    }

    template Keys_ptr new_keys(const Keys&);
    template Keys_ptr new_keys(Keys&&);
    template Ids_map_ptr new_ids_map(const Ids_map&);
    template Ids_map_ptr new_ids_map(Ids_map&&);

    Ids_map cons_ids_map(const Keys& keys)
    {
        Ids_map map;
        if constexpr (enabled_reserve_v<Ids_map>) map.reserve(keys.size());
        Idx idx = 0;
        for (auto& k : keys) {
            map.emplace(k, idx++);
        }
        return map;
    }

    Ids_map_ptr new_ids_map(const Keys& keys)
    {
        return new_ids_map(cons_ids_map(keys));
    }

    bool is_seq(const Ptr& ptr) noexcept
    {
        return List::is_me(ptr) || is_key(ptr);
    }

    void check_is_seq(const Ptr& ptr)
    {
        expect(is_seq(ptr), "Expected a sequence, got: "s + ptr->to_string());
    }

    bool valid_keys(const Keys& keys) noexcept
    {
        return all_of(keys, valid_key);
    }

    void check_keys(const Keys& keys)
    {
        for_each(keys, check_key);
    }

    bool valid_ids_map(const Ids_map& imap, const Keys& keys) noexcept
    {
        return size(imap) == size(keys)
            && all_of(keys, [&imap](auto& k){ return imap.contains(k); });
    }

    void check_ids_map(const Ids_map& imap, const Keys& keys)
    {
        expect(valid_ids_map(imap, keys),
               "Keys map does not correspond to keys:\n"s
               + imap.to_string() + " !~ " + to_string(keys));
    }

    Key_id add_key(Keys& keys, Ids_map& imap, Key key_)
    {
        Key_id id = keys.size();
        auto [_, inserted] = imap.try_emplace(key_, id);
        expect(inserted, "Key '"s + to_string(key_)
               + "' is already present in the map: " + imap.to_string());
        keys.emplace_back(move(key_));
        return id;
    }
}
