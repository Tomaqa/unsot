#pragma once

#include "solver.hpp"

#include "util/string.hpp"
#include "util/fun.hpp"
#include "util/alg.hpp"
#include "solver/flow/alg.hpp"

namespace unsot::solver {
    template <typename B, typename OdeSolver>
    Mixin<B, OdeSolver>::Mixin()
    {
        this->option(":dt") = to_string(Ode_solver::def_dt);
        this->option(":keep-flow-invariant") = "0";
        this->option(":require-flow-progress") = "0";
        //+ add ':produce-trajectories' and setting plot script file
    }

    template <typename B, typename OdeSolver>
    bool Mixin<B, OdeSolver>::is_aux_var(const Key& key_) const noexcept
    {
        if (starts_with(key_, flow::t_key)) return false;
        return Inherit::is_aux_var(key_);
    }

    template <typename B, typename OdeSolver>
    bool Mixin<B, OdeSolver>::valid_option_value(const Option& opt, const Option_value& val) const
    try {
        return Inherit::valid_option_value(opt, val);
    }
    catch (Dummy) {
        if (opt == ":dt") return is_value<Real>(val);
        if (util::contains({":keep-flow-invariant", ":require-flow-progress"}, opt))
            return is_value<Bool>(val);

        throw;
    }

    template <typename B, typename OdeSolver>
    void Mixin<B, OdeSolver>::apply_option(const Option& opt)
    try {
        Inherit::apply_option(opt);
    }
    catch (Dummy) {
        auto& val = this->coption(opt);
        if (opt == ":dt") {
            ode_solver().dt() = to_value<Real>(val).value();
            return;
        }
        const auto cond = to_value<Bool>(val);
        if (opt == ":keep-flow-invariant") {
            return cond.value() ? ode_solver().keep_invariant() : ode_solver().unkeep_invariant();
        }
        if (opt == ":require-flow-progress") {
            return cond.value() ? require_flow_progress() : relax_flow_progress();
        }

        throw;
    }

    template <typename B, typename OdeSolver>
    void Mixin<B, OdeSolver>::define_flow(Key fkey, Formula phi, expr::Keys ode_keys,
                                          expr::Keys real_arg_keys, expr::Keys bool_arg_keys)
    {
        typename flow::Def::template Define<That>(this->that(), move(fkey), move(phi),
            move(ode_keys), move(real_arg_keys), move(bool_arg_keys)
        ).perform();
    }

    template <typename B, typename OdeSolver>
    void Mixin<B, OdeSolver>::declare_final_var(Key vkey)
    {
        this->pre_add_real(vkey);
        auto& reals = this->reals();
        reals.push_back(reals.template new_var_tp<Final_var>(move(vkey)));
    }

    template <typename B, typename OdeSolver>
    void Mixin<B, OdeSolver>::declare_invariant(Key vkey, Key ode_key, const Flow_inst& finst)
    {
        auto sat_vid = this->pre_add_bool(vkey);
        auto& preds = this->bools();
        var::Id final_vid = flow::is_global_key(ode_key) || ode_key == flow::tau_key
                          ? var::invalid_id
                          : cfinal_var_ptr(ode_key, finst)->cid();
        preds.push_back(preds.template new_var_tp<Invariant>(move(ode_key), move(final_vid),
            this->cflow_insts_ptr(), finst.cid(), move(vkey)
        ));
        this->post_add_bool(move(sat_vid));

        SMT_CVERB2LN("declare_invariant: " << preds.cback()->ckey());
    }

    template <typename B, typename OdeSolver>
    void Mixin<B, OdeSolver>::define_ode(const Key& ode_key, Key fun_key, const flow::Id& fid)
    {
        auto& fdef = cflow_def(fid);
        auto final_id = fdef.cflow().ckeys().t_ode_key_id(ode_key);
        //+ the formula is not necessary at all
        expr::Keys arg_keys{fdef.cfinal_keys()[final_id], fdef.cinit_keys()[final_id]};
        auto phi = Formula::cons("=", List{expr::new_key(arg_keys[0]), expr::new_key(arg_keys[1])});
        this->template define_fun_tp<Bool>(move(fun_key), move(phi), move(arg_keys));
    }

    template <typename B, typename OdeSolver>
    flow::Id Mixin<B, OdeSolver>::add_flow_def(flow::Def fdef, Flow flow)
    {
        flow::Id fid = ode_solver().add_flow(move(flow));
        flow_id(fdef.ckey()) = fid;
        flow_defs().push_back(move(fdef));

        return fid;
    }

    template <typename B, typename OdeSolver>
    Formula Mixin<B, OdeSolver>::
        expand_def_flow(const Key& fkey, const expr::Keys& init_vals,
                        const expr::Keys& real_arg_vals, const expr::Keys& bool_arg_vals)
    {
        return typename Flow_inst::Instantiate(this->that(),
            cflow_id(fkey), init_vals, real_arg_vals, bool_arg_vals
        ).perform().expand();
    }

    template <typename B, typename OdeSolver>
    void Mixin<B, OdeSolver>::
        assert_def_flow(const Key& fkey, const expr::Keys& init_vals,
                        const expr::Keys& real_arg_vals, const expr::Keys& bool_arg_vals)
    {
        this->assert_expr(expand_def_flow(fkey, init_vals, real_arg_vals, bool_arg_vals));
    }

    template <typename B, typename OdeSolver>
    Key Mixin<B, OdeSolver>::instantiate_ode_fun_inst(Fun_inst fun, Key ode_key,
                                                      var::Id final_vid, flow::Inst_id finst_id)
    {
        auto f = [&ode_key, &final_vid, &finst_id](This* this_, auto&& fun_){
            auto&& [k, phi] = move(fun_);
            auto sat_vid = this_->pre_add_bool(k);
            auto& preds = this_->real_preds();
            preds.push_back(preds.template new_fun_tp<Ode>(move(phi), move(ode_key),
                move(final_vid), this_->cflow_insts_ptr(), move(finst_id), move(k)
            ));
            this_->post_add_bool(move(sat_vid));
        };
        return this->template instantiate_fun_inst_tp<Real, do_check, allow_dups>(move(fun),
                                                                                  move(f));
    }

    template <typename B, typename OdeSolver>
    Key Mixin<B, OdeSolver>::instantiate_def_ode(const Key& fun_key, const Key& ode_key,
                                                 const Flow_inst& finst)
    {
        auto final_id = finst.cflow().ckeys().t_ode_key_id(ode_key);
        auto& final_ptr = cfinal_var_ptr(ode_key, finst);
        auto inst_f = bind(&This::instantiate_ode_fun_inst, _1, _2,
                           move(ode_key), final_ptr->cid(), finst.cid());
        const expr::Keys arg_keys{final_ptr->ckey(), finst.cinit_values()[final_id]};
        return this->instantiate_def_fun_tp(fun_key, arg_keys, move(inst_f));
    }

    template <typename B, typename OdeSolver>
    void Mixin<B, OdeSolver>::init_solve()
    {
        Inherit::init_solve();
        if (auto run_l_ = this->run_l()) run_l_->set_trajects_ostreams();
    }

    template <typename B, typename OdeSolver>
    String Mixin<B, OdeSolver>::var_stats_to_string() const
    {
        ostringstream oss;
        oss << Inherit::var_stats_to_string();

        size_t n_odes = 0;
        size_t n_invars = 0;
        size_t n_finals = 0;
        for (auto& finst : cflow_insts()) {
            n_odes += finst.code_var_ids().size();
            n_invars += finst.cinvariant_var_ids().size();
            n_finals += finst.cfinal_var_ids().size();
        }

        const auto b = this->cbools().size();
        const auto r = this->creals().size();
        const auto rp = this->creal_preds().size();
        oss << "# ODEs: " << n_odes << " (" << (double(n_odes)/rp)*100 << " % of real preds)\n"
            << "# invariants: " << n_invars << " (" << (double(n_invars)/b)*100 << " % of bools)\n"
            << "# final vars: " << n_finals << " (" << (double(n_finals)/r)*100 << " % of reals)\n"
            ;
        return oss.str();
    }
}

#include "solver/flow.tpp"
#include "solver/var.tpp"
#include "solver/check_sat.tpp"
#include "solver/parser.tpp"
