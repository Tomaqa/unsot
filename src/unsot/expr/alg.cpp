#include "expr/alg.hpp"

#include "util/string/alg.hpp"

namespace unsot::expr {
    bool is_deep_n(const List& ls, int levels)
    {
        if (!is_deep(ls)) return false;
        if (--levels <= 0) return true;
        return all_of(ls, as_clists([levels](auto& l){
            return is_deep_n(l, levels);
        }));
    }

    bool is_deep_flat(const List& ls)
    {
        if (!is_deep(ls)) return false;
        return all_of(ls, as_clists(is_flat));
    }

    bool is_deep_sparse(const List& ls)
    {
        if (!is_deep(ls)) return false;
        return none_of(ls, as_clists(is_deep));
    }
}
