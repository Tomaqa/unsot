#include "expr/opers_arith.hpp"

#include "expr/opers.tpp"

#ifndef NO_EXPLICIT_TP_INST
namespace unsot::expr::fun {
    template struct Opers<Int>;
    template struct Opers<Real>;
    template struct Opers<Idx>;
}
#endif  /// NO_EXPLICIT_TP_INST
