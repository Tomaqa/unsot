#pragma once

#include "expr/opers.hpp"

#include "util/numeric/alg.hpp"
#include "util/hash.hpp"

namespace unsot::expr::fun {
    using numeric::apx_equal;
    using numeric::apx_less_equal;
    using numeric::apx_greater_equal;
    using numeric::apx_less;
    using numeric::apx_greater;

    template <typename T, typename U, typename V>
    template <typename T_, Req<is_integral_v<T_>>>
    const typename Opers<T, U, V>::Un_fs_map
    Opers<T, U, V>::integral_un_fs_map{
        //+ std::identity
        {"+",    [](T a) -> T  { return a; }},
        {"-",    std::negate()},
        {"~",    std::bit_not()},
        {"min",  [](T a) -> T  { return a; }},
        {"max",  [](T a) -> T  { return a; }},
        {"abs",  [](T a) -> T  { return abs(a); }},
        {"log",  [](T a) -> T  { return log2(a); }},
    };

    template <typename T, typename U, typename V>
    template <typename T_, Req<is_integral_v<T_>>>
    const typename Opers<T, U, V>::Bin_fs_map
    Opers<T, U, V>::integral_bin_fs_map{
        {"+",    std::plus()},
        {"-",    std::minus()},
        {"*",    std::multiplies()},
        {"/",    std::divides()},
        {"%",    std::modulus()},
        {"div",  std::divides()},
        {"rem",  std::modulus()},
        {"^",    [](T a, U b) -> T  { return pow(double(a), double(b)); }},
        {"pow",  [](T a, U b) -> T  { return pow(double(a), double(b)); }},
        {"&",    std::bit_and()},
        {"|",    std::bit_or()},
        {"xor",  std::bit_xor()},
        {"<<",   [](T a, U b) -> T  { return a << b; }},
        {">>",   [](T a, U b) -> T  { return a >> b; }},
        {"min",  [](T a, U b) -> T  { return min(a, b); }},
        {"max",  [](T a, U b) -> T  { return max(a, b); }},
    };

    template <typename T, typename U, typename V>
    template <typename T_, Req<is_integral_v<T_>>>
    const typename Opers<T, U, V>::Ter_fs_map
    Opers<T, U, V>::integral_ter_fs_map{
        {"+",    [](T a, U b, V c) -> T  { return a + b + c; }},
        {"*",    [](T a, U b, V c) -> T  { return a * b * c; }},
        {"min",  [](T a, U b, V c) -> T  { return min(a, min(b, c)); }},
        {"max",  [](T a, U b, V c) -> T  { return max(a, max(b, c)); }},
        {"ite",  [](bool a, U b, V c) -> T { return a ? b : c; }},
    };

    template <typename T, typename U, typename V>
    template <typename T_, Req<is_integral_v<T_>>>
    const typename Opers<T, U, V>::Un_preds_map
    Opers<T, U, V>::integral_un_preds_map{};

    template <typename T, typename U, typename V>
    template <typename T_, Req<is_integral_v<T_>>>
    const typename Opers<T, U, V>::Bin_preds_map
    Opers<T, U, V>::integral_bin_preds_map{
        {"=",    std::equal_to()},
        {"<=",   std::less_equal()},
        {">=",   std::greater_equal()},
        {"<",    std::less()},
        {">",    std::greater()},
    };

    template <typename T, typename U, typename V>
    template <typename T_, Req<is_integral_v<T_>>>
    const typename Opers<T, U, V>::Ter_preds_map
    Opers<T, U, V>::integral_ter_preds_map{
        {"=",    [](T a, U b, V c) -> Bool { return a == b && b == c; }},
    };

    template <typename T, typename U, typename V>
    template <typename T_, Req<is_floating_point_v<T_>>>
    const typename Opers<T, U, V>::Un_fs_map
    Opers<T, U, V>::floating_point_un_fs_map{
        {"+",    [](T a) -> T { return a; }},
        {"-",    std::negate()},
        {"min",  [](T a) -> T { return a; }},
        {"max",  [](T a) -> T { return a; }},
        {"abs",  [](T a) -> T { return abs(a); }},
        {"sqrt", [](T a) -> T { return sqrt(a); }},
        {"cbrt", [](T a) -> T { return cbrt(a); }},
        {"sin",  [](T a) -> T { return sin(a); }},
        {"cos",  [](T a) -> T { return cos(a); }},
        {"tan",  [](T a) -> T { return tan(a); }},
        {"tg",   [](T a) -> T { return tan(a); }},
        {"exp",  [](T a) -> T { return exp(a); }},
        {"log",  [](T a) -> T { return log2(a); }},
        {"ln",   [](T a) -> T { return log(a); }},
    };

    template <typename T, typename U, typename V>
    template <typename T_, Req<is_floating_point_v<T_>>>
    const typename Opers<T, U, V>::Bin_fs_map
    Opers<T, U, V>::floating_point_bin_fs_map{
        {"+",   std::plus()},
        {"-",   std::minus()},
        {"*",   std::multiplies()},
        {"/",   std::divides()},
        {"^",   [](T a, U b) -> T  { return pow(a, b); }},
        {"pow", [](T a, U b) -> T  { return pow(a, b); }},
        {"min", [](T a, U b) -> T  { return min(a, b); }},
        {"max", [](T a, U b) -> T  { return max(a, b); }},
    };

    template <typename T, typename U, typename V>
    template <typename T_, Req<is_floating_point_v<T_>>>
    const typename Opers<T, U, V>::Ter_fs_map
    Opers<T, U, V>::floating_point_ter_fs_map{
        {"+",    [](T a, U b, V c) -> T { return a + b + c; }},
        {"*",    [](T a, U b, V c) -> T { return a * b * c; }},
        {"min",  [](T a, U b, V c) -> T  { return min(a, min(b, c)); }},
        {"max",  [](T a, U b, V c) -> T  { return max(a, max(b, c)); }},
        {"ite",  [](bool a, U b, V c) -> T { return a ? b : c; }},
    };

    template <typename T, typename U, typename V>
    template <typename T_, Req<is_floating_point_v<T_>>>
    const typename Opers<T, U, V>::Un_preds_map
    Opers<T, U, V>::floating_point_un_preds_map{};

    template <typename T, typename U, typename V>
    template <typename T_, Req<is_floating_point_v<T_>>>
    const typename Opers<T, U, V>::Bin_preds_map
    Opers<T, U, V>::floating_point_bin_preds_map{
        {"=",   std::equal_to()},
        {"<=",  std::less_equal()},
        {">=",  std::greater_equal()},
        {"<",   std::less()},
        {">",   std::greater()},
        {"~",   [](T a, U b) -> Bool { return apx_equal(a, b); }},
        {"<~",  [](T a, U b) -> Bool { return apx_less_equal(a, b); }},
        {">~",  [](T a, U b) -> Bool { return apx_greater_equal(a, b); }},
        {"<<",  [](T a, U b) -> Bool { return apx_less(a, b); }},
        {">>",  [](T a, U b) -> Bool { return apx_greater(a, b); }},
    };

    template <typename T, typename U, typename V>
    template <typename T_, Req<is_floating_point_v<T_>>>
    const typename Opers<T, U, V>::Ter_preds_map
    Opers<T, U, V>::floating_point_ter_preds_map{
        {"=",   [](T a, U b, V c) -> Bool { return a == b && b == c; }},
    };

    template <typename T, typename U, typename V>
    const typename Opers<T, U, V>::Un_bools_map
    Opers<T, U, V>::un_bools_map{
        {"not", std::logical_not()},
        {"and", [](bool a) -> Bool { return a; }},
        {"or",  [](bool a) -> Bool { return a; }},
    };

    template <typename T, typename U, typename V>
    const typename Opers<T, U, V>::Bin_bools_map
    Opers<T, U, V>::bin_bools_map{
        {"and", std::logical_and()},
        {"or",  std::logical_or()},
        {"xor", std::bit_xor()},
        {"=>",  [](bool a, bool b) -> Bool { return !a || b; }},
        {"=",   std::equal_to()},
    };

    template <typename T, typename U, typename V>
    const typename Opers<T, U, V>::Ter_bools_map
    Opers<T, U, V>::ter_bools_map{
        {"and", [](bool a, bool b, bool c) -> Bool { return a && b && c; }},
        {"or",  [](bool a, bool b, bool c) -> Bool { return a || b || c; }},
        {"xor", [](bool a, bool b, bool c) -> Bool { return a ^ b ^ c; }},
        {"=>",  [](bool a, bool b, bool c) -> Bool { return !a || !b || c; }},
        {"=",   [](bool a, bool b, bool c) -> Bool { return a == b && b == c; }},
    };

    template <typename T, typename U, typename V>
    bool Opers<T, U, V>::valid_unary_f(const Key& key_) noexcept
    {
        return unary_fs_map().contains(key_);
    }

    template <typename T, typename U, typename V>
    bool Opers<T, U, V>::valid_binary_f(const Key& key_) noexcept
    {
        return binary_fs_map().contains(key_);
    }

    template <typename T, typename U, typename V>
    bool Opers<T, U, V>::valid_ternary_f(const Key& key_) noexcept
    {
        return ternary_fs_map().contains(key_);
    }

    template <typename T, typename U, typename V>
    bool Opers<T, U, V>::valid_nary_f(const Key& key_, int n) noexcept
    {
        switch (n) {
        case 1: return valid_unary_f(key_);
        default:
        case 2: return valid_binary_f(key_);
        case 3: return valid_ternary_f(key_);
        }
    }

    template <typename T, typename U, typename V>
    bool Opers<T, U, V>::valid_f(const Key& key_) noexcept
    {
        return valid_binary_f(key_)
            || valid_unary_f(key_)
            || valid_ternary_f(key_);
    }

    template <typename T, typename U, typename V>
    void Opers<T, U, V>::check_unary_f(const Key& key_)
    {
        expect(valid_unary_f(key_),
               "Expected "s + type() + " unary function key, got: "s + key_);
    }

    template <typename T, typename U, typename V>
    void Opers<T, U, V>::check_binary_f(const Key& key_)
    {
        expect(valid_binary_f(key_),
               "Expected "s + type() + " binary function key, got: "s + key_);
    }

    template <typename T, typename U, typename V>
    void Opers<T, U, V>::check_ternary_f(const Key& key_)
    {
        expect(valid_ternary_f(key_),
               "Expected "s + type() + " ternary function key, got: "s + key_);
    }

    template <typename T, typename U, typename V>
    void Opers<T, U, V>::check_nary_f(const Key& key_, int n)
    {
        switch (n) {
        case 1: return check_unary_f(key_);
        default:
        case 2: return check_binary_f(key_);
        case 3: return check_ternary_f(key_);
        }
    }

    template <typename T, typename U, typename V>
    void Opers<T, U, V>::check_f(const Key& key_)
    {
        expect(valid_f(key_), "Expected "s + type() + " function key, got: "s + key_);
    }

    template <typename T, typename U, typename V>
    bool Opers<T, U, V>::valid_unary_pred(const Key& key_) noexcept
    {
        return unary_preds_map().contains(key_);
    }

    template <typename T, typename U, typename V>
    bool Opers<T, U, V>::valid_binary_pred(const Key& key_) noexcept
    {
        return binary_preds_map().contains(key_);
    }

    template <typename T, typename U, typename V>
    bool Opers<T, U, V>::valid_ternary_pred(const Key& key_) noexcept
    {
        return ternary_preds_map().contains(key_);
    }

    template <typename T, typename U, typename V>
    bool Opers<T, U, V>::valid_nary_pred(const Key& key_, int n) noexcept
    {
        switch (n) {
        case 1: return valid_unary_pred(key_);
        default:
        case 2: return valid_binary_pred(key_);
        case 3: return valid_ternary_pred(key_);
        }
    }

    template <typename T, typename U, typename V>
    bool Opers<T, U, V>::valid_pred(const Key& key_) noexcept
    {
        return valid_binary_pred(key_)
            || valid_unary_pred(key_)
            || valid_ternary_pred(key_);
    }

    template <typename T, typename U, typename V>
    void Opers<T, U, V>::check_unary_pred(const Key& key_)
    {
        expect(valid_unary_pred(key_),
               "Expected "s + type() + " unary predicate key, got: "s + key_);
    }

    template <typename T, typename U, typename V>
    void Opers<T, U, V>::check_binary_pred(const Key& key_)
    {
        expect(valid_binary_pred(key_),
               "Expected "s + type() + " binary predicate key, got: "s + key_);
    }

    template <typename T, typename U, typename V>
    void Opers<T, U, V>::check_ternary_pred(const Key& key_)
    {
        expect(valid_ternary_pred(key_),
               "Expected "s + type() + " ternary predicate key, got: "s + key_);
    }

    template <typename T, typename U, typename V>
    void Opers<T, U, V>::check_nary_pred(const Key& key_, int n)
    {
        switch (n) {
        case 1: return check_unary_pred(key_);
        default:
        case 2: return check_binary_pred(key_);
        case 3: return check_ternary_pred(key_);
        }
    }

    template <typename T, typename U, typename V>
    void Opers<T, U, V>::check_pred(const Key& key_)
    {
        expect(valid_pred(key_), "Expected "s + type() + " predicate key, got: "s + key_);
    }

    template <typename T, typename U, typename V>
    bool Opers<T, U, V>::valid_unary_bool(const Key& key_) noexcept
    {
        return unary_bools_map().contains(key_);
    }

    template <typename T, typename U, typename V>
    bool Opers<T, U, V>::valid_binary_bool(const Key& key_) noexcept
    {
        return binary_bools_map().contains(key_);
    }

    template <typename T, typename U, typename V>
    bool Opers<T, U, V>::valid_ternary_bool(const Key& key_) noexcept
    {
        return ternary_bools_map().contains(key_);
    }

    template <typename T, typename U, typename V>
    bool Opers<T, U, V>::
        valid_nary_bool(const Key& key_, int n) noexcept
    {
        switch (n) {
        case 1: return valid_unary_bool(key_);
        default:
        case 2: return valid_binary_bool(key_);
        case 3: return valid_ternary_bool(key_);
        }
    }

    template <typename T, typename U, typename V>
    bool Opers<T, U, V>::valid_bool(const Key& key_) noexcept
    {
        return valid_binary_bool(key_)
            || valid_unary_bool(key_)
            || valid_ternary_bool(key_);
    }

    template <typename T, typename U, typename V>
    void Opers<T, U, V>::check_unary_bool(const Key& key_)
    {
        expect(valid_unary_bool(key_),
               "Expected "s + type() + " unary Boolean key, got: "s + key_);
    }

    template <typename T, typename U, typename V>
    void Opers<T, U, V>::check_binary_bool(const Key& key_)
    {
        expect(valid_binary_bool(key_),
               "Expected "s + type() + " binary Boolean key, got: "s + key_);
    }

    template <typename T, typename U, typename V>
    void Opers<T, U, V>::check_ternary_bool(const Key& key_)
    {
        expect(valid_ternary_bool(key_),
               "Expected "s + type() + " ternary Boolean key, got: "s + key_);
    }

    template <typename T, typename U, typename V>
    void Opers<T, U, V>::check_nary_bool(const Key& key_, int n)
    {
        switch (n) {
        case 1: return check_unary_bool(key_);
        default:
        case 2: return check_binary_bool(key_);
        case 3: return check_ternary_bool(key_);
        }
    }

    template <typename T, typename U, typename V>
    void Opers<T, U, V>::check_bool(const Key& key_)
    {
        expect(valid_bool(key_), "Expected "s + type() + " Boolean key, got: "s + key_);
    }

    template <typename T, typename U, typename V>
    bool Opers<T, U, V>::valid_unary(const Key& key_) noexcept
    {
        return valid_unary_f(key_) || valid_unary_pred(key_)
            || valid_unary_bool(key_);
    }

    template <typename T, typename U, typename V>
    bool Opers<T, U, V>::valid_binary(const Key& key_) noexcept
    {
        return valid_binary_f(key_) || valid_binary_pred(key_)
            || valid_binary_bool(key_);
    }

    template <typename T, typename U, typename V>
    bool Opers<T, U, V>::valid_ternary(const Key& key_) noexcept
    {
        return valid_ternary_f(key_) || valid_ternary_pred(key_)
            || valid_ternary_bool(key_);
    }

    template <typename T, typename U, typename V>
    bool Opers<T, U, V>::valid_nary(const Key& key_, int n) noexcept
    {
        switch (n) {
        case 1: return valid_unary(key_);
        default:
        case 2: return valid_binary(key_);
        case 3: return valid_ternary(key_);
        }
    }

    template <typename T, typename U, typename V>
    bool Opers<T, U, V>::valid(const Key& key_) noexcept
    {
        return valid_f(key_) || valid_pred(key_) || valid_bool(key_);
    }

    template <typename T, typename U, typename V>
    void Opers<T, U, V>::check_unary(const Key& key_)
    {
        expect(valid_unary(key_),
               "Expected "s + type() + " unary operation key, got: "s + key_);
    }

    template <typename T, typename U, typename V>
    void Opers<T, U, V>::check_binary(const Key& key_)
    {
        expect(valid_binary(key_),
               "Expected "s + type() + " binary operation key, got: "s + key_);
    }

    template <typename T, typename U, typename V>
    void Opers<T, U, V>::check_ternary(const Key& key_)
    {
        expect(valid_ternary(key_),
               "Expected "s + type() + " ternary operation key, got: "s + key_);
    }

    template <typename T, typename U, typename V>
    void Opers<T, U, V>::check_nary(const Key& key_, int n)
    {
        switch (n) {
        case 1: return check_unary(key_);
        default:
        case 2: return check_binary(key_);
        case 3: return check_ternary(key_);
        }
    }

    template <typename T, typename U, typename V>
    void Opers<T, U, V>::check(const Key& key_)
    {
        expect(valid(key_), "Expected "s + type() + " operation key, got: "s + key_);
    }
}
