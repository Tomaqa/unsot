#include "expr.hpp"

namespace unsot::expr {
    const Key& Elem_base::cget_key_check() const
    {
        check_is_key();
        return cget_key();
    }

    Key& Elem_base::get_key_check()&
    {
        check_is_key();
        return get_key();
    }

    void Elem_base::check_is_key() const
    {
        expect(is_key(), "Expected key in expr element, got: "s + to_string());
    }

    void Elem_base::check_is_not_key() const
    {
        expect(!is_key(), "Expected a value in expr element, got key: "s + to_string());
    }
}

////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////

namespace unsot::expr {
    bool is_key(const Ptr& ptr) noexcept
    {
        return Elem_base::is_me(ptr) && Elem_base::cast(ptr).is_key();
    }

    void check_is_key(const Ptr& ptr)
    {
        Elem_base::cast_check(ptr).check_is_key();
    }

    bool is_valid_key(const Elem_base& elem) noexcept
    {
        return elem.is_key() && valid_key(elem.cget_key());
    }

    void check_is_valid_key(const Elem_base& elem)
    {
        check_key(elem.cget_key_check());
    }
}
