#include "expr/var/vars_arith.hpp"

#include "expr/var/vars.tpp"

#ifndef NO_EXPLICIT_TP_INST
namespace unsot::expr::var {
    template class aux::_Vars<Int>::Aux_base_t::Base;
    template class aux::_Vars<Real>::Aux_base_t::Base;
    template class aux::_Vars<Int, Real>::Aux_base_t::Base;
    template class aux::_Vars<Int>::Base;
    template class aux::_Vars<Real>::Base;
    template class aux::_Vars<Int, Real>::Base;

    template class aux::_Avars<Int>::Auto_assign_mixin;
    template class aux::_Avars<Real>::Auto_assign_mixin;
}
#endif  /// NO_EXPLICIT_TP_INST
