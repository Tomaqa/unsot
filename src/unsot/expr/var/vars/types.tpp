#pragma once

#include "expr/var/vars/types.hpp"

namespace unsot::expr::var::vars::aux::types {
    template <typename Sort, typename ArgSort, typename ConfT>
    template <typename B>
    Base<Sort, ArgSort, ConfT>::Fun_mixin<B>::
        Fun_mixin(Keys_ptr kptr, Values_ptr vptr, Ids_map_ptr imap_ptr,
                  Id id_, Fun fun_, Args_base_link_ptr args_l_ptr,
                  bool is_set_)
        : Inherit(move(kptr), move(vptr), move(imap_ptr), move(id_),
                  move(fun_), is_set_),
          _args_base_l_ptr(move(args_l_ptr))
    { }

    ////////////////////////////////////////////////////////////////

    template <typename Sort, typename ArgSort, typename ConfT>
    template <typename B>
    Base<Sort, ArgSort, ConfT>::Auto_mixin<B>::
        Auto_mixin(Keys_ptr kptr, Values_ptr vptr, Ids_map_ptr imap_ptr,
                   Id id_, Preds_base_link_ptr preds_l_ptr, bool is_set_)
        : Inherit(move(kptr), move(vptr), move(imap_ptr), move(id_), is_set_),
          _preds_base_l_ptr(move(preds_l_ptr))
    { }
}
