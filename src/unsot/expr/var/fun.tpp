#pragma once

#include "expr/var/fun.hpp"

#include "util/alg.hpp"
#include "util/flag/alg.hpp"
#include "util/numeric/alg.hpp"

namespace unsot::expr::var::fun {
    template <typename B, typename ArgVar, typename FunT>
    Mixin<B, ArgVar, FunT>::Mixin(Keys_ptr kptr, Values_ptr vptr,
                                  Ids_map_ptr imap_ptr, Id id_, Fun fun_,
                                  bool is_set_)
        : Inherit(move(kptr), move(vptr), move(imap_ptr), move(id_), is_set_),
          _fun(move(fun_))
    { }

    template <typename B, typename ArgVar, typename FunT>
    bool Mixin<B, ArgVar, FunT>::lvalid_pre_init() const noexcept
    {
        assert(!empty());
        return cfun().increased_size() == 0;
    }

    template <typename B, typename ArgVar, typename FunT>
    void Mixin<B, ArgVar, FunT>::lcheck_pre_init() const
    {
        using unsot::to_string;
        expect(lvalid_pre_init(),
               "Fun contains undefined keys: "s
               + to_string(cfun().ckeys()) + "[ " + to_string(carg_ids()) + "]");
    }

    template <typename B, typename ArgVar, typename FunT>
    void Mixin<B, ArgVar, FunT>::lrecover_pre_init()
    {
        fun().revert_keys();
    }

    template <typename B, typename ArgVar, typename FunT>
    Flag Mixin<B, ArgVar, FunT>::computable_body() const noexcept
    {
        if (Inherit::computable_body()) return true;
        return evaluable_arg_ids(carg_ids());
    }

    template <typename B, typename ArgVar, typename FunT>
    Flag Mixin<B, ArgVar, FunT>::evaluable_part(Idx idx) const noexcept
    {
        //+ remember also this one inside additional eval. flag
        return evaluable_arg_ids(cfun().cpart_key_ids(idx));
    }

    template <typename B, typename ArgVar, typename FunT>
    Flag Mixin<B, ArgVar, FunT>::surely_evaluable_part(Idx idx) const noexcept
    {
        //+ remember also this one inside additional eval. flag?
        return surely_evaluable_arg_ids(cfun().cpart_key_ids(idx));
    }

    template <typename B, typename ArgVar, typename FunT>
    Flag Mixin<B, ArgVar, FunT>::evaluable_arg_ids(const Arg_ids& aids) const noexcept
    {
        return all_true(aids, [this](auto& aid){
            return carg_ptr(aid)->evaluable();
        });
    }

    template <typename B, typename ArgVar, typename FunT>
    Flag Mixin<B, ArgVar, FunT>::surely_evaluable_arg_ids(const Arg_ids& aids) const noexcept
    {
        return all_true(aids, [this](auto& aid){
            return carg_ptr(aid)->surely_evaluable();
        });
    }

    template <typename B, typename ArgVar, typename FunT>
    bool Mixin<B, ArgVar, FunT>::compute_body_prepare()
    {
        if (!Inherit::compute_body_prepare()) return false;
        eval_arg_ids(carg_ids());
        return true;
    }

    template <typename B, typename ArgVar, typename FunT>
    typename Mixin<B, ArgVar, FunT>::Value
    Mixin<B, ArgVar, FunT>::compute_body_value(bool success)
    {
        if (!success) return Inherit::compute_body_value(success);
        if constexpr (_debug_) return cfun().eval_check();
        else return cfun().eval().value();
    }

    template <typename B, typename ArgVar, typename FunT>
    typename Mixin<B, ArgVar, FunT>::Arg_value
    Mixin<B, ArgVar, FunT>::eval_part(Idx idx)
    {
        eval_arg_ids(cfun().cpart_key_ids(idx));
        return cfun().eval_part(idx);
    }

    template <typename B, typename ArgVar, typename FunT>
    void Mixin<B, ArgVar, FunT>::eval_arg_ids(const Arg_ids& aids)
    {
        assert(computable_body());
        for_each(aids, [this](auto& aid){
            return arg_var(aid).eval();
        });
    }

    template <typename B, typename ArgVar, typename FunT>
    Flag Mixin<B, ArgVar, FunT>::independent() const noexcept
    {
        if (Inherit::independent()) return true;
        if (auto flag = this->computable(); !flag) return flag;

        return all_true(cdepend_ids(), [this](auto& aid){
            return independent_arg(aid);
        });
    }

    template <typename B, typename ArgVar, typename FunT>
    Flag Mixin<B, ArgVar, FunT>::independent_arg(const Id& aid) const noexcept
    {
        return carg_ptr(aid)->independent();
    }

    template <typename B, typename ArgVar, typename FunT>
    String Mixin<B, ArgVar, FunT>::body_to_string() const
    {
        return cfun().Formula::to_string() + Inherit::body_to_string();
    }

    template <typename B, typename ArgVar, typename FunT>
    bool Mixin<B, ArgVar, FunT>::lequals(const This& rhs) const noexcept
    {
        return cformula().equals(rhs.cformula());
    }

    ////////////////////////////////////////////////////////////////

    template <typename B>
    bool Pred_mixin<B>::falsified() const noexcept
    {
        return this->is_set() && !this->cvalue();
    }

    ////////////////////////////////////////////////////////////////

    template <typename B>
    Flag Visit_mixin<B>::evaluable_arg_ids(const Arg_ids& aids) const noexcept
    {
        auto sg = this->template make_visited_ids_scope_guard<Arg_var>();

        return all_true(aids, [this](auto& aid){
            return Arg_var::visit_id(aid, this->carg_ptr(aid),
                                     &Base::evaluable, &Base::surely_evaluable);
        });
    }

    ////////////////////////////////////////////////////////////////

    template <typename B>
    Distance Distance_mixin<B>::compute_distance() noexcept
    {
        //+ Excellent for unsat inputs 'pred_order' and 'eq_diamond'
        //+ ... poor for 'bounce' (and probably other BMC inputs)
        //+ return min(carg_ids(), [this](auto& aid){
        //+ Good for 'bounce' (little worse than the naive approach),
        //+ little worse for unsat inputs than `min',
        //+ but still much better than the naive approach here
        return numeric::max(this->cdepend_ids(), [this](auto& aid){
            if (this->independent_arg(aid)) return 0U;
            return this->arg_var(aid).eval_distance();
        });
    }
}
