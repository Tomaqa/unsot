#include "expr/fun_arith.hpp"

#include "expr/fun.tpp"

#ifndef NO_EXPLICIT_TP_INST
namespace unsot::expr::fun {
    template class Fun_tp<Int>;
    template class Fun_tp<Real>;
    template class Fun_tp<Idx>;

    template class Fun_tp<Int, Fun_conf>;
    template class Fun_tp<Real, Fun_conf>;
    template class Fun_tp<Idx, Fun_conf>;

    template class Fun_tp<Int, Pred_conf>;
    template class Fun_tp<Real, Pred_conf>;
    template class Fun_tp<Idx, Pred_conf>;
}
#endif  /// NO_EXPLICIT_TP_INST
