#include "expr/preprocess.hpp"

#include "util/string/alg.hpp"
#include "util/alg.hpp"
#include "util/scope_guard.hpp"
#include "expr/preprocess/run.hpp"

#include <fstream>
#include <sstream>

#include "expr/preprocess.tpp"

namespace unsot::expr {
    using namespace expr::fun;

    const Preprocess::Reserved_macro_fs_map
    Preprocess::reserved_macro_fs_map{
        {Macro::include_key,                 &Preprocess::parse_macro_include},
        {Macro::def_key,                     &Preprocess::parse_macro_def},
        {Macro::undef_key,                   &Preprocess::parse_macro_undef},
        {Macro::let_key,                     &Preprocess::parse_macro_let},
        {Macro::end_key_of(Macro::let_key),  &Preprocess::parse_macro_endlet},
        {Macro::set_key,                     &Preprocess::parse_macro_set},
        {Macro::if_key,                      &Preprocess::parse_macro_if},
        {Macro::ifdef_key,                   &Preprocess::parse_macro_ifdef},
        {Macro::ifndef_key,                  &Preprocess::parse_macro_ifndef},
        {Macro::isdef_key,                   &Preprocess::parse_macro_isdef},
        {Macro::isndef_key,                  &Preprocess::parse_macro_isndef},
        {Macro::for_key,                     &Preprocess::parse_macro_for},
        {Macro::while_key,                   &Preprocess::parse_macro_while},
        {Macro::null_key,                    &Preprocess::parse_macro_null},
        {Macro::listp_key,                   &Preprocess::parse_macro_listp},
        {Macro::numberp_key,                 &Preprocess::parse_macro_numberp},
        {Macro::refp_key,                    &Preprocess::parse_macro_refp},
        {Macro::equal_key,                   &Preprocess::parse_macro_equal},
        {Macro::eq_key,                      &Preprocess::parse_macro_eq},
        {Macro::len_key,                     &Preprocess::parse_macro_len},
        {Macro::print_key,                   &Preprocess::parse_macro_print},
        {Macro::assert_key,                  &Preprocess::parse_macro_assert},
        {Macro::error_key,                   &Preprocess::parse_macro_error},
        {Macro::car_key,                     &Preprocess::parse_macro_car},
        {Macro::cdr_key,                     &Preprocess::parse_macro_cdr},
        {Macro::nth_key,                     &Preprocess::parse_macro_nth},
        {Macro::nthcdr_key,                  &Preprocess::parse_macro_nthcdr},
        {Macro::ref_name_key,                &Preprocess::parse_macro_ref_name},
    };

    Preprocess::Preprocess(List ls)
        : _list(move(ls))
    { }

    Preprocess::Preprocess(String input)
        : Preprocess(perform_lines(move(input)))
    { }

    Preprocess::Preprocess(istream& is)
        : Preprocess(perform_lines(is))
    { }

    List Preprocess::perform_lines(String input)
    {
        return parse_lines(move(input));
    }

    List Preprocess::perform_lines(istream& is)
    {
        return parse_lines(is);
    }

    List Preprocess::perform_list()
    {
        return perform_list(move(_list));
    }

    List Preprocess::perform_list(List ls)
    {
        if (ls.empty()) return {};
        parse_nested<no_inc>(ls);
        return ls;
    }

    List Preprocess::perform()
    {
        return perform_list();
    }

    List Preprocess::perform(String input)
    {
        return perform_list(perform_lines(move(input)));
    }

    List Preprocess::perform(istream& is)
    {
        return perform_list(perform_lines(is));
    }

    void Preprocess::set_run(Run& run) noexcept
    {
        _run_l = &run;
    }

    Preprocess::Run& Preprocess::run_ref()
    {
        if (auto l = run_l()) return *l;
        THROW("The Preprocess::Run object is not set.");
    }

    namespace {
        template <typename M, typename = If<is_container_v<M>>>
        using Opt_it = Cond_t<is_cref_v<M>, Optional<typename Decay<M>::const_iterator>,
                                            Optional<typename Decay<M>::iterator>>;

        template <typename M>
        Opt_it<M> find_macro_impl(M&& map, const Key& key_)
        {
            if (auto it = map.find(key_); it != end(map))
                return it;
            return {};
        }
    }

    Optional<Preprocess::Macros_map::const_iterator>
    Preprocess::cfind_macro(const Key& key_) const noexcept
    {
        return find_macro_impl(cmacros_map(), key_);
    }

    Optional<Preprocess::Macros_map::iterator>
    Preprocess::find_macro(const Key& key_) noexcept
    {
        return find_macro_impl(macros_map(), key_);
    }

    bool Preprocess::contains(const Key& key_) const noexcept
    {
        return contains_macro(key_) || contains_let(key_);
    }

    void Preprocess::check_contains(const Key& key_) const
    {
        expect(contains(key_), "Identifier '"s + key_ + "' has not been defined.");
    }

    bool Preprocess::contains_macro(const Key& key_) const noexcept
    {
        return cmacros_map().contains(key_);
    }

    void Preprocess::check_contains_macro(const Key& key_) const
    {
        expect(contains_macro(key_), "Macro named '"s + key_ + "' has not been defined.");
    }

    void Preprocess::check_not_contains_macro(const Key& key_) const
    {
        expect(!contains_macro(key_), "Macro named '"s + key_ + "' has already been defined.");
    }

    namespace {
        template <typename M>
        Opt_it<M> find_let_ptr_impl(M&& map, const Key& key_)
        {
            if (auto it_opt = find_macro_impl(map, key_);
                it_opt.valid() && !Let::cast(it_opt.value()->second).undeclared()
            ) return it_opt;
            return {};
        }
    }

    Optional<Preprocess::Let_ptrs_map::const_iterator>
    Preprocess::cfind_let_ptr(const Key& key_) const noexcept
    {
        return find_let_ptr_impl(clet_ptrs_map(), key_);
    }

    Optional<Preprocess::Let_ptrs_map::iterator>
    Preprocess::find_let_ptr(const Key& key_) noexcept
    {
        return find_let_ptr_impl(let_ptrs_map(), key_);
    }

    bool Preprocess::contains_let(const Key& key_) const noexcept
    {
        return cfind_let_ptr(key_).valid();
    }

    void Preprocess::check_contains_let(const Key& key_) const
    {
        expect(contains_let(key_),
               "Let named '"s + key_ + "' is not defined within this context.");
    }

    void Preprocess::add_macro(const Key& key_, Macro macro_)
    {
        macro(key_) = move(macro_);
    }

    void Preprocess::erase_macro(const Key& key_)
    {
        macros_map().erase(key_);
    }

    void Preprocess::pop_let(const Key& key_, bool grouped)
    {
        let(key_).pop(grouped);
    }

    String Preprocess::parse_lines(String&& input)
    {
        istringstream iss(move(input));
        return parse_lines(iss);
    }

    String Preprocess::parse_lines(istream& is)
    {
        static const auto& define_key = Macro::define_key;
        static const auto& endlet_key = Macro::end_key_of(Macro::let_key);

        String str;
        if (int size_ = istream_remain_size(is); size_ > 0)
            str.reserve(size_*1.2);

        String buff;
        buff.reserve(40);
        is >> std::noskipws;
        for (char c; is >> c;) {
            switch (c) {
            default:
                str += c;
                break;
            case '(': case ')':
                str += c;
                break;
            case ';':
                getline(is, buff);
                str += ' ';
                break;
            case macro_char:
                str += macro_char;
                int i = is.peek();
                if (i == istream::traits_type::eof() || isspace(i) || i == ';') {
                    str += escape_char;
                    break;
                }

                const auto pos = is.tellg();
                is >> std::skipws >> buff >> std::noskipws;
                if (buff == define_key)
                    parse_lines_macro_define(is, str);
                else if (buff == endlet_key)
                    parse_lines_macro_endlet(is, str);
                else if (ends_with(buff, Macro::char_str)) {
                    str += buff + escape_char;
                }
                else {
                    is.seekg(pos);
                    break;
                }

                str += ' ';
                break;
            }
        }
        return str;
    }

    void Preprocess::parse_lines_macro_define(istream& is, String& str)
    {
        static const auto full_enddef_key = key_to_macro(Macro::end_key_of(Macro::def_key));

        String tmp;
        getline(is, tmp);
        if (istringstream iss(move(tmp)); iss >> tmp) {
            if (is_macro(tmp) || !util::contains(tmp, '(')) tmp += "()";
            tmp += parse_lines(iss);
        }
        str += Macro::def_key + ' ' + move(tmp) + ' ' + full_enddef_key;
    }

    void Preprocess::parse_lines_macro_endlet(istream& is, String& str)
    {
        static const auto& endlet_key = Macro::end_key_of(Macro::let_key);

        str += endlet_key;

        String tmp;
        getline(is, tmp);
        istringstream iss(move(tmp));
        tmp = parse_lines(iss);

        if (all_of(tmp, isspace)) str += "()";
        else str += move(tmp);
    }

    Ptr Preprocess::peek_let_ref(Pos& pos, int depth)
    {
        const auto& key_ = peek_key_check(pos);
        check_is_let_ref(key_);
        String_view lview = let_ref_to_key_view(key_);
        assert(!lview.empty() || key_ == Macro::two_chars_str);
        return peek_let_ref(pos, move(lview), depth);
    }

    Ptr Preprocess::get_let_ref(Pos& pos, int depth)
    {
        Ptr lptr = peek_let_ref(pos, depth);
        pos.next();
        return lptr;
    }

    Ptr Preprocess::extract_let_ref(Pos& pos, int depth)
    {
        Ptr lptr = peek_let_ref(pos, depth);
        pos.erase();
        return lptr;
    }

    Ptr Preprocess::peek_let_ref(Pos& pos, String_view let_key_vw, int depth)
    {
        assert(is_let_ref_at(pos));
        /// Since `let_key_vw` is view only
        List arg_ls;
        if (let_key_vw.empty()) {
            expect(pos.next() && is_list_at(pos),
                   "Expected a list in reference expansion, got: "s + pos.to_string());
            arg_ls = extract_parse_list(pos, depth);
            //+ `maybe_copy` not used ..
            //+ if it is let ref., create a new let pointing to the ref?
            let_key_vw = cast_as_key_check(as_const(arg_ls));
            pos.prev();
        }

        if (auto it_opt = find_let_ptr(let_key_vw)) {
            return it_opt.value()->second;
        }

        check_contains_let(let_key_vw);
        THROW(error);
    }

    void Preprocess::maybe_escape_key(Pos& pos, Key& key_)
    {
        auto sg = Scope_guard([&pos, &key_]{
            /// Possibly escape the empty key
            if (!pos.at_back()) return;
            if (key_ == Macro::char_str) key_ += escape_char;
        });

        assert(!empty(key_));
        if (!is_escape_char(key_.back())) return;

        expect(invoke([&key_]{
            const size_t size_ = size(key_);
            if (size_ == 1) return false;
            char c = key_[size_-2];
            return is_escape_char(c) || is_macro_char(c);
        }), "Missing character to escape: "s + to_string(key_));
    }

    namespace {
        template <bool first>
        void maybe_merge(Key& key_, Key& succ)
        {
            const bool is_succ_macro = is_macro(succ);
            if (key_ == Macro::char_str && is_succ_macro) {
                if constexpr (first) {
                    key_ += move(succ);
                    succ.clear();
                }
                else {
                    key_ += macro_char;
                    macro_to_key(succ);
                }
                return;
            }

            const size_t size_ = size(key_);
            if (size_ > 0 && is_escape_char(key_.back())
                && (size_ == 1 || !is_escape_char(key_[size_-2]))) {
                key_.pop_back();
                succ = escape(move(succ));
            }
        }

        void merge_back(Keys& keys, Key& key_, Key&& back_k){
            assert(!empty(back_k) && back_k.back() == escape_char);
            assert(size(back_k) <= size(Macro::two_chars_escape_str));
            Key suff = move(back_k);
            do keys.pop_back(); while (!empty(keys) && empty(keys.back()));
            Key& k = empty(keys) ? key_ : keys.back();
            k += move(suff);
        }
    }

    Keys Preprocess::split_key(Key& key_)
    {
        auto keys = inplace_split_if<Keys>(key_, [](char c){
            return is_macro_char(c) || is_escape_char(c) || is_arith_exp_char(c);
        });
        if (empty(keys)) return {};
        auto& front_k = keys.front();
        maybe_merge<true>(key_, front_k);
        if (empty(key_) && size(keys) == 1) {
            key_ = move(front_k);
            return {};
        }

        for (Idx bound = size(keys)-1, i = 0; i < bound; ++i) {
            maybe_merge<false>(keys[i], keys[i+1]);
        }
        while (!empty(keys) && empty(keys.back())) keys.pop_back();
        if (empty(keys)) return {};

        auto& back_k = keys.back();
        if (back_k == escape_char_str) {
            merge_back(keys, key_, move(back_k));
            if (!empty(keys)) {
                auto& bk = keys.back();
                if (is_cat(bk)) merge_back(keys, key_, move(bk));
            }
        }

        return keys;
    }

    void Preprocess::parse_key_multi(Pos& pos, Keys&& keys, int depth)
    {
        int size_ = size(keys);
        auto& bkey = keys.back();
        const bool cat_last = is_cat(bkey);
        if (cat_last) rm_prefix_cat_attr(bkey);

        {
            const auto bit = pos.it();
            pos.next();
            for_each(make_move_iterator(begin(keys)), make_move_iterator(--end(keys)),
                     [&pos, &size_](auto&& k){
                if (k != Macro::two_chars_str) pos.emplace(new_elem(move(k)));
                else --size_;
            });
            pos.emplace(new_elem(move(keys.back())));
            assert(size_ > 0);
            pos.set_it(bit);
        }

        parse_key_single_not_arith<Expand_lets::yes>(pos, peek_key(pos), depth);
        /// use prev to ensure the iterator will remain valid (and e.g. not deleted)
        auto last_it = std::prev(pos.it(), 1);
        for (int i = 0; i < size_; ++i) {
            assert(!is_let_at(pos));
            if (!is_key_at(pos)) {
                parse_value(pos, depth);
            }
            else {
                auto& key_ = peek_key(pos);
                if (is_arith_exp(key_)) {
                    parse_eval_arith_exp(pos, depth);
                    --size_;
                }
                else {
                    parse_key_single_not_arith<Expand_lets::yes>(pos, key_, depth);
                }
            }
            const auto it = pos.it();
            pos.set_it(last_it);
            assert(!is_let_at(pos));
            suffix(pos);
            pos.set_it(it);
            last_it = std::prev(it, 1);
        }

        if (cat_last) cat(pos, depth);
    }

    void Preprocess::cat(Pos& pos, int depth)
    {
        expect(!pos.at_front(), "Expected left argument to concatenation.");
        if (pos) {
            assert(!is_let_at(pos));
            expect(!is_key_at(pos) || !is_infix_cat(peek_key(pos)),
                   "Consecutive concatenation operators: "s + pos.to_string());
            if (is_list_at(pos)) expand_list(pos, depth);
            else expand_elem<Expand_lets::yes>(pos, depth);
        }
        expect(pos, "Expected right argument to concatenation.");
        const bool r_is_list = is_list_at(pos);
        pos.prev();
        maybe_expand_let_at(pos);
        const bool l_is_list = is_list_at(pos);
        expect(l_is_list || is_key_at(pos),
               "Left argument of concatenation must be a key or a list.");
        assert(!is_let_at(pos));

        if (!l_is_list && !r_is_list) cat_key_elem(pos);
        else if (l_is_list && r_is_list) cat_lists(pos);
        else if (l_is_list && !r_is_list) cat_list_elem(pos);
        else cat_key_list(pos);

        assert(!pos.at_begin());
        assert(!is_let_at(pos.prev()) && !pos.next().at_begin());
    }

    void Preprocess::suffix(Pos& pos)
    {
        if (is_list_at(pos)) suffix_list(pos);
        else suffix_key(pos);
    }

    void Preprocess::suffix_key(Pos& pos)
    {
        assert(is_elem_at(pos));
        expect(is_key_at(pos),
               "Left argument of concatenation cannot be a value: "s + pos.to_string());
        const Ptr& rhs = *std::next(pos.it(), 1);
        if (List::is_me(rhs)) return cat_key_list(pos);
        cat_key_elem(pos);
    }

    void Preprocess::suffix_list(Pos& pos)
    {
        const Ptr& rhs = *std::next(pos.it(), 1);
        if (List::is_me(rhs)) cat_lists(pos);
        else cat_list_elem(pos);
    }

    void Preprocess::cat_key_elem(Pos& pos)
    {
        maybe_deep_copy_at(pos);
        assert(is_key_at(pos));
        auto& key_ = get_key(pos);
        String suff = extract_maybe_copy_elem(pos).to_string();
        suffix_key(key_, move(suff));
    }

    void Preprocess::cat_key_list(Pos& pos)
    {
        assert(is_key_at(pos));
        String pref = extract_maybe_copy_key(pos);
        maybe_deep_copy_at(pos);
        auto& ls = get_list(pos);
        prefix_list(ls, move(pref));
    }

    void Preprocess::cat_lists(Pos& pos)
    {
        maybe_shallow_copy_at(pos);
        auto& ls = get_list(pos);
        ls.splice(ls.end(), extract_maybe_shallow_copy_list(pos));
    }

    void Preprocess::cat_list_elem(Pos& pos)
    {
        maybe_deep_copy_at(pos);
        auto& ls = get_list(pos);
        String suff = extract_maybe_copy_elem(pos).to_string();
        suffix_list(ls, move(suff));
    }

    void Preprocess::suffix(Ptr& ptr, String suff)
    {
        if (List::is_me(ptr)) return suffix_list(List::cast(ptr), move(suff));
        suffix_key(cast_key_check(ptr), move(suff));
    }

    void Preprocess::prefix(Ptr& ptr, String pref)
    {
        if (List::is_me(ptr)) prefix_list(List::cast(ptr), move(pref));
        else prefix_elem(Elem::cast(ptr), move(pref));
    }

    void Preprocess::suffix_key(Key& key_, String suff)
    {
        key_ += move(suff);
    }

    void Preprocess::prefix_elem(Elem& elem, String pref)
    {
        if (elem.is_key()) return prefix_key(elem.get_key(), move(pref));
        String str = move(elem).to_string();
        prefix_key(str, move(pref));
        elem.emplace<Key>(move(str));
    }

    void Preprocess::prefix_key(Key& key_, String pref)
    {
        key_ = move(pref)+move(key_);
    }

    void Preprocess::suffix_list(List& ls, String suff)
    {
        for (auto& ptr : ls) suffix(ptr, suff);
    }

    void Preprocess::prefix_list(List& ls, String pref)
    {
        for (auto& ptr : ls) prefix(ptr, pref);
    }

    void Preprocess::parse_macro_include(Pos& pos, int depth)
    {
        expect(is_key_at(pos) || is_let_at(pos),
               "Expected filename path, got: "s + pos.to_string());
        //! expand<Expand_lets::copy>(pos, depth);
        expand<Expand_lets::yes>(pos, depth);
        const Path path = extract_maybe_copy_key_check(pos);

        ifstream ifs(run_ref().open_ifstream(path));
        List ls = parse_lines(ifs);
        parse_nested<no_inc>(ls, depth);
        pos.splice(move(ls));
    }

    void Preprocess::parse_macro_def(Pos& pos, int depth)
    {
        const Ptr mptr = extract_macro_key(pos, depth);
        auto& mkey = cast_key(mptr);
        expect(!util::contains(mkey, macro_char),
               "User macro key must not contain macro char '#': "s + to_string(mkey));
        Macro::check_is_not_reserved_key(mkey);
        check_not_contains_macro(mkey);

        auto body = extract_macro_body(pos, Macro::def_key);
        Pos body_pos(body);
        body_pos.check_not_at_end();
        /// Expand and extract arguments list
        expand<Expand_lets::ban>(body_pos, depth);
        const Ptr aptr = body_pos.extract_check();
        auto& ls = List::cast_check(aptr);
        auto arg_keys = cast_to_keys_check(ls);

        add_macro(mkey, {move(arg_keys), move(body)});
    }

    void Preprocess::parse_macro_undef(Pos& pos, int depth)
    {
        const Ptr mptr = extract_macro_key(pos, depth);
        auto& mkey = cast_key(mptr);
        check_contains_macro(mkey);
        erase_macro(mkey);
    }

    void Preprocess::parse_macro_let(Pos& pos, int depth)
    {
        pos.check_not_at_end();
        if (!is_list_at(pos)) {
            const Ptr ptr = extract_macro_key(pos, depth);
            auto& lkey = cast_key(ptr);
            return push_let_grouped_extract<false>(pos, depth, lkey);
        }

        List args_ls = extract_list(pos);
        Keys lkeys;
        lkeys.reserve(args_ls.size()+1);
        ++depth;
        Pos args_pos(args_ls);
        while (args_pos) {
            /// Expansion is postponed until now to allow lets to be dependent on previous ones
            expand<Expand_lets::no>(args_pos, depth);
            if (!args_pos) break;
            Ptr ptr;
            if (!is_list_at(args_pos)) {
                ptr = extract_macro_key<no_exp>(args_pos, depth);
                push_let_grouped_extract<true, false>(args_pos, depth, cast_key(ptr));
            }
            else {
                List arg_ls = extract_list(args_pos);
                expect(arg_ls.size() == 2 && is_key(arg_ls.cfront()),
                       "Expected let key and body, got: "s + arg_ls.to_string());
                Pos arg_pos(arg_ls);
                ptr = extract_macro_key<no_exp>(arg_pos, depth);
                push_let_grouped_extract<true>(arg_pos, depth, cast_key(ptr));
            }
            lkeys.push_back(cast_key(ptr));
        }

        expect(!lkeys.empty(),
               "Expected a group of (possibly initialized) variables.");
        _let_keys_stack.push(move(lkeys));
    }

    void Preprocess::parse_macro_endlet(Pos& pos, int depth)
    {
        bool grouped = false;
        auto pop_f = [this, &grouped](const Key& lkey){
            check_contains_let(lkey);
            pop_let(lkey, grouped);
        };

        expand<Expand_lets::no>(pos, depth);
        if (pos && !is_list_at(pos)) {
            const Ptr ptr = extract_macro_key<no_exp>(pos, depth);
            auto& lkey = cast_key(ptr);
            return pop_f(lkey);
        }

        Keys lkeys;
        if (pos) {
            List keys_ls = extract_list(pos);
            check_is_flat(keys_ls);
            lkeys.reserve(keys_ls.size());
            Pos keys_pos(keys_ls);
            while (keys_pos) {
                const Ptr ptr = extract_macro_key<no_exp>(keys_pos, depth+1);
                auto& lkey = cast_key(ptr);
                lkeys.push_back(lkey);
            }
        }
        if (empty(lkeys)) {
            expect(!empty(_let_keys_stack),
                   "No preceding group "s + key_to_macro(Macro::let_key) + " found.");
            lkeys = move(_let_keys_stack.top());
            _let_keys_stack.pop();
            grouped = true;
        }

        for_each(lkeys, move(pop_f));
    }

    void Preprocess::parse_macro_set(Pos& pos, int depth)
    {
        expand<Expand_lets::no>(pos, depth);
        Ptr lptr;
        if (is_let_at(pos)) {
            lptr = pos.extract();
        }
        else {
            const Ptr ptr = extract_macro_key<no_exp>(pos, depth);
            auto& lkey = cast_key(ptr);
            auto it_opt = find_let_ptr(lkey);
            if (it_opt.invalid() || it_opt.value() == end(let_ptrs_map())) {
                check_contains_let(lkey);
                THROW(error);
            }
            lptr = it_opt.value()->second;
        }
        auto& let_ = Let::cast(lptr);
        set_let_extract_tp(pos, depth, [&let_](Ptr ptr){ let_.set(move(ptr)); });
    }

    void Preprocess::parse_macro_if(Pos& pos, int depth)
    {
        const bool cond = extract_macro_if_head(pos, depth);
        parse_macro_if_body(pos, depth, cond);
    }

    void Preprocess::parse_macro_ifdef(Pos& pos, int depth)
    {
        const bool cond = extract_macro_isdef(pos, depth);
        parse_macro_if_body(pos, depth, cond);
    }

    void Preprocess::parse_macro_ifndef(Pos& pos, int depth)
    {
        const bool cond = extract_macro_isdef<do_neg>(pos, depth);
        parse_macro_if_body(pos, depth, cond);
    }

    void Preprocess::parse_macro_for(Pos& pos, int depth)
    {
        Pos args_pos(extract_parse_list_check<no_inc, Expand_lets::no>(pos, depth));
        ++depth;
        //+ allow multiple variables (-> list_at_pos)
        /// Do not destroy the key
        const Ptr key_ptr = args_pos.extract_check();
        const Key& var = cast_key_check(key_ptr);
        Macro::check_is_not_reserved_key(var);
        args_pos.check_not_at_end();
        Let& let_ = push_let(var);

        Macro_for_loop_f loop_f;
        if (args_pos.at_back()) {
            /// Variant 3 - explicit arguments
            auto ptr = args_pos.peek();
            maybe_expand_let(ptr);
            check_is_seq(ptr);
            loop_f = parse_macro_for3_args(let_, move(ptr));
            args_pos.next();
        }
        else {
            Elem init = extract_eval_arith_elem<no_exp>(args_pos, depth);
            loop_f = is_list_at(args_pos)
                   /// Variant 2 - condition and step functions
                   ? parse_macro_for2_args(args_pos, depth, let_, move(init))
                   /// Variant 1 - discrete range
                   : parse_macro_for1_args(args_pos, let_, move(init));
        }
        expect(!args_pos, "Additional arguments: "s + args_pos.to_string());

        const auto body = extract_macro_body(pos, Macro::for_key);
        auto loop_step_f = bind(&This::macro_for_loop_step, this, ref(pos), depth, cref(body));

        invoke(move(loop_f), move(loop_step_f));
        pop_let(var);
    }

    void Preprocess::parse_macro_while(Pos&, int /*depth*/)
    {
        //++ like 'if', but the condition must be evaluated repeatadly
        THROW("Not implemented yet.");
    }

    void Preprocess::parse_macro_print(Pos& pos, int depth)
    {
        expand<Expand_lets::no>(pos, depth);
        const auto ptr = pos.extract_check();
        std::cerr << ptr << std::endl;
    }

    void Preprocess::parse_macro_assert(Pos& pos, int depth)
    {
        String msg = pos.cpeek_check()->to_string();
        if (!pos.at_back()) {
            const bool is_ar = is_arith_exp(msg);
            const bool is_mac = !is_ar && is_macro(msg);
            if (is_ar || is_mac) {
                pos.next();
                invoke([&]{
                    if (is_mac) {
                        if (!is_list_at(pos)) return;
                        auto k = macro_to_key_view(msg);
                        if (!contains_macro(k) || empty(cmacro(k).arg_keys))
                            return;
                    }
                    msg += " " + pos.cpeek()->to_string();
                });
                pos.prev();
            }
        }
        const bool cond = extract_eval_arith_pred(pos, depth);
        expect(cond, "Preprocessor assertion failed: " + msg);
    }

    void Preprocess::parse_macro_error(Pos& pos, int depth)
    {
        expand<Expand_lets::no>(pos, depth);
        const auto ptr = pos.extract_check();
        THROW("Preprocessor error invoked: " + move(ptr)->to_string());
    }

    void Preprocess::parse_macro_ref_name(Pos& pos, int depth)
    {
        auto ls = extract_parse_list_check<do_inc, Expand_lets::no>(pos, depth);
        expect(ls.size() == 1 && Let::is_me(ls.front()),
               "Expected list with a let reference, got: "s + ls.to_string());
        auto& let_ = Let::cast(ls.front());
        pos.emplace(new_key(let_.ckey()));
    }

    Macro::Body Preprocess::extract_macro_body(Pos& pos, Key key_)
    {
        const Key end_key = key_to_macro(Macro::end_key_of(key_));
        key_ = key_to_macro(move(key_));
        Macro::Body body;
        bool found = false;
        int nested_cnt = 0;
        while (pos) {
            Ptr ptr = pos.extract();
            if (is_key(ptr)) {
                const auto& k = cast_key(ptr);
                if (k == end_key) {
                    if (nested_cnt == 0) {
                        found = true;
                        break;
                    }
                    --nested_cnt;
                }
                if (k == key_) ++nested_cnt;
            }
            body.emplace_back(move(ptr));
        }
        expect(found, to_string(end_key) + " not found.");

        return body;
    }

    bool Preprocess::extract_macro_if_head(Pos& pos, int depth)
    {
        //+ optimize if the arg. is a predicate macro ('null' etc.)
        return extract_eval_arith_pred(pos, depth);
    }

    void Preprocess::parse_macro_if_body(Pos& pos, int depth, bool cond)
    {
        static const auto full_if_key = key_to_macro(Macro::if_key);
        static const auto full_elif_key =
            key_to_macro(Macro::if_elif_key);
        static const auto full_else_key =
            key_to_macro(Macro::if_else_key);
        static const auto full_ifdef_key =
            key_to_macro(Macro::ifdef_key);
        static const auto full_ifndef_key =
            key_to_macro(Macro::ifndef_key);
        static const auto full_endif_key =
            key_to_macro(Macro::end_key_of(Macro::if_key));

        bool picked = cond;
        bool del = !picked;
        int nested_del_cnt = 0;
        while (pos) {
            const bool is_list = is_list_at(pos);
            if (is_list || !is_key_at(pos)) {
                if (del) {
                    pos.erase();
                    continue;
                }
                if (is_list) parse_list(pos, depth);
                else if (is_let_at(pos)) parse_let(pos, depth);
                else parse_value(pos, depth);
                continue;
            }

            auto& key_ = peek_key(pos);
            if (key_ == full_endif_key) {
                pos.erase();
                if (!nested_del_cnt) return;
                --nested_del_cnt;
                continue;
            }
            const bool is_else = (key_ == full_else_key);
            if (is_else || key_ == full_elif_key) {
                pos.erase();
                if (nested_del_cnt) continue;

                if (picked) del = true;
                else {
                    picked = is_else || extract_macro_if_head(pos, depth);
                    del = !picked;
                }
                continue;
            }
            if (del) {
                if (key_ == full_if_key
                    || key_ == full_ifdef_key || key_ == full_ifndef_key
                ) ++nested_del_cnt;
                pos.erase();
                continue;
            }
            parse_key(pos, depth);
        }

        THROW("'"s + full_endif_key + "' not found.");
    }

    Preprocess::Macro_for_loop_f
    Preprocess::parse_macro_for1_args(Pos& args_pos, Let& let_, Elem init)
    {
        Int init_i, end_i;
        try {
            init_i = init.get_value<Int>();
            end_i = peek_int_check(args_pos);
        }
        catch (const Error&) {
            THROW("Expected two integers, got: ")
                + to_string(init) + " " + args_pos.cpeek()->to_string();
        }
        args_pos.erase();

        Ptr& ptr = let_.ptr();
        return [&ptr, init_i, end_i](Macro_for_loop_step_f step_f){
            for (Int i = init_i; i <= end_i; ++i) {
                ptr = new_int(i);
                invoke(step_f);
            }
        };
    }

    Preprocess::Macro_for_loop_f
    Preprocess::parse_macro_for2_args(Pos& args_pos, int depth, Let& let_, Elem init)
    {
        auto init_x = move(init).to_value<Real>().value();
        auto& var = let_.ckey();

        auto pred_fun = extract_eval_arith_cons_fun<Pred<Real>>(
            args_pos, depth, {var}
        );
        auto pred = [CAPTURE_RVAL(pred_fun)](Real x){
            return pred_fun({x});
        };

        auto next_fun = extract_eval_arith_cons_fun<Fun<Real>>(
            args_pos, depth, {var}
        );
        auto next_f = [CAPTURE_RVAL(next_fun)](Real x){
            return next_fun({x});
        };

        Ptr& ptr = let_.ptr();
        return [&ptr, init_x, CAPTURE_RVAL2(pred, next_f)](Macro_for_loop_step_f step_f){
            for (Real x = init_x; pred(x); x = next_f(x)) {
                ptr = new_real(x);
                invoke(step_f);
            }
        };
    }

    Preprocess::Macro_for_loop_f Preprocess::parse_macro_for3_args(Let& let_, Ptr ptr)
    {
        return [&let_, CAPTURE_RVAL(ptr)](Macro_for_loop_step_f step_f){
            if (List::is_me(ptr)) for (const auto& p : List::cast(ptr)) {
                let_.ptr() = p;
                invoke(step_f);
            }
            else for (const auto& c : cast_key(ptr)) {
                let_.ptr() = new_elem(c);
                invoke(step_f);
            }
        };
    }

    void Preprocess::macro_for_loop_step(Pos& pos, int depth, const Macro::Body& body)
    {
        auto body_cp = body;
        parse_nested(body_cp, depth);
        pos.splice(move(body_cp));
    }

    void Preprocess::parse_user_macro_push_args(Pos& pos, const Key& key_, int depth)
    {
        auto& arg_keys = cmacro(key_).arg_keys;
        const int asize = size(arg_keys);
        if (asize == 0) return;

        expand<Expand_lets::no>(pos, depth);
        auto args_ls = extract_maybe_deep_copy_list_check(pos);
        const int als_size = args_ls.size();
        expect(als_size <= asize,
               "Too many arguments: expected "s + to_string(asize)
               + ", got: " + to_string(als_size));

        Pos args_pos(args_ls);
        ++depth;
        int i = 0;
        for (; i < als_size; ++i) {
            const auto& akey = arg_keys[i];
            auto ptr = args_pos.get();
            push_let(akey, move(ptr));
        }
        /// Omitted parameters are defaulted to empty keys
        for (; i < asize; ++i) {
            const auto& akey = arg_keys[i];
            push_let(akey, new_key());
        }
        assert(!args_pos);
    }

    void Preprocess::parse_user_macro_pop_args(const Key& key_)
    {
        //+ grouped?
        for (auto& akey : cmacro(key_).arg_keys) pop_let(akey);
    }

    Elem Preprocess::extract_eval_arith_exp(Pos& pos, int depth)
    {
        const Key key_ = extract_key(pos);
        const bool is_int_ = is_arith_exp_and_int_check(key_);

        expect(pos, "Expected arithmetic expression after '"s + to_string(key_) + "' key.");

        expand<Expand_lets::all>(pos, depth);
        const auto ptr = pos.extract_check();
        if (is_int_) return eval_check<Int>(ptr);
        else return eval_check<Real>(ptr);
    }

    template <bool isCarOrCdr, bool isCarOrNth, typename NewPtrF>
    Ref<Ptr> Preprocess::peek_accessor_tp(Pos& pos, int depth, NewPtrF new_ptr)
    {
        auto& args_ls = peek_parse_list_check<do_inc, Expand_lets::no>(pos, depth);
        Pos args_pos(args_ls);
        [[maybe_unused]] Int idx;
        if constexpr (isCarOrCdr) {
            expect(args_ls.size() == 1, "Expected single argument, got: "s + args_ls.to_string());
        }
        else {
            expect(args_ls.size() == 2 && is_int_at(args_pos),
                   "Expected (<idx> <arg>), got: "s + args_pos.to_string());

            idx = get_int(args_pos);
            expect(idx >= 0, "Expected non-negative index, got: "s + to_string(idx));
        }

        auto& ptr = args_pos.peek();
        maybe_expand_let(ptr);
        check_is_seq(ptr);

        if constexpr (isCarOrCdr) return Ref<Ptr>(invoke(move(new_ptr), *this, ptr));
        else return Ref<Ptr>(invoke(move(new_ptr), *this, ptr, idx));
    }

    template <bool isCarOrCdr, bool isCarOrNth, typename NewPtrF>
    void Preprocess::parse_accessor_tp(Pos& pos, int depth, NewPtrF new_ptr)
    {
        auto ref = peek_accessor_tp<isCarOrCdr, isCarOrNth>(pos, depth, move(new_ptr));
        auto& ptr = pos.peek();
        if constexpr (!isCarOrNth) ptr = move(ref).item();
        else if (ref.is_owner()) ptr = move(ref).item();
        else ptr = ref.citem();
        pos.next();
    }

    Ref<Ptr> Preprocess::peek_macro_car_impl(Ptr& src_ptr)
    try {
        if (is_key(src_ptr)) {
            const auto& key_ = cast_key(src_ptr);
            if (empty(key_)) throw dummy;
            return Ref<Ptr>(new_key(key_.front()));
        }

        auto& ls = List::cast(src_ptr);
        if (ls.empty()) throw dummy;
        return Ref<Ptr>(ls.front());
    }
    catch (Dummy) {
        THROW("Attempt to access first element of an empty sequence.");
    }

    Ptr Preprocess::peek_macro_cdr_impl(const Ptr& src_ptr)
    {
        if (is_key(src_ptr)) {
            auto& key_ = cast_key(src_ptr);
            if (empty(key_)) return new_key();
            return new_elem(key_.substr(1));
        }

        auto& src_ls = List::cast(src_ptr);
        return List::shallow_copy(++src_ls.cbegin(), src_ls.cend()).to_ptr();
    }

    Ref<Ptr> Preprocess::peek_macro_nth_impl(Ptr& src_ptr, Idx idx)
    try {
        if (is_key(src_ptr)) {
            const auto& key_ = cast_key(src_ptr);
            if (idx >= int(size(key_))) throw dummy;
            return Ref<Ptr>(new_elem(key_.substr(idx, 1)));
        }

        auto& src_ls = List::cast(src_ptr);
        if (idx >= int(src_ls.size())) throw dummy;
        auto& ptr = *std::next(src_ls.begin(), idx);
        return Ref<Ptr>(ptr);
    }
    catch (Dummy) {
        THROW("Attempt to access out of bounds [") + to_string(idx) + "] element of a sequence.";
    }

    Ptr Preprocess::peek_macro_nthcdr_impl(const Ptr& src_ptr, Idx idx)
    {
        if (is_key(src_ptr)) {
            auto& key_ = cast_key(src_ptr);
            if (idx >= int(size(key_))) return new_key();
            return new_elem(key_.substr(idx));
        }

        auto& src_ls = List::cast(src_ptr);
        if (idx >= int(src_ls.size())) return List::new_me();
        const auto bit = src_ls.begin();
        auto it = std::next(bit, idx);
        return List::shallow_copy(it, src_ls.end()).to_ptr();
    }
}

////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////

namespace unsot::expr {
    template class Preprocess::Foo<true, false>;
    template class Preprocess::Foo<true, true>;
    template class Preprocess::Foo<false, false>;
    template class Preprocess::Foo<false, true>;

    template class Preprocess::Foo_exp_lets<int(Preprocess::Expand_lets::neutral)>;
    template class Preprocess::Foo_exp_lets<int(Preprocess::Expand_lets::no)>;
    template class Preprocess::Foo_exp_lets<int(Preprocess::Expand_lets::yes)>;
    template class Preprocess::Foo_exp_lets<int(Preprocess::Expand_lets::all)>;
    template class Preprocess::Foo_exp_lets<int(Preprocess::Expand_lets::copy)>;
    template class Preprocess::Foo_exp_lets<int(Preprocess::Expand_lets::ban)>;
}

////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////

namespace unsot::expr {
    bool is_macro(String_view view) noexcept
    {
        return is_macro_char(view.front());
    }

    void check_is_macro(String_view view)
    {
        expect(is_macro(view), "Expected macro, got: "s + to_string(view));
    }

    void check_is_not_macro(String_view view)
    {
        if (!is_macro(view)) return;
        String msg = "Unexpected macro ";
        const bool is_let_ = is_let_ref(view);
        if (is_let_) msg += "(nor reference) ";
        msg += "'" + to_string(view) + "', did you mean '";
        if (is_let_) msg += Macro::two_chars_str + " " + let_ref_to_key(view);
        else msg += macro_to_key(view);
        msg += "'?";
        THROW(move(msg));
    }

    bool is_macro_char(char c) noexcept
    {
        return c == macro_char;
    }

    bool is_let_ref(String_view view) noexcept
    {
        return starts_with(view, Macro::two_chars_str)
            && view.back() != escape_char;
    }

    void check_is_let_ref(String_view view)
    {
        expect(is_let_ref(view),
               "Expected macro reference, got: "s + to_string(view));
    }

    void check_is_not_let_ref(String_view view)
    {
        expect(!is_let_ref(view),
               "Unexpected macro reference '"s + to_string(view)
               + "', did you mean '" + macro_to_key(view) + "'?");
    }

    bool is_cat(String_view view) noexcept
    {
        return ends_with(view, Macro::two_chars_escape_str);
    }

    bool is_infix_cat(String_view view) noexcept
    {
        return view == Macro::two_chars_escape_str;
    }

    bool is_prefix_cat(String_view view) noexcept
    {
        assert(size(Macro::two_chars_escape_str) == 3);
        return is_cat(view) && view.size() > 3;
    }

    namespace {
        inline bool lis_int_arith_exp(String_view view) noexcept
        {
            static const String chars = "di";
            return view.size() == 2 && util::contains(chars, view[1]);
        }

        inline bool lis_real_arith_exp(String_view view) noexcept
        {
            return view.size() == 1 || (view.size() == 2 && view[1] == 'f');
        }
    }

    bool is_arith_exp(String_view view) noexcept
    {
        return is_arith_exp_char(view.front());
    }

    bool is_int_arith_exp(String_view view) noexcept
    {
        return is_arith_exp(view) && lis_int_arith_exp(view);
    }

    bool is_real_arith_exp(String_view view) noexcept
    {
        return is_arith_exp(view) && lis_real_arith_exp(view);
    }

    bool is_arith_exp_char(char c) noexcept
    {
        return c == arith_exp_char;
    }

    void check_is_arith_exp(String_view view)
    {
        expect(is_arith_exp(view),
               "Expected arithmetic expansion key, got: "s + to_string(view));
    }

    void check_is_int_arith_exp(String_view view)
    {
        expect(is_int_arith_exp(view),
               "Expected integer arithmetic expansion key, got: "s
               + to_string(view));
    }

    void check_is_real_arith_exp(String_view view)
    {
        expect(is_real_arith_exp(view),
               "Expected real arithmetic expansion key, got: "s
               + to_string(view));
    }

    Optional<bool> is_arith_exp_and_int(String_view view) noexcept
    {
        if (!is_arith_exp(view)) return {};
        if (lis_int_arith_exp(view)) return true;
        if (lis_real_arith_exp(view)) return false;
        return {};
    }

    bool is_arith_exp_and_int_check(String_view view)
    {
        auto opt = is_arith_exp_and_int(view);
        if (opt.valid()) return opt.cvalue();
        check_is_arith_exp(view);
        check_is_int_arith_exp(view);
        check_is_real_arith_exp(view);
        THROW(error);
    }

    bool is_escaped(String_view view) noexcept
    {
        return is_escape_char(view.front());
    }

    bool is_escape_char(char c) noexcept
    {
        return c == escape_char;
    }

    bool is_let_ref_at(const Pos& pos) noexcept
    {
        return is_key_at(pos) && is_let_ref(peek_key(pos));
    }

    void check_is_let_ref_at(const Pos& pos)
    {
        check_is_key_at(pos);
        check_is_let_ref(peek_key(pos));
    }

    bool is_let_at(const Pos& pos) noexcept
    {
        return Let::is_me(pos.cpeek());
    }

    void check_is_let_at(const Pos& pos)
    {
        return Let::check_is_me(pos.cpeek_check());
    }

    void check_is_not_let_at(const Pos& pos)
    {
        return Let::check_is_not_me(pos.cpeek_check());
    }

    Let extract_let(Pos& pos)
    {
        return Let::cast(pos.extract());
    }

    Let extract_let_check(Pos& pos)
    {
        return Let::cast_check(pos.extract_check());
    }

    Key key_to_macro(Key key_)
    {
        return Macro::char_str + move(key_);
    }

    void macro_to_key(Key& macro)
    {
        macro.erase(0, 1);
    }

    Key&& macro_to_key(Key&& macro)
    {
        macro_to_key(macro);
        return move(macro);
    }

    String_view macro_to_key_view(const Key& macro) noexcept
    {
        String_view view(macro);
        view.remove_prefix(1);
        return view;
    }

    Key key_to_let_ref(Key key_)
    {
        return Macro::two_chars_str + move(key_);
    }

    void let_ref_to_key(Key& lref)
    {
        lref.erase(0, 2);
    }

    Key&& let_ref_to_key(Key&& lref)
    {
        let_ref_to_key(lref);
        return move(lref);
    }

    String_view let_ref_to_key_view(const Key& lref) noexcept
    {
        String_view view(lref);
        view.remove_prefix(2);
        return view;
    }

    void rm_prefix_cat_attr(Key& key_)
    {
        assert(size(Macro::two_chars_escape_str) == 3);
        key_.erase(size(key_)-3);
    }

    Key escape(Key key_)
    {
        return escape_char_str + move(key_);
    }

    void unescape(Key& key_)
    {
        key_.erase(0, 1);
        assert(!empty(key_));
        if (size(key_) == 1) return;
        if (is_escape_char(key_.back())) key_.pop_back();
    }

    Key&& unescape(Key&& key_)
    {
        unescape(key_);
        return move(key_);
    }
}
