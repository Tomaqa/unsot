#include "expr/preprocess/run.hpp"

#include "util/string/alg.hpp"
#include "expr/alg.hpp"

namespace unsot::expr {
    String Preprocess::Run::usage() const
    {
        return Inherit::usage() + lusage();
    }

    String Preprocess::Run::lusage() const
    {
        return usage_row('D', "Define user macro (<id>=<content>)");
    }

    List Preprocess::Run::preprocess()
    {
        init_preprocess();
        List ls = preprocess_lines();
        return preprocess_list(move(ls));
    }

    void Preprocess::Run::init_preprocess()
    {
        _preprocess.set_run(*this);
        for (auto& [key, val] : _macros_map) {
            _preprocess.add_macro(key, {{}, val});
        }
    }

    //! does not work with here-strings !
    List Preprocess::Run::preprocess_lines()
    {
        return _preprocess.perform_lines(is());
    }

    List Preprocess::Run::preprocess_list(List&& ls)
    {
        return _preprocess.perform_list(move(ls));
    }

    void Preprocess::Run::do_stuff()
    {
        os() << to_lines(preprocess());
    }

    String Preprocess::Run::getopt_str() const noexcept
    {
        return Inherit::getopt_str() + lgetopt_str();
    }

    String Preprocess::Run::lgetopt_str() const noexcept
    {
        return "D:";
    }

    bool Preprocess::Run::process_opt(char c)
    {
        if (Inherit::process_opt(c)) return true;
        return lprocess_opt(c);
    }

    bool Preprocess::Run::lprocess_opt(char c)
    {
        switch (c) {
        case 'D': {
            auto [key, val] = split_to_pair(optarg, '=');
            _macros_map.emplace(move(key), move(val));
            return true;
        }}

        return false;
    }
}
