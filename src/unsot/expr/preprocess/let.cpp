#include "expr/preprocess.hpp"

#include "util/alg.hpp"

namespace unsot::expr {
    Ptr Let::to_ptr() const&
    {
        auto ptr_ = expand();
        return as_const(ptr_)->to_ptr();
    }

    const Ptr& Let::cptr_check() const
    {
        check_not_undeclared();
        check_not_undefined();
        assert(!is_self_ref());
        return cptr();
    }

    Ptr& Let::ptr_check()&
    {
        check_not_undeclared();
        check_not_undefined();
        assert(!is_self_ref());
        return ptr();
    }

    void Let::check_not_undeclared() const
    {
        expect(!undeclared(), undeclared_msg());
    }

    void Let::check_not_undefined() const
    {
        expect(!undefined(), undefined_msg());
    }

    String Let::undeclared_msg() const
    {
        return "Attempt to access undeclared let: "s + ckey();
    }

    String Let::undefined_msg() const
    {
        return "Attempt to access undefined let: "s + ckey();
    }

    void Let::check_is_not_self_ref(const Ptr& ptr_) const
    {
        expect(!is_self_ref(ptr_), is_self_ref_msg());
    }

    String Let::is_self_ref_msg() const
    {
        using unsot::to_string;
        return "Attempt to self reference a let: "s + ckey();
    }

    Ptr Let::expand() const
    {
        Ptr ptr_ = cptr_check();
        while (Let::is_me(ptr_)) ptr_ = Let::cast(ptr_).cptr_check();
        assert(!Let::is_me(ptr_));
        return ptr_;
    }

    Ptr Let::expand_all() const
    {
        Ptr ptr_ = expand();
        if (List::is_me(ptr_)) for_each_if(List::cast(ptr_), Let::is_me, [](auto& p){
            p = Let::cast(p).expand_all();
        });
        return ptr_;
    }

    template <bool groupedV>
    void Let::push(Ptr rhs)
    {
        check_is_not_self_ref(rhs);
        stack().push({move(rhs), groupedV});
        //! skipping is not enough, see `cptr`
        //! meta_stack().push({groupedV});
        //! if (!is_self_ref(rhs)) return stack().push({move(rhs)});
        //!
        //! expect(!undeclared(), is_self_ref_msg());
        //! ++stack_entry().skipped_cnt;
    }

    template void Let::push<false>(Ptr);
    template void Let::push<true>(Ptr);

    void Let::pop(bool grouped)
    {
        using unsot::to_string;
        static constexpr auto grouped_str_f = [](bool grp){
            return grp ? "grouped" : "individual";
        };
        expect(bool push_grouped = cstack_entry().grouped; grouped == push_grouped,
        //! expect(bool push_grouped = cmeta_stack_entry().grouped; grouped == push_grouped,
               "Inconsistent let '"s + to_string(ckey())
               + "' pop: pushed as " + grouped_str_f(push_grouped)
               + ", popping as " + grouped_str_f(grouped));
        stack().pop();
        //! meta_stack().pop();
        //! if (auto& skipped_cnt = stack_entry().skipped_cnt; skipped_cnt > 0) --skipped_cnt;
        //! else stack().pop();
    }

    void Let::set(Ptr rhs)
    {
        assert(rhs);
        check_is_not_self_ref(rhs);
        ptr().swap(rhs);
    }

    String Let::to_string() const&
    {
        if (undeclared()) return key_to_string() + ":undeclared";
        else if (undefined()) return key_to_string() + ":undefined";
        return cptr()->to_string();
    }

    String Let::to_string()&&
    {
        return to_string();
    }

    String Let::key_to_string() const
    {
        return key_to_let_ref(ckey());
    }

    bool Let::lequals(const This& rhs) const noexcept
    {
        if (ckey() != rhs.ckey()) return false;
        if (undeclared()) return rhs.undeclared();
        if (undefined()) return rhs.undefined();
        return cptr()->equals(*rhs.cptr());
    }

    bool Let::equals_base_anyway(const Base& rhs) const noexcept
    {
        /// Prefer comparison with the underlying pointer with other types
        if (undeclared() || undefined()) return false;
        return cptr()->equals(rhs);
    }
}
