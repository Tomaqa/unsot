#include "expr/pos.hpp"

#include "expr/pos.tpp"

namespace unsot::expr::aux::pos {
    template class Const_crtp<Const_pos, const List>;
    template class Const_crtp<Pos, List>;

    template class Crtp<Pos, List>;

    template struct Const_foo<Const_pos, Elem>;
    template struct Const_foo<Pos, Elem>;

    template struct Foo<Elem>;
}

////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////

namespace unsot::expr {
    Key extract_key(Pos& pos)
    {
        return cast_key(pos.extract());
    }

    List extract_list(Pos& pos)
    {
        return List::cast(pos.extract());
    }

    Key extract_copy_key(Pos& pos)
    {
        const Ptr ptr = pos.extract();
        return cast_key(ptr);
    }

    List extract_shallow_copy_list(Pos& pos)
    {
        const Ptr ptr = pos.extract();
        return List::cast(ptr).shallow_copy();
    }

    List extract_deep_copy_list(Pos& pos)
    {
        const Ptr ptr = pos.extract();
        return List::cast(ptr).deep_copy();
    }

    Key extract_maybe_copy_key(Pos& pos)
    {
        Ptr ptr = pos.extract();
        if (ptr.unique()) return cast_key(move(ptr));
        return cast_key(as_const(ptr));
    }

    List extract_maybe_shallow_copy_list(Pos& pos)
    {
        Ptr ptr = pos.extract();
        if (ptr.unique()) return List::cast(move(ptr));
        return List::cast(ptr).shallow_copy();
    }

    List extract_maybe_deep_copy_list(Pos& pos)
    {
        Ptr ptr = pos.extract();
        if (ptr.unique()) return List::cast(move(ptr));
        return List::cast(ptr).deep_copy();
    }

    Key extract_key_check(Pos& pos)
    {
        return cast_key_check(pos.extract_check());
    }

    List extract_list_check(Pos& pos)
    {
        return List::cast_check(pos.extract_check());
    }

    Key extract_copy_key_check(Pos& pos)
    {
        const Ptr ptr = pos.extract_check();
        return cast_key_check(ptr);
    }

    List extract_shallow_copy_list_check(Pos& pos)
    {
        const Ptr ptr = pos.extract_check();
        return List::cast_check(ptr).shallow_copy();
    }

    List extract_deep_copy_list_check(Pos& pos)
    {
        const Ptr ptr = pos.extract_check();
        return List::cast_check(ptr).deep_copy();
    }

    Key extract_maybe_copy_key_check(Pos& pos)
    {
        Ptr ptr = pos.extract_check();
        if (ptr.unique()) return cast_key_check(move(ptr));
        return cast_key_check(as_const(ptr));
    }

    List extract_maybe_shallow_copy_list_check(Pos& pos)
    {
        Ptr ptr = pos.extract_check();
        if (ptr.unique()) return List::cast_check(move(ptr));
        return List::cast_check(ptr).shallow_copy();
    }

    List extract_maybe_deep_copy_list_check(Pos& pos)
    {
        Ptr ptr = pos.extract_check();
        if (ptr.unique()) return List::cast_check(move(ptr));
        return List::cast_check(ptr).deep_copy();
    }

    void maybe_shallow_copy_at(Pos& pos)
    {
        maybe_shallow_copy(pos.peek());
    }

    void maybe_deep_copy_at(Pos& pos)
    {
        maybe_deep_copy(pos.peek());
    }
}
