#include "expr/fun.hpp"

namespace unsot::expr::fun {
    namespace aux {
        Args_base::Args_base(Keys_ptr kptr, Ids_map_ptr imap_ptr)
            : _keys_ptr(move(kptr)),
              _ids_map_ptr(imap_ptr ? move(imap_ptr)
                                    : new_ids_map(ckeys())),
              _key_ids_ptr(new_key_ids())
        { }

        Args_base::Key_ids_ptr Args_base::new_key_ids(Key_ids&& kids)
        {
            return MAKE_SHARED(move(kids));
        }

        void Args_base::linit_with_args(Keys_ptr kptr, Ids_map_ptr imap_ptr)
        {
            keys_ptr() = move(kptr);
            ids_map_ptr() = move(imap_ptr);
            key_ids_ptr() = new_key_ids();
        }
    }

    ////////////////////////////////////////////////////////////////

    void Fun_base::virtual_init_with_args(Keys_ptr kptr, Ids_map_ptr imap_ptr)
    {
        linit_with_args(move(kptr), move(imap_ptr));
        virtual_init();
    }

    bool Fun_base::lvalid_post_init() const noexcept
    {
        return valid_ids_map(cids_map(), ckeys());
    }

    void Fun_base::lcheck_post_init() const
    {
        check_ids_map(cids_map(), ckeys());
    }

    void Fun_base::init_arg_list(Ptr& ptr)
    {
        Inherit::init_arg_list(ptr);
        assert(is_me(ptr));

        auto kids_ptr = cast(as_const(ptr)).ckey_ids_ptr();
        part_key_ids_ptrs().push_back(kids_ptr);
        auto& kids = *kids_ptr;
        key_ids().insert(kids);
    }

    Ptr Fun_base::init_arg_list_new(Ptr&& ptr)
    {
        if (is_me(ptr)) return Inherit::init_arg_list_new(move(ptr));
        maybe_deep_copy(ptr);
        if (Formula::is_me(ptr))
            return new_me(Formula::cast(move(ptr)), ckeys_ptr(), cids_map_ptr());
        return new_me(List::cast(move(ptr)), ckeys_ptr(), cids_map_ptr());
    }

    void Fun_base::init_arg_key(Ptr& ptr)
    {
        Inherit::init_arg_key(ptr);

        auto [added, id] = init_arg_key_add(ptr);

        auto kids_ptr = new_key_ids();
        part_key_ids_ptrs().push_back(kids_ptr);
        kids_ptr->insert(id);
        key_ids().insert(id);
    }

    Pair<bool, Key_id> Fun_base::init_arg_key_add(Ptr& ptr)
    {
        const auto& akey = cast_key(ptr);
        auto& imap = cids_map();
        auto it = imap.find(akey);
        const bool found = (it != std::end(imap));
        Key_id kid = found ? it->second : add_key(akey);

        return {!found, move(kid)};
    }

    void Fun_base::init_arg_value(Ptr& ptr)
    {
        Inherit::init_arg_value(ptr);

        part_key_ids_ptrs().push_back(new_key_ids());
    }

    /// `if constexpr` works only in templated functions
    void Fun_base::reserve(size_t size_)
    {
        Inherit::reserve(size_);
        reserve_impl(*this, size_);
    }

    void Fun_base::resize(size_t size_)
    {
        Inherit::resize(size_);
        resize_impl(*this, size_);
        assert(ckeys().size() == size_);
    }

    void Fun_base::reserve_keys(size_t size_)
    {
        reserve_keys_impl(*this, size_);
    }

    void Fun_base::resize_keys(size_t size_)
    {
        resize_keys_impl(*this, size_);
    }

    template <typename F>
    void Fun_base::reserve_impl(F& fun, size_t size_)
    {
        fun.reserve_keys(size_);
        if constexpr (enabled_reserve_v<Key_ids>)
            fun.key_ids().reserve(size_);
        fun.part_key_ids_ptrs().reserve(size_);
    }

    template <typename F>
    void Fun_base::resize_impl(F& fun, size_t size_)
    {
        fun.resize_keys(size_);
        if constexpr (enabled_resize_v<Key_ids>) fun.key_ids().resize(size_);
        fun.part_key_ids_ptrs().resize(size_);
    }

    template <typename F>
    void Fun_base::reserve_keys_impl(F& fun, size_t size_)
    {
        fun.keys().reserve(size_);
        if constexpr (enabled_reserve_v<Ids_map>)
            fun.ids_map().reserve(size_);
    }

    template <typename F>
    void Fun_base::resize_keys_impl(F& fun, size_t size_)
    {
        fun.keys().resize(size_);
        if constexpr (enabled_resize_v<Ids_map>)
            fun.ids_map().resize(size_);
    }

    Key_id Fun_base::add_key(Key key_)
    {
        ++increased_size();
        return add_key_impl(move(key_));
    }

    Key_id Fun_base::add_key_impl(Key key_)
    {
        return expr::add_key(keys(), ids_map(), move(key_));
    }

    void Fun_base::revert_keys()
    {
        const int inc_size = increased_size();
        if (!ckeys_ptr() || inc_size == 0) return;
        const auto& keys_ = ckeys();
        const int ksize = std::size(keys_);
        assert(ksize >= inc_size);
        auto& imap = ids_map();
        assert(int(std::size(imap)) == ksize);
        for (int i = ksize-inc_size; i < ksize; ++i) {
            const Key& key_ = keys_[i];
            const auto it = imap.find(key_);
            assert(it != imap.end());
            if constexpr (!enabled_resize_v<Key_ids>) {
                [[maybe_unused]] const int cnt = key_ids().erase(it->second);
                assert(cnt == 1);
            }
            imap.erase(it);
        }

        resize_keys(ksize - inc_size);
        increased_size() = 0;
    }

    String Fun_base::to_string() const&
    {
        return to_string_impl(*this);
    }

    String Fun_base::to_string()&&
    {
        return to_string_impl(move(*this));
    }

    template <typename F>
    String Fun_base::to_string_impl(F&& fun)
    {
        using unsot::to_string;
        if (!fun.ckeys_ptr()) return {};

        String str;
        if (!std::empty(fun.ckeys())) {
            str += "[ "s + to_string(FORWARD(fun.keys())) + "]";
            if (std::size(fun.ckey_ids()) != std::size(fun.ckeys()))
                str += "< "s + to_string(FORWARD(fun.key_ids())) + ">";
        }

        return FORWARD(fun).Inherit::to_string() + move(str);
    }
}

////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////

namespace unsot::expr::fun {
    Idx find_key_id(const Base& fun, const Key_id& kid)
    {
        const int size_ = fun.size();
        for (Idx idx = 0; idx < size_; ++idx) {
            if (fun.cpart_key_ids(idx).contains(kid)) return idx;
        }
        return invalid_idx;
    }
}
