#pragma once

#include "expr/pos.hpp"

#include "util/string/alg.hpp"

namespace unsot::expr::aux::pos {
    template <typename T, typename L>
    void Const_crtp<T, L>::check_at_begin() const
    {
        expect(at_begin(), "Expected begin of expr list, got: "s + to_string());
    }

    template <typename T, typename L>
    void Const_crtp<T, L>::check_not_at_begin() const
    {
        expect(!at_begin(), "Unexpected begin of expr list: "s + to_string());
    }

    template <typename T, typename L>
    void Const_crtp<T, L>::check_at_end() const
    {
        expect(at_end(), "Expected end of expr list, got: "s + to_string());
    }

    template <typename T, typename L>
    void Const_crtp<T, L>::check_not_at_end() const
    {
        expect(!at_end(), "Unexpected end of expr list: "s + to_string());
    }

    template <typename T, typename L>
    void Const_crtp<T, L>::check_at_front() const
    {
        expect(at_front(), "Expected (valid) front of expr list, got: "s + to_string());
    }

    template <typename T, typename L>
    void Const_crtp<T, L>::check_at_back() const
    {
        expect(at_back(), "Expected (valid) back of expr list, got: "s + to_string());
    }

    template <typename T, typename L>
    typename Const_crtp<T, L>::That&
    Const_crtp<T, L>::set_it(Iterator it_) noexcept
    {
        if (it_ == end()) return unset_it();
        _it.emplace(it_);
        return this->that();
    }

    template <typename T, typename L>
    typename Const_crtp<T, L>::That& Const_crtp<T, L>::set_it() noexcept
    {
        return set_it(begin());
    }

    template <typename T, typename L>
    typename Const_crtp<T, L>::That& Const_crtp<T, L>::unset_it() noexcept
    {
        _it.reset();
        return this->that();
    }

    template <typename T, typename L>
    typename Const_crtp<T, L>::That& Const_crtp<T, L>::set_back_it() noexcept
    {
        return set_it(--end());
    }

    template <typename T, typename L>
    typename Const_crtp<T, L>::That& Const_crtp<T, L>::next() noexcept
    {
        assert(!at_end());
        if (++rit() == end()) return unset_it();
        return this->that();
    }

    template <typename T, typename L>
    typename Const_crtp<T, L>::That& Const_crtp<T, L>::prev() noexcept
    {
        assert(!at_begin());
        return set_it(--it());
    }

    template <typename T, typename L>
    const Ptr& Const_crtp<T, L>::cpeek() const noexcept
    {
        assert(!at_end());
        return *crit();
    }

    template <typename T, typename L>
    const Ptr& Const_crtp<T, L>::cget() noexcept
    {
        auto& ptr = cpeek();
        next();
        return ptr;
    }

    template <typename T, typename L>
    const Ptr& Const_crtp<T, L>::cpeek_check() const
    {
        check_not_at_end();
        return cpeek();
    }

    template <typename T, typename L>
    const Ptr& Const_crtp<T, L>::cget_check()
    {
        check_not_at_end();
        return cget();
    }

    template <typename T, typename L>
    String Const_crtp<T, L>::to_string() const&
    {
        return make_to_string(*this);
    }

    ////////////////////////////////////////////////////////////////

    template <typename T, typename L>
    Ptr& Crtp<T, L>::peek()& noexcept
    {
        assert(!this->at_end());
        return *this->rit();
    }

    template <typename T, typename L>
    Ptr& Crtp<T, L>::get()& noexcept
    {
        auto& ptr = peek();
        this->next();
        return ptr;
    }

    template <typename T, typename L>
    Ptr Crtp<T, L>::extract()
    {
        Ptr ptr = move(peek());
        erase();
        return ptr;
    }

    template <typename T, typename L>
    Ptr& Crtp<T, L>::peek_check()&
    {
        this->check_not_at_end();
        return peek();
    }

    template <typename T, typename L>
    Ptr& Crtp<T, L>::get_check()&
    {
        this->check_not_at_end();
        return get();
    }

    template <typename T, typename L>
    Ptr Crtp<T, L>::extract_check()
    {
        this->check_not_at_end();
        return extract();
    }

    template <typename T, typename L>
    void Crtp<T, L>::clear()
    {
        list().clear();
        this->unset_it();
    }

    template <typename T, typename L>
    typename Crtp<T, L>::That& Crtp<T, L>::insert(Ptr ptr)
    {
        list().insert(it(), move(ptr));
        return this->that();
    }

    template <typename T, typename L>
    typename Crtp<T, L>::That&
    Crtp<T, L>::insert(const_iterator first, const_iterator last)
    {
        list().insert(it(), first, last);
        return this->that();
    }

    template <typename T, typename L>
    typename Crtp<T, L>::That& Crtp<T, L>::erase()
    {
        auto it_ = list().erase(this->crit());
        return this->set_it(it_);
    }

    template <typename T, typename L>
    typename Crtp<T, L>::That&
    Crtp<T, L>::erase_until(const_iterator last)
    {
        auto it_ = list().erase(this->crit(), last);
        return this->set_it(it_);
    }

    template <typename T, typename L>
    typename Crtp<T, L>::That& Crtp<T, L>::erase_until()
    {
        return erase_until(this->cend());
    }

    template <typename T, typename L>
    typename Crtp<T, L>::That& Crtp<T, L>::splice(List&& rhs)
    {
        list().splice(it(), move(rhs));
        return this->that();
    }

    template <typename T, typename L>
    typename Crtp<T, L>::That&
    Crtp<T, L>::splice(List&& rhs, const_iterator it_)
    {
        list().splice(it(), move(rhs), it_);
        return this->that();
    }

    template <typename T, typename L>
    typename Crtp<T, L>::That&
    Crtp<T, L>::splice(List&& rhs,
                       const_iterator first, const_iterator last)
    {
        list().splice(it(), move(rhs), first, last);
        return this->that();
    }

    template <typename T, typename L>
    String Crtp<T, L>::to_string()&&
    {
        return make_to_string(move(*this));
    }

    ////////////////////////////////////////////////////////////////

    template <typename P, typename E>
    bool Const_foo<P, E>::is_elem_base_at(const P& pos) noexcept
    {
        return Elem_base::is_me(pos.cpeek());
    }

    template <typename P, typename E>
    bool Const_foo<P, E>::is_key_at(const P& pos) noexcept
    {
        return expr::is_key(pos.cpeek());
    }

    template <typename P, typename E>
    bool Const_foo<P, E>::is_list_at(const P& pos) noexcept
    {
        return List::is_me(pos.cpeek());
    }

    template <typename P, typename E>
    bool Const_foo<P, E>::is_seq_at(const P& pos) noexcept
    {
        return is_seq(pos.cpeek());
    }

    template <typename P, typename E>
    void Const_foo<P, E>::check_is_elem_base_at(const P& pos)
    {
        Elem_base::check_is_me(pos.cpeek_check());
    }

    template <typename P, typename E>
    void Const_foo<P, E>::check_is_key_at(const P& pos)
    {
        check_is_key(pos.cpeek_check());
    }

    template <typename P, typename E>
    void Const_foo<P, E>::check_is_list_at(const P& pos)
    {
        List::check_is_me(pos.cpeek_check());
    }

    template <typename P, typename E>
    void Const_foo<P, E>::check_is_seq_at(const P& pos)
    {
        check_is_seq(pos.cpeek_check());
    }

    template <typename P, typename E>
    bool Const_foo<P, E>::is_elem_at(const P& pos) noexcept
    {
        return E::is_me(pos.cpeek());
    }

    template <typename P, typename E>
    void Const_foo<P, E>::check_is_elem_at(const P& pos)
    {
        E::check_is_me(pos.cpeek_check());
    }

    template <typename P, typename E>
    bool Const_foo<P, E>::is_int_at(const P& pos) noexcept
    {
        return is_int<E>(pos.cpeek());
    }

    template <typename P, typename E>
    bool Const_foo<P, E>::is_real_at(const P& pos) noexcept
    {
        return is_real<E>(pos.cpeek());
    }

    template <typename P, typename E>
    void Const_foo<P, E>::check_is_int_at(const P& pos)
    {
        check_is_int<E>(pos.cpeek_check());
    }

    template <typename P, typename E>
    void Const_foo<P, E>::check_is_real_at(const P& pos)
    {
        check_is_real<E>(pos.cpeek_check());
    }

    template <typename P, typename E>
    bool Const_foo<P, E>::is_flat_list_at(const P& pos) noexcept
    {
        return is_list_at(pos) && is_flat(peek_list(pos));
    }

    template <typename P, typename E>
    bool Const_foo<P, E>::is_deep_list_at(const P& pos) noexcept
    {
        return is_list_at(pos) && is_deep(peek_list(pos));
    }

    template <typename P, typename E>
    void Const_foo<P, E>::check_is_flat_list_at(const P& pos)
    {
        check_is_flat(peek_list_check(pos));
    }

    template <typename P, typename E>
    void Const_foo<P, E>::check_is_deep_list_at(const P& pos)
    {
        check_is_deep(peek_list_check(pos));
    }

    ////////////////////////////////////////////////////////////////

    template <typename E>
    E Foo<E>::extract_elem(Pos& pos)
    {
        return E::cast(pos.extract());
    }

    template <typename E>
    E Foo<E>::extract_copy_elem(Pos& pos)
    {
        const Ptr ptr = pos.extract();
        return E::cast(ptr);
    }

    template <typename E>
    E Foo<E>::extract_maybe_copy_elem(Pos& pos)
    {
        Ptr ptr = pos.extract();
        if (ptr.unique()) return E::cast(move(ptr));
        return E::cast(as_const(ptr));
    }

    template <typename E>
    E Foo<E>::extract_elem_check(Pos& pos)
    {
        return E::cast_check(pos.extract_check());
    }

    template <typename E>
    E Foo<E>::extract_copy_elem_check(Pos& pos)
    {
        const Ptr ptr = pos.extract_check();
        return E::cast_check(ptr);
    }

    template <typename E>
    E Foo<E>::extract_maybe_copy_elem_check(Pos& pos)
    {
        Ptr ptr = pos.extract_check();
        if (ptr.unique()) return E::cast_check(move(ptr));
        return E::cast_check(as_const(ptr));
    }

    template <typename E>
    Int Foo<E>::extract_int(Pos& pos)
    {
        return extract_value<Int, E>(pos);
    }

    template <typename E>
    Real Foo<E>::extract_real(Pos& pos)
    {
        return extract_value<Real, E>(pos);
    }

    template <typename E>
    Int Foo<E>::extract_copy_int(Pos& pos)
    {
        return extract_copy_value<Int, E>(pos);
    }

    template <typename E>
    Real Foo<E>::extract_copy_real(Pos& pos)
    {
        return extract_copy_value<Real, E>(pos);
    }

    template <typename E>
    Int Foo<E>::extract_maybe_copy_int(Pos& pos)
    {
        return extract_maybe_copy_value<Int, E>(pos);
    }

    template <typename E>
    Real Foo<E>::extract_maybe_copy_real(Pos& pos)
    {
        return extract_maybe_copy_value<Real, E>(pos);
    }

    template <typename E>
    Int Foo<E>::extract_int_check(Pos& pos)
    {
        return extract_value_check<Int, E>(pos);
    }

    template <typename E>
    Real Foo<E>::extract_real_check(Pos& pos)
    {
        return extract_value_check<Real, E>(pos);
    }

    template <typename E>
    Int Foo<E>::extract_copy_int_check(Pos& pos)
    {
        return extract_copy_value_check<Int, E>(pos);
    }

    template <typename E>
    Real Foo<E>::extract_copy_real_check(Pos& pos)
    {
        return extract_copy_value_check<Real, E>(pos);
    }

    template <typename E>
    Int Foo<E>::extract_maybe_copy_int_check(Pos& pos)
    {
        return extract_maybe_copy_value_check<Int, E>(pos);
    }

    template <typename E>
    Real Foo<E>::extract_maybe_copy_real_check(Pos& pos)
    {
        return extract_maybe_copy_value_check<Real, E>(pos);
    }
}

////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////

namespace unsot::expr::aux::pos {
    template <typename T, typename L>
    template <typename B>
    void Const_crtp<T, L>::To_string_mixin<B>::
        perform_head_post(String& str, const Conf& conf, int depth)
    {
        Inherit::perform_head_post(str, conf, depth);

        if (!conf.top_brackets) return;

        if (_pos.at_front()) str += ">";
        else {
            _mark_pos = !_pos.at_end();
            _pos_it = _pos.cit();
        }
    }

    template <typename T, typename L>
    template <typename B>
    void Const_crtp<T, L>::To_string_mixin<B>::
        perform_body_pre(String& str, const Conf& conf, int depth, Iterator it)
    {
        Inherit::perform_body_pre(str, conf, depth, it);

        if (_mark_pos && it == _pos_it) {
            str += repeat(conf.delim, 2) + ">";
            _mark_pos = false;
        }
    }

    template <typename T, typename L>
    template <typename B>
    void Const_crtp<T, L>::To_string_mixin<B>::
        perform_tail_pre(String& str, const Conf& conf, int depth)
    {
        Inherit::perform_tail_pre(str, conf, depth);

        if (!conf.top_brackets) return;

        if (_pos.at_end()) str += ">";
    }
}
