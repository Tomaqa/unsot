#pragma once

#include "expr/preprocess.hpp"

#include "util/fun.hpp"
#include "util/alg.hpp"

namespace unsot::expr {
    template <bool expandV, bool negV>
    Ptr Preprocess::Foo<expandV, negV>::extract_macro_key(Preprocess& prep, Pos& pos, int depth)
    {
        if constexpr (expandV) {
            check_is_key_at(pos);
            prep.expand_key<Expand_lets::no>(pos, depth);
        }

        Ptr ptr = pos.extract_check();
        auto& key_ = cast_key_check(ptr);
        check_is_not_macro(key_);
        return ptr;
    }

    template <bool expandV, bool negV>
    bool Preprocess::Foo<expandV, negV>::extract_macro_isdef(Preprocess& prep, Pos& pos, int depth)
    {
        auto contains_f = [&prep](const auto& ptr) -> bool {
            if (Let::is_me(ptr)) return true;
            assert(Elem_base::is_me(ptr));
            if (!is_key(ptr)) return false;
            auto& key_ = cast_key(ptr);
            return Macro::is_reserved_key(key_) || prep.contains(key_);
        };

        return prep.extract_pred<expandV, Expand_lets::no>(pos, depth,
            [&contains_f](This*, Pos& pos_, int /*dep*/) -> bool {
                const auto ptr = pos_.extract_check();
                if (!List::is_me(ptr)) return negV ^ contains_f(ptr);

                auto& ls = List::cast(ptr);
                return all_of(ls, [&contains_f](auto& p) -> bool {
                    if (List::is_me(p)) return negV;
                    return negV ^ contains_f(p);
                });
        });
    }

    template <bool expandV, bool negV>
    bool Preprocess::Foo<expandV, negV>::extract_macro_null(Preprocess& prep, Pos& pos, int depth)
    {
        return prep.extract_nary_pred<1, expandV, Expand_lets::no>(pos, depth,
            [](This*, Pos& pos_, int /*dep*/){
                auto& ptr = pos_.cpeek();
                if (List::is_me(ptr)) return List::cast(ptr).empty();
                if (is_key(ptr)) return empty(cast_key(ptr));
                if (!Let::is_me(ptr)) return false;
                auto& let_ = Let::cast(ptr);
                return let_.undeclared() || let_.undefined();
        });
    }

    template <bool expandV, bool negV>
    bool Preprocess::Foo<expandV, negV>::extract_macro_listp(Preprocess& prep, Pos& pos, int depth)
    {
        return prep.extract_nary_pred<1, expandV, Expand_lets::no>(pos, depth,
            [](This*, Pos& pos_, int /*dep*/){
                return is_list_at(pos_);
        });
    }

    template <bool expandV, bool negV>
    bool Preprocess::Foo<expandV, negV>::
        extract_macro_numberp(Preprocess& prep, Pos& pos, int depth)
    {
        return prep.extract_nary_pred<1, expandV, Expand_lets::no>(pos, depth,
            [](This*, Pos& pos_, int /*dep*/){
                auto& ptr = pos_.cpeek();
                return Elem_base::is_me(ptr) && !is_key(ptr);
        });
    }

    template <bool expandV, bool negV>
    bool Preprocess::Foo<expandV, negV>::extract_macro_refp(Preprocess& prep, Pos& pos, int depth)
    {
        return prep.extract_nary_pred<1, expandV, Expand_lets::no>(pos, depth,
            [](This*, Pos& pos_, int /*dep*/){
                return is_let_at(pos_);
        });
    }

    template <bool expandV, bool negV>
    bool Preprocess::Foo<expandV, negV>::extract_macro_equal(Preprocess& prep, Pos& pos, int depth)
    {
        return prep.extract_nary_pred<2, expandV, Expand_lets::no>(pos, depth,
            [](This*, Pos& pos_, int /*dep*/){
                const auto& ptr1 = pos_.get();
                const auto& ptr2 = pos_.cpeek();
                return ptr1->equals(*ptr2);
        });
    }

    template <bool expandV, bool negV>
    bool Preprocess::Foo<expandV, negV>::extract_macro_eq(Preprocess& prep, Pos& pos, int depth)
    {
        return prep.extract_nary_pred<2, expandV, Expand_lets::no>(pos, depth,
            [](This*, Pos& pos_, int /*dep*/){
                const auto& ptr1 = pos_.get();
                const auto& ptr2 = pos_.cpeek();
                return ptr1.get() == ptr2.get();
        });
    }

    template <bool expandV, bool negV>
    Elem Preprocess::Foo<expandV, negV>::extract_macro_len(Preprocess& prep, Pos& pos, int depth)
    {
        return prep.extract_nary_eval<1, expandV, Expand_lets::yes>(pos, depth,
            [](This*, Pos& pos_, int /*dep*/) -> Int {
                auto& ptr = pos_.cpeek();
                return List::is_me(ptr) ? size(List::cast(ptr))
                                        : size(cast_key_check(ptr));
        });
    }

    template <bool expandV, bool negV>
    Elem Preprocess::Foo<expandV, negV>::extract_eval_arith(Preprocess& prep, Pos& pos, int depth)
    {
        return prep.extract_eval<expandV>(pos, depth, [](This* this_l, Pos& pos_, int dep){
            if (is_list_at(pos_)) return this_l->extract_eval_arith_list<no_exp>(pos_, dep);
            return this_l->extract_eval_arith_elem(pos_, dep);
        });
    }

    template <bool expandV, bool negV>
    Elem Preprocess::Foo<expandV, negV>::
        extract_eval_arith_elem(Preprocess& prep, Pos& pos, int depth)
    {
        assert(is_elem_at(pos));
        if (!is_key_at(pos)) return extract_maybe_copy_elem(pos);

        const auto& key_ = peek_key(pos);
        if (is_arith_exp(key_)) return prep.extract_eval_arith_exp(pos, depth);

        if constexpr (!expandV) THROW("Attempt to evaluate key: "s + to_string(key_));
        prep.expand_key(pos, depth);
        return prep.extract_eval_arith<no_exp>(pos, depth);
    }

    template <bool expandV, bool negV>
    Elem Preprocess::Foo<expandV, negV>::
        extract_eval_arith_list(Preprocess& prep, Pos& pos, int depth)
    {
        assert(is_list_at(pos));
        //+ parametrize for integrals too
        auto ret = invoke(prep.extract_eval_arith_cons_fun<fun::Tp<Real>, expandV>(pos, depth));
        if (valid<Bool>(ret)) return get<Bool>(ret);
        return get<Real>(ret);
    }

    ////////////////////////////////////////////////////////////////

    template <int expLets>
    void Preprocess::Foo_exp_lets<expLets>::
        maybe_check_is_not_let([[maybe_unused]] const Preprocess& prep, const Ptr& ptr)
    {
        if constexpr (expand_lets != Expand_lets::yes && expand_lets != Expand_lets::all
                      && expand_lets != Expand_lets::ban) {
            if constexpr (expand_lets != Expand_lets::neutral) return;
            auto& exp = prep.cexpanding_lets();
            if (exp != Expand_lets::yes && exp != Expand_lets::all && exp != Expand_lets::ban)
                return;
        }
        Let::check_is_not_me(ptr);
    };

    template <int expLets>
    void Preprocess::Foo_exp_lets<expLets>::
        maybe_assert_no_lets([[maybe_unused]] const Preprocess& prep,
                             [[maybe_unused]] const Ptr& ptr) noexcept
    {
        if constexpr (!_debug_) return;
        if constexpr (expand_lets != Expand_lets::yes && expand_lets != Expand_lets::all
                      && expand_lets != Expand_lets::ban && expand_lets != Expand_lets::neutral
        ) return;
        auto& exp = prep.cexpanding_lets();
        if (util::contains({Expand_lets::yes, Expand_lets::all, Expand_lets::ban}, expand_lets)
            || util::contains({Expand_lets::yes, Expand_lets::all, Expand_lets::ban}, exp)
        ) assert(!Let::is_me(ptr));
        if (util::contains({Expand_lets::all, Expand_lets::ban}, expand_lets)
            || util::contains({Expand_lets::all, Expand_lets::ban}, exp)
        ) assert(!List::is_me(ptr) || none_of(List::cast(ptr), Let::is_me));
    };

    template <int expLets>
    Ptr Preprocess::Foo_exp_lets<expLets>::expand_let(const Preprocess& prep, const Let& let_)
    {
        static const auto check_f = [](const Ptr& ptr){
            Let::check_is_not_me(ptr);
            //+ does not seem to be necessary (acc. to tests)
            //+ if (List::is_me(ptr)) for_each(List::cast(ptr), Let::check_is_not_me);
        };

        if constexpr (expand_lets == Expand_lets::no) return let_.cptr_check();
        if constexpr (expand_lets == Expand_lets::yes) return let_.expand();
        if constexpr (expand_lets == Expand_lets::all) return let_.expand_all();
        if constexpr (expand_lets == Expand_lets::copy) return let_.deep_copy_to_ptr();
        if constexpr (expand_lets == Expand_lets::ban) {
            auto ptr = prep.expand_let<Expand_lets::neutral>(let_);
            check_f(ptr);
            return ptr;
        }

        auto& exp = prep.cexpanding_lets();
        if (exp == Expand_lets::no) return let_.cptr_check();
        if (exp == Expand_lets::yes) return let_.expand();
        if (exp == Expand_lets::all) return let_.expand_all();
        if (exp == Expand_lets::copy) return let_.deep_copy_to_ptr();
        assert(exp == Expand_lets::ban);
        auto ptr = prep.expand_let<def_expanding_lets>(let_);
        check_f(ptr);
        return ptr;
    }

    template <int expLets>
    void Preprocess::Foo_exp_lets<expLets>::
        parse_key_single_not_arith(Preprocess& prep, Pos& pos, Key& key_, int depth)
    {
        if (!is_macro(key_)) {
            if (is_escaped(key_)) {
                assert(size(key_) > 1);
                unescape(key_);
            }
            pos.next();
            return;
        }

        assert(!is_cat(key_) || is_infix_cat(key_));
        if (is_let_ref(key_)) {
            if constexpr (expand_lets == Expand_lets::ban)
                THROW("Unexpected let reference: "s + pos.to_string());
            /// If expanded before analyzing it afterwards,
            /// the information whether it was a let reference may be lost
            else if constexpr (expand_lets == Expand_lets::no)
                pos.peek() = prep.peek_let_ref(pos, depth);
            else if constexpr (expand_lets == Expand_lets::yes || expand_lets == Expand_lets::all
                               || expand_lets == Expand_lets::copy
            ) prep.expand_let_ref<expand_lets>(pos, depth);
            else {
                static_assert(expand_lets == Expand_lets::neutral);
                auto& exp = prep.cexpanding_lets();
                expect(exp != Expand_lets::ban, "Unexpected let reference: "s + pos.to_string());
                if (exp == Expand_lets::no) pos.peek() = prep.peek_let_ref(pos, depth);
                else if (exp == Expand_lets::yes) prep.expand_let_ref<Expand_lets::yes>(pos, depth);
                else if (exp == Expand_lets::all) prep.expand_let_ref<Expand_lets::all>(pos, depth);
                else {
                    assert(exp == Expand_lets::copy);
                    prep.expand_let_ref<Expand_lets::copy>(pos, depth);
                }
            }

            pos.next();
            return;
        }

        Key mkey = macro_to_key(move(key_));
        pos.erase();
        parse_macro(prep, pos, move(mkey), depth);
    }

    template <int expLets>
    void Preprocess::Foo_exp_lets<expLets>::
        parse_macro(Preprocess& prep, Pos& pos, const Key& key_, int depth)
    try {
        if (empty(key_) || key_ == Macro::char_str)
            return parse_macro_exp(prep, pos, depth);
        /// empty key '#\'
        if (is_escaped(key_)) {
            assert(key_ == escape_char_str);
            pos.emplace(new_key());
            return;
        }
        if (is_macro(key_)) {
            expect(size(key_) == 2 && key_.back() == escape_char,
                   "Attempt to expand macro name with '#': "s + to_string(key_));
            assert(key_ == Macro::char_str + escape_char);
            return prep.cat(pos, depth);
        }
        if (key_ == ref_exp_char_str)
            return prep.parse_macro_exp<Expand_lets::yes>(pos, depth);

        auto it = reserved_macro_fs_map.find(key_);
        if (it == end(reserved_macro_fs_map))
            return parse_user_macro(prep, pos, key_, depth);
        else invoke(it->second, &prep, pos, depth);
    }
    catch (const Error& err) {
        THROW("At '") + key_ + "':\n" + err;
    }

    /// Not reference expansion, this is handled in `peek_let_ref`
    template <int expLets>
    void Preprocess::Foo_exp_lets<expLets>::
        parse_macro_exp(Preprocess& prep, Pos& pos, int depth)
    try {
        List ls = prep.extract_parse_list_check<do_inc, Expand_lets::no>(pos, depth);
        expect(ls.size() == 1,
               "Expected single argument in the expansion list, got: "s + ls.to_string());
        const bool was_at_begin = pos.at_begin();
        auto it = pos.it();
        const auto prev_it = was_at_begin ? it : std::prev(it, 1);
        pos.splice(move(ls));
        it = pos.it();
        if (was_at_begin) pos.set_it();
        else pos.set_it(prev_it).next();

        if (is_let_at(pos)) {
            auto& ptr = pos.get();
            ptr = prep.expand_let<expand_lets>(Let::cast(ptr));
            return;
        }

        Key mkey = extract_maybe_copy_key_check(pos);
        parse_macro(prep, pos, move(mkey), depth+1);

        /// do not care where the parse ended
    }
    catch (const Error& err) {
        THROW("In macro expansion:\n") + err;
    }

    template <int expLets>
    void Preprocess::Foo_exp_lets<expLets>::
        parse_user_macro(Preprocess& prep, Pos& pos, const Key& key_, int depth)
    {
        auto lptr_it_opt = prep.cfind_let_ptr(key_);
        const bool has_let = lptr_it_opt.valid();
        Optional<Macros_map::const_iterator> macro_it_opt;
        if (!has_let) macro_it_opt = prep.cfind_macro(key_);
        const bool has_macro = !has_let && macro_it_opt.valid();
        expect(has_let || has_macro, "User macro was not defined.");

        if (has_let) {
            auto ptr = prep.expand_let<expand_lets>(Let::cast(lptr_it_opt.value()->second));
            pos.emplace(move(ptr));
            return;
        }

        Macro::Body body = macro_it_opt.value()->second.body;
        prep.parse_user_macro_push_args(pos, key_, depth);
        prep.parse_nested<no_inc, expand_lets>(body, depth);
        pos.splice(move(body));
        prep.parse_user_macro_pop_args(key_);
    }
}
