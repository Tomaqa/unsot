#pragma once

#include "expr.hpp"

#include "util/string/alg.hpp"

#include <sstream>

namespace unsot::expr {
    template <typename E>
    List::List(String input, Tag<E> t)
        : List(istringstream(move(input)), t)
    { }

    template <typename E>
    List::List(istream& is, Tag<E>)
    {
        streampos last_pos = is.tellg();
        try { parse<E>(is, last_pos, 0); }
        catch (const Error& err) {
            is.clear();
            auto size_ = is.tellg() - last_pos;
            if (size_ <= 0) throw;
            is.seekg(last_pos);
            String str;
            str.reserve(size_);
            for (decltype(size_) i = 0; i < size_; ++i) {
                const char c = is.get();
                str += c;
            }
            THROW("At '") + move(str) + "':\n" + err;
        }
    }

    template <typename E>
    List& List::parse(istream& is, streampos& last_pos, int depth)
    {
        using unsot::to_string;

        ostringstream oss_buf;

        bool closed = false;
        is >> std::noskipws;
        for (char c; is >> c;) {
            if (isspace(c)) continue;
            if (!isprint(c)) {
                expect(int i = c; i == 0,
                       "Unexpected non-printable character ("s
                       + to_string(i) + ")");
            }
            if (c == '(') {
                last_pos = is.tellg();
                emplace_back(List().parse<E>(is, last_pos, depth+1).to_ptr());
                continue;
            }
            if (c == ')') {
                expect(depth > 0,
                       "Unexpected closing bracket in top level expression.");
                closed = true;
                break;
            }
            oss_buf << c;
            if (c = is.peek(); isspace(c) || c == '(') {
                emplace_back(new_elem<E>(oss_buf.str()));
                oss_buf.str("");
            }
        }

        expect(closed || depth == 0,
               "Closing bracket at level "s
               + to_string(depth) + " not found.");

        if (!std::empty(oss_buf.str())) {
            emplace_back(new_elem<E>(oss_buf.str()));
        }

        return *this;
    }

    ////////////////////////////////////////////////////////////////

    namespace aux {
        template <typename E>
        bool List_foo<E>::is_as_elem(const List& ls) noexcept
        {
            return ls.size() == 1 && E::is_me(ls.cfront());
        }

        template <typename E>
        void List_foo<E>::check_is_as_elem(const List& ls)
        try {
            expect(is_as_elem(ls),
                   "expr list is not convertible to single element: "s + ls.to_string());
        }
        catch (Error& err) {
            try {
                if (!ls.empty()) E::check_is_me(ls.cfront());
            }
            catch (const Error& err2) {
                err += "\n" + err2;
            }
            THROW(err);
        }

                template <typename E>
        bool List_foo<E>::has_elems_only(const List& ls) noexcept
        {
            return all_of(ls, E::is_me);
        }

        template <typename E>
        void List_foo<E>::check_has_elems_only(const List& ls)
        try {
            for_each(ls, E::check_is_me);
        }
        catch (const Error& err) {
            try { check_is_flat(ls); }
            catch (const Error& err2) {
                THROW(err2) + "\n" + err;
            }
            THROW(err);
        }
    }
}


////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////

namespace unsot::expr {
    template <typename T, typename L>
    String List::To_string_crtp<T, L>::perform_impl(const Conf& conf, int depth)
    {
        const bool brackets = depth > 0 || conf.top_brackets;

        String str;

        perform_head_pre(str, conf, depth);
        if (brackets) str += "(";
        perform_head_post(str, conf, depth);

        const Iterator eit = list().end();
        for (Iterator it = list().begin(); it != eit; ++it) {
            perform_body_pre(str, conf, depth, it);
            //! it is probably source of a crash when run with '-EE'
            auto&& ptr = Forward_as_ref<Ptr, Add_lref<List>>(*it);
            if (!conf.rec || !List::is_me(ptr))
                str += conf.delim + ptr_to_string(FORWARD(ptr), depth);
            else {
                //! top_brackets is propagated
                auto to_str = List::make_to_string(List::cast(FORWARD(ptr)));
                str += move(to_str).perform_impl(conf, depth+1);
            }
            perform_body_post(str, conf, depth);
        }

        if (brackets) str += conf.delim;
        perform_tail_pre(str, conf, depth);
        if (brackets) str += ")";
        perform_tail_post(str, conf, depth);

        return str;
    }

    template <typename T, typename L>
    void List::To_string_crtp<T, L>::perform_head_pre(String& str, const Conf& conf, int depth)
    {
        if (depth == 0) return;
        if (depth > 1 && is_flat(clist())) str += conf.delim;
        else str += conf.ls_delim + repeat(conf.delim, depth*4);
    }

    template <typename T, typename L>
    String List::To_string_crtp<T, L>::ptr_to_string(const Ptr& ptr, int /*depth*/)
    {
        return ptr->to_string();
    }

    template <typename T, typename L>
    String List::To_string_crtp<T, L>::ptr_to_string(Ptr&& ptr, int /*depth*/)
    {
        return move(ptr)->to_string();
    }
}
