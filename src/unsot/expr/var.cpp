#include "expr/var.hpp"

namespace unsot::expr::var {
    Base::Base(Keys_ptr kptr, Ids_map_ptr imap_ptr, Id id_, bool is_set_)
        : _keys_ptr(move(kptr)), _ids_map_ptr(move(imap_ptr)),
          _id(move(id_)), _is_set(is_set_)
    { }

    bool Base::lvalid_pre_init() const noexcept
    {
        return valid_key();
    }

    void Base::lcheck_pre_init() const
    {
        check_key();
    }

    void Base::lrecover_pre_init()
    {
        if (!ckeys_ptr() || empty(ckeys())) return;
        auto& last_key = ckeys().back();
        /// leave it inconsistent if the key is not at the end
        if (ckey() != last_key) return;
        keys().pop_back();
        [[maybe_unused]] const int cnt = ids_map().erase(last_key);
        assert(cnt == 1);
    }

    bool Base::valid_key() const noexcept
    {
        return expr::valid_key(ckey());
    }

    void Base::check_key() const
    try {
        expr::check_key(ckey());
    }
    catch (const Error& err) {
        THROW("Invalid Var key:\n") + err;
    }

    void Base::check_is_set() const
    {
        expect(is_set(), "Attempt to read unset Var: "s + ckey());
    }

    void Base::check_is_unset() const
    {
        expect(is_unset(), "Var is not unset: "s + ckey());
    }

    void Base::set_computed()
    {
        check_is_set();
        rcomputed() = true;
    }

    Base& Base::set() noexcept
    {
        if (unlocked()) set_impl();
        return *this;
    }

    Base& Base::unset() noexcept
    {
        if (unlocked()) unset_impl();
        return *this;
    }

    Base& Base::reset() noexcept
    {
        if (unlocked()) reset_impl();
        return *this;
    }

    void Base::set_impl() noexcept
    {
        set_only_impl();
        reset_impl();
    }

    void Base::unset_impl() noexcept
    {
        unset_only_impl();
        reset_impl();
    }

    void Base::set_only_impl() noexcept
    {
        EVAR_CVERB3LN("set");
        ris_set() = true;
    }

    void Base::unset_only_impl() noexcept
    {
        EVAR_CVERB3LN("unset");
        ris_set() = false;
    }

    void Base::reset_impl() noexcept
    {
        EVAR_CVERB3LN("reset");
        unset_computed();
        computable_flag().forget();
        consistent_flag().forget();
    }

    /// Impossible combinations with `computable':
    ///  C &  E
    /// 01 &  ?
    ///  1 &  0
    Flag Base::evaluable() const noexcept
    {
        if (auto flag = surely_evaluable(); flag.valid()) {
            EVAR_CVERB3LN("surely_evaluable -> " << flag);
            return flag;
        }
        return compute_computability();
    }

    void Base::check_evaluable() const
    {
        expect(evaluable(), "Attempt to evaluate unevaluable Var: "s + ckey());
    }

    Flag Base::computable() const noexcept
    {
        if (auto flag = surely_computable(); flag.valid()) {
            EVAR_CVERB3LN("surely_computable -> " << flag);
            return flag;
        }
        return compute_computability();
    }

    void Base::check_computable() const
    {
        expect(computable(), "Attempt to compute uncomputable Var: "s + ckey());
    }

    Flag Base::surely_evaluable() const noexcept
    {
        if (is_set()) return true;
        return surely_computable();
    }

    Flag Base::surely_computable() const noexcept
    {
        if (ccomputable_flag().valid()) return ccomputable_flag();
        if (locked()) return false;
        return unknown;
    }

    Flag Base::compute_computability() const noexcept
    {
        auto flag = computable_body();
        EVAR_CVERB3LN("computable_body -> " << flag);
        assert(flag == computable_body());
        computable_flag() = flag;
        return flag;
    }

    Flag Base::computable_body() const noexcept
    {
        return unknown;
    }

    Flag Base::consistent() noexcept(!_debug_)
    {
        if (auto flag = surely_consistent(); flag.valid()) {
            EVAR_CVERB3LN("surely_consistent -> " << flag);
            return flag;
        }
        if (is_unset() || !computable()) {
            EVAR_CVERB3LN("consistent -> unknown");
            return unknown;
        }
        return compute_consistency();
    }

    void Base::check_consistent()
    {
        expect(consistent(), "Var is in inconsistent state: "s + ckey());
    }

    Flag Base::surely_consistent() const noexcept
    {
        if (cconsistent_flag().valid()) return cconsistent_flag();
        if (computed()) return true;
        return unknown;
    }

    Flag Base::compute_consistency()
    {
        auto flag = consistent_body();
        EVAR_CVERB3LN("consistent_body -> " << flag);
        consistent_flag() = flag;
        return flag;
    }

    Flag Base::consistent_body()
    {
        return unknown;
    }

    Flag Base::independent() const noexcept
    {
        if (locked()) return true;
        if (!empty(cdepend_ids())) return false;
        if (computed()) return true;
        return unknown;
    }

    String Base::to_string() const&
    {
        if (!initialized()) return Dynamic::to_string();
        return head_to_string() + body_to_string();
    }

    String Base::head_to_string() const
    {
        using unsot::to_string;
        return to_string(ckey());
    }

    String Base::body_to_string() const
    {
        return value_to_string();
    }

    String Base::value_to_string() const
    {
        if (is_unset()) {
            if (auto flag = computable(); flag.unknown()) return flag.to_string();
            else return flag.cvalue() ? "*" : "";
        }

        String str = (computed() ? ":" : "=") + value_to_string_body();
        if (surely_consistent().is_false()) str += "!";
        return str;
    }

    String Base::value_to_string_body() const
    {
        return "1";
    }

    bool Base::lequals(const This& rhs) const noexcept
    {
        if (is_unset()) return rhs.is_unset();
        return rhs.is_set();
    }
}
