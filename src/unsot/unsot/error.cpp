#include "unsot.hpp"
#include "unsot/error.hpp"

namespace unsot {
    Error Error::operator +(const Error& rhs) const
    {
        return {cmsg() + rhs.cmsg()};
    }

    Error Error::operator +(const String& rhs) const
    {
        return {cmsg() + rhs};
    }

    Error& Error::operator +=(const Error& rhs)
    {
        msg() += rhs.cmsg();
        return *this;
    }

    Error& Error::operator +=(const String& rhs)
    {
        msg() += rhs;
        return *this;
    }
}

////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////

namespace unsot {
    Error operator +(const String& lhs, const Error& rhs)
    {
        return {lhs + rhs.cmsg()};
    }

    ostream& operator <<(ostream& os, const Error& rhs)
    {
        if (!rhs._printed) {
            if (!empty(rhs.cmsg())) os << rhs.to_string();
            else os << "<empty error message>";
            rhs._printed = true;
        }
        return os;
    }
}
