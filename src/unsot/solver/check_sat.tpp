#pragma once

#include "solver/check_sat.hpp"

#include <omp.h>

namespace unsot::solver {
    template <typename B, typename OdeSolver>
    void Mixin<B, OdeSolver>::Check_sat::pre_compute_of(Flow_inst&)
    {
        if (!this->cprofiling()) return;
        compute_flows_duration() -= omp_get_wtime();
    }

    template <typename B, typename OdeSolver>
    void Mixin<B, OdeSolver>::Check_sat::post_compute_of(Flow_inst&)
    {
        if (!this->cprofiling()) return;
        compute_flows_duration() += omp_get_wtime();
        ++compute_flows_count();
    }
}
