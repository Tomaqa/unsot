#include "solver.hpp"
#include "solver/flow.hpp"

#include "util/string/alg.hpp"

namespace unsot::solver::flow {
    bool is_param_key(const Key& key) noexcept
    {
        return starts_with(key, param_key_prefix)
            && size(key) != size(param_key_prefix);
    }

    bool is_init_key(const Key& key) noexcept
    {
        return is_param_key(key);
    }

    bool is_final_key(const Key& key) noexcept
    {
        return ends_with(key, final_key_suffix)
            && size(key) != size(final_key_suffix);
    }

    Key to_param_key(Key key) noexcept
    {
        return param_key_prefix + move(key);
    }

    Key to_init_key(Key ode_key) noexcept
    {
        return to_param_key(move(ode_key));
    }

    Key to_final_key(Key ode_key) noexcept
    {
        return move(ode_key) + final_key_suffix;
    }

    Key param_to_ode_key(Key key) noexcept
    {
        return key.erase(0, 1);
    }

    Key init_to_ode_key(Key key) noexcept
    {
        return param_to_ode_key(move(key));
    }

    Key final_to_ode_key(Key key) noexcept
    {
        key.pop_back();
        return key;
    }

    Key to_ode_variant_key(Key ode_key) noexcept
    {
        static long counter = 0;
        return to_derived_key(move(ode_key)) + "_" + to_string(++counter);
    }

    Key to_inv_variant_key(Key ode_key) noexcept
    {
        static long counter = 0;
        return move(ode_key) + "_inv_" + to_string(++counter);
    }

    Key ode_variant_to_ode_key(Key ode_variant_key) noexcept
    {
        while (isdigit(ode_variant_key.back())) ode_variant_key.pop_back();
        ode_variant_key.pop_back();
        return to_ode_key(move(ode_variant_key));
    }

    Key inv_variant_to_ode_key(Key inv_variant_key) noexcept
    {
        while (isdigit(inv_variant_key.back())) inv_variant_key.pop_back();
        return inv_variant_key.erase(size(inv_variant_key)-5);
    }
}
