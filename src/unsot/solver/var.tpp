#pragma once

#include "solver/var.hpp"

namespace unsot::solver::var {
    template <typename B, typename InstT>
    template <TUPLE_WITH_SEQ_TYPENAMES>
    Variant_mixin<B, InstT>::Variant_mixin(Keys_ptr kptr, Values_ptr vptr,
                                           Ids_map_ptr imap_ptr, Id id_,
                                           TUPLE_WITH_SEQ_PARAMS(targs),
                                           Key ode_key_, Id final_vid,
                                           Flow_insts_ptr finsts_ptr,
                                           flow::Inst_id finst_id)
        : Inherit(move(kptr), move(vptr), move(imap_ptr), move(id_),
                  UNPACK_TUPLE(targs)),
          _ode_key(move(ode_key_)), _final_var_id(move(final_vid)),
          _flow_insts_ptr(move(finsts_ptr)), _flow_inst_id(move(finst_id))
    { }

    template <typename B, typename InstT>
    void Variant_mixin<B, InstT>::notify_set_variant() noexcept
    {
        flow_inst().notice_set_variant(this->cid());
    }

    template <typename B, typename InstT>
    void Variant_mixin<B, InstT>::notify_unset_variant() noexcept
    {
        flow_inst().notice_unset_variant(this->cid());
    }

    template <typename B, typename InstT>
    void Variant_mixin<B, InstT>::set_only_impl() noexcept
    {
        assert(this->is_unset());
        Inherit::set_only_impl();
        notify_set_variant();
    }

    template <typename B, typename InstT>
    void Variant_mixin<B, InstT>::unset_only_impl() noexcept
    {
        assert(this->is_set());
        notify_unset_variant();
        Inherit::unset_only_impl();
    }

    template <typename B, typename InstT>
    void Variant_mixin<B, InstT>::reset_impl() noexcept
    {
        Inherit::reset_impl();
        flow_inst().reset();
    }

    ////////////////////////////////////////////////////////////////

    template <typename B>
    void Ode_mixin<B>::add_assignees()
    {
        assert(contains(this->cfun().ckey_ids(), this->cfinal_var_id()));
        auto& finst = this->cflow_inst();
        assert(contains(finst.cfinal_var_ids(), this->cfinal_var_id()));
        for (auto& fid : finst.cfinal_var_ids()) {
            [[maybe_unused]] bool added = this->add_assignee(fid);
            assert(added);
            assert(!contains(carg_ids(), fid));
            assert(contains(cdepend_ids(), fid));
            assert(contains(this->cassignee_ids(), fid));
        }
    }

    template <typename B>
    void Ode_mixin<B>::notify_set_variant() noexcept
    {
        if (!this->falsified()) Inherit::notify_set_variant();
    }

    template <typename B>
    void Ode_mixin<B>::notify_unset_variant() noexcept
    {
        if (!this->falsified()) Inherit::notify_unset_variant();
    }

    template <typename B>
    Flag Ode_mixin<B>::surely_computable() const noexcept
    {
        if (auto flag = Inherit::surely_computable(); flag.valid())
            return flag;
        if (this->falsified()) return true;
        return this->cflow_inst().surely_evaluable();
    }

    template <typename B>
    Flag Ode_mixin<B>::computable_body() const noexcept
    {
        /// assignability does not influence computability
        return this->computable_body_not_assign();
    }

    template <typename B>
    Flag Ode_mixin<B>::surely_any_assignable() const noexcept
    {
        if (this->cflow_inst().surely_evaluable().valid()) return false;
        return Inherit::surely_any_assignable();
    }

    template <typename B>
    Flag Ode_mixin<B>::any_assignable_body() const noexcept
    {
        if (auto flag = computable_body(); !flag) return flag;
        auto& fvar = this->cfinal_var();
        assert(this->assignable(fvar));
        this->prepare_to_assign(fvar);
        return true;
    }

    template <typename B>
    bool Ode_mixin<B>::compute_body_prepare_not_assign()
    {
        return Inherit::Fun_t::Inherit::compute_body_prepare();
    }

    template <typename B>
    bool Ode_mixin<B>::compute_body_prepare_assign()
    {
        return this->flow_inst().eval(this->cid());
    }

    template <typename B>
    typename Ode_mixin<B>::Value
    Ode_mixin<B>::compute_body_value_not_assign(bool success)
    {
        assert(success);
        return Inherit::Fun_t::Inherit::compute_body_value(success);
    }
}
