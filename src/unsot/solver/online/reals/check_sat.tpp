#pragma once

#include "solver/online/reals/check_sat.hpp"

namespace unsot::solver::online::reals {
    template <typename B>
    void Mixin<B>::Check_sat::reset_of(Flow_inst& finst)
    {
        Inherit::reset_of(finst);

        auto& trigger = finst.trigger_ode();
        auto& fvar = trigger.final_var();
        assert(trigger.cid() == finst.ct_ode_id()
               || util::contains(finst.ccurrent_odes_view().caccessors(), trigger.cid()));

        for (auto& vptr : finst.current_invariants_view())
            reset_invariant(vptr);
        for (auto& vptr : finst.current_odes_view())
            reset_ode(vptr);
        reset_ode(finst.t_ode_ptr());

        fvar.depend_ids().clear();
        assert(all_of(finst.cfinal_var_ids(), [this](auto& fid){
            return empty(this->csolver().creals().cvar(fid).cdepend_ids());
        }));
    }

    template <typename B>
    void Mixin<B>::Check_sat::reset_flow_variant(var::Ptr& vptr)
    {
        const bool is_invariant = vptr->is_var();
        if (is_invariant) reset_invariant(vptr);
        else reset_ode(vptr);
    }

    template <typename B>
    void Mixin<B>::Check_sat::reset_invariant(var::Ptr& vptr)
    {
        if (!this->tracked(vptr)) return;
        this->reset_real_pred_body(vptr);
    }

    template <typename B>
    void Mixin<B>::Check_sat::reset_ode(var::Ptr& vptr)
    {
        if (!this->tracked(vptr)) return;
        this->reset_real_pred_with_assigned(vptr);
    }

    template <typename B>
    String Mixin<B>::Check_sat::profiling_to_string(Duration total) const
    {
        ostringstream oss;
        oss << Inherit::profiling_to_string(total);

        const Duration td = this->ct_propagate_ref().cduration();
        const Duration fd = this->ccompute_flows_duration();
        const auto fc = this->ccompute_flows_count();
        oss << Inherit::name() << "::compute_flows time: " << fd << " s (" << (fd/td)*100 << " % of " << T_propagate::name() << ")\n"
            << Inherit::name() << "::compute_flows: " << fc << " x (avg: " << (fd/fc) << ")\n"
            ;
        return oss.str();
    }
}

#include "solver/online/reals/check_sat/t_prop.tpp"
