#pragma once

#include "solver/online/reals/check_sat/t_prop.hpp"

namespace unsot::solver::online::reals {
    template <typename B>
    void Mixin<B>::Check_sat::T_propagate::post_check_sat_notice(var::Ptr& vptr)
    {
        Inherit::post_check_sat_notice(vptr);

        if (Invariant::is_me(vptr)) notice_invariant(vptr);
    }

    template <typename B>
    void Mixin<B>::Check_sat::T_propagate::notice_invariant(var::Ptr& vptr)
    {
        SMT_CVERB4LN(Inherit::name() << "::notice_invariant: " << vptr);
        assert(vptr->is_var());

        /// An inv. can be the last one that enables computation of the flow,
        /// and since skipped preds are removed from the queue,
        /// the inv. must cause the computation via a previously skipped ODE
        auto& inv = Invariant::cast(vptr);
        auto& finst = inv.flow_inst();
        if (!finst.all_variants_set()) return;

        this->notice_real_pred(finst.t_ode_ptr());
    }
}
