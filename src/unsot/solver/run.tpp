#pragma once

#include "solver/run.hpp"

#include "util/string/alg.hpp"

namespace unsot::solver {
    template <typename B, typename OdeSolver>
    Path Mixin<B, OdeSolver>::Run::plot_path_suffix(const flow::Id& fid) const
    {
        return "_" + csolver().cflow_def(fid).ckey();
    }

    template <typename B, typename OdeSolver>
    String Mixin<B, OdeSolver>::Run::usage() const
    {
        return Inherit::usage() + Ode_run::lusage();
    }

    template <typename B, typename OdeSolver>
    void Mixin<B, OdeSolver>::Run::init()
    {
        Inherit::init();
        Ode_run::linit();
    }

    template <typename B, typename OdeSolver>
    void Mixin<B, OdeSolver>::Run::do_stuff()
    {
        Inherit::do_stuff();
    }

    template <typename B, typename OdeSolver>
    void Mixin<B, OdeSolver>::Run::finish()
    {
        Inherit::finish();
        Ode_run::lfinish();
    }

    template <typename B, typename OdeSolver>
    String Mixin<B, OdeSolver>::Run::getopt_str() const noexcept
    {
        return Inherit::getopt_str() + Ode_run::lgetopt_str();
    }

    template <typename B, typename OdeSolver>
    bool Mixin<B, OdeSolver>::Run::process_opt(char c)
    {
        if (Inherit::process_opt(c)) return true;
        return Ode_run::lprocess_opt(c);
    }

    template <typename B, typename OdeSolver>
    void Mixin<B, OdeSolver>::Run::plot_trajects()
    {
        if (csolver().cmode() != Mode::sat) return;
        Ode_run::plot_trajects();
    }

    template <typename B, typename OdeSolver>
    void Mixin<B, OdeSolver>::Run::plot_traject(const flow::Id& fid)
    {
        if (starts_with(csolver().cflow_def(fid).ckey(),
                        Solver::aux_var_key_prefix)) return;
        Ode_run::plot_traject(fid);
    }
}
