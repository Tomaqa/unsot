#include "solver.hpp"
#include "solver/flow/def.hpp"

namespace unsot::solver::flow {
    Def::Def(Key dkey, expr::Keys real_arg_keys_, expr::Keys bool_arg_keys_)
        : _key(move(dkey)), _real_arg_keys(move(real_arg_keys_)),
          _bool_arg_keys(move(bool_arg_keys_))
    { }
}
