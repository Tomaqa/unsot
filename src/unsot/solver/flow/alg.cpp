#include "solver.hpp"
#include "solver/flow/alg.hpp"

#include "util/alg.hpp"

namespace unsot::solver::flow {
    expr::Keys make_arg_keys(expr::Keys init_keys, expr::Keys real_arg_keys)
    {
        return cat(move(init_keys), move(real_arg_keys));
    }

    expr::Keys make_masked_arg_keys(expr::Keys init_keys,
                                    expr::Keys real_arg_keys,
                                    const Mask& mask)
    {
        return make_masked_arg_keys(
            make_arg_keys(move(init_keys), move(real_arg_keys)), mask
        );
    }

    namespace {
        inline expr::Keys
        omit_masked_arg_keys(expr::Keys&& arg_keys, const Mask& mask,
                             Idx start_idx = 0, Idx end_idx = 0)
        {
            const int size_ = size(arg_keys);
            if (end_idx == 0) end_idx = size_;
            if (start_idx == end_idx) return move(arg_keys);

            expr::Keys res;
            res.reserve(size_);
            move_n(begin(arg_keys), start_idx, back_inserter(res));
            for (Idx idx = start_idx; idx < end_idx; ++idx) {
                auto& akey = arg_keys[idx];
                if (masked(mask, idx)) continue;
                res.push_back(move(akey));
            }
            move(std::next(begin(arg_keys), end_idx), end(arg_keys),
                 back_inserter(res));
            return res;
        }
    }

    expr::Keys make_masked_arg_keys(expr::Keys arg_keys, const Mask& mask)
    {
        return omit_masked_arg_keys(move(arg_keys), mask);
    }

    expr::Keys make_masked_arg_keys_keep_init(expr::Keys init_keys, expr::Keys real_arg_keys,
                                              const Mask& mask)
    {
        const size_t init_keys_size = size(init_keys);
        return make_masked_arg_keys_keep_n(
            make_arg_keys(move(init_keys), move(real_arg_keys)),
            mask, init_keys_size
        );
    }

    expr::Keys make_masked_arg_keys_keep_n(expr::Keys arg_keys, const Mask& mask, int keep_n)
    {
        return omit_masked_arg_keys(move(arg_keys), mask, keep_n);
    }
}
