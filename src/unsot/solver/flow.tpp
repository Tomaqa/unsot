#pragma once

#include "solver/flow.hpp"

namespace unsot::solver::flow {}

#include "solver/flow/def.tpp"
#include "solver/flow/inst.tpp"
