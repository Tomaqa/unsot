#pragma once

#include "solver/offline/reals/equal/check_sat.hpp"

namespace unsot::solver::offline::reals::equal {
    template <typename B>
    void Mixin<B>::Check_sat::pre_compute_of(Flow_inst& finst)
    {
        Inherit::pre_compute_of(finst);
        this->progress() = true;
        this->set_last_depend_id(finst.ctrigger_ode_id());
    }

    template <typename B>
    String Mixin<B>::Check_sat::profiling_to_string(Duration total) const
    {
        ostringstream oss;
        oss << Inherit::profiling_to_string(total);

        const Duration ud = this->cupdate_reals_duration();
        const Duration fd = this->ccompute_flows_duration();
        const auto fc = this->ccompute_flows_count();
        oss << "Compute flows time: " << fd << " s (" << (fd/ud)*100 << " % of updates)\n"
            << "Compute flows: " << fc << " x (avg: " << (fd/fc) << ")\n"
            ;
        return oss.str();
    }
}
