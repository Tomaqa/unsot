#include "solver.tpp"
#include "solver/run.tpp"

#ifndef NO_EXPLICIT_TP_INST
namespace unsot {
    template class aux::_S::Unsot_t::Mixin;
}
#endif  /// NO_EXPLICIT_TP_INST
