#include "smt/solver/bools.hpp"

namespace unsot::smt::solver {
    bool valid_arg_pairs(const Lists& arg_pairs) noexcept
    {
        return all_of(arg_pairs, [](auto& ls){
            return ls.size() == 2 && is_flat(ls);
        });
    }

    void check_arg_pairs(const Lists& arg_pairs)
    {
        expect(valid_arg_pairs(arg_pairs),
               "Invalid format of arguments: "s + to_string(arg_pairs));
    }

    String var_to_string(const var::Base& var)
    {
        return var.head_to_string() + var.value_to_string();
    }
}
