#pragma once

#include "smt/solver/reals/alg.hpp"

#include "util/alg.hpp"
#include "util/numeric/alg.hpp"

namespace unsot::smt::solver::reals::aux {
    template <bool dependV, typename M>
    M make_real_to_preds_views_map(M&& map, Preds& preds)
    {
        for (auto& vptr : preds) {
            auto& pred = Pred::cast(vptr);
            auto& pred_id = pred.cid();
            auto& rids = invoke([&pred]() -> auto& {
                if constexpr (dependV) return pred.cdepend_ids();
                else return pred.carg_ids();
            });
            for (auto& real_id : rids) {
                auto& vw = view(map, real_id);
                if constexpr (enabled_contains_v<decltype(vw)>)
                    assert(!vw.contains(pred_id));
                else assert(!util::contains(vw.caccessors(), pred_id));
                vw.push_back(pred_id);
            }
        }

        return move(map);
    }

    template <bool dependV, typename M, typename RealsMap, typename RealsT>
    M make_pred_to_preds_views_map(M&& map, const RealsMap& reals_map,
                                   const RealsT& reals, Preds& preds)
    {
        for (auto& vptr : preds) {
            auto& pred = Pred::cast(vptr);
            auto& pred_id = pred.cid();
            auto& vw = view(map, pred_id);
            auto& rids = invoke([&pred]() -> auto& {
                if constexpr (dependV) return pred.cdepend_ids();
                else return pred.carg_ids();
            });
            for (auto& real_id : rids) {
                if (reals.cptr(real_id)->locked()) continue;
                for (auto& pid : view(reals_map, real_id).caccessors()) {
                    if (pid == pred_id) continue;
                    vw.insert(pid);
                }
            }
        }

        bools::shrink_views_map(map);
        return move(map);
    }

    template <typename M, typename RealsMap, typename RealsT, typename BinPred>
    M make_real_to_assigners_views_map_tp(M&& map, const RealsMap& arg_reals_map,
                                          const RealsT& reals, BinPred keep_pred)
    {
        for (auto& rptr : reals) {
            if (rptr->locked()) continue;
            assert(rptr->is_unset());
            auto& real_id = rptr->cid();
            auto& assigners = view(map, real_id);
            auto& real_as_arg_vw = view(arg_reals_map, real_id);
            for (auto& pptr : real_as_arg_vw) {
                if (pptr->is_set() || !keep_pred(rptr, pptr)) continue;
                assert(util::contains(Preds::Types::Assigner::cast(pptr).cassignee_ids(), real_id));
                assigners.insert(pptr->cid());
            }
        }

        bools::shrink_views_map(map);
        return move(map);
    }

    template <typename M, typename RealsMap, typename RealsT>
    M make_real_to_assigners_views_map(M&& map, const RealsMap& arg_reals_map, const RealsT& reals)
    {
        using Ass = Preds::Types::Assigner;
        return make_real_to_assigners_views_map_tp(move(map), arg_reals_map, reals,
                                                   [](auto& rptr, auto& pptr){
            return Ass::is_me(pptr)
                && util::contains(Ass::cast(pptr).cassignee_ids(), rptr->cid());
        });
    }

    template <typename M, typename RealsMap, typename RealsT>
    M make_initial_real_to_assigners_views_map(M&& map, const RealsMap& arg_reals_map,
                                               const RealsT& reals)
    {
        using Ass = Preds::Types::Assigner;

        map = make_real_to_assigners_views_map_tp(move(map), arg_reals_map, reals,
                                                  [](auto& /*rptr*/, auto& pptr){
            return maybe_initial_assigner(pptr);
        });

        /// Keep only variables which assigners depend only on initial vars
        //+ not too efficient but sufficient?
        while (0 < erase_if(map, [&map, &arg_reals_map](auto& pair){
            auto& real_id = pair.first;
            auto& real_as_arg_vw = view(arg_reals_map, real_id);
            return any_of(real_as_arg_vw, [&](auto& pptr){
                if (!Ass::is_me(pptr)) return false;
                auto& ass = Ass::cast(pptr);
                if (!util::contains(ass.cassignee_ids(), real_id)) return false;

                return any_of(ass.carg_ids(), [&map](auto& aid){
                    return !map.contains(aid);
                });
            });
        }));

        return move(map);
    }

    template <typename V, typename InitAssignMap, typename RealsMap, typename RealsT>
    V make_sorted_initial_real_vars_view(const InitAssignMap& init_map,
                                         const RealsMap& arg_reals_map,
                                         RealsT& reals, const Preds& preds,
                                         [[maybe_unused]] Verbosity verb)
    {
        using Ass = Preds::Types::Assigner;
        using var::Distance;
        using var::inf_dist;

        const int init_size = size(init_map);
        static constexpr int min_init_vars = 3;
        /// It is no use for such small number of initial variables
        if (init_size < min_init_vars) {
            V init_vw(reals);
            to(init_vw.accessors(), init_map, [](auto& pair){ return pair.first; });
            return init_vw;
        }

        const int psize = preds.size();
        Hash<var::Id, Distance> max_dists;
        max_dists.reserve(init_size);

        Vector<Idx> idxs_map;
        Vector<Idx> aux_ass_idxs;
        Vector<Idx> aux_pred_idxs;
        idxs_map.reserve(psize);
        aux_ass_idxs.reserve(psize/2);
        aux_pred_idxs.reserve(psize/2);
        //+ hash pred's arguments -> do not insert preds with equivalent args
        for (Idx i = 0; i < psize; ++i) {
            auto& pptr = preds[i];
            //+ does not work well, e.g. ruins 'glucose' with 'INT_ODES'
            //+ maybe only because of not using `ass_size` in FW alg. (below)?
            //+ if (pptr->is_set()) continue;
            if (!Ass::is_me(pptr)) {
                aux_pred_idxs.push_back(i);
                continue;
            }
            auto& ass_ids = Ass::cast(pptr).cassignee_ids();
            if (empty(ass_ids)) {
                aux_pred_idxs.push_back(i);
                continue;
            }
            auto& aid = *cbegin(ass_ids);
            auto init_it = init_map.find(aid);
            if (init_it == init_map.end() || !init_it->second.contains(pptr->cid())) {
                aux_ass_idxs.push_back(i);
                continue;
            }

            assert(size(ass_ids) == 1);
            if (auto [_, inserted] = max_dists.insert({aid, inf_dist}); !inserted)
                continue;
            idxs_map.push_back(i);
        }
        assert(int(size(idxs_map)) == init_size);
        append(idxs_map, move(aux_ass_idxs));
        const int ass_size = size(idxs_map);
        append(idxs_map, move(aux_pred_idxs));
        const int idxs_size = size(idxs_map);
        assert(idxs_size <= psize);

        //+ just one vector
        Vector<Vector<Distance>> D(ass_size, Vector<Distance>(idxs_size, inf_dist));
        /// Init. the matrix only with edges from assigners to assignees
        for (Idx i = 0; i < ass_size; ++i) {
            auto& ass_ids = Ass::cast(preds[idxs_map[i]]).cassignee_ids();
            for (Idx j = 0; j < idxs_size; ++j) {
                if (i == j) {
                    D[i][j] = 0;
                    continue;
                }
                assert(D[i][j] == inf_dist);

                auto& pid = preds[idxs_map[j]]->cid();
                for (auto& aid : ass_ids) {
                    if (view(arg_reals_map, aid).contains(pid)) {
                        D[i][j] = 1;
                        break;
                    }
                }
            }
        }

        /// Floyd-Warshall algorithm using only rectangular submatrix
        for (Idx k = 0; k < ass_size; ++k) {
            //+ `ass_size` should be used somehow, this estimate is inaccurate
            //+ (e.g. it does not distinguish init. ass. in 'car' at all)
            //+ on the other hand, `init_size` "preserves going backwards",
            //+ especially through ODE flows (e.g. in 'glucose')
            for (Idx i = 0; i < init_size; ++i) {
                auto& dik = D[i][k];
                if (dik == inf_dist) continue;
                for (Idx j = 0; j < idxs_size; ++j) {
                    auto& d = D[i][j];
                    auto& dkj = D[k][j];
                    if (dkj == inf_dist) continue;
                    d = min(d, dik+dkj);
                }
            }
        }

        V init_vw(reals);
        for (Idx i = 0; i < init_size; ++i) {
            Distance max_dist = numeric::max(D[i], [](Distance d){
                return (d == inf_dist) ? 0 : d;
            });
            auto& rid = Ass::cast(preds[idxs_map[i]]).cassignee_id();
            init_vw.accessors().push_back(rid);
            max_dists[rid] = max_dist;
        }
        assert(init_vw.size() == size(init_map));

        if constexpr (_debug_) if (verb >= 4) {
            const int w = max(6UL, 1+numeric::max(preds, [](auto& vptr){
                return vptr->ckey().size();
            }));
            _SMT_CVERBLN("\"Distances\" from assigners:");
            _SMT_CVERB("\t");
            for (Idx i = 0; i < idxs_size; ++i)
                _SMT_CVERB(std::setw(w) << preds[idxs_map[i]]->ckey());
            _SMT_CVERB(endl);
            for (Idx i = 0; i < ass_size; ++i) {
                _SMT_CVERB(preds[idxs_map[i]]->ckey() << "\t");
                for (Idx j = 0; j < idxs_size; ++j) {
                    _SMT_CVERB(std::setw(w));
                    const Distance d = D[i][j];
                    if (d == inf_dist) _SMT_CVERB("inf");
                    else _SMT_CVERB(d);
                }
                _SMT_CVERB(endl);
            }
            _SMT_CVERB(endl);
            _SMT_CVERBLN("Max. \"distances\" of initial assigners:");
            for (auto& rptr : init_vw) {
                _SMT_CVERBLN(rptr << ": " << max_dists[rptr->cid()]);
            }
        }

        /// The "most initial" variables come first
        //++ the number of options (of all poss. assigners) must also be considered
        /// Stable sort should not be necessary, but at least
        /// it allows to "force" equally dist. assigners to come before others
        stable_sort(init_vw.accessors(), [&max_dists](auto& rid1, auto& rid2){
            return max_dists[rid1] > max_dists[rid2];
        });

        return init_vw;
    }
}
