#include "smt/solver/reals/alg.hpp"

#include "smt/solver/reals/alg.tpp"

namespace unsot::smt::solver::reals {
    bool maybe_initial_assigner(const var::Ptr& vptr) noexcept
    {
        using Ass = Preds::Types::Assigner;
        return Ass::is_me(vptr) && maybe_initial(Ass::cast(vptr));
    }

    bool maybe_initial(const Preds::Types::Assigner& ass) noexcept
    {
        return !empty(ass.cassignee_ids()) && ass.independent();
    }

    namespace aux {
        template Views_map<Preds_view>
            make_real_to_preds_views_map<true>(Views_map<Preds_view>&&, Preds&);
        template Views_map<Preds_view>
            make_real_to_preds_views_map<false>(Views_map<Preds_view>&&, Preds&);
        template Views_map<Preds_uniq_view>
            make_real_to_preds_views_map<true>(Views_map<Preds_uniq_view>&&, Preds&);
        template Views_map<Preds_uniq_view>
            make_real_to_preds_views_map<false>(Views_map<Preds_uniq_view>&&, Preds&);
        template Views_map<Preds_uniq_view>
            make_pred_to_preds_views_map<true>(Views_map<Preds_uniq_view>&&,
                                               const Views_map<Preds_uniq_view>&,
                                               const Reals<>&, Preds&);
        template Views_map<Preds_uniq_view>
            make_pred_to_preds_views_map<false>(Views_map<Preds_uniq_view>&&,
                                               const Views_map<Preds_uniq_view>&,
                                               const Reals<>&, Preds&);
        template Views_map<Preds_uniq_view>
            make_pred_to_preds_views_map<true>(Views_map<Preds_uniq_view>&&,
                                               const Views_map<Preds_uniq_view>&,
                                               const Reals<var::Type::auto_assignee>&, Preds&);
        template Views_map<Preds_uniq_view>
            make_pred_to_preds_views_map<false>(Views_map<Preds_uniq_view>&&,
                                               const Views_map<Preds_uniq_view>&,
                                               const Reals<var::Type::auto_assignee>&, Preds&);

        template Views_map<Preds_uniq_view>
            make_real_to_assigners_views_map(Views_map<Preds_uniq_view>&&,
                                             const Views_map<Preds_uniq_view>&,
                                             const Reals<>&);
        template Views_map<Preds_uniq_view>
            make_real_to_assigners_views_map(Views_map<Preds_uniq_view>&&,
                                             const Views_map<Preds_uniq_view>&,
                                             const Reals<var::Type::auto_assignee>&);
        template Views_map<Preds_uniq_view>
            make_initial_real_to_assigners_views_map(Views_map<Preds_uniq_view>&&,
                                             const Views_map<Preds_uniq_view>&,
                                             const Reals<>&);
        template Views_map<Preds_uniq_view>
            make_initial_real_to_assigners_views_map(Views_map<Preds_uniq_view>&&,
                                             const Views_map<Preds_uniq_view>&,
                                             const Reals<var::Type::auto_assignee>&);

        template View<>
            make_sorted_initial_real_vars_view(const Views_map<Preds_uniq_view>&,
                                               const Views_map<Preds_uniq_view>&,
                                               Reals<>&, const Preds&, Verbosity);
        template View<var::Type::auto_assignee>
            make_sorted_initial_real_vars_view(const Views_map<Preds_uniq_view>&,
                                               const Views_map<Preds_uniq_view>&,
                                               Reals<var::Type::auto_assignee>&,
                                               const Preds&, Verbosity);
    }
}
