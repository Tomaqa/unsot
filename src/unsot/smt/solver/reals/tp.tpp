#include "smt/solver/reals.tpp"

#ifndef NO_EXPLICIT_TP_INST
namespace TP_INST_SOLVER_NAMESPACE {
    template class aux::_S::Reals_t::Mixin;
}
#endif  /// NO_EXPLICIT_TP_INST
