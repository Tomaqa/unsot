#include "smt/solver.tpp"
#include "smt/solver/run.tpp"

#ifndef NO_EXPLICIT_TP_INST
namespace TP_INST_SOLVER_NAMESPACE {
    template class aux::_S::Base::Crtp;
}
#endif  /// NO_EXPLICIT_TP_INST
