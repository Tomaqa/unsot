#pragma once

#include "smt/solver/offline/bools/run.hpp"

namespace unsot::smt::solver::offline::bools {
    template <typename B>
    List Mixin<B>::Run::preprocess()
    {
        this->_macros_map["OFFLINE"];

        return Inherit::preprocess();
    }
}
