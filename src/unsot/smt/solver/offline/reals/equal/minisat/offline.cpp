#include "smt/solver/offline/reals/equal/minisat.hpp"

#include "smt/solver/offline/bools.tpp"
#include "smt/solver/offline/reals.tpp"
#include "smt/solver/offline/reals/equal.tpp"

#include "smt/solver/offline/bools/run.tpp"

#ifndef NO_EXPLICIT_TP_INST
namespace unsot::smt::solver::offline {
    template class solver::aux::_S::Offline_bools_t::Mixin;
    template class solver::aux::_S::Offline_reals_t::Mixin;
    template class solver::aux::_S::Offline_equal_t::Mixin;
}
#endif  /// NO_EXPLICIT_TP_INST
