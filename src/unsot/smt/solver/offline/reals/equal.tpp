#pragma once

#include "smt/solver/offline/reals/equal.hpp"

#include "util/alg.hpp"

namespace unsot::smt::solver::offline::reals::equal {
    template <typename B>
    void Mixin<B>::init_solve()
    {
        Inherit::init_solve();

        typename That::Set_reals_view(this->that()).perform();
        typename That::Set_real_preds_view(this->that()).perform();

        // preprocess_equalities();
    }

    /*
    template <typename B>
    void Mixin<B>::preprocess_equalities()
    {
        set_var_equalities();

        for (auto& var_eq_preds : cvar_equalities_map()) {
            if (size(var_eq_preds) <= 1) continue;
            auto clause = to<Clause>(var_eq_preds,
                [this](auto& vep){
                return this->creal_pred_key(vep.pred_id);
            });
            this->assert_cnf(Cnf::pair_conflict(move(clause)));
        }
    }

    template <typename B>
    void Mixin<B>::set_var_equalities()
    {
        var_equalities_map().resize(size(this->creals()));

        for (auto& vptr : this->real_preds()) {
            auto& pred = Pred::cast(vptr);
            if (pred.coper() != "=") continue;

            const Expr& expr = pred.carg_formula();
            if (expr.size() != 3) continue;
            if (pred.lhs_evaluable() || !pred.rhs_evaluable()) continue;
            auto& key_eptr = *++cbegin(expr);
            if (!Expr::is_etoken(key_eptr)) continue;

            auto& key = Expr::ccast_token(key_eptr);
            /// uniq. of value, (+according to `apx_equal') not checked!
            /// Real val = pred.rhs_eval();
            const auto var_id = pred.arg_id(key);
            auto& pred_id = pred.cid();
            auto& var_eq_preds = var_equalities(var_id);
            if (empty(var_eq_preds)) {
                var_eq_preds.reserve(16);
                var_eq_preds.push_back({pred_id});
                continue;
            }

            var_eq_preds.push_back({pred_id});
        }
    }
    */
}

////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////

namespace unsot::smt::solver::offline::reals::equal {
    template <typename B>
    typename Mixin<B>::Set_reals_view& Mixin<B>::Set_reals_view::perform()
    {
        auto& view = solver.reals_view();
        view.assign(solver.reals());
        /*
        sort(view.pointers(), [this](auto& ptr1, auto& ptr2){
            if (!Real_var::is_me(*ptr2)) return true;
            if (!Real_var::is_me(*ptr1)) return false;
            auto& var1 = Real_var::cast(ptr1);
            auto& var2 = Real_var::cast(ptr2);
            return var1.order() < var2.order();
        });
        */

        return *this;
    }

    ////////////////////////////////////////////////////////////////

    template <typename B>
    typename Mixin<B>::Set_real_preds_view&
    Mixin<B>::Set_real_preds_view::perform()
    {
        solver.real_preds_view().assign(solver.real_preds());

        return *this;
    }
}

#include "smt/solver/offline/reals/equal/check_sat.tpp"
