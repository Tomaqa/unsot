#pragma once

#include "smt/solver/offline/reals.hpp"

namespace unsot::smt::solver::offline::reals {}

#include "smt/solver/offline/reals/check_sat.tpp"
