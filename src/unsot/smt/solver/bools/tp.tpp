#include "smt/solver/bools.tpp"

#ifndef NO_EXPLICIT_TP_INST
namespace TP_INST_SOLVER_NAMESPACE {
    template class aux::_S::Bools_t::Mixin;
}
#endif  /// NO_EXPLICIT_TP_INST
