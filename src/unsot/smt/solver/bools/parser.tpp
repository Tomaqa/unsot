#pragma once

#include "smt/solver/bools/parser.hpp"

namespace unsot::smt::solver::bools {
    template <typename B, typename SatSolver>
    void Mixin<B, SatSolver>::Parser::
        post_preprocess_cmd(expr::Ptr& eptr, Key& cmd, Pos& pos)
    {
        Inherit::post_preprocess_cmd(eptr, cmd, pos);
        if (cmd == "ite") {
            return post_preprocess_cmd_ite(eptr, cmd, pos);
        }
        if (cmd == "distinct") {
            return post_preprocess_cmd_distinct(eptr, cmd, pos);
        }
        if (cmd == "declare-const") {
            return post_preprocess_cmd_declare_const(eptr, cmd, pos);
        }
    }

    template <typename B, typename SatSolver>
    void Mixin<B, SatSolver>::Parser::
        post_preprocess_cmd_ite(expr::Ptr&, Key& cmd, Pos& pos)
    {
        auto& ls = pos.list();
        expect(ls.size() == 4,
               "Invalid format: expected (ite <cond> <then> <else>)"s
               + ", got: " + pos.to_string());

        cmd = "and";
        auto cond_eptr = pos.extract();
        auto then_eptr = pos.extract();
        auto else_eptr = pos.extract();

        auto impl_eptr = new_key("=>");
        pos.insert(List::new_me({impl_eptr, cond_eptr, move(then_eptr)}));
        pos.insert(List::new_me({move(impl_eptr),
            List::new_me({new_key("not"), move(cond_eptr)}), move(else_eptr)
        }));
    }

    //+ add possibility of more arguments (also at "=" ?)
    template <typename B, typename SatSolver>
    void Mixin<B, SatSolver>::Parser::
        post_preprocess_cmd_distinct(expr::Ptr& eptr, Key& cmd, Pos& pos)
    {
        auto& ls = pos.list();
        expect(ls.size() == 3,
               "Invalid format: expected (distinct <arg1> <arg2>)"s
               + ", got: " + pos.to_string());

        cmd = "=";
        eptr = List::new_me({new_key("not"), move(eptr)});
    }

    template <typename B, typename SatSolver>
    void Mixin<B, SatSolver>::Parser::
        post_preprocess_cmd_declare_const(expr::Ptr&, Key& cmd, Pos& pos)
    {
        auto& ls = pos.list();
        expect(ls.size() == 3 && is_flat(ls),
               "Invalid format: expected (declare-const <const_id> <sort>)"s
               + ", got: " + pos.to_string());

        cmd = "declare-fun";
        pos.next().insert(List::new_me());
    }

    template <typename B, typename SatSolver>
    void Mixin<B, SatSolver>::Parser::post_preprocess_elem(expr::Ptr& eptr)
    {
        Inherit::post_preprocess_elem(eptr);
        if (!is_key(eptr)) return;
        auto& key_ = cast_key(eptr);
        if (key_ == "true") {
            eptr = new_bool(true);
            return;
        }
        if (key_ == "false") {
            eptr = new_bool(false);
            return;
        }
    }

    template <typename B, typename SatSolver>
    void Mixin<B, SatSolver>::Parser::parse_cmd(const Key& cmd, Pos& pos)
    try {
        Inherit::parse_cmd(cmd, pos);
    }
    catch (Dummy) {
        if (cmd == "declare-fun") {
            return parse_cmd_declare_fun(move(pos));
        }
        if (cmd == "define-fun") {
            return parse_cmd_define_fun(move(pos));
        }

        throw;
    }

    template <typename B, typename SatSolver>
    void Mixin<B, SatSolver>::Parser::parse_cmd_declare_fun(Pos&& pos)
    {
        auto& ls = pos.list();
        expect(ls.size() == 4 && is_key_at(pos)
               && is_flat_list_at(pos.next()) && is_key_at(pos.next()),
               "Invalid format: expected (declare-fun <fun_id> (<arg_sort>*) <sort>)"s
               + ", got: " + pos.to_string());
        pos.set_it().next();

        const auto& key_ = get_key(pos);
        auto arg_sorts = cast_to_keys_check(get_list(pos));
        const auto& sort = peek_key(pos);

        expect(empty(arg_sorts),
               "Declaring uninterpreted functions is not supported: "s + to_string(arg_sorts));

        this->solver().declare_var(key_, sort);
    }

    template <typename B, typename SatSolver>
    void Mixin<B, SatSolver>::Parser::parse_cmd_define_fun(Pos&& pos)
    {
        auto& ls = pos.list();
        expect(ls.size() == 5 && is_key_at(pos)
               && is_deep_list_at(pos.next()) && is_key_at(pos.next()),
               "Invalid format: expected (define-fun <fun_id> ((<arg> <arg_sort>)*) <sort> <body>)"s
               + ", got: " + pos.to_string());
        pos.set_it().next();

        const auto& key_ = get_key(pos);
        auto arg_pairs = cast_to_lists(get_list(pos));
        const auto& sort = get_key(pos);
        const expr::Ptr& eptr = pos.peek();

        if (Elem_base::is_me(eptr)) {
            expect(empty(arg_pairs),
                   "Unexpected arguments in variable definition: "s + to_string(arg_pairs));
            return this->solver().define_var(key_, sort, Elem::cast(eptr));
        }

        check_arg_pairs(arg_pairs);
        //+ resolve in future ...
        expect(all_equal(arg_pairs, [](auto& ls1, auto& ls2){
                   return cast_key(ls1.cback()) == cast_key(ls2.cback());
               }),
               "Defining function with various argument sorts is not supported yet: "s
               + to_string(arg_pairs));

        Key args_sort;
        Keys arg_keys;
        if (!empty(arg_pairs)) {
            args_sort = move(cast_key(arg_pairs.front().back()));
            arg_keys = to<Keys>(move(arg_pairs), [](auto&& l){
                return cast_key(move(l.front()));
            });
        }

        this->solver().define_fun(key_, sort, Formula::cons(List::cast(eptr)),
                                  move(arg_keys), args_sort);
    }
}
