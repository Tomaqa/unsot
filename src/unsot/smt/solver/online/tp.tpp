#include "smt/solver/online.tpp"
#include "smt/solver/online/run.tpp"

#include "smt/solver/strategy.tpp"

#ifndef NO_EXPLICIT_TP_INST
namespace TP_INST_SOLVER_NAMESPACE {
    template class aux::_S::Online_t::Mixin;
}
#endif  /// NO_EXPLICIT_TP_INST
