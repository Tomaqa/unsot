#pragma once

#include "smt/solver/online/bools.hpp"

namespace unsot::smt::solver::online::bools {
    template <typename B>
    Mixin<B>::Mixin()
    {
        lset_check_sat_strategy();

        this->option(":t-learn-strategy");
    }

    template <typename B>
    void Mixin<B>::lset_check_sat_strategy()
    {
        this->sat_solver().connect(this->check_sat_ref());
    }

    template <typename B>
    bool Mixin<B>::valid_option_value(const Option& opt, const Option_value& val) const
    try {
        return Inherit::valid_option_value(opt, val);
    }
    catch (Dummy) {
        if (opt == ":t-learn-strategy") return true;

        throw;
    }

    template <typename B>
    void Mixin<B>::apply_option(const Option& opt)
    try {
        Inherit::apply_option(opt);
    }
    catch (Dummy) {
        auto& val = this->coption(opt);
        if (opt == ":t-learn-strategy") return this->set_t_learn_strategy(val);

        throw;
    }

    template <typename B>
    void Mixin<B>::set_all_strategies_impl(const String& name, bool force)
    {
        bool any_success = false;
        try {
            Inherit::set_all_strategies_impl(name, force);
            any_success = true;
        }
        catch (Dummy) { }

        try {
            set_t_learn_strategy(name, force);
            any_success = true;
        }
        //+ only "unknown strategy" error, not other errors ... ideally just catch dummy
        catch (const Error&) { }

        if (!any_success) throw dummy;
    }

    template <typename B>
    void Mixin<B>::set_t_suggest_decision_strategy_impl(const String& name, bool force)
    try {
        Inherit::set_t_suggest_decision_strategy_impl(name, force);
    }
    catch (Dummy) {
        using Order = typename Check_sat::T_suggest_decision::Order;

        if (name == Order::svariant_name())
            return this->check_sat_ref().template set_t_suggest_decision_strategy_tp<Order>(force);

        throw;
    }

    template <typename B>
    void Mixin<B>::set_t_learn_strategy_impl(const String& name, bool force)
    {
        bool any_success = false;
        try {
            set_t_explain_strategy(name, force);
            any_success = true;
        }
        //+ only "unknown strategy" error, not other errors ... ideally just catch dummy
        catch (const Error&) { }
        try {
            set_analyze_t_inconsistency_strategy(name, force);
            any_success = true;
        }
        catch (const Error&) { }

        if (!any_success) throw dummy;
    }

    template <typename B>
    void Mixin<B>::set_t_explain_strategy_impl(const String& /*name*/, bool /*force*/)
    {
        throw dummy;
    }

    template <typename B>
    void Mixin<B>::set_analyze_t_inconsistency_strategy_impl(const String& /*name*/, bool /*force*/)
    {
        throw dummy;
    }
}

#include "smt/solver/online/bools/check_sat.tpp"
