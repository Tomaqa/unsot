#pragma once

#include "smt/solver/online/run.hpp"

namespace unsot::smt::solver::online {
    template <typename B>
    String Mixin<B>::Run::usage() const
    {
        return Inherit::usage() + lusage();
    }

    template <typename B>
    String Mixin<B>::Run::lusage() const
    {
        return this->usage_row('S', "Set a T-strategy");
    }

    template <typename B>
    String Mixin<B>::Run::getopt_str() const noexcept
    {
        return Inherit::getopt_str() + lgetopt_str();
    }

    template <typename B>
    String Mixin<B>::Run::lgetopt_str() const noexcept
    {
        return "S:";
    }

    template <typename B>
    bool Mixin<B>::Run::process_opt(char c)
    {
        if (Inherit::process_opt(c)) return true;
        return lprocess_opt(c);
    }

    template <typename B>
    bool Mixin<B>::Run::lprocess_opt(char c)
    {
        auto& s = this->solver();

        switch (c) {
        case 'S':
            //+ allow printing all supported variants
            s.set_all_strategies(optarg, do_force);
            return true;
        }

        return false;
    }

    template <typename B>
    List Mixin<B>::Run::preprocess()
    {
        this->_macros_map["ONLINE"];

        return Inherit::preprocess();
    }
}
