#pragma once

#include "smt/solver/online/check_sat.hpp"

#include <sstream>
#include <omp.h>

namespace unsot::smt::solver::online {
    template <typename B>
    void Mixin<B>::Check_sat::perform_init_profiled()
    {
        t_propagate_ref().pre_check_sat_init();
        t_suggest_decision_ref().pre_check_sat_init();
        Inherit::perform_init_profiled();
        t_propagate_ref().post_check_sat_init();
        t_suggest_decision_ref().post_check_sat_init();
    }

    template <typename B>
    void Mixin<B>::Check_sat::notice_backtrack_to(int level)
    {
        SMT_CVERB3LN("backtrack: " << this->decision_level() << " -> " << level);

        const bool prof = this->cprofiling();
        double start;
        if (prof) start = omp_get_wtime();

        Online_check_sat_base::notice_backtrack_to(level);

        if (prof) {
            const double finish = omp_get_wtime();
            const Duration duration = finish - start;
            t_backtrack_duration() += duration;
            ++t_backtrack_count();
        }
    }

    template <typename B>
    void Mixin<B>::Check_sat::notice_restart()
    {
        t_propagate_ref().pre_check_sat_restart();
        t_suggest_decision_ref().pre_check_sat_restart();

        SMT_CVERB3LN("restart (from " << this->decision_level() << ")");
        restarting() = true;
        notice_restart_impl();
        restarting() = false;

        t_propagate_ref().post_check_sat_restart();
        t_suggest_decision_ref().post_check_sat_restart();
    }

    template <typename B>
    void Mixin<B>::Check_sat::notice_restart_impl()
    {
        Online_check_sat_base::notice_restart();
    }

    template <typename B>
    String Mixin<B>::Check_sat::profiling_to_string(Duration total) const
    {
        ostringstream oss;
        oss << Inherit::profiling_to_string(total);

        const Duration sd = this->cduration();
        const Duration bd = ct_backtrack_duration();
        const auto bc = ct_backtrack_count();
        oss << "t-backtrack time: " << bd << " s (" << (bd/sd)*100 << " % of solve)\n"
            << "t-backtracks: " << bc << " x (avg: " << (bd/bc) << ")\n"
            << ct_propagate_ref().profiling_to_string(sd)
            << ct_suggest_decision_ref().profiling_to_string(sd)
            ;
        return oss.str();
    }
}

////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////

namespace unsot::smt::solver::online {
    template <typename B>
    template <typename B2, typename... Args>
    void Mixin<B>::Check_sat::Strategy_mixin<B2, Args...>::pre_check_sat_init()
    {
        this->profiling() = this->ccheck_sat_ref().cprofiling();
    }

    template <typename B>
    template <typename B2, typename... Args>
    void Mixin<B>::Check_sat::Strategy_mixin<B2, Args...>::post_check_sat_init()
    {
        SMT_CVERB1LN("using " << this->full_name());
    }

    template <typename B>
    template <typename B2, typename... Args>
    void Mixin<B>::Check_sat::Strategy_mixin<B2, Args...>::perform_init(Args&... args)
    {
        Inherit::perform_init(args...);

        progressed() = false;
    }

    template <typename B>
    template <typename B2, typename... Args>
    void Mixin<B>::Check_sat::Strategy_mixin<B2, Args...>::perform_finish()
    {
        Inherit::perform_finish();

        if (this->cprofiling()) {
            if (cprogressed()) ++progressed_count();
        }
    }

    template <typename B>
    template <typename B2, typename... Args>
    void Mixin<B>::Check_sat::Strategy_mixin<B2, Args...>::post_check_sat_restart()
    {
        restart();
    }

    template <typename B>
    template <typename B2, typename... Args>
    void Mixin<B>::Check_sat::Strategy_mixin<B2, Args...>::restart()
    {
        SMT_CVERB4LN(Inherit::name() << "::restart");

        progressed() = false;
    }

    template <typename B>
    template <typename B2, typename... Args>
    String Mixin<B>::Check_sat::Strategy_mixin<B2, Args...>::profiling_to_string(Duration total) const
    {
        ostringstream oss;
        oss << Inherit::profiling_to_string(total)
            << Inherit::name() << " count (progressed): " << cprogressed_count() << " x\n"
            ;
        return oss.str();
    }
}

#include "smt/solver/online/check_sat/t_prop.tpp"
#include "smt/solver/online/check_sat/t_sugg.tpp"
