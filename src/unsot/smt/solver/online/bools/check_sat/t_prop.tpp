#pragma once

#include "smt/solver/online/bools/check_sat/t_prop.hpp"

namespace unsot::smt::solver::online::bools {
    template <typename B>
    template <bool doAssert>
    bool Mixin<B>::Check_sat::T_propagate::maybe_performed_with_tp(const var::Ptr& vptr) const noexcept
    {
        const bool res = maybe_performed_with_impl(vptr);
        /// !maybe_performed_with => !performed_with
        if constexpr (doAssert) {
            assert(res || !performed_with_tp<no_assert>(vptr));
        }
        return res;
    }

    template <typename B>
    bool Mixin<B>::Check_sat::T_propagate::maybe_performed_with_impl(const var::Ptr& vptr) const noexcept
    {
        return vptr->surely_consistent();
    }

    template <typename B>
    template <bool doAssert>
    bool Mixin<B>::Check_sat::T_propagate::performed_with_tp(const var::Ptr& vptr) const noexcept
    {
        /// It is currently not used, only for assertions ...
        if constexpr (_debug_) {
            const bool res = performed_with_impl(vptr);
            /// performed_with => maybe_performed_with
            if constexpr (doAssert) {
                assert(!res || maybe_performed_with_tp<no_assert>(vptr));
            }
            return res;
        }

        EXIT("Implemented only in debug mode!");
    }

    template <typename B>
    bool Mixin<B>::Check_sat::T_propagate::performed_with_impl(const var::Ptr& vptr) const noexcept
    {
        return cperformed_bools().contains(vptr->cid());
    }

    template <typename B>
    void Mixin<B>::Check_sat::T_propagate::post_check_sat_t_backtrack_bool(var::Ptr& vptr)
    {
        Inherit::post_check_sat_t_backtrack_bool(vptr);

        if constexpr (_debug_) {
            const bool was_t_propagated = performed_with_tp<no_assert>(vptr);
            assert(!maybe_performed_with_tp<no_assert>(vptr));

            const int cnt = performed_bools().erase(vptr->cid());
            assert(!performed_with(vptr));
            assert(!was_t_propagated || cnt == 1);
            assert(was_t_propagated || cnt == 0);
            if (was_t_propagated) SMT_CVERB4LN("backtracked " << T_propagate::name() << "d: " << vptr);
        }
    }

    template <typename B>
    void Mixin<B>::Check_sat::T_propagate::perform_with(var::Ptr& vptr, const var::Ptr& from)
    {
        perform_print_with(vptr, from);
        SMT_CVERB3(endl);
        perform_with_impl(vptr, from);
    }

    template <typename B>
    void Mixin<B>::Check_sat::T_propagate::perform_print_with(const var::Ptr& vptr, const var::Ptr& from) const
    {
        SMT_CVERB3(Inherit::name() << ": " << vptr << " <- " << from);
    }

    template <typename B>
    void Mixin<B>::Check_sat::T_propagate::perform_with_impl(var::Ptr& vptr, const var::Ptr& from)
    {
        auto& ch = this->check_sat_ref();

        assert(vptr->cid() != from->cid());
        assert(ch.tracked(vptr));
        assert(maybe_performed_with(vptr));
        assert(!performed_with(vptr));

        ch.track(vptr);
        if constexpr (_debug_) {
            auto [_, inserted] = performed_bools().insert(vptr->cid());
            assert(inserted);
            assert(performed_with(vptr));
        }
        assert(ch.cdecision_level_of(vptr->cid()) == ch.decision_level());
        assert(ch.cdecision_level_of(vptr->cid()) >= ch.cdecision_level_of(from->cid()));

        if (!maybe_learn_from(vptr, from)) perform_not_learn_with(vptr);
    }

    template <typename B>
    void Mixin<B>::Check_sat::T_propagate::perform_not_learn_with(var::Ptr& vptr)
    {
        auto& s = this->solver();
        auto& sat_vid = s.csat_var_id(vptr->cid());
        assert(vptr->is_set());
        const bool val = Var::cast(vptr).get();
        SMT_CVERB4LN(Inherit::name() << ": sat_propagate: [" << sat_vid << "] <- " << val);
        s.sat_solver().sat_propagate(sat_vid, val);
    }

    template <typename B>
    bool Mixin<B>::Check_sat::T_propagate::maybe_learn_from(var::Ptr& vptr, const var::Ptr& from)
    {
        if (should_not_learn_from(vptr, from)) return false;
        if (should_learn_from(vptr, from)) {
            learn_from(vptr, from);
            return true;
        }

        return maybe_learn_from_anyway(vptr, from);
    }

    template <typename B>
    bool Mixin<B>::Check_sat::T_propagate::should_not_learn_from(const var::Ptr&, const var::Ptr& /*from*/) const
    {
        //+ have not found a conf. with improved results
        //+ return cdecision_level_of(vptr->cid()) == cdecision_level_of(from->cid());
        return true;
    }

    template <typename B>
    bool Mixin<B>::Check_sat::T_propagate::should_learn_from(const var::Ptr&, const var::Ptr& /*from*/) const
    {
        return false;
    }

    template <typename B>
    void Mixin<B>::Check_sat::T_propagate::learn_from(var::Ptr& vptr, const var::Ptr& from)
    {
        SMT_CVERB4LN(Inherit::name() << "::learn_from: " << vptr << " <- " << from);
        this->check_sat_ref().t_explain_ref().perform(vptr);
    }

    template <typename B>
    bool Mixin<B>::Check_sat::T_propagate::maybe_learn_from_anyway(var::Ptr& vptr, const var::Ptr& from)
    {
        assert(!should_not_learn_from(vptr, from));
        assert(!should_learn_from(vptr, from));
        return maybe_learn_short_clause_from(vptr, from);
    }

    template <typename B>
    bool Mixin<B>::Check_sat::T_propagate::maybe_learn_short_clause_from(var::Ptr& vptr, const var::Ptr& from)
    {
        SMT_CVERB4LN(Inherit::name() << "::learn_short_clause_from: " << vptr << " <- " << from);
        const var::Distance max_len = short_clause_threshold();
        return this->check_sat_ref().t_explain_ref().perform_limited(vptr, max_len);
    }

    template <typename B>
    var::Distance Mixin<B>::Check_sat::T_propagate::short_clause_threshold() const
    {
        return 2;
    }

    template <typename B>
    void Mixin<B>::Check_sat::T_propagate::restart()
    {
        if constexpr (_debug_) {
            performed_bools().clear();
        }
    }
}
