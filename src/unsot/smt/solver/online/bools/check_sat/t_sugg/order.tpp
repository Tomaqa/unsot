#pragma once

#include "smt/solver/online/bools/check_sat/t_sugg/order.hpp"

namespace unsot::smt::solver::online::bools {
    template <typename B>
    template <typename B2>
    void Mixin<B>::Check_sat::T_suggest_decision::Order_mixin<B2>::post_check_sat_init()
    {
        Inherit::post_check_sat_init();

        this->enabled() = true;
    }

    template <typename B>
    template <typename B2>
    Optional<typename Mixin<B>::Check_sat::T_suggest_decision::template Order_mixin<B2>::Ret>
    Mixin<B>::Check_sat::T_suggest_decision::Order_mixin<B2>::perform_body()
    {
        var::Id vid = -1;
        if (this->valid_current_state()) {
            vid = ccurrent_bool_id();
            assert(vid >= 0);
        }

        auto& solver = this->csolver();
        auto& bools = solver.cbools();
        const int size_ = bools.size();
        ++vid;
        assert(vid < size_);

        for (; vid < size_; ++vid) {
            auto& vptr = bools.cptr(vid);
            if (vptr->is_set()) continue;

            return this->perform_with(vptr);
        }

        /// There can be the last dummy decision that does nothing
        return Inherit::perform_body();
    }

    template <typename B>
    template <typename B2>
    void Mixin<B>::Check_sat::T_suggest_decision::Order_mixin<B2>::
        perform_print_with(const var::Ptr& vptr, const Flag& val) const
    {
        Inherit::perform_print_with(vptr, val);
        SMT_CVERB3("[" << vptr->cid() << "]");
    }

    template <typename B>
    template <typename B2>
    void Mixin<B>::Check_sat::T_suggest_decision::Order_mixin<B2>::backtrack_impl()
    {
        Inherit::backtrack_impl();
        --current_bool_id();
        assert(this->invalid_current_bool() || this->ccurrent_bool_ptr()->is_set());
    }

    template <typename B>
    template <typename B2>
    void Mixin<B>::Check_sat::T_suggest_decision::Order_mixin<B2>::restart()
    {
        Inherit::restart();
        current_bool_id() = var::invalid_id;
    }
}
