#include "smt/solver/online/bools.tpp"
#include "smt/solver/online/bools/run.tpp"

#include "smt/solver/strategy.tpp"
#include "smt/solver/online.tpp"

#ifndef NO_EXPLICIT_TP_INST
namespace TP_INST_SOLVER_NAMESPACE {
    template class aux::_S::Online_bools_t::Mixin;
}
#endif  /// NO_EXPLICIT_TP_INST
