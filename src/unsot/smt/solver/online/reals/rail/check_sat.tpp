#pragma once

#include "smt/solver/online/reals/rail/check_sat.hpp"

#include "util/alg.hpp"
#include "util/string/alg.hpp"
#include "expr/alg.hpp"

namespace unsot::smt::solver::online::reals::rail {
    template <typename B>
    template <typename B2>
    Mixin<B>::Check_sat::Rail_mixin<B2>::Rail_mixin()
    {
        static_init();
    }

    template <typename B>
    template <typename B2>
    Mixin<B>::Check_sat::Rail_mixin<B2>::Rail_mixin(Rail_mixin&& rhs)
        : Inherit(move(rhs))
        //+ , _timing_to_visits_map(move(rhs._timing_to_visits_map))
    {
        static_init();
    }

    template <typename B>
    template <typename B2>
    typename Mixin<B>::Check_sat::template Rail_mixin<B2>&
    Mixin<B>::Check_sat::Rail_mixin<B2>::operator =(Rail_mixin&& rhs)
    {
        if (this == &rhs) return *this;

        Inherit::operator =(move(rhs));

        //+ _timing_to_visits_map = move(rhs._timing_to_visits_map);

        static_init();

        return *this;
    }

    template <typename B>
    template <typename B2>
    void Mixin<B>::Check_sat::Rail_mixin<B2>::static_init()
    {
        this->template set_t_propagate_strategy_tp<T_propagate>();
        this->template set_t_suggest_decision_strategy_tp<T_suggest_decision>();
        this->template set_analyze_t_inconsistency_strategy_tp<Analyze_t_inconsistency>();
    }

/*+ not currently used, because it would require more detailed learning - not only the visits alone,
    but considering all boolean combinations that are possible to appear in schedule constraints

    template <typename B>
    template <typename B2>
    bool Mixin<B>::Check_sat::Rail_mixin<B2>::is_timing(const var::Ptr& vptr) const
    {
        if (vptr->is_var()) return false;

        return ctiming_to_visits_map().contains(vptr->cid());
    }

    template <typename B>
    template <typename B2>
    bool Mixin<B>::Check_sat::Rail_mixin<B2>::compute_is_timing(const var::Ptr& vptr) const
    {
        if (vptr->is_var()) return false;

        auto& pred = Pred::cast(vptr);
        //+ identify it with a named (defined) function
        auto& phi = pred.cformula();
        if (phi.size() != 2) return false;
        if (!is_value(phi.cback())) return false;
        /// Do not store "greater-than" timings since they do not trigger any propagations
        if (!util::contains({"<", "<=", "=", "~", "<<", "<~"}, phi.coper())) return false;

        auto& s = this->csolver();
        const auto is_t_var_f = [&s](auto& eptr){
            if (!is_key(eptr)) return false;
            auto& p = s.creals().cptr(cast_key(eptr));
            if (!p->is_var()) return false;
            return s.is_t_key(p->ckey());
        };

        auto& lhs = phi.cfront();
        /// Absolute or relative timing
        //!! distinguish them, relative needs to learn extra implication
        if (!List::is_me(lhs)) return is_t_var_f(lhs);

        auto& lhs_phi = Formula::cast(lhs);
        if (lhs_phi.size() != 2 || lhs_phi.coper() != "-") return false;
        return all_of(lhs_phi, is_t_var_f);
    }

    template <typename B>
    template <typename B2>
    void Mixin<B>::Check_sat::Rail_mixin<B2>::perform_init_impl()
    {
        Inherit::perform_init_impl();

        set_timing_to_visits_map();
        if (!SMT_VERBOSITY3) return;
        _SMT_CVERBLN("timing_to_visits_map:");
        auto& s = this->csolver();
        auto& bools = s.cbools();
        for (auto& [vid, entry] : ctiming_to_visits_map()) {
            auto& vptr = bools.cptr(vid);
            _SMT_CVERB(vptr << " : ");
            _SMT_CVERBLN(entry.visits);
        }
        _SMT_CVERB(endl);
    }

    template <typename B>
    void Mixin<B>::Check_sat::set_timing_to_visits_map()
    {
        auto& s = this->csolver();
        auto& bools = s.cbools();
        //+ it would be better to do it right within parsing
        for_each_if_formula_rec_pre(s.cbool_formula(), [this, &bools](auto& eptr){
            auto& phi = Formula::cast(eptr);
            if (phi.size() != 2 || phi.coper() != "=>") return;
            auto& rhs = phi.cback();
            if (!is_key(rhs)) return;
            auto& t_ptr = bools.cptr(cast_key(rhs));
            if (!compute_is_timing(t_ptr)) return;

            auto& visit_key = cast_key(phi.cfront());
            if constexpr (_debug_) {
                assert(bools.contains(visit_key));
                auto& visit_ptr = bools.cptr(visit_key);
                assert(visit_ptr->is_var());
                assert(visit_ptr->ckey() == visit_key);
            }

            timing_to_visits_map_entry(t_ptr->cid()).visits.push_back({&visit_key});
        });
    }
*/
}

#include "smt/solver/online/reals/rail/check_sat/t_prop.tpp"
#include "smt/solver/online/reals/rail/check_sat/t_sugg.tpp"
#include "smt/solver/online/reals/rail/check_sat/t_learn.tpp"
