#pragma once

#include "smt/solver/online/reals/rail/check_sat/t_prop.hpp"

#include "util/ref.hpp"

namespace unsot::smt::solver::online::reals::rail {
    template <typename B>
    template <typename B2>
    Optional<typename Mixin<B>::Check_sat::template Rail_mixin<B2>::T_propagate::Perform_with_result>
    Mixin<B>::Check_sat::Rail_mixin<B2>::T_propagate::try_perform_with_not_inferenced(const var::Ptr& vptr)
    {
        if (auto opt_ret = Inherit::try_perform_with_not_inferenced(vptr)) return opt_ret;

        /*+
        auto& ch = this->ccheck_sat_ref();
        if (ch.is_timing(vptr)) return perform_with_timing(vptr);
        */

        return {};
    }
/*
    template <typename B>
    template <typename B2>
    typename Mixin<B>::Check_sat::template Rail_mixin<B2>::T_propagate::Perform_with_result
    Mixin<B>::Check_sat::Rail_mixin<B2>::T_propagate::perform_with_timing(const var::Ptr& vptr)
    {
        assert(vptr->consistent());
        auto& pred = Pred::cast(vptr);
        if (pred.get()) return {};

        auto& s = this->solver();
        auto& ch = this->check_sat_ref();
        assert(ch.ctiming_to_visits_map().contains(pred.cid()));
        //+ it was already found in `is_timing`
        auto& entry = ch.timing_to_visits_map_entry(pred.cid());
        if (entry.learned) return {};

        /// lazily learn "!pred(k) => V_l=0^k-1 visit*(l)"
        /// (use only less operator in the case when pred is "="/"~")
        //!! "visit1* =>" for relative timings is missing
        SMT_CVERB3LN(Inherit::name() << "::perform_with_timing: " << vptr);

        Ref<const Key> pred_key_ref;
        auto& phi = pred.cformula();
        if (!util::contains({"=", "~"}, phi.coper())) pred_key_ref = pred.ckey();
        else {
            auto phi_cp = phi;
            phi_cp.oper() = "<"s + move(phi_cp.oper());
            phi_cp.virtual_init();
            Key key_ = s.instantiate_real_pred(move(phi_cp));
            //+ the variable was recently stored, looking up again
            auto& p = s.real_preds().ptr(key_);
            Pred::cast(p).compute();
            this->perform_with(p, vptr);

            pred_key_ref = move(key_);
        }
        auto& pred_key = pred_key_ref.citem();

        short learned_count{};
        auto& t_exp = ch.t_explain_ref();
        for (auto& visit : entry.visits) {
            const Key& visit_key = *visit.key_link;
            const Idx k = s.compute_bmc_step_of(visit_key);
            assert(k != invalid_idx);

            Clause confl;
            confl.reserve(k+1);
            confl.push_back(pred_key);
            Key v_key = visit_key;
            for (Idx l = 0; l < k; ++l) {
                s.replace_bmc_step_of(v_key, l);
                if constexpr (_debug_) {
                    auto& bools = s.cbools();
                    assert(bools.contains(v_key));
                    auto& visit_ptr = bools.cptr(v_key);
                    assert(visit_ptr->is_var());
                    assert(visit_ptr->ckey() == v_key);
                }
                confl.push_back(v_key);
            }

            t_exp.perform_with(move(confl));
            if (k > 1) learned_count += k-1;
        }

        entry.learned = true;
        return {0, learned_count};
    }
*/
}
