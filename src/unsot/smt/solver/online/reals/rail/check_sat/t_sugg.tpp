#pragma once

#include "smt/solver/online/reals/rail/check_sat/t_sugg.hpp"

#include "util/string/alg.hpp"

namespace unsot::smt::solver::online::reals::rail {
    namespace {
        inline bool key_match(const var::Ptr& vptr, const String& str)
        {
            return util::contains(vptr->ckey(), str);
        }

        //+ using multiple checks instead of a single
        inline bool key_match_mode(const var::Ptr& vptr, const String& mode)
        {
            assert(vptr->is_var());
            return key_match(vptr, mode) && !key_match(vptr, "maybe_");
        }

        //+ using multiple checks instead of a single
        inline bool key_match_next_seg(const var::Ptr& vptr)
        {
            assert(vptr->is_var());
            return key_match(vptr, "next_") && key_match(vptr, "->");
        }
    }

    template <typename B>
    template <typename B2>
    template <typename B3>
    typename Mixin<B>::Check_sat::template Rail_mixin<B2>::template T_suggest_decision_bmc_mixin<B3>::Var_info
    Mixin<B>::Check_sat::Rail_mixin<B2>::T_suggest_decision_bmc_mixin<B3>::compute_var_info(const var::Ptr& vptr)
    {
        auto info = Inherit::compute_var_info(vptr);
        if (info.bmc_step == invalid_idx) return info;
        if (!vptr->is_var()) return info;

        auto& val = info.suggest_value;
        if (key_match(vptr, "enter")) val = true;
        else if (key_match_mode(vptr, "idle")) val = false;
        else if (key_match_next_seg(vptr)) val = false;

        return info;
    }

    template <typename B>
    template <typename B2>
    template <typename B3>
    Flag Mixin<B>::Check_sat::Rail_mixin<B2>::T_suggest_decision_bmc_mixin<B3>::
        sort_bool_view_compare_rest(const var::Ptr& vptr1, const var::Ptr& vptr2) const
    {
        if (auto flag = Inherit::sort_bool_view_compare_rest(vptr1, vptr2); flag.valid()) return flag;

        const bool is_var1 = vptr1->is_var();
        const bool is_var2 = vptr2->is_var();
        if (!is_var1 && !is_var2) return unknown;

        if (is_var1 && key_match(vptr1, "enter")) return true;
        if (is_var2 && key_match(vptr2, "enter")) return false;

        if (is_var1 && key_match_mode(vptr1, "idle")) return true;
        if (is_var2 && key_match_mode(vptr2, "idle")) return false;

        if (is_var1 && key_match_next_seg(vptr1)) return true;
        if (is_var2 && key_match_next_seg(vptr2)) return false;

        return unknown;
    }
}
