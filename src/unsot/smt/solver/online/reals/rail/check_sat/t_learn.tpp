#pragma once

#include "smt/solver/online/reals/rail/check_sat/t_learn.hpp"

namespace unsot::smt::solver::online::reals::rail {
    template <typename B>
    template <typename B2>
    void Mixin<B>::Check_sat::Rail_mixin<B2>::Analyze_t_inconsistency::perform_body(var::Ptr& vptr)
    {
        Inherit::perform_body(vptr);

        //+ auto& ch = this->check_sat_ref();
        //+ if (ch.is_timing(vptr)) learn_from_timing(vptr);
    }
/*
    template <typename B>
    template <typename B2>
    void Mixin<B>::Check_sat::Rail_mixin<B2>::Analyze_t_inconsistency::learn_from_timing(const var::Ptr&)
    { }
*/
}
