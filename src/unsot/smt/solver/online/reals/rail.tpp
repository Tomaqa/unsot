#pragma once

#include "smt/solver/online/reals/rail.hpp"

namespace unsot::smt::solver::online::reals::rail {
    template <typename B>
    Mixin<B>::Mixin()
    {
        static_init();
    }

    template <typename B>
    void Mixin<B>::static_init()
    {
        set_check_sat_strategy_tp();
    }

    template <typename B>
    void Mixin<B>::set_check_sat_strategy_impl(const String& name, bool force)
    try {
        Inherit::set_check_sat_strategy_impl(name, force);
    }
    catch (Dummy) {
        using Rail = typename Check_sat::Rail;

        if (name == Rail::svariant_name()) return set_check_sat_strategy_tp(force);

        throw;
    }
}

#include "smt/solver/online/reals/rail/check_sat.tpp"
