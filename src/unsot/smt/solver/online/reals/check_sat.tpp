#pragma once

#include "smt/solver/online/reals/check_sat.hpp"

#include "util/alg.hpp"
#include "smt/solver/reals/alg.hpp"

#include <iostream>
#include <sstream>

namespace unsot::smt::solver::online::reals {
    template <typename B>
    void Mixin<B>::Check_sat::perform_init_impl()
    {
        Inherit::perform_init_impl();

        auto& s = this->solver();
        real_preds_views_map() = make_depend_pred_to_preds_views_map<Real_preds_view>(s);
        auto& arg_map = arg_real_to_preds_views_map();
        arg_map = make_arg_real_to_preds_views_map<Real_to_preds_view>(s);
        auto& ass_map = real_to_assigners_views_map();
        ass_map = make_real_to_assigners_views_map<Real_to_preds_view>(s, arg_map);
    }

    template <typename B>
    void Mixin<B>::Check_sat::perform_finish_profiled()
    {
        Inherit::perform_finish_profiled();
        this->t_inconsistent_count() = this->canalyze_t_inconsistency_ref().cprogressed_count();
    }

    template <typename B>
    void Mixin<B>::Check_sat::post_compute_real_pred([[maybe_unused]] var::Ptr& vptr, const var::Ptr& /*from*/)
    {
        assert(vptr->surely_consistent().valid());
    }

    template <typename B>
    typename Mixin<B>::Sat_solver::Clause
    Mixin<B>::Check_sat::make_depend_real_preds_clause_of(const var::Id& vid, var::Distance max_len)
    {
        //+ it seems that "proper" sorting of literals can make a difference in confl. reasoning
        auto clause = Inherit::make_depend_real_preds_clause_of(vid, max_len);

        /// Do not include T-propagated literals, since they are just consequences
        /// ... but they are actually never present there, because
        /// they are either not in `depend_ids`, or they are unit-prop. instead of T-prop.
        /// (inferial predicates are *not* T-propagated)
        if constexpr (_debug_) {
            for (auto& lit : clause) {
                auto& lvid = this->csolver().cbool_id(lit.var_id());
                if (vid == lvid) continue;
                auto& lptr = this->csolver().cbools().cptr(lvid);
                assert(!this->t_propagate_ref().performed_with(lptr));
            }
        }

        return clause;
    }

    template <typename B>
    void Mixin<B>::Check_sat::t_backtrack_bool_impl(var::Ptr& vptr)
    {
        const bool is_var = vptr->is_var();
        /// Backtracking is unsetting, which is a special case of resetting
        if (!is_var) {
            reset_assigned_of_real_pred(vptr);
            pre_reset_real_pred(vptr);
        }
        Inherit::t_backtrack_bool_impl(vptr);
        if (!is_var) post_reset_real_pred(vptr);
    }

    template <typename B>
    void Mixin<B>::Check_sat::reset_real_pred(var::Ptr& vptr)
    {
        assert(Pred::is_me(vptr));
        pre_reset_real_pred(vptr);
        reset_real_pred_body(vptr);
        post_reset_real_pred(vptr);
    }

    template <typename B>
    void Mixin<B>::Check_sat::pre_reset_real_pred([[maybe_unused]] var::Ptr& vptr)
    {
        assert(this->tracked(vptr));
    }

    template <typename B>
    void Mixin<B>::Check_sat::reset_real_pred_body(var::Ptr& vptr)
    {
        assert(this->tracked(vptr));
        vptr->reset();
    }

    template <typename B>
    void Mixin<B>::Check_sat::post_reset_real_pred(var::Ptr& /*vptr*/)
    { }

    template <typename B>
    void Mixin<B>::Check_sat::reset_real_pred_with_assigned(var::Ptr& vptr)
    {
        reset_assigned_of_real_pred(vptr);
        reset_real_pred(vptr);
    }

    template <typename B>
    void Mixin<B>::Check_sat::reset_assigned_of_real_pred(var::Ptr& vptr)
    {
        assert(this->tracked(vptr));
        assert(Pred::is_me(vptr));
        auto& t_prop = this->ct_propagate_ref();
        if (!t_prop.has_inferenced(vptr)) return;
        auto& id_ = vptr->cid();
        auto& ass = Preds::Types::Assigner::cast(vptr);

        assert(!empty(ass.cassigned_ids()));
        for (auto& aid : ass.cassigned_ids()) {
            auto& vw = arg_real_to_preds_view(aid);
            for (auto& pid : vw.caccessors()) {
                if (pid == id_) continue;
                auto& ptr_ = vw.access(pid);
                if (!t_prop.maybe_performed_with(ptr_)) continue;
                SMT_CVERB4LN(vptr->ckey() << ": reset " << ptr_->ckey());
                reset_real_pred_with_assigned(ptr_);
            }
        }
    }
}

#include "smt/solver/online/reals/check_sat/init_map.tpp"
#include "smt/solver/online/reals/check_sat/t_prop.tpp"
#include "smt/solver/online/reals/check_sat/t_sugg.tpp"
#include "smt/solver/online/reals/check_sat/t_learn.tpp"
