#pragma once

#include "smt/solver/online/reals/check_sat/t_sugg.hpp"

namespace unsot::smt::solver::online::reals {}

#include "smt/solver/online/reals/check_sat/t_sugg/bmc.tpp"
