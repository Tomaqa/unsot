#pragma once

#include "smt/solver/online/reals/check_sat/t_prop.hpp"

#include <iostream>

namespace unsot::smt::solver::online::reals {
    template <typename B>
    bool Mixin<B>::Check_sat::T_propagate::inferial(const var::Ptr& vptr) const noexcept
    {
        using Ass = Preds::Types::Assigner;
        return Ass::is_me(vptr) && inferial(Ass::cast(vptr));
    }

    template <typename B>
    bool Mixin<B>::Check_sat::T_propagate::inferial(const Preds::Types::Assigner& ass) const noexcept
    {
        /*!! Causes assert failures in rail experiments, but not in other experiments ...
             Failed to reproduce the error in 'expr_var_test' so far
        assert(ass.computable() || !ass.computable());
        */
        return ass.any_assignable();
    }

    template <typename B>
    bool Mixin<B>::Check_sat::T_propagate::has_inferenced(const var::Ptr& vptr) const noexcept
    {
        using Ass = Preds::Types::Assigner;
        if (!Ass::is_me(vptr)) return false;
        /// it is *not* true that has_inferenced => performed_with
        return has_inferenced(Ass::cast(vptr));
    }

    template <typename B>
    bool Mixin<B>::Check_sat::T_propagate::has_inferenced(const Preds::Types::Assigner& ass) const noexcept
    {
        return ass.has_assigned();
    }

    template <typename B>
    bool Mixin<B>::Check_sat::T_propagate::empty() const noexcept
    {
        return cpending_real_preds().empty();
    }

    template <typename B>
    void Mixin<B>::Check_sat::T_propagate::post_check_sat_notice(var::Ptr& vptr)
    {
        Inherit::post_check_sat_notice(vptr);

        assert(!this->maybe_performed_with(vptr));
        assert(!has_inferenced(vptr));

        if (!vptr->is_var()) notice_real_pred(vptr);
    }

    template <typename B>
    void Mixin<B>::Check_sat::T_propagate::notice_real_pred(var::Ptr& vptr)
    {
        SMT_CVERB4LN(Inherit::name() << "::notice_real_pred: " << vptr);

        assert(Pred::is_me(vptr));
        assert(this->csolver().creal_preds().local_contains(vptr->cid()));
        assert(this->ccheck_sat_ref().tracked(vptr));
        assert(!this->maybe_performed_with(vptr));
        assert(!has_inferenced(vptr));

        notice_real_pred_impl(vptr);
    }

    template <typename B>
    void Mixin<B>::Check_sat::T_propagate::notice_real_pred_impl(var::Ptr& vptr)
    {
        auto& vid = vptr->cid();
        if constexpr (postpone_already_pending_v) pending_real_preds().insert(vid);
        else pending_real_preds().push_back(vid);
    }

    template <typename B>
    void Mixin<B>::Check_sat::T_propagate::perform_init()
    {
        Inherit::perform_init();

        this->check_sat_ref().analyze_t_inconsistency_ref().restart();
    }

    template <typename B>
    typename Mixin<B>::Check_sat::T_propagate::Strategy_conf::Ret
    Mixin<B>::Check_sat::T_propagate::perform_body()
    {
        auto& ch = this->check_sat_ref();
        auto& t_inc = ch.analyze_t_inconsistency_ref();
        auto& queue = pending_real_preds();
        SMT_CVERB4LN(Inherit::name() << ": pending: " << queue);

        [[maybe_unused]] const bool was_confl = t_inc.cprogressed();

        int n_props = 0;
        int n_confl = 0;
        static constexpr int max_confl = 1;
        static_assert(max_confl > 0);
        while (!queue.empty()) {
            auto& vptr = queue.front();
            queue.pop_front();
            assert(n_confl < max_confl);
            if (!ch.tracked(vptr)) continue;
            if constexpr (!postpone_already_pending_v) {
                if (this->maybe_performed_with(vptr)) continue;
            }

            SMT_CVERB4LN(Inherit::name() << ": t-check: " << vptr);
            assert(ch.cdecision_level_of(vptr->cid()) <= ch.decision_level());
            const Flag consistent = vptr->consistent();
            if (consistent.unknown()) {
                assert(!vptr->computable());
                continue;
            }
            ch.post_compute_real_pred(vptr);

            if (consistent.is_false()) {
                t_inc.perform(vptr);
                if (++n_confl == max_confl) return Sat::unsat;
                continue;
            }

            auto opt_ret = try_perform_with(vptr);
            if (!opt_ret) continue;
            auto [nc, np] = move(opt_ret).value();
            n_confl += nc;
            assert(nc == 0 || t_inc.cprogressed());
            assert(nc > 0 || was_confl || !t_inc.cprogressed());
            if (n_confl >= max_confl) return Sat::unsat;
            n_props += np;
        }

        assert(n_confl == 0 || t_inc.cprogressed());
        assert(n_confl > 0 || was_confl || !t_inc.cprogressed());
        if (n_confl > 0) return Sat::unsat;

        assert(n_props == 0 || this->cprogressed());
        return (n_props > 0) ? Sat::unknown : Sat::sat;
    }

    template <typename B>
    Optional<typename Mixin<B>::Check_sat::T_propagate::Perform_with_result>
    Mixin<B>::Check_sat::T_propagate::try_perform_with(const var::Ptr& vptr)
    {
        assert(this->maybe_performed_with(vptr));
        /// T-propagate only after applying an inference rule
        if (has_inferenced(vptr)) return perform_with_inferenced(vptr);
        return try_perform_with_not_inferenced(vptr);
    }

    template <typename B>
    Optional<typename Mixin<B>::Check_sat::T_propagate::Perform_with_result>
    Mixin<B>::Check_sat::T_propagate::try_perform_with_not_inferenced([[maybe_unused]] const var::Ptr& vptr)
    {
        assert(!has_inferenced(vptr));
        return {};
    }

    template <typename B>
    typename Mixin<B>::Check_sat::T_propagate::Perform_with_result
    Mixin<B>::Check_sat::T_propagate::perform_with_inferenced(const var::Ptr& vptr)
    {
        assert(vptr->surely_consistent());
        assert(has_inferenced(vptr));

        auto& ch = this->check_sat_ref();
        auto& t_inc = ch.analyze_t_inconsistency_ref();
        this->progressed() = true;
        bool was_confl = t_inc.cprogressed();

        short n_props = 0;
        short n_confl = 0;
        short n_after_confl = 0;
        static constexpr short max_confl = 1;
        static constexpr short max_after_confl = 32;
        static_assert(max_confl > 0);
        static_assert(max_after_confl > 0);
        //+ Traversing `assigned_ids`-->`arg_real_to_preds_view` would require an extra effort
        //+ in the case of flows, otherwise propagations would be missed (-> 'unknown')
        //+ .. it does not seem to improve anything in 'smt2_test' anyway
        for (auto& ptr_ : ch.real_preds_view(vptr->cid())) {
            assert(n_confl < max_confl);
            assert(n_after_confl < max_after_confl);
            auto opt_ret = try_perform_from_inferenced_with(ptr_, vptr);
            if (!opt_ret) continue;
            auto [nc, np] = invoke([CAPTURE_RVAL(opt_ret)]{
                return opt_ret.valid() ? move(opt_ret).value() : Perform_with_result{};
            });
            n_props += np;
            if (nc == 0) {
                if (was_confl && ++n_after_confl == max_after_confl) break;
                continue;
            }
            assert(t_inc.cprogressed());
            n_confl += nc;
            if (n_confl >= max_confl) break;
            if (++n_after_confl == max_after_confl) break;
            was_confl = true;
        }

        return {n_confl, n_props};
    }

    template <typename B>
    Optional<typename Mixin<B>::Check_sat::T_propagate::Perform_with_result>
    Mixin<B>::Check_sat::T_propagate::try_perform_from_inferenced_with(var::Ptr& vptr, const var::Ptr& from)
    {
        assert(has_inferenced(from));
        assert(vptr->cid() != from->cid());
        assert(Pred::is_me(vptr) && Pred::is_me(from));
        if (this->maybe_performed_with(vptr)) return Perform_with_result{};
        assert(!has_inferenced(vptr));

        SMT_CVERB4LN(Inherit::name() << "::try_perform_from_inferenced_with: " << vptr << " <- " << from);
        auto& ch = this->check_sat_ref();

        if (!ch.tracked(vptr)) {
            if (inferial(vptr)) return try_perform_from_inferenced_with_inferial(vptr, from);
            if (!Pred::cast(vptr).try_compute()) return {};
            assert(vptr->surely_consistent());
            this->perform_with(vptr, from);
            Perform_with_result ret{0, 1};
            if (auto opt_ret = try_perform_with_not_inferenced(vptr))
                ret += move(opt_ret).value();
            return ret;
        }

        const Flag consistent = vptr->consistent();
        if (consistent.unknown()) return {};
        ch.post_compute_real_pred(vptr, from);

        if (consistent) {
            if constexpr (postpone_already_pending_v) {
                if (cpending_real_preds().contains(vptr->cid())) return Perform_with_result{};
                /// Do not postpone already skipped preds
            }
            return try_perform_with(vptr);
        }

        ch.analyze_t_inconsistency_ref().perform(vptr);
        return Perform_with_result{1, 0};
    }

    template <typename B>
    Optional<typename Mixin<B>::Check_sat::T_propagate::Perform_with_result>
    Mixin<B>::Check_sat::T_propagate::try_perform_from_inferenced_with_inferial(var::Ptr&, const var::Ptr& /*from*/)
    {
        /// Skip inferencing further values
        //+ remember as a form of suggestion, if no init. suggestable?
        return {};
    }

    template <typename B>
    void Mixin<B>::Check_sat::T_propagate::perform_with_impl(var::Ptr& vptr, const var::Ptr& from)
    {
        Inherit::perform_with_impl(vptr, from);

        auto& ch = this->check_sat_ref();
        ch.post_compute_real_pred(vptr, from);
    }

    template <typename B>
    bool Mixin<B>::Check_sat::T_propagate::should_learn_from(const var::Ptr& vptr,
                                                             const var::Ptr& from) const
    {
        //+ does not seem to be helpful
        //+ return is_initial_real_assigner(vptr->cid());
        return Inherit::should_learn_from(vptr, from);
    }

    template <typename B>
    void Mixin<B>::Check_sat::T_propagate::restart()
    {
        Inherit::restart();
        pending_real_preds().clear();
    }
}
