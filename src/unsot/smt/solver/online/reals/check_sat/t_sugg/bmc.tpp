#pragma once

#include "smt/solver/online/reals/check_sat/t_sugg/bmc.hpp"

#include "util/alg.hpp"
#include "util/numeric/alg.hpp"
#include "smt/solver/reals/alg.hpp"

#include <iostream>

namespace unsot::smt::solver::online::reals {
    template <typename B>
    template <typename B2>
    void Mixin<B>::Check_sat::T_suggest_decision::Bmc_mixin<B2>::post_check_sat_init()
    {
        Inherit::post_check_sat_init();

        init_var_info_map();
        this->enabled() = (cmax_bmc_step() > 0);
        if (!this->cenabled()) return;

        set_bool_views();
        if (SMT_VERBOSITY3) {
            _SMT_CVERBLN("Sorted booleans to be suggested:");
            for (auto& vw : cbool_views()) _SMT_CVERBLN(vw);
            _SMT_CVERB(endl);
        }

        this->state_stack().reserve(1+this->csolver().cbools().size()/2);
    }

    template <typename B>
    template <typename B2>
    void Mixin<B>::Check_sat::T_suggest_decision::Bmc_mixin<B2>::init_var_info_map()
    {
        auto& bools = this->csolver().cbools();
        auto& map = var_info_map();
        map.reserve(bools.size()*1.1);
        Idx max_n = invalid_idx;

        for (auto& vptr : bools) {
            auto [it, inserted] = map.emplace(vptr->cid(), compute_var_info(vptr));
            assert(inserted);
            auto& info = it->second;
            max_n = max(max_n, info.bmc_step);
        }

        assert(max_n != invalid_idx);
        max_bmc_step() = max_n;
    }

    template <typename B>
    template <typename B2>
    typename Mixin<B>::Check_sat::T_suggest_decision::template Bmc_mixin<B2>::Var_info
    Mixin<B>::Check_sat::T_suggest_decision::Bmc_mixin<B2>::compute_var_info(const var::Ptr& vptr)
    {
        return {compute_bmc_step_of(vptr)};
    }

    template <typename B>
    template <typename B2>
    Idx Mixin<B>::Check_sat::T_suggest_decision::Bmc_mixin<B2>::compute_bmc_step_of(const var::Ptr& vptr)
    {
        if (vptr->is_var()) return this->csolver().compute_bmc_step_of(vptr->ckey());
        //! it can be also `Fun` since also reals get here
        auto& pred = Pred::cast(vptr);
        return numeric::max(pred.carg_ids(), [this, &pred](auto& aid){
            return compute_bmc_step_of(pred.carg_ptr(aid));
        });
    }

    template <typename B>
    template <typename B2>
    void Mixin<B>::Check_sat::T_suggest_decision::Bmc_mixin<B2>::set_bool_views()
    {
        auto& bools = this->solver().bools();
        const Idx size_ = cmax_bmc_step()+1;
        auto& views = bool_views();
        views.reserve(size_);
        for (Idx i = 0; i < size_; ++i) {
            views.emplace_back(bools);
        }

        for (const auto& vptr : bools) {
            if (vptr->is_set()) continue;
            auto& vid = vptr->cid();
            const Idx n = cvar_info(vid).bmc_step;
            if (n == invalid_idx) continue;
            views[n].push_back(vid);
        }

        sort_bool_views();
    }

    template <typename B>
    template <typename B2>
    void Mixin<B>::Check_sat::T_suggest_decision::Bmc_mixin<B2>::sort_bool_views()
    {
        for (auto& vw : bool_views()) sort_bool_view(vw);
    }

    template <typename B>
    template <typename B2>
    void Mixin<B>::Check_sat::T_suggest_decision::Bmc_mixin<B2>::sort_bool_view(Bool_view& vw)
    {
        //!! `sort` can cause some invalid memory operations, I dont know why (view iterators?)
        stable_sort(vw.accessors(), [this](const var::Id& vid1, const var::Id& vid2){
            return sort_bool_view_compare(vid1, vid2).is_true();
        });
    }

    template <typename B>
    template <typename B2>
    Flag Mixin<B>::Check_sat::T_suggest_decision::Bmc_mixin<B2>::sort_bool_view_compare(const var::Id& vid1, const var::Id& vid2) const
    {
        if (auto flag = sort_bool_view_compare_prior(vid1, vid2); flag.valid()) return flag;

        auto& bools = this->csolver().cbools();
        return sort_bool_view_compare_rest(bools.cptr(vid1), bools.cptr(vid2));
    }

    template <typename B>
    template <typename B2>
    Flag Mixin<B>::Check_sat::T_suggest_decision::Bmc_mixin<B2>::sort_bool_view_compare_prior(const var::Id& vid1, const var::Id& vid2) const
    {
        if (vid1 == vid2) {
            assert(&vid1 == &vid2);
            return false;
        }

        return unknown;
    }

    template <typename B>
    template <typename B2>
    Flag Mixin<B>::Check_sat::T_suggest_decision::Bmc_mixin<B2>::sort_bool_view_compare_rest(const var::Ptr&, const var::Ptr&) const
    {
        return unknown;
    }

    template <typename B>
    template <typename B2>
    Optional<typename Mixin<B>::Check_sat::T_suggest_decision::template Bmc_mixin<B2>::Ret>
    Mixin<B>::Check_sat::T_suggest_decision::Bmc_mixin<B2>::perform_body()
    {
        const Idx max_step = cmax_bmc_step();
        Idx bmc_step = 0;
        Idx vidx = -1;
        if (!this->invalid_current_state()) {
            auto& state = this->ccurrent_state();
            bmc_step = state.bmc_step;
            vidx = state.var_idx;
            assert(vidx >= 0);
        }
        assert(bmc_step >= 0);
        assert(bmc_step <= max_step);

        ++vidx;
        assert(size_t(vidx) <= cbool_view(bmc_step).size());

        for (; bmc_step <= max_step; ++bmc_step, vidx = 0) {
            auto& vw = cbool_view(bmc_step);
            const Idx vw_size = vw.size();
            for (; vidx < vw_size; ++vidx) {
                auto& vptr = vw[vidx];
                if (vptr->is_set()) continue;

                this->add_state({bmc_step, vidx, vptr->cid()});
                return this->perform_with(vptr);
            }
        }

        /// There can be some variables that are not BMC-unrolled
        /// + the last decision can be dummy and do nothing
        return Inherit::perform_body();
    }

    template <typename B>
    template <typename B2>
    Flag Mixin<B>::Check_sat::T_suggest_decision::Bmc_mixin<B2>::perform_value_with(const var::Ptr& vptr) const
    {
        auto& info = cvar_info(vptr->cid());
        if (auto flag = info.suggest_value; flag.valid()) return flag;

        return Inherit::perform_value_with(vptr);
    }

    template <typename B>
    template <typename B2>
    void Mixin<B>::Check_sat::T_suggest_decision::Bmc_mixin<B2>::backtrack_impl()
    {
        Inherit::backtrack_impl();
        assert(this->invalid_current_bool() || this->ccurrent_bool_ptr()->is_set());
    }

    template <typename B>
    template <typename B2>
    bool Mixin<B>::Check_sat::T_suggest_decision::Bmc_mixin<B2>::maybe_backtrack_from_bool_head(const var::Ptr& vptr) const
    {
        if (!Inherit::maybe_backtrack_from_bool_head(vptr)) return false;
        if (this->invalid_current_state()) return false;

        return true;
    }

    template <typename B>
    template <typename B2>
    bool Mixin<B>::Check_sat::T_suggest_decision::Bmc_mixin<B2>::is_ok(const State& state) const noexcept
    {
        if (!Inherit::is_ok(state)) return false;
        if (state.var_id == var::invalid_id) return false;
        return cbool_view(state.bmc_step)[state.var_idx]->is_set();
    }

    template <typename B>
    template <typename B2>
    String Mixin<B>::Check_sat::T_suggest_decision::Bmc_mixin<B2>::state_to_string(const State& state) const
    {
        return Inherit::state_to_string(state)
             + "<" + to_string(state.bmc_step) + ">[" + to_string(state.var_idx) + "]";
    }
}

#include "smt/solver/online/reals/check_sat/t_sugg/bmc/sort.tpp"
