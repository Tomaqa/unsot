#pragma once

#include "smt/solver/online/reals/check_sat/t_learn.hpp"

namespace unsot::smt::solver::online::reals {
    template <typename B>
    typename Mixin<B>::Sat_solver::Clause
    Mixin<B>::Check_sat::T_explain::explain_impl(const var::Ptr& vptr, var::Distance max_len)
    {
        auto clause = this->check_sat_ref().make_depend_real_preds_conflict_of(vptr->cid(), max_len);
        if (!empty(clause)) this->csolver().csat_solver().neg(*begin(clause));
        return clause;
    }

    ////////////////////////////////////////////////////////////////

    template <typename B>
    void Mixin<B>::Check_sat::Analyze_t_inconsistency::backtrack(var::Ptr& vptr, typename Sat_solver::Clause& confl)
    {
        Inherit::backtrack(vptr, confl);

        this->check_sat_ref().reset_real_pred(vptr);
    }

    template <typename B>
    typename Mixin<B>::Sat_solver::Clause
    Mixin<B>::Check_sat::Analyze_t_inconsistency::explain_impl(const var::Ptr& vptr, var::Distance max_len)
    {
        return this->check_sat_ref().make_depend_real_preds_conflict_of(vptr->cid(), max_len);
    }
}

#include "smt/solver/online/reals/check_sat/t_learn/car.tpp"
