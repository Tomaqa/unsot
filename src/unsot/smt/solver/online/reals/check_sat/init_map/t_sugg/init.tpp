#pragma once

#include "smt/solver/online/reals/check_sat/init_map/t_sugg/init.hpp"

#include "util/alg.hpp"
#include "util/random.hpp"
#include "smt/solver/reals/alg.hpp"

#include <iostream>

namespace unsot::smt::solver::online::reals {
    template <typename B>
    template <typename B2>
    template <typename B3>
    bool Mixin<B>::Check_sat::Init_map_mixin<B2>::T_suggest_decision::Initial_mixin<B3>::is_initial_real_assigner(const var::Id& vid) const noexcept
    {
        return cinitial_real_assigners_view().contains(vid);
    }

    template <typename B>
    template <typename B2>
    template <typename B3>
    bool Mixin<B>::Check_sat::Init_map_mixin<B2>::T_suggest_decision::Initial_mixin<B3>::invalid_current_bool() const noexcept
    {
        if (Inherit::invalid_current_bool()) return true;
        return ccurrent_bool_id() == var::invalid_id;
    }

    template <typename B>
    template <typename B2>
    template <typename B3>
    void Mixin<B>::Check_sat::Init_map_mixin<B2>::T_suggest_decision::Initial_mixin<B3>::rm_current_state() noexcept
    {
        const bool dbg = _debug_ && this->valid_current_bool();
        if (dbg) {
        /*++ if non-initial assigners are included:
        if constexpr (_debug_) {
            assert(ass_id != var::invalid_id);
        */
            auto& ass = Preds::Types::Assigner::cast(this->ccurrent_bool_ptr());
            auto& rptr = ccurrent_initial_real_var_ptr();
            SMT_CVERB4(Inherit::name() << "::rm_current_state: " << ass << " : " << rptr << " -> ");
            assert(util::contains(ass.cassignee_ids(), rptr->cid()));
            assert(!ass.has_assigned() || rptr->cid() == ass.cassigned_id());
            assert(!ass.has_assigned() || rptr->is_set());
        }

        Inherit::rm_current_state();

        if (dbg) {
        //++ if constexpr (_debug_) {
            if (this->invalid_current_state()) {
                SMT_CVERB4LN("<none>")
            }
            else {
                SMT_CVERB4LN(ccurrent_initial_real_var_ptr());
                assert(invalid_current_bool() || ccurrent_initial_real_var_ptr()->is_set());
                //++ assert(ccurrent_state().assigner_id != var::invalid_id);
                //++ assert(ccurrent_initial_real_var_ptr()->is_set());
            }
        }
    }

    template <typename B>
    template <typename B2>
    template <typename B3>
    void Mixin<B>::Check_sat::Init_map_mixin<B2>::T_suggest_decision::Initial_mixin<B3>::post_check_sat_init()
    {
        Inherit::post_check_sat_init();

        auto& s = this->solver();
        auto& ch = this->check_sat_ref();
        auto& arg_map = ch.carg_real_to_preds_views_map();
        //++ auto& ass_map = this->creal_to_assigners_views_map();

        /// Is set in the `Init_map_mixin`
        auto& init_map = ch.initial_real_to_assigners_views_map();
        auto& init_vw = initial_reals_view();
        init_vw = make_sorted_initial_real_vars_view<Reals_view>(s, arg_map, init_map);
        this->enabled() = !init_vw.empty();
        order_initial_real_views();
        SMT_CVERB3LN("Initial vars (sorted): " << init_vw);

        auto& init_ass_vw = initial_real_assigners_view();
        for (auto& [rid, ass_vw] : init_map) {
            for (auto& ass_id : ass_vw.caccessors()) {
                assert(!init_ass_vw.contains(ass_id));
                init_ass_vw.push_back(ass_id);
            }
            /// Include regular assigners after the initial ones
            /*++
            for (auto& ass_id : view(ass_map, rid).caccessors()) {
                if (is_initial_real_assigner(ass_id)) continue;
                assert(!ass_vw.contains(ass_id));
                ass_vw.push_back(ass_id);
            }
            */
        }

        SMT_CVERB4LN("Mapping of init. vars to init. assigners:" << endl << init_map);
        SMT_CVERB3(endl);
    }

    template <typename B>
    template <typename B2>
    template <typename B3>
    void Mixin<B>::Check_sat::Init_map_mixin<B2>::T_suggest_decision::Initial_mixin<B3>::order_initial_real_views()
    {
        /*+ ideal for 'xor' experiment, but generally worse
        reverse(initial_reals_view().accessors());
        */

        /*+ scatter instead of shuffle - try to cover all parts of the interval soon
            (sth. like [1, 2, 3, 4, 5, 6, 7, 8] -> [1, 8, 3, 6, 2, 7, 4, 5])
        */
        auto& ch = this->check_sat_ref();
        for (auto& [rid, ass_vw] : ch.initial_real_to_assigners_views_map()) {
            shuffle(ass_vw.accessors());
        }
    }

    template <typename B>
    template <typename B2>
    template <typename B3>
    Optional<typename Mixin<B>::Check_sat::template Init_map_mixin<B2>::T_suggest_decision::template Initial_mixin<B3>::Ret>
    Mixin<B>::Check_sat::Init_map_mixin<B2>::T_suggest_decision::Initial_mixin<B3>::perform_body()
    {
        auto& init_vw = cinitial_reals_view();
        const int size_ = init_vw.size();
        assert(size_ > 0);

        /*+ "thresholding" or "avoiding early decisions from starving"
            I failed to find dependable parameters that would work
            satisfactorily for most experiments
            these ones work quite well for 'car'
        static constexpr long min_counter_limit = (1UL << 12);
        static constexpr long max_counter_limit = (1UL << 29);
        static_assert(min_counter_limit < max_counter_limit);
        assert(min_counter_limit > 2L*size_);
        long lim = max_counter_limit;
        //+ top-down; what about bottom-up ?
        const auto limits = to<Vector<long>>(init_vw, [this, &lim](auto& vptr){
            auto l = lim;
            lim /= max(2UL, this->cinitial_real_to_assigners_view(vptr->cid()).size()/2);
            return l;
        });
        auto& preds = this->csolver().creal_preds();
        for (auto& state : this->state_stack()) {
            const long limit = limits[state.init_real_var_idx];
            if (limit < min_counter_limit) break;
            assert(state.counter+1 <= limit);
            if (++state.counter < limit) continue;
            SMT_CVERB4LN("t_suggest_decision - forced backtrack");
            auto& aptr = preds.cptr(state.assigner_id);
            assert(aptr->is_set() && !Pred::cast(aptr).falsified());
            assert(this->decision_level_of(state.assigner_id) <= this->decision_level());
            this->t_backtrack_to(this->decision_level_of(state.assigner_id)-1);
            break;
        }
        */

        Idx idx;
        Idx ass_idx = 0;
        if (auto& next = next_state()) {
            idx = next->init_real_var_idx;
            if (init_vw[idx]->is_unset()) {
                ass_idx = next->assigner_idx;
                if constexpr (_debug_) {
                    assert(ass_idx > 0);
                    auto& ch = this->ccheck_sat_ref();
                    auto& ass_vw = ch.cinitial_real_to_assigners_view(next->init_real_var_id);
                    const int ass_cnt = ass_vw.size();
                    assert(ass_idx <= ass_cnt);
                    /*++ if non-initial assigners are included:
                    assert(ass_idx < int(this->initial_real_to_assigners_view(next->init_real_var_id).size()));
                    // include a sort of skipping though, it seems to be beneficial
                    // (but then, instead of return {}, try suggesting non-init. ass.?)
                    */
                }
                this->add_state(move(next.value()));
            }
            else {
                //+ it used to give slightly better results
                //+ if `current` idx is used (and incr.) (is it still true?)
                ++idx;
            }
            next.invalidate();
        }
        else if (this->invalid_current_state()) {
            idx = 0;
        }
        else {
            auto& state = this->ccurrent_state();
            assert(state.init_real_var_idx != invalid_idx);
            idx = state.init_real_var_idx+1;
        }
        assert(idx != invalid_idx);
        assert(ass_idx != invalid_idx);
        assert(idx <= size_);

        for (; idx < size_; ++idx, ass_idx = 0) {
            assert(cnext_state().invalid());
            if (init_vw[idx]->is_set()) continue;

            auto& vid = init_vw.caccessor(idx);
            if (auto opt_ret = try_perform_with_initial_real_var(vid, idx, ass_idx))
                return opt_ret;
        }

        assert(idx == size_);
        //+ if really tried all combinations, finish
        return Inherit::perform_body();
    }

    template <typename B>
    template <typename B2>
    template <typename B3>
    Optional<typename Mixin<B>::Check_sat::template Init_map_mixin<B2>::T_suggest_decision::template Initial_mixin<B3>::Ret>
    Mixin<B>::Check_sat::Init_map_mixin<B2>::T_suggest_decision::Initial_mixin<B3>::try_perform_with_initial_real_var(const var::Id& vid, const Idx idx, Idx ass_idx)
    {
        auto& ch = this->ccheck_sat_ref();

        assert(ch.is_initial_real_var(vid));
        assert(this->csolver().creals().cptr(vid)->is_unset());

        auto state_l = this->invalid_current_state() ? nullptr
                     : &this->current_state();
        const bool set_new_state = !state_l || state_l->init_real_var_idx != idx;
        assert(set_new_state || state_l->init_real_var_id == vid);

        auto& ass_vw = ch.cinitial_real_to_assigners_view(vid);
        const int ass_size = ass_vw.size();
        assert(ass_idx != invalid_idx);
        assert(ass_idx <= ass_size);
        //++ assert(ass_idx < ass_size);
        for (; ass_idx < ass_size; ++ass_idx) {
            auto& ass_ptr = ass_vw[ass_idx];
            auto& aid = ass_ptr->cid();
            if (ch.tracked(ass_ptr)) {
                assert(Pred::cast(ass_ptr).falsified());
                /*++ if non-initial assigners are included:
                assert(!is_initial_real_assigner(aid)
                       || Pred::cast(ass_ptr).falsified());
                if (!Pred::cast(ass_ptr).falsified()) break;
                !is_initial_real_assigner(aid) -> sugg. only if inferial
                */
                continue;
            }

            if (set_new_state) {
                this->add_state({idx, ass_idx, vid, aid});
            }
            else {
                assert(state_l);
                state_l->assigner_idx = ass_idx;
                state_l->assigner_id = aid;
            }

            //+ add `Priority::max` if supported
            return this->perform_with(ass_ptr);
        }

        if (!set_new_state) {
            assert(cnext_state().invalid());
            rm_current_state();
        }
        //+ the other case?

        /// No suitable assigner found
        return {};
    }

    template <typename B>
    template <typename B2>
    template <typename B3>
    void Mixin<B>::Check_sat::Init_map_mixin<B2>::T_suggest_decision::Initial_mixin<B3>::post_check_sat_restart()
    {
        if (this->invalid_current_state()) return Inherit::post_check_sat_restart();

        /// Restarts from SAT solver does not seem to be beneficial,
        /// because it discards all assignments and starts all over again
        auto& stack = this->state_stack();
        stack.erase(stack.begin()+1, stack.end());
        backtrack();
        assert(this->invalid_current_state());
        assert(cnext_state().valid());
    }

    template <typename B>
    template <typename B2>
    template <typename B3>
    void Mixin<B>::Check_sat::Init_map_mixin<B2>::T_suggest_decision::Initial_mixin<B3>::backtrack()
    {
        auto& ch = this->check_sat_ref();
        for (auto state_l = &this->current_state();; --state_l) {
            assert(state_l == &this->current_state());
            auto& state = *state_l;
            auto& vid = state.init_real_var_id;
            auto& ass_vw = ch.initial_real_to_assigners_view(vid);
            const int ass_size = ass_vw.size();
            auto& ass_idx = state.assigner_idx;
            assert(ass_idx != invalid_idx);
            assert(ass_idx <= ass_size);
            /*++ if non-initial assigners are included:
            assert(ass_idx < ass_size);
            if (++ass_idx < ass_size) return Inherit::backtrack();
            */
            if (ass_idx < ass_size) return Inherit::backtrack();

            rm_current_state();
            next_state().invalidate();
            if (this->valid_current_state()) continue;

            return restart();
        }
    }

    template <typename B>
    template <typename B2>
    template <typename B3>
    void Mixin<B>::Check_sat::Init_map_mixin<B2>::T_suggest_decision::Initial_mixin<B3>::backtrack_impl()
    {
        auto& ch = this->check_sat_ref();
        auto& state = this->current_state();
        auto& vid = state.init_real_var_id;
        auto& ass_vw = ch.initial_real_to_assigners_view(vid);
        const int ass_size = ass_vw.size();
        auto& ass_idx = ++state.assigner_idx;

        state.assigner_id = (ass_idx < ass_size) ? ass_vw[ass_idx]->cid() : var::invalid_id;
        /*++
        SMT_CVERB4LN("moving suggestion of initial assigner: " << ass_vw[ass_idx-1]
                     << " -> " << ass_ptr);
        state.assigner_id = ass_ptr->cid();
        + state.counter = 0;
        */

        /// Don't learn not to repeat this suggest combination
        /// - it is not true that all remaining combinations are UNSAT
        /// (SAT solver can backjump just to change some early decision)

        next_state() = move(state);
        Inherit::backtrack_impl();
    }

    template <typename B>
    template <typename B2>
    template <typename B3>
    void Mixin<B>::Check_sat::Init_map_mixin<B2>::T_suggest_decision::Initial_mixin<B3>::restart()
    {
        Inherit::restart();
        next_state().invalidate();

        /// do not shuffle assigners again, it is not beneficial
        //+ what if used with "thresholding" ?
        //+ order_initial_real_views();
    }

    template <typename B>
    template <typename B2>
    template <typename B3>
    bool Mixin<B>::Check_sat::Init_map_mixin<B2>::T_suggest_decision::Initial_mixin<B3>::maybe_backtrack_from_bool_head(const var::Ptr& vptr) const
    {
        if (!Inherit::maybe_backtrack_from_bool_head(vptr)) return false;
        //++ if (invalid_current_state()) return false;
        if (vptr->is_var() || Pred::cast(vptr).falsified()) return false;

        return true;
    }

    template <typename B>
    template <typename B2>
    template <typename B3>
    bool Mixin<B>::Check_sat::Init_map_mixin<B2>::T_suggest_decision::Initial_mixin<B3>::maybe_backtrack_from_bool_body(const var::Ptr& vptr)
    {
        /// Skip all not-being-suggested variables
        //+ cache the assigner somehow, this skipping can be calling often
        auto& vid = vptr->cid();
        auto& stack = this->state_stack();
        const auto bit = begin(stack);
        const auto eit = end(stack);
        for (auto it = eit; it != bit;) {
            auto& state = *--it;
            assert(state.init_real_var_idx != invalid_idx);
            assert(state.init_real_var_idx < cinitial_reals_view().size());
            if (state.assigner_id == var::invalid_id) continue;
            assert(this->csolver().creal_preds().local_contains(state.assigner_id));
            if (vid != state.assigner_id) return false;

            stack.erase(it+1, eit);
            return true;
        }

        restart();
        /*++ if non-initial assigners are included:
        assert(ccurrent_state().assigner_id != var::invalid_id);
        if (bool_ptr->cid() == ccurrent_state().assigner_id)
            backtrack();
        */
        return false;
    }

    template <typename B>
    template <typename B2>
    template <typename B3>
    bool Mixin<B>::Check_sat::Init_map_mixin<B2>::T_suggest_decision::Initial_mixin<B3>::is_ok(const State& state) const noexcept
    {
        if (!Inherit::is_ok(state)) return false;
        if (state.assigner_id == var::invalid_id) return true;
        //++ if (state.assigner_id == var::invalid_id) return false;
        return cinitial_reals_view()[state.init_real_var_idx]->is_set();
    }

    template <typename B>
    template <typename B2>
    template <typename B3>
    String Mixin<B>::Check_sat::Init_map_mixin<B2>::T_suggest_decision::Initial_mixin<B3>::state_to_string(const State& state) const
    {
        return Inherit::state_to_string(state)
             + "[" + to_string(state.init_real_var_idx) + "]";
    }

    template <typename B>
    template <typename B2>
    template <typename B3>
    String Mixin<B>::Check_sat::Init_map_mixin<B2>::T_suggest_decision::Initial_mixin<B3>::current_state_to_string_extra() const
    {
        String str = Inherit::current_state_to_string_extra();
        if (auto& next = cnext_state()) {
            str += "{" + to_string(next->init_real_var_idx) + "}";
        }
        str += " - initial reals: " + cinitial_reals_view().to_string();

        return str;
    }
}
