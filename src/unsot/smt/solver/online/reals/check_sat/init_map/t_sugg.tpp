#pragma once

#include "smt/solver/online/reals/check_sat/init_map/t_sugg.hpp"

namespace unsot::smt::solver::online::reals {
    template <typename B>
    template <typename B2>
    template <typename B3>
    Flag Mixin<B>::Check_sat::Init_map_mixin<B2>::T_suggest_decision::Bmc_sort_mixin<B3>::
        sort_bool_view_compare_initial(const Preds::Types::Assigner& ass1, const Preds::Types::Assigner& ass2) const
    {
        if (auto flag = Inherit::sort_bool_view_compare_initial(ass1, ass2); flag.valid())
            return flag;

        static const auto is_initial_f = [](auto& ass, auto& init_map){
            //+ pre-compute this
            auto& aids = ass.cassignee_ids();
            assert(size(aids) == 1);
            auto& aid = *cbegin(aids);
            auto init_it = init_map.find(aid);
            if (init_it == init_map.end()) return false;
            auto& init_assigners_vw = init_it->second;
            return init_assigners_vw.contains(ass.cid());
        };

        auto& init_map = this->ccheck_sat_ref().cinitial_real_to_assigners_views_map();

        if (!is_initial_f(ass1, init_map)) return false;
        if (!is_initial_f(ass2, init_map)) return true;

        return unknown;
    }
}

#include "smt/solver/online/reals/check_sat/init_map/t_sugg/init.tpp"
