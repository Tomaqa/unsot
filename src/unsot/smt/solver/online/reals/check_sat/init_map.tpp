#pragma once

#include "smt/solver/online/reals/check_sat/init_map.hpp"

#include "smt/solver/reals/alg.hpp"

namespace unsot::smt::solver::online::reals {
    template <typename B>
    template <typename B2>
    bool Mixin<B>::Check_sat::Init_map_mixin<B2>::is_initial_real_var(const var::Id& vid) const noexcept
    {
        return cinitial_real_to_assigners_views_map().contains(vid);
    }

    template <typename B>
    template <typename B2>
    void Mixin<B>::Check_sat::Init_map_mixin<B2>::perform_init_impl()
    {
        Inherit::perform_init_impl();

        auto& s = this->solver();
        auto& arg_map = this->carg_real_to_preds_views_map();

        //+ ideally, some initial T-propagation would be beneficial before this
        //+ (it must consider the possibility of a conflict -> UNSAT)
        auto& init_map = initial_real_to_assigners_views_map();
        init_map = make_initial_real_to_assigners_views_map<Real_to_preds_view>(s, arg_map);
    }
}

#include "smt/solver/online/reals/check_sat/init_map/t_sugg.tpp"
