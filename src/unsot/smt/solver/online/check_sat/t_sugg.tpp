#pragma once

#include "smt/solver/online/check_sat/t_sugg.hpp"

namespace unsot::smt::solver::online {
    template <typename B>
    Optional<typename Mixin<B>::Check_sat::T_suggest_decision::Ret>
    Mixin<B>::Check_sat::T_suggest_decision::perform()
    {
        if (!enabled()) return {};

        return Inherit::perform();
    }

    template <typename B>
    void Mixin<B>::Check_sat::T_suggest_decision::perform_init()
    {
        Inherit::perform_init();

        assert(cenabled());
    }

    template <typename B>
    Optional<typename Mixin<B>::Check_sat::T_suggest_decision::Ret>
    Mixin<B>::Check_sat::T_suggest_decision::perform_body()
    {
        return this->check_sat_ref().Online_check_sat_base::t_suggest_decision();
    }

    template <typename B>
    void Mixin<B>::Check_sat::T_suggest_decision::perform_finish()
    {
        this->progressed() = this->cret().valid();

        Inherit::perform_finish();
    }

    template <typename B>
    void Mixin<B>::Check_sat::T_suggest_decision::post_check_sat_restart()
    {
        Inherit::post_check_sat_restart();
        assert(invalid_current_state());
    }

    template <typename B>
    void Mixin<B>::Check_sat::T_suggest_decision::backtrack()
    {
        assert(valid_current_state());
    }

    template <typename B>
    void Mixin<B>::Check_sat::T_suggest_decision::finish()
    {
        SMT_CVERB3LN(Inherit::name() << "::finished");
        enabled() = false;
    }
}

////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////

namespace unsot::smt::solver::online {
    template <typename B>
    template <typename B2, typename StateT>
    void Mixin<B>::Check_sat::T_suggest_decision::Stack_mixin<B2, StateT>::perform_init()
    {
        Inherit::perform_init();

        SMT_CVERB4LN(Inherit::name() << current_state_to_string() << " ...");

        assert(all_of(cstate_stack(), [this](auto& state){
            return is_ok(state);
        }));
    }

    template <typename B>
    template <typename B2, typename StateT>
    void Mixin<B>::Check_sat::T_suggest_decision::Stack_mixin<B2, StateT>::backtrack()
    {
        Inherit::backtrack();
        rm_current_state();
    }

    template <typename B>
    template <typename B2, typename StateT>
    void Mixin<B>::Check_sat::T_suggest_decision::Stack_mixin<B2, StateT>::restart()
    {
        Inherit::restart();
        state_stack().clear();
    }

    template <typename B>
    template <typename B2, typename StateT>
    String Mixin<B>::Check_sat::T_suggest_decision::Stack_mixin<B2, StateT>::current_state_to_string() const
    {
        String str;
        if (this->valid_current_state()) str += state_to_string(ccurrent_state());
        str += current_state_to_string_extra();

        return str;
    }
}
