#pragma once

#include "smt/solver/online/check_sat/t_prop.hpp"

namespace unsot::smt::solver::online {
    template <typename B>
    typename Mixin<B>::Check_sat::T_propagate::Strategy_conf::Ret
    Mixin<B>::Check_sat::T_propagate::perform()
    {
        /// Postpone direct propagations from assertions
        assert(this->csolver().cmode() != Mode::sat && this->csolver().cmode() != Mode::unsat);
        if (this->csolver().cmode() == Mode::assert) return true;
        if (empty()) return true;

        return Inherit::perform();
    }

    template <typename B>
    void Mixin<B>::Check_sat::T_propagate::post_check_sat_restart()
    {
        Inherit::post_check_sat_restart();
        assert(empty());
    }
}
