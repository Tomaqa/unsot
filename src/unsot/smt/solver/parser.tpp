#pragma once

#include "smt/solver/parser.hpp"

#include "expr/alg.hpp"

#include <iostream>

namespace unsot::smt::solver {
    template <typename S>
    Crtp<S>::Parser::Parser(Solver& solver_)
        : _solver(solver_)
    { }

    template <typename S>
    bool Crtp<S>::Parser::is_plain_cmd(const List& ls) noexcept
    {
        return is_as_key(ls);
    }

    template <typename S>
    void Crtp<S>::Parser::check_is_plain_cmd(const List& ls)
    {
        expect(is_plain_cmd(ls), "Unexpected additional arguments: "s + ls.to_string());
    }

    template <typename S>
    Preprocess Crtp<S>::Parser::preprocess_lines(istream& is)
    {
        return Preprocess(is);
    }

    template <typename S>
    List Crtp<S>::Parser::preprocess(istream& is)
    {
        List ls = preprocess_lines(is).perform();
        preprocess_list(ls);
        return ls;
    }

    template <typename S>
    void Crtp<S>::Parser::preprocess_list(List& ls)
    {
        auto eptr = move(ls).to_ptr();
        try { preprocess_sublist(eptr); }
        catch (Ignore) {
            THROW("The top expression was preprocessed to key: "s + eptr->to_string());
        }
        ls = List::cast(move(eptr));
    }

    template <typename S>
    void Crtp<S>::Parser::preprocess_sublist(expr::Ptr& eptr)
    {
        auto& ls = List::cast(eptr);
        for (auto& subeptr : ls) {
            if (List::is_me(subeptr)) try {
                preprocess_sublist(subeptr);
                continue;
            }
            catch (Ignore) { }
            post_preprocess_elem(subeptr);
        }

        if (!List::is_me(eptr)) return;
        post_preprocess_list(eptr);
    }

    template <typename S>
    void Crtp<S>::Parser::post_preprocess_list(expr::Ptr& eptr)
    {
        auto& ls = List::cast(eptr);
        if (ls.empty()) return;
        Pos pos(ls);
        if (!is_key_at(pos)) return;
        auto& cmd = get_key(pos);
        post_preprocess_cmd(eptr, cmd, pos);
    }

    template <typename S>
    void Crtp<S>::Parser::parse(List& ls)
    try {
        expect(is_deep(ls), "Unexpected key(s) at top level.");
        for_each(ls, BIND_THIS(parse_sublist));
    }
    catch (Ignore) { }

    template <typename S>
    void Crtp<S>::Parser::parse_sublist(expr::Ptr& eptr)
    {
        parse_expand_sublist(eptr);
        solver().parsed_list().push_back(eptr);
        List& ls = List::cast(eptr);
        Pos pos(ls);
        const auto& cmd = get_key_check(pos);
        try { parse_cmd(cmd, pos); }
        catch (Dummy) {
            THROW("Unknown command identifier: ") + to_string(cmd);
        }
    }

    template <typename S>
    void Crtp<S>::Parser::parse_expand_sublist(expr::Ptr& eptr)
    {
        List& ls = List::cast(eptr);
        if (ls.empty()) return;

        if (is_key(ls.cfront())) {
            Pos pos(ls);
            auto& cmd = get_key(pos);
            parse_expand_cmd(eptr, cmd, pos);
        }

        if (!List::is_me(eptr)) return;
        for (auto& subeptr : List::cast(eptr)) {
            if (List::is_me(subeptr)) parse_expand_sublist(subeptr);
            else parse_expand_elem(subeptr);
        }
    }

    template <typename S>
    void Crtp<S>::Parser::parse_cmd(const Key& cmd, Pos& pos)
    {
        assert(cast_key(pos.prev().peek()) == cmd
               && pos.next().clist().size() >= 1);
        if (cmd == "set-option") {
            return parse_cmd_set_option(move(pos));
        }
        if (cmd == "get-option") {
            return parse_cmd_get_option(move(pos));
        }
        if (cmd == "assert") {
            return parse_cmd_assert(move(pos));
        }
        if (cmd == "check-sat") {
            return parse_cmd_check_sat(move(pos));
        }
        if (cmd == "get-value") {
            return parse_cmd_get_value(move(pos));
        }
        if (cmd == "get-model") {
            return parse_cmd_get_model(move(pos));
        }
        if (cmd == "echo") {
            return parse_cmd_echo(move(pos));
        }
        if (cmd == "exit") {
            return parse_cmd_exit(move(pos));
        }

        throw dummy;
    }

    template <typename S>
    void Crtp<S>::Parser::parse_cmd_set_option(Pos&& pos)
    {
        auto& ls = pos.clist();
        expect(ls.size() == 3 && is_flat(ls),
               "Invalid format: expected (set_option <opt_kw> <value>), got: " + pos.to_string());

        const Option& opt = get_key_check(pos);
        Option_value val = get_elem(move(pos)).to_string();
        solver().set_option(opt, move(val));
    }

    template <typename S>
    void Crtp<S>::Parser::parse_cmd_get_option(Pos&& pos)
    {
        auto& ls = pos.clist();
        expect(ls.size() == 2 && is_flat(ls),
               "Invalid format: expected (get_option <opt_kw>), got: " + pos.to_string());

        if (csolver().cverbosity() <= -1) return;

        const Option& opt = get_key_check(pos);
        Option_value val = solver().get_option(opt);
        solver().os() << val << std::endl;
    }

    template <typename S>
    void Crtp<S>::Parser::parse_cmd_assert(Pos&& pos)
    {
        expect(pos.clist().size() == 2,
               "Invalid format: expected (assert <<list>|<elem>>), got: "s + pos.to_string());

        //+ needed to not destroy `parsed_list`
        solver().assert_expr(pos.cpeek()->deep_copy_to_ptr());
    }

    template <typename S>
    void Crtp<S>::Parser::parse_cmd_check_sat(Pos&& pos)
    {
        check_is_plain_cmd(pos.clist());
        solver().solve();
    }

    template <typename S>
    void Crtp<S>::Parser::parse_cmd_get_value(Pos&& pos)
    {
        expect(pos.clist().size() == 2 && is_flat_list_at(pos),
               "Invalid format: expected (get-value (<arg>*)), got: "s + pos.to_string());

        if (!solver().allowed_get_value()) return;

        for_each(peek_list(pos), as_ckeys_check([this](auto& k){
            solver().print_var(k);
        }));
    }

    template <typename S>
    void Crtp<S>::Parser::parse_cmd_get_model(Pos&& pos)
    {
        check_is_plain_cmd(pos.clist());
        if (!solver().allowed_get_value()) return;
        solver().print_vars();
    }

    //+ brackets inside the string literal should be treated as symbols..
    template <typename S>
    void Crtp<S>::Parser::parse_cmd_echo(Pos&& pos)
    {
        auto& ls = pos.list();
        expect(is_plain_cmd(ls) || (is_flat(ls) && peek_key_check(pos)[0] == '"'),
               "Invalid format: expected (echo <string>), got: "s + pos.to_string());

        if (csolver().cverbosity() <= -1) return;

        if (pos) {
            ls.erase(ls.cbegin());
            Key& first_key = peek_key(pos);
            if (size(first_key) == 1) pos.erase();
            else first_key.erase(0, 1);
            String str = to_string(cast_to_elems(as_const(ls)));
            str.pop_back();
            expect(str.back() == '"', "Closing '\"' not found: "s + str);
            str.pop_back();
            //+ double "\"\"" should be treated as escape seq.
            expect(!util::contains(str, '"'), "Additional '\"' characters: "s + str);
            solver().os() << str;
        }
        solver().os() << std::endl;
    }

    template <typename S>
    void Crtp<S>::Parser::parse_cmd_exit(Pos&& pos)
    {
        check_is_plain_cmd(pos.clist());
        throw ignore;
    }
}
