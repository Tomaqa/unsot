#pragma once

#include "smt/solver/bools.hpp"

#include "expr/alg.hpp"

#include <iostream>
#include <sstream>

namespace unsot::smt::solver::bools {
    template <typename B, typename SatSolver>
    Mixin<B, SatSolver>::Mixin()
        : _bool_formula(Formula::cons({new_key("and"), new_int(1)}))
    {
        static_init();
    }

    template <typename B, typename SatSolver>
    void Mixin<B, SatSolver>::static_init()
    {
        auto& ssolver = sat_solver();
        ssolver.add_new_var("1");
        ssolver.add_new_var("0");
    }

    template <typename B, typename SatSolver>
    bool Mixin<B, SatSolver>::valid_sort(const Key& sort) const noexcept
    {
        return sort == sort_key;
    }

    template <typename B, typename SatSolver>
    void Mixin<B, SatSolver>::check_sort(const Key& sort) const
    {
        expect(valid_sort(sort), "Invalid sort identifier: "s + to_string(sort));
    }

    template <typename B, typename SatSolver>
    bool Mixin<B, SatSolver>::contains(const Key& key_) const noexcept
    {
        //+ use `find..` instead of `contains` plus lookup ..
        return Inherit::contains(key_) || contains_bool(key_);
    }

    template <typename B, typename SatSolver>
    bool Mixin<B, SatSolver>::contains_var(const Key& key_) const noexcept
    {
        return Inherit::contains_var(key_) || contains_bool_var(key_);
    }

    template <typename B, typename SatSolver>
    bool Mixin<B, SatSolver>::contains_bool(const Key& key_) const noexcept
    {
        return cbools().contains(key_);
    }

    template <typename B, typename SatSolver>
    void Mixin<B, SatSolver>::check_contains_bool(const Key& key_) const
    {
        expect(contains_bool(key_), "Undeclared/undefined boolean: "s + to_string(key_));
    }

    template <typename B, typename SatSolver>
    bool Mixin<B, SatSolver>::contains_bool_var(const Key& key_) const noexcept
    {
        assert(!contains_bool(key_) || Var::is_me(cbools().cptr(key_)));
        return contains_bool(key_);
    }

    template <typename B, typename SatSolver>
    bool Mixin<B, SatSolver>::contains_bool_fun(const Key& key_) const noexcept
    {
        /// Bool functions are handled via variables by SAT, not `Vars'
        //+ variables are not distinguishable from functions
        return contains_bool_var(key_);
    }

    template <typename B, typename SatSolver>
    const var::Id
    Mixin<B, SatSolver>::find_bool_id(const typename Sat_solver::Var_id& sat_vid) const noexcept
    {
        auto& map = cbool_ids_map();
        auto it = map.find(sat_vid);
        if (it == map.end()) return var::invalid_id;
        return it->second;
    }

    template <typename B, typename SatSolver>
    const var::Ptr* Mixin<B, SatSolver>::cfind_bool_ptr(const typename Sat_solver::Var_id& sat_vid) const noexcept
    {
        const auto vid = find_bool_id(sat_vid);
        if (vid == var::invalid_id) return nullptr;
        return &bools().ptr(vid);
    }

    template <typename B, typename SatSolver>
    bool Mixin<B, SatSolver>::contains_fun_def(const Key& key_) const noexcept
    {
        return cfun_defs_map().contains(key_);
    }

    template <typename B, typename SatSolver>
    void Mixin<B, SatSolver>::check_contains_fun_def(const Key& key_) const
    {
        expect(contains_fun_def(key_), "Undefined function: "s + to_string(key_));
    }

    template <typename B, typename SatSolver>
    void Mixin<B, SatSolver>::check_not_contains_fun_def(const Key& key_) const
    {
        expect(!contains_fun_def(key_), "Function already defined: "s + to_string(key_));
    }

    template <typename B, typename SatSolver>
    bool Mixin<B, SatSolver>::contains_def_bool_fun(const Key& key_) const noexcept
    {
        //+ returns true also for non-bool fun_defs
        return contains_fun_def(key_);
    }

    template <typename B, typename SatSolver>
    bool Mixin<B, SatSolver>::contains_formula(const Formula& phi) const noexcept
    {
        return cformula_keys_map().contains(phi);
    }

    template <typename B, typename SatSolver>
    void Mixin<B, SatSolver>::reserve_bools(size_t size_)
    {
        bools().reserve(size_);
    }

    template <typename B, typename SatSolver>
    void Mixin<B, SatSolver>::declare_var_impl(Key& key_, const Key& sort)
    {
        check_sort(sort);
        /// Now it should not throw up into `declare_var'
        if (sort != sort_key) throw dummy;

        declare_bool_var(move(key_));
    }

    template <typename B, typename SatSolver>
    void Mixin<B, SatSolver>::define_var_impl(Key& key_, const Key& sort, Elem& elem)
    {
        check_sort(sort);
        //+
        expect(!elem.is_key(),
               "Definition of variable alias is not supported yet.");
        /// Now it should not throw up into `define_var'
        if (sort != sort_key) throw dummy;

        define_bool_var(move(key_), move(elem).template to_value_check<Bool>());
    }

    template <typename B, typename SatSolver>
    void Mixin<B, SatSolver>::declare_bool_var(Key key_)
    {
        define_bool_var_tp(move(key_));
    }

    template <typename B, typename SatSolver>
    void Mixin<B, SatSolver>::define_bool_var(Key key_, Bool val)
    {
        define_bool_var_tp(move(key_), move(val));
    }

    template <typename B, typename SatSolver>
    template <typename ValT>
    void Mixin<B, SatSolver>::define_bool_var_tp(Key&& key_, ValT&& val)
    {
        auto sat_vid = pre_add_bool(key_);
        define_var_tp<Bool>(move(key_), move(val));
        post_add_bool(move(sat_vid));
    }

    template <typename B, typename SatSolver>
    typename Mixin<B, SatSolver>::Sat_solver::Var_id
    Mixin<B, SatSolver>::pre_add_bool(const Key& key_)
    {
        return add_new_sat_var(key_);
    }

    template <typename B, typename SatSolver>
    void Mixin<B, SatSolver>::post_add_bool(typename Sat_solver::Var_id sat_vid)
    {
        auto& vptr = cbools().cptrs().back();
        auto& vid = vptr->cid();
        bool_ids_map().emplace(sat_vid, vid);
        assert(vid == sat_var_ids_map().size());
        sat_var_ids_map().emplace_back(move(sat_vid));
    }

    template <typename B, typename SatSolver>
    typename Mixin<B, SatSolver>::Sat_solver::Var_id
    Mixin<B, SatSolver>::add_new_sat_var(const Key& key_)
    {
        this->check_not_contains(key_);
        return sat_solver().add_new_var(key_);
    }

    template <typename B, typename SatSolver>
    Key Mixin<B, SatSolver>::instantiate_expr(Formula phi)
    try {
        phi.maybe_virtual_init();
        return instantiate_expr_impl(phi);
    }
    catch (Dummy) {
        THROW(conv_to_bool_msg(phi));
    }

    template <typename B, typename SatSolver>
    Key Mixin<B, SatSolver>::instantiate_expr_impl(Formula& phi)
    {
        if (!try_conv_to_bool(phi)) throw dummy;

        return instantiate_bool(move(phi));
    }

    template <typename B, typename SatSolver>
    void Mixin<B, SatSolver>::assert_elem(Elem elem)
    {
        bool_formula().push_back(elem.to_ptr());
        sat_solver().add_eclause(expr::bools::Lit(move(elem)));
    }

    template <typename B, typename SatSolver>
    void Mixin<B, SatSolver>::assert_expr(Formula phi)
    try {
        phi.maybe_virtual_init();
        assert_expr_impl(phi);
    }
    catch (Dummy) {
        THROW(conv_to_bool_msg(phi));
    }

    template <typename B, typename SatSolver>
    void Mixin<B, SatSolver>::assert_expr_impl(Formula& phi)
    {
        if (!try_conv_to_bool(phi)) throw dummy;

        bool_formula().push_back(phi.to_ptr());
        assert_bool(move(phi));
    }

    template <typename B, typename SatSolver>
    bool Mixin<B, SatSolver>::try_conv_to_bool(Formula& phi)
    {
        assert(phi.initialized() && !phi.empty());

        auto eptr = move(phi).to_ptr();
        const bool ret = try_conv_to_bool(eptr);
        phi = Formula::cast(move(eptr));
        return ret;
    }

    template <typename B, typename SatSolver>
    bool Mixin<B, SatSolver>::try_conv_to_bool(expr::Ptr& eptr)
    try {
        auto& phi = Formula::cast(eptr);
        if (!Bools::Types::Pred::Fun::Opers::valid_bool(phi)) return false;

        for (auto& subeptr : phi) {
            if (List::is_me(subeptr)) {
                if (!try_conv_to_bool_impl(subeptr)) return false;
                continue;
            }

            if (is_key(subeptr)) {
                auto& k = cast_key(subeptr);
                if (!contains_bool(k)) return false;
            }
        }

        return true;
    }
    catch (const Error& err) {
        THROW(err) + "\nAt: " + to_string(eptr);
    }

    template <typename B, typename SatSolver>
    void Mixin<B, SatSolver>::conv_to_bool(Formula& phi)
    {
        expect(try_conv_to_bool(phi), conv_to_bool_msg(phi));
    }

    template <typename B, typename SatSolver>
    String Mixin<B, SatSolver>::conv_to_bool_msg(const Formula& phi) const
    {
        return "Conversion to boolean form failed: "s + phi.to_string();
    }

    template <typename B, typename SatSolver>
    bool Mixin<B, SatSolver>::try_conv_to_bool_impl(expr::Ptr& eptr)
    {
        return try_conv_to_bool(eptr);
    }

    template <typename B, typename SatSolver>
    Key Mixin<B, SatSolver>::instantiate_bool(Formula phi)
    {
        phi.maybe_virtual_init();
        return instantiate_bool_fun_inst(make_bool_auto_fun_inst(move(phi)));
    }

    //! it does not recognize inline usage of previously defined functions
    template <typename B, typename SatSolver>
    void Mixin<B, SatSolver>::assert_bool(Formula phi)
    {
        phi.maybe_virtual_init();
        check_contains_free_arg_keys<Bool>(*this, phi);
        assert_cnf(move(phi));
    }

    template <typename B, typename SatSolver>
    typename Mixin<B, SatSolver>::Fun_inst
    Mixin<B, SatSolver>::make_fun_inst(const Key& key_, const Keys& arg_keys)
    {
        //+ two lookups
        check_contains_fun_def(key_);
        return fun_def(key_).instantiate(arg_keys);
    }

    template <typename B, typename SatSolver>
    typename Mixin<B, SatSolver>::Fun_inst
    Mixin<B, SatSolver>::make_auto_fun_inst(Formula phi, Key key_)
    {
        return Fun_def(this->that_l(), move(key_), move(phi)).instantiate();
    }

    template <typename B, typename SatSolver>
    typename Mixin<B, SatSolver>::Fun_inst
    Mixin<B, SatSolver>::make_bool_auto_fun_inst(Formula phi)
    {
        return make_auto_fun_inst(move(phi), aux_bool_var_key_prefix);
    }

    template <typename B, typename SatSolver>
    Formula Mixin<B, SatSolver>::expand_fun_inst(Fun_inst fun)
    {
        return move(fun.formula);
    }

    template <typename B, typename SatSolver>
    void Mixin<B, SatSolver>::assert_fun_inst(Fun_inst fun)
    {
        assert_expr(expand_fun_inst(move(fun)));
    }

    /// Adds CNF clauses according to `key <=> expr`
    /// Bool functions are all handled by SAT solver,
    /// it does not use `Fun`s at all
    template <typename B, typename SatSolver>
    Key Mixin<B, SatSolver>::instantiate_bool_fun_inst(Fun_inst fun)
    {
        return instantiate_fun_inst_tp<Bool, no_check>(move(fun),
            [](This* this_, auto&& fun_){
                this_->declare_bool_var(fun_.key);
                this_->assert_bool(move(fun_).to_pred());
        });
    }

    template <typename B, typename SatSolver>
    Formula Mixin<B, SatSolver>::expand_bool_fun_inst(Fun_inst fun)
    {
        return expand_fun_inst(move(fun));
    }

    template <typename B, typename SatSolver>
    void Mixin<B, SatSolver>::assert_bool_fun_inst(Fun_inst fun)
    {
        assert_bool(expand_bool_fun_inst(move(fun)));
    }

    template <typename B, typename SatSolver>
    void Mixin<B, SatSolver>::assert_cnf(expr::bools::Cnf cnf)
    {
        /// Add all newly introduced variables
        for (Idx init_i = cnf.init_idx(), last_i = cnf.last_idx(), i = init_i; i <= last_i; i++) {
            add_new_sat_var(expr::bools::var_id(cnf.cx(), i));
        }
        this->auto_var_counter() += cnf.increased_size();
        /// If it returns false, it is in conflicting state.. (unsat)
        sat_solver().add_ecnf(move(cnf));
    }

    template <typename B, typename SatSolver>
    void Mixin<B, SatSolver>::define_fun_impl(Key& key_, const Key& sort, Formula& phi,
                                              Keys& arg_keys, const Key& args_sort)
    {
        check_sort(sort);
        /// Now it should not throw up into `define_fun'
        if (sort != sort_key) throw dummy;
        if (!empty(arg_keys)) {
            check_sort(args_sort);
            if (args_sort != sort_key) throw dummy;
        }

        define_bool_fun(move(key_), move(phi), move(arg_keys));
    }

    template <typename B, typename SatSolver>
    void Mixin<B, SatSolver>::define_bool_fun(Key key_, Formula phi, Keys arg_keys)
    {
        define_fun_tp<Bool>(move(key_), move(phi), move(arg_keys));
    }

    template <typename B, typename SatSolver>
    Formula Mixin<B, SatSolver>::expand_def_fun(const Key& key_, const Keys& arg_keys)
    {
        return expand_fun_inst(make_fun_inst(key_, arg_keys));
    }

    template <typename B, typename SatSolver>
    void Mixin<B, SatSolver>::assert_def_fun(const Key& key_, const Keys& arg_keys)
    {
        assert_fun_inst(make_fun_inst(key_, arg_keys));
    }

    template <typename B, typename SatSolver>
    Key Mixin<B, SatSolver>::instantiate_def_bool_fun(const Key& key_, const Keys& arg_keys)
    {
        return instantiate_def_fun_tp(key_, arg_keys, &This::instantiate_bool_fun_inst);
    }

    template <typename B, typename SatSolver>
    Formula Mixin<B, SatSolver>::
        expand_def_bool_fun(const Key& key_, const Keys& arg_keys)
    {
        return expand_bool_fun_inst(make_fun_inst(key_, arg_keys));
    }

    template <typename B, typename SatSolver>
    void Mixin<B, SatSolver>::assert_def_bool_fun(const Key& key_, const Keys& arg_keys)
    {
        assert_bool_fun_inst(make_fun_inst(key_, arg_keys));
    }

    template <typename B, typename SatSolver>
    void Mixin<B, SatSolver>::init_solve()
    {
        auto& ssolver = sat_solver();
        //+ it would be more efficient if this fact was known earlier
        /// Cannot assert earlier, lower classes are possibly not ready yet
        ssolver.add_eclause("1");
        ssolver.add_eclause("~0");
        Inherit::init_solve();
    }

    template <typename B, typename SatSolver>
    void Mixin<B, SatSolver>::finish_solve(const Sat& sat)
    {
        Inherit::finish_solve(sat);
        assert(this->cmode() != Mode::sat || this->ccheck_sat_ref().all_sat());
    }

    template <typename B, typename SatSolver>
    String Mixin<B, SatSolver>::var_to_string_impl(const Key& key_) const
    {
        if (!contains_bool(key_)) throw dummy;
        return var_to_string(*cbools().cptr(key_));
    }

    template <typename B, typename SatSolver>
    void Mixin<B, SatSolver>::print_vars_impl() const noexcept
    {
        for (auto& vptr : cbools()) {
            this->print_var_impl(vptr->ckey());
        }
    }

    template <typename B, typename SatSolver>
    String Mixin<B, SatSolver>::var_stats_to_string() const
    {
        ostringstream oss;

        oss << "# bools: " << cbools().size() << "\n";
        return oss.str();
    }
}

////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////

namespace unsot::smt::solver::bools {
    template <typename B, typename SatSolver>
    Mixin<B, SatSolver>::Fun_def::
        Fun_def(Solver_link solver_l_, Key key_, Formula phi, Keys arg_keys_)
        : _solver_l(solver_l_), _key(move(key_)),
          _formula(move(phi)), _arg_keys(move(arg_keys_))
    {
        assert(cformula().initialized());
    }

    template <typename B, typename SatSolver>
    typename Mixin<B, SatSolver>::Fun_inst
    Mixin<B, SatSolver>::Fun_def::instantiate(const Keys& arg_keys_)
    {
        using unsot::to_string;
        expect(size(arg_keys_) == size(carg_keys()),
               "Size of argument keys mismatch: expected "s
               + to_string(size(carg_keys())) + ", got: "
               + to_string(size(arg_keys_))
               + "\n(" + to_string(carg_keys())
               + " != " + to_string(arg_keys_) + ")");

        Formula iphi = replace_copy(cformula(), carg_keys(), arg_keys_);
        assert(iphi.initialized());

        /*+
        for_each_if_key_rec(iphi, as_keys([this](auto& key_){
            if (!csolver().contains_fun_def(key_)) return;
            expect(key_ != ckey(),
                   "Circular dependency in function instantiation: "s
                   + this->to_string());
            // arg_keys
            // not only reals !!!!
            key_ = solver().instantiate_def_real_fun(key_);
        }));
        */

        Key ikey = solver().new_var_key(ckey());
        return {move(ikey), move(iphi)};
    }

    template <typename B, typename SatSolver>
    String Mixin<B, SatSolver>::Fun_def::to_string() const&
    {
        using unsot::to_string;
        return to_string(ckey()) + "(" + to_string(carg_keys()) + ")"
             + " : " + cformula().to_string();
    }

    ////////////////////////////////////////////////////////////////

    template <typename B, typename SatSolver>
    Formula Mixin<B, SatSolver>::Fun_inst::to_pred() const&
    {
        return Fun_inst(*this).to_pred();
    }

    template <typename B, typename SatSolver>
    Formula Mixin<B, SatSolver>::Fun_inst::to_pred()&&
    {
        return Formula::cons({new_key("="), new_key(move(key)),
                              move(formula).to_ptr()});
    }

    template <typename B, typename SatSolver>
    String Mixin<B, SatSolver>::Fun_inst::to_string() const&
    {
        using unsot::to_string;
        return to_string(key) + " : " + formula.to_string();
    }

    ////////////////////////////////////////////////////////////////

    template <typename B, typename SatSolver>
    size_t Mixin<B, SatSolver>::Formula_hash::operator ()(const Formula& phi) const
    {
        return invoke(std::hash<String>(), to_string(phi));
    }
}

#include "smt/solver/bools/check_sat.tpp"
#include "smt/solver/bools/parser.tpp"
