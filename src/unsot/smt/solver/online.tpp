#pragma once

#include "smt/solver/online.hpp"

#include "util/alg.hpp"

namespace unsot::smt::solver::online {
    template <typename B>
    Mixin<B>::Mixin()
    {
        this->option(":t-propagate-strategy");
        this->option(":t-suggest-strategy");
    }

    template <typename B>
    bool Mixin<B>::valid_option_value(const Option& opt, const Option_value& val) const
    try {
        return Inherit::valid_option_value(opt, val);
    }
    catch (Dummy) {
        if (util::contains({":t-propagate-strategy", ":t-suggest-strategy"}, opt)) return true;

        throw;
    }

    template <typename B>
    void Mixin<B>::apply_option(const Option& opt)
    try {
        Inherit::apply_option(opt);
    }
    catch (Dummy) {
        auto& val = this->coption(opt);
        if (opt == ":t-propagate-strategy") return this->set_t_propagate_strategy(val);
        if (opt == ":t-suggest-strategy") return this->set_t_suggest_decision_strategy(val);

        throw;
    }

    template <typename B>
    void Mixin<B>::set_all_strategies_impl(const String& name, bool force)
    {
        bool any_success = false;
        try {
            Inherit::set_all_strategies_impl(name, force);
            any_success = true;
        }
        catch (Dummy) { }

        //+ if `any_success`, check-sat strategy was set, which is already enough
        //+ but later in `online::bools` it would be undisting. whether this or e.g. T-prop was set

        try {
            set_t_propagate_strategy(name, force);
            any_success = true;
        }
        //+ only "unknown strategy" error, not other errors ... ideally just catch dummy
        catch (const Error&) { }
        try {
            set_t_suggest_decision_strategy(name, force);
            any_success = true;
        }
        catch (const Error&) { }

        if (!any_success) throw dummy;
    }

    template <typename B>
    void Mixin<B>::set_t_propagate_strategy_impl(const String& /*name*/, bool /*force*/)
    {
        //+ "none" would disable T-propagations entirely?

        throw dummy;
    }

    template <typename B>
    void Mixin<B>::set_t_suggest_decision_strategy_impl(const String& name, bool force)
    {
        if (name == "none") return this->check_sat_ref().set_t_suggest_decision_strategy_tp(force);

        throw dummy;
    }
}

#include "smt/solver/online/check_sat.tpp"
