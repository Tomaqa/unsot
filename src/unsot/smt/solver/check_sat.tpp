#pragma once

#include "smt/solver/check_sat.hpp"

#include <sstream>
#include <omp.h>

namespace unsot::smt::solver {
    template <typename S>
    void Crtp<S>::Check_sat::perform_init()
    {
        const bool prof = this->cprofiling();
        double start;
        if (prof) start = omp_get_wtime();

        perform_init_profiled();

        if (!prof) return;

        const double finish = omp_get_wtime();
        init_duration() += finish - start;
    }

    template <typename S>
    void Crtp<S>::Check_sat::perform_init_profiled()
    {
        perform_init_impl();
    }

    template <typename S>
    void Crtp<S>::Check_sat::perform_init_impl()
    {
        Inherit::perform_init();
    }

    template <typename S>
    void Crtp<S>::Check_sat::perform_finish()
    {
        const bool prof = this->cprofiling();
        double start;
        if (prof) start = omp_get_wtime();

        perform_finish_profiled();

        if (!prof) return;

        const double finish = omp_get_wtime();
        finish_duration() += finish - start;
    }

    template <typename S>
    void Crtp<S>::Check_sat::perform_finish_profiled()
    {
        perform_finish_impl();
    }

    template <typename S>
    void Crtp<S>::Check_sat::perform_finish_impl()
    {
        Inherit::perform_finish();
    }

    template <typename S>
    String Crtp<S>::Check_sat::profiling_to_string(Duration total) const
    {
        ostringstream oss;
        oss << Inherit::profiling_to_string(total);

        const Duration sd = this->cduration();
        const Duration isd = cinit_duration();
        const Duration fsd = cfinish_duration();
        oss << Inherit::name() << " init time: " << isd << " s (" << (isd/sd)*100 << " % of " << Inherit::name() << ")\n"
            << Inherit::name() << " finish time: " << fsd << " s (" << (fsd/sd)*100 << " % of " << Inherit::name() << ")\n"
            ;
        return oss.str();
    }
}
