#pragma once

#include "smt/sat/solver/minisat.hpp"

#include <fstream>

namespace unsot::smt::sat::solver::minisat {
    template <typename B>
    bool Mixin<B>::contains(const Var_id& x) const noexcept
    {
        return size_t(x) < vars_size();
    }

    template <typename B>
    typename Mixin<B>::Clause Mixin<B>::make_clause(const Orig_clause& clause)
    {
        Orig_vec<Lit> lits;
        to_orig_vec_of_lits(lits, clause);
        return make_clause(lits);
    }

    template <typename B>
    typename Mixin<B>::Clause Mixin<B>::make_clause(const Orig_vec<Lit>& lits)
    {
        Clause clause;
        const int size_ = size(lits);
        clause.reserve(size_);
        for (Idx i = 0; i < size_; ++i) {
            clause.push_back(lits[i]);
        }
        return clause;
    }

    template <typename B>
    template <typename ContT>
    void Mixin<B>::to_orig_vec_of_lits(Orig_vec<Lit>& lits, ContT&& cont)
    {
        const int size_ = size(cont);
        lits.growTo(size_);
        for (Idx i = 0; i < size_; ++i) {
            lits[i] = FORWARD(cont[i]);
        }
    }

    template <typename B>
    bool Mixin<B>::add_clause(Clause clause)
    {
        Orig_vec<Lit> lits;
        to_orig_vec_of_lits(lits, move(clause));
        return this->solver().addClause_(cast_orig_lits(lits));
    }

    template <typename B>
    typename Mixin<B>::Cnf Mixin<B>::to_cnf() const
    {
        return to<Cnf>(this->csolver().clausesBegin(), this->csolver().clausesEnd(),
                       clauses_size(), [](const Orig_clause& clause){
            return make_clause(clause);
        });
    }

    template <typename B>
    ostream& Mixin<B>::to_dimacs(ostream& os) const
    {
        char fname[] = "/tmp/minisat_dimacs-XXXXXX";
        mkstemp(fname);
        this->csolver().toDimacs(fname);

        //+ inefficient
        ifstream(fname) >> os.rdbuf();
        remove(fname);

        return os;
    }

    template <typename B>
    bool Mixin<B>::solve(const Clause& assumps)
    {
        Orig_vec<Lit> lits;
        to_orig_vec_of_lits(lits, assumps);
        return this->solver().solve(cast_orig_lits(lits));
    }

    template <typename B>
    Sat Mixin<B>::solve_limited(const Clause& assumps)
    {
        Orig_vec<Lit> lits;
        to_orig_vec_of_lits(lits, assumps);
        return to_value(this->solver().solveLimited(cast_orig_lits(lits)));
    }

    template <typename B>
    Value Mixin<B>::value(const Var_id& x) const
    {
        return to_value(this->csolver().value(x));
    }

    template <typename B>
    Value Mixin<B>::model_value(const Var_id& x) const
    {
        return to_value(this->csolver().modelValue(x));
    }

    template <typename B>
    const Values& Mixin<B>::cmodel() const
    {
        _model = to_values(this->csolver().model);
        return _model;
    }

    template <typename B>
    const typename Mixin<B>::Clause& Mixin<B>::cconflict() const
    {
        auto& conflict_ = this->csolver().conflict;
        _conflict = make_clause(ccast_orig_vec_of_lits(conflict_.toVec()));
        return _conflict;
    }
}
