#pragma once

#include "smt/sat/solver/run.hpp"

#include "util/map.hpp"

namespace unsot::smt::sat::solver {
    template <typename S, typename ConfT>
    String Crtp<S, ConfT>::Run::usage() const
    {
        return Inherit::usage() + lusage();
    }

    template <typename S, typename ConfT>
    String Crtp<S, ConfT>::Run::lusage() const
    {
        return {};
    }

    template <typename S, typename ConfT>
    void Crtp<S, ConfT>::Run::init()
    {
        Inherit::init();
        linit();
    }

    template <typename S, typename ConfT>
    void Crtp<S, ConfT>::Run::do_stuff()
    {
        Clause clause;
        Map<int, Var_id> vars;
        for (int i; is() >> i;) {
            if (i == 0) {
                solver().add_clause(move(clause));
                clause = {};
                continue;
            }

            const bool neg = (i < 0);
            if (neg) i = -i;

            auto [it, inserted] = vars.try_emplace(i);
            Var_id x = inserted ? solver().new_var() : it->second;
            if (inserted) it->second = x;
            clause.emplace_back(move(x), neg);
        }

        expect(empty(clause), "The input does not end with 0.");

        const Sat sat = solver().solve();
        os() << sat << endl;
        if (!sat) return;

        os() << endl << "Model: " << csolver().cmodel() << endl;
        for (auto& [i, x] : vars) {
            os() << i << "=" << csolver().model_value(x) << " ";
        }
        os() << endl;
    }

    template <typename S, typename ConfT>
    String Crtp<S, ConfT>::Run::getopt_str() const noexcept
    {
        return Inherit::getopt_str() + lgetopt_str();
    }

    template <typename S, typename ConfT>
    String Crtp<S, ConfT>::Run::lgetopt_str() const noexcept
    {
        return {};
    }

    template <typename S, typename ConfT>
    bool Crtp<S, ConfT>::Run::process_opt(char c)
    {
        if (Inherit::process_opt(c)) return true;
        return lprocess_opt(c);
    }

    template <typename S, typename ConfT>
    bool Crtp<S, ConfT>::Run::lprocess_opt(char)
    {
        return false;
    }
}
