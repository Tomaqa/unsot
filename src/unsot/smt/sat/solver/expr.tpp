#pragma once

namespace unsot::smt::sat::solver::expr {
    template <typename B>
    bool Mixin<B>::contains(const Var_id& x) const noexcept
    {
        return cvar_ids_bimap().contains(x);
    }

    template <typename B>
    bool Mixin<B>::contains(const bools::Var_id& ex) const noexcept
    {
        return cvar_ids_bimap().contains(ex);
    }

    template <typename B>
    void Mixin<B>::check_contains(const bools::Var_id& ex) const
    {
        expect(contains(ex), "Undefined SAT (expr) variable: "s + to_string(ex));
    }

    template <typename B>
    typename Mixin<B>::Cnf Mixin<B>::to_cnf(bools::Cnf ecnf) const
    {
        return to<Cnf>(move(ecnf), [this](auto&& eclause){
            return to_clause(move(eclause));
        });
    }

    template <typename B>
    typename Mixin<B>::Clause Mixin<B>::to_clause(bools::Clause eclause) const
    {
        return to<Clause>(move(eclause), [this](auto&& elit){
            return to_lit(move(elit));
        });
    }

    template <typename B>
    typename Mixin<B>::Lit Mixin<B>::to_lit(bools::Lit ep) const
    {
        const bool sign_ = ep.sign();
        bools::Var_id ex = move(ep).var_id();
        auto x = cvar_id(ex);
        return {move(x), sign_};
    }

    template <typename B>
    bools::Cnf Mixin<B>::to_ecnf(Cnf cnf) const
    {
        return to<bools::Cnf>(move(cnf), BIND_THIS(to_eclause));
    }

    template <typename B>
    bools::Clause Mixin<B>::to_eclause(Clause clause) const
    {
        return to<bools::Clause>(move(clause), BIND_THIS(to_elit));
    }

    template <typename B>
    bools::Lit Mixin<B>::to_elit(Lit p) const
    {
        const bool sign_ = p.sign();
        Var_id x = move(p).var_id();
        auto ex = cevar_id(x);
        return bools::Lit(move(ex), sign_);
    }

    template <typename B>
    typename Mixin<B>::Var_id Mixin<B>::add_new_var(const bools::Var_id& ex)
    {
        Var_id x = this->new_var();
        //+ check contains?
        var_ids_bimap().insert(ex, x);
        return x;
    }

    template <typename B>
    bool Mixin<B>::add_ecnf(bools::Cnf ecnf)
    {
        for (auto&& eclause : move(ecnf)) {
            if (!add_eclause(move(eclause))) return false;
        }
        return true;
    }

    template <typename B>
    bools::Cnf Mixin<B>::to_ecnf() const
    {
        return to_ecnf(this->to_cnf());
    }
}
