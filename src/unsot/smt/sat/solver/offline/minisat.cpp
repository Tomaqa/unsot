#include "smt/sat/solver/offline/minisat.hpp"

#include "smt/sat/solver.tpp"
#include "smt/sat/solver/minisat.tpp"

#include "smt/sat/solver/run.tpp"

#ifndef NO_EXPLICIT_TP_INST
namespace unsot::smt::sat::solver {
    template class offline::Minisat::Base::Crtp;
    template class offline::Minisat::Solver_mixin;
    template class offline::Minisat::Minisat_t::Mixin;
}
#endif  /// NO_EXPLICIT_TP_INST
