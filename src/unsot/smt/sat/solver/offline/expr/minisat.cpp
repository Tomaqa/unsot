#include "smt/sat/solver/offline/expr/minisat.hpp"

#include "smt/sat/solver.tpp"
#include "smt/sat/solver/expr.tpp"
#include "smt/sat/solver/minisat.tpp"

#include "smt/sat/solver/run.tpp"

#ifndef NO_EXPLICIT_TP_INST
namespace unsot::smt::sat::solver {
    template class offline::expr::Minisat::Base::Crtp;
    template class offline::expr::Minisat::Expr_t::Mixin;
    template class offline::expr::Minisat::Solver_mixin;
    template class offline::expr::Minisat::Minisat_t::Mixin;
}
#endif  /// NO_EXPLICIT_TP_INST
