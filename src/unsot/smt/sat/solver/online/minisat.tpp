#pragma once

namespace unsot::smt::sat::solver::online::minisat {
    template <typename B>
    Mixin<B>::Mixin(Mixin&& rhs)
        : Inherit(move(rhs))
    {
        linit();
    }

    template <typename B>
    Mixin<B>& Mixin<B>::operator =(Mixin&& rhs)
    {
        if (this != &rhs) {
            Inherit::operator =(move(rhs));
            linit();
        }
        return *this;
    }

    template <typename B>
    void Mixin<B>::linit()
    {
        this->solver().connect(*this);
    }

    template <typename B>
    void Mixin<B>::sat_propagate(Lit p)
    {
        assert(this->value(p.var_id()).unknown());
        this->solver().Solver::Inherit::uncheckedEnqueue(move(p));
    }

    template <typename B>
    void Mixin<B>::sat_learn(Clause clause)
    {
        this->solver().learn(move(clause));
    }

    template <typename B>
    void Mixin<B>::sat_backtrack()
    {
        sat_backtrack_to(decision_level() - 1);
    }

    template <typename B>
    void Mixin<B>::sat_backtrack_to(int level)
    {
        this->solver().Solver::Inherit::cancelUntil(level);
    }
}

////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////

namespace unsot::smt::sat::solver::online::minisat {
    template <typename B>
    void Mixin<B>::Solver::connect(Online_solver& osolver)
    {
        _online_solver_l = &osolver;
    }

    template <typename B>
    Orig_lbool Mixin<B>::Solver::search(int /*nof_conflicts*/)
    {
        _searching = true;
        /// `-1` disables restarting the search
        /// (otherwise when a threshold of `nof_conflicts` is reached)
        auto ret = Inherit::search(-1);
        /// keeping these restarts seems to degrade the performance,
        /// esp. in UNSAT cases (even with e.g. `nof_conflicts*64`)
        _searching = false;
        return ret;
    }

    template <typename B>
    void Mixin<B>::Solver::newDecisionLevel()
    {
        Inherit::newDecisionLevel();
        online_solver().notify_new_decision_level();
    }

    template <typename B>
    Orig_lit Mixin<B>::Solver::pickBranchLit()
    {
        auto opt_sugg = online_solver().t_suggest_decision();
        if (!opt_sugg) return Inherit::pickBranchLit();

        auto& sugg = opt_sugg.value();
        auto& vid = sugg.var_id;
        assert(vid != ::Minisat::var_Undef);
        assert(this->value(vid) == ::Minisat::l_Undef);
        assert(this->decision[vid]);

        auto flag = move(sugg.flag);
        if (flag.valid()) {
            return Lit(move(vid), flag.cvalue());
        }

        return this->mkBranchLit(move(vid));
    }

    template <typename B>
    Orig_clause_ref Mixin<B>::Solver::propagate()
    {
        while (true) {
            if (auto cref = Inherit::propagate(); cref != Orig_undef_clause_ref)
                return cref;

            const Sat sat = online_solver().t_propagate_exec();
            if (sat) return Orig_undef_clause_ref;
            if (sat == Sat::unsat) {
                assert(_learnt_cref != Orig_undef_clause_ref);
                return _learnt_cref;
            }
        }
    }

    template <typename B>
    void Mixin<B>::Solver::uncheckedEnqueue(Orig_lit p, Orig_clause_ref from)
    {
        Inherit::uncheckedEnqueue(p, move(from));
        online_solver().notify(move(p));
    }

    template <typename B>
    void Mixin<B>::Solver::learn(typename Online_solver::Clause oclause)
    {
        Orig_vec<typename Online_solver::Lit> lits;
        Online_solver::to_orig_vec_of_lits(lits, move(oclause));
        auto& orig_lits = Online_solver::cast_orig_lits(lits);
        static constexpr bool learnt = true;
        Orig_clause_ref cref = this->ca.alloc(orig_lits, learnt);
        assert(cref != Orig_undef_clause_ref);

        Orig_clause& clause = this->ca[cref];
        //+ unit propagation is not supported (should be?)
        assert(clause.size() > 1);
        Orig_lit& p = clause[0];

        this->learnts.push(cref);
        this->attachClause(cref);
        this->claBumpActivity(clause);

        const bool is_confl = (this->value(p) != ::Minisat::l_Undef);
        if (!is_confl) Inherit::uncheckedEnqueue(p, cref);
        else _learnt_cref = move(cref);
    }

    template <typename B>
    void Mixin<B>::Solver::cancelUntil(int level)
    {
        if (this->decisionLevel() <= level) return;
        online_solver().notify_backtrack_to(level);
        Inherit::cancelUntil(level);
    }

    template <typename B>
    void Mixin<B>::Solver::cancelAll()
    {
        if (_searching) online_solver().notify_restart();
        Inherit::cancelUntil(0);
    }

    template <typename B>
    void Mixin<B>::Solver::externReason(Orig_lit p, Orig_clause_ref& confl)
    {
        if (confl != Orig_undef_clause_ref)
            return Inherit::externReason(move(p), confl);

        assert(p != ::Minisat::lit_Undef);
        assert(this->value(p) != ::Minisat::l_Undef);
        online_solver().t_explain(move(p));
        assert(_learnt_cref != Orig_undef_clause_ref);
        confl = _learnt_cref;
    }

    template <typename B>
    void Mixin<B>::Solver::reduceDB()
    {
        //+ frequent calling causes significant slowdown (sorting etc.)
        //+ Inherit::reduceDB();
    }
}
