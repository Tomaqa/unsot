#include "smt/sat/solver.tpp"
#include "smt/sat/solver/expr.tpp"
#include "smt/sat/solver/minisat.tpp"
#include "smt/sat/solver/online/minisat.tpp"

#include "smt/sat/solver/run.tpp"

#ifndef NO_EXPLICIT_TP_INST
namespace unsot::smt::sat::solver {
    template class aux::_Minisat::Base::Crtp;
    template class aux::_Minisat::Expr_t::Mixin;
    template class aux::_Minisat::Solver_mixin;
    template class aux::_Minisat::Minisat_t::Mixin;
    template class aux::_Minisat::Online_t::Mixin;
    template class aux::_Minisat::Online_minisat_t::Mixin;
}
#endif  /// NO_EXPLICIT_TP_INST
