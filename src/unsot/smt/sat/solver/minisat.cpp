#include "smt/sat/solver/minisat.hpp"

namespace unsot::smt::sat::solver::minisat {
    String Lit::to_string() const&
    {
        using unsot::to_string;
        return (is_neg() ? "-" : "") + to_string(var_id());
    }
}

////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////

namespace unsot::smt::sat::solver::minisat {
    Value to_value(const Orig_lbool& lbool) noexcept
    {
        if (lbool == ::Minisat::l_True) return true;
        if (lbool == ::Minisat::l_False) return false;
        return unknown;
    }

    Values to_values(const Orig_lbools& lbools)
    {
        Values values;
        const int size_ = size(lbools);
        values.reserve(size_);
        for (Idx i = 0; i < size_; ++i) {
            values.push_back(to_value(lbools[i]));
        }
        return values;
    }
}
