#pragma once

namespace unsot::smt::sat::solver {
    template <typename S, typename Conf>
    void Crtp<S, Conf>::neg(Clause& clause) const noexcept
    {
        for (auto& l : clause) neg(l);
    }

    template <typename S, typename Conf>
    void Crtp<S, Conf>::check_contains(const Var_id& x) const
    {
        expect(contains(x), "Undefined SAT variable: "s + to_string(x));
    }

    template <typename S, typename Conf>
    bool Crtp<S, Conf>::add_cnf(Cnf cnf)
    {
        for (auto&& clause : move(cnf)) {
            if (!add_clause(move(clause))) return false;
        }
        return true;
    }
}
