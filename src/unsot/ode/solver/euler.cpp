#include "ode/solver/euler.hpp"

#include "util/alg.hpp"

namespace unsot::ode::solver {
    void Euler::do_step(const flow::Id& fid, const State& in, State& out,
                        const Real dt_, const Config& config) const
    {
        cflow(fid).eval_step(in, out, config);
        for_each(out.ode_begin(), out.ode_end(), in.code_begin(),
                 [dt_](Real& o, Real i){
            o *= dt_;
            o += i;
        });
    }
}
