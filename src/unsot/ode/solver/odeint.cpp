#include "ode/solver/odeint.hpp"

#include "util/numeric/alg.hpp"

namespace unsot::ode::solver {
    Odeint::Odeint(Flow flow_)
        : Odeint(Flows{move(flow_)})
    { }

    Odeint::Odeint(Flows flows_)
        : Inherit(move(flows_))
    {
        /// `add_flow' is virtual, but it does not take effect in constructor!
        steppers().resize(size());
    }

    Odeint::Odeint(initializer_list<Flow> list)
        : Odeint(Flows(move(list)))
    { }

    flow::Id Odeint::add_flow(Flow flow_)
    {
        steppers().push_back({});
        return Inherit::add_flow(move(flow_));
    }

    void Odeint::do_step(const flow::Id& fid, const State& in, State& out,
                         const Real dt_, const Config& config) const
    {
        auto f = [this, &fid, &config]
                 (const auto& x, auto& dx, const Real /*t_*/){
            cflow(fid).eval_step(static_cast<const Reals&>(x),
                                 static_cast<Reals&>(dx), config);
        };

        stepper(fid).do_step(move(f), in, in.ct(), out, dt_);
    }
}
