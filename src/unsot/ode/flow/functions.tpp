#pragma once

#include "ode/flow.hpp"
#include "ode/flow/functions.hpp"

#include "util/alg.hpp"
#include "util/view.hpp"
#include "expr/alg.hpp"

namespace unsot::ode::flow::aux {
    using namespace expr;

    template <typename FunT>
    Flow::Functions<FunT>::Functions(const Keys& keys_)
        : Functions(keys_, Formulas{})
    { }

    template <typename FunT>
    Flow::Functions<FunT>::Functions(const Keys& keys_, Formula phi)
        : Functions(keys_, Formulas{move(phi)})
    { }

    template <typename FunT>
    Flow::Functions<FunT>::Functions(const Keys& keys_, Formulas phis)
        : _keys_l(&keys_), _reals_ptr(expr::new_values<Real>(keys_.ckeys())),
          _funs(to<Funs>(move(phis)))
    { }

    template <typename FunT>
    Flow::Functions<FunT>::
        Functions(const Keys& keys_, Formulas phis, Formula def_phi)
        : Functions(keys_, cat(Formulas{move(def_phi)}, move(phis)))
    { }

    template <typename FunT>
    void Flow::Functions<FunT>::linit()
    {
        init_funs();
    }

    template <typename FunT>
    bool Flow::Functions<FunT>::lvalid_post_init() const noexcept
    {
        return valid_funs();
    }

    template <typename FunT>
    void Flow::Functions<FunT>::lcheck_post_init() const
    {
        check_funs();
    }

    template <typename FunT>
    void Flow::Functions<FunT>::init_funs()
    {
        for_each(funs(), BIND_THIS(init_fun));
    }

    template <typename FunT>
    void Flow::Functions<FunT>::init_fun(Fun& fun_)
    {
        fun_.virtual_init_with_args(ckeys().cptr(), creals_ptr(), ckeys().cids_map_ptr());

        if constexpr (_debug_) {
            for_each_if_list_rec_pre(fun_, [](auto& eptr){
                //+ if constexpr (!has_preds_v) assert(ode::Fun::is_me(eptr));
                //+ else assert(ode::Fun::is_me(eptr) || ode::Pred::is_me(eptr));
                assert(ode::Fun::is_me(eptr) || ode::Pred::is_me(eptr));
            });
        }
    }

    template <typename FunT>
    bool Flow::Functions<FunT>::valid_funs() const noexcept
    {
        return all_of(cfuns(), BIND_THIS(valid_fun));
    }

    template <typename FunT>
    void Flow::Functions<FunT>::check_funs() const
    {
        for_each(cfuns(), BIND_THIS(check_fun));
    }

    template <typename FunT>
    bool Flow::Functions<FunT>::valid_fun(const Fun& fun_) const noexcept
    {
        return valid_fun_keys(fun_)
            && valid_formula(fun_);
    }

    template <typename FunT>
    void Flow::Functions<FunT>::check_fun(const Fun& fun_) const
    {
        check_fun_keys(fun_);
        check_formula(fun_);
    }

    template <typename FunT>
    bool Flow::Functions<FunT>::valid_fun_keys(const Fun& fun_) const noexcept
    {
        assert(fun_.empty() || ckeys().ckeys() == fun_.ckeys());
        return fun_.increased_size() == 0;
    }

    template <typename FunT>
    void Flow::Functions<FunT>::check_fun_keys(const Fun& fun_) const
    {
        using unsot::to_string;
        expect(valid_fun_keys(fun_),
               "Additional keys: "s + to_string(fun_.ckeys())
               + " [ " + to_string(fun_.cids_map()) + "]");
    }

    template <typename FunT>
    bool Flow::Functions<FunT>::valid_formula(const Formula& phi) const noexcept
    try {
        assert(!phi.empty() || std::empty(phi.coper()));
        if constexpr (!has_preds_v) assert(Fun::Opers::valid_f(phi));
        else assert(Fun::Opers::valid_pred(phi) || Fun::Opers::valid_bool(phi));

        return valid_formula_head_impl(phi)
            && valid_formula_body_impl(phi);
    }
    catch (Ignore) {
        return true;
    }
    catch (const Error&) {
        return false;
    }

    template <typename FunT>
    void Flow::Functions<FunT>::check_formula(const Formula& phi) const
    {
        check_formula_head(phi);
        check_formula_body(phi);
    }

    template <typename FunT>
    bool Flow::Functions<FunT>::valid_formula_head(const Formula& phi) const noexcept
    try {
        return valid_formula_head_impl(phi);
    }
    catch (Ignore) {
        return true;
    }
    catch (const Error&) {
        return false;
    }

    template <typename FunT>
    void Flow::Functions<FunT>::check_formula_head(const Formula& phi) const
    {
        expect(valid_formula_head(phi), "Invalid formula head: "s + phi.to_string());
    }

    template <typename FunT>
    bool Flow::Functions<FunT>::
        valid_formula_body(const Formula& phi) const noexcept
    try {
        return valid_formula_body_impl(phi);
    }
    catch (Ignore) {
        return true;
    }
    catch (const Error&) {
        return false;
    }

    template <typename FunT>
    void Flow::Functions<FunT>::check_formula_body(const Formula& phi) const
    {
        expect(valid_formula_body(phi), "Invalid formula body: "s + phi.to_string());
    }

    template <typename FunT>
    bool Flow::Functions<FunT>::valid_formula_head_impl(const Formula& phi) const
    {
        return !std::empty(phi.coper()) && !phi.empty();
    }

    /// Check if it contains no key other than in `Keys`
    template <typename FunT>
    bool Flow::Functions<FunT>::valid_formula_body_impl(const Formula& phi) const
    {
        return all_of(phi, [this](auto& eptr){
            if (Formula::is_me(eptr)) {
                auto& subphi = Formula::cast(eptr);
                return This::valid_formula_head_impl(subphi)
                    && This::valid_formula_body_impl(subphi);
            }
            assert(!List::is_me(eptr));
            if (!is_key(eptr)) {
                assert(is_value(eptr));
                return true;
            }

            /*! if it ret. false here because of const. argument not found,
            it will "just" not be recogn. as inv. by Flow_def,
            but it should fail immediately! */
            return ckeys().contains(cast_key(eptr));
        });
    }

    template <typename FunT>
    bool Flow::Functions<FunT>::valid_fun_key(const Key& key_) const noexcept
    {
        return is_special_key(key_) || ckeys().contains_ode_key(key_);
    }

    template <typename FunT>
    void Flow::Functions<FunT>::check_fun_key(const Key& key_) const
    {
        using unsot::to_string;
        expect(valid_fun_key(key_),
               "Invalid key of function variants: "s + to_string(key_));
    }

    template <typename FunT>
    bool Flow::Functions<FunT>::
        contains_any_valid_fun_key(const Formula& phi) const noexcept
    {
        if (phi.empty()) return false;
        return any_of(phi, [this](auto& eptr){
            return contains_any_valid_fun_key(eptr);
        });
    }

    template <typename FunT>
    bool Flow::Functions<FunT>::contains_any_valid_fun_key(const expr::Ptr& eptr) const noexcept
    {
        if (is_key(eptr)) return valid_fun_key(cast_key(eptr));
        if (Elem_base::is_me(eptr)) return false;

        try {
            for_each_if_key_rec(List::cast(eptr), as_ckeys([this](auto& key_){
                if (valid_fun_key(key_)) throw ignore;
            }));
            return false;
        }
        catch (Ignore) { return true; }
    }

    template <typename FunT>
    const Key& Flow::Functions<FunT>::get_fun_key(const Formula& phi) const noexcept
    {
        assert(This::valid_formula_head_impl(phi));
        /// Take into account only the first argument
        auto& front = phi.cfront();
        if (is_key(front)) return cast_key(front);
        if (is_atom_formula(phi) || !Formula::is_me(front)) {
            assert(Formula::is_me(front) || !List::is_me(front));
            return global_key;
        }

        return get_fun_key(Formula::cast(front));
    }

    template <typename FunT>
    const Key& Flow::Functions<FunT>::get_fun_key_check(const Formula& phi) const
    {
        check_formula_head(phi);
        return get_fun_key(phi);
    }

    template <typename FunT>
    bool Flow::Functions<FunT>::all_get_fun_keys_equal(const Formula& phi) const noexcept
    {
        if (is_atom_formula(phi)) return true;

        View<const Formula*> atoms;
        for_each_if_formula_rec_pre(phi, as_cformulas([&atoms](auto& subphi){
            atoms.push_back(&subphi);
        }), BIND_THIS(is_atom_formula));

        return all_equal(atoms, wrap(equal_to<Key>(), BIND_THIS(get_fun_key)));
    }

    template <typename FunT>
    void Flow::Functions<FunT>::add_formula(Formula phi)
    {
        Fun fun_ = move(phi);
        init_fun(fun_);
        funs().emplace_back(move(fun_));
    }

    template <typename FunT>
    bool Flow::Functions<FunT>::valid_id(const Id& id) const noexcept
    {
        const Id size_ = size();
        return id >= 0 && id < size_;
    }

    template <typename FunT>
    void Flow::Functions<FunT>::check_id(const Id& id) const
    {
        using unsot::to_string;
        expect(valid_id(id), "Invalid ID of function variants: "s + to_string(id));
    }

    template <typename FunT>
    Real Flow::Functions<FunT>::operator ()(const Reals& state, const Id& id) const
    {
        return invoke(cfun(id), state);
    }

    template <typename FunT>
    String Flow::Functions<FunT>::to_string() const&
    {
        return head_to_string() + body_to_string();
    }

    template <typename FunT>
    String Flow::Functions<FunT>::body_to_string() const
    {
        String str("\n");
        for (Id size_ = size(), id_ = 0; id_ < size_; ++id_) {
            str += "  " + fun_to_string(id_) + "\n";
        }
        return str;
    }

    template <typename FunT>
    String Flow::Functions<FunT>::fun_to_string(const Id& id_) const
    {
        return cfun(id_).to_string();
    }

    template <typename FunT>
    bool Flow::Functions<FunT>::lequals(const This& rhs) const noexcept
    {
        return equals_keys(rhs.ckeys()) && equals_funs(rhs.cfuns());
    }

    template <typename FunT>
    bool Flow::Functions<FunT>::equals_keys(const Keys& keys_) const noexcept
    {
        return ckeys() == keys_;
    }

    template <typename FunT>
    bool Flow::Functions<FunT>::equals_funs(const Funs& funs_) const noexcept
    {
        return cfuns() == funs_;
    }
}
