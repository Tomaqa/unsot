#include "ode/flow.hpp"
#include "ode/flow/timer.hpp"

namespace unsot::ode::flow::aux {
    using Timer = Flow::Timer;

    Timer::Timer(const Keys& keys_)
        : Timer(keys_, Formulas{}, Formulas{})
    { }

    Timer::Timer(const Keys& keys_, Formula tau_phi, Formula t_phi)
        : Timer(keys_, Formulas{move(tau_phi)}, Formulas{move(t_phi)})
    { }

    Timer::Timer(const Keys& keys_, Formulas tau_phis, Formulas t_phis)
        : _tau_invs(Local_invariants::cons(keys_, tau_key, move(tau_phis))),
          _t_invs(Local_invariants::cons(keys_, t_key, move(t_phis)))
    {
        //+ set_t_init(state);
        //+ set_t_end(state);
    }

    //+ Time Flow::time_len() const noexcept
    //+ {
    //+     return min(0., ct_end() - ct_init());
    //+ }

    //+ void Flow::set_t_init(const State& state) const
    //+ {
    //+     t_init() = state.ct();
    //+ }

    //+ void Flow::set_t_end(const State& state) const
    //+ {
    //+     t_end() = min(ct_end(), state.ct()+ctau_end()) ..
    //+     podle podminek na _tau; ty mohou zaviset i na stavu
    //+     -> ruzne varianty stejne jako v ODE invariantech
    //+ }

    bool Timer::valid_ids(const Id& tau_id, const Id& t_id) const noexcept
    {
        return ctau_invs().valid_id(tau_id) && ct_invs().valid_id(t_id);
    }

    void Timer::check_ids(const Id& tau_id, const Id& t_id) const
    {
        using unsot::to_string;
        expect(valid_ids(tau_id, t_id),
               "Invalid timer ids (tau, t): "s
               + "(" + to_string(tau_id) + ", " + to_string(t_id) + ")");
    }

    bool Timer::operator ()(const Reals& state,
                            const Id& tau_id, const Id& t_id) const
    {
        //! if (state.ct() > ct_end()) return false;
        return invoke(ctau_invs(), state, tau_id)
            && invoke(ct_invs(), state, t_id);
    }

    String Timer::to_string() const&
    {
        return ctau_invs().to_string() + ct_invs().to_string();
    }

    bool Timer::equals(const This& rhs) const noexcept
    {
        return ctau_invs() == rhs.ctau_invs()
            && ct_invs() == rhs.ct_invs();
    }
}
