#include "ode/flow.hpp"
#include "ode/flow/parse.hpp"

#include "util/string/alg.hpp"
#include "util/alg.hpp"
#include "expr/alg.hpp"
#include "expr/pos.hpp"

namespace unsot::ode::flow {
    using namespace expr;

    Parse::Parse(Keys keys_)
        : _keys(move(keys_)), _invs(ckeys()), _odes(ckeys())
    { }

    Parse::Parse(Keys keys_, List odes_ls, List invs_ls)
        : Parse(move(keys_))
    {
        parse_body(move(odes_ls), move(invs_ls));
    }

    Parse::Parse(Keys keys_, List ls)
        : Parse(move(keys_))
    {
        parse_body(move(ls));
    }

    Parse::Parse(List ls)
        : Parse(parse_keys(ls))
    {
        parse_body(move(ls));
    }

    Keys Parse::parse_keys(List& ls)
    {
        expect(!ls.empty() && List::is_me(ls.cfront()),
               "Expected subexpression with Keys description, got: "s
               + ls.to_string());
        //+ expect(!ls.empty() && is_deep(ls),
        //+        "Expected expressions with Keys and flow descriptions, "s
        //+        + "or only flow description, got: "s + ls.to_string());

        List keys_ls = List::cast(move(ls.front()));
        ls.pop_front();
        return Keys::parse(move(keys_ls));
    }

    namespace {
        inline Formula maybe_cons_formula(expr::Ptr&& eptr)
        {
            auto& subls = List::cast(eptr);
            Formula phi;
            if (!subls.empty()) {
                phi = Formula::is_me(eptr) ? Formula::cast(eptr)
                                           : Formula::cons(move(subls));
            }
            return phi;
        }
    }

    void Parse::parse_body(List&& ls)
    {
        ls.emerge();
        if (is_deep_n(ls, 2)) {
            /// It cannot have lower size - thanks to `emerge'
            expect(ls.size() == 2,
                   "Expected ODEs and invariants expressions "s
                   + "with subexpr., got: " + ls.to_string());
            Pos pos(ls);
            List odes_ls = move(get_list(pos));
            List invs_ls = peek_list(move(pos));
            return parse_body(move(odes_ls), move(invs_ls));
        }

        if (!is_deep(ls)) return parse_body(move(ls), {});

        Formulas odes_phis;
        Formulas invs_phis;
        for (auto&& eptr : move(ls)) {
            Formula phi = maybe_cons_formula(move(eptr));
            auto& phis = valid_ode_head(phi) ? odes_phis : invs_phis;
            phis.emplace_back(move(phi));
        }

        parse_body(move(odes_phis), move(invs_phis));
    }

    All_formulas Parse::parse_odes(List ls) const
    {
        if (!is_deep(ls)) ls.nest();

        expect(!is_deep_n(ls, 2),
               "Invalid ODEs expression: "s + ls.to_string());

        return parse_odes(to<Formulas>(move(ls), maybe_cons_formula));
    }

    All_formulas_map Parse::parse_invs(List ls) const
    {
        if (!is_deep(ls)) ls.nest();
        return parse_invs(to<Formulas>(move(ls), maybe_cons_formula));
    }

    All_formulas Parse::parse_odes(Formulas odes_phis) const
    {
        using unsot::to_string;

        const int count = odes_phis.size();
        const int ode_size = ckeys().ode_size();

        expect(count >= ode_size,
               "Size of ODE formulas mismatch: expected at least "s
               + to_string(ode_size) + ", got: " + to_string(count));

        All_formulas all_odes_phis;
        all_odes_phis.resize(ode_size);
        const size_t res_size = max(4, 1 + count/ode_size);
        for (auto& ophis : all_odes_phis) ophis.reserve(res_size);

        for (auto&& ophi : move(odes_phis)) {
            ophi.maybe_virtual_init();
            check_ode_head(ophi);
            const auto okey_v = get_ode_key_view(ophi);
            auto& ophis = all_odes_phis[ckeys().ckey_id(okey_v)];
            ophis.emplace_back(to_ode_formula(move(ophi)));
        }

        return all_odes_phis;
    }

    All_formulas_map Parse::parse_invs(Formulas invs_phis) const
    {
        for (auto& iphi : invs_phis) {
            if (iphi.empty()) continue;
            iphi.maybe_virtual_init();
            check_inv_head(iphi);
            iphi = to_inv_formula(move(iphi));
        }

        return divide_invs_formulas(move(invs_phis));
    }

    All_formulas_map
    Parse::divide_invs_formulas(Formulas invs_phis) const
    {
        All_formulas_map invs_map;
        int res_size = std::size(invs_phis)/4;
        for_each_inv_key(ckeys(),
            [&invs_map, CAPTURE_RVAL(res_size)](auto& key_){
                invs_map[key_].reserve(max(4, res_size));
        });

        for (auto&& iphi : move(invs_phis)) {
            if (iphi.empty()) continue;
            iphi.maybe_virtual_init();
            const auto& ikey = get_inv_key_check(iphi);
            invs_map[ikey].emplace_back(move(iphi));
        }

        return invs_map;
    }

    //+ Perform the parse lazily - only here, not in constructor
    Flow Parse::perform() const&
    {
        return {ckeys(), call_odes_formulas(), call_invs_formulas_map()};
    }

    //+ Perform the parse lazily - only here, not in constructor
    Flow Parse::perform()&&
    {
        return {move(keys()), move(all_odes_formulas()),
                move(all_invs_formulas_map())};
    }

    bool Parse::valid_inv(const Formula& phi) const noexcept
    {
        return valid_inv_head(phi) && valid_inv_body(phi);
    }

    void Parse::check_inv(const Formula& phi) const
    {
        check_inv_head(phi);
        check_inv_body(phi);
    }

    bool Parse::valid_ode(const Formula& phi) const noexcept
    {
        return valid_ode_head(phi) && valid_ode_body(phi);
    }

    void Parse::check_ode(const Formula& phi) const
    {
        check_ode_head(phi);
        check_ode_body(phi);
    }

    bool Parse::valid_inv_head(const Formula& phi) const noexcept
    {
        return (Flow::Invariants::Fun::Opers::valid_pred(phi)
                || Flow::Invariants::Fun::Opers::valid_bool(phi))
            && cinvs().valid_formula_head(phi);
    }

    void Parse::check_inv_head(const Formula& phi) const
    {
        Flow::Invariants::Fun::Opers::check(phi);
        cinvs().check_formula_head(phi);
    }

    bool Parse::valid_inv_body(const Formula& phi) const noexcept
    {
        assert(valid_inv_head(phi));
        return cinvs().valid_formula_body(phi);
    }

    void Parse::check_inv_body(const Formula& phi) const
    {
        cinvs().check_formula_body(phi);
    }

    bool Parse::valid_ode_head(const Formula& phi) const noexcept
    {
        if (phi.size() != 2 || phi.coper() != "="
            || !is_derived_key(get_derived_key(phi))
            || !valid_ode_key(get_ode_key_view(phi))
        ) return false;

        return true;
    }

    void Parse::check_ode_head(const Formula& phi) const
    {
        expect(valid_ode_head(phi),
               "Invalid head format of ODE: "s + phi.to_string());
    }

    bool Parse::valid_ode_body(const Formula& phi) const noexcept
    {
        assert(valid_ode_head(phi));
        auto& eptr = phi.cback();
        if (Elem_base::is_me(eptr)) return true;
        if (!Formula::is_me(eptr)) return false;
        auto& ophi = Formula::cast(eptr);
        return Flow::Odes::Fun::Opers::valid_f(ophi)
            && codes().valid_formula(ophi);
    }

    void Parse::check_ode_body(const Formula& phi) const
    {
        expect(valid_ode_body(phi),
               "Invalid body format of ODE: "s + phi.to_string());
    }

    const Key& Parse::get_inv_key(const Formula& phi) const noexcept
    {
        auto& inv_key = cinvs().get_fun_key(phi);
        if (is_global_key(inv_key)) return inv_key;
        if (valid_inv_key(inv_key)
            && cinvs().all_get_fun_keys_equal(phi)) return inv_key;
        return global_key;
    }

    const Key& Parse::get_inv_key_check(const Formula& phi) const
    {
        check_inv_head(phi);
        return get_inv_key(phi);
    }

    const Key& Parse::get_derived_key(const Formula& phi) const noexcept
    {
        return codes().get_fun_key(phi);
    }

    const Key& Parse::get_derived_key_check(const Formula& phi) const
    {
        check_ode_head(phi);
        return get_derived_key(phi);
    }

    Key Parse::get_ode_key(const Formula& phi) const noexcept
    {
        return to_ode_key(get_derived_key(phi));
    }

    Key Parse::get_ode_key_check(const Formula& phi) const
    {
        return to_ode_key_check(get_derived_key_check(phi));
    }

    String_view Parse::get_ode_key_view(const Formula& phi) const noexcept
    {
        return to_ode_key_view(get_derived_key(phi));
    }

    String_view Parse::get_ode_key_view_check(const Formula& phi) const
    {
        return to_ode_key_view_check(get_derived_key_check(phi));
    }

    Formula Parse::to_ode_formula(Formula phi)
    {
        auto& eptr = phi.back();
        if (List::is_me(eptr)) return Formula::cast(move(eptr));
        return Formula("+", List(move(eptr)));
    }

    Formula Parse::to_inv_formula(Formula phi)
    {
        return phi;
    }
}

////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////

namespace unsot::ode::flow {
    bool is_derived_key(String_view view) noexcept
    {
        return ends_with(view, derived_key_suffix);
    }

    void check_is_derived_key(String_view view)
    {
        expect(is_derived_key(view),
               "Expected ODE derivative key, got: "s + to_string(view));
    }

    void check_is_not_derived_key(String_view view)
    {
        expect(!is_derived_key(view),
               "Unexpected ODE derivative key: "s + to_string(view));
    }

    bool ode_key_corresponds_to(const Key& ode_key,
                                const Key& derived_key) noexcept
    {
        if (size(ode_key) != size(derived_key) - derived_key_suffix_size)
            return false;
        return starts_with(derived_key, ode_key);
    }

    void check_ode_key_corresponds_to(const Key& ode_key,
                                      const Key& derived_key)
    {
        expect(ode_key_corresponds_to(ode_key, derived_key),
               "ODE key does not correspond to its derivative: "s
               + to_string(ode_key) + " <-> " + to_string(derived_key));
    }

    bool is_derived_key_corresponding_to(const Key& derived_key,
                                         const Key& ode_key) noexcept
    {
        return is_derived_key(derived_key)
            && ode_key_corresponds_to(ode_key, derived_key);
    }

    void check_is_derived_key_corresponding_to(const Key& derived_key,
                                               const Key& ode_key)
    {
        check_is_derived_key(derived_key);
        check_ode_key_corresponds_to(ode_key, derived_key);
    }

    Key to_derived_key(const Key& ode_key) noexcept
    {
        return ode_key + derived_key_suffix;
    }

    Key to_derived_key_check(const Key& ode_key)
    {
        check_is_not_derived_key(ode_key);
        return to_derived_key(ode_key);
    }

    Key to_ode_key(const Key& derived_key) noexcept
    {
        const size_t key_size = size(derived_key) - derived_key_suffix_size;
        return derived_key.substr(0, key_size);
    }

    Key to_ode_key_check(const Key& derived_key)
    {
        check_is_derived_key(derived_key);
        return to_ode_key(derived_key);
    }

    String_view to_ode_key_view(const Key& derived_key) noexcept
    {
        String_view view(derived_key);
        view.remove_suffix(derived_key_suffix_size);
        return view;
    }

    String_view to_ode_key_view_check(const Key& derived_key)
    {
        check_is_derived_key(derived_key);
        return to_ode_key_view(derived_key);
    }
}
