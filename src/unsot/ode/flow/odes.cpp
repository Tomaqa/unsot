#include "ode/flow.hpp"
#include "ode/flow/odes.hpp"

namespace unsot::ode::flow::aux {
    using Odes = Flow::Odes;

    Odes::Odes(const Keys& keys_, Key key_, Formulas phis, Formula def_phi)
        : Inherit(keys_, move(key_), move(phis), move(def_phi))
    { }

    bool Odes::valid_funs() const noexcept
    {
        return valid_funs_nonempty() && Inherit::valid_funs();
    }

    void Odes::check_funs() const
    try {
        check_funs_nonempty();
        Inherit::check_funs();
    }
    catch (const Error& err) {
        using unsot::to_string;
        THROW("Invalid ODE formulas '") + to_string(cfuns()) + "'\n: " + err;
    }

    bool Odes::valid_funs_nonempty() const noexcept
    {
        return size() > 0;
    }

    void Odes::check_funs_nonempty() const
    {
        expect(valid_funs_nonempty(), "No ODE formula given.");
    }

    bool Odes::valid_fun_key(const Key& key_) const noexcept
    {
        return !is_special_key(key_) && Inherit::valid_fun_key(key_);
    }

    void Odes::check_fun_key(const Key& key_) const
    {
        using unsot::to_string;
        expect(valid_fun_key(key_), "Invalid ODE key: "s + to_string(key_));
    }
}
