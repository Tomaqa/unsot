#include "ode/flow.hpp"
#include "ode/flow/functions.hpp"

#include "ode/flow/functions.tpp"

namespace unsot::ode::flow::aux {
    template class Flow::Functions<Fun>;
    template class Flow::Functions<Pred>;
}
