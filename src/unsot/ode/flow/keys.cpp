#include "ode/flow.hpp"
#include "ode/flow/keys.hpp"

#include "util/string.hpp"
#include "util/alg.hpp"
#include "util/hash.hpp"
#include "expr/pos.hpp"
#include "ode/flow/parse.hpp"

namespace unsot::ode::flow {
    using namespace expr;

    Keys::Keys(expr::Keys ode_keys_, expr::Keys arg_keys_)
    {
        const size_t ode_size_ = std::size(ode_keys_);
        const size_t arg_size_ = std::size(arg_keys_);
        ode_keys_.reserve(ode_size_ + arg_size_ + special_args_size);
        move(arg_keys_, back_inserter(ode_keys_));

        ptr() = expr::new_keys(move(ode_keys_));
        init(ode_size_);
    }

    Keys::Keys(expr::Keys keys_, size_t ode_size_)
        : _ptr(expr::new_keys(move(keys_)))
    {
        init(ode_size_);
    }

    void Keys::init(size_t ode_size_)
    {
        set_subsizes(ode_size_);
        check();
        keys().push_back(t_key);
        keys().push_back(tau_key);
        set_ids_map();
    }

    void Keys::set_subsizes(size_t ode_size_)
    {
        using unsot::to_string;
        expect(ode_size_ <= size(),
               "Passed size of ODE keys is greater than total size: "s
               + to_string(ode_size_) + " > " + to_string(size()));
        _ode_size = ode_size_;
        _arg_size = size() - ode_size();
    }

    bool Keys::valid() const
    {
        return valid_subsizes()
            && valid_keys()
            && valid_ode_keys()
            && valid_unique()
            && valid_special();
    }

    void Keys::check() const
    {
        check_subsizes();
        check_keys();
        check_ode_keys();
        check_unique();
        check_special();
    }

    bool Keys::valid_subsizes() const noexcept
    {
        return ode_size() > 0
            && ode_size() + arg_size() == size();
    }

    void Keys::check_subsizes() const
    {
        using unsot::to_string;
        expect(valid_subsizes(),
               "Invalid argument keys (sub)sizes: "s
               + to_string(ode_size()) + ","
               + to_string(arg_size())
               + "; total: " + to_string(size()));
    }

    bool Keys::valid_keys() const noexcept
    {
        return all_of(ckeys(), valid_key);
    }

    void Keys::check_keys() const
    {
        for_each(ckeys(), check_key);
    }

    bool Keys::valid_key(const Key& key_) noexcept
    {
        return expr::valid_key(key_);
    }

    void Keys::check_key(const Key& key_)
    {
        using unsot::to_string;
        expect(valid_key(key_),
               "Invalid flow key: '"s + to_string(key_) + "'");
    }

    bool Keys::valid_ode_keys() const noexcept
    {
        return all_of(code_begin(), code_end(), valid_ode_key);
    }

    void Keys::check_ode_keys() const
    {
        for_each_ode_key(*this, check_ode_key);
    }

    /// In addition to 'valid_key'
    bool Keys::valid_ode_key(const Key& key_) noexcept
    {
        return !is_derived_key(key_);
    }

    void Keys::check_ode_key(const Key& key_)
    {
        using unsot::to_string;
        expect(valid_ode_key(key_),
               "Invalid flow ODE key: '"s + to_string(key_) + "'");
    }

    bool Keys::valid_unique() const
    {
        Hash<Key> s;
        return all_of(ckeys(), [&s](const Key& key){
            auto [_, inserted] = s.insert(key);
            return inserted;
        });
    }

    void Keys::check_unique() const
    {
        expect(valid_unique(),
               "Argument keys must be distinct: "s + ckeys().to_string());
    }

    bool Keys::valid_special() const noexcept
    {
        return none_of(ckeys(), is_special_key);
    }

    void Keys::check_special() const
    {
        expect(valid_special(),
               "Argument keys must not include special arguments: "s
               + ckeys().to_string());
    }

    Keys Keys::parse(List ls)
    try {
        Keys rhs;
        const size_t ode_size_ = rhs.parse_impl(move(ls));
        rhs.init(ode_size_);
        return rhs;
    }
    catch (const Error& err) {
        THROW("Invalid format of flow keys '") + ls.to_string() + "'\n: " + err;
    }

    size_t Keys::parse_impl(List&& ls)
    {
        const bool is_flat_ = is_flat(ls);
        Const_pos pos(ls);
        expect(is_flat_
               || (is_deep(ls) && !ls.empty()
                  && is_flat(get_list(pos))
                  && (ls.size() == 1
                     || (ls.size() == 2 && is_flat(get_list(pos))))
                  ),
               "Expected ODE keys, or one or two lists of keys: "s
               + "ODE (and optionally constant) flow keys.");

        const size_t ode_size_ = is_flat_ ? ls.size()
                                          : List::cast(ls.cfront()).size();
        if (!is_flat_) ls.flatten();

        ptr() = expr::new_keys(cast_to_keys(move(ls)));
        return ode_size_;
    }

    Key_id Keys::t_ode_key_id(const Key& key_) const
    {
        if (key_ == t_key) return 0;
        check_contains_ode_key(key_);
        //+ two lookups
        return ckey_id(key_)+1;
    }

    expr::Keys Keys::ct_ode_keys_slice() const
    {
        expr::Keys keys_;
        keys_.reserve(t_ode_size());
        keys_.push_back(t_key);
        copy(code_begin(), code_end(), back_inserter(keys_));
        return keys_;
    }

    expr::Keys Keys::t_ode_keys_slice()
    {
        expr::Keys keys_;
        keys_.reserve(t_ode_size());
        keys_.push_back(t_key);
        move(ode_begin(), ode_end(), back_inserter(keys_));
        return keys_;
    }

    bool Keys::contains(const Key& key_) const noexcept
    {
        return cids_map().contains(key_);
    }

    void Keys::check_contains(const Key& key_) const
    {
        using unsot::to_string;
        expect(contains(key_), "Unknown key: "s + to_string(key_));
    }

    bool Keys::contains_ode_key(const Key& ode_key) const noexcept
    {
        return util::contains(code_begin(), code_end(), ode_key);
    }

    void Keys::check_contains_ode_key(const Key& ode_key) const
    {
        using unsot::to_string;
        expect(contains_ode_key(ode_key),
               "Unknown ODE key: "s + to_string(ode_key));
    }

    bool Keys::contains_arg_key(const Key& arg_key) const noexcept
    {
        return util::contains(carg_begin(), carg_end(), arg_key);
    }

    void Keys::check_contains_arg_key(const Key& arg_key) const
    {
        using unsot::to_string;
        expect(contains_arg_key(arg_key),
               "Unknown argument key: "s + to_string(arg_key));
    }

    void Keys::set_arg_keys(expr::Keys arg_keys_)
    {
        *this = {ode_keys_slice(), move(arg_keys_)};
    }

    bool Keys::valid_state(const State& state) const noexcept
    {
        return ode_size() == state.ode_size()
            && arg_size() == state.arg_size();
    }

    void Keys::check_state(const State& state) const
    {
        expect(valid_state(state),
               "Invalid flow state: " + state.to_string());
    }

    String Keys::to_string() const&
    {
        using unsot::to_string;
        String str(to_string(code_keys_slice()));
        if (arg_size() > 0) {
            str += "| " + to_string(carg_keys_slice());
        }
        return str;
    }

    bool Keys::equals(const This& rhs) const noexcept
    {
        return ckeys() == rhs.ckeys();
    }
}
