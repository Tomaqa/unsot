#include "ode/solver.hpp"

#include "util/numeric/alg.hpp"

namespace unsot::ode::solver {
    using numeric::apx_equal;
    using numeric::def_rel_eps;
    using numeric::def_abs_eps;

    Base::Base(Flow flow_)
        : Base(Flows{move(flow_)})
    { }

    Base::Base(Flows flows_)
    {
        reserve(std::size(flows_));
        //! `add_flow` is virtual!
        //+ using `Dynamic` would require `to_string`
        for (auto&& flow_ : move(flows_)) add_flow(move(flow_));
    }

    Base::Base(initializer_list<Flow> list)
        : Base(Flows(move(list)))
    { }

    long Base::step_count() const noexcept
    {
        //! return cflow().time_len()/cstep_size();
        return 1e3/cdt();
    }

    void Base::reserve(size_t size_)
    {
        flow_ids().reserve(size_);
        flow_ptrs().reserve(size_);
        traject_ptrs().reserve(size_);
    }

    flow::Id Base::add_flow(Flow flow_)
    {
        flow_ptrs().emplace_back(new_flow(move(flow_)));
        add_traject();
        flow::Id fid = std::size(cflow_ptrs());
        flow_ids().push_back(fid);
        return fid;
    }

    flow::Id Base::add_def_flow()
    {
        return add_flow({});
    }

    void Base::add_traject(Traject traject_)
    {
        traject_ptrs().emplace_back(new_traject(move(traject_)));
    }

    void Base::add_traject()
    {
        add_traject({cflow_ptrs().back()});
    }

    State Base::solve(const flow::Id& fid, const State& in,
                      const Config& config) const
    {
        State out(in);
        solve(fid, out, config);
        return out;
    }

    void Base::solve(const flow::Id& fid, State& state,
                     const Config& config) const
    {
        check_input(fid, state, config);
        const long max_ = 1e4;
        traject(fid).reset(min(max_, step_count()));
        eval(fid, state, config);
    }

    State Base::solve(const flow::Id& fid, List ls) const
    {
        expect(ls.size() == 2 && is_deep(ls),
               "Expected two expressions of flow initial values (State)"s
               + " and chosen flow configuration (Config).");

        const auto state = State::parse(List::cast(move(ls.front())));
        const auto config = Config::parse(List::cast(move(ls.back())));
        return solve(fid, state, config);
    }

    void Base::check_input(const flow::Id& fid, const State& state,
                           const Config& config) const
    {
        cflow(fid).check_input(state, config);
    }

    bool Base::invariant(const flow::Id& fid, const State& state,
                         const Config& config) const
    {
        return cflow(fid).invariant(state, config);
    }

    bool Base::invariant_check(const flow::Id& fid, const State& state,
                               const Config& config) const
    {
        check_input(fid, state, config);
        return invariant(fid, state, config);
    }

    void Base::do_steps(const flow::Id& fid, State& state, State& tmp,
                        const Real dt_, const Config& config) const
    {
        while (invariant(fid, state, config)) {
            do_step(fid, state, tmp, dt_, config);
            tmp.advance_t(state, dt_);
            traject(fid).add_step(state);
            state.swap(tmp);
        }
        if (kept_invariant()) {
            state.swap(tmp);
        }
    }

    void Base::do_step_check(const flow::Id& fid, const State& in, State& out,
                             const Real dt_, const Config& config) const
    {
        check_input(fid, in, config);
        check_input(fid, out, config);
        do_step(fid, in, out, dt_, config);
    }

    void Base::eval(const flow::Id& fid, State& state,
                    const Config& config) const
    {
        static constexpr auto stop_f = [](const Real dt_){
            //+ initial dt must affect the precision
            const double rel_eps = pow(def_rel_eps, 1.25);
            const double abs_eps = pow(def_abs_eps, 1.5);
            return apx_equal(0., dt_, rel_eps, abs_eps);
        };

        State& x = state;
        if (is_end(fid, x, config)) return;

        constexpr double interpol_coef = 1./4.;
        for (auto [tmp, h] = tuple(x, cdt());; h *= interpol_coef) {
            do_steps(fid, x, tmp, h, config);
            if (stop_f(h)) break;
            if (!kept_invariant()) {
                state.swap(tmp);
            }
        }
    }

    bool Base::is_almost_end(const flow::Id& fid, const State& state,
                             const Config& config) const
    {
        if (is_end_check(fid, state, config)) return true;
        State next(state);
        do_step(fid, state, next, cdt(), config);
        next.advance_t(state, cdt());
        return is_end(fid, next, config);
    }
}
