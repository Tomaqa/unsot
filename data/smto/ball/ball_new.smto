;;;; Bouncing ball from dReal in SMT+ODE format
;; `(x, v)' is continuous state
;; `x' denotes vertical position
;; `v' denotes either positive or negative velocity

;;; ODEs definition

#define G 9.8
#define D 0.1

(define-ft F    () _1)

(define-ft Dv   () (+ -#G (* _1 #D (^ v 2)) ))

(declare-odes (x v) ())
(add-dt-flow x F dx (v))
(add-dt-flow v Dv dv_down ( 1))
(add-dt-flow v Dv dv_up   (-1))

;;; ODE invariants

#define X_MAX() 15
#define V_MAX() 18

(add-ode-invariant  x       (>= x  0))
(add-ode-invariant  x       (<= x  #X_MAX))
(add-ode-invariant  v       (>= v -#V_MAX))
(add-ode-invariant  v       (<= v  #V_MAX))
(add-flow-invariant dv_up   (>= v  0))
(add-flow-invariant dv_down (<= v  0))

;;; Locations definition

(define-loc down ((v dv_down)))
(define-loc up   ((v dv_up))  )

;;; Jumps definition

#define K 0.95

; (define-jump down (>  x 0) down (and (= x' x) (= v' v) ))
  (define-jump down (<= x 0) up   (and (= x' 0) (= v' (* -#K v)) ))
; (define-jump up   (>  v 0) up   (and (= x' x) (= v' v) ))
; (define-jump up   (<= v 0) down (and (= x' x) (= v' 0) ))
  (define-jump up   (<= v 0) down               (= v' 0)  )

;;; Initializations

(init-t 0)

(init-ode x ( 5 #X_MAX 0.25))
(init-ode v   0)

(init-loc down)

;;; Evaluation

(eval-odes (x v) ())
