;;;; Car 2D model in SMT+ODE format
;; `(left, right, acc, brake)' is discrete state
;; `(x, y, v, a, om)' is continuous state

#include data/expr/stdlib.expr

;;;; Main parameters

#ifdef ONLINE
(set-option :t-strategy car)
#endif

#ifndef WIDTH
#define WIDTH 2
#endif
#ifndef LENGTH
#define LENGTH 10
#endif

#ifndef CAR_SIZE
#define CAR_SIZE 0.25
#endif
#define CAR_SIZE/2 $(/ #CAR_SIZE 2)

#ifndef MAX_T
#define MAX_T 20
#endif

#ifndef TAU
#define TAU 0.5
#endif

#ifndef DT
#define DT 5e-2
#endif

#ifndef GOAL_T_COEF
#define GOAL_T_COEF 0.8
#endif
#assert (and (> #GOAL_T_COEF 0) (<= #GOAL_T_COEF 1))
#define GOAL_T $(* #MAX_T #GOAL_T_COEF)

#define STEPS $d(+ $(/ #MAX_T #TAU))

;;;; Bools definition

#set Main_bools (left right acc brake rapid_acc)
#DECL_BOOL_CONSTS

;; `acc & !brake' <=> accelerate forward
;; `acc & brake' <=> decelerate
;; `rapid_acc & acc' <=> accelerate rapidly (or decelerate)

#define ACC_IDLE_MODE(    acc brake rapid_acc )      (not #acc)
#define ACC_MODE(         acc brake rapid_acc ) (and      #acc  (not #brake)  (not #rapid_acc))
#define RAPID_ACC_MODE(   acc brake rapid_acc ) (and      #acc  (not #brake)       #rapid_acc )
#define BRAKE_MODE(       acc brake rapid_acc ) (and      #acc       #brake   (not #rapid_acc))
#define RAPID_BRAKE_MODE( acc brake rapid_acc ) (and      #acc       #brake        #rapid_acc )

#define STEER_IDLE_MODE(  left right ) (and (not #left) (not #right))
#define STEER_LEFT_MODE(  left right )           #left
#define STEER_RIGHT_MODE( left right )                       #right

(assert (and
    #for (i #FIRST_STEP #LAST_STEP)
        (not (and #AT(left #i) #AT(right #i)))
        (=> (not #AT(acc #i)) (not #AT(brake #i)))
        (=> (not #AT(acc #i)) (not #AT(rapid_acc #i)))
    #endfor
))

;;;; Reals definition

#set Main_odes (x y v om)
#set Main_reals (a)
#DECL_REAL_CONSTS

(declare-const t_middle Real)
(declare-const t_finish Real)

;;;; Initializations

#define INIT_X $(/ #WIDTH 2)
#define INIT_Y $(+ #WIDTH (/ #LENGTH 2))
#define OUT_X_0 0
#define OUT_X_1 $(+ #OUT_X_0 (* #WIDTH 2) #LENGTH)
#define OUT_Y_0 #OUT_X_0
#define OUT_Y_1 #OUT_X_1
#define IN_X_0 $(+ #OUT_X_0 #WIDTH)
#define IN_X_1 $(+ #IN_X_0 #LENGTH)
#define IN_Y_0 #IN_X_0
#define IN_Y_1 #IN_X_1

(assert (and  (= #FIRST_T 0) (= #FIRST(v) 0) (= #FIRST(om) #PI/2)
              (= #FIRST(x) #INIT_X) (= #FIRST(y) #INIT_Y)
              (= #LAST(a) 0)
))

;;;; Flows definition

#define OM #PI/2

#define MIN_A $(* #g -2)
#define MAX_A $(* #g  1)
#define MIN_A/2 $(/ #MIN_A 2)
#define MAX_A/2 $(/ #MAX_A 2)

#def OM_V_COEF()
#let thres #MAX_A
    (+ (< v #thres) (* (>= v #thres) (/ (* 2 #thres) (+ v #thres)) ) )
#endlet thres
#enddef

#define MAX_V 60

(set-option :dt #DT)
(set-option :keep-flow-invariant true)
(set-option :require-flow-progress true)

(define-flow f #ODES #FLOW_DEF_ARGS (and
    ;; ODEs
    (= x' (* v (cos om)))
    (= y' (* v (sin om)))
    (= v' &a)
    (= #ACC_IDLE_MODE   (&acc &brake &rapid_acc) (= &a         0) )
    (= #ACC_MODE        (&acc &brake &rapid_acc) (= &a  #MAX_A/2) )
    (= #RAPID_ACC_MODE  (&acc &brake &rapid_acc) (= &a    #MAX_A) )
    (= #BRAKE_MODE      (&acc &brake &rapid_acc) (= &a  #MIN_A/2) )
    (= #RAPID_BRAKE_MODE(&acc &brake &rapid_acc) (= &a    #MIN_A) )
    (= #STEER_IDLE_MODE (&left &right) (= om'                      0)  )
    (= #STEER_LEFT_MODE (&left &right) (= om' (*     #OM  #OM_V_COEF)) )
    (= #STEER_RIGHT_MODE(&left &right) (= om' (* $(- #OM) #OM_V_COEF)) )
    (=> (~ &v 0) (and (not &brake) #STEER_IDLE_MODE(&left &right) ))
    ;; (Adding constraints on braking etc. after finishing does not work well)
    ;; Invariants
    (< _tau #TAU)
    (and (>= v      0) (<= v #MAX_V))
    (and (> x (+ #OUT_X_0 #CAR_SIZE/2)) (< x (- #OUT_X_1 #CAR_SIZE/2)))
    (and (> y (+ #OUT_Y_0 #CAR_SIZE/2)) (< y (- #OUT_Y_1 #CAR_SIZE/2)))
    (or (< x (- #IN_X_0 #CAR_SIZE/2)) (> x (+ #IN_X_1 #CAR_SIZE/2))
        (< y (- #IN_Y_0 #CAR_SIZE/2)) (> y (+ #IN_Y_1 #CAR_SIZE/2))
    )
    ;; Jumps
    #ODE_NO_JUMPS
    (~ #FINAL_T #FINAL_PARAM_T)
))

;;;; Flows instantiation

#ASSERT_FLOWS

;;;; Additional constraints

#def MIDDLE(i)
    (and (> #AT(x #i) #IN_X_1) (< #AT(y #i)  #INIT_Y) )
#enddef

#def FINISH(i)
    (and (< #AT(x #i) #IN_X_0) (> #AT(y #i)  #INIT_Y) )
#enddef

(assert (and  #for (i #FIRST_STEP+1 #LAST_STEP)
                  (= #T_AT(#i) (* #i #TAU))
              #endfor

              ;; The car crosses the middle point
              (or #for (i #FIRST_STEP+1 #LAST_STEP-1)
                  #MIDDLE(#i)
              #endfor)
              ;; Set the middle time
              #for (i #FIRST_STEP+1 #LAST_STEP-1)
                  (=> (and #MIDDLE(#i) (not #MIDDLE($d(- #i 1))) )
                      (= #T_AT(#i) t_middle)
                  )
              #endfor

              ;; The car approaches the finish line AFTER the middle
              (or #for (i $d(+ #FIRST_STEP 2) #LAST_STEP)
                  (and (> #T_AT(#i) t_middle) #FINISH(#i))
              #endfor)
              ;; Set the finish time
              #for (i $d(+ #FIRST_STEP 2) #LAST_STEP)
                  (=> (and (> #T_AT(#i) t_middle) #FINISH(#i) (not #FINISH($d(- #i 1))) )
                      (= #T_AT(#i) t_finish)
                  )
              #endfor

              (~ #LAST(v) 0)
              (<= t_finish #GOAL_T)
))

;;;; Results

(check-sat)

#GET_VALUES
(get-value (t_middle t_finish))
