;;;; Standard library with basic macros

#ifndef __STDLIB_EXPR
#define __STDLIB_EXPR

;;; Physical constants

#define PI 3.14159265
#define PI/2 $(/ #PI 2)
#define PI2 $(* #PI 2)

#define g 9.780326

;;; Control macros

#define exit #error EXIT

;++ the condition is actually always evaluated, so it affects performance
#def assert*(cond)
#ifndef DEBUG
#elif #cond
#else #error (Debug assertion failed: #cond)
#endif
#enddef

;;; Null macros

#def null*(x)
#if #numberp(#x) $d(not #x)
#else #null(#x)
#endif
#enddef

;;; Logical macros

#let Bool_operators (and or xor => =)

;; "Exclusive" ite operator
;; -> requires the other branch to be false
#define xite(cond a b) (and (= #cond #a) (= (not #cond) #b))

#def EQUALS(args value)
#if #listp^(#args)
#else #set args (#args)
#endif
#for (const #args)
    (= #const #value)
#endfor
#enddef

;;; Arithmetic macros

; #define +1(x) $d(+ #x 1)
; #define -1(x) $d(- #x 1)

#let Arith_comparators (< > <= >= =)
#let Apx_comparators (<< >> <~ >~ ~)

#define ARITH_IF(cond a) (* #cond #a)
#def ARITH_ITE(cond a b)
    (+ (* #cond #a) (* (not #cond) #b) )
#enddef

#def INTERVAL_STEP(const min max step)
#if (= #max #min)
#elif (>= #max #min) #assert (> #step 0)
#else #assert (< #step 0)
#endif
(or 0
#for (v #min (<~ v #max) (+ v #step))
    (= #const #v)
#endfor
)
#enddef

#def INTERVAL_COUNT(const min max count)
#assert (>= #count 0)
#let ((diff $(- #max #min)) (icount $d #count) step)
#if $d(= #icount 0) ;; nothing
#elif $d(= #icount 1) (= #const $(+ #min (/ #diff 2))) ;; middle
#else
#set step $(/ #diff (- #icount 1))
    #INTERVAL_STEP(#const #min #max #step)
#endif
#endlet
#enddef

#def INTERVAL_STEP_COUNT(const min step count)
#assert (> #count 0)
    #INTERVAL_COUNT(#const #min $(+ #min (* #step $d(- #count 1))) #count)
#enddef

;; Pass both intervals as sorted pairs
#def INTERVAL_OVERLAP(int1 int2)
#let ((int1_min #nth(0 #int1))
      (int1_max #nth(1 #int1))
      (int2_min #nth(0 #int2))
      (int2_max #nth(1 #int2)))
    (and (<~ #int1_min #int2_max) (>~ #int1_max #int2_min))
#endlet
#enddef

;;; Sequence macros

#define last-idx(expr) $d(- #len(#expr) 1)
#define last(expr) #nth(#last-idx(#expr) #expr)

#define caar(expr) #car(#car(#expr))
#define cadr(expr) #nth(1 #expr)
#define cdar(expr) #cdr(#car(#expr))
#define cddr(expr) #nthcdr(2 #expr)
#define caaar(expr) #car(#caar(#expr))
#define caadr(expr) #car(#cadr(#expr))
#define cadar(expr) #nth(1 #car(#expr))
#define caddr(expr) #nth(2 #expr)
#define cdaar(expr) #cdr(#caar(#expr))
#define cdadr(expr) #cdr(#cadr(#expr))
#define cddar(expr) #cddr(#car(#expr))
#define cdddr(expr) #nthcdr(3 #expr)

#def nth*(idx expr)
#if (< #idx #len(#expr)) #nth(#idx #expr)
#else #
#endif
#enddef

#def next(elem expr)
#if #null(#expr)
#elif #equal(#elem #car(#expr)) #cadr(#expr)
#else #next(#elem #cdr(#expr))
#endif
#enddef
#def nextcdr(elem expr)
#if (not #null(#expr))
#let rest #cdr(#expr)
#if #equal(#elem #car(#expr)) #rest
#else #nextcdr(#elem #rest)
#endif
#endlet rest
#endif
#enddef

#def prev(elem expr)
#if #null(#expr)
#elif #equal(#elem #car(#expr)) #_prev_elem
#else
#let _prev_elem #car(#expr)
    #prev(#elem #cdr(#expr))
#endlet _prev_elem
#endif
#enddef

;+ cannot check for a substring starting with numbers ...
#def _key_contains(key str)
#assert*(#keyp^(#key))
#assert*(#keyp^(#str))
#if #null^(#str) 1
#else
#let (c (found 0) (d_idx 0) (dlen #len(#str)))
#for (idx 0 #last-idx(#key))
#set c #nth(#idx #key)
    #if #found
    #elif #equal(#c #nth(#d_idx #str))
        #set d_idx $d(% (+ #d_idx 1) #dlen)
        #if (= #d_idx 0) #set found 1
        #endif
    #else #set d_idx 0
    #endif
#endfor
    #found
#endlet
#endif
#enddef
#def _list_contains(expr elem)
#assert*(#listp^(#expr))
#if #null^(#expr) 0
#elif #equal(#elem #car(#expr)) 1
#else #_list_contains(#cdr(#expr) #elem)
#endif
#enddef
#def contains(expr elem)
#assert (not #numberp^(#expr))
#if #keyp^(#expr) #_key_contains(#expr #elem)
#else #_list_contains(#expr #elem)
#endif
#enddef

;;; Key macros

#define keyp(key) (and (not #listp(#key)) (not #numberp(#key)) (not #refp(#key)))

#def keywordp(key)
#if (not #keyp(#key)) 0
#elif #null(#key) 0
#else #equal(#car(#key) :)
#endif
#enddef

#def crop-keyword(key)
#assert #keywordp(#key)
    #cdr(#key)
#enddef

#let _alphabet abcdefghijklmnopqrstuvwxyz
#let _ALPHABET ABCDEFGHIJKLMNOPQRSTUVWXYZ
#let (l u)
#for (i 0 #last-idx(#_alphabet))
#set l #nth(#i #_alphabet)
#set u #nth(#i #_ALPHABET)
#let _Dcase_of_#u #l
#let _Ucase_of_#l #u
#endfor
#endlet

#def downcase(key)
#let (res c)
#for (i 0 #last-idx(#key))
    #set c #nth(#i #key)
    #set res #res## #if #contains(#_ALPHABET #c) #(_Dcase_of_#c) #else #c #endif
#endfor
    #res
#endlet
#enddef
#def upcase(key)
#let (res c)
#for (i 0 #last-idx(#key))
    #set c #nth(#i #key)
    #set res #res## #if #contains(#_alphabet #c) #(_Ucase_of_#c) #else #c #endif
#endfor
    #res
#endlet
#enddef

#def split(key delim)
#assert #keyp^(#key)
#assert #keyp^(#delim)
#let (c word skipped (d_idx 0) (dlen #len(#delim)))
(#for (idx 0 #last-idx(#key))
#set c #nth(#idx #key)
    #if #equal(#c #nth(#d_idx #delim))
        #set d_idx $d(% (+ #d_idx 1) #dlen)
        #if (= #d_idx 0)
            #word
            #set word #
            #set skipped #
        #else
            #set skipped #skipped#c
        #endif
    #else
        #set word #word#skipped#c
        #set skipped #
        #set d_idx 0
    #endif
#endfor #word)
#endlet
#enddef

;;; List macros

#def unpack(expr)
#for (elem #expr)
    #elem
#endfor
#enddef

#define AS_PARAMS(args) &#args

#def PAIR_CONFLICT(bools)
#assert #listp(#bools)
#if (not #null(#bools))
#let ((b1 #car(#bools))
      (rest #cdr(#bools)))
#for (b #rest)
    (not (and #b1 #b))
#endfor
    #PAIR_CONFLICT(#rest)
#endlet
#endif
#enddef

;;; Let macros

#define varp(x) (or #refp(#x) #keyp(#x))

#define null^(x) #null(#^(x))
#define listp^(x) #listp(#^(x))
#define numberp^(x) #numberp(#^(x))
#define keyp^(x) #keyp(#^(x))
#define keywordp^(x) #keywordp(#^(x))

#def ^*(x)
#if #refp(#x) #^(x)
#else #x
#endif
#enddef

#def ref-name*(x)
#assert #varp(#x)
#if #refp(#x) #ref-name(#x)
#else #x
#endif
#enddef

#define _-(arg key) #ref-name*(#arg) ## -#key

#def isdef*(id)
#ifndef #id 0
#else $d(not #null(#(#id)))
#endif
#enddef

#def set*(id val)
#ifdef #id #set #id #val
#else      #let #id #val
#endif
#enddef

#def =(id val)
#assert*((not #null(#id)))
#if (or (not #listp(#val)) #null(#val))
    #set*(#id #val)
#elif #keywordp(#car(#val))
    #defobject(#id #val)
#else #defarray(#id #val)
#endif
#enddef

;;; Array macros

#define [](arr idx) #ref-name*(#arr) ## [#idx##]
#define []!(arr idx) #(#[](#arr #idx))
#define [,](arr idx1 idx2) #[](#[](#arr #idx1) #idx2)
#define [,]!(arr idx1 idx2) #(#[,](#arr #idx1 #idx2))
#define [,,](arr idx1 idx2 idx3) #[](#[,](#arr #idx1 #idx2) #idx3)
#define [,,]!(arr idx1 idx2 idx3) #(#[,,](#arr #idx1 #idx2 #idx3))
#def []*(id idx)
#set id #[](#id #idx)
#ifdef #id #(#id)
#else #
#endif
#enddef

#def []=(id idx val id*)
#assert (not #null(#id))
#assert #numberp(#idx)
;; there must not be name conflict with an output variable used in 'id*' ...
#let _aux_elem_id #[](#id #idx)
    #=(#_aux_elem_id #val)
    #if (not #null(#id*))
        #set #id* #_aux_elem_id
    #endif
#endlet _aux_elem_id
#enddef

#define array-name(arr) #ref-name(#arr)
#define array-idx-name(arr) #_-(#arr idx)
#define array-idx(arr) #(#array-idx-name(#arr))

#def defarray(id expr)
#assert #listp(#expr)
#let (_aux_id*)
#let #id (#for (idx 0 #last-idx(#expr))
    #[]=(#id #idx #nth(#idx #expr) ##_aux_id*)
    #let #array-idx-name(#_aux_id*) #idx
    #if #refp(#(#_aux_id*)) #(#_aux_id*)
    #else ##(#_aux_id*)
    #endif
#endfor)
#endlet
#enddef

;;; Object macros

#define .(obj key) #ref-name*(#obj) ## .#key
#define .!(obj key) #(#.(#obj #key))
#def .*(id key)
#set id #.(#id #key)
#ifdef #id #(#id)
#else #
#endif
#enddef

#def .=(id key val id*)
#assert (not #null(#id))
#assert (not #null(#key))
;; there must not be name conflict with an output variable used in 'id*' ...
#let _aux_attr_id #.(#id #key)
    #=(#_aux_attr_id #val)
    #if (not #null(#id*))
        #set #id* #_aux_attr_id
    #endif
#endlet _aux_attr_id
#enddef

#define object-name(obj) #array-name(#obj)
#define object-idx-name(obj) #array-idx-name(#obj)
#define object-idx(obj) #array-idx(#obj)
#define object-key-name(obj) #_-(#obj key)
#define object-key(obj) #(#object-key-name(#obj))
#define object-keys-name(obj) #_-(#obj keys)
#define object-keys(obj) #(#object-keys-name(#obj))

#define make-object-name(id) make-#id
#define make-object(id) #(#make-object-name(#id))

#def defobject(id expr)
#assert #listp(#expr)
#let ((keys ()) (make ()) (was_keyword 0) attr_key attr_id ref (arr_idx 0) i)
#defarray(#id (#for (idx 0 #last-idx(#expr))
#set i #nth(#idx #expr)
#if #keywordp(#i)
    #assert (not #was_keyword)
    #set attr_key #crop-keyword(#i)
    #set was_keyword 1
#elif #was_keyword
    #.=(#id #attr_key #i ##attr_id)
    ;+ does not distinguish whether it is already a ref or not,
    ;+ but when it does, it does not work properly ..
    #set ref ##(#attr_id)
    #ref

    #let #object-idx-name(#attr_id) #arr_idx
    #let #object-key-name(#attr_id) #attr_key

    #set keys #keys## (#attr_key)
    #set make #make## (:#attr_key #ref)

    #set was_keyword 0
    #set arr_idx $d(+ #arr_idx 1)
#else #error (Expected a keyword, got: #i)
#endif
#endfor))
    #defarray(#object-keys-name(#id) #keys)
    #let #make-object-name(#id) #make
#endlet
#enddef

;;;; UNSOT-related macros

#ifdef N
#define N-1 $d(- #N 1)
#define N-2 $d(- #N 2)

#define STEPS #N
#endif

#define STEPS-1 $d(- #STEPS 1)
#define STEPS-2 $d(- #STEPS 2)

#define AS_INIT_PARAMS(args) #AS_PARAMS(#args)
#define AS_FINALS(args) #args##*
#define AS_FINAL_PARAMS(args) #AS_FINALS(#AS_PARAMS(#args))

;;; Consts related macros

#let ((Aux_bools ()) (Main_bools ())
      (Aux_reals ()) (Main_reals ())
      (Aux_odes ()) (Main_odes ())
)

#define BOOLS #Main_bools#Aux_bools

#define _T _t
#define _T_ODES(odes) (#_T) ## #odes

#define REALS #Main_reals#Aux_reals
#define ODES #Main_odes#Aux_odes
#define T_ODES #_T_ODES(#ODES)
#define ALL_REALS #REALS#T_ODES

#define T_AUX_ODES #_T_ODES(#Aux_odes)
#define ALL_AUX_REALS #Aux_reals#T_AUX_ODES
#define T_MAIN_ODES #_T_ODES(#Main_odes)
#define ALL_MAIN_REALS #Main_reals#T_MAIN_ODES

#define AT(const i) #const##<#i##>

#define FIRST_STEP 0
#define FIRST_STEP+1 1
#define FIRST_STEP+2 2
#define LAST_STEP #STEPS
#define LAST_STEP-1 #STEPS-1
#define LAST_STEP-2 #STEPS-2
#define FIRST(const) #AT(#const #FIRST_STEP)
#define FIRST+1(const) #AT(#const #FIRST_STEP+1)
#define FIRST+2(const) #AT(#const #FIRST_STEP+2)
#define LAST(const) #AT(#const #LAST_STEP)
#define LAST-1(const) #AT(#const #LAST_STEP-1)
#define LAST-2(const) #AT(#const #LAST_STEP-2)

#define T_AT(i) #AT(#_T #i)
#define FIRST_T #FIRST(#_T)
#define LAST_T #LAST(#_T)

#define INIT_T #AS_INIT_PARAM(#_T)
#define FINAL_T #AS_FINALS(#_T)
#define FINAL_PARAM_T #AS_FINAL_PARAMS(#_T)

;;; Declaration/definition macros

#def DECL_CONSTS(args type)
#assert #listp^(#args)
#for (const #args)
#for (i #FIRST_STEP #LAST_STEP)
    (declare-const #AT(#const #i) #type)
#endfor
#endfor
#enddef

#define DECL_BOOL_CONSTS #DECL_CONSTS(#BOOLS Bool)

#define DECL_REAL_CONSTS #DECL_CONSTS(#ALL_REALS Real)

#def AS_PARAMS_WITH_SORTS(args sort)
#assert #listp^(#args)
#for (const #args)
    (#AS_PARAMS(#const) #sort)
#endfor
#enddef

#def AS_INIT_PARAMS_WITH_SORTS(args sort)
#assert #listp^(#args)
#for (const #args)
    (#AS_INIT_PARAMS(#const) #sort)
#endfor
#enddef

#def AS_FINAL_PARAMS_WITH_SORTS(args sort)
#assert #listp^(#args)
#for (const #args)
    (#AS_FINAL_PARAMS(#const) #sort)
#endfor
#enddef

;;; Flow definition

;; Set `FLOW_ADD_*_ARGS` to add other arguments
#let ((Flow_add_bool_args ())
      (Flow_add_real_args ())
      (Flow_add_init_bool_args ())
      (Flow_add_init_real_args ())
      (Flow_add_final_bool_args ())
      (Flow_add_final_real_args ())
      (Flow_add_bool_single_args ())
      (Flow_add_real_single_args ())
)

#def FLOW_DEF_ARGS()
    (#AS_PARAMS_WITH_SORTS(#BOOLS Bool)) ##
    (#AS_PARAMS_WITH_SORTS(#REALS Real)) ##
    (#AS_PARAMS_WITH_SORTS(#Flow_add_bool_args Bool)) ##
    (#AS_PARAMS_WITH_SORTS(#Flow_add_real_args Real)) ##
    (#AS_INIT_PARAMS_WITH_SORTS(#Flow_add_init_bool_args Bool)) ##
    (#AS_INIT_PARAMS_WITH_SORTS(#Flow_add_init_real_args Real)) ##
    (#AS_PARAMS_WITH_SORTS(#AS_FINALS(#T_ODES) Real)) ##
    (#AS_PARAMS_WITH_SORTS(#AS_FINALS(#BOOLS) Bool)) ##
    (#AS_PARAMS_WITH_SORTS(#AS_FINALS(#REALS) Real)) ##
    (#AS_PARAMS_WITH_SORTS(#AS_FINALS(#Flow_add_bool_args) Bool)) ##
    (#AS_PARAMS_WITH_SORTS(#AS_FINALS(#Flow_add_real_args) Real)) ##
    (#AS_PARAMS_WITH_SORTS(#AS_FINALS(#Flow_add_final_bool_args) Bool)) ##
    (#AS_PARAMS_WITH_SORTS(#AS_FINALS(#Flow_add_final_real_args) Real)) ##
    (#AS_PARAMS_WITH_SORTS(#Flow_add_bool_single_args Bool)) ##
    (#AS_PARAMS_WITH_SORTS(#Flow_add_real_single_args Real)) ##
    ()
#enddef

;;; Flows instantiation

#def _FLOW_INPUTS(i args)
#for (const #args)
    #AT(#const #i)
#endfor
#enddef

#define FLOW_INPUTS(i) (#_FLOW_INPUTS(#i #T_ODES))

#def _FLOW_I_ARGS(i)
#for (const #BOOLS#REALS##
            #Flow_add_bool_args#Flow_add_real_args##
            #Flow_add_init_bool_args#Flow_add_init_real_args)
    #AT(#const #i)
#endfor
#enddef

#def _FLOW_J_ARGS(i)
#let j $d(+ #i 1)
#for (const #T_ODES#BOOLS#REALS##
            #Flow_add_bool_args#Flow_add_real_args##
            #Flow_add_final_bool_args#Flow_add_final_real_args)
    #AT(#const #j)
#endfor
#endlet j
#enddef

#def FLOW_ARGS(i)
    (#_FLOW_I_ARGS(#i)) ##
    (#_FLOW_J_ARGS(#i)) ##
    ()
#enddef

#def FLOW(id i)
    (flow #id
        #FLOW_INPUTS(#i)
        #FLOW_ARGS(#i) ## #Flow_add_bool_single_args#Flow_add_real_single_args
    )
#enddef

#def FLOWS(id) 1
#for (i #FIRST_STEP #LAST_STEP-1)
    #FLOW(#id #i)
#endfor
#enddef

#define ASSERT_FLOWS (assert (and #FLOWS(f)))

;;;; Jump conditions

#def JUMPS(args value)
    #EQUALS(#AS_FINAL_PARAMS(#args) #value)
#enddef

#define ODE_NO_JUMP(const) #JUMPS(#const #AS_FINALS(#const))
#define T_NO_JUMP #ODE_NO_JUMP(#_T)

#def _ODE_NO_JUMPS(args)
#for (const #args)
    #ODE_NO_JUMP(#const)
#endfor
#enddef

#define ODE_NO_JUMPS #_ODE_NO_JUMPS(#ODES)
#define T_ODE_NO_JUMPS #_ODE_NO_JUMPS(#T_ODES)

#define MAIN_ODE_NO_JUMPS #_ODE_NO_JUMPS(#Main_odes)
#define T_MAIN_ODE_NO_JUMPS #_ODE_NO_JUMPS(#T_MAIN_ODES)

#define PARAM_NO_JUMP(const) #JUMPS(#const #AS_INIT_PARAMS(#const))

#def PARAM_NO_JUMPS(args)
#assert #listp^(#args)
#for (const #args)
    #PARAM_NO_JUMP(#const)
#endfor
#enddef

;;; Interactive macros

#def VALUES()
#for (const #Main_bools)
#for (i #FIRST_STEP #LAST_STEP)
    #AT(#const #i)
#endfor
#endfor
#unpack(#Flow_add_bool_single_args)
#for (const #ALL_MAIN_REALS)
#for (i #FIRST_STEP #LAST_STEP)
    #AT(#const #i)
#endfor
#endfor
#unpack(#Flow_add_real_single_args)
#enddef

#define GET_VALUES (get-value (#VALUES))


#endif  ;; __STDLIB_EXPR
