(declare-const n1 Bool)
(declare-const n2 Bool)

(declare-const t0 Real)
(declare-const t Real)
(declare-const v Real)
#ifdef A
(declare-const a Real)
#endif

;; The same conflict is encountered twice with -DA
;; .. because pure Booleans are being decided, not the predicates
;; The inconsistency is discovered as late as the already learned clause becomes recognizable
;; MiniSat does recognize the conflict sooner without 'a', but does not with it

(assert (and (=> n1 (= v 10)) (=> n2 (= v 10))
             (= t0 0)
             (xor n1 n2)
             (< t 10)
             #ifdef A
             (= t (+ t0 v a))
             (=> n1 (= a 1)) (=> n2 (= a 1))
             #else
             (= t (+ t0 v))
             #endif
))

(check-sat)
