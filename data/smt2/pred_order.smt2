#ifndef N
#define N 100
#endif

#include data/expr/stdlib.expr

#ifdef ONLINE
(set-option :t-suggest-strategy init)
#endif

(declare-const x Real)
(declare-const y Real)

;; choose ordering from 0 .. 7
#ifndef ORDER
#define ORDER 1
#endif
#assert (and (>= #ORDER 0) (<= #ORDER 7))

#define TRUE_PRED  (= x y)
; #define TRUE_PRED  (= x (+ y 1))
#define FALSE_PRED (< x y)

;; putting equalities into pair conflicts does not help significantly
#define X_INTERVAL (and  #INTERVAL_COUNT(x 0 #N $(+ #N 1)) )
#define Y_INTERVAL (and  #INTERVAL_COUNT(y 0 #N $(+ #N 1)) )

;; Number of backtracks for N=100 should be 101
#if (= #ORDER 0)
(assert #X_INTERVAL)
(assert #Y_INTERVAL)
(assert #TRUE_PRED)
(assert #FALSE_PRED)
#elif (= #ORDER 1)
(assert #FALSE_PRED)
(assert #X_INTERVAL)
(assert #Y_INTERVAL)
(assert #TRUE_PRED)
#elif (= #ORDER 2)
(assert #X_INTERVAL)
(assert #Y_INTERVAL)
(assert #FALSE_PRED)
(assert #TRUE_PRED)
;; same as:
; (assert #X_INTERVAL)
; (assert #FALSE_PRED)
; (assert #Y_INTERVAL)
; (assert #TRUE_PRED)
#elif (= #ORDER 3)
(assert #FALSE_PRED)
(assert #TRUE_PRED)
(assert #X_INTERVAL)
(assert #Y_INTERVAL)
#elif (= #ORDER 4)
(assert #X_INTERVAL)
(assert #TRUE_PRED)
(assert #FALSE_PRED)
(assert #Y_INTERVAL)
#elif (= #ORDER 5)
(assert #X_INTERVAL)
(assert #FALSE_PRED)
(assert #TRUE_PRED)
(assert #Y_INTERVAL)
;; same as:
; (assert #X_INTERVAL)
; (assert #TRUE_PRED)
; (assert #Y_INTERVAL)
; (assert #FALSE_PRED)
#elif (= #ORDER 6)
(assert #FALSE_PRED)
(assert #X_INTERVAL)
(assert #TRUE_PRED)
(assert #Y_INTERVAL)
#elif (= #ORDER 7)
(assert #TRUE_PRED)
(assert #X_INTERVAL)
(assert #FALSE_PRED)
(assert #Y_INTERVAL)
;; same as:
; (assert #TRUE_PRED)
; (assert #X_INTERVAL)
; (assert #Y_INTERVAL)
; (assert #FALSE_PRED)
;; .. and:
; (assert #TRUE_PRED)
; (assert #FALSE_PRED)
; (assert #X_INTERVAL)
; (assert #Y_INTERVAL)
#endif


(check-sat)
