#ifndef N
#define N 10
#endif

#include data/expr/stdlib.expr

#ifdef ONLINE
;++ bmc_init
(set-option :t-suggest-strategy init)
#endif

#ifndef OPER
#define OPER =
#endif

#ifndef L
#define L 4
#endif
#define L-1 $d(- #L 1)

#ifndef M
#define M 4
#endif
#define M-1 $d(- #M 1)

#ifndef EXP
#define EXP -1.5
#endif

#define ZERO(m l) #AT(zero#m #l)
#define X(m l) #AT(x#m #l)

#for (l 1 #L)
#for (m 1 #M)
(declare-const #X(#m #l) Real)
(declare-const #ZERO(#m #l) Bool)
#endfor
#endfor

#define MAX_BITS 32
#define MAX_MUL_BITS 14
#assert (< (log #N) #MAX_MUL_BITS)

#define MAX_EXP_BITS $d(- #MAX_BITS #MAX_MUL_BITS)
#define DIV_M(m) $d(/ #m #MAX_EXP_BITS)
#define MOD_M(m) $d(% #m #MAX_EXP_BITS)
#let _max_m $d(<< 1 #MAX_EXP_BITS)
#let _exp_max $d $f(^ #EXP #MAX_EXP_BITS)
#define _EXP_MOD(m) $d $f(^ #EXP #MOD_M(#m))
#for (m 1 #M)
#let _exp_#m 1
    #for (j 1 #DIV_M(#m))
    #set _exp_#m $d(% (* #(_exp_#m) #_exp_max) #_max_m)
    #endfor
#set _exp_#m $d(% (* #(_exp_#m) #_EXP_MOD(#m)) #_max_m)
#endfor

#define _VAL(i m) $d(* #i #(_exp_#m))
#def _EXPR(l i m)
#if (= #l 1) #_VAL(#i #m)
#else (+ #_VAL(#i #m) #X(#m $d(- #l 1)))
#endif
#enddef
#define EQUAL(l i m) (= #X(#m #l) #_EXPR(#l #i #m))

#define FINAL_PROD(m) (* 1 #for (m2 #m #M-1) #X(#m2 #L) #endfor)
#define FINAL_SUM(m) (+ 0 #for (m2 $d(+ #m 1) #M) #X(#m2 #L) #endfor)

(assert (and  #for (l 1 #L)
                  #for (m 1 #M)
                      (or #for (i $d(- #N) #N) #EQUAL(#l #i #m) #endfor)
                      (= #EQUAL(#l 0 #m) #ZERO(#m #l))
                      #if (< #m #M) #for (i $d(- #N) #N)
                      (#OPER #EQUAL(#l #i #m) #EQUAL(#l #i $d(+ #m 1)))
                      #endfor #endif
                  #endfor
                  #if (> #M 2)
                  #for (m 2 #M-1)
                      (=> #ZERO(#m #l) (< (* #X(#m #l) #X($d(+ #m 1) #l)) (* #X($d(- #m 1) #l) #X($d(+ #m 1) #l)) ))
                  #endfor
                  #endif
              #endfor
              (or #for (l 1 #L) #for (m 1 #M) #ZERO(#m #l) #endfor #endfor)
              (or #for (l 1 #L) #for (m 1 #M) (not #ZERO(#m #l)) #endfor #endfor)
              (or #for (m 1 #M-1)
                  (and (<= #FINAL_PROD(#m) #FINAL_SUM(#m))
                       (>= #FINAL_PROD(#m) #FINAL_SUM(#m)) )
              #endfor)
))

(check-sat)
(get-model)
