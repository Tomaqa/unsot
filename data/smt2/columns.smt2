#ifndef N
#define N 2
#endif

#include data/expr/stdlib.expr

#ifdef ONLINE
(set-option :t-suggest-strategy bmc_init)
#endif

#ifndef L
#define L 3
#endif
#define L-1 $d(- #L 1)

#define X(n l) #AT(x#n #l)

#for (n 0 #N-1)
#for (l 1 #L)
(declare-const #X(#n #l) Real)
#endfor
#endfor

(assert (and
    (or #for (j 0 #N-1)
        (and #for (i 2 #L)
             #for (n1 0 #N-1)
             #let n2 $d(% (+ #n1 #j) #N)
             (= #X(#n1 #i) #X(#n2 $d(- #i 1)))
             #endlet n2
             #endfor
             #endfor
    ) #endfor)
    #for (n 0 #N-1) (= #X(#n 1) #n) #endfor
    #for (l 1 #L)
    #for (n 1 #N-1) (not (= #X(#n #l) #X($d(- #n 1) #l))) #endfor
    #endfor
    #for (l 2 #L)
    #for (n 0 #N-1) (~ #X(#n #l) #X(#n $d(- #l 1))) #endfor
    #endfor
))

(check-sat)
(get-model)
