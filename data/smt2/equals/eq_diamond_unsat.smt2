#ifdef SMT
(set-logic QF_UF)
(declare-sort Real 0)
#endif

#ifndef N
#define N 12
#endif

#include data/expr/stdlib.expr

#ifdef ONLINE
(set-option :t-suggest-strategy none)
#endif

#for (i 0 #N-1)
    (declare-fun #AT(x #i) () Real)
#endfor
#for (i 0 #N-2)
    (declare-fun #AT(y #i) () Real)
    (declare-fun #AT(z #i) () Real)
#endfor

(assert (and true #for (i 0 #N-2)
                  #let j $d(+ #i 1)
                      (or (and (= #AT(x #i) #AT(y #i)) (= #AT(y #i) #AT(x #j)))
                          (and (= #AT(x #i) #AT(z #i)) (= #AT(z #i) #AT(x #j)))
                      )
                  #endlet j
#endfor))
(assert (not (= #FIRST(x) #LAST-1(x))))

#ifndef SMT
(assert (= #FIRST(x) 0))
#endif
(check-sat)
