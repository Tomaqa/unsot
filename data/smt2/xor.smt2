#ifndef N
#define N 100
#endif

#include data/expr/stdlib.expr

#ifdef ONLINE
(set-option :t-suggest-strategy bmc_init)
#endif

#ifndef L
#define L 4
#endif

#define all 1
#define some 2
#define none 3

#ifndef THRES
#define THRES 0
#endif

#for (i 1 #L)
(declare-const #AT(x #i) Real)
(declare-const #AT(a #i) Bool)
#endfor

#define INC (= #AT(x #j) (+ #AT(x #i) 1))
#define VAL (= #AT(x #j) #j)

(assert (and  #INTERVAL_COUNT(#AT(x 1) 0 #N $(+ #N 1))
              #AT(a 1)
              #for (j 2 #L)
              #let ((i $d(- #j 1)))
              #if (not #THRES)
              #INC
              #elif (= #THRES #all)
              (ite (>= #AT(x #i) 0) #VAL #INC)
              #elif (= #THRES #some)
              (ite (< #AT(x #i) $d(/ #N 2)) #VAL #INC)
              #elif (= #THRES #none)
              (ite (< #AT(x #i) -1) #VAL #INC)
              #endif
              (xor (< #AT(x #j) 0) #AT(a #j))
              #endlet
              #endfor
              (< #AT(x #L) -2)
))

(check-sat)
