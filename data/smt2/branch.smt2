#ifdef ONLINE
(set-option :t-suggest-strategy init)
#endif

#ifndef N
#define N 3
#endif

#ifndef L
#define L 2
#endif
#define L-1 $d(- #L 1)

#ifndef UNSAT
#define UNSAT 0
#endif

#for (i 1 #L)
(declare-const x#i Real)
#endfor

(assert (and
    #for (i 1 #L)
    (or #for (j 1 #N) (= x#i #j) #endfor)
    #endfor
    #for (i 1 #L-1)
    (= x#i x$d(+ #i 1))
    #endfor
    #if #UNSAT
    (> x#L #N)
    #else
    (>= x#L #N)
    #endif
))

(check-sat)
(get-model)
