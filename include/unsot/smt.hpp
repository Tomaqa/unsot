#pragma once

#include "util.hpp"
#include "util/flag.hpp"
#include "expr.hpp"

namespace unsot::smt {
    using namespace util;
    using namespace expr;

    struct Sat;
}

namespace unsot::smt {
    struct Sat : Inherit<Flag, Sat> {
        static constexpr int sat = true_value;
        static constexpr int unsat = false_value;
        static constexpr int unknown = unknown_value;

        using Inherit::Inherit;

        String to_string() const& noexcept;
    };

    /// Cannot use `Sat sat = Sat::sat` - conflict with the namespace name
}
