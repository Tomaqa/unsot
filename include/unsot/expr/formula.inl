#pragma once

namespace unsot::expr {
    template <typename L, Req<is_list_v<L>>>
    Formula::Formula(Key op, L&& ls)
        : Inherit(FORWARD(ls)), _oper(move(op))
    { }

    template <typename F>
    auto Formula::make_to_string(F&& phi)
    {
        using F_ = Rm_ref<F>;
        return To_string<F_>(Ref<F_>(FORWARD(phi)));
    }
}

////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////

namespace unsot::expr {
    template <typename B>
    void Formula::To_string_mixin<B>::
        perform_head_post(String& str, const Conf& conf, int /*depth*/)
    {
        str += conf.delim + FORWARD(this->list()).oper();
    }

    template <typename B>
    String Formula::To_string_mixin<B>::ptr_to_string(const Ptr& ptr, int depth)
    {
        return ptr_to_string_impl(ptr, depth);
    }

    template <typename B>
    String Formula::To_string_mixin<B>::ptr_to_string(Ptr&& ptr, int depth)
    {
        return ptr_to_string_impl(move(ptr), depth);
    }

    template <typename B>
    template <typename P>
    String Formula::To_string_mixin<B>::ptr_to_string_impl(P&& ptr, int depth)
    {
        if (!Formula::is_me(ptr))
            return Inherit::ptr_to_string(FORWARD(ptr), depth);
        return Formula::cast(FORWARD(ptr)).Formula::to_string();
    }
}
