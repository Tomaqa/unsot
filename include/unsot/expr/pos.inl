#pragma once

namespace unsot::expr::aux::pos {
    template <typename T, typename L>
    Const_crtp<T, L>::Const_crtp(Ref ref_)
        : _ref(move(ref_))
    {
        set_it();
    }

    template <typename T, typename L>
    template <typename L_, Req<is_list_v<L_>>>
    Const_crtp<T, L>& Const_crtp<T, L>::operator =(L_&& ls)
    {
        _ref = FORWARD(ls);
        set_it();
        return *this;
    }

    template <typename T, typename L>
    typename Const_crtp<T, L>::const_iterator
    Const_crtp<T, L>::cit() const noexcept
    {
        if constexpr (is_const_v) return _it.value_or(cend());
        else return at_end() ? cend() : crit();
    }

    template <typename T, typename L>
    typename Const_crtp<T, L>::Iterator
    Const_crtp<T, L>::it() noexcept
    {
        return _it.value_or(end());
    }

    template <typename T, typename L>
    bool Const_crtp<T, L>::at_begin() const noexcept
    {
        return cit() == cbegin();
    }

    template <typename T, typename L>
    bool Const_crtp<T, L>::at_end() const noexcept
    {
        return _it.invalid();
    }

    template <typename T, typename L>
    bool Const_crtp<T, L>::at_front() const noexcept
    {
        return !at_end() && crit() == cbegin();
    }

    template <typename T, typename L>
    bool Const_crtp<T, L>::at_back() const noexcept
    {
        return !at_end() && std::next(crit(), 1) == cend();
    }

    template <typename T, typename L>
    auto Const_crtp<T, L>::make_to_string(const Const_crtp& pos_)
    {
        return To_string<const List>(pos_);
    }

    ////////////////////////////////////////////////////////////////

    template <typename T, typename L>
    template <typename L_, Req<is_list_v<L_>>>
    Crtp<T, L>& Crtp<T, L>::operator =(L_&& ls)
    {
        if constexpr (is_cref_v<L_>) Inherit::Parent::operator =(List(ls));
        else Inherit::Parent::operator =(FORWARD(ls));
        return *this;
    }

    template <typename T, typename L>
    auto Crtp<T, L>::make_to_string(Crtp&& pos_)
    {
        return To_string<List>(move(pos_));
    }

    template <typename T, typename L>
    template <typename... Args>
    typename Crtp<T, L>::That& Crtp<T, L>::emplace(Args&&... args)
    {
        list().emplace(it(), FORWARD(args)...);
        return this->that();
    }
}

////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////

namespace unsot::expr::aux::pos {
    template <typename T, typename L>
    template <typename B>
    template <typename P_>
    Const_crtp<T, L>::To_string_mixin<B>::To_string_mixin(P_&& pos_)
        : Inherit(Ref(FORWARD(pos_).list())), _pos(FORWARD(pos_))
    { }
}

////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////

namespace unsot::expr {
    template <typename L, Req<is_list_v<L>>>
    auto make_pos(L&& ls)
    {
        if constexpr (is_cref_v<L>) return Const_pos(ls);
        else return Pos(FORWARD(ls));
    }

    template <typename P>
    bool is_elem_base_at(const P& pos) noexcept
    {
        return aux::pos::Const_foo<P>::is_elem_base_at(pos);
    }

    template <typename P>
    bool is_key_at(const P& pos) noexcept
    {
        return aux::pos::Const_foo<P>::is_key_at(pos);
    }

    template <typename P>
    bool is_list_at(const P& pos) noexcept
    {
        return aux::pos::Const_foo<P>::is_list_at(pos);
    }

    template <typename P>
    bool is_seq_at(const P& pos) noexcept
    {
        return aux::pos::Const_foo<P>::is_seq_at(pos);
    }

    template <typename P>
    void check_is_elem_base_at(const P& pos)
    {
        return aux::pos::Const_foo<P>::check_is_elem_base_at(pos);
    }

    template <typename P>
    void check_is_key_at(const P& pos)
    {
        return aux::pos::Const_foo<P>::check_is_key_at(pos);
    }

    template <typename P>
    void check_is_list_at(const P& pos)
    {
        return aux::pos::Const_foo<P>::check_is_list_at(pos);
    }

    template <typename P>
    void check_is_seq_at(const P& pos)
    {
        return aux::pos::Const_foo<P>::check_is_seq_at(pos);
    }

    template <typename E, typename P>
    bool is_elem_at(const P& pos) noexcept
    {
        return aux::pos::Const_foo<P, E>::is_elem_at(pos);
    }

    template <typename Arg, typename E, typename P>
    bool is_value_at(const P& pos) noexcept
    {
        return is_value<Arg, E>(pos.cpeek());
    }

    template <typename E, typename P>
    void check_is_elem_at(const P& pos)
    {
        return aux::pos::Const_foo<P, E>::check_is_elem_at(pos);
    }

    template <typename Arg, typename E, typename P>
    void check_is_value_at(const P& pos)
    {
        check_is_value<Arg, E>(pos.cpeek_check());
    }

    template <typename E, typename P>
    bool is_int_at(const P& pos) noexcept
    {
        return aux::pos::Const_foo<P, E>::is_int_at(pos);
    }

    template <typename E, typename P>
    bool is_real_at(const P& pos) noexcept
    {
        return aux::pos::Const_foo<P, E>::is_real_at(pos);
    }

    template <typename E, typename P>
    void check_is_int_at(const P& pos)
    {
        return aux::pos::Const_foo<P, E>::check_is_int_at(pos);
    }

    template <typename E, typename P>
    void check_is_real_at(const P& pos)
    {
        return aux::pos::Const_foo<P, E>::check_is_real_at(pos);
    }

    template <typename P>
    bool is_flat_list_at(const P& pos) noexcept
    {
        return aux::pos::Const_foo<P>::is_flat_list_at(pos);
    }

    template <typename P>
    bool is_deep_list_at(const P& pos) noexcept
    {
        return aux::pos::Const_foo<P>::is_deep_list_at(pos);
    }

    template <typename P>
    void check_is_flat_list_at(const P& pos)
    {
        return aux::pos::Const_foo<P>::check_is_flat_list_at(pos);
    }

    template <typename P>
    void check_is_deep_list_at(const P& pos)
    {
        return aux::pos::Const_foo<P>::check_is_deep_list_at(pos);
    }

    template <typename P>
    Forward_as_ref<Elem_base, P> peek_elem_base(P&& pos) noexcept
    {
        return peek_elem<Elem_base>(FORWARD(pos));
    }

    template <typename P>
    Forward_as_ref<Key, P> peek_key(P&& pos)
    {
        return cast_key(FORWARD(pos).peek());
    }

    template <typename P>
    Forward_as_ref<List, P> peek_list(P&& pos) noexcept
    {
        return List::cast(FORWARD(pos).peek());
    }

    template <typename P>
    auto get_elem_base(P&& pos) noexcept
        -> Cond_t<Decay<P>::is_const_v, const Elem_base&, Forward_as_ref<Elem_base, P>>
    {
        return get_elem<Elem_base>(FORWARD(pos));
    }

    template <typename P>
    auto get_key(P&& pos)
        -> Cond_t<Decay<P>::is_const_v, const Key&, Forward_as_ref<Key, P>>
    {
        return cast_key(FORWARD(pos).get());
    }

    template <typename P>
    auto get_list(P&& pos) noexcept
        -> Cond_t<Decay<P>::is_const_v, const List&, Forward_as_ref<List, P>>
    {
        return List::cast(FORWARD(pos).get());
    }

    template <typename P>
    Forward_as_ref<Elem_base, P> peek_elem_base_check(P&& pos)
    {
        return peek_elem_check<Elem_base>(FORWARD(pos));
    }

    template <typename P>
    Forward_as_ref<Key, P> peek_key_check(P&& pos)
    {
        return cast_key_check(FORWARD(pos).peek_check());
    }

    template <typename P>
    Forward_as_ref<List, P> peek_list_check(P&& pos)
    {
        return List::cast_check(FORWARD(pos).peek_check());
    }

    template <typename P>
    auto get_elem_base_check(P&& pos)
        -> Cond_t<Decay<P>::is_const_v, const Elem_base&, Forward_as_ref<Elem_base, P>>
    {
        return get_elem_check<Elem_base>(FORWARD(pos));
    }

    template <typename P>
    auto get_key_check(P&& pos)
        -> Cond_t<Decay<P>::is_const_v, const Key&, Forward_as_ref<Key, P>>
    {
        return cast_key_check(FORWARD(pos).get_check());
    }

    template <typename P>
    auto get_list_check(P&& pos)
        -> Cond_t<Decay<P>::is_const_v, const List&, Forward_as_ref<List, P>>
    {
        return List::cast_check(FORWARD(pos).get_check());
    }

    template <typename E, typename P>
    Forward_as_ref<E, P> peek_elem(P&& pos) noexcept
    {
        return E::cast(FORWARD(pos).peek());
    }

    template <typename Arg, typename E, typename P>
    Forward_as_ref<Arg, P> peek_value(P&& pos)
    {
        return cast_value<Arg, E>(FORWARD(pos).peek());
    }

    template <typename E, typename P>
    auto get_elem(P&& pos) noexcept
        -> Cond_t<Decay<P>::is_const_v, const E&, Forward_as_ref<E, P>>
    {
        return E::cast(FORWARD(pos).get());
    }

    template <typename Arg, typename E, typename P>
    auto get_value(P&& pos)
        -> Cond_t<Decay<P>::is_const_v, const Arg&, Forward_as_ref<Arg, P>>
    {
        return cast_value<Arg, E>(FORWARD(pos).get());
    }

    template <typename E>
    E extract_elem(Pos& pos)
    {
        return aux::pos::Foo<E>::extract_elem(pos);
    }

    template <typename Arg, typename E>
    Arg extract_value(Pos& pos)
    {
        return cast_value<Arg, E>(pos.extract());
    }

    template <typename E>
    E extract_copy_elem(Pos& pos)
    {
        return aux::pos::Foo<E>::extract_copy_elem(pos);
    }

    template <typename Arg, typename E>
    Arg extract_copy_value(Pos& pos)
    {
        const Ptr ptr = pos.extract();
        return cast_value<Arg, E>(ptr);
    }

    template <typename E>
    E extract_maybe_copy_elem(Pos& pos)
    {
        return aux::pos::Foo<E>::extract_maybe_copy_elem(pos);
    }

    template <typename Arg, typename E>
    Arg extract_maybe_copy_value(Pos& pos)
    {
        Ptr ptr = pos.extract();
        if (ptr.unique()) return cast_value<Arg, E>(move(ptr));
        return cast_value<Arg, E>(as_const(ptr));
    }

    template <typename E, typename P>
    Forward_as_ref<E, P> peek_elem_check(P&& pos)
    {
        return E::cast_check(FORWARD(pos).peek_check());
    }

    template <typename Arg, typename E, typename P>
    Forward_as_ref<Arg, P> peek_value_check(P&& pos)
    {
        return cast_value_check<Arg, E>(FORWARD(pos).peek_check());
    }

    template <typename E, typename P>
    auto get_elem_check(P&& pos)
        -> Cond_t<Decay<P>::is_const_v, const E&, Forward_as_ref<E, P>>
    {
        return E::cast_check(FORWARD(pos).get_check());
    }

    template <typename Arg, typename E, typename P>
    auto get_value_check(P&& pos)
        -> Cond_t<Decay<P>::is_const_v, const Arg&, Forward_as_ref<Arg, P>>
    {
        return cast_value_check<Arg, E>(FORWARD(pos).get_check());
    }

    template <typename E>
    E extract_elem_check(Pos& pos)
    {
        return aux::pos::Foo<E>::extract_elem_check(pos);
    }

    template <typename Arg, typename E>
    Arg extract_value_check(Pos& pos)
    {
        return cast_value_check<Arg, E>(pos.extract_check());
    }

    template <typename E>
    E extract_copy_elem_check(Pos& pos)
    {
        return aux::pos::Foo<E>::extract_copy_elem_check(pos);
    }

    template <typename Arg, typename E>
    Arg extract_copy_value_check(Pos& pos)
    {
        const Ptr ptr = pos.extract_check();
        return cast_value_check<Arg, E>(ptr);
    }

    template <typename E>
    E extract_maybe_copy_elem_check(Pos& pos)
    {
        return aux::pos::Foo<E>::extract_maybe_copy_elem_check(pos);
    }

    template <typename Arg, typename E>
    Arg extract_maybe_copy_value_check(Pos& pos)
    {
        Ptr ptr = pos.extract_check();
        if (ptr.unique()) return cast_value_check<Arg, E>(move(ptr));
        return cast_value_check<Arg, E>(as_const(ptr));
    }

    template <typename E, typename P>
    Forward_as_ref<Int, P> peek_int(P&& pos)
    {
        return peek_value<Int, E>(FORWARD(pos));
    }

    template <typename E, typename P>
    Forward_as_ref<Real, P> peek_real(P&& pos)
    {
        return peek_value<Real, E>(FORWARD(pos));
    }

    template <typename E, typename P>
    auto get_int(P&& pos)
        -> Cond_t<Decay<P>::is_const_v, const Int&, Forward_as_ref<Int, P>>
    {
        return get_value<Int, E>(FORWARD(pos));
    }

    template <typename E, typename P>
    auto get_real(P&& pos)
        -> Cond_t<Decay<P>::is_const_v, const Real&, Forward_as_ref<Real, P>>
    {
        return get_value<Real, E>(FORWARD(pos));
    }

    template <typename E>
    Int extract_int(Pos& pos)
    {
        return aux::pos::Foo<E>::extract_int(pos);
    }

    template <typename E>
    Real extract_real(Pos& pos)
    {
        return aux::pos::Foo<E>::extract_real(pos);
    }

    template <typename E>
    Int extract_copy_int(Pos& pos)
    {
        return aux::pos::Foo<E>::extract_copy_int(pos);
    }

    template <typename E>
    Real extract_copy_real(Pos& pos)
    {
        return aux::pos::Foo<E>::extract_copy_real(pos);
    }

    template <typename E>
    Int extract_maybe_copy_int(Pos& pos)
    {
        return aux::pos::Foo<E>::extract_maybe_copy_int(pos);
    }

    template <typename E>
    Real extract_maybe_copy_real(Pos& pos)
    {
        return aux::pos::Foo<E>::extract_maybe_copy_real(pos);
    }

    template <typename E, typename P>
    Forward_as_ref<Int, P> peek_int_check(P&& pos)
    {
        return peek_value_check<Int, E>(FORWARD(pos));
    }

    template <typename E, typename P>
    Forward_as_ref<Real, P> peek_real_check(P&& pos)
    {
        return peek_value_check<Real, E>(FORWARD(pos));
    }

    template <typename E, typename P>
    auto get_int_check(P&& pos)
        -> Cond_t<Decay<P>::is_const_v, const Int&, Forward_as_ref<Int, P>>
    {
        return get_value_check<Int, E>(FORWARD(pos));
    }

    template <typename E, typename P>
    auto get_real_check(P&& pos)
        -> Cond_t<Decay<P>::is_const_v, const Real&, Forward_as_ref<Real, P>>
    {
        return get_value_check<Real, E>(FORWARD(pos));
    }

    template <typename E>
    Int extract_int_check(Pos& pos)
    {
        return aux::pos::Foo<E>::extract_int_check(pos);
    }

    template <typename E>
    Real extract_real_check(Pos& pos)
    {
        return aux::pos::Foo<E>::extract_real_check(pos);
    }

    template <typename E>
    Int extract_copy_int_check(Pos& pos)
    {
        return aux::pos::Foo<E>::extract_copy_int_check(pos);
    }

    template <typename E>
    Real extract_copy_real_check(Pos& pos)
    {
        return aux::pos::Foo<E>::extract_copy_real_check(pos);
    }

    template <typename E>
    Int extract_maybe_copy_int_check(Pos& pos)
    {
        return aux::pos::Foo<E>::extract_maybe_copy_int_check(pos);
    }

    template <typename E>
    Real extract_maybe_copy_real_check(Pos& pos)
    {
        return aux::pos::Foo<E>::extract_maybe_copy_real_check(pos);
    }
}
