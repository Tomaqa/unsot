#pragma once

#include "util/run.hpp"
#include "expr/preprocess.hpp"

#include "util/hash.hpp"

namespace unsot::expr {
    class Preprocess::Run : public virtual Inherit<util::Run> {
    public:
        using Inherit::Inherit;
        virtual ~Run()                                              = default;

        void do_stuff() override;

        String usage() const override;

        [[nodiscard]] virtual List preprocess();
    protected:
        using Macros_map = Hash<String, String>;

        String lusage() const;

        virtual void init_preprocess();
        virtual List preprocess_lines();
        virtual List preprocess_list(List&&);

        String getopt_str() const noexcept override;
        String lgetopt_str() const noexcept;
        bool process_opt(char) override;
        bool lprocess_opt(char);

        Preprocess _preprocess{};
        Macros_map _macros_map{};
    };
}
