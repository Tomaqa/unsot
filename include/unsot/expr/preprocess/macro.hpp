#pragma once

#include "util/string.hpp"
#include "util/string/alg.hpp"

namespace unsot::expr {
    struct Macro {
        using Body = List;

        static inline const String char_str = unsot::to_string(macro_char);
        static inline const String two_chars_str = repeat(macro_char, 2);
        static inline const String three_chars_str = repeat(macro_char, 3);
        static inline const String char_escape_str = char_str + escape_char;
        static inline const String two_chars_escape_str = two_chars_str + escape_char;

        static inline const String include_key = "include";
        static inline const String define_key = "define";
        static inline const String def_key = "def";
        static inline const String undef_key = "undef";
        static inline const String let_key = "let";
        static inline const String set_key = "set";
        static inline const String if_key = "if";
        static inline const String ifdef_key = "ifdef";
        static inline const String ifndef_key = "ifndef";
        static inline const String if_else_key = "else";
        static inline const String if_elif_key = "elif";
        static inline const String isdef_key = "isdef";
        static inline const String isndef_key = "isndef";
        static inline const String for_key = "for";
        static inline const String while_key = "while";
        static inline const String null_key = "null";
        static inline const String listp_key = "listp";
        static inline const String numberp_key = "numberp";
        static inline const String refp_key = "refp";
        static inline const String equal_key = "equal";
        static inline const String eq_key = "eq";
        static inline const String len_key = "len";
        static inline const String print_key = "print";
        static inline const String assert_key = "assert";
        static inline const String error_key = "error";
        static inline const String car_key = "car";
        static inline const String cdr_key = "cdr";
        static inline const String nth_key = "nth";
        static inline const String nthcdr_key = "nthcdr";
        static inline const String ref_name_key = "ref-name";

        static inline const String end_key = "end";

        static bool is_end_key(String_view) noexcept;

        static bool is_macro_of(String_view, const Key&) noexcept;
        static bool is_end_key_of(String_view, const Key&) noexcept;

        static bool is_reserved_key(const Key&) noexcept;
        static bool is_reserved_end_key(const Key&) noexcept;
        static void check_is_not_reserved_key(const Key&);

        static const Key& end_key_of(String_view);

        Keys arg_keys{};
        Body body{};
    };
}
