#pragma once

#include "util/scope_guard.hpp"

namespace unsot::expr::var {
    template <typename T>
    T Base::cons_tp(Keys_ptr& kptr, Ids_map_ptr& imap_ptr, Key key_, bool is_set_)
    {
        Id id_ = add_key(*kptr, *imap_ptr, move(key_));
        return cons_tp<T>(kptr, imap_ptr, move(id_), is_set_);
    }

    template <typename T>
    Ptr Base::make_ptr(T&& t)
    {
        return MAKE_UNIQUE(FORWARD(t));
    }

    const Key& Base::ckey() const noexcept
    {
        return ckeys()[cid()];
    }

    Key& Base::key()& noexcept
    {
        return keys()[cid()];
    }

    bool Base::computed() const noexcept
    {
        assert(!_computed || is_set());
        return _computed;
    }

    const auto& Base::cdepend_id() const
    {
        assert(size(cdepend_ids()) == 1);
        return *cbegin(cdepend_ids());
    }

    auto& Base::depend_id()
    {
        assert(size(cdepend_ids()) == 1);
        return *begin(depend_ids());
    }

    ////////////////////////////////////////////////////////////////

    template <typename B, typename Sort>
    template <typename T, typename V>
    T Mixin<B, Sort>::
        cons_tp(Keys_ptr& kptr, Values_ptr& vptr, Ids_map_ptr& imap_ptr, Key key_, V val)
    {
        static constexpr bool valid_ = !is_same_v<V, Dummy>;
        Id id_ = add_key_value(*kptr, *vptr, *imap_ptr, move(key_), move(val));
        return cons_tp<T>(kptr, vptr, imap_ptr, move(id_), valid_);
    }

    template <typename B, typename Sort>
    const typename Mixin<B, Sort>::Value&
    Mixin<B, Sort>::cvalue() const noexcept
    {
        return cvalues()[this->cid()];
    }

    template <typename B, typename Sort>
    typename Mixin<B, Sort>::Value& Mixin<B, Sort>::value()& noexcept
    {
        return values()[this->cid()];
    }

    ////////////////////////////////////////////////////////////////

    template <typename B>
    template <typename VisitorT>
    auto Visit_mixin<B>::make_visited_ids_scope_guard() const noexcept
    {
        Scope_guard sg;
        if (!is_first_visited) return sg;

        is_first_visited = false;
        visited_ids.insert(this->cid());
        sg.set_guard([]{
            is_first_visited = true;
            visited_ids.clear();
            VisitorT::visited_ids.clear();
        });

        return sg;
    }

    ////////////////////////////////////////////////////////////////

    template <typename B>
    Distance Distance_mixin<B>::compute_distance() noexcept
    {
        return inf_dist;
    }
}
