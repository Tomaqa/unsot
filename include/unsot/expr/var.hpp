#pragma once

#include "expr.hpp"

#include "util.hpp"
#include "util/optional.hpp"
#include "util/flag.hpp"
#include "util/hash.hpp"

#define _EVAR_CVERB_HEAD this->ckey() << "[" << this->cid() << "]: "
#define EVAR_CVERBLN(x) CVERBLN(_EVAR_CVERB_HEAD << x)
#define EVAR_CVERB2LN(x) CVERB2LN(_EVAR_CVERB_HEAD << x)
#define EVAR_CVERB3LN(x) CVERB3LN(_EVAR_CVERB_HEAD << x)

namespace unsot::expr::var {
    enum class Type { regular = 0, assignee, auto_assignee };

    namespace type {
        template <Type typeV> constexpr bool is_assign_v = typeV != Type::regular;
    }

    struct Conf {};

    struct Visit_conf : Conf {
        using Visitable = true_type;
    };

    struct Distance_conf : Conf {
        using Has_distance = true_type;
    };

    struct Full_conf : Visit_conf, Distance_conf {};

    namespace conf {
        template <typename ConfT> using Visitable = Fwd_t<typename ConfT::Visitable>;
        template <typename ConfT> using Has_distance = Fwd_t<typename ConfT::Has_distance>;

        template <typename ConfT>
            constexpr bool visitable_v = enabled_bool_v<Visitable, ConfT>;
        template <typename ConfT>
            constexpr bool has_distance_v = enabled_bool_v<Has_distance, ConfT>;

        template <Type typeV> using Tp = Cond_t<typeV != Type::auto_assignee, Conf, Full_conf>;
    }

    class Base;
    template <typename B, typename Sort> class Mixin;

    template <typename B> class Visit_mixin;
    template <typename B> class Distance_mixin;

    template <typename Sort, typename ArgSort = Sort, Type typeV = Type{},
              typename ConfT = conf::Tp<typeV>>
        class Vars;

    using Ptr = Unique_ptr<Base>;
    using Ptrs = Vector<Ptr>;

    using Id = Key_id;

    using Distance = unsigned;
    constexpr Distance inf_dist = ~0U;

    constexpr Id invalid_id = invalid_idx;

    namespace aux {
        template <typename T> using Is_visitable = Void<typename Decay<T>::Visit_t>;
    }

    template <typename T> constexpr bool is_visitable_v = enabled_v<aux::Is_visitable, T>;
}

namespace unsot::expr {
    template <typename Sort, typename ConfT = var::Conf>
        using Var = Cond_mixin<var::conf::has_distance_v<ConfT>, var::Distance_mixin,
                    Cond_mixin<var::conf::visitable_v<ConfT>, var::Visit_mixin,
                    var::Mixin<var::Base, Sort>>>;

    namespace aux {
        template <typename T> using Is_var = Void<typename Decay<T>::Var_t>;
    }

    template <typename T> constexpr bool is_var_v = enabled_v<aux::Is_var, T>;
}

namespace unsot::expr::var {
    class Base : public Dynamic<Base, Ptr>, public Lock {
    public:
        using typename Dynamic<Base, Ptr>::This;

        static_assert(is_base_v);

        /// Ids of vars. that this depends on
        /// Do not mix more sorts!
        using Depend_ids = Hash<Id>;

        Base()                                                      = default;
        virtual ~Base()                                             = default;
        Base(const Base&)                                            = delete;
        Base& operator =(const Base&)                                = delete;
        Base(Base&&)                                                = default;
        Base& operator =(Base&&)                                    = default;

        using Dynamic::cons_tp;
        template <typename T> static T cons_tp(Keys_ptr&, Ids_map_ptr&, Key, bool is_set_ = false);

        template <typename T> static Ptr make_ptr(T&&);

        virtual bool is_var() const noexcept                  { return true; }
        static bool is_me(const Ptr& ptr) noexcept   { return ptr->is_var(); }

        const auto& ckeys() const noexcept            { return *ckeys_ptr(); }
        const auto& keys() const& noexcept                 { return ckeys(); }
        const auto& cids_map() const noexcept      { return *cids_map_ptr(); }
        const auto& ids_map() const& noexcept           { return cids_map(); }
        inline const Key& ckey() const noexcept;
        const auto& key() const& noexcept                   { return ckey(); }
        auto&& key()&& noexcept                        { return move(key()); }
        const auto& cid() const noexcept                       { return _id; }
        const auto& id() const noexcept                      { return cid(); }

        bool is_set() const noexcept                       { return _is_set; }
        bool is_unset() const noexcept                   { return !is_set(); }
        void check_is_set() const;
        void check_is_unset() const;

        const auto& ccomputable_flag() const noexcept     { return _computable_flag; }
        auto& computable_flag() const noexcept            { return _computable_flag; }

        /// Flag that the evaluated value was retrieved by computation
        inline bool computed() const noexcept;
        /// Fails if var is not set
        void set_computed();
        void unset_computed() noexcept                  { _computed = false; }

        virtual const Depend_ids& cdepend_ids() const                     = 0;
        inline const auto& cdepend_id() const;

        /// Sets the value and resets
        Base& set() noexcept;
        /// Unsets the value and resets
        Base& unset() noexcept;
        /// Ignores the value - resets only
        Base& reset() noexcept;

        Flag evaluable() const noexcept;
        virtual void check_evaluable() const;
        Flag computable() const noexcept;
        virtual void check_computable() const;
        virtual Flag surely_evaluable() const noexcept;
        virtual Flag surely_computable() const noexcept;
        Flag compute_computability() const noexcept;

        /// Checks *local* consistency - of set value and comp. value
        Flag consistent() noexcept(!_debug_);
        virtual void check_consistent();
        virtual Flag surely_consistent() const noexcept;
        Flag compute_consistency();

        virtual Flag independent() const noexcept;

        String to_string() const& override;
        virtual String head_to_string() const;
        virtual String body_to_string() const;
        virtual String value_to_string() const;
    protected:
        explicit Base(Keys_ptr, Ids_map_ptr, Id, bool is_set_ = false);

        bool lvalid_pre_init() const noexcept;
        void lcheck_pre_init() const;
        void lrecover_pre_init();

        virtual bool valid_key() const noexcept;
        virtual void check_key() const;

        const Keys_ptr& ckeys_ptr() const noexcept       { return _keys_ptr; }
        Keys_ptr& keys_ptr() noexcept                    { return _keys_ptr; }
        auto& keys()& noexcept                         { return *keys_ptr(); }
        const Ids_map_ptr& cids_map_ptr() const noexcept
                                                      { return _ids_map_ptr; }
        Ids_map_ptr& ids_map_ptr() noexcept           { return _ids_map_ptr; }
        auto& ids_map()& noexcept                   { return *ids_map_ptr(); }
        inline Key& key()& noexcept;
        auto& id() noexcept                                    { return _id; }

        bool& ris_set() noexcept                           { return _is_set; }

        bool& rcomputed() noexcept                       { return _computed; }

        const auto& cconsistent_flag() const noexcept
                                                  { return _consistent_flag; }
        auto& consistent_flag() noexcept          { return _consistent_flag; }

        inline auto& depend_id();

        virtual void set_impl() noexcept;
        virtual void unset_impl() noexcept;
        virtual void set_only_impl() noexcept;
        virtual void unset_only_impl() noexcept;
        /// Common for all - `set`, `unset` and `reset`
        virtual void reset_impl() noexcept;

        virtual Flag computable_body() const noexcept;

        virtual Flag consistent_body();

        virtual String value_to_string_body() const;

        bool lequals(const This&) const noexcept;
    private:
        virtual Depend_ids& depend_ids() const                { throw error; }

        Keys_ptr _keys_ptr{};
        Ids_map_ptr _ids_map_ptr{};
        Id _id{};

        bool _is_set{};
        mutable Flag _computable_flag{};
        bool _computed{};

        Flag _consistent_flag{};
    };

    template <typename B, typename Sort>
    class Mixin : public Inherit<B, Mixin<B, Sort>> {
    public:
        using Inherit = unsot::Inherit<B, Mixin<B, Sort>>;
        using typename Inherit::This;
        using typename Inherit::Base;
        using Var_t = This;

        static_assert(!Inherit::is_base_v);

        using Value = Sort;
        using Values = expr::Values<Value>;
        using Values_ptr = expr::Values_ptr<Value>;

        using typename Inherit::Depend_ids;

        /// By default, all plain `Var's share the same dependencies
        /// ... which are however usually supposed to be empty
        static inline Depend_ids shared_depend_ids;

        Mixin()                                                     = default;
        virtual ~Mixin()                                            = default;
        Mixin(const Mixin&)                                          = delete;
        Mixin& operator =(const Mixin&)                              = delete;
        Mixin(Mixin&&)                                              = default;
        Mixin& operator =(Mixin&&)                                  = default;
        Mixin& operator =(Value);

        using Inherit::cons_tp;
        template <typename T, typename V = Dummy>
            static T cons_tp(Keys_ptr&, Values_ptr&, Ids_map_ptr&, Key, V = {});

        const Depend_ids& cdepend_ids() const override    { return shared_depend_ids; }

        Mixin& set(Value) noexcept;

        /// Only gets its value (which must be set), does NOT compute
        Value get() const;
        /// Evaluates (computes) only when unset; must be evaluable
        Value eval();
        /// Always computes the result; must be computable
        Value compute();

        explicit operator Value() const                      { return get(); }

        Optional<Value> try_get() const noexcept;
        /// The bool is true iff the value was computed now
        Pair<Optional<Value>, bool> try_eval() noexcept;
        Optional<Value> try_compute() noexcept(!_debug_);

        using Inherit::equals;
        virtual bool equals(const Value&) const noexcept;
    protected:
        explicit Mixin(Keys_ptr, Values_ptr, Ids_map_ptr, Id, bool is_set_ = false);

        const auto& cvalues_ptr() const noexcept       { return _values_ptr; }
        auto& values_ptr() noexcept                    { return _values_ptr; }
        const auto& cvalues() const noexcept        { return *cvalues_ptr(); }
        const auto& values() const& noexcept             { return cvalues(); }
        auto& values()& noexcept                     { return *values_ptr(); }
        auto&& values()&& noexcept                  { return move(values()); }
        const Value& cvalue() const noexcept;
        const auto& value() const& noexcept               { return cvalue(); }
        Value& value()& noexcept;
        auto&& value()&& noexcept                    { return move(value()); }

        virtual void set_value_impl(Value) noexcept;
        virtual void set_value_only_impl(Value) noexcept;

        virtual Value compute_body();
        /// Prepare everything necessary before gaining the result. value
        /// If the prep. fails, return false
        virtual bool compute_body_prepare();
        /// Actually compute the resulting value
        virtual Value compute_body_value(bool success);
        virtual void compute_body_finish(bool success);

        Flag consistent_body() override;

        String value_to_string_body() const override;

        bool lequals(const This&) const noexcept;
    private:
        Values_ptr _values_ptr{};
    };

    namespace aux::visit {
        struct Base {
            static inline bool is_first_visited{true};
        };

        template <typename Sort>
        struct Sort_base : Base {
            static inline Hash<Id> visited_ids;
        };
    }

    /// For purposes of cyclic dependencies avoidance
    template <typename B>
    class Visit_mixin : public Inherit<B, Visit_mixin<B>>,
                        protected aux::visit::Sort_base<typename B::Value> {
    public:
        using Inherit = unsot::Inherit<B, Visit_mixin<B>>;
        using Visit_sort_base = aux::visit::Sort_base<typename B::Value>;
        using typename Inherit::This;
        using typename Inherit::Base;
        using Visit_t = This;

        using Inherit::Parent::operator =;
        Visit_mixin()                                               = default;
        virtual ~Visit_mixin()                                      = default;
        Visit_mixin(const Visit_mixin&)                              = delete;
        Visit_mixin& operator =(const Visit_mixin&)                  = delete;
        Visit_mixin(Visit_mixin&&)                                  = default;
        Visit_mixin& operator =(Visit_mixin&&)                      = default;

        template <typename Arg, typename FirstPred, typename VisitedPred>
            static Flag visit_id(const Id&, const Arg&, FirstPred, VisitedPred) noexcept;
    protected:
        template <typename> friend class Visit_mixin;

        using Visit_sort_base::is_first_visited;
        using Visit_sort_base::visited_ids;

        using Inherit::Inherit;

        template <typename VisitorT> auto make_visited_ids_scope_guard() const noexcept;
    };

    //! It is not designed well, it does not work with non-auto assignees
    //! It does not work at all at the moment (after the refactoring)...
    template <typename B>
    class Distance_mixin : public Inherit<B, Distance_mixin<B>> {
    public:
        using Inherit = unsot::Inherit<B, Distance_mixin<B>>;
        using typename Inherit::This;
        using typename Inherit::Base;
        using Distance_t = This;

        using Inherit::Parent::operator =;
        Distance_mixin()                                            = default;
        virtual ~Distance_mixin()                                   = default;
        Distance_mixin(const Distance_mixin&)                        = delete;
        Distance_mixin& operator =(const Distance_mixin&)            = delete;
        Distance_mixin(Distance_mixin&&)                            = default;
        Distance_mixin& operator =(Distance_mixin&&)                = default;

        const Optional<Distance>& cdistance() const noexcept
                                                         { return _distance; }
        const Optional<Distance>& distance() const noexcept
                                                       { return cdistance(); }
        Optional<Distance>& distance() noexcept          { return _distance; }

        Distance eval_distance() noexcept;
        virtual Distance compute_distance() noexcept;
    protected:
        using Inherit::Inherit;

        void reset_impl() noexcept override;
    private:
        Optional<Distance> _distance{};
    };
}

#include "expr/var/fun.hpp"
#include "expr/var/assign.hpp"

#include "expr/var.inl"
