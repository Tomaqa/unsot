#pragma once

#include "expr/formula.hpp"

#include "util/flag.hpp"
#include "util/union.hpp"
#include "util/fun.hpp"
#include "util/fun/alg.hpp"
#include "util/hash.hpp"

namespace unsot::expr::fun {
    namespace conf {
        template <typename ConfT> using Is_fun = Fwd_t<typename ConfT::Is_fun>;
        template <typename ConfT> using Is_pred = Fwd_t<typename ConfT::Is_pred>;

        template <typename ConfT> constexpr bool is_fun_v = enabled_bool_v<Is_fun, ConfT>;
        template <typename ConfT> constexpr bool is_pred_v = enabled_bool_v<Is_pred, ConfT>;
    }

    struct Conf {
        using Is_fun = true_type;
        using Is_pred = true_type;
    };
    struct Fun_conf : Conf {
        using Is_pred = false_type;
    };
    struct Pred_conf : Conf {
        using Is_fun = false_type;
    };

    class Fun_base;
    template <typename Arg, typename ConfT = Conf> class Fun_tp;

    template <typename T, typename U = T, typename V = U> struct Opers;

    using Base = Fun_base;
    template <typename Arg, typename ConfT = Conf>
        using Tp = Fun_tp<Arg, ConfT>;

    template <typename Arg, typename ConfT>
        constexpr bool ret_is_union_v = conf::is_fun_v<ConfT>
                                     && conf::is_pred_v<ConfT>
                                     && !is_bool_v<Arg>;
    template <typename Arg, typename ConfT = Conf>
        using Ret = Cond_t<ret_is_union_v<Arg, ConfT>, Union<Arg, Bool>,
                    Cond_t<conf::is_fun_v<ConfT>, Arg,
                    Cond_t<conf::is_pred_v<ConfT>, Bool,
                    void>>>;

    template <typename Arg, typename ConfT = Conf>
        Arg ret_to_value(Ret<Arg, ConfT>) noexcept;
    template <typename Arg, typename ConfT = Conf>
        Arg ret_to_value_check(Ret<Arg, ConfT>);

    template <typename Arg, typename E = Elem>
        Flag evaluable(const Ptr&) noexcept;
    template <typename RetT, typename Arg = RetT, typename E = Elem,
              typename P, Req<is_ptr_v<P>> = 0>
        Optional<RetT> eval(P&&) noexcept;
    template <typename RetT, typename Arg = RetT, typename E = Elem,
              typename P, Req<is_ptr_v<P>> = 0>
        RetT eval_check(P&&);

    Idx find_key_id(const Base&, const Key_id&);
}

namespace unsot::expr {
    template <typename Arg> using Fun = fun::Tp<Arg, fun::Fun_conf>;
    template <typename Arg> using Pred = fun::Tp<Arg, fun::Pred_conf>;

    template <typename Arg, typename L, Req<is_list_v<L>> = 0>
        Ptr new_fun(L&&, Keys_ptr, Values_ptr<Arg> = nullptr, Ids_map_ptr = nullptr);
    template <typename Arg, typename L, Req<is_list_v<L>> = 0>
        Ptr new_fun(Key, L&&, Keys_ptr, Values_ptr<Arg> = nullptr, Ids_map_ptr = nullptr);
}

namespace unsot::expr::fun {
    namespace aux {
        class Args_base {
        public:
            using Key_ids = Hash<Key_id>;
            using Key_ids_ptr = Shared_ptr<Key_ids>;

            Args_base()                                             = default;
            ~Args_base()                                            = default;
            Args_base(const Args_base&)                             = default;
            Args_base& operator =(const Args_base&)                 = default;
            Args_base(Args_base&&)                                  = default;
            Args_base& operator =(Args_base&&)                      = default;
            Args_base(Keys_ptr, Ids_map_ptr = nullptr);

            const auto& ckeys_ptr() const noexcept       { return _keys_ptr; }
            const Keys& ckeys() const noexcept        { return *ckeys_ptr(); }
            const Keys& keys() const& noexcept             { return ckeys(); }
            Keys&& keys()&& noexcept                  { return move(keys()); }

            const auto& cids_map_ptr() const noexcept { return _ids_map_ptr; }
            const Ids_map& cids_map() const noexcept
                                                   { return *cids_map_ptr(); }
            const Ids_map& ids_map() const& noexcept    { return cids_map(); }
            Ids_map&& ids_map()&& noexcept         { return move(ids_map()); }

            const auto& ckey_ids_ptr() const noexcept { return _key_ids_ptr; }
            const Key_ids& ckey_ids() const noexcept
                                                   { return *ckey_ids_ptr(); }
            const Key_ids& key_ids() const& noexcept    { return ckey_ids(); }
            Key_ids&& key_ids()&& noexcept         { return move(key_ids()); }

            inline const Key_id& ckey_id(const Key&) const;

            inline const Key_ids_ptr& cpart_key_ids_ptr(Idx) const noexcept;
            const Key_ids& cpart_key_ids(Idx idx) const noexcept
                                           { return *cpart_key_ids_ptr(idx); }
        protected:
            using Part_key_ids_ptrs = Vector<Key_ids_ptr>;

            static Key_ids_ptr new_key_ids(Key_ids&& = {});

            void linit_with_args(Keys_ptr, Ids_map_ptr);

            auto& keys_ptr() noexcept                    { return _keys_ptr; }
            Keys& keys()& noexcept                     { return *keys_ptr(); }

            auto& ids_map_ptr() noexcept              { return _ids_map_ptr; }
            Ids_map& ids_map()& noexcept            { return *ids_map_ptr(); }

            auto& key_ids_ptr() noexcept              { return _key_ids_ptr; }
            Key_ids& key_ids()& noexcept            { return *key_ids_ptr(); }

            const auto& cpart_key_ids_ptrs() const noexcept
                                                { return _part_key_ids_ptrs; }
            auto& part_key_ids_ptrs() noexcept  { return _part_key_ids_ptrs; }
            inline Key_ids_ptr& part_key_ids_ptr(Idx) noexcept;
            Key_ids& part_key_ids(Idx idx) noexcept
                                            { return *part_key_ids_ptr(idx); }
        private:
            Keys_ptr _keys_ptr{};
            Ids_map_ptr _ids_map_ptr{};

            Key_ids_ptr _key_ids_ptr{};
            Part_key_ids_ptrs _part_key_ids_ptrs{};
        };

        template <typename Arg>
        class Values_base {
        public:
            using Value = Arg;
            using Values = expr::Values<Arg>;
            using Values_ptr = expr::Values_ptr<Arg>;

            Values_base()                                           = default;
            ~Values_base()                                          = default;
            Values_base(const Values_base&)                         = default;
            Values_base& operator =(const Values_base&)             = default;
            Values_base(Values_base&&)                              = default;
            Values_base& operator =(Values_base&&)                  = default;
            Values_base(Values_ptr);
            Values_base(const Keys&, Values_ptr = nullptr);

            const auto& cvalues_ptr() const noexcept   { return _values_ptr; }
            auto& values_ptr() const noexcept          { return _values_ptr; }
            const auto& cvalues() const noexcept    { return *cvalues_ptr(); }
            auto& values() const& noexcept           { return *values_ptr(); }
            auto&& values() const&& noexcept        { return move(values()); }
        private:
            mutable Values_ptr _values_ptr{};
        };
    }

    class Fun_base : public Inherit<Formula, Fun_base>,
                     public aux::Args_base {
    public:
        Fun_base()                                                  = default;
        virtual ~Fun_base()                                         = default;
        Fun_base(const Fun_base&)                                   = default;
        Fun_base& operator =(const Fun_base&)                       = default;
        Fun_base(Fun_base&&)                                        = default;
        Fun_base& operator =(Fun_base&&)                            = default;
        Fun_base(Fun_base& fun)                  : Fun_base(as_const(fun)) { }
        template <typename L, Req<is_list_v<L>> = 0>
            Fun_base(L&&, Keys_ptr, Ids_map_ptr = nullptr);
        template <typename L, Req<is_list_v<L>> = 0>
            Fun_base(Key, L&&, Keys_ptr, Ids_map_ptr = nullptr);
        template <typename L, Req<is_list_v<L>> = 0> Fun_base(L&&);
        template <typename L, Req<is_list_v<L>> = 0> Fun_base(Key, L&&);

        //+ constructors without ptrs should not be supported by `cons_tp`
        virtual void virtual_init_with_args(Keys_ptr, Ids_map_ptr);

        size_t increased_size() const noexcept     { return _increased_size; }

        //+ not updated after insertion/deletion (ids etc.)

        void reserve(size_t) override;
        void resize(size_t) override;

        /// Revert changes made to shared keys
        void revert_keys();

        String to_string() const& override;
        String to_string()&& override;
    protected:
        bool lvalid_post_init() const noexcept;
        void lcheck_post_init() const;

        void init_arg_list(Ptr&) override;
        Ptr init_arg_list_new(Ptr&&) override;
        void init_arg_key(Ptr&) override;
        virtual Pair<bool, Key_id> init_arg_key_add(Ptr&);
        void init_arg_value(Ptr&) override;

        size_t& increased_size()  noexcept         { return _increased_size; }

        virtual void reserve_keys(size_t);
        virtual void resize_keys(size_t);

        virtual Key_id add_key(Key);
    private:
        template <typename F> static void reserve_impl(F&, size_t);
        template <typename F> static void resize_impl(F&, size_t);
        template <typename F> static void reserve_keys_impl(F&, size_t);
        template <typename F> static void resize_keys_impl(F&, size_t);

        virtual Key_id add_key_impl(Key);

        template <typename F> static String to_string_impl(F&&);

        /// (zero-initialization)
        size_t _increased_size{};
    };

    template <typename Arg, typename ConfT>
    class Fun_tp : public Inherit<Fun_base, Fun_tp<Arg, ConfT>>,
                   public aux::Values_base<Arg> {
    public:
        using Inherit = unsot::Inherit<Fun_base, Fun_tp<Arg, ConfT>>;
        using Values_base = aux::Values_base<Arg>;
        using typename Inherit::This;

        class Run;

        using typename Values_base::Value;
        using typename Values_base::Values;
        using typename Values_base::Values_ptr;

        static constexpr bool is_fun_v = conf::is_fun_v<ConfT>;
        static constexpr bool is_pred_v = conf::is_pred_v<ConfT>;
        static constexpr bool is_bool_v = expr::is_bool_v<Value>;

        static_assert(is_fun_v || is_pred_v);

        using Ret = fun::Ret<Value, ConfT>;

        using Opers = fun::Opers<Value>;

        Fun_tp()                                                    = default;
        virtual ~Fun_tp()                                           = default;
        Fun_tp(const Fun_tp&)                                       = default;
        Fun_tp& operator =(const Fun_tp&)                           = default;
        Fun_tp(Fun_tp&&)                                            = default;
        Fun_tp& operator =(Fun_tp&&)                                = default;
        Fun_tp(Fun_tp& fun)                        : Fun_tp(as_const(fun)) { }
        template <typename L, Req<is_list_v<L>> = 0>
            Fun_tp(L&&, Keys_ptr, Values_ptr = nullptr, Ids_map_ptr = nullptr);
        template <typename L, Req<is_list_v<L>> = 0>
            Fun_tp(Key, L&&,  Keys_ptr, Values_ptr = nullptr, Ids_map_ptr = nullptr);
        template <typename L, Req<is_list_v<L>> = 0> Fun_tp(L&&);
        template <typename L, Req<is_list_v<L>> = 0> Fun_tp(Key, L&&);

        //+ constructors without ptrs should not be supported by `cons_tp`
        void virtual_init_with_args(Keys_ptr, Ids_map_ptr) override;
        virtual void virtual_init_with_args(Keys_ptr, Values_ptr, Ids_map_ptr);

        void reserve(size_t) override;
        void resize(size_t) override;

        Ret operator ()(initializer_list<Value> ils) const
                                             { return eval_check(move(ils)); }
        template <typename V, Req<is_values_v<V>> = 0>
            Ret operator ()(V&& vals) const
                                         { return eval_check(FORWARD(vals)); }
        Ret operator ()() const                       { return eval_check(); }

        Optional<Ret> eval(initializer_list<Value> ils) const noexcept;
        template <typename Vs, Req<is_values_v<Vs>> = 0>
            Optional<Ret> eval(Vs&&) const noexcept;
        Optional<Ret> eval() const noexcept;
        Ret eval_check(initializer_list<Value> ils) const;
        template <typename Vs, Req<is_values_v<Vs>> = 0>
            Ret eval_check(Vs&&) const;
        Ret eval_check() const;

        template <typename V = Value> static V ret_to_value(Ret) noexcept;

        //+ no option to check
        Value eval_part(Idx) const noexcept;

        String to_string() const& override;
        String to_string()&& override;
    protected:
        using Lazy_value = Lazy<Value>;
        using Lazy_values = Vector<Lazy_value>;

        using Un_f = typename Opers::Un_f;
        using Bin_f = typename Opers::Bin_f;
        using Ter_f = typename Opers::Ter_f;
        using Un_pred = typename Opers::Un_pred;
        using Bin_pred = typename Opers::Bin_pred;
        using Ter_pred = typename Opers::Ter_pred;
        using Un_bool = typename Opers::Un_bool;
        using Bin_bool = typename Opers::Bin_bool;
        using Ter_bool = typename Opers::Ter_bool;
        using Cont_f = util::Container_f<Lazy_values, Bin_f>;
        using Cont_pred = util::Container_f<Lazy_values, Bin_pred>;
        using Cont_bool = util::Container_f<Lazy_values, Bin_bool>;

        static constexpr bool f_same_as_pred = is_same_v<Un_f, Un_pred>;
        using F_holder = Cond_t<is_fun_v, Cond_t<is_pred_v && !f_same_as_pred,
            Union<Lazy_value, Un_f, Bin_f, Ter_f, Cont_f,
                  Un_pred, Bin_pred, Ter_pred, Cont_pred,
                  Un_bool, Bin_bool, Ter_bool, Cont_bool>,
            Union<Lazy_value, Un_f, Bin_f, Ter_f, Cont_f,
                  Un_bool, Bin_bool, Ter_bool, Cont_bool>>,
            Union<Lazy_value, Un_pred, Bin_pred, Ter_pred, Cont_pred,
                  Un_bool, Bin_bool, Ter_bool, Cont_bool>
        >;

        void linit();

        void init_oper() override;

        void init_arg_list(Ptr&) override;
        Ptr init_arg_list_new(Ptr&&) override;
        Pair<bool, Key_id> init_arg_key_add(Ptr&) override;
        void init_arg_value(Ptr&) override;

        virtual Lazy_value make_lazy_fun(const Ptr& ptr) const noexcept;
        virtual Lazy_value make_lazy_key(const Key_id&) const noexcept;
        virtual Lazy_value make_lazy_value(Value) const noexcept;

        virtual void simplify();
        virtual void simplify_to_lazy_value();
        virtual void simplify_unary();

        const auto& clazy_values() const noexcept     { return _lazy_values; }
        const auto& lazy_values() const noexcept    { return clazy_values(); }
        auto& lazy_values() noexcept                  { return _lazy_values; }
        const auto& clazy_value(Idx idx) const noexcept
                                               { return clazy_values()[idx]; }
        const auto& lazy_value(Idx idx) const noexcept
                                                  { return clazy_value(idx); }
        auto& lazy_value(Idx idx) noexcept      { return lazy_values()[idx]; }

        const auto& cf_holder() const noexcept           { return _f_holder; }
        const auto& f_holder() const noexcept          { return cf_holder(); }
        auto& f_holder() noexcept                        { return _f_holder; }

        void reserve_keys(size_t) override;
        void resize_keys(size_t) override;
    private:
        Key_id add_key_impl(Key) override;

        template <typename F> static String to_string_impl(F&&);

        Lazy_values _lazy_values{};

        F_holder _f_holder{};
    };
}

#include "expr/opers.hpp"

#include "expr/fun.inl"
