#pragma once

#include "expr/formula.hpp"

namespace unsot::expr::bools {
    struct Lit;
    struct Clause;
    class Cnf;

    using Var_id = Key;

    template <typename T> constexpr bool is_lit_v = is_derived_from_v<Decay<T>, Lit>;
    template <typename T> constexpr bool is_clause_v = is_derived_from_v<Decay<T>, Clause>;

    constexpr const char* neg_prefix = "~";

    Var_id var_id(Var_id, Idx);

    bool is_neg(const Key&) noexcept;

    constexpr Bool neg(Bool val)                              { return !val; }
    Key neg(const Key&);
    Key neg(Key&&);
    template <typename E = Elem, typename E_, Req<is_elem_v<E_, E>> = 0> Decay<E_> neg(E_&&);
    template <typename L, Req<is_lit_v<L>> = 0> Decay<L> neg(L&&);
    template <typename C, Req<is_clause_v<C>> = 0> Decay<C> neg(C&&);

    template <typename C, Req<is_clause_v<C>> = 0> Clause to_conflict(C&&);
    template <typename C, Req<is_clause_v<C>> = 0> Cnf to_pair_conflict(C&&);

    extern template Clause to_conflict(const Clause&);
    extern template Clause to_conflict(Clause&&);
    extern template Cnf to_pair_conflict(const Clause&);
    extern template Cnf to_pair_conflict(Clause&&);
}

namespace unsot::expr {
    template <typename E = Elem, typename... Args> Ptr new_bool(Args&&...);

    template <typename E = Elem, typename T>
        Forward_as<Bool, T> cast_bool(T&&);
    template <typename E = Elem, typename P, Req<is_ptr_v<P>> = 0>
        Forward_as<Bool, P> cast_bool_check(P&&);

    template <typename E = Elem, typename P, Req<is_ptr_v<P>> = 0>
        auto cast_bool_l(P&&) noexcept;

    template <typename E = Elem> bool is_bool(const Ptr&) noexcept;
    template <typename E = Elem> void check_is_bool(const Ptr&);

    template <typename E = Elem, typename T>
        Optional<Bool> to_bool(T&&) noexcept;
    template <typename E = Elem, typename P, Req<is_ptr_v<P>> = 0>
        Bool to_bool_check(P&&);
}

namespace unsot::expr::bools {
    //+ inherit from `Elem`
    struct Lit : Inherit<Key> {
        using Inherit::Inherit;
        Lit()                                                       = default;
        /// `int` prevents ambiguous overload from `string`
        explicit Lit(Var_id, int sign_);
        template <typename E = Elem, typename E_, Req<is_elem_v<E_, E>> = 0>
            Lit(E_&&, bool sign_ = true, Tag<E> = {});

        bool is_neg() const noexcept;
        bool sign() const noexcept;

        Lit& neg();
        Lit to_neg() const&;
        Lit to_neg()&&;

        Var_id cvar_id() const&;
        Var_id var_id() const&                           { return cvar_id(); }
        Var_id var_id()&&;

        Ptr to_ptr() const&;
        Ptr to_ptr()&&;
    };

    struct Clause : Inherit<Vector<Lit>, Clause> {
        using Inherit::Inherit;
        template <typename K, Req<is_keys_v<K>> = 0> Clause(K&&);

        Clause& neg();
        Clause to_neg() const&;
        Clause&& to_neg()&&;

        Ptr to_ptr() const&;
        Ptr to_ptr()&&;

        String to_string() const&;
        String to_string()&&;
    };
    extern template Clause::Clause(const Keys&);
    extern template Clause::Clause(Keys&&);

    /// Converts arbitrary boolean expr. into CNF via Tseitin transformation
    class Cnf : public Inherit<Vector<Clause>> {
    public:
        static constexpr const char* def_var = "_x";

        using Inherit::Inherit;
        Cnf()                                                       = default;
        ~Cnf()                                                      = default;
        Cnf(const Cnf&)                                             = default;
        Cnf& operator =(const Cnf&)                                 = default;
        Cnf(Cnf&&)                                                  = default;
        Cnf& operator =(Cnf&&)                                      = default;
        template <typename L, Req<is_list_v<L>> = 0> Cnf(L&&, Idx init_idx_, Var_id = def_var);
        template <typename L, Req<is_list_v<L>> = 0> Cnf(L&&, Var_id = def_var);

        const auto& cx() const noexcept                         { return _x; }
        const auto& x() const noexcept                        { return cx(); }
        Idx init_idx() const noexcept                    { return _init_idx; }
        Idx last_idx() const noexcept;

        size_t increased_size() const noexcept;

        Formula to_formula() const&;
        Formula to_formula()&&;
    protected:
        static inline long gcounter = 1;

        long counter() const noexcept                   { return _counter; }
        long& counter() noexcept                        { return _counter; }

        Var_id new_var();

        void add_not(Lit p, Formula& not_phi);
        void add_and(Lit p, Formula& and_phi);
        void add_or(Lit p, Formula& and_phi);
        void add_bin_and(Lit p, Formula& and_phi);
        void add_bin_or(Lit p, Formula& or_phi);
        void add_bin_xor(Lit p, Formula& xor_phi);
        void add_bin_iff(Lit p, Formula& iff_phi);
        void add_bin_implies(Lit p, Formula& impl_phi);

        void parse(Ptr&, bool is_top = false, bool neg_ = false);
    private:
        template <typename L, Req<is_list_v<L>> = 0> void parse_top(L&&);
        void parse_elem(Elem&, bool is_top, bool neg_);
        void parse_elems(Formula&, const Var_id&, bool is_and, bool is_or);

        Idx _init_idx{};
        Var_id _x{};
        long _counter = init_idx();
    };
    extern template void Cnf::parse_top(const List&);
    extern template void Cnf::parse_top(List&&);
    extern template void Cnf::parse_top(const Formula&);
    extern template void Cnf::parse_top(Formula&&);
}

#include "expr/bools.inl"
