#pragma once

namespace unsot::expr {
    constexpr Elem_base::Elem_base(const Key& key_)
    {
        check_key(key_);
    }

    bool Elem_base::is_me(const Ptr& ptr) noexcept
    {
        assert(ptr);
        assert(!ptr->is_elem() || !ptr->is_list());
        return ptr->is_elem();
    }

    ////////////////////////////////////////////////////////////////

    template <typename... Args>
    template <typename K, Req<is_key_v<K>>>
    constexpr Elem_tp<Args...>::Elem_tp(K&& key_)
        : Inherit(key_), Union(FORWARD(key_))
    { }

    template <typename... Args>
    template <typename Arg, Req<!is_key_v<Arg>>>
    constexpr Elem_tp<Args...>::Elem_tp(Arg&& arg)
        : Union(FORWARD(arg))
    { }

    template <typename... Args>
    template <typename Arg>
    Elem_tp<Args...>& Elem_tp<Args...>::operator =(Arg&& arg)
    {
        emplace<Arg>(FORWARD(arg));
        return *this;
    }

    template <typename... Args>
    template <typename Arg>
    constexpr const auto& Elem_tp<Args...>::cget_value() const
    {
        return this->template cget<Arg>();
    }

    template <typename... Args>
    template <typename Arg>
    constexpr const auto& Elem_tp<Args...>::get_value() const&
    {
        return this->template get<Arg>();
    }

    template <typename... Args>
    template <typename Arg>
    constexpr auto& Elem_tp<Args...>::get_value()&
    {
        return this->template get<Arg>();
    }

    template <typename... Args>
    template <typename Arg>
    constexpr auto&& Elem_tp<Args...>::get_value()&&
    {
        return move(*this).template get<Arg>();
    }

    template <typename... Args>
    template <typename Arg>
    constexpr auto Elem_tp<Args...>::cget_value_l() const noexcept
    {
        return this->template cget_l<Arg>();
    }

    template <typename... Args>
    template <typename Arg>
    constexpr auto Elem_tp<Args...>::get_value_l() const noexcept
    {
        return this->template get_l<Arg>();
    }

    template <typename... Args>
    template <typename Arg>
    constexpr auto Elem_tp<Args...>::get_value_l() noexcept
    {
        return this->template get_l<Arg>();
    }

    template <typename... Args>
    const Key& Elem_tp<Args...>::cget_key() const
    {
        return cget_value<Key>();
    }

    template <typename... Args>
    const Key& Elem_tp<Args...>::get_key() const&
    {
        return get_value<Key>();
    }

    template <typename... Args>
    Key& Elem_tp<Args...>::get_key()&
    {
        return get_value<Key>();
    }

    template <typename... Args>
    Key&& Elem_tp<Args...>::get_key()&&
    {
        return move(*this).template get_value<Key>();
    }

    template <typename... Args>
    constexpr auto Elem_tp<Args...>::cget_key_l() const noexcept
    {
        return cget_value_l<Key>();
    }

    template <typename... Args>
    constexpr auto Elem_tp<Args...>::get_key_l() const noexcept
    {
        return get_value_l<Key>();
    }

    template <typename... Args>
    constexpr auto Elem_tp<Args...>::get_key_l() noexcept
    {
        return get_value_l<Key>();
    }

    template <typename... Args>
    template <typename Arg>
    constexpr bool Elem_tp<Args...>::is_value() const noexcept
    {
        return this->template valid<Arg>();
    }

    template <typename... Args>
    bool Elem_tp<Args...>::is_key() const noexcept
    {
        return is_value<Key>();
    }

    template <typename... Args>
    template <typename Arg, typename... Ts>
    Arg& Elem_tp<Args...>::emplace(Ts&&... args)
    {
        auto& arg = Union::template emplace<Arg>(FORWARD(args)...);
        if constexpr (is_key_v<DECAY(arg)>) check_key(arg);
        return arg;
    }

    template <typename... Args>
    template <typename Arg>
    Optional<Arg> Elem_tp<Args...>::to_value() const& noexcept
    {
        return this->template to<Arg>();
    }

    template <typename... Args>
    template <typename Arg>
    Optional<Arg> Elem_tp<Args...>::to_value()&& noexcept
    {
        return move(*this).template to<Arg>();
    }

    template <typename... Args>
    template <typename Arg>
    Arg Elem_tp<Args...>::to_value_check() const&
    {
        return to_value_check_impl<Arg>(*this);
    }

    template <typename... Args>
    template <typename Arg>
    Arg Elem_tp<Args...>::to_value_check()&&
    {
        return to_value_check_impl<Arg>(move(*this));
    }

    template <typename... Args>
    template <typename Arg, typename E>
    Arg Elem_tp<Args...>::to_value_check_impl(E&& elem)
    {
        if constexpr (is_key_v<Arg>) {
            elem.check_is_key();
            return FORWARD(elem).template to_value<Arg>().value();
        }
        else try { return to_check<Arg>(FORWARD(elem)); }
        catch (const Error& err) {
            THROW("Conversion of expr element to value failed:\n") + err;
        }
    }

    template <typename... Args>
    String Elem_tp<Args...>::to_string() const&
    {
        return Union::to_string();
    }

    template <typename... Args>
    String Elem_tp<Args...>::to_string()&&
    {
        return move(*this).Union::to_string();
    }

    template <typename... Args>
    bool Elem_tp<Args...>::equals(const This& rhs) const noexcept
    {
        return static_cast<const Union&>(*this).equals(rhs);
    }

    //+ template <typename Arg>
    //+ size_t Elem_tp::hash() const
    //+ {
    //+     return std::hash<Arg>()(cvalue());
    //+ }
}

////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////

namespace unsot::expr {
    template <typename Arg, typename E, typename... Args>
    Ptr new_value(Args&&... args)
    {
        return E::new_me(Arg(FORWARD(args)...));
    }

    template <typename E>
    Ptr new_key()
    {
        return new_value<Key, E>();
    }

    template <typename E, typename T, typename... Args>
    Ptr new_key(T&& first, Args&&... args)
    {
        /// construction from reference to char would fail
        if constexpr (is_same_v<DECAY(first), char>)
            return new_value<Key, E>(to_string(first), FORWARD(args)...);
        else return new_value<Key, E>(FORWARD(first), FORWARD(args)...);
    }

    template <typename E, typename... Args>
    Ptr new_int(Args&&... args)
    {
        return new_value<Int, E>(FORWARD(args)...);
    }

    template <typename E, typename... Args>
    Ptr new_real(Args&&... args)
    {
        return new_value<Real, E>(FORWARD(args)...);
    }

    template <typename E, typename T>
    Ptr new_elem(T&& arg)
    {
        if constexpr (is_same_v<DECAY(arg), char>) return new_elem<E>(to_string(arg));
        else if constexpr (is_integral_v<T>) return new_int<E>(FORWARD(arg));
        else if constexpr (is_floating_point_v<T>) return new_real<E>(FORWARD(arg));
        else if constexpr (is_key_v<T>) {
            Int ival;
            if (get_value(ival, arg)) return new_int<E>(move(ival));
            Real rval;
            if (get_value(rval, arg)) return new_real<E>(move(rval));
            return new_key<E>(FORWARD(arg));
        }
        else static_assert(false_tp<T>);
    }

    template <typename T, Req<is_ptr_v<T> || is_elem_base_v<T>>>
    Forward_as_ref<Key, T> cast_key(T&& t)
    {
        return Elem_base::cast(FORWARD(t)).get_key();
    }

    template <typename P, Req<is_ptr_v<P>>>
    Forward_as_ref<Key, P> cast_key_check(P&& ptr)
    {
        return Elem_base::cast_check(FORWARD(ptr)).get_key_check();
    }

    template <typename Arg, typename E, typename T, Req<is_ptr_v<T> || is_elem_v<T, E>>>
    Forward_as_ref<Arg, T> cast_value(T&& t)
    {
        return E::cast(FORWARD(t)).template get_value<Arg>();
    }

    template <typename Arg, typename E, typename P, Req<is_ptr_v<P>>>
    Forward_as_ref<Arg, P> cast_value_check(P&& ptr)
    {
        return E::cast_check(FORWARD(ptr)).template get_value<Arg>();
    }

    template <typename Arg, typename E, typename P, Req<is_ptr_v<P>>>
    auto cast_value_l(P&& ptr) noexcept
    {
        //+ it not tested whether it is `E` or not ...
        return E::cast(FORWARD(ptr)).template get_value_l<Arg>();
    }

    template <typename E, typename T, Req<is_ptr_v<T> || is_elem_v<T, E>>>
    Forward_as_ref<Int, T> cast_int(T&& t)
    {
        return cast_value<Int, E>(FORWARD(t));
    }

    template <typename E, typename T, Req<is_ptr_v<T> || is_elem_v<T, E>>>
    Forward_as_ref<Real, T> cast_real(T&& t)
    {
        return cast_value<Real, E>(FORWARD(t));
    }

    template <typename E, typename P, Req<is_ptr_v<P>>>
    Forward_as_ref<Int, P> cast_int_check(P&& ptr)
    {
        return cast_value_check<Int, E>(FORWARD(ptr));
    }

    template <typename E, typename P, Req<is_ptr_v<P>>>
    Forward_as_ref<Real, P> cast_real_check(P&& ptr)
    {
        return cast_value_check<Real, E>(FORWARD(ptr));
    }

    template <typename E, typename P, Req<is_ptr_v<P>>>
    auto cast_key_l(P&& ptr) noexcept
    {
        //+ it not tested whether it is `E` or not ...
        return E::cast(FORWARD(ptr)).get_key_l();
    }

    template <typename E, typename P, Req<is_ptr_v<P>>>
    auto cast_int_l(P&& ptr) noexcept
    {
        return cast_value_l<Int, E>(FORWARD(ptr));
    }

    template <typename E>
    auto cast_real_l(Ptr& ptr) noexcept
    {
        return cast_value_l<Real, E>(FORWARD(ptr));
    }

    template <typename Arg, typename E>
    bool is_value(const Ptr& ptr) noexcept
    {
        if (!E::is_me(ptr)) return false;
        auto& elem = E::cast(ptr);
        if constexpr (is_void_v<Arg>) return !elem.is_key();
        else return elem.template is_value<Arg>();
    }

    template <typename Arg, typename E>
    void check_is_value(const Ptr& ptr)
    {
        if constexpr (is_void_v<Arg>) {
            expect((is_value<Arg, E>(ptr)),
                   "Expected expr element with any value (not a key), got: "s + ptr->to_string());
        }
        else cast_value_check<Arg, E>(ptr);
    }

    template <typename E>
    bool is_int(const Ptr& ptr) noexcept
    {
        return is_value<Int, E>(ptr);
    }

    template <typename E>
    bool is_real(const Ptr& ptr) noexcept
    {
        return is_value<Real, E>(ptr);
    }

    template <typename E>
    void check_is_int(const Ptr& ptr)
    {
        check_is_value<Int, E>(ptr);
    }

    template <typename E>
    void check_is_real(const Ptr& ptr)
    {
        check_is_value<Real, E>(ptr);
    }

    template <typename Arg, typename E, typename T, Req<is_ptr_v<T> || is_elem_v<T, E>>>
    Optional<Arg> to_value(T&& t) noexcept
    {
        //+ also add possibility to omit the check ?
        if constexpr (is_ptr_v<T>) if (!E::is_me(t)) return {};
        return E::cast(FORWARD(t)).template to_value<Arg>();
    }

    template <typename Arg, typename E, typename P, Req<is_ptr_v<P>>>
    Arg to_value_check(P&& ptr)
    {
        return E::cast_check(FORWARD(ptr)).template to_value_check<Arg>();
    }

    template <typename E, typename T, Req<is_ptr_v<T> || is_elem_v<T, E>>>
    Optional<Int> to_int(T&& t) noexcept
    {
        return to_value<Int, E>(FORWARD(t));
    }

    template <typename E, typename T, Req<is_ptr_v<T> || is_elem_v<T, E>>>
    Optional<Real> to_real(T&& t) noexcept
    {
        return to_value<Real, E>(FORWARD(t));
    }

    template <typename E, typename P, Req<is_ptr_v<P>>>
    Int to_int_check(P&& ptr)
    {
        return to_value_check<Int, E>(FORWARD(ptr));
    }

    template <typename E, typename P, Req<is_ptr_v<P>>>
    Real to_real_check(P&& ptr)
    {
        return to_value_check<Real, E>(FORWARD(ptr));
    }
}
