#pragma once

#include "expr/opers.hpp"

#ifdef NO_EXPLICIT_TP_INST
#include "expr/opers.tpp"
#endif

#ifndef NO_EXPLICIT_TP_INST
namespace unsot::expr::fun {
    extern template struct Opers<Int>;
    extern template struct Opers<Real>;
    extern template struct Opers<Idx>;
}
#endif  /// NO_EXPLICIT_TP_INST
