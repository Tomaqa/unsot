#pragma once

#include "expr.hpp"
#include "expr/formula.hpp"

#include "util/alg.hpp"
#include "util/alg/rec.hpp"

namespace unsot::expr {
    template <typename F = Fwd> auto as_ckeys(F = fwd);
    template <typename F = Fwd> auto as_clists(F = fwd);
    template <typename F = Fwd> auto as_cformulas(F = fwd);
    template <typename K = Key&, typename F = Fwd> auto as_keys(F = fwd);
    template <typename L = List&, typename F = Fwd> auto as_lists(F = fwd);
    template <typename L = Formula&, typename F = Fwd> auto as_formulas(F = fwd);
    template <typename F = Fwd> auto as_ckeys_check(F = fwd);
    template <typename F = Fwd> auto as_clists_check(F = fwd);
    template <typename F = Fwd> auto as_cformulas_check(F = fwd);
    template <typename K = Key&, typename F = Fwd> auto as_keys_check(F = fwd);
    template <typename L = List&, typename F = Fwd> auto as_lists_check(F = fwd);
    template <typename L = Formula&, typename F = Fwd> auto as_formulas_check(F = fwd);

    template <typename E = Elem, typename F = Fwd> auto as_celems(F = fwd);
    template <typename Arg, typename E = Elem, typename F = Fwd> auto as_cvalues(F = fwd);
    template <typename E = Elem&, typename F = Fwd> auto as_elems(F = fwd);
    template <typename Arg, typename E = Elem, typename F = Fwd> auto as_values(F = fwd);
    template <typename E = Elem, typename F = Fwd> auto as_celems_check(F = fwd);
    template <typename Arg, typename E = Elem, typename F = Fwd> auto as_cvalues_check(F = fwd);
    template <typename E = Elem&, typename F = Fwd> auto as_elems_check(F = fwd);
    template <typename Arg, typename E = Elem, typename F = Fwd> auto as_values_check(F = fwd);

    template <typename E = Elem, typename F = Fwd> auto as_cints(F = fwd);
    template <typename E = Elem, typename F = Fwd> auto as_creals(F = fwd);
    template <typename I = Int&, typename E = Elem, typename F = Fwd> auto as_ints(F = fwd);
    template <typename R = Real&, typename E = Elem, typename F = Fwd> auto as_reals(F = fwd);
    template <typename E = Elem, typename F = Fwd> auto as_cints_check(F = fwd);
    template <typename E = Elem, typename F = Fwd> auto as_creals_check(F = fwd);
    template <typename I = Int&, typename E = Elem, typename F = Fwd> auto as_ints_check(F = fwd);
    template <typename R = Real&, typename E = Elem, typename F = Fwd> auto as_reals_check(F = fwd);

    //+ template <typename UnF, typename LoopF>
    //+     auto as_clist_or_csublists(const List&, UnF,
    //+                                LoopF loop = for_each);
    //+ template <typename UnF, typename LoopF>
    //+     auto as_list_or_sublists(List&, UnF, LoopF loop = for_each);
    //+ template <typename UnF, typename LoopF>
    //+     auto as_clist_or_csublists_check(const List&, UnF,
    //+                                      LoopF loop = for_each);
    //+ template <typename UnF, typename LoopF>
    //+     auto as_list_or_sublists_check(List&, UnF,
    //+                                    LoopF loop = for_each);

    template <typename L, Req<is_list_v<L>> = 0, typename UnF>
        void for_each_if_elem_base(L&&, UnF);
    template <typename L, Req<is_list_v<L>> = 0, typename UnF>
        void for_each_if_key(L&&, UnF);
    template <typename L, Req<is_list_v<L>> = 0, typename UnF>
        void for_each_if_list(L&&, UnF);
    template <typename L, Req<is_list_v<L>> = 0, typename UnF>
        void for_each_if_formula(L&&, UnF);

    template <typename E = Elem, typename L, Req<is_list_v<L>> = 0, typename UnF>
        void for_each_if_elem(L&&, UnF);
    template <typename Arg, typename E = Elem, typename L, Req<is_list_v<L>> = 0, typename UnF>
        void for_each_if_value(L&&, UnF);

    template <typename E = Elem, typename L, Req<is_list_v<L>> = 0, typename UnF>
        void for_each_if_int(L&&, UnF);
    template <typename E = Elem, typename L, Req<is_list_v<L>> = 0, typename UnF>
        void for_each_if_real(L&&, UnF);

    template <typename L, Req<is_list_v<L>> = 0, typename UnF>
        void for_each_if_elem_base_rec(L&&, UnF);
    template <typename L, Req<is_list_v<L>> = 0, typename UnF>
        void for_each_if_key_rec(L&&, UnF);
    template <typename L, Req<is_list_v<L>> = 0, typename UnF, typename BreakPred = False_pred>
        void for_each_if_list_rec_pre(L&&, UnF, BreakPred = false_pred);
    template <typename L, Req<is_list_v<L>> = 0, typename UnF, typename BreakPred = False_pred>
        void for_each_if_list_rec_post(L&&, UnF, BreakPred = false_pred);
    template <typename L, Req<is_list_v<L>> = 0, typename UnF, typename BreakPred = False_pred>
        void for_each_if_formula_rec_pre(L&&, UnF, BreakPred = false_pred);
    template <typename L, Req<is_list_v<L>> = 0, typename UnF, typename BreakPred = False_pred>
        void for_each_if_formula_rec_post(L&&, UnF, BreakPred = false_pred);

    template <typename E = Elem, typename L, Req<is_list_v<L>> = 0, typename UnF>
        void for_each_if_elem_rec(L&&, UnF);
    template <typename Arg, typename E = Elem, typename L, Req<is_list_v<L>> = 0, typename UnF>
        void for_each_if_value_rec(L&&, UnF);

    template <typename E = Elem, typename L, Req<is_list_v<L>> = 0, typename UnF>
        void for_each_if_int_rec(L&&, UnF);
    template <typename E = Elem, typename L, Req<is_list_v<L>> = 0, typename UnF>
        void for_each_if_real_rec(L&&, UnF);

    bool is_deep_n(const List&, int levels);
    bool is_deep_flat(const List&);
    bool is_deep_sparse(const List&);

    template <typename L, Req<is_list_v<L> && !is_cref_v<L>> = 0>
        L replace(L&&, const Key& from, const Key& to);
    template <typename L, Req<is_list_v<L> && !is_cref_v<L>> = 0>
        L replace(L&&, const Keys& from, const Keys& to);
    template <typename L, Req<is_list_v<L>> = 0>
        L replace_copy(const L&, const Key& from, const Key& to);
    template <typename L, Req<is_list_v<L>> = 0>
        L replace_copy(const L&, const Keys& from, const Keys& to);

    template <typename L, Req<is_list_v<L>> = 0> String to_lines(L&&);
}

#include "expr/alg.inl"
