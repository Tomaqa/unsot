#pragma once

#include "util/scope_guard.hpp"

namespace unsot::expr {
    const auto& Preprocess::cmacro(const Key& key_) const
    {
        return cmacros_map()[key_];
    }

    auto& Preprocess::macro(const Key& key_)
    {
        return macros_map()[key_];
    }

    const auto& Preprocess::clet(const Key& key_) const
    {
        return Let::cast(clet_ptrs_map()[key_]);
    }

    auto& Preprocess::let(const Key& key_)
    {
        return Let::cast(let_ptrs_map()[key_]);
    }

    const auto& Preprocess::cexpanding_lets() const noexcept
    {
        assert(_expanding_lets != Expand_lets::neutral);
        return _expanding_lets;
    }

    auto& Preprocess::expanding_lets() noexcept
    {
        assert(_expanding_lets != Expand_lets::neutral);
        return _expanding_lets;
    }

    template <bool groupedV, typename... Args>
    Let& Preprocess::push_let(const Key& key_, Args&&... args)
    {
        auto [it, inserted] = let_ptrs_map().try_emplace(key_);
        auto& [k, ptr] = *it;
        if (inserted) ptr = Let::new_me(key_);
        auto& let_ = Let::cast(ptr);
        let_.push<groupedV>(FORWARD(args)...);
        return let_;
    }

    template <Preprocess::Expand_lets expLets, bool assertNoLets>
    void Preprocess::parse(Pos& pos, int depth)
    try {
        if (is_list_at(pos)) return parse_list<expLets, assertNoLets>(pos, depth);
        if (is_elem_at(pos)) return parse_elem<expLets, assertNoLets>(pos, depth);
        parse_let<expLets>(pos, depth);
    }
    catch (const Error& err) {
        if (depth == 0) throw;
        THROW("At level ") + to_string(depth) + ", '" + pos.to_string() + "':\n" + err;
    }

    template <Preprocess::Expand_lets expLets, bool assertNoLets>
    void Preprocess::parse_elem(Pos& pos, int depth)
    {
        if (is_key_at(pos)) return parse_key<expLets, assertNoLets>(pos, depth);
        parse_value(pos, depth);
    }

    template <Preprocess::Expand_lets expLets, bool assertNoLets>
    void Preprocess::parse_key(Pos& pos, int depth)
    {
        Key& key_ = peek_key(pos);
        Keys keys;
        if (!empty(key_)) {
            //+ escaped '#' are not considered
            expect(!util::contains(key_, Macro::three_chars_str),
                   "'###' is not permitted: "s + to_string(key_));
            maybe_escape_key(pos, key_);
            keys = split_key(key_);
        }
        if (empty(keys)) return parse_key_single<expLets, assertNoLets>(pos, key_, depth);
        parse_key_multi(pos, move(keys), depth);
    }

    void Preprocess::parse_value(Pos& pos, int /*depth*/)
    {
        assert(is_elem_at(pos) && !is_key_at(pos));
        pos.next();
    }

    template <Preprocess::Expand_lets expLets, bool assertNoLets>
    void Preprocess::parse_list(Pos& pos, int depth)
    {
        List& ls = get_list(pos);
        parse_nested<do_inc, expLets, assertNoLets>(ls, depth);
    }

    template <Preprocess::Expand_lets expLets>
    void Preprocess::parse_let(Pos& pos, int /*depth*/)
    {
        assert(is_let_at(pos));
        maybe_check_is_not_let_at<expLets>(pos);
        pos.next();
    }

    template <Preprocess::Expand_lets expLets, bool assertNoLets>
    Preprocess::Iterator Preprocess::expand(Pos& pos, int depth)
    {
        return expand_tp(pos, depth, &This::parse<expLets, assertNoLets>);
    }

    template <Preprocess::Expand_lets expLets, bool assertNoLets>
    Preprocess::Iterator Preprocess::expand_elem(Pos& pos, int depth)
    {
        return expand_tp(pos, depth, &This::parse_elem<expLets, assertNoLets>);
    }

    template <Preprocess::Expand_lets expLets, bool assertNoLets>
    Preprocess::Iterator Preprocess::expand_key(Pos& pos, int depth)
    {
        return expand_tp(pos, depth, &This::parse_key<expLets, assertNoLets>);
    }

    Preprocess::Iterator Preprocess::expand_value(Pos& pos, int /*depth*/)
    {
        return ++pos.it();
    }

    template <bool incDepth, Preprocess::Expand_lets expLets, bool assertNoLets>
    Preprocess::Iterator Preprocess::expand_list(Pos& pos, int depth)
    {
        peek_parse_list<incDepth, expLets, assertNoLets>(pos, depth);
        return ++pos.it();
    }

    template <bool incDepth, Preprocess::Expand_lets expLets, bool assertNoLets>
    void Preprocess::parse_nested(List& ls, int depth)
    {
        Pos pos(ls);
        parse_nested<incDepth, expLets, assertNoLets>(pos, depth);
    }

    template <bool incDepth, Preprocess::Expand_lets expLets, bool assertNoLets>
    void Preprocess::parse_nested(Pos& pos, int depth)
    {
        if constexpr (incDepth) ++depth;
        while (pos) parse<expLets, assertNoLets>(pos, depth);
    }

    template <bool incDepth, Preprocess::Expand_lets expLets, bool assertNoLets>
    List& Preprocess::peek_parse_list(Pos& pos, int depth)
    {
        List& ls = peek_list(pos);
        parse_nested<incDepth, expLets, assertNoLets>(ls, depth);
        return ls;
    }

    template <bool incDepth, Preprocess::Expand_lets expLets, bool assertNoLets>
    List Preprocess::extract_parse_list(Pos& pos, int depth)
    {
        List ls = extract_list(pos);
        parse_nested<incDepth, expLets, assertNoLets>(ls, depth);
        return ls;
    }

    template <bool incDepth, Preprocess::Expand_lets expLets, bool assertNoLets>
    List& Preprocess::peek_parse_list_check(Pos& pos, int depth)
    {
        List& ls = peek_list_check(pos);
        parse_nested<incDepth, expLets, assertNoLets>(ls, depth);
        return ls;
    }

    template <bool incDepth, Preprocess::Expand_lets expLets, bool assertNoLets>
    List Preprocess::extract_parse_list_check(Pos& pos, int depth)
    {
        List ls = extract_list_check(pos);
        parse_nested<incDepth, expLets, assertNoLets>(ls, depth);
        return ls;
    }

    template <Preprocess::Expand_lets expLets, typename ParseF>
    void Preprocess::parse_enclosure_tp(Pos& pos, int depth, ParseF f)
    {
        auto encl = Parse_enclosure<expLets>(*this);
        invoke(move(f), this, pos, depth);
    }

    template <Preprocess::Expand_lets expLets, bool assertNoLets, typename ParseF>
    Preprocess::Iterator Preprocess::expand_tp(Pos& pos, int depth, ParseF f)
    {
        auto it = pos.it();
        if (!pos) return it;

        const bool was_front = pos.at_front();
        if (!was_front) --it;
        parse_enclosure_tp<expLets>(pos, depth, move(f));
        auto end_it = pos.it();
        if (was_front) pos.set_it();
        else pos.set_it(++it);

        if constexpr (assertNoLets) maybe_assert_no_lets_at<expLets>(pos);

        return end_it;
    }

    template <Preprocess::Expand_lets expLets>
    void Preprocess::maybe_check_is_not_let(const Ptr& ptr) const
    {
        Foo_exp_lets<int(expLets)>::maybe_check_is_not_let(*this, ptr);
    }

    template <Preprocess::Expand_lets expLets>
    void Preprocess::maybe_assert_no_lets(const Ptr& ptr) const noexcept
    {
        Foo_exp_lets<int(expLets)>::maybe_assert_no_lets(*this, ptr);
    }

    template <Preprocess::Expand_lets expLets>
    void Preprocess::maybe_check_is_not_let_at(const Pos& pos) const
    {
        maybe_check_is_not_let<expLets>(pos.cpeek_check());
    }

    template <Preprocess::Expand_lets expLets>
    void Preprocess::maybe_assert_no_lets_at(const Pos& pos) const noexcept
    {
        if constexpr (!_debug_) return;
        if (!pos.at_end()) maybe_assert_no_lets<expLets>(pos.cpeek());
    }

    template <Preprocess::Expand_lets expLets>
    Ptr Preprocess::expand_let(const Let& let_) const
    {
        return Foo_exp_lets<int(expLets)>::expand_let(*this, let_);
    }

    template <Preprocess::Expand_lets expLets>
    void Preprocess::maybe_expand_let(Ptr& ptr) const
    {
        if (!Let::is_me(ptr)) return;
        ptr = expand_let<expLets>(Let::cast(ptr));
    }

    template <Preprocess::Expand_lets expLets>
    void Preprocess::maybe_expand_let_at(Pos& pos) const
    {
        maybe_expand_let<expLets>(pos.peek());
    }

    template <Preprocess::Expand_lets expLets>
    void Preprocess::expand_let_ref(Pos& pos, int depth)
    {
        Ptr lptr = peek_let_ref(pos, depth);
        //+ allow also (deep) copy?
        pos.peek() = expand_let<expLets>(Let::cast(lptr));
    }

    template <bool expandV, Preprocess::Expand_lets expLets, typename EvalF>
    void Preprocess::parse_eval(Pos& pos, int depth, EvalF f)
    {
        //+ extracting and inserting the same elem. right after
        pos.emplace(extract_eval<expandV, expLets>(pos, depth, move(f)).to_ptr());
    }

    template <bool expandV, Preprocess::Expand_lets expLets, typename EvalF>
    Preprocess::Iterator Preprocess::expand_eval(Pos& pos, int depth, EvalF f)
    {
        return expand_tp(pos, depth, [CAPTURE_RVAL(f)](This* this_l, Pos& pos_, int dep){
            this_l->parse_eval<expandV, expLets>(pos_, dep, move(f));
        });
    }

    template <bool expandV, Preprocess::Expand_lets expLets, typename EvalF>
    Elem Preprocess::extract_eval(Pos& pos, int depth, EvalF f)
    {
        if constexpr (expandV) {
            pos.check_not_at_end();
            /// To avoid inf. loop
            if (!is_key_at(pos) || !is_arith_exp(peek_key(pos)))
                expand<expLets, no_assert>(pos, depth);
        }
        maybe_check_is_not_let_at<expLets>(pos);
        maybe_assert_no_lets_at<expLets>(pos);
        return invoke(move(f), this, pos, depth);
    }

    template <bool expandV, Preprocess::Expand_lets expLets, typename EvalF>
    bool Preprocess::extract_pred(Pos& pos, int depth, EvalF f)
    {
        return extract_eval<expandV, expLets>(pos, depth,
                                              move(f)).template to_value<bool>().value();
    }

    template <int n, bool expandV, Preprocess::Expand_lets expLets, typename EvalF>
    Elem Preprocess::extract_nary_eval(Pos& pos, int depth, EvalF f)
    {
        return extract_eval<no_exp, Expand_lets::no>(pos, depth,
            [CAPTURE_RVAL(f)](This* this_l, Pos& pos_, int dep){
                Pos args_pos(extract_list_check(pos_));
                if constexpr (expandV) {
                    this_l->parse_nested<do_inc, expLets>(args_pos, dep);
                    args_pos.set_it();
                }
                expect(args_pos.clist().size() == n,
                       "Expected "s + to_string(n) + " arguments, got: " + args_pos.to_string());

                return invoke(move(f), this_l, args_pos, dep);
        });
    }

    template <int n, bool expandV, Preprocess::Expand_lets expLets, typename EvalF>
    bool Preprocess::extract_nary_pred(Pos& pos, int depth, EvalF f)
    {
        return extract_pred<no_exp, Expand_lets::no>(pos, depth,
            [CAPTURE_RVAL(f)](This* this_l, Pos& pos_, int dep){
                return this_l->extract_nary_eval<n, expandV, expLets>(pos_, dep, move(f));
        });
    }

    template <Preprocess::Expand_lets expLets, bool assertNoLets>
    void Preprocess::parse_key_single(Pos& pos, Key& key_, int depth)
    {
        if (is_arith_exp(key_)) return parse_eval_arith_exp(pos, depth);

        const bool prefix_cat = is_prefix_cat(key_);
        if (prefix_cat) rm_prefix_cat_attr(key_);

        parse_key_single_not_arith<expLets, assertNoLets>(pos, key_, depth);

        if (prefix_cat) cat(pos, depth);
    }

    template <Preprocess::Expand_lets expLets, bool assertNoLets>
    void Preprocess::parse_key_single_not_arith(Pos& pos, Key& key_, int depth)
    {
        Foo_exp_lets<int(expLets)>::parse_key_single_not_arith(*this, pos, key_, depth);
        if constexpr (_debug_ && assertNoLets) {
            if (!pos.at_begin()) maybe_assert_no_lets<expLets>(*std::prev(pos.cit(), 1));
        }
    }

    template <Preprocess::Expand_lets expLets>
    void Preprocess::parse_macro(Pos& pos, const Key& key_, int depth)
    {
        Foo_exp_lets<int(expLets)>::parse_macro(*this, pos, key_, depth);
    }

    template <Preprocess::Expand_lets expLets>
    void Preprocess::parse_macro_exp(Pos& pos, int depth)
    {
        Foo_exp_lets<int(expLets)>::parse_macro_exp(*this, pos, depth);
    }

    template <Preprocess::Expand_lets expLets>
    void Preprocess::parse_user_macro(Pos& pos, const Key& key_, int depth)
    {
        Foo_exp_lets<int(expLets)>::parse_user_macro(*this, pos, key_, depth);
    }

    void Preprocess::parse_macro_isdef(Pos& pos, int depth)
    {
        parse_eval<no_exp, Expand_lets::no>(pos, depth, &This::extract_macro_isdef<>);
    }

    void Preprocess::parse_macro_isndef(Pos& pos, int depth)
    {
        parse_eval<no_exp, Expand_lets::no>(pos, depth, &This::extract_macro_isdef<do_neg>);
    }

    void Preprocess::parse_macro_null(Pos& pos, int depth)
    {
        parse_eval<no_exp, Expand_lets::no>(pos, depth, &This::extract_macro_null<>);
    }

    void Preprocess::parse_macro_listp(Pos& pos, int depth)
    {
        parse_eval<no_exp, Expand_lets::no>(pos, depth, &This::extract_macro_listp<>);
    }

    void Preprocess::parse_macro_numberp(Pos& pos, int depth)
    {
        parse_eval<no_exp, Expand_lets::no>(pos, depth, &This::extract_macro_numberp<>);
    }

    void Preprocess::parse_macro_refp(Pos& pos, int depth)
    {
        parse_eval<no_exp, Expand_lets::no>(pos, depth, &This::extract_macro_refp<>);
    }

    void Preprocess::parse_macro_equal(Pos& pos, int depth)
    {
        parse_eval<no_exp, Expand_lets::no>(pos, depth, &This::extract_macro_equal<>);
    }

    void Preprocess::parse_macro_eq(Pos& pos, int depth)
    {
        parse_eval<no_exp, Expand_lets::no>(pos, depth, &This::extract_macro_eq<>);
    }

    void Preprocess::parse_macro_len(Pos& pos, int depth)
    {
        parse_eval<no_exp, Expand_lets::no>(pos, depth, &This::extract_macro_len<>);
    }

    Ref<Ptr> Preprocess::peek_macro_car(Pos& pos, int depth)
    {
        return peek_accessor_tp<true, true>(pos, depth, &This::peek_macro_car_impl);
    }

    Ptr Preprocess::peek_macro_cdr(Pos& pos, int depth)
    {
        return peek_accessor_tp<true, false>(pos, depth, &This::peek_macro_cdr_impl).item();
    }

    Ref<Ptr> Preprocess::peek_macro_nth(Pos& pos, int depth)
    {
        return peek_accessor_tp<false, true>(pos, depth, &This::peek_macro_nth_impl);
    }

    Ptr Preprocess::peek_macro_nthcdr(Pos& pos, int depth)
    {
        return peek_accessor_tp<false, false>(pos, depth, &This::peek_macro_nthcdr_impl).item();
    }

    void Preprocess::parse_macro_car(Pos& pos, int depth)
    {
        parse_accessor_tp<true, true>(pos, depth, &This::peek_macro_car_impl);
    }

    void Preprocess::parse_macro_cdr(Pos& pos, int depth)
    {
        parse_accessor_tp<true, false>(pos, depth, &This::peek_macro_cdr_impl);
    }

    void Preprocess::parse_macro_nth(Pos& pos, int depth)
    {
        parse_accessor_tp<false, true>(pos, depth, &This::peek_macro_nth_impl);
    }

    void Preprocess::parse_macro_nthcdr(Pos& pos, int depth)
    {
        parse_accessor_tp<false, false>(pos, depth, &This::peek_macro_nthcdr_impl);
    }

    template <bool expandV>
    Ptr Preprocess::extract_macro_key(Pos& pos, int depth)
    {
        return Foo<expandV>::extract_macro_key(*this, pos, depth);
    }

    template <bool negV, bool expandV>
    bool Preprocess::extract_macro_isdef(Pos& pos, int depth)
    {
        return Foo<expandV, negV>::extract_macro_isdef(*this, pos, depth);
    }

    template <bool expandV>
    bool Preprocess::extract_macro_null(Pos& pos, int depth)
    {
        return Foo<expandV>::extract_macro_null(*this, pos, depth);
    }

    template <bool expandV>
    bool Preprocess::extract_macro_listp(Pos& pos, int depth)
    {
        return Foo<expandV>::extract_macro_listp(*this, pos, depth);
    }

    template <bool expandV>
    bool Preprocess::extract_macro_numberp(Pos& pos, int depth)
    {
        return Foo<expandV>::extract_macro_numberp(*this, pos, depth);
    }

    template <bool expandV>
    bool Preprocess::extract_macro_refp(Pos& pos, int depth)
    {
        return Foo<expandV>::extract_macro_refp(*this, pos, depth);
    }

    template <bool expandV>
    bool Preprocess::extract_macro_equal(Pos& pos, int depth)
    {
        return Foo<expandV>::extract_macro_equal(*this, pos, depth);
    }

    template <bool expandV>
    bool Preprocess::extract_macro_eq(Pos& pos, int depth)
    {
        return Foo<expandV>::extract_macro_eq(*this, pos, depth);
    }

    template <bool expandV>
    Elem Preprocess::extract_macro_len(Pos& pos, int depth)
    {
        return Foo<expandV>::extract_macro_len(*this, pos, depth);
    }

    void Preprocess::parse_eval_arith(Pos& pos, int depth)
    {
        parse_eval(pos, depth, &This::extract_eval_arith<no_exp>);
    }

    Preprocess::Iterator Preprocess::expand_eval_arith(Pos& pos, int depth)
    {
        return expand_eval(pos, depth, &This::extract_eval_arith<no_exp>);
    }

    void Preprocess::parse_eval_arith_exp(Pos& pos, int depth)
    {
        parse_eval(pos, depth, &This::extract_eval_arith_exp);
    }

    Preprocess::Iterator Preprocess::expand_eval_arith_exp(Pos& pos, int depth)
    {
        return expand_eval(pos, depth, &This::extract_eval_arith_exp);
    }

    template <bool expandV>
    Elem Preprocess::extract_eval_arith(Pos& pos, int depth)
    {
        return Foo<expandV>::extract_eval_arith(*this, pos, depth);
    }

    template <bool expandV>
    Elem Preprocess::extract_eval_arith_elem(Pos& pos, int depth)
    {
        return Foo<expandV>::extract_eval_arith_elem(*this, pos, depth);
    }

    template <bool expandV>
    Elem Preprocess::extract_eval_arith_list(Pos& pos, int depth)
    {
        return Foo<expandV>::extract_eval_arith_list(*this, pos, depth);
    }

    template <typename FunT, bool expandV>
    FunT Preprocess::extract_eval_arith_cons_fun(Pos& pos, int depth, Keys keys)
    {
        if constexpr (expandV) {
            check_is_list_at(pos);
            expand_list<do_inc, Expand_lets::all>(pos, depth);
        }
        return FunT::cons(extract_maybe_deep_copy_list(pos), new_keys(move(keys)));
    }

    template <bool expandV>
    bool Preprocess::extract_eval_arith_pred(Pos& pos, int depth)
    {
        return extract_pred<expandV>(pos, depth, &This::extract_eval_arith<no_exp>);
    }

    template <typename F>
    void Preprocess::set_let_extract_tp(Pos& pos, int depth, F f)
    {
        pos.check_not_at_end();
        if (is_list_at(pos)) expand_list<no_inc>(pos, depth);
        else if (!is_let_at(pos)) expand_elem(pos, depth);
        invoke(move(f), pos.extract());
    }

    template <bool groupedV, bool extractV>
    void Preprocess::push_let_grouped_extract(Pos& pos, int depth, const Key& lkey)
    {
        Macro::check_is_not_reserved_key(lkey);
        if constexpr (extractV) {
            set_let_extract_tp(pos, depth, [this, &lkey](Ptr ptr){
                push_let<groupedV>(lkey, move(ptr));
            });
        }
        else push_let<groupedV>(lkey, new_key());
    }
}

////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////

namespace unsot::expr {
    template <auto expLets>
    Preprocess::Parse_enclosure<expLets>::Parse_enclosure(Preprocess& prep)
        : preprocess_ref(prep), prev_exp_lets(preprocess_ref.cexpanding_lets())
    {
        if constexpr (expLets != Expand_lets::neutral) {
            preprocess_ref.expanding_lets() = expLets;
        }
    }

    template <auto expLets>
    Preprocess::Parse_enclosure<expLets>::~Parse_enclosure()
    {
        if constexpr (expLets != Expand_lets::neutral) {
            preprocess_ref.expanding_lets() = prev_exp_lets;
        }
    }
}

////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////

namespace unsot::expr {
    template <typename P>
    Forward_as_ref<Let, P> peek_let(P&& pos) noexcept
    {
        return Let::cast(FORWARD(pos).peek());
    }

    template <typename P>
    auto get_let(P&& pos) noexcept
        -> Cond_t<Decay<P>::is_const_v, const Let&, Forward_as_ref<Let, P>>
    {
        return Let::cast(FORWARD(pos).get());
    }

    template <typename P>
    Forward_as_ref<Let, P> peek_let_check(P&& pos)
    {
        return Let::cast_check(FORWARD(pos).peek_check());
    }

    template <typename P>
    auto get_let_check(P&& pos)
        -> Cond_t<Decay<P>::is_const_v, const Let&, Forward_as_ref<Let, P>>
    {
        return Let::cast_check(FORWARD(pos).get_check());
    }

    template <typename I>
    [[nodiscard]] List preprocess(I&& input)
    {
        if constexpr (is_list_v<I>) return Preprocess().perform_list(FORWARD(input));
        else return Preprocess().perform(FORWARD(input));
    }
}
