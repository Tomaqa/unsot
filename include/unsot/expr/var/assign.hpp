#pragma once

namespace unsot::expr::var::assign {
    /// More accurately "assignee mixin"
    template <typename B> class Mixin;

    template <typename B, typename AssigneeT = typename B::Arg_var> class Assigner_mixin;
    template <typename B> class Equality_mixin;

    /// More accurately "auto-assignable mixin"
    template <typename B, typename AssignerT> class Auto_mixin;

    template <typename B> class Auto_visit_mixin;
    template <typename B> class Distance_mixin;
    template <typename B> class Assigner_distance_mixin;
}

namespace unsot::expr::var {
    template <typename Sort, typename ConfT = Conf>
        using Assignee = Cond_mixin<conf::has_distance_v<ConfT>, assign::Distance_mixin,
                         assign::Mixin<Var<Sort, ConfT>>>;

    template <typename B, typename ConfT = Conf,
              typename AssigneeT = Assignee<typename B::Arg_value, ConfT>>
        using Assigner = Cond_mixin<conf::has_distance_v<ConfT>, assign::Assigner_distance_mixin,
                         assign::Assigner_mixin<B, AssigneeT>>;
    template <typename B, typename ConfT = Conf,
              typename AssigneeT = Assignee<typename B::Arg_value, ConfT>>
        using Equality = assign::Equality_mixin<Assigner<B, ConfT, AssigneeT>>;

    template <typename Sort, typename AssignerT, typename ConfT = Conf>
        using Auto_assignee_base = Cond_mixin<conf::visitable_v<ConfT>, assign::Auto_visit_mixin,
                                   assign::Auto_mixin<Assignee<Sort, ConfT>, AssignerT>>;

    template <typename Sort, Type typeV, typename ConfT = Conf, typename AutoVar = void>
        using Tp = Cond_t<typeV == Type::regular, Var<Sort, ConfT>,
                   Cond_t<!type::is_assign_v<typeV>, void,
                   Cond_t<typeV == Type::auto_assignee && !is_void_v<AutoVar>, AutoVar,
                   Assignee<Sort, ConfT>>>>;

    namespace aux {
        template <typename T> using Is_assignee = Void<typename Decay<T>::Assignee_t>;
        template <typename T> using Is_assigner = Void<typename Decay<T>::Assigner_t>;
        template <typename T> using Is_equality = Void<typename Decay<T>::Equality_t>;
        template <typename T> using Is_auto_assignee = Void<typename Decay<T>::Auto_assignee_t>;
    }

    template <typename T> constexpr bool is_assignee_v = enabled_v<aux::Is_assignee, T>;
    template <typename T> constexpr bool is_assigner_v = enabled_v<aux::Is_assigner, T>;
    template <typename T> constexpr bool is_equality_v = enabled_v<aux::Is_equality, T>;
    template <typename T> constexpr bool is_auto_assignee_v = enabled_v<aux::Is_auto_assignee, T>;
}

namespace unsot::expr::var::assign {
    template <typename B>
    class Mixin : public Inherit<B, Mixin<B>> {
    public:
        using Inherit = unsot::Inherit<B, Mixin<B>>;
        using typename Inherit::This;
        using typename Inherit::Base;
        using Assignee_t = This;

        using typename Inherit::Value;

        using typename Inherit::Depend_ids;

        using Inherit::Parent::operator =;
        Mixin()                                                     = default;
        virtual ~Mixin()                                            = default;
        Mixin(const Mixin&)                                          = delete;
        Mixin& operator =(const Mixin&)                              = delete;
        Mixin(Mixin&&)                                              = default;
        Mixin& operator =(Mixin&&)                                  = default;

        virtual bool allowed_assigner(const Id&) const noexcept
                                                              { return true; }

        virtual void connect_assigner(Id)                                  { }

        const Depend_ids& cdepend_ids() const override { return _depend_ids; }
        Depend_ids& depend_ids() const override        { return _depend_ids; }

        bool cassigned() const noexcept                  { return _assigned; }

        virtual bool maybe_assignable() const noexcept;

        virtual void prepare_to_assign_by(Id) const noexcept;
        virtual void forget_to_assign_by(const Id&) const noexcept;

        virtual bool still_assignable() const noexcept;

        virtual void assign_me(Value) noexcept;
        /// It is user's (or predicate's) responsibility to add assigner's id
        virtual void add_depend_id(Id) const noexcept;
        virtual void rm_depend_id(const Id&) const noexcept;
    protected:
        using Inherit::Inherit;

        bool& assigned() noexcept                        { return _assigned; }

        void reset_impl() noexcept override;
    private:
        mutable Depend_ids _depend_ids{};

        bool _assigned{};
    };

    template <typename B, typename AssigneeT>
    class Assigner_mixin : public Inherit<B, Assigner_mixin<B, AssigneeT>> {
    public:
        using Inherit = unsot::Inherit<B, Assigner_mixin<B, AssigneeT>>;
        using typename Inherit::This;
        using typename Inherit::Base;
        using Assigner_t = This;

        using typename Inherit::Value;

        using Assignee = AssigneeT;

        using typename Inherit::Arg_value;

        using typename Inherit::Arg_ids;
        using Assigned_ids = Vector<Id>;

        using Inherit::Parent::operator =;
        Assigner_mixin()                                            = default;
        virtual ~Assigner_mixin()                                   = default;
        Assigner_mixin(const Assigner_mixin&)                        = delete;
        Assigner_mixin& operator =(const Assigner_mixin&)            = delete;
        Assigner_mixin(Assigner_mixin&&)                            = default;
        Assigner_mixin& operator =(Assigner_mixin&&)                = default;

        const auto& cassignee_ids() const noexcept   { return _assignee_ids; }
        const auto& cassignee_id() const noexcept;

        const auto& cid_to_assign() const noexcept   { return _id_to_assign; }

        const auto& cassigned_ids() const noexcept   { return _assigned_ids; }
        const auto& cassigned_id() const noexcept;

        bool nothing_to_assign() const noexcept;
        bool has_assigned() const noexcept;

        const auto& cassignee(const Id&) const;
        auto& assignee(const Id&);
        const auto& cassignee(const Key&) const;
        auto& assignee(const Key&);

        virtual Idx assignee_idx(const Id&) const;

        /// Must be called before computation
        virtual Flag any_assignable() const noexcept;
        virtual Flag surely_any_assignable() const noexcept;

        virtual void prepare_to_assign(const Assignee&) const noexcept;
        virtual void forget_to_assign(const Assignee&) const noexcept;

        virtual void assign(Arg_value, Assignee&) noexcept;

        Flag independent_arg(const Id&) const noexcept override;
    protected:
        using Inherit::Inherit;

        void linit();

        auto& assignee_ids() noexcept                { return _assignee_ids; }
        auto& assignee_id() noexcept;

        auto& id_to_assign() const noexcept          { return _id_to_assign; }

        auto& assigned_ids() noexcept                { return _assigned_ids; }
        auto& assigned_id() noexcept;

        virtual Id assignee_id(const expr::Ptr&) const noexcept;

        virtual void add_assignees()                                      = 0;
        virtual Id add_assignee(const Key&);
        virtual bool add_assignee(Id);
        Id try_add_assignee(const expr::Ptr&);

        void reset_impl() noexcept override;

        Flag computable_body() const noexcept override;

        virtual Flag any_assignable_body() const noexcept;
        virtual Flag assignable(const Assignee&) const noexcept;
        virtual Flag try_prepare_to_assign(const Assignee&) const noexcept;
        virtual Flag surely_still_assignable(const Assignee&) const noexcept;
        virtual bool surely_still_assignable_impl(const Assignee&) const noexcept;
        virtual Flag computable_body_not_assign() const noexcept;


        bool compute_body_prepare() override;
        virtual bool compute_body_prepare_not_assign();
        virtual bool compute_body_prepare_assign()                        = 0;
        Value compute_body_value(bool success) override;
        virtual Value compute_body_value_not_assign(bool success);
        virtual Value compute_body_value_assign(bool success);
    private:
        Arg_ids _assignee_ids{};
        mutable Id _id_to_assign{invalid_id};
        Assigned_ids _assigned_ids{};
    };

    template <typename B>
    class Equality_mixin : public Inherit<B, Equality_mixin<B>> {
    public:
        using Inherit = unsot::Inherit<B, Equality_mixin<B>>;
        using typename Inherit::This;
        using typename Inherit::Base;
        using Equality_t = This;

        using typename Inherit::Assignee;

        using typename Inherit::Arg_value;

        using Inherit::Parent::operator =;
        Equality_mixin()                                            = default;
        virtual ~Equality_mixin()                                   = default;
        Equality_mixin(const Equality_mixin&)                        = delete;
        Equality_mixin& operator =(const Equality_mixin&)            = delete;
        Equality_mixin(Equality_mixin&&)                            = default;
        Equality_mixin& operator =(Equality_mixin&&)                = default;

        Idx assignee_idx(const Id&) const override;
    protected:
        using Inherit::Inherit;

        bool lvalid_pre_init() const noexcept;
        void lcheck_pre_init() const;

        void add_assignees() override;

        Flag assignable(const Assignee&) const noexcept override;
        bool surely_still_assignable_impl(const Assignee&) const noexcept override;

        bool compute_body_prepare_assign() override;
    };

    template <typename B, typename AssignerT>
    class Auto_mixin : public Inherit<B, Auto_mixin<B, AssignerT>> {
    public:
        using Inherit = unsot::Inherit<B, Auto_mixin<B, AssignerT>>;
        using typename Inherit::This;
        using typename Inherit::Base;
        using Auto_assignee_t = This;

        using typename Inherit::Value;

        using Assigner = AssignerT;

        using typename Inherit::Depend_ids;

        using Inherit::Parent::operator =;
        Auto_mixin()                                                = default;
        virtual ~Auto_mixin()                                       = default;
        Auto_mixin(const Auto_mixin&)                                = delete;
        Auto_mixin& operator =(const Auto_mixin&)                    = delete;
        Auto_mixin(Auto_mixin&&)                                    = default;
        Auto_mixin& operator =(Auto_mixin&&)                        = default;

        void connect_assigner(Id) override;
        void add_assigner_id(Id);

        const Depend_ids& cassigner_ids() const noexcept
                                                     { return _assigner_ids; }

        virtual const Ptr& cassigner_ptr(const Id&) const                 = 0;
        virtual Ptr& assigner_ptr(const Id&)                              = 0;
        const auto& cassigner(const Id&) const;
        auto& assigner(const Id&);

        bool maybe_assignable() const noexcept override;

        void prepare_to_assign_by(Id) const noexcept override;
        void forget_to_assign_by(const Id&) const noexcept override;
    protected:
        using Inherit::Inherit;

        Depend_ids& assigner_ids() noexcept          { return _assigner_ids; }

        Flag computable_body() const noexcept override;

        bool compute_body_prepare() override;
    private:
        virtual Flag computable_body_impl() const noexcept;

        template <typename BodyPred> bool compute_body_prepare_impl(BodyPred) const;

        Depend_ids _assigner_ids{};
    };

    template <typename B>
    class Auto_visit_mixin : public Inherit<B, Auto_visit_mixin<B>> {
    public:
        using Inherit = unsot::Inherit<B, Auto_visit_mixin<B>>;
        using typename Inherit::This;
        using typename Inherit::Base;

        using typename Inherit::Assigner;

        using Inherit::Parent::operator =;
        Auto_visit_mixin()                                          = default;
        virtual ~Auto_visit_mixin()                                 = default;
        Auto_visit_mixin(const Auto_visit_mixin&)                    = delete;
        Auto_visit_mixin& operator =(const Auto_visit_mixin&)        = delete;
        Auto_visit_mixin(Auto_visit_mixin&&)                        = default;
        Auto_visit_mixin& operator =(Auto_visit_mixin&&)            = default;
    protected:
        using Inherit::Inherit;
    private:
        Flag computable_body_impl() const noexcept override;
    };

    template <typename B>
    class Distance_mixin : public Inherit<B, Distance_mixin<B>> {
    public:
        using Inherit = unsot::Inherit<B, Distance_mixin<B>>;
        using typename Inherit::This;
        using typename Inherit::Base;
        using Assignee_distance_t = This;

        using Inherit::Parent::operator =;
        Distance_mixin()                                            = default;
        virtual ~Distance_mixin()                                   = default;
        Distance_mixin(const Distance_mixin&)                        = delete;
        Distance_mixin& operator =(const Distance_mixin&)            = delete;
        Distance_mixin(Distance_mixin&&)                            = default;
        Distance_mixin& operator =(Distance_mixin&&)                = default;

        virtual void assign_me_distance(Distance assigner_dist) noexcept;
    protected:
        using Inherit::Inherit;
    };

    template <typename B>
    class Assigner_distance_mixin
        : public Inherit<B, Assigner_distance_mixin<B>> {
    public:
        using Inherit = unsot::Inherit<B, Assigner_distance_mixin<B>>;
        using typename Inherit::This;
        using typename Inherit::Base;

        using typename Inherit::Assignee;

        using typename Inherit::Arg_value;

        using Inherit::Parent::operator =;
        Assigner_distance_mixin()                                   = default;
        virtual ~Assigner_distance_mixin()                          = default;
        Assigner_distance_mixin(const Assigner_distance_mixin&)      = delete;
        Assigner_distance_mixin& operator =(const Assigner_distance_mixin&)
                                                                     = delete;
        Assigner_distance_mixin(Assigner_distance_mixin&&)          = default;
        Assigner_distance_mixin& operator =(Assigner_distance_mixin&&)
                                                                    = default;
    protected:
        using Inherit::Inherit;

        void assign(Arg_value, Assignee&) noexcept override;
    };
}

#include "expr/var/assign.inl"
