#pragma once

namespace unsot::expr::var::vars {
    namespace aux {
        template <typename Sort, typename ArgSort>
        typename Base<Sort, ArgSort>::Ptrs_ptr
        Base<Sort, ArgSort>::new_ptrs(Ptrs&& ptrs_)
        {
            return MAKE_SHARED(move(ptrs_));
        }

        template <typename Sort, typename ArgSort>
        template <typename T, typename... Args>
        Ptr Base<Sort, ArgSort>::new_var_tp(Args&&... args)
        {
            return T::new_me(keys_ptr(), values_ptr(), ids_map_ptr(), FORWARD(args)...);
        }

        template <typename Sort, typename ArgSort>
        template <typename BaseT>
        void Base<Sort, ArgSort>::connect(BaseT& rhs)
        {
            ptrs_ptr() = rhs.ptrs_ptr();
            ptrs_view().connect(ptrs());
            keys_ptr() = rhs.keys_ptr();
            values_ptr() = rhs.values_ptr();
            ids_map_ptr() = rhs.ids_map_ptr();
        }

        template <typename Sort, typename ArgSort>
        const auto& Base<Sort, ArgSort>::cptr(const Key_id& kid) const noexcept
        {
            return cptrs()[kid];
        }

        template <typename Sort, typename ArgSort>
        auto& Base<Sort, ArgSort>::ptr(const Key_id& kid)& noexcept
        {
            return ptrs()[kid];
        }

        template <typename Sort, typename ArgSort>
        const auto& Base<Sort, ArgSort>::cptr(const Key& key_) const
        {
            return cptr(ckey_id(key_));
        }

        template <typename Sort, typename ArgSort>
        auto& Base<Sort, ArgSort>::ptr(const Key& key_)&
        {
            return ptr(ckey_id(key_));
        }

        template <typename Sort, typename ArgSort>
        const auto& Base<Sort, ArgSort>::ckey(const Key_id& kid) const noexcept
        {
            return expr::key(ckeys(), kid);
        }

        template <typename Sort, typename ArgSort>
        auto& Base<Sort, ArgSort>::key(const Key_id& kid)& noexcept
        {
            return expr::key(keys(), kid);
        }

        template <typename Sort, typename ArgSort>
        const auto& Base<Sort, ArgSort>::cvalue(const Key_id& kid) const noexcept
        {
            return expr::value<Value>(cvalues(), kid);
        }

        template <typename Sort, typename ArgSort>
        auto& Base<Sort, ArgSort>::value(const Key_id& kid)& noexcept
        {
            return expr::value<Value>(values(), kid);
        }

        template <typename Sort, typename ArgSort>
        const auto& Base<Sort, ArgSort>::cvalue(const Key& key_) const
        {
            return cvalue(ckey_id(key_));
        }

        template <typename Sort, typename ArgSort>
        auto& Base<Sort, ArgSort>::value(const Key& key_)&
        {
            return value(ckey_id(key_));
        }

        template <typename Sort, typename ArgSort>
        const auto& Base<Sort, ArgSort>::ckey_id(const Key& key_) const
        {
            return expr::key_id(cids_map(), key_);
        }

        template <typename Sort, typename ArgSort>
        auto& Base<Sort, ArgSort>::key_id(const Key& key_)
        {
            return expr::key_id(ids_map(), key_);
        }
    }

    ////////////////////////////////////////////////////////////////

    template <typename Sort, typename ArgSort>
    template <typename BaseT>
    Base<Sort, ArgSort>::Base(BaseT& rhs, Args_base& args)
    {
        this->connect(rhs);
        connect_args(args);
    }

    template <typename Sort, typename ArgSort>
    typename Base<Sort, ArgSort>::Args_base_link_ptr
    Base<Sort, ArgSort>::new_args_base_l()
    {
        if constexpr (is_global_v) return MAKE_SHARED(this);
        else return make_shared<Args_base_link>(nullptr);
    }

    template <typename Sort, typename ArgSort>
    template <typename T, typename FunT, typename... Args>
    Ptr Base<Sort, ArgSort>::new_fun_tp(Formula phi, Args&&... args)
    {
        auto& abase = args_base();
        return this->template new_var_tp<T>(
            FunT::cons(move(phi), abase.keys_ptr(), abase.values_ptr(), abase.ids_map_ptr()),
            cargs_base_l_ptr(), FORWARD(args)...
        );
    }

    template <typename Sort, typename ArgSort>
    size_t Base<Sort, ArgSort>::size() const noexcept
    {
        if constexpr (is_global_v) return this->global_size();
        else return this->local_size();
    }

    template <typename Sort, typename ArgSort>
    bool Base<Sort, ArgSort>::empty() const noexcept
    {
        if constexpr (is_global_v) return this->global_empty();
        else return this->local_empty();
    }

    template <typename Sort, typename ArgSort>
    typename Base<Sort, ArgSort>::const_iterator
    Base<Sort, ArgSort>::cbegin() const noexcept
    {
        if constexpr (is_global_v) return this->cgbegin();
        else return this->clbegin();
    }

    template <typename Sort, typename ArgSort>
    typename Base<Sort, ArgSort>::const_iterator
    Base<Sort, ArgSort>::cend() const noexcept
    {
        if constexpr (is_global_v) return this->cgend();
        else return this->clend();
    }

    template <typename Sort, typename ArgSort>
    typename Base<Sort, ArgSort>::iterator
    Base<Sort, ArgSort>::begin() noexcept
    {
        if constexpr (is_global_v) return this->gbegin();
        else return this->lbegin();
    }

    template <typename Sort, typename ArgSort>
    typename Base<Sort, ArgSort>::iterator
    Base<Sort, ArgSort>::end() noexcept
    {
        if constexpr (is_global_v) return this->gend();
        else return this->lend();
    }

    template <typename Sort, typename ArgSort>
    const Ptr& Base<Sort, ArgSort>::operator [](Idx idx) const noexcept
    {
        if constexpr (is_global_v) return this->cptr(idx);
        else return this->cptrs_view()[idx];
    }

    template <typename Sort, typename ArgSort>
    Ptr& Base<Sort, ArgSort>::operator [](Idx idx) noexcept
    {
        if constexpr (is_global_v) return this->ptr(idx);
        else return this->ptrs_view()[idx];
    }

    template <typename Sort, typename ArgSort>
    const Ptr& Base<Sort, ArgSort>::operator [](nullptr_t) const noexcept
    {
        return operator [](Idx(0));
    }

    template <typename Sort, typename ArgSort>
    Ptr& Base<Sort, ArgSort>::operator [](nullptr_t) noexcept
    {
        return operator [](Idx(0));
    }

    template <typename Sort, typename ArgSort>
    bool Base<Sort, ArgSort>::contains(const Key& key_) const noexcept
    {
        if constexpr (is_global_v) return this->global_contains(key_);
        else return this->local_contains(key_);
    }

    ////////////////////////////////////////////////////////////////

    template <typename B, Type typeV, typename ConfT>
    const auto& Types_mixin<B, typeV, ConfT>::cvar(const Key_id& kid) const noexcept
    {
        return Types::Var::cast(this->cptr(kid));
    }

    template <typename B, Type typeV, typename ConfT>
    auto& Types_mixin<B, typeV, ConfT>::var(const Key_id& kid)& noexcept
    {
        return Types::Var::cast(this->ptr(kid));
    }

    template <typename B, Type typeV, typename ConfT>
    const auto& Types_mixin<B, typeV, ConfT>::cvar(const Key& key_) const
    {
        return cvar(this->ckey_id(key_));
    }

    template <typename B, Type typeV, typename ConfT>
    auto& Types_mixin<B, typeV, ConfT>::var(const Key& key_)&
    {
        return var(this->ckey_id(key_));
    }

    template <typename B, Type typeV, typename ConfT>
    template <typename ValT>
    void Types_mixin<B, typeV, ConfT>::add_var(Key key_, ValT val)
    {
        this->push_back(this->template new_var_tp<typename Types::Var>(
            move(key_), move(val)
        ));
    }

    ////////////////////////////////////////////////////////////////

    template <typename B>
    const auto& Funs_mixin<B>::cfun(const Key_id& kid) const noexcept
    {
        return Types::Fun::cast(this->cptr(kid));
    }

    template <typename B>
    auto& Funs_mixin<B>::fun(const Key_id& kid)& noexcept
    {
        return Types::Fun::cast(this->ptr(kid));
    }

    template <typename B>
    const auto& Funs_mixin<B>::cfun(const Key& key_) const
    {
        return cfun(this->ckey_id(key_));
    }

    template <typename B>
    auto& Funs_mixin<B>::fun(const Key& key_)&
    {
        return fun(this->ckey_id(key_));
    }

    template <typename B>
    template <typename ValT>
    void Funs_mixin<B>::add_fun(Formula phi, Key key_, ValT val)
    {
        this->push_back(this->template new_fun_tp<typename Types::Fun>(
            move(phi), move(key_), move(val)
        ));
    }

    ////////////////////////////////////////////////////////////////

    template <typename B>
    const auto& Preds_mixin<B>::cpred(const Key_id& kid) const noexcept
    {
        return Types::Pred::cast(this->cptr(kid));
    }

    template <typename B>
    auto& Preds_mixin<B>::pred(const Key_id& kid)& noexcept
    {
        return Types::Pred::cast(this->ptr(kid));
    }

    template <typename B>
    const auto& Preds_mixin<B>::cpred(const Key& key_) const
    {
        return cpred(this->ckey_id(key_));
    }

    template <typename B>
    auto& Preds_mixin<B>::pred(const Key& key_)&
    {
        return pred(this->ckey_id(key_));
    }

    template <typename B>
    const auto& Preds_mixin<B>::cassigner(const Key_id& kid) const noexcept
    {
        return Types::Assigner::cast(this->cptr(kid));
    }

    template <typename B>
    auto& Preds_mixin<B>::assigner(const Key_id& kid)& noexcept
    {
        return Types::Assigner::cast(this->ptr(kid));
    }

    template <typename B>
    const auto& Preds_mixin<B>::cassigner(const Key& key_) const
    {
        return cassigner(this->ckey_id(key_));
    }

    template <typename B>
    auto& Preds_mixin<B>::assigner(const Key& key_)&
    {
        return assigner(this->ckey_id(key_));
    }

    template <typename B>
    template <typename ValT>
    void Preds_mixin<B>::add_pred(Formula phi, Key key_, ValT val)
    {
        using Pred = typename Types::Pred;
        using Equality = typename Types::Equality;
        phi.maybe_virtual_init();
        this->push_back((phi.coper() != "=")
            ? this->template new_fun_tp<Pred>(move(phi), move(key_), move(val))
            : this->template new_fun_tp<Equality>(move(phi), move(key_), move(val))
        );
    }

    ////////////////////////////////////////////////////////////////

    template <typename B>
    typename Auto_assign_mixin<B>::Preds_base_link_ptr
    Auto_assign_mixin<B>::new_preds_base_l()
    {
        return make_shared<Preds_base_link>(nullptr);
    }

    template <typename B>
    template <typename ValT>
    void Auto_assign_mixin<B>::add_var(Key key_, ValT val)
    {
        this->push_back(this->template new_var_tp<typename Types::Var>(
            cpreds_base_l_ptr(), move(key_), move(val)
        ));
    }
}
