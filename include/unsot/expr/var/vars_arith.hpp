#pragma once

#include "expr/var/vars.hpp"

#include "expr/var_arith.hpp"

#ifdef NO_EXPLICIT_TP_INST
#include "expr/var/vars.tpp"
#include "expr/var/vars/types.tpp"
#endif

#ifndef NO_EXPLICIT_TP_INST
namespace unsot::expr::var {
    namespace aux {
        template <typename Sort, typename ArgSort = Sort>
            using _Vars = var::Vars<Sort, ArgSort>;
        template <typename Sort, typename ArgSort = Sort>
            using _Avars = var::Vars<Sort, ArgSort, Type::auto_assignee>;
    }

    extern template class aux::_Vars<Int>::Aux_base_t::Base;
    extern template class aux::_Vars<Real>::Aux_base_t::Base;
    extern template class aux::_Vars<Int, Real>::Aux_base_t::Base;
    extern template class aux::_Vars<Int>::Base;
    extern template class aux::_Vars<Real>::Base;
    extern template class aux::_Vars<Int, Real>::Base;

    extern template class aux::_Avars<Int>::Auto_assign_mixin;
    extern template class aux::_Avars<Real>::Auto_assign_mixin;
}

namespace unsot::expr::var::vars {
    namespace aux {
        template <typename Sort, typename ArgSort = Sort>
            using _Types = vars::Types<Sort, ArgSort>;
        template <typename Sort, typename ArgSort = Sort>
            using _Ftypes = vars::Types<Sort, ArgSort, Type::regular, Full_conf>;
        template <typename Sort, typename ArgSort = Sort>
            using _Atypes = vars::Types<Sort, ArgSort, Type::auto_assignee>;
    }

    static_assert(is_same_v<aux::_Types<Int>::Fun::Vars_fun_t,
                            typename Types<Int>::Fun::Vars_fun_t::Fun_mixin>);
    static_assert(is_same_v<aux::_Types<Int>::Fun::Vars_fun_t,
                            Types<Int, Int, Type::assignee>::Fun::Vars_fun_t>);
    static_assert(is_same_v<aux::_Ftypes<Int>::Fun::Vars_fun_t,
                            aux::_Atypes<Int>::Fun::Vars_fun_t>);
    static_assert(is_same_v<aux::_Types<Int>::Fun::Vars_fun_t,
                            Types<Int>::Fun_mixin<Fun_base<Int>>>);
    static_assert(is_same_v<aux::_Atypes<Int>::Fun::Vars_fun_t,
                            aux::_Atypes<Int>::Fun_mixin<Fun_base<Int, Full_conf>>>);
    static_assert(is_same_v<aux::_Types<Int>::Pred::Vars_fun_t,
                            aux::_Types<Int>::Equality::Vars_fun_t>);

    extern template class aux::_Types<Int>::Fun::Vars_fun_t::Fun_mixin;
    extern template class aux::_Types<Int>::Pred::Vars_fun_t::Fun_mixin;
    extern template class aux::_Types<Real>::Fun::Vars_fun_t::Fun_mixin;
    extern template class aux::_Types<Int, Real>::Pred::Vars_fun_t::Fun_mixin;
    extern template class aux::_Ftypes<Int, Real>::Pred::Vars_fun_t::Fun_mixin;
    extern template class aux::_Atypes<Int>::Fun::Vars_fun_t::Fun_mixin;
    extern template class aux::_Atypes<Int>::Pred::Vars_fun_t::Fun_mixin;
    extern template class aux::_Atypes<Real>::Fun::Vars_fun_t::Fun_mixin;

    extern template class aux::_Atypes<Int>::Var::Vars_auto_assignee_t::Auto_mixin;
    extern template class aux::_Atypes<Real>::Var::Vars_auto_assignee_t::Auto_mixin;
}

namespace unsot::expr::var {
    static_assert(is_same_v<vars::aux::_Types<Int>::Equality::Assigner_t,
                            vars::aux::_Types<Int>::Assigner::Assigner_t>);

    extern template class vars::aux::_Types<Int>::Equality::Assigner_t::Assigner_mixin;
    extern template class vars::aux::_Types<Int>::Equality::Equality_t::Equality_mixin;
    extern template class vars::aux::_Types<Int, Real>::Equality::Assigner_t::Assigner_mixin;
    extern template class vars::aux::_Types<Int, Real>::Equality::Equality_t::Equality_mixin;
    extern template class vars::aux::_Ftypes<Int, Real>::Equality::Assigner_t::Assigner_mixin;
    extern template class vars::aux::_Ftypes<Int, Real>::Equality::Equality_t::Equality_mixin;
    extern template class vars::aux::_Ftypes<Int, Real>::Equality::Assigner_distance_mixin;
    extern template class vars::aux::_Atypes<Int>::Equality::Assigner_t::Assigner_mixin;
    extern template class vars::aux::_Atypes<Int>::Equality::Equality_t::Equality_mixin;
    extern template class vars::aux::_Atypes<Int>::Equality::Assigner_distance_mixin;

    extern template class vars::aux::_Atypes<Int>::Var::Auto_assignee_t::Auto_mixin;
    extern template class vars::aux::_Atypes<Int>::Var::Auto_visit_mixin;
    extern template class vars::aux::_Atypes<Real>::Var::Auto_assignee_t::Auto_mixin;
    extern template class vars::aux::_Atypes<Real>::Var::Auto_visit_mixin;
}
#endif  /// NO_EXPLICIT_TP_INST
