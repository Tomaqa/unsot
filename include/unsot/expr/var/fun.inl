#pragma once

#include "util/flag/alg.hpp"

namespace unsot::expr::var::fun {
    template <typename B, typename ArgVar, typename FunT>
    template <typename T, typename V>
    T Mixin<B, ArgVar, FunT>::
        cons_tp(Keys_ptr& kptr, Values_ptr& vptr, Ids_map_ptr& imap_ptr,
                Fun fun_, Key key_, V val)
    {
        static constexpr bool valid_ = !is_same_v<V, Dummy>;
        Id id_ = add_key_value(*kptr, *vptr, *imap_ptr,
                               move(key_), move(val));
        return cons_tp<T>(kptr, vptr, imap_ptr, move(id_), move(fun_),
                          valid_);
    }

    template <typename B, typename ArgVar, typename FunT>
    const auto& Mixin<B, ArgVar, FunT>::cformula() const noexcept
    {
        return static_cast<const Formula&>(cfun());
    }

    template <typename B, typename ArgVar, typename FunT>
    auto& Mixin<B, ArgVar, FunT>::formula() noexcept
    {
        return static_cast<Formula&>(fun());
    }

    template <typename B, typename ArgVar, typename FunT>
    const typename Mixin<B, ArgVar, FunT>::Arg_ids&
    Mixin<B, ArgVar, FunT>::carg_ids() const
    {
        return cfun().ckey_ids();
    }

    template <typename B, typename ArgVar, typename FunT>
    const typename Mixin<B, ArgVar, FunT>::Depend_ids&
    Mixin<B, ArgVar, FunT>::cdepend_ids() const
    {
        return carg_ids();
    }

    template <typename B, typename ArgVar, typename FunT>
    const Ptr& Mixin<B, ArgVar, FunT>::carg_ptr(const Key& key_) const
    {
        return carg_ptr(cfun().ckey_id(key_));
    }

    template <typename B, typename ArgVar, typename FunT>
    Ptr& Mixin<B, ArgVar, FunT>::arg_ptr(const Key& key_)
    {
        return arg_ptr(cfun().ckey_id(key_));
    }

    template <typename B, typename ArgVar, typename FunT>
    const auto& Mixin<B, ArgVar, FunT>::carg_var(const Id& aid) const
    {
        return Arg_var::cast(carg_ptr(aid));
    }

    template <typename B, typename ArgVar, typename FunT>
    auto& Mixin<B, ArgVar, FunT>::arg_var(const Id& aid)
    {
        return Arg_var::cast(arg_ptr(aid));
    }

    template <typename B, typename ArgVar, typename FunT>
    const auto& Mixin<B, ArgVar, FunT>::carg_var(const Key& key_) const
    {
        return Arg_var::cast(carg_ptr(key_));
    }

    template <typename B, typename ArgVar, typename FunT>
    auto& Mixin<B, ArgVar, FunT>::arg_var(const Key& key_)
    {
        return Arg_var::cast(arg_ptr(key_));
    }
}
