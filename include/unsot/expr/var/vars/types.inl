#pragma once

namespace unsot::expr::var::vars::aux::types {
    template <typename Sort, typename ArgSort, typename ConfT>
    template <typename B>
    template <typename T, typename V>
    T Base<Sort, ArgSort, ConfT>::Fun_mixin<B>::
        cons_tp(Keys_ptr& kptr, Values_ptr& vptr, Ids_map_ptr& imap_ptr,
                Fun fun_, Args_base_link_ptr args_l_ptr, Key key_, V val)
    {
        static constexpr bool valid_ = !is_same_v<V, Dummy>;
        Id id_ = add_key_value(*kptr, *vptr, *imap_ptr,
                               move(key_), move(val));
        return cons_tp<T>(kptr, vptr, imap_ptr, move(id_), move(fun_),
                          move(args_l_ptr), valid_);
    }

    template <typename Sort, typename ArgSort, typename ConfT>
    template <typename B>
    const Ptr& Base<Sort, ArgSort, ConfT>::Fun_mixin<B>::
        carg_ptr(const Id& kid) const
    {
        return cargs_base().cptr(kid);
    }

    template <typename Sort, typename ArgSort, typename ConfT>
    template <typename B>
    Ptr& Base<Sort, ArgSort, ConfT>::Fun_mixin<B>::
        arg_ptr(const Id& kid)
    {
        return args_base().ptr(kid);
    }

    ////////////////////////////////////////////////////////////////

    template <typename Sort, typename ArgSort, typename ConfT>
    template <typename B>
    template <typename T, typename V>
    T Base<Sort, ArgSort, ConfT>::Auto_mixin<B>::
        cons_tp(Keys_ptr& kptr, Values_ptr& vptr, Ids_map_ptr& imap_ptr,
                Preds_base_link_ptr preds_l_ptr, Key key_, V val)
    {
        static constexpr bool valid_ = !is_same_v<V, Dummy>;
        Id id_ = add_key_value(*kptr, *vptr, *imap_ptr,
                               move(key_), move(val));
        return cons_tp<T>(kptr, vptr, imap_ptr, move(id_), move(preds_l_ptr),
                          valid_);
    }

    template <typename Sort, typename ArgSort, typename ConfT>
    template <typename B>
    const Ptr& Base<Sort, ArgSort, ConfT>::Auto_mixin<B>::
        cassigner_ptr(const Id& kid) const
    {
        return cpreds_base().cptr(kid);
    }

    template <typename Sort, typename ArgSort, typename ConfT>
    template <typename B>
    Ptr& Base<Sort, ArgSort, ConfT>::Auto_mixin<B>::
        assigner_ptr(const Id& kid)
    {
        return preds_base().ptr(kid);
    }
}
