#pragma once

#include "util/flag/alg.hpp"

namespace unsot::expr::var::assign {
    template <typename B, typename AssigneeT>
    const auto& Assigner_mixin<B, AssigneeT>::cassignee_id() const noexcept
    {
        assert(size(cassignee_ids()) == 1);
        return *cbegin(cassignee_ids());
    }

    template <typename B, typename AssigneeT>
    auto& Assigner_mixin<B, AssigneeT>::assignee_id() noexcept
    {
        assert(size(cassignee_ids()) == 1);
        return *begin(assignee_ids());
    }

    template <typename B, typename AssigneeT>
    const auto& Assigner_mixin<B, AssigneeT>::cassigned_id() const noexcept
    {
        assert(size(cassigned_ids()) == 1);
        return *cbegin(cassigned_ids());
    }

    template <typename B, typename AssigneeT>
    auto& Assigner_mixin<B, AssigneeT>::assigned_id() noexcept
    {
        assert(size(cassigned_ids()) == 1);
        return *begin(assigned_ids());
    }

    template <typename B, typename AssigneeT>
    const auto& Assigner_mixin<B, AssigneeT>::cassignee(const Id& aid) const
    {
        return Assignee::cast(this->carg_ptr(aid));
    }

    template <typename B, typename AssigneeT>
    auto& Assigner_mixin<B, AssigneeT>::assignee(const Id& aid)
    {
        return Assignee::cast(this->arg_ptr(aid));
    }

    template <typename B, typename AssigneeT>
    const auto& Assigner_mixin<B, AssigneeT>::cassignee(const Key& key_) const
    {
        return Assignee::cast(this->carg_ptr(key_));
    }

    template <typename B, typename AssigneeT>
    auto& Assigner_mixin<B, AssigneeT>::assignee(const Key& key_)
    {
        return Assignee::cast(this->arg_ptr(key_));
    }

    ////////////////////////////////////////////////////////////////

    template <typename B, typename AssignerT>
    const auto& Auto_mixin<B, AssignerT>::cassigner(const Id& aid) const
    {
        return Assigner::cast(cassigner_ptr(aid));
    }

    template <typename B, typename AssignerT>
    auto& Auto_mixin<B, AssignerT>::assigner(const Id& aid)
    {
        return Assigner::cast(assigner_ptr(aid));
    }

    ////////////////////////////////////////////////////////////////

    template <typename B>
    Flag Auto_visit_mixin<B>::computable_body_impl() const noexcept
    {
        auto sg = this->template make_visited_ids_scope_guard<Assigner>();

        return any_true(this->cassigner_ids(), [this](auto& aid){
            return Assigner::visit_id(aid, this->cassigner(aid),
                                      &Assigner::any_assignable,
                                      &Assigner::surely_any_assignable);
        });
    }

    ////////////////////////////////////////////////////////////////

    template <typename B>
    void Assigner_distance_mixin<B>::assign(Arg_value val, Assignee& ass) noexcept
    {
        Inherit::assign(move(val), ass);
        ass.assign_me_distance(this->eval_distance());
    }
}
