#pragma once

#include "expr/fun.hpp"

namespace unsot::expr::var::fun {
    template <typename B, typename ArgVar, typename FunT> class Mixin;
    template <typename B> class Pred_mixin;

    template <typename B> class Visit_mixin;
    template <typename B> class Distance_mixin;
}

namespace unsot::expr::var {
    template <typename Sort, typename ConfT, typename ArgVar, typename FunT>
        using Fun_base_tp = Cond_mixin<conf::has_distance_v<ConfT>,
                                       fun::Distance_mixin,
                            Cond_mixin<conf::visitable_v<ConfT>,
                                       fun::Visit_mixin,
                            Cond_mixin<FunT::is_pred_v, fun::Pred_mixin,
                            fun::Mixin<Var<Sort, ConfT>, ArgVar, FunT>>>>;
    template <typename Sort, typename ConfT = Conf,
              typename ArgVar = Var<Sort, ConfT>,
              typename FunT = expr::Fun<Sort>>
        using Fun_base = Fun_base_tp<Sort, ConfT, ArgVar, FunT>;
    template <typename Sort, typename ConfT = Conf,
              typename ArgVar = Var<Sort, ConfT>,
              typename PredT = expr::Pred<Sort>>
        using Pred_base = Fun_base_tp<Bool, ConfT, ArgVar, PredT>;

    namespace aux {
        template <typename T> using Is_fun = Void<typename Decay<T>::Fun_t>;
        template <typename T> using Is_pred = Void<typename Decay<T>::Pred_t>;
    }

    template <typename T>
        constexpr bool is_fun_v = enabled_v<aux::Is_fun, T>;
    template <typename T>
        constexpr bool is_pred_v = enabled_v<aux::Is_pred, T>;
}

namespace unsot::expr::var::fun {
    template <typename B, typename ArgVar, typename FunT>
    class Mixin : public Inherit<B, Mixin<B, ArgVar, FunT>> {
    public:
        using Inherit = unsot::Inherit<B, Mixin<B, ArgVar, FunT>>;
        using typename Inherit::This;
        using typename Inherit::Base;
        using Fun_t = This;

        using typename Inherit::Value;
        using typename Inherit::Values_ptr;

        using Arg_var = ArgVar;

        static_assert(!Arg_var::is_base_v);

        using Fun = FunT;

        static_assert(is_same_v<Value, typename Fun::Ret>);

        using Arg_value = typename Fun::Value;
        using Arg_values = expr::Values<Arg_value>;
        using Arg_values_ptr = expr::Values_ptr<Arg_value>;

        static_assert(is_same_v<typename Arg_var::Value, Arg_value>);

        using typename Inherit::Depend_ids;
        using Arg_ids = typename Fun::Key_ids;

        static_assert(is_same_v<Id, typename Arg_ids::value_type>);
        static_assert(is_same_v<Depend_ids, Arg_ids>);

        using Inherit::Parent::operator =;
        Mixin()                                                     = default;
        virtual ~Mixin()                                            = default;
        Mixin(const Mixin&)                                          = delete;
        Mixin& operator =(const Mixin&)                              = delete;
        Mixin(Mixin&&)                                              = default;
        Mixin& operator =(Mixin&&)                                  = default;

        using Inherit::cons_tp;
        template <typename T, typename V = Dummy>
            static T cons_tp(Keys_ptr&, Values_ptr&, Ids_map_ptr&, Fun, Key, V = {});

        bool is_var() const noexcept override                { return false; }

        size_t size() const noexcept             { return std::size(cfun()); }
        bool empty() const noexcept             { return std::empty(cfun()); }

        const auto& cfun() const noexcept                     { return _fun; }
        const auto& fun() const& noexcept                   { return cfun(); }
        auto& fun()& noexcept                                 { return _fun; }
        auto&& fun()&& noexcept                        { return move(fun()); }
        const auto& cformula() const noexcept;
        auto& formula() noexcept;

        virtual const Arg_ids& carg_ids() const;
        const Depend_ids& cdepend_ids() const override;

        virtual const Ptr& carg_ptr(const Id&) const                      = 0;
        virtual Ptr& arg_ptr(const Id&)                                   = 0;
        const Ptr& carg_ptr(const Key&) const;
        Ptr& arg_ptr(const Key&);
        const auto& carg_var(const Id&) const;
        auto& arg_var(const Id&);
        const auto& carg_var(const Key&) const;
        auto& arg_var(const Key&);

        virtual Flag evaluable_part(Idx) const noexcept;
        virtual Flag surely_evaluable_part(Idx) const noexcept;

        /// These are partial evaluations
        /// which should yet eval. to `Arg_value`
        virtual Arg_value eval_part(Idx);

        Flag independent() const noexcept override;
        virtual Flag independent_arg(const Id&) const noexcept;

        String body_to_string() const override;
    protected:
        using Opers = expr::fun::Opers<Value, Arg_value>;

        explicit Mixin(Keys_ptr, Values_ptr, Ids_map_ptr, Id, Fun, bool is_set_ = false);

        bool lvalid_pre_init() const noexcept;
        void lcheck_pre_init() const;
        void lrecover_pre_init();

        Flag computable_body() const noexcept override;
        virtual Flag evaluable_arg_ids(const Arg_ids&) const noexcept;
        virtual Flag surely_evaluable_arg_ids(const Arg_ids&) const noexcept;

        bool compute_body_prepare() override;
        Value compute_body_value(bool success) override;
        virtual void eval_arg_ids(const Arg_ids&);

        bool lequals(const This&) const noexcept;
    private:
        virtual Arg_ids& arg_ids()                            { throw error; }

        Fun _fun{};
    };

    /// Every predicate has `Bool` output value
    template <typename B>
    class Pred_mixin : public Inherit<B, Pred_mixin<B>> {
    public:
        using Inherit = unsot::Inherit<B, Pred_mixin<B>>;
        using typename Inherit::This;
        using typename Inherit::Base;
        using Pred_t = This;

        using typename Inherit::Value;

        static_assert(is_same_v<Value, Bool>);

        using typename Inherit::Fun;

        using typename Inherit::Arg_value;

        static_assert(is_derived_from_v<Fun, expr::Pred<Arg_value>>);

        using Inherit::Parent::operator =;
        Pred_mixin()                                                = default;
        virtual ~Pred_mixin()                                       = default;
        Pred_mixin(const Pred_mixin&)                                = delete;
        Pred_mixin& operator =(const Pred_mixin&)                    = delete;
        Pred_mixin(Pred_mixin&&)                                    = default;
        Pred_mixin& operator =(Pred_mixin&&)                        = default;

        virtual bool falsified() const noexcept;
    protected:
        using Inherit::Inherit;
    };

    template <typename B>
    class Visit_mixin : public Inherit<B, Visit_mixin<B>> {
    public:
        using Inherit = unsot::Inherit<B, Visit_mixin<B>>;
        using typename Inherit::This;
        using typename Inherit::Base;
        using Fun_visit_t = This;

        using typename Inherit::Arg_var;

        using typename Inherit::Arg_ids;

        using Inherit::Parent::operator =;
        Visit_mixin()                                               = default;
        virtual ~Visit_mixin()                                      = default;
        Visit_mixin(const Visit_mixin&)                              = delete;
        Visit_mixin& operator =(const Visit_mixin&)                  = delete;
        Visit_mixin(Visit_mixin&&)                                  = default;
        Visit_mixin& operator =(Visit_mixin&&)                      = default;
    protected:
        using Inherit::Inherit;

        Flag evaluable_arg_ids(const Arg_ids&) const noexcept override;
    };

    template <typename B>
    class Distance_mixin : public Inherit<B, Distance_mixin<B>> {
    public:
        using Inherit = unsot::Inherit<B, Distance_mixin<B>>;
        using typename Inherit::This;
        using typename Inherit::Base;
        using Fun_distance_t = This;

        using Inherit::Parent::operator =;
        Distance_mixin()                                            = default;
        virtual ~Distance_mixin()                                   = default;
        Distance_mixin(const Distance_mixin&)                        = delete;
        Distance_mixin& operator =(const Distance_mixin&)            = delete;
        Distance_mixin(Distance_mixin&&)                            = default;
        Distance_mixin& operator =(Distance_mixin&&)                = default;

        Distance compute_distance() noexcept override;
    protected:
        using Inherit::Inherit;
    };
}

#include "expr/var/fun.inl"
