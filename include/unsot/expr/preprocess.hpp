#pragma once

#include "expr.hpp"

#include "util/string.hpp"
#include "util/optional.hpp"
#include "util/fun.hpp"
#include "util/ref.hpp"
#include "util/hash.hpp"
#include "expr/fun_arith.hpp"
#include "expr/pos.hpp"

#include <stack>

namespace unsot::expr {
    struct Macro;
    class Let;

    class Preprocess;

    constexpr char macro_char = '#';
    constexpr char arith_exp_char = '$';
    constexpr char escape_char = '\\';

    inline const String escape_char_str = unsot::to_string(escape_char);

    constexpr char ref_exp_char = '^';

    inline const String ref_exp_char_str = unsot::to_string(ref_exp_char);

    bool is_macro(String_view) noexcept;
    void check_is_macro(String_view);
    void check_is_not_macro(String_view);
    bool is_macro_char(char) noexcept;

    bool is_let_ref(String_view) noexcept;
    void check_is_let_ref(String_view);
    void check_is_not_let_ref(String_view);

    bool is_cat(String_view) noexcept;
    bool is_infix_cat(String_view) noexcept;
    bool is_prefix_cat(String_view) noexcept;

    bool is_arith_exp(String_view) noexcept;
    bool is_int_arith_exp(String_view) noexcept;
    bool is_real_arith_exp(String_view) noexcept;
    bool is_arith_exp_char(char) noexcept;
    void check_is_arith_exp(String_view);
    void check_is_int_arith_exp(String_view);
    void check_is_real_arith_exp(String_view);

    Optional<bool> is_arith_exp_and_int(String_view) noexcept;
    bool is_arith_exp_and_int_check(String_view);

    bool is_escaped(String_view) noexcept;
    bool is_escape_char(char) noexcept;

    bool is_let_ref_at(const Pos&) noexcept;
    void check_is_let_ref_at(const Pos&);

    bool is_let_at(const Pos&) noexcept;
    void check_is_let_at(const Pos&);
    void check_is_not_let_at(const Pos&);

    template <typename P> Forward_as_ref<Let, P> peek_let(P&&) noexcept;
    template <typename P>
        auto get_let(P&&) noexcept
            -> Cond_t<Decay<P>::is_const_v, const Let&, Forward_as_ref<Let, P>>;
    Let extract_let(Pos&);
    template <typename P> Forward_as_ref<Let, P> peek_let_check(P&&);
    template <typename P>
        auto get_let_check(P&&)
            -> Cond_t<Decay<P>::is_const_v, const Let&, Forward_as_ref<Let, P>>;
    Let extract_let_check(Pos&);

    [[nodiscard]] Key key_to_macro(Key);
    void macro_to_key(Key&);
    [[nodiscard]] Key&& macro_to_key(Key&&);
    [[nodiscard]] inline Key macro_to_key(const Key& macro)
                                          { return macro_to_key(Key(macro)); }
    [[nodiscard]] String_view macro_to_key_view(const Key&) noexcept;

    [[nodiscard]] Key key_to_let_ref(Key);
    void let_ref_to_key(Key&);
    [[nodiscard]] Key&& let_ref_to_key(Key&&);
    [[nodiscard]] inline Key let_ref_to_key(const Key& lref)
                                         { return let_ref_to_key(Key(lref)); }
    [[nodiscard]] String_view let_ref_to_key_view(const Key&) noexcept;

    void rm_prefix_cat_attr(Key&);

    [[nodiscard]] Key escape(Key);
    void unescape(Key&);
    [[nodiscard]] Key&& unescape(Key&&);
    [[nodiscard]] inline Key unescape(const Key& key_)
                                               { return unescape(Key(key_)); }

    template <typename I> [[nodiscard]] List preprocess(I&&);
}

#include "expr/preprocess/macro.hpp"
#include "expr/preprocess/let.hpp"

namespace unsot::expr {
    /// Undefined behavior if the list contains elements other than `Elem`
    class Preprocess : public Object<Preprocess> {
    public:
        class Run;

        using Iterator = List::iterator;

        Preprocess()                                                = default;
        ~Preprocess()                                               = default;
        Preprocess(const Preprocess&)                                = delete;
        Preprocess& operator =(const Preprocess&)                    = delete;
        Preprocess(Preprocess&&)                                    = default;
        Preprocess& operator =(Preprocess&&)                        = default;
        Preprocess(const char* input)          : Preprocess(String(input)) { }
        Preprocess(String);
        Preprocess(istream&);

        [[nodiscard]] static List perform_lines(String);
        [[nodiscard]] static List perform_lines(istream&);
        [[nodiscard]] List perform_list();
        [[nodiscard]] List perform_list(List);

        [[nodiscard]] List perform();
        [[nodiscard]] List perform(String);
        [[nodiscard]] List perform(istream&);

        [[nodiscard]] Run* run_l() const noexcept           { return _run_l; }
        void set_run(Run&) noexcept;
        Run& run_ref();

        bool contains(const Key&) const noexcept;
        void check_contains(const Key&) const;

        bool contains_macro(const Key&) const noexcept;
        void check_contains_macro(const Key&) const;
        void check_not_contains_macro(const Key&) const;

        bool contains_let(const Key&) const noexcept;
        void check_contains_let(const Key&) const;

        void add_macro(const Key&, Macro);
        void erase_macro(const Key&);

        template <bool groupedV = false, typename... Args> Let& push_let(const Key&, Args&&...);
        void pop_let(const Key&, bool grouped = false);
    protected:
        enum class Expand_lets { neutral = 0, no, yes, all, copy, ban };
        static constexpr Expand_lets def_expanding_lets = Expand_lets::no;

        using Macros_map = Hash<Key, Macro>;
        using Let_ptrs_map = Hash<Key, Ptr>;

        using Let_keys_stack = std::stack<Keys>;

        using Reserved_macro_f = function<void(This*, Pos&, int)>;
        using Reserved_macro_fs_map = Hash<Key, Reserved_macro_f>;

        using Macro_for_loop_step_f = Lazy<void>;
        using Macro_for_loop_f = function<void(Macro_for_loop_step_f)>;

        static const Reserved_macro_fs_map reserved_macro_fs_map;

        explicit Preprocess(List);

        const auto& cmacros_map() const noexcept       { return _macros_map; }
        auto& macros_map() noexcept                    { return _macros_map; }
        inline const auto& cmacro(const Key&) const;
        inline auto& macro(const Key&);

        const auto& clet_ptrs_map() const noexcept   { return _let_ptrs_map; }
        auto& let_ptrs_map() noexcept                { return _let_ptrs_map; }
        inline const auto& clet(const Key&) const;
        inline auto& let(const Key&);

        inline const auto& cexpanding_lets() const noexcept;
        inline auto& expanding_lets() noexcept;

        Optional<Macros_map::const_iterator> cfind_macro(const Key&) const noexcept;
        Optional<Macros_map::iterator> find_macro(const Key&) noexcept;

        Optional<Let_ptrs_map::const_iterator> cfind_let_ptr(const Key&) const noexcept;
        Optional<Let_ptrs_map::iterator> find_let_ptr(const Key&) noexcept;

        static String parse_lines(istream&);
        static String parse_lines(String&&);

        static void parse_lines_macro_define(istream&, String&);
        static void parse_lines_macro_endlet(istream&, String&);

        template <Expand_lets = Expand_lets::neutral, bool assertNoLets = true>
            void parse(Pos&, int depth);
        template <Expand_lets = Expand_lets::neutral, bool assertNoLets = true>
            void parse_elem(Pos&, int depth);
        template <Expand_lets = Expand_lets::neutral, bool assertNoLets = true>
            void parse_key(Pos&, int depth);
        inline void parse_value(Pos&, int depth);
        template <Expand_lets = Expand_lets::neutral, bool assertNoLets = true>
            void parse_list(Pos&, int depth);
        template <Expand_lets = Expand_lets::neutral> void parse_let(Pos&, int depth);
        template <Expand_lets = Expand_lets::neutral, bool assertNoLets = true>
            Iterator expand(Pos&, int depth);
        template <Expand_lets = Expand_lets::neutral, bool assertNoLets = true>
            Iterator expand_elem(Pos&, int depth);
        template <Expand_lets = Expand_lets::neutral, bool assertNoLets = true>
            Iterator expand_key(Pos&, int depth);
        inline Iterator expand_value(Pos&, int depth);
        template <bool incDepth = true, Expand_lets = Expand_lets::neutral,
                  bool assertNoLets = true>
            Iterator expand_list(Pos&, int depth);

        template <bool incDepth = true, Expand_lets = Expand_lets::neutral,
                  bool assertNoLets = true>
            void parse_nested(List&, int depth = 0);
        template <bool incDepth = true, Expand_lets = Expand_lets::neutral,
                  bool assertNoLets = true>
            void parse_nested(Pos&, int depth);

        /// Only lists cannot become anything other than list again after exp.
        template <bool incDepth = true, Expand_lets = Expand_lets::neutral,
                  bool assertNoLets = true>
            List& peek_parse_list(Pos&, int depth);
        template <bool incDepth = true, Expand_lets = Expand_lets::neutral,
                  bool assertNoLets = true>
            List extract_parse_list(Pos&, int depth);
        template <bool incDepth = true, Expand_lets = Expand_lets::neutral,
                  bool assertNoLets = true>
            List& peek_parse_list_check(Pos&, int depth);
        template <bool incDepth = true, Expand_lets = Expand_lets::neutral,
                  bool assertNoLets = true>
            List extract_parse_list_check(Pos&, int depth);

        template <Expand_lets, typename ParseF>
            void parse_enclosure_tp(Pos&, int depth, ParseF);
        template <Expand_lets = Expand_lets::neutral, bool assertNoLets = false, typename ParseF>
            Iterator expand_tp(Pos&, int depth, ParseF);

        template <Expand_lets = Expand_lets::neutral>
            void maybe_check_is_not_let(const Ptr&) const;
        template <Expand_lets = Expand_lets::neutral>
            void maybe_assert_no_lets(const Ptr&) const noexcept;
        template <Expand_lets = Expand_lets::neutral>
            void maybe_check_is_not_let_at(const Pos&) const;
        template <Expand_lets = Expand_lets::neutral>
            void maybe_assert_no_lets_at(const Pos&) const noexcept;

        template <Expand_lets = Expand_lets::yes> Ptr expand_let(const Let&) const;
        template <Expand_lets = Expand_lets::yes> void maybe_expand_let(Ptr&) const;
        template <Expand_lets = Expand_lets::yes> void maybe_expand_let_at(Pos&) const;

        Ptr peek_let_ref(Pos&, int depth);
        Ptr get_let_ref(Pos&, int depth);
        Ptr extract_let_ref(Pos&, int depth);
        template <Expand_lets = Expand_lets::yes> void expand_let_ref(Pos&, int depth);

        Ptr peek_let_ref(Pos& pos, String_view let_key_vw, int depth);

        template <bool expandV = true, Expand_lets = Expand_lets::all, typename EvalF>
            void parse_eval(Pos&, int depth, EvalF);
        template <bool expandV = true, Expand_lets = Expand_lets::all, typename EvalF>
            Iterator expand_eval(Pos&, int depth, EvalF);
        template <bool expandV = true, Expand_lets = Expand_lets::all, typename EvalF>
            Elem extract_eval(Pos&, int depth, EvalF);
        template <bool expandV = true, Expand_lets = Expand_lets::all, typename EvalF>
            bool extract_pred(Pos&, int depth, EvalF);
        template <int n, bool expandV = true, Expand_lets = Expand_lets::all, typename EvalF>
            Elem extract_nary_eval(Pos&, int depth, EvalF);
        template <int n, bool expandV = true, Expand_lets = Expand_lets::all, typename EvalF>
            bool extract_nary_pred(Pos&, int depth, EvalF);

        static void maybe_escape_key(Pos& pos, Key&);
        static Keys split_key(Key&);
        template <Expand_lets = Expand_lets::neutral, bool assertNoLets = true>
            void parse_key_single(Pos&, Key&, int depth);
        template <Expand_lets = Expand_lets::neutral, bool assertNoLets = true>
            void parse_key_single_not_arith(Pos&, Key&, int depth);
        void parse_key_multi(Pos&, Keys&&, int depth);

        /// concatenates previous place with the current
        void cat(Pos&, int depth);
        /// adds suffix to the current place from the next
        static void suffix(Pos& pos);
        static void suffix_key(Pos& pos);
        static void suffix_list(Pos& pos);
        static void cat_key_elem(Pos&);
        static void cat_key_list(Pos&);
        static void cat_lists(Pos&);
        static void cat_list_elem(Pos&);
        static void suffix(Ptr&, String suff);
        static void prefix(Ptr&, String pref);
        static void suffix_key(Key&, String suff);
        static void prefix_elem(Elem&, String suff);
        static void prefix_key(Key&, String pref);
        static void suffix_list(List&, String suff);
        static void prefix_list(List&, String pref);

        template <Expand_lets = Expand_lets::neutral>
            void parse_macro(Pos&, const Key&, int depth);
        template <Expand_lets = Expand_lets::neutral>
            void parse_macro_exp(Pos&, int depth);
        template <Expand_lets = Expand_lets::neutral>
            void parse_user_macro(Pos&, const Key&, int depth);
        void parse_ref_exp(Pos&, int depth);

        void parse_macro_include(Pos&, int depth);
        void parse_macro_def(Pos&, int depth);
        void parse_macro_undef(Pos&, int depth);
        void parse_macro_let(Pos&, int depth);
        void parse_macro_endlet(Pos&, int depth);
        void parse_macro_set(Pos&, int depth);
        void parse_macro_if(Pos&, int depth);
        void parse_macro_ifdef(Pos&, int depth);
        void parse_macro_ifndef(Pos&, int depth);
        inline void parse_macro_isdef(Pos&, int depth);
        inline void parse_macro_isndef(Pos&, int depth);
        void parse_macro_for(Pos&, int depth);
        void parse_macro_while(Pos&, int depth);
        /// '#' or '()'
        inline void parse_macro_null(Pos&, int depth);
        inline void parse_macro_listp(Pos&, int depth);
        inline void parse_macro_numberp(Pos&, int depth);
        inline void parse_macro_refp(Pos&, int depth);
        inline void parse_macro_equal(Pos&, int depth);
        inline void parse_macro_eq(Pos&, int depth);
        /// size of key/list, fails with number
        inline void parse_macro_len(Pos&, int depth);
        void parse_macro_print(Pos&, int depth);
        void parse_macro_assert(Pos&, int depth);
        void parse_macro_error(Pos&, int depth);
        inline Ref<Ptr> peek_macro_car(Pos&, int depth);
        inline Ptr peek_macro_cdr(Pos&, int depth);
        inline Ref<Ptr> peek_macro_nth(Pos&, int depth);
        inline Ptr peek_macro_nthcdr(Pos&, int depth);
        inline void parse_macro_car(Pos&, int depth);
        inline void parse_macro_cdr(Pos&, int depth);
        inline void parse_macro_nth(Pos&, int depth);
        inline void parse_macro_nthcdr(Pos&, int depth);
        void parse_macro_ref_name(Pos&, int depth);

        template <bool expandV = true> Ptr extract_macro_key(Pos&, int depth);
        Macro::Body extract_macro_body(Pos&, Key);

        bool extract_macro_if_head(Pos&, int depth);
        void parse_macro_if_body(Pos&, int depth, bool cond);

        template <bool negV = false, bool expandV = true>
            bool extract_macro_isdef(Pos&, int depth);

        Macro_for_loop_f parse_macro_for1_args(Pos&, Let&, Elem init);
        Macro_for_loop_f parse_macro_for2_args(Pos&, int depth, Let&, Elem init);
        Macro_for_loop_f parse_macro_for3_args(Let&, Ptr);
        void macro_for_loop_step(Pos&, int depth, const Macro::Body&);

        template <bool expandV = true> bool extract_macro_null(Pos&, int depth);
        template <bool expandV = true> bool extract_macro_listp(Pos&, int depth);
        template <bool expandV = true> bool extract_macro_numberp(Pos&, int depth);
        template <bool expandV = true> bool extract_macro_refp(Pos&, int depth);
        template <bool expandV = true> bool extract_macro_equal(Pos&, int depth);
        template <bool expandV = true> bool extract_macro_eq(Pos&, int depth);
        template <bool expandV = true> Elem extract_macro_len(Pos&, int depth);

        void parse_user_macro_push_args(Pos&, const Key&, int depth);
        void parse_user_macro_pop_args(const Key&);

        inline void parse_eval_arith(Pos&, int depth);
        inline Iterator expand_eval_arith(Pos&, int depth);
        inline void parse_eval_arith_exp(Pos&, int depth);
        inline Iterator expand_eval_arith_exp(Pos&, int depth);

        template <bool expandV = true>
            Elem extract_eval_arith(Pos&, int depth);
        template <bool expandV = true>
            Elem extract_eval_arith_elem(Pos&, int depth);
        Elem extract_eval_arith_exp(Pos&, int depth);
        template <bool expandV = true>
            Elem extract_eval_arith_list(Pos&, int depth);
        template <typename FunT, bool expandV = true>
            FunT extract_eval_arith_cons_fun(Pos&, int depth, Keys = {});
        template <bool expandV = true>
            bool extract_eval_arith_pred(Pos&, int depth);

        template <typename F>
            void set_let_extract_tp(Pos& pos, int depth, F);
        template <bool groupedV, bool extractV = true>
            void push_let_grouped_extract(Pos&, int depth, const Key&);
    private:
        /// `Expand_lets` is the tp. parameter, but GCC would complain that it is protected
        template <auto expLets> struct Parse_enclosure;

        template <bool expandV, bool negV = false> class Foo;
        template <int expLets> class Foo_exp_lets;

        static constexpr bool do_inc = true;
        static constexpr bool no_inc = false;
        static constexpr bool do_neg = true;
        static constexpr bool no_neg = false;
        static constexpr bool do_exp = true;
        static constexpr bool no_exp = false;
        static constexpr bool copy_lets = true;
        static constexpr bool keep_lets = false;

        template <bool isCarOrCdr, bool isCarOrNth, typename NewPtrF>
            Ref<Ptr> peek_accessor_tp(Pos&, int depth, NewPtrF);
        template <bool isCarOrCdr, bool isCarOrNth, typename NewPtrF>
            void parse_accessor_tp(Pos&, int depth, NewPtrF);
        Ref<Ptr> peek_macro_car_impl(Ptr& src_ptr);
        Ptr peek_macro_cdr_impl(const Ptr& src_ptr);
        Ref<Ptr> peek_macro_nth_impl(Ptr& src_ptr, Idx);
        Ptr peek_macro_nthcdr_impl(const Ptr& src_ptr, Idx);

        List _list{};

        Macros_map _macros_map{};
        Let_ptrs_map _let_ptrs_map{};

        Let_keys_stack _let_keys_stack{};

        Expand_lets _expanding_lets{def_expanding_lets};

        Run* _run_l{};
    };
}

namespace unsot::expr {
    template <auto expLets>
    struct Preprocess::Parse_enclosure {
         Parse_enclosure(Preprocess&);
         ~Parse_enclosure();

         Preprocess& preprocess_ref;
         Expand_lets prev_exp_lets;
    };

    template <bool expandV, bool negV>
    class Preprocess::Foo final {
    public:
        static Ptr extract_macro_key([[maybe_unused]] Preprocess&, Pos&,
                                     [[maybe_unused]] int depth);
        static bool extract_macro_isdef(Preprocess&, Pos&, int depth);
        static bool extract_macro_null(Preprocess&, Pos&, int depth);
        static bool extract_macro_listp(Preprocess&, Pos&, int depth);
        static bool extract_macro_numberp(Preprocess&, Pos&, int depth);
        static bool extract_macro_refp(Preprocess&, Pos&, int depth);
        static bool extract_macro_equal(Preprocess&, Pos&, int depth);
        static bool extract_macro_eq(Preprocess&, Pos&, int depth);
        static Elem extract_macro_len(Preprocess&, Pos&, int depth);
        static Elem extract_eval_arith(Preprocess&, Pos&, int depth);
        static Elem extract_eval_arith_elem(Preprocess&, Pos&, int depth);
        static Elem extract_eval_arith_list(Preprocess&, Pos&, int depth);
    };
    extern template class Preprocess::Foo<true, false>;
    extern template class Preprocess::Foo<true, true>;
    extern template class Preprocess::Foo<false, false>;
    extern template class Preprocess::Foo<false, true>;

    template <int expLets>
    class Preprocess::Foo_exp_lets final {
    public:
        static constexpr Expand_lets expand_lets = Expand_lets(expLets);

        static void maybe_check_is_not_let(const Preprocess&, const Ptr&);
        static void maybe_assert_no_lets(const Preprocess&, const Ptr&) noexcept;
        static Ptr expand_let(const Preprocess&, const Let&);
        static void parse_key_single_not_arith(Preprocess&, Pos&, Key&, int depth);
        static void parse_macro(Preprocess&, Pos&, const Key&, int depth);
        static void parse_macro_exp(Preprocess&, Pos&, int depth);
        static void parse_user_macro(Preprocess&, Pos&, const Key&, int depth);
    };
    extern template class Preprocess::Foo_exp_lets<int(Preprocess::Expand_lets::neutral)>;
    extern template class Preprocess::Foo_exp_lets<int(Preprocess::Expand_lets::no)>;
    extern template class Preprocess::Foo_exp_lets<int(Preprocess::Expand_lets::yes)>;
    extern template class Preprocess::Foo_exp_lets<int(Preprocess::Expand_lets::all)>;
    extern template class Preprocess::Foo_exp_lets<int(Preprocess::Expand_lets::copy)>;
    extern template class Preprocess::Foo_exp_lets<int(Preprocess::Expand_lets::ban)>;
}

#include "expr/preprocess.inl"
