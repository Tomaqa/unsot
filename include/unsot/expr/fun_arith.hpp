#pragma once

#include "expr/fun.hpp"

#ifdef NO_EXPLICIT_TP_INST
#include "expr/fun.tpp"
#endif

#ifndef NO_EXPLICIT_TP_INST
namespace unsot::expr::fun {
    extern template class Fun_tp<Int>;
    extern template class Fun_tp<Real>;
    extern template class Fun_tp<Idx>;

    extern template class Fun_tp<Int, Fun_conf>;
    extern template class Fun_tp<Real, Fun_conf>;
    extern template class Fun_tp<Idx, Fun_conf>;

    extern template class Fun_tp<Int, Pred_conf>;
    extern template class Fun_tp<Real, Pred_conf>;
    extern template class Fun_tp<Idx, Pred_conf>;
}
#endif  /// NO_EXPLICIT_TP_INST

#include "expr/opers_arith.hpp"
