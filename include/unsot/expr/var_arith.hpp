#pragma once

#include "expr/var.hpp"

#include "expr/fun_arith.hpp"

#ifdef NO_EXPLICIT_TP_INST
#include "expr/var.tpp"
#endif

#ifndef NO_EXPLICIT_TP_INST
namespace unsot::expr {
    extern template class Var<Int>::Mixin;
    extern template class Var<Real>::Mixin;
    extern template class Var<Int, var::Full_conf>::Visit_mixin;
    extern template class Var<Real, var::Full_conf>::Visit_mixin;
    extern template class Var<Int, var::Full_conf>::Distance_mixin;
    extern template class Var<Real, var::Full_conf>::Distance_mixin;
}

namespace unsot::expr::var {
    namespace aux {
        template <typename Sort> using _Fbase = Fun_base<Sort>;
        template <typename Sort> using _Ffbase = Fun_base<Sort, Full_conf>;
        template <typename Sort> using _Pbase = Pred_base<Sort>;
        template <typename Sort> using _Fpbase = Pred_base<Sort, Full_conf>;

        template <typename Sort> using _Ass = Assignee<Sort>;
        template <typename Sort> using _Fass = Assignee<Sort, Full_conf>;
    }

    extern template class aux::_Fbase<Int>::Fun_t::Mixin;
    extern template class aux::_Fbase<Real>::Fun_t::Mixin;
    extern template class aux::_Ffbase<Int>::Fun_t::Mixin;
    extern template class aux::_Ffbase<Real>::Fun_t::Mixin;
    extern template class aux::_Ffbase<Int>::Fun_visit_t::Visit_mixin;
    extern template class aux::_Ffbase<Real>::Fun_visit_t::Visit_mixin;
    extern template class aux::_Ffbase<Int>::Fun_distance_t::Distance_mixin;
    extern template class aux::_Ffbase<Real>::Fun_distance_t::Distance_mixin;
    extern template class aux::_Pbase<Int>::Fun_t::Mixin;
    extern template class aux::_Pbase<Real>::Fun_t::Mixin;
    extern template class aux::_Pbase<Int>::Pred_mixin;
    extern template class aux::_Pbase<Real>::Pred_mixin;
    extern template class aux::_Fpbase<Int>::Fun_t::Mixin;
    extern template class aux::_Fpbase<Real>::Fun_t::Mixin;
    extern template class aux::_Fpbase<Int>::Pred_mixin;
    extern template class aux::_Fpbase<Real>::Pred_mixin;
    extern template class aux::_Fpbase<Int>::Fun_visit_t::Visit_mixin;
    extern template class aux::_Fpbase<Real>::Fun_visit_t::Visit_mixin;
    extern template class aux::_Fpbase<Int>::Fun_distance_t::Distance_mixin;
    extern template class aux::_Fpbase<Real>::Fun_distance_t::Distance_mixin;

    extern template class aux::_Ass<Int>::Assignee_t::Mixin;
    extern template class aux::_Ass<Real>::Assignee_t::Mixin;
    extern template class aux::_Fass<Int>::Assignee_t::Mixin;
    extern template class aux::_Fass<Real>::Assignee_t::Mixin;
    extern template class aux::_Fass<Int>::Assignee_distance_t::Distance_mixin;
    extern template class aux::_Fass<Real>::Assignee_distance_t::Distance_mixin;
}
#endif  /// NO_EXPLICIT_TP_INST
