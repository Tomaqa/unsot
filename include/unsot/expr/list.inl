#pragma once

#include "util/alg.hpp"

namespace unsot::expr {
    template <typename ItT, Req<is_iterator_v<ItT>>>
    List::List(ItT first, ItT last)
        : List(first, last, deep_copy_tag)
    { }

    template <typename ItT, Req<is_iterator_v<ItT>>>
    List::List(ItT first, ItT last, Shallow_copy_tag)
    {
        to(*this, first, last);
    }

    template <typename ItT, Req<is_iterator_v<ItT>>>
    List::List(ItT first, ItT last, Deep_copy_tag)
    {
        insert(cend(), first, last);
    }

    template <typename T, typename E, typename U, typename... Args>
    T List::cons_tp(U&& first, Args&&... args)
    {
        if constexpr ((is_string_v<U> || is_same_v<Decay<U>, istream>) && sizeof...(Args) == 0)
            return Inherit::template cons_tp<T>(FORWARD(first), tag<E>);
        else return Inherit::template cons_tp<T>(FORWARD(first), FORWARD(args)...);
    }

    template <typename T>
    T List::cons_tp()
    {
        return Inherit::template cons_tp<T>();
    }

    template <typename ItT>
    List List::shallow_copy(ItT first, ItT last)
    {
        return List(first, last, shallow_copy_tag);
    }

    template <typename ItT>
    List List::deep_copy(ItT first, ItT last)
    {
        return List(first, last, deep_copy_tag);
    }

    bool List::is_me(const Ptr& ptr) noexcept
    {
        assert(ptr);
        assert(!ptr->is_elem() || !ptr->is_list());
        return ptr->is_list();
    }

    template <typename ItT>
    List::iterator List::insert(const_iterator pos_, ItT first, ItT last)
    {
        for (auto it = first; it != last; ++it) {
            emplace(pos_, (*it)->to_ptr());
            //+ but it can be also move iterators ...
            //+ emplace(pos_, (*it)->deep_copy_to_ptr());
        }
        return util::to_iterator<Container>(pos_);
    }

    template <typename L>
    auto List::make_to_string(L&& ls)
    {
        using L_ = Rm_ref<L>;
        return To_string<L_>(Ref<L_>(FORWARD(ls)));
    }
}

////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////

namespace unsot::expr {
    template <typename L, Req<is_list_v<L>>>
    Forward_as_ref<Elem_base, L> cast_as_elem_base(L&& ls) noexcept
    {
        return cast_as_elem<Elem_base>(FORWARD(ls));
    }

    template <typename L, Req<is_list_v<L>>>
    Forward_as_ref<Key, L> cast_as_key(L&& ls)
    {
        return cast_key(Forward_as_ref<Ptr, L>(ls.front()));
    }

    template <typename L, Req<is_list_v<L>>>
    Forward_as_ref<Elem_base, L> cast_as_elem_base_check(L&& ls)
    {
        return cast_as_elem_check<Elem_base>(FORWARD(ls));
    }

    template <typename L, Req<is_list_v<L>>>
    Forward_as_ref<Key, L> cast_as_key_check(L&& ls)
    {
        check_is_as_elem_base(ls);
        return cast_as_key(FORWARD(ls));
    }

    template <typename E, typename L, Req<is_list_v<L>>>
    Forward_as_ref<E, L> cast_as_elem(L&& ls) noexcept
    {
        return E::cast(Forward_as_ref<Ptr, L>(ls.front()));
    }

    template <typename Arg, typename E, typename L, Req<is_list_v<L>>>
    Forward_as_ref<Arg, L> cast_as_value(L&& ls)
    {
        return cast_value<Arg, E>(Forward_as_ref<Ptr, L>(ls.front()));
    }

    template <typename E, typename L, Req<is_list_v<L>>>
    Forward_as_ref<E, L> cast_as_elem_check(L&& ls)
    {
        check_is_as_elem<E>(ls);
        return cast_as_elem<E>(FORWARD(ls));
    }

    template <typename Arg, typename E, typename L, Req<is_list_v<L>>>
    Forward_as_ref<Arg, L> cast_as_value_check(L&& ls)
    {
        check_is_as_elem<E>(ls);
        return cast_as_value<Arg, E>(FORWARD(ls));
    }

    template <typename Arg, typename E, typename L, Req<is_list_v<L>>>
    auto cast_as_value_l(L&& ls) noexcept
    {
        //+ it not tested whether it is as value ...
        return cast_value_l<Arg, E>(Forward_as_ref<Ptr, L>(ls.front()));
    }

    template <typename E, typename L, Req<is_list_v<L>>>
    Forward_as_ref<Int, L> cast_as_int(L&& ls)
    {
        return cast_int<E>(Forward_as_ref<Ptr, L>(ls.front()));
    }

    template <typename E, typename L, Req<is_list_v<L>>>
    Forward_as_ref<Real, L> cast_as_real(L&& ls)
    {
        return cast_real<E>(Forward_as_ref<Ptr, L>(ls.front()));
    }

    template <typename E, typename L, Req<is_list_v<L>>>
    Forward_as_ref<Int, L> cast_as_int_check(L&& ls)
    {
        check_is_as_elem<E>(ls);
        return cast_as_int<E>(FORWARD(ls));
    }

    template <typename E, typename L, Req<is_list_v<L>>>
    Forward_as_ref<Real, L> cast_as_real_check(L&& ls)
    {
        check_is_as_elem<E>(ls);
        return cast_as_real<E>(FORWARD(ls));
    }

    template <typename E, typename L, Req<is_list_v<L>>>
    auto cast_as_key_l(L&& ls) noexcept
    {
        //+ it not tested whether it is as value ...
        return cast_key_l<E>(Forward_as_ref<Ptr, L>(ls.front()));
    }

    template <typename E, typename L, Req<is_list_v<L>>>
    auto cast_as_int_l(L&& ls) noexcept
    {
        //+ it not tested whether it is as value ...
        return cast_int_l<E>(Forward_as_ref<Ptr, L>(ls.front()));
    }

    template <typename E, typename L, Req<is_list_v<L>>>
    auto cast_as_real_l(L&& ls) noexcept
    {
        //+ it not tested whether it is as value ...
        return cast_real_l<E>(Forward_as_ref<Ptr, L>(ls.front()));
    }

    template <typename E>
    bool is_as_elem(const List& ls) noexcept
    {
        return aux::List_foo<E>::is_as_elem(ls);
    }

    template <typename Arg, typename E>
    bool is_as_value(const List& ls) noexcept
    {
        return is_as_elem<E>(ls) && is_value<Arg, E>(ls.cfront());
    }

    template <typename E>
    void check_is_as_elem(const List& ls)
    {
        aux::List_foo<E>::check_is_as_elem(ls);
    }

    template <typename Arg, typename E>
    void check_is_as_value(const List& ls)
    {
        check_is_as_elem<E>(ls);
        check_is_value<Arg, E>(ls.cfront());
    }

    template <typename E>
    bool is_as_int(const List& ls) noexcept
    {
        return is_as_value<Int, E>(ls);
    }

    template <typename E>
    bool is_as_real(const List& ls) noexcept
    {
        return is_as_value<Real, E>(ls);
    }

    template <typename E>
    void check_is_as_int(const List& ls)
    {
        check_is_as_value<Int, E>(ls);
    }

    template <typename E>
    void check_is_as_real(const List& ls)
    {
        check_is_as_value<Real, E>(ls);
    }

    template <typename E>
    bool has_elems_only(const List& ls) noexcept
    {
        return aux::List_foo<E>::has_elems_only(ls);
    }

    template <typename Arg, typename E>
    bool has_values_only(const List& ls) noexcept
    {
        return all_of(ls, is_value<Arg, E>);
    }

    template <typename E>
    void check_has_elems_only(const List& ls)
    {
        return aux::List_foo<E>::check_has_elems_only(ls);
    }

    template <typename Arg, typename E>
    void check_has_values_only(const List& ls)
    try {
        for_each(ls, check_is_value<Arg, E>);
    }
    catch (const Error& err) {
        try { check_has_elems_only<E>(ls); }
        catch (const Error& err2) {
            THROW(err2) + "\n" + err;
        }
        THROW(err);
    }

    template <typename E>
    bool has_ints_only(const List& ls) noexcept
    {
        return has_values_only<Int, E>(ls);
    }

    template <typename E>
    bool has_reals_only(const List& ls) noexcept
    {
        return has_values_only<Real, E>(ls);
    }

    template <typename E>
    void check_has_ints_only(const List& ls)
    {
        check_has_values_only<Int, E>(ls);
    }

    template <typename E>
    void check_has_reals_only(const List& ls)
    {
        check_has_values_only<Real, E>(ls);
    }

    template <typename L, Req<is_list_v<L>>>
    Keys cast_to_keys(L&& ls)
    {
        return to<Keys>(FORWARD(ls), cast_key<Forward_as_ref<Ptr, L>>);
    }

    template <typename L, Req<is_list_v<L>>>
    Lists cast_to_lists(L&& ls) noexcept
    {
        return to<Lists>(FORWARD(ls), List::cast<Forward_as_ref<Ptr, L>>);
    }

    template <typename L, Req<is_list_v<L>>>
    Keys cast_to_keys_check(L&& ls)
    {
        return to<Keys>(FORWARD(ls), cast_key_check<Forward_as_ref<Ptr, L>>);
    }

    template <typename L, Req<is_list_v<L>>>
    Lists cast_to_lists_check(L&& ls)
    {
        check_is_deep(ls);
        return cast_to_lists(FORWARD(ls));
    }

    template <typename E, typename L, Req<is_list_v<L>>>
    Values<E> cast_to_elems(L&& ls) noexcept
    {
        return to<Values<E>>(FORWARD(ls),
                             E::template cast<Forward_as_ref<Ptr, L>>);
    }

    template <typename Arg, typename E, typename L, Req<is_list_v<L>>>
    Values<Arg> cast_to_values(L&& ls)
    {
        return to<Values<Arg>>(FORWARD(ls),
                               cast_value<Arg, E, Forward_as_ref<Ptr, L>>);
    }

    template <typename E, typename L, Req<is_list_v<L>>>
    Values<E> cast_to_elems_check(L&& ls)
    {
        return to<Values<E>>(FORWARD(ls),
                             E::template cast_check<Forward_as_ref<Ptr, L>>);
    }

    template <typename Arg, typename E, typename L, Req<is_list_v<L>>>
    Values<Arg> cast_to_values_check(L&& ls)
    {
        return to<Values<Arg>>(FORWARD(ls),
            cast_value_check<Arg, E, Forward_as_ref<Ptr, L>>
        );
    }

    template <typename E, typename L, Req<is_list_v<L>>>
    Ints cast_to_ints(L&& ls)
    {
        return cast_to_values<Int, E>(FORWARD(ls));
    }

    template <typename E, typename L, Req<is_list_v<L>>>
    Reals cast_to_reals(L&& ls)
    {
        return cast_to_values<Real, E>(FORWARD(ls));
    }

    template <typename E, typename L, Req<is_list_v<L>>>
    Ints cast_to_ints_check(L&& ls)
    {
        return cast_to_values_check<Int, E>(FORWARD(ls));
    }

    template <typename E, typename L, Req<is_list_v<L>>>
    Reals cast_to_reals_check(L&& ls)
    {
        return cast_to_values_check<Real, E>(FORWARD(ls));
    }

    template <typename Arg, typename E, typename L, Req<is_list_v<L>>>
    Values<Optional<Arg>> to_values(L&& ls) noexcept
    {
        return to<Values<Optional<Arg>>>(FORWARD(ls),
            to_value<Arg, E, Forward_as<Ptr, L>>
        );
    }

    template <typename Arg, typename E, typename L, Req<is_list_v<L>>>
    Values<Arg> to_values_check(L&& ls)
    {
        return to<Values<Arg>>(FORWARD(ls),
            to_value_check<Arg, E, Forward_as<Ptr, L>>
        );
    }

    template <typename E, typename L, Req<is_list_v<L>>>
    Values<Optional<Int>> to_ints(L&& ls) noexcept
    {
        return to_values<Int, E>(FORWARD(ls));
    }

    template <typename E, typename L, Req<is_list_v<L>>>
    Values<Optional<Real>> to_reals(L&& ls) noexcept
    {
        return to_values<Real, E>(FORWARD(ls));
    }

    template <typename E, typename L, Req<is_list_v<L>>>
    Ints to_ints_check(L&& ls)
    {
        return to_values_check<Int, E>(FORWARD(ls));
    }

    template <typename E, typename L, Req<is_list_v<L>>>
    Reals to_reals_check(L&& ls)
    {
        return to_values_check<Real, E>(FORWARD(ls));
    }
}
