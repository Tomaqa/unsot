#pragma once

#include "expr.hpp"

namespace unsot::expr {
    class Formula : public Inherit<List, Formula> {
    public:
        template <typename F> struct To_string;

        using Inherit::Inherit;
        Formula()                                                   = default;
        virtual ~Formula()                                          = default;
        Formula(const Formula&)                                     = default;
        Formula& operator =(const Formula&)                         = default;
        Formula(Formula&&)                                          = default;
        Formula& operator =(Formula&&)                              = default;
        template <typename L, Req<is_list_v<L>> = 0> Formula(Key, L&&);

        /// is called in `valid`, but is available
        /// to allow ensuring before `virtual_init`
        virtual bool has_valid_oper() const noexcept;
        virtual void check_has_valid_oper() const;

        virtual bool has_valid_args() const noexcept;
        virtual void check_has_valid_args() const;

        const Key& coper() const noexcept                    { return _oper; }
        const Key& oper() const& noexcept                  { return coper(); }
        Key& oper()& noexcept                                { return _oper; }
        Key&& oper()&& noexcept                       { return move(oper()); }

        virtual void reserve(size_t);
        virtual void resize(size_t);

        template <typename F> static auto make_to_string(F&&);
        String to_string() const& override;
        String to_string()&& override;
    protected:
        template <typename B> class To_string_mixin;

        bool lvalid_pre_init() const noexcept;
        void lcheck_pre_init() const;
        void linit();

        bool lhas_valid_oper() const noexcept;
        void lcheck_has_valid_oper() const;
        virtual void init_oper();

        bool lhas_valid_args() const noexcept;
        void lcheck_has_valid_args() const;
        virtual void init_args();
        virtual void init_arg(Ptr&);
        virtual void init_arg_list(Ptr&);
        virtual Ptr init_arg_list_new(Ptr&&);
        virtual void init_arg_key(Ptr&)                                    { }
        virtual void init_arg_value(Ptr&)                                  { }

        bool lequals(const This&) const noexcept;
    private:
        template <typename F>
            static void reserve_impl([[maybe_unused]] F&, [[maybe_unused]] size_t);
        template <typename F>
            static void resize_impl([[maybe_unused]] F&, [[maybe_unused]] size_t);

        Key _oper{};
    };
}

namespace unsot::expr {
    template <typename B>
    class Formula::To_string_mixin : public Inherit<B> {
    public:
        using Inherit = unsot::Inherit<B>;

        using typename Inherit::Conf;

        using Inherit::Inherit;
    protected:
        void perform_head_post(String&, const Conf&, int depth) override;
        String ptr_to_string(const Ptr&, int depth) override;
        String ptr_to_string(Ptr&&, int depth) override;
    private:
        template <typename P> String ptr_to_string_impl(P&&, int depth);
    };

    template <typename F>
    struct Formula::To_string
        : Inherit<To_string_mixin<List::To_string_crtp<To_string<F>, F>>> {
        using Inherit =
            unsot::Inherit<To_string_mixin<List::To_string_crtp<To_string<F>,
                                                                F>>>;

        using Inherit::Inherit;
    };
    extern template class
        List::To_string_crtp<Formula::To_string<const Formula>,
                             const Formula>;
    extern template class
        List::To_string_crtp<Formula::To_string<Formula>, Formula>;
}

#include "expr/formula.inl"
