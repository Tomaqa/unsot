#pragma once

namespace unsot::solver {
    template <typename B, typename OdeSolver>
    const auto& Mixin<B, OdeSolver>::
        cflow_def(const flow::Id& fid) const noexcept
    {
        return flow::def(cflow_defs(), fid);
    }

    template <typename B, typename OdeSolver>
    auto& Mixin<B, OdeSolver>::flow_def(const flow::Id& fid) noexcept
    {
        return flow::def(flow_defs(), fid);
    }

    template <typename B, typename OdeSolver>
    const auto& Mixin<B, OdeSolver>::cflow_def(const Key& fkey) const
    {
        return flow::def(cflow_defs(), cflow_ids_map(), fkey);
    }

    template <typename B, typename OdeSolver>
    auto& Mixin<B, OdeSolver>::flow_def(const Key& fkey) noexcept
    {
        return flow::def(flow_defs(), cflow_ids_map(), fkey);
    }

    template <typename B, typename OdeSolver>
    const auto& Mixin<B, OdeSolver>::cflow_id(const Key& fkey) const
    {
        return flow::id(cflow_ids_map(), fkey);
    }

    template <typename B, typename OdeSolver>
    auto& Mixin<B, OdeSolver>::flow_id(const Key& fkey) noexcept
    {
        return flow::id(flow_ids_map(), fkey);
    }

    template <typename B, typename OdeSolver>
    const auto& Mixin<B, OdeSolver>::
        cflow_inst(const flow::Inst_id& fid) const
    {
        return flow::inst(cflow_insts(), fid);
    }

    template <typename B, typename OdeSolver>
    auto& Mixin<B, OdeSolver>::flow_inst(const flow::Inst_id& fid)
    {
        return flow::inst(flow_insts(), fid);
    }

    template <typename B, typename OdeSolver>
    const auto& Mixin<B, OdeSolver>::
        cfinal_var_ptr(const Key& ode_key, const Flow_inst& finst) const
    {
        assert(!flow::is_global_key(ode_key));
        assert(!flow::is_special_key(ode_key) || ode_key == flow::t_key);
        auto final_id = finst.cflow().ckeys().t_ode_key_id(ode_key);
        auto& final_key = finst.cfinal_keys()[final_id];
        return this->creals().cptr(final_key);
    }
}
