#pragma once

namespace unsot {
    template <typename B, typename T>
    Item_error_mixin<B, T>::Item_error_mixin(T&& item_, Error err)
        : Inherit(move(err)), _item(FORWARD(item_))
    { }
}
