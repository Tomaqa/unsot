#pragma once

#include "unsot/object.hpp"
#include "unsot/string.hpp"

#include <vector>

namespace unsot {
    template <typename T>
    class Vector : public Inherit<std::vector<T>, Vector<T>> {
    public:
        using Inherit = unsot::Inherit<std::vector<T>, Vector<T>>;
        using typename Inherit::This;
        using Base = typename Inherit::Parent;

        using typename Inherit::value_type;
        using typename Inherit::size_type;
        using typename Inherit::reference;
        using typename Inherit::const_reference;
        using typename Inherit::iterator;
        using typename Inherit::const_iterator;

        using Inherit::Inherit;

        using Inherit::size;

        const_reference operator [](size_type) const;
        reference operator [](size_type);

        /// Like `resize`, but never reduces size
        using Inherit::resize;
        void expand(size_type);
        void expand(size_type, const value_type&);

        String to_string() const;

        bool equals(const This&) const;
    };
}

#include "unsot/vector.inl"
