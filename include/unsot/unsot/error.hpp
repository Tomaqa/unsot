#pragma once

#include "unsot/object.hpp"
#include "unsot/string.hpp"

#ifndef DEBUG
#define THROW(msg) throw Error(msg)
#else
#define THROW(msg) \
    throw Error(unsot::to_string(__FILE__) \
                + ":" + unsot::to_string(__LINE__) \
                + ":\n" + unsot::to_string(__PRETTY_FUNCTION__) \
                + ":\n" + msg)
#endif

#define expect(condition, msg)            { if (condition); else THROW(msg); }

#define EXIT(msg) { CSTREAMLN(std::cerr, msg); exit(1); }

namespace unsot {
    template <typename B, typename T> class Item_error_mixin;

    template <typename T> using Item_error = Item_error_mixin<Error, T>;
}

namespace unsot {
    class Error : public Static<Error> {
    public:
        Error(String msg_ = "")                         : _msg(move(msg_)) { }
        ~Error()                                                    = default;
        Error(const Error&)                                         = default;
        Error& operator =(const Error&)                             = default;
        Error(Error&&)                                              = default;
        Error& operator =(Error&&)                                  = default;

        const String& cmsg() const noexcept                   { return _msg; }
        const String& msg() const& noexcept                 { return cmsg(); }
        String msg()&& noexcept                        { return move(msg()); }

        Error operator +(const Error&) const;
        Error operator +(const String&) const;
        friend Error operator +(const String& lhs, const Error& rhs);
        Error& operator +=(const Error&);
        Error& operator +=(const String&);

        const String& to_string() const                     { return cmsg(); }
        friend ostream& operator <<(ostream&, const Error&);
    private:
        String& msg()& noexcept                               { return _msg; }

        String _msg;
        mutable bool _printed{};
    };

    template <typename B, typename T>
    class Item_error_mixin : public Inherit<B, Item_error_mixin<B, T>> {
    public:
        using Inherit = unsot::Inherit<B, Item_error_mixin<B, T>>;
        using typename Inherit::This;

        Item_error_mixin(T&& item_, Error = {});
        ~Item_error_mixin()                                         = default;
        Item_error_mixin(const Item_error_mixin&)                   = default;
        Item_error_mixin& operator =(const Item_error_mixin&)       = default;
        Item_error_mixin(Item_error_mixin&&)                        = default;
        Item_error_mixin& operator =(Item_error_mixin&&)            = default;

        const T& citem() const noexcept                      { return _item; }
        T& item() noexcept                                   { return _item; }
    private:
        T _item;
    };

    const Error error;
}

#include "unsot/error.inl"
