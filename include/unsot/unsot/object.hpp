#pragma once

namespace unsot {
    template <bool cond, template<typename...> typename MixinT, typename T>
        using Cond_mixin = Cond_t<cond, MixinT<T>, T>;
}

namespace unsot {
    template <typename T>
    class Crtp {
    public:
        friend T;

        using That = T;

        const auto& cthat() const noexcept;
        auto& that() noexcept;
        auto cthat_l() const noexcept;
        auto that_l() noexcept;
    protected:
        Crtp()                                                      = default;
        ~Crtp()                                                     = default;
        Crtp(const Crtp&)                                           = default;
        Crtp& operator =(const Crtp&)                               = default;
        Crtp(Crtp&&)                                                = default;
        Crtp& operator =(Crtp&&)                                    = default;
    };

    namespace aux {
        struct Object_base;
        struct Static_base;
        class Dynamic_base_base;
        template <typename BaseT, typename P> class Dynamic_base;

        template <typename B, typename T> struct Static_mixin;
        template <typename B, typename T> class Dynamic_mixin;

        template <typename T>
            using Is_object = Void<If<is_derived_from_v<T, Object_base>>>;
        template <typename T>
            using Is_static = Void<If<is_derived_from_v<T, Static_base>>>;
        template <typename T>
            using Is_dynamic = Void<If<is_derived_from_v<T, Dynamic_base_base>>>;

        template <typename T>
            constexpr bool is_object_v = enabled_v<aux::Is_object, T>;
        template <typename T>
            constexpr bool is_static_v = enabled_v<aux::Is_static, T>;
        template <typename T>
            constexpr bool is_dynamic_v = enabled_v<aux::Is_dynamic, T>;

        struct Object_base {
            bool equals(Dummy) const noexcept                { return false; }
        };

        struct Static_base : Object_base {};

        class Dynamic_base_base : public Object_base {
        public:
            bool initialized() const noexcept         { return _initialized; }

            virtual void virtual_init();
            void maybe_virtual_init();

            template <typename T, typename... Args> static T cons_tp(Args&&...);

            virtual String to_string() const&;
        protected:
            virtual void recover_virtual_init()                            { }
            virtual bool valid_pre_init() const noexcept      { return true; }
            virtual void check_pre_init() const                            { }
            virtual void recover_pre_init()                                { }
            virtual void init()                                            { }
            virtual void recover_init()                                    { }
            virtual bool valid_post_init() const noexcept     { return true; }
            virtual void check_post_init() const                           { }
            virtual void recover_post_init()                               { }
        private:
            /// (zero-initialization)
            bool _initialized{};
        };

        template <typename BaseT, typename P>
        class Dynamic_base : public Dynamic_base_base {
        public:
            using Base = BaseT;
            using Ptr = P;

            /// calls `T::cons_tp` (!)
            template <typename T, typename... Args> static Ptr new_tp(Args&&...);

            virtual Ptr to_ptr() const&                                   = 0;
            virtual Ptr to_ptr()&&                                        = 0;

            virtual Ptr shallow_copy_to_ptr() const                       = 0;
            virtual Ptr deep_copy_to_ptr() const                          = 0;
        protected:
            virtual bool equals_base(const Base&) const noexcept          = 0;
            bool equals_this(const Base&) const noexcept      { return true; }
            virtual bool equals_base_anyway(const Base&) const noexcept
                                                             { return false; }
        };

        template <typename B>
        class Parent_mixin : public B {
        public:
            using Parent = B;

            using Parent::Parent;
            Parent_mixin()                                          = default;
            Parent_mixin(const Parent&);
            Parent_mixin(Parent&&) noexcept;

            const Parent& cparent() const noexcept;
            const Parent& parent() const& noexcept       { return cparent(); }
            Parent& parent()& noexcept;
            Parent&& parent()&& noexcept            { return move(parent()); }
        };

        template <typename B>
        struct String_mixin : B {
            using B::B;

            explicit operator String() const     { return this->to_string(); }
        };

        template <typename B, typename T, typename = If<is_class_v<T>>>
        class Object_mixin : public B {
        public:
            using This = T;

            static_assert(!is_same_v<This, B>);

            using B::B;
        protected:
            struct Access;

            /// the non-const variants are not useful inside of `This` class
            const auto& cthis() const noexcept;
            auto& rthis() noexcept;
            auto&& mthis() noexcept;
            auto cthis_l() const noexcept;
            auto this_l() noexcept;
        };

        template <typename B, typename T, typename _>
        struct Object_mixin<B, T, _>::Access : Parent_mixin<This> {
            using This = Parent_mixin<Object_mixin::This>;

            friend Object_mixin;

            friend Dynamic_base_base;
            template <typename, typename> friend class Dynamic_base;
            template <typename, typename> friend struct Static_mixin;
            template <typename, typename> friend class Dynamic_mixin;

            using This::This;
        };

        template <typename B, typename T>
        struct Static_mixin : Object_mixin<Parent_mixin<B>, T> {
            using typename Object_mixin<Parent_mixin<B>, T>::This;
            using typename Object_mixin<Parent_mixin<B>, T>::Parent;

            using Object_mixin<Parent_mixin<B>, T>::Object_mixin;

            String to_string() const&;
        };

        template <typename B, typename T>
        class Dynamic_mixin : public Object_mixin<Parent_mixin<B>, T> {
        public:
            using typename Object_mixin<Parent_mixin<B>, T>::This;
            using typename Object_mixin<Parent_mixin<B>, T>::Parent;
            using typename Object_mixin<Parent_mixin<B>, T>::Base;
            using typename Object_mixin<Parent_mixin<B>, T>::Ptr;

            static constexpr bool is_base_v = is_same_v<This, Base>;

            template <typename P_> static constexpr bool is_ptr_v = is_same_v<Decay<P_>, Ptr>;

            using Object_mixin<Parent_mixin<B>, T>::Object_mixin;

            void virtual_init() override;

            /// calls `virtual_init` after construction (!)
            /// cannot use plain `This` as ret. type
            /// -> must use `Ret_if` instead of `Req`
            template <typename T_ = This, typename... Args>
                static auto cons(Args&&...)
                    -> Ret_if<T_, !is_abstract_v<T_>>;
            template <typename T_ = This, typename U>
                static auto cons(initializer_list<U>)
                    -> Ret_if<T_, is_constructible_v<T_, initializer_list<U>>>;
            template <typename T_ = This>
                static auto cons(const This&)
                    -> Ret_if<T_, is_copy_constructible_v<T_>>;
            template <typename T_ = This>
                static auto cons(This&&)
                    -> Ret_if<T_, is_move_constructible_v<T_>>;

            /// define `make_ptr` in class hierarchy (~ e.g. `make_unique`)
            /// calls `new_tp`
            template <typename T_ = This, Req<!is_abstract_v<T_>> = 0, typename... Args>
                static Ptr new_me(Args&&...);
            template <typename T_ = This, typename U,
                      Req<is_constructible_v<T_, initializer_list<U>>> = 0>
                static Ptr new_me(initializer_list<U>);
            template <typename T_ = This, Req<is_copy_constructible_v<T_>> = 0>
                static Ptr new_me(const This&);
            template <typename T_ = This, Req<is_move_constructible_v<T_>> = 0>
                static Ptr new_me(This&&);

            static bool is_me(const Ptr&) noexcept;
            static void check_is_me(const Ptr&);
            static void check_is_not_me(const Ptr&);
            static String is_me_msg(const Ptr&);
            static String is_not_me_msg(const Ptr&);

            template <typename T_> static Forward_as_ref<This, T_> cast(T_&&) noexcept;
            template <typename P_, typename = If<is_ptr_v<P_>>>
                static Forward_as_ref<This, P_> cast_check(P_&&);
            template <typename P_, typename = If<is_ptr_v<P_>>>
                static Forward_as_ptr<This, P_> dcast_l(P_&&) noexcept;
            template <typename T_> static Forward_as_ref<This, T_> dcast(T_&&);

            Ptr to_ptr() const& override;
            Ptr to_ptr()&& override;

            Ptr shallow_copy_to_ptr() const override;
            Ptr deep_copy_to_ptr() const override;

            using Parent::equals;
            bool equals(const This&) const noexcept;
            template <typename T_ = This, typename U,
                      Req<is_base_of_v<T_, U> || is_base_of_v<U, T_>> = 0>
                bool dynamic_equals(const U&) const noexcept;
        protected:
            friend Dynamic_base_base;
            template <typename, typename> friend class Dynamic_base;
            template <typename, typename> friend class Dynamic_mixin;

            using typename Object_mixin<Parent_mixin<B>, T>::Access;

            void recover_virtual_init() override;
            void lrecover_virtual_init()                                   { }
            bool valid_pre_init() const noexcept override;
            void check_pre_init() const override;
            void recover_pre_init() override;
            bool lvalid_pre_init() const noexcept             { return true; }
            void lcheck_pre_init() const                                   { }
            void lrecover_pre_init()                                       { }
            void init() override;
            void recover_init() override;
            void learly_init()                                             { }
            void linit()                                                   { }
            void lrecover_init()                                           { }
            bool valid_post_init() const noexcept override;
            void check_post_init() const override;
            void recover_post_init() override;
            bool lvalid_post_init() const noexcept            { return true; }
            void lcheck_post_init() const                                  { }
            void lrecover_post_init()                                      { }

            template <typename T_, typename ConvF> static Ptr to_ptr_tp(T_&&, ConvF);

            bool equals_base(const Base&) const noexcept override;
            bool equals_this(const This&) const noexcept;
            bool lequals(const This&) const noexcept          { return true; }
        private:
            template <typename T_ = This>
                using Make_ptr = decltype(declval<T_>().make_ptr(declval<T_>()));

            static void to_ptr_throw(const String& msg);
        };

        template <typename B, typename T, bool is_class = false,
                  bool is_static = false, bool is_dynamic = false>
        struct Inherit_mixin : Parent_mixin<B> {
            using Parent_mixin<B>::Parent_mixin;
        };

        template <typename B, typename T>
        struct Inherit_mixin<B, T, true, false, false> : Object_mixin<Parent_mixin<B>, T> {
            using Object_mixin<Parent_mixin<B>, T>::Object_mixin;
        };

        template <typename B, typename T>
        struct Inherit_mixin<B, T, true, true> : String_mixin<Static_mixin<B, T>> {
            using typename String_mixin<Static_mixin<B, T>>::Parent;

            using String_mixin<Static_mixin<B, T>>::String_mixin;

            String to_string() const&;
        };

        /// no need for `String_mixin` here (virtuals are used)
        template <typename B, typename T>
        struct Inherit_mixin<B, T, true, false, true> : Dynamic_mixin<B, T> {
            static_assert(!Dynamic_mixin<B, T>::is_base_v);

            using Dynamic_mixin<B, T>::Dynamic_mixin;
        };
    }

    using aux::is_object_v;
    using aux::is_static_v;
    using aux::is_dynamic_v;

    template <typename T>
    struct Object : aux::Object_mixin<aux::Object_base, T> {};

    template <typename T>
    struct Static : aux::String_mixin<aux::Static_mixin<aux::Static_base, T>> {};

    template <typename T, typename P>
    struct Dynamic : aux::String_mixin<aux::Dynamic_mixin<aux::Dynamic_base<T, P>, T>> {};

    template <typename B, typename T>
    struct Inherit : aux::Inherit_mixin<B, T, is_class_v<T>, is_static_v<B>, is_dynamic_v<B>> {
        using aux::Inherit_mixin<B, T, is_class_v<T>, is_static_v<B>, is_dynamic_v<B>>::
            Inherit_mixin;
    };
}

#include "unsot/object.inl"
