#pragma once

#include "unsot/string.hpp"

namespace unsot {
    template <typename T>
    const auto& Crtp<T>::cthat() const noexcept
    {
        return static_cast<const That&>(*this);
    }

    template <typename T>
    auto& Crtp<T>::that() noexcept
    {
        return static_cast<That&>(*this);
    }

    template <typename T>
    auto Crtp<T>::cthat_l() const noexcept
    {
        return static_cast<const That*>(this);
    }

    template <typename T>
    auto Crtp<T>::that_l() noexcept
    {
        return static_cast<That*>(this);
    }

    ////////////////////////////////////////////////////////////////

    namespace aux {
        template <typename T, typename... Args>
        T Dynamic_base_base::cons_tp(Args&&... args)
        {
            /// To allow protected constructors too
            typename T::Dynamic_mixin::Access t(FORWARD(args)...);
            t.virtual_init();
            return static_cast<T&&>(t);
        }

    ////////////////////////////////////////////////////////////////

        template <typename BaseT, typename P>
        template <typename T, typename... Args>
        typename Dynamic_base<BaseT, P>::Ptr
        Dynamic_base<BaseT, P>::new_tp(Args&&... args)
        {
            return T::make_ptr(T::template cons_tp<T>(FORWARD(args)...));
        }

    ////////////////////////////////////////////////////////////////

        template <typename B>
        Parent_mixin<B>::Parent_mixin(const Parent& rhs)
            : Parent(rhs)
        { }

        template <typename B>
        Parent_mixin<B>::Parent_mixin(Parent&& rhs) noexcept
            : Parent(move(rhs))
        { }

        template <typename B>
        const typename Parent_mixin<B>::Parent&
        Parent_mixin<B>::cparent() const noexcept
        {
            return static_cast<const Parent&>(*this);
        }

        template <typename B>
        typename Parent_mixin<B>::Parent& Parent_mixin<B>::parent()& noexcept
        {
            return static_cast<Parent&>(*this);
        }

    ////////////////////////////////////////////////////////////////

        template <typename B, typename T, typename _>
        const auto& Object_mixin<B, T, _>::cthis() const noexcept
        {
            return static_cast<const This&>(*this);
        }

        template <typename B, typename T, typename _>
        auto& Object_mixin<B, T, _>::rthis() noexcept
        {
            return static_cast<This&>(*this);
        }

        template <typename B, typename T, typename _>
        auto&& Object_mixin<B, T, _>::mthis() noexcept
        {
            return static_cast<This&&>(move(*this));
        }

        template <typename B, typename T, typename _>
        auto Object_mixin<B, T, _>::cthis_l() const noexcept
        {
            return static_cast<const This*>(this);
        }

        template <typename B, typename T, typename _>
        auto Object_mixin<B, T, _>::this_l() noexcept
        {
            return static_cast<This*>(this);
        }

    ////////////////////////////////////////////////////////////////

        template <typename B, typename T>
        String Static_mixin<B, T>::to_string() const&
        {
            return this->cthis().to_string();
        }

    ////////////////////////////////////////////////////////////////

        template <typename B, typename T>
        void Dynamic_mixin<B, T>::virtual_init()
        try {
            Parent::virtual_init();
        }
        catch (Error& err) {
            if constexpr (is_abstract_v<This>) throw;
            else throw Item_error<This>(this->mthis(), move(err));
        }

        template <typename B, typename T>
        void Dynamic_mixin<B, T>::recover_virtual_init()
        {
            Parent::recover_virtual_init();
            call(this->rthis(), &Access::lrecover_virtual_init);
        }

        template <typename B, typename T>
        bool Dynamic_mixin<B, T>::valid_pre_init() const noexcept
        {
            return Parent::valid_pre_init()
                && call(this->cthis(), &Access::lvalid_pre_init);
        }

        template <typename B, typename T>
        void Dynamic_mixin<B, T>::check_pre_init() const
        {
            Parent::check_pre_init();
            call(this->cthis(), &Access::lcheck_pre_init);
        }

        template <typename B, typename T>
        void Dynamic_mixin<B, T>::recover_pre_init()
        {
            Parent::recover_pre_init();
            call(this->rthis(), &Access::lrecover_pre_init);
        }

        template <typename B, typename T>
        void Dynamic_mixin<B, T>::init()
        {
            call(this->rthis(), &Access::learly_init);
            Parent::init();
            call(this->rthis(), &Access::linit);
        }

        template <typename B, typename T>
        void Dynamic_mixin<B, T>::recover_init()
        {
            Parent::recover_init();
            call(this->rthis(), &Access::lrecover_init);
        }

        template <typename B, typename T>
        bool Dynamic_mixin<B, T>::valid_post_init() const noexcept
        {
            return Parent::valid_post_init()
                && call(this->cthis(), &Access::lvalid_post_init);
        }

        template <typename B, typename T>
        void Dynamic_mixin<B, T>::check_post_init() const
        {
            Parent::check_post_init();
            call(this->cthis(), &Access::lcheck_post_init);
        }

        template <typename B, typename T>
        void Dynamic_mixin<B, T>::recover_post_init()
        {
            Parent::recover_post_init();
            call(this->rthis(), &Access::lrecover_post_init);
        }

        template <typename B, typename T>
        template <typename T_, typename... Args>
        auto Dynamic_mixin<B, T>::cons(Args&&... args)
            -> Ret_if<T_, !is_abstract_v<T_>>
        {
            return T_::template cons_tp<T_>(FORWARD(args)...);
        }

        template <typename B, typename T>
        template <typename T_, typename U>
        auto Dynamic_mixin<B, T>::cons(initializer_list<U> ils)
            -> Ret_if<T_, is_constructible_v<T_, initializer_list<U>>>
        {
            return T_::template cons_tp<T_>(move(ils));
        }

        template <typename B, typename T>
        template <typename T_>
        auto Dynamic_mixin<B, T>::cons(const This& rhs)
            -> Ret_if<T_, is_copy_constructible_v<T_>>
        {
            T_ t(rhs);
            t.maybe_virtual_init();
            return t;
        }

        template <typename B, typename T>
        template <typename T_>
        auto Dynamic_mixin<B, T>::cons(This&& rhs)
            -> Ret_if<T_, is_move_constructible_v<T_>>
        {
            T_ t(move(rhs));
            t.maybe_virtual_init();
            return t;
        }

        template <typename B, typename T>
        template <typename T_, Req<!is_abstract_v<T_>>, typename... Args>
        typename Dynamic_mixin<B, T>::Ptr
        Dynamic_mixin<B, T>::new_me(Args&&... args)
        {
            return T_::template new_tp<T_>(FORWARD(args)...);
        }

        template <typename B, typename T>
        template <typename T_, typename U,
                  Req<is_constructible_v<T_, initializer_list<U>>>>
        typename Dynamic_mixin<B, T>::Ptr
        Dynamic_mixin<B, T>::new_me(initializer_list<U> ils)
        {
            return T_::template new_tp<T_>(move(ils));
        }

        template <typename B, typename T>
        template <typename T_, Req<is_copy_constructible_v<T_>>>
        typename Dynamic_mixin<B, T>::Ptr
        Dynamic_mixin<B, T>::new_me(const This& rhs)
        {
            return T_::make_ptr(T_::cons(rhs));
        }

        template <typename B, typename T>
        template <typename T_, Req<is_move_constructible_v<T_>>>
        typename Dynamic_mixin<B, T>::Ptr
        Dynamic_mixin<B, T>::new_me(This&& rhs)
        {
            return T_::make_ptr(T_::cons(move(rhs)));
        }

        template <typename B, typename T>
        bool Dynamic_mixin<B, T>::is_me(const Ptr& ptr) noexcept
        {
            assert(ptr);
            return This::dcast_l(ptr);
        }

        template <typename B, typename T>
        void Dynamic_mixin<B, T>::check_is_me(const Ptr& ptr)
        {
            expect(This::is_me(ptr), is_not_me_msg(ptr));
        }

        template <typename B, typename T>
        void Dynamic_mixin<B, T>::check_is_not_me(const Ptr& ptr)
        {
            expect(!This::is_me(ptr), is_me_msg(ptr));
        }

        template <typename B, typename T>
        String Dynamic_mixin<B, T>::is_me_msg(const Ptr& ptr)
        {
            using unsot::to_string;
            return "Unexpected type "s + typeid(This).name() + ": " + to_string(*ptr);
        }

        template <typename B, typename T>
        String Dynamic_mixin<B, T>::is_not_me_msg(const Ptr& ptr)
        {
            using unsot::to_string;
            return "Expected type:\n"s + typeid(This).name()
                 + "\nGot:\n" + typeid(*ptr).name()
                 + "\nObject: " + to_string(*ptr);
        }

        template <typename B, typename T>
        template <typename T_>
        Forward_as_ref<typename Dynamic_mixin<B, T>::This, T_>
        Dynamic_mixin<B, T>::cast(T_&& t) noexcept
        {
            if constexpr (!is_ptr_v<T_>) return Forward_as_ref<This, T_>(t);
            else {
                auto&& ptr = FORWARD(t);
                if constexpr (_debug_) {
                    if (!This::is_me(ptr)) CDBGLN(This::is_not_me_msg(ptr));
                    assert(This::is_me(ptr));
                }
                return Forward_as_ref<This, T_>(*ptr);
            }
        }

        template <typename B, typename T>
        template <typename P_, typename>
        Forward_as_ref<typename Dynamic_mixin<B, T>::This, P_>
        Dynamic_mixin<B, T>::cast_check(P_&& ptr)
        {
            This::check_is_me(ptr);
            return This::cast(FORWARD(ptr));
        }

        template <typename B, typename T>
        template <typename P_, typename>
        Forward_as_ptr<typename Dynamic_mixin<B, T>::This, P_>
        Dynamic_mixin<B, T>::dcast_l(P_&& ptr) noexcept
        {
            return dynamic_cast<Forward_as_ptr<This, P_>>(get(FORWARD(ptr)));
        }

        template <typename B, typename T>
        template <typename T_>
        Forward_as_ref<typename Dynamic_mixin<B, T>::This, T_>
        Dynamic_mixin<B, T>::dcast(T_&& t)
        {
            if constexpr (!is_ptr_v<T_>) return dynamic_cast<Forward_as_ref<This, T_>>(t);
            else return dynamic_cast<Forward_as_ref<This, T_>>(*t);
        }

        template <typename B, typename T>
        typename Dynamic_mixin<B, T>::Ptr Dynamic_mixin<B, T>::to_ptr() const&
        {
            return to_ptr_tp(this->cthis(), [](auto& t) -> auto& { return t; });
        }

        template <typename B, typename T>
        typename Dynamic_mixin<B, T>::Ptr Dynamic_mixin<B, T>::to_ptr()&&
        {
            return to_ptr_tp(this->mthis(), [](auto&& t) -> auto&& { return move(t); });
        }

        template <typename B, typename T>
        template <typename T_, typename ConvF>
        typename Dynamic_mixin<B, T>::Ptr Dynamic_mixin<B, T>::to_ptr_tp(T_&& t, ConvF f)
        {
            static constexpr bool copy = is_cref_v<T_>;
            static constexpr bool constructible = copy ? is_copy_constructible_v<This>
                                                       : is_move_constructible_v<This>;
            if constexpr (enabled_v<Make_ptr, This> && constructible)
                return This::make_ptr(f(FORWARD(t)));
            else if constexpr (!constructible)
                to_ptr_throw("not "s + (copy ? "copyable" : "movable"));
            else to_ptr_throw("make_ptr not implemented");
            THROW(error);
        }

        template <typename B, typename T>
        void Dynamic_mixin<B, T>::to_ptr_throw(const String& msg)
        {
            THROW(typeid(This).name()) + "::to_ptr: " + msg + "!";
        }

        template <typename B, typename T>
        typename Dynamic_mixin<B, T>::Ptr Dynamic_mixin<B, T>::shallow_copy_to_ptr() const
        {
            if constexpr (enabled_member_shallow_copy_v<This>)
                return to_ptr_tp(this->cthis(), [](auto& t){ return t.shallow_copy(); });
            else to_ptr_throw("shallow_copy not implemented");
            THROW(error);
        }

        template <typename B, typename T>
        typename Dynamic_mixin<B, T>::Ptr Dynamic_mixin<B, T>::deep_copy_to_ptr() const
        {
            using unsot::deep_copy;
            return to_ptr_tp(this->cthis(), [](auto& t){ return deep_copy(t); });
        }

        template <typename B, typename T>
        bool Dynamic_mixin<B, T>::equals(const This& rhs) const noexcept
        {
            if constexpr (is_base_v) return equals_base(rhs);
            else return equals_this(rhs);
        }

        template <typename B, typename T>
        bool Dynamic_mixin<B, T>::equals_base(const Base& rhs) const noexcept
        {
            if constexpr (is_base_v) return equals_this(rhs);
            else return dynamic_equals(rhs);
        }

        template <typename B, typename T>
        bool Dynamic_mixin<B, T>::equals_this(const This& rhs) const noexcept
        {
            return Parent::equals_this(rhs)
                && call(this->cthis(), &Access::lequals, rhs);
        }

        template <typename B, typename T>
        template <typename T_, typename U, Req<is_base_of_v<T_, U> || is_base_of_v<U, T_>>>
        bool Dynamic_mixin<B, T>::dynamic_equals(const U& rhs) const noexcept
        {
            static_assert(is_same_v<T_, This>);
            /// `equals` is not virtual, need to call it precisely on `This` !
            if constexpr (is_same_v<This, U>) return this->cthis().equals(rhs);
            if constexpr (is_base_of_v<This, U>) return rhs.dynamic_equals(this->cthis());
            if (auto derived_l = dynamic_cast<const This*>(&rhs))
                return this->cthis().equals(*derived_l);

            return this->equals_base_anyway(rhs) || rhs.equals_base_anyway(this->cthis());
        }

    ////////////////////////////////////////////////////////////////

        template <typename B, typename T>
        String Inherit_mixin<B, T, true, true>::to_string() const&
        {
            if constexpr (enabled_member_to_string_v<Parent>)
                return Parent::to_string();
            else static_assert(false_tp<Parent>, "Attempt to call not implemented `to_string`");
        }
    }
}
