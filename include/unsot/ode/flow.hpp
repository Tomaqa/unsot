#pragma once

#include "ode.hpp"

#include "util/hash.hpp"

namespace unsot::ode::flow {
    namespace aux {
        class Flow;
    }

    class Keys;
    class State;

    struct Config;

    struct Variant;

    class Parse;

    using Ptr = Shared_ptr<Flow>;
    using Ptrs = Vector<Ptr>;

    using States = Vector<State>;

    using All_formulas = Vector<Formulas>;

    using All_formulas_map = Hash<Key, Formulas>;

    using Id = Idx;
    using Ids = Vector<Id>;

    using Variants = Vector<Variant>;

    static constexpr Id def_id = 0;
    static constexpr Id first_id = 1;

    inline const Key t_key = "_t";
    inline const Key tau_key = "_tau";
    constexpr size_t special_args_size = 2;

    inline const Key global_key = "";

    inline bool is_special_key(const Key&) noexcept;
    void check_is_special_key(const Key&);
    inline bool is_global_key(const Key&) noexcept;
    void check_is_global_key(const Key&);

    inline Idx to_idx(const Id&) noexcept;
}

namespace unsot::ode {
    inline flow::Ptr new_flow(Flow&&);
}

namespace unsot::ode::flow::aux {
    /// A flow is a set of odes that share the same signature
    /// and are evaluated synchronously (-> they are all 'unified')
    /// All variants are abstract here, determined only directly by an id,
    /// but not by concrete condition
    /// Construction from particular `Arg_keys` for each ode
    /// is not supported yet (thus, no unification is needed so far)
    class Flow : public Static<Flow> {
    public:
        template <typename FunT> class Functions;
        template <typename B> class Local_mixin;
        class Odes;
        class Invariants;
        class Local_invariants;
        class Timer;

        template <typename FunT>
            using Local_functions = Local_mixin<Functions<FunT>>;

        using All_odes = Vector<Odes>;
        using All_local_invariants = Vector<Local_invariants>;

        Flow()                                                      = default;
        ~Flow()                                                     = default;
        Flow(const Flow&)                                           = default;
        Flow& operator =(const Flow&)                               = default;
        Flow(Flow&&)                                                = default;
        Flow& operator =(Flow&&)                                    = default;
        Flow(Keys, All_formulas all_odes_phis, All_formulas_map all_invs_phis_map);
        Flow(Keys, Formulas odes_phis, All_formulas_map all_invs_phis_map);
        Flow(Keys, All_formulas all_odes_phis, Formulas invs_phis);
        Flow(Keys, Formulas odes_phis, Formulas invs_phis);

        static Flow parse(List);

        /// Dynamic addition of odes is not supported yet

        const Keys& ckeys() const noexcept            { return *ckeys_ptr(); }
        const Keys& keys() const& noexcept                 { return ckeys(); }
        Keys&& keys()&& noexcept                      { return move(keys()); }

        const auto& call_odes() const noexcept           { return _all_odes; }
        const auto& all_odes() const& noexcept         { return call_odes(); }
        auto&& all_odes()&& noexcept              { return move(all_odes()); }

        const Invariants& cginvs() const noexcept    { return *cginvs_ptr(); }
        const Invariants& ginvs() const& noexcept         { return cginvs(); }
        Invariants&& ginvs()&& noexcept              { return move(ginvs()); }

        const auto& call_linvs() const noexcept         { return _all_linvs; }
        const auto& all_linvs() const& noexcept       { return call_linvs(); }
        auto&& all_linvs()&& noexcept            { return move(all_linvs()); }

        const Timer& ctimer() const noexcept         { return *ctimer_ptr(); }
        const Timer& timer() const& noexcept              { return ctimer(); }
        Timer&& timer()&& noexcept                   { return move(timer()); }

        inline size_t size() const noexcept;

        void check_input(const State&, const Config&) const;

        void eval_step(const Reals& in, Reals& out, const Config&) const;
        bool timer(const Reals& state, const Config&) const;
        bool local_invariants(const Reals& state, const Config&) const;
        bool global_invariant(const Reals& state, const Config&) const;
        bool invariant(const Reals& state, const Config&) const;

        String to_string() const&;

        bool equals(const This&) const noexcept;
    protected:
        using Keys_ptr = Shared_ptr<Keys>;
        using Keys_link = const Keys*;

        using Invariants_ptr = Shared_ptr<Invariants>;

        using Timer_ptr = Shared_ptr<Timer>;

        Flow(Keys);

        inline static Keys_ptr new_keys(Keys&&);
        inline static Invariants_ptr new_invs(Invariants&&);
        inline static Timer_ptr new_timer(Timer&&);

        void set_all_odes(All_formulas&&);
        void set_all_invs(All_formulas_map&&);

        void set_ginvs(All_formulas_map&);
        void set_timer(All_formulas_map&);
        void set_ginvs(Formulas&&);
        void set_timer(Formulas&& tau_phis, Formulas&& t_phis);
        void set_keys(Keys&&);
        void set_ginvs(Invariants&&);
        void set_timer(Timer&&);

        /// Use one of them, not both of them
        void init_all_linvs();
        void set_all_linvs(All_formulas_map&);

        const Keys_ptr& ckeys_ptr() const noexcept       { return _keys_ptr; }
        Keys_ptr& keys_ptr() noexcept                    { return _keys_ptr; }
        Keys& keys()& noexcept                         { return *keys_ptr(); }

        All_odes& all_odes()& noexcept                   { return _all_odes; }

        const Invariants_ptr& cginvs_ptr() const noexcept
                                                        { return _ginvs_ptr; }
        Invariants_ptr& ginvs_ptr() noexcept            { return _ginvs_ptr; }
        Invariants& ginvs()& noexcept                 { return *ginvs_ptr(); }

        All_local_invariants& all_linvs()& noexcept     { return _all_linvs; }

        const Timer_ptr& ctimer_ptr() const noexcept    { return _timer_ptr; }
        Timer_ptr& timer_ptr() noexcept                 { return _timer_ptr; }
        Timer& timer()& noexcept                      { return *timer_ptr(); }

        bool valid_state(const State&) const noexcept;
        void check_state(const State&) const;

        bool valid_config(const Config&) const noexcept;
        void check_config(const Config&) const;
        template <typename AllFuns>
            bool valid_ids(const AllFuns&, const Ids&) const noexcept;
        template <typename AllFuns>
            void check_ids(const AllFuns&, const Ids&) const;
    private:
        void init(All_formulas all_odes_phis, All_formulas_map all_invs_phis_map);

        void init();
        bool valid() const noexcept;
        void check() const;
        bool valid_keys() const noexcept;
        void check_keys() const;
        template <typename AllFuns>
            bool valid_keys_tp(const AllFuns&) const noexcept;
        template <typename AllFuns> void check_keys_tp(const AllFuns&) const;

        Keys_ptr _keys_ptr{};

        All_odes _all_odes{};
        Invariants_ptr _ginvs_ptr{};
        All_local_invariants _all_linvs{};
        Timer_ptr _timer_ptr{};
    };
}

namespace unsot::ode {
    class Flow : public Inherit<flow::aux::Flow, Flow> {
    public:
        using Inherit::Inherit;
    };
}

namespace unsot::ode::flow {
    struct Config : Static<Config> {
        ~Config()                                                   = default;
        Config(const Config&)                                       = default;
        Config& operator =(const Config&)                           = default;
        Config(Config&&)                                            = default;
        Config& operator =(Config&&)                                = default;
        inline Config(Ids odes_ids_ = {},
                      Id tau_id_ = def_id, Id t_id_ = def_id,
                      Ids linvs_ids_ = {}, Id ginv_id_ = def_id);
        Config(Ids odes_ids_, Ids timer_ids, Ids linvs_ids_, Id ginv_id_);
        Config(Ids odes_ids_, Ids timer_ids, Ids invs_ids);
        Config(Ids odes_ids_, Ids invs_ids);

        void set_timer_ids(Ids& invs_ids) noexcept;
        void set_linvs_ids_and_ginv_id(Ids&& invs_ids);

        static Config parse(List);

        String to_string() const&;

        bool equals(const This&) const noexcept;

        Ids odes_ids{};
        Id tau_id{};
        Id t_id{};
        Ids linvs_ids{};
        Id ginv_id{};
    };

    struct Variant {
        Key key;
        Id id;
    };
}

#include "ode/flow/keys.hpp"
#include "ode/flow/state.hpp"
#include "ode/flow/functions.hpp"
#include "ode/flow/odes.hpp"
#include "ode/flow/invariants.hpp"
#include "ode/flow/timer.hpp"

#include "ode/flow.inl"
