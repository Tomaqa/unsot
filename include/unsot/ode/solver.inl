#pragma once

namespace unsot::ode::solver {
    Base::Traject_ptr Base::new_traject(Traject&& traject_)
    {
        return MAKE_SHARED(move(traject_));
    }

    const auto& Base::cflow_ptr_by_idx(Idx idx) const noexcept
    {
        return cflow_ptrs()[idx];
    }

    auto& Base::flow_ptr_by_idx(Idx idx) noexcept
    {
        return flow_ptrs()[idx];
    }

    const auto& Base::cflow_ptr(const flow::Id& fid) const noexcept
    {
        return cflow_ptr_by_idx(flow::to_idx(fid));
    }

    auto& Base::flow_ptr(const flow::Id& fid) noexcept
    {
        return flow_ptr_by_idx(flow::to_idx(fid));
    }

    const Flow& Base::cflow(const flow::Id& fid) const noexcept
    {
        return *cflow_ptr(fid);
    }

    Flow& Base::flow(const flow::Id& fid)& noexcept
    {
        return *flow_ptr(fid);
    }

    const Flow& Base::cflow_by_idx(Idx idx) const noexcept
    {
        return *cflow_ptr_by_idx(idx);
    }

    Flow& Base::flow_by_idx(Idx idx)& noexcept
    {
        return *flow_ptr_by_idx(idx);
    }

    const auto& Base::ctraject_ptr_by_idx(Idx idx) const noexcept
    {
        return ctraject_ptrs()[idx];
    }

    auto& Base::traject_ptr_by_idx(Idx idx) const noexcept
    {
        return traject_ptrs()[idx];
    }

    const auto& Base::ctraject_ptr(const flow::Id& fid) const noexcept
    {
        return ctraject_ptr_by_idx(flow::to_idx(fid));
    }

    auto& Base::traject_ptr(const flow::Id& fid) const noexcept
    {
        return traject_ptr_by_idx(flow::to_idx(fid));
    }

    const auto& Base::ctraject(const flow::Id& fid) const noexcept
    {
        return *ctraject_ptr(fid);
    }

    Base::Traject& Base::traject(const flow::Id& fid) const& noexcept
    {
        return *traject_ptr(fid);
    }

    const auto& Base::ctraject_by_idx(Idx idx) const noexcept
    {
        return *ctraject_ptr_by_idx(idx);
    }

    Base::Traject& Base::traject_by_idx(Idx idx) const& noexcept
    {
        return *traject_ptr_by_idx(idx);
    }
}
