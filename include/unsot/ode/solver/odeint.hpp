#pragma once

#include "ode/solver.hpp"

#include <boost/numeric/odeint/stepper/runge_kutta4.hpp>

namespace unsot::ode::solver {
    namespace odeint = boost::numeric::odeint;

    class Odeint : public Inherit<Base> {
    public:
        using Inherit::Inherit;
        virtual ~Odeint()                                           = default;
        explicit Odeint(Flow);
        explicit Odeint(Flows);
        Odeint(initializer_list<Flow>);

        flow::Id add_flow(Flow) override;

        void do_step(const flow::Id&, const State& in, State& out,
                     const Real dt_, const Config&) const override;
    protected:
        using Stepper = odeint::runge_kutta4<Reals::Parent>;
        using Steppers = Vector<Stepper>;

        auto& steppers() const noexcept                  { return _steppers; }
        inline auto& stepper(const flow::Id&) const noexcept;
    private:
        mutable Steppers _steppers{};
    };
}

#include "ode/solver/odeint.inl"
