#pragma once

#include "util/string.hpp"

namespace unsot::ode::solver {
    template <typename S>
    Path Base::Run<S>::plot_path(const flow::Id& fid) const
    {
        Path path = _plot_path;
        if (csolver().size() > 1) path += plot_path_suffix(fid);
        return path;
    }

    template <typename S>
    Path Base::Run<S>::plot_path_suffix(const flow::Id& fid) const
    {
        return "_" + to_string(fid);
    }

    template <typename S>
    const Ostream_ptr& Base::Run<S>::cplot_os_ptr(const flow::Id& fid) const
    {
        return _plot_os_ptrs[flow::to_idx(fid)];
    }

    template <typename S>
    Ostream_ptr& Base::Run<S>::plot_os_ptr(const flow::Id& fid)
    {
        return _plot_os_ptrs[flow::to_idx(fid)];
    }

    template <typename S>
    Ostream& Base::Run<S>::plot_os(const flow::Id& fid) const
    {
        return *cplot_os_ptr(fid);
    }

    template <typename S>
    ofstream& Base::Run<S>::plot_ofs(const flow::Id& fid)
    {
        return _plot_ofss[flow::to_idx(fid)];
    }

    template <typename S>
    String Base::Run<S>::usage() const
    {
        return Inherit::usage() + lusage();
    }

    template <typename S>
    String Base::Run<S>::lusage() const
    {
        return usage_row('t', "Sets file to plot trajectories into")
             + usage_row('T', "Sets different gnuplot script file")
             + usage_row('s', "Set stepping size of the ODE solver");
    }

    template <typename S>
    void Base::Run<S>::init()
    {
        Inherit::init();
        linit();
    }

    template <typename S>
    void Base::Run<S>::do_stuff()
    {
        solver().add_def_flow();
        set_trajects_ostreams();

        String line;
        getline(is(), line);
        solver().flow() = Flow::parse(move(line));
        //+ print these only when in interactive mode
        os() << solver().cflow() << endl;

        while (getline(is(), line)) {
            if (std::empty(line)) continue;
            State state;
            //+ do not catch in non-interactive mode
            try {
                state = State::parse(move(line));
            }
            catch (const Error& err) {
                cerr << err << endl;
                continue;
            }
            os() << state << endl;

            for (int k = 0; getline(is(), line);) {
                if (line.empty()) {
                    if (k == 0) continue;
                    break;
                }

                try {
                    Config config = Config::parse(move(line));
                    solver().solve(state, move(config));
                }
                catch (const Error& err) {
                    cerr << err << endl;
                    continue;
                }

                os() << "-> " << state << endl;
                solver().traject().write_block(k++);
                state.reset_tau();
            }

            //+ gen. plot, allow multiple files
        }
    }

    template <typename S>
    void Base::Run<S>::finish()
    {
        Inherit::finish();
        lfinish();
    }

    template <typename S>
    void Base::Run<S>::lfinish()
    {
        plot_trajects();
    }

    template <typename S>
    String Base::Run<S>::getopt_str() const noexcept
    {
        return Inherit::getopt_str() + lgetopt_str();
    }

    template <typename S>
    String Base::Run<S>::lgetopt_str() const noexcept
    {
        return "t:T:s:";
    }

    template <typename S>
    bool Base::Run<S>::process_opt(char c)
    {
        if (Inherit::process_opt(c)) return true;
        return lprocess_opt(c);
    }

    template <typename S>
    bool Base::Run<S>::lprocess_opt(char c)
    {
        switch (c) {
        case 't':
            _plot_path = optarg;
            _plot = true;
            return true;
        case 'T':
            _plot_script_path = optarg;
            return true;
        case 's':
            solver().dt() = to_value_check<Real>(optarg);
            return true;
        }

        return false;
    }

    template <typename S>
    void Base::Run<S>::set_trajects_ostreams()
    {
        if (!_plot) return;
        set_plot_ostream_ptrs();
        for (auto& fid : csolver().cflow_ids()) {
            solver().traject_ptr(fid)->set_ostream(plot_os(fid));
        }
    }

    template <typename S>
    void Base::Run<S>::set_plot_ostream_ptrs()
    {
        /// The size must correspond to total number of flows
        const size_t size_ = csolver().size();
        _plot_os_ptrs.resize(size_);
        _plot_ofss.resize(size_);
        for (auto& fid : csolver().cflow_ids()) {
            set_plot_ostream_ptr(fid);
        }
    }

    template <typename S>
    void Base::Run<S>::set_plot_ostream_ptr(const flow::Id& fid)
    {
        const Path path = plot_path(fid);
        set_stream_ptr(plot_os_ptr(fid), plot_ofs(fid), path,
                       "Output plot ("s + to_string(path) + ") stream error.");
    }

    template <typename S>
    void Base::Run<S>::plot_trajects()
    {
        if (!_plot) return;
        cout << endl;
        //+ also offer single multiplot or else
        for (auto& fid : csolver().cflow_ids()) {
            plot_traject(fid);
        }
    }

    template <typename S>
    void Base::Run<S>::plot_traject(const flow::Id& fid)
    {
        static const Path script_path =
            project_dirname()/"tools/plot/traject.sh";
        const String cmd = to_string(script_path) + " "
                         + to_string(plot_path(fid)) + " '' "
                         + to_string(_plot_script_path);
        expect(system(cmd.c_str()) == 0, "The plot system call failed.");
    }
}
