#pragma once

namespace unsot::ode::solver {
    auto& Odeint::stepper(const flow::Id& fid) const noexcept
    {
        return steppers()[flow::to_idx(fid)];
    }
}
