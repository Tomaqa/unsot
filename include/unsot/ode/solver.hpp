#pragma once

#include "ode/flow.hpp"

namespace unsot::ode::solver {
    class Base;

    using State = flow::State;
    using States = flow::States;
    using Config = flow::Config;
}

namespace unsot::ode::solver {
    /// Provides whole solution of particular flows
    /// Only single flow is supported so far
    /// (No unification of flows is supported yet)
    class Base : public Object<Base> {
    public:
        template <typename S> class Run;

        class Traject;

        using Traject_ptr = Shared_ptr<Traject>;
        using Traject_ptrs = Vector<Traject_ptr>;

        static constexpr Real def_dt = 5e-3;

        Base()                                                      = default;
        virtual ~Base()                                             = default;
        Base(const Base&)                                           = default;
        Base& operator =(const Base&)                               = default;
        Base(Base&&)                                                = default;
        Base& operator =(Base&&)                                    = default;
        explicit Base(Flow);
        explicit Base(Flows);
        Base(initializer_list<Flow>);

        const auto& cflow_ids() const noexcept           { return _flow_ids; }
        const auto& flow_ids() const& noexcept         { return cflow_ids(); }
        auto&& flow_ids()&& noexcept              { return move(flow_ids()); }
        const auto& cflow_id(Idx idx) const noexcept
                                                  { return cflow_ids()[idx]; }

        const auto& cflow_ptrs() const noexcept         { return _flow_ptrs; }
        const auto& flow_ptrs() const& noexcept       { return cflow_ptrs(); }
        auto&& flow_ptrs()&& noexcept            { return move(flow_ptrs()); }
        inline const auto&
            cflow_ptr(const flow::Id& = flow::def_id) const noexcept;
        inline const Flow&
            cflow(const flow::Id& = flow::def_id) const noexcept;
        const auto& flow(const flow::Id& fid = flow::def_id) const& noexcept
                                                        { return cflow(fid); }
        auto&& flow(const flow::Id& fid = flow::def_id)&& noexcept
                                                   { return move(flow(fid)); }
        inline const auto& cflow_ptr_by_idx(Idx) const noexcept;
        inline const Flow& cflow_by_idx(Idx) const noexcept;
        const auto& flow_by_idx(Idx idx) const& noexcept
                                                 { return cflow_by_idx(idx); }
        auto&& flow_by_idx(Idx idx)&& noexcept
                                            { return move(flow_by_idx(idx)); }

        inline const auto&
            ctraject_ptr(const flow::Id& = flow::def_id) const noexcept;
        inline const auto&
            ctraject(const flow::Id& = flow::def_id) const noexcept;
        auto&& traject(const flow::Id& fid = flow::def_id)&& noexcept
                                                { return move(traject(fid)); }
        inline const auto& ctraject_ptr_by_idx(Idx) const noexcept;
        inline const auto& ctraject_by_idx(Idx) const noexcept;
        auto&& traject_by_idx(Idx idx)&& noexcept
                                         { return move(traject_by_idx(idx)); }

        Real cdt() const noexcept                              { return _dt; }
        Real dt() const& noexcept                            { return cdt(); }
        Real& dt()& noexcept                                   { return _dt; }

        bool kept_invariant() const noexcept       { return _kept_invariant; }
        void keep_invariant() noexcept             { _kept_invariant = true; }
        void unkeep_invariant() noexcept          { _kept_invariant = false; }

        size_t size() const noexcept       { return std::size(cflow_ptrs()); }
        bool empty() const noexcept       { return std::empty(cflow_ptrs()); }

        void reserve(size_t);

        virtual flow::Id add_flow(Flow);
        flow::Id add_def_flow();

        [[nodiscard]] State solve(const flow::Id&, const State& in,
                                  const Config&) const;
        virtual void solve(const flow::Id&, State&, const Config&) const;
        [[nodiscard]] virtual State solve(const flow::Id&, List) const;
        [[nodiscard]] State solve(const State& in, const Config& config) const
                                   { return solve(flow::def_id, in, config); }
        void solve(State& state, const Config& config) const
                                       { solve(flow::def_id, state, config); }
        [[nodiscard]] State solve(List ls) const
                                     { return solve(flow::def_id, move(ls)); }

        virtual void check_input(const flow::Id&, const State&,
                                 const Config&) const;
        void check_input(const State& state, const Config& config) const
                                 { check_input(flow::def_id, state, config); }

        virtual bool invariant(const flow::Id&, const State&,
                               const Config&) const;
        bool invariant_check(const flow::Id&, const State&,
                             const Config&) const;
        bool is_end(const flow::Id& fid, const State& state,
                    const Config& config) const
                              { return !invariant(move(fid), state, config); }
        bool is_end_check(const flow::Id& fid, const State& state,
                          const Config& config) const
                        { return !invariant_check(move(fid), state, config); }
        bool invariant(const State& state, const Config& config) const
                            { return invariant(flow::def_id, state, config); }
        bool invariant_check(const State& state,
                             const Config& config) const
                      { return invariant_check(flow::def_id, state, config); }
        bool is_end(const State& state, const Config& config) const
                               { return is_end(flow::def_id, state, config); }
        bool is_end_check(const State& state, const Config& config) const
                         { return is_end_check(flow::def_id, state, config); }

        virtual void do_step(const flow::Id&, const State& in, State& out,
                             const Real dt_, const Config&) const         = 0;
        void do_step_check(const flow::Id&, const State& in, State& out,
                           const Real dt_, const Config&) const;
        void do_step(const State& in, State& out, const Real dt_,
                     const Config& config) const
                              { do_step(flow::def_id, in, out, dt_, config); }
        void do_step_check(const State& in, State& out, const Real dt_,
                           const Config& config) const
                        { do_step_check(flow::def_id, in, out, dt_, config); }

        virtual bool is_almost_end(const flow::Id&, const State&,
                                   const Config&) const;
        bool is_almost_end(const State& state, const Config& config) const
                        { return is_almost_end(flow::def_id, state, config); }
    protected:
        inline static Traject_ptr new_traject(Traject&&);

        flow::Ids& flow_ids()& noexcept                  { return _flow_ids; }
        flow::Ptrs& flow_ptrs()& noexcept               { return _flow_ptrs; }
        inline auto& flow_ptr(const flow::Id& = flow::def_id) noexcept;
        inline Flow& flow(const flow::Id& = flow::def_id)& noexcept;
        inline auto& flow_ptr_by_idx(Idx) noexcept;
        inline Flow& flow_by_idx(Idx)& noexcept;

        const auto& ctraject_ptrs() const noexcept   { return _traject_ptrs; }
        auto& traject_ptrs() const noexcept          { return _traject_ptrs; }
        inline auto&
            traject_ptr(const flow::Id& = flow::def_id) const noexcept;
        inline Traject&
            traject(const flow::Id& = flow::def_id) const& noexcept;
        inline auto& traject_ptr_by_idx(Idx) const noexcept;
        inline Traject& traject_by_idx(Idx) const& noexcept;

        long step_count() const noexcept;

        virtual void add_traject(Traject);
        void add_traject();

        virtual void do_steps(const flow::Id&, State&, State& tmp,
                              const Real dt_, const Config&) const;
        void do_steps(State& state, State& tmp, const Real dt_,
                      const Config& config) const
                          { do_steps(flow::def_id, state, tmp, dt_, config); }

        virtual void eval(const flow::Id&, State&, const Config&) const;
        void eval(State& state, const Config& config) const
                                        { eval(flow::def_id, state, config); }
    private:
        flow::Ids _flow_ids{};
        flow::Ptrs _flow_ptrs{};
        mutable Traject_ptrs _traject_ptrs{};

        Real _dt{def_dt};
        bool _kept_invariant{};
    };
}

#include "ode/solver/traject.hpp"

#include "ode/solver.inl"
