#pragma once

#include "ode/flow.hpp"

#include "util/string.hpp"

namespace unsot::ode::flow {
    constexpr const char* derived_key_suffix = "'";

    constexpr size_t derived_key_suffix_size = String_view(derived_key_suffix).size();

    bool is_derived_key(String_view) noexcept;
    void check_is_derived_key(String_view);
    void check_is_not_derived_key(String_view);

    bool ode_key_corresponds_to(const Key&, const Key& derived_key) noexcept;
    void check_ode_key_corresponds_to(const Key&, const Key& derived_key);
    bool is_derived_key_corresponding_to(const Key&,
                                         const Key& ode_key) noexcept;
    void check_is_derived_key_corresponding_to(const Key&,
                                               const Key& ode_key);

    Key to_derived_key(const Key& ode_key) noexcept;
    Key to_derived_key_check(const Key& ode_key);
    Key to_ode_key(const Key& derived_key) noexcept;
    Key to_ode_key_check(const Key& derived_key);
    String_view to_ode_key_view(const Key& derived_key) noexcept;
    String_view to_ode_key_view_check(const Key& derived_key);
}

namespace unsot::ode::flow {
    class Parse : public Object<Parse> {
    public:
        Parse()                                                     = default;
        ~Parse()                                                    = default;
        Parse(const Parse&)                                         = default;
        Parse& operator =(const Parse&)                             = default;
        Parse(Parse&&)                                              = default;
        Parse& operator =(Parse&&)                                  = default;
        explicit Parse(Keys);
        Parse(Keys, List odes_ls, List invs_ls);
        Parse(Keys, List);
        Parse(List);
        inline Parse(List keys_ls, List odes_ls, List invs_ls);
        inline Parse(List keys_ls, List);

        All_formulas parse_odes(List) const;
        All_formulas_map parse_invs(List) const;
        All_formulas parse_odes(Formulas) const;
        All_formulas_map parse_invs(Formulas) const;

        All_formulas_map divide_invs_formulas(Formulas) const;

        [[nodiscard]] Flow perform() const&;
        [[nodiscard]] Flow perform()&&;

        bool valid_inv(const Formula&) const noexcept;
        void check_inv(const Formula&) const;
        bool valid_ode(const Formula&) const noexcept;
        void check_ode(const Formula&) const;

        bool valid_inv_head(const Formula&) const noexcept;
        void check_inv_head(const Formula&) const;
        bool valid_inv_body(const Formula&) const noexcept;
        void check_inv_body(const Formula&) const;
        bool valid_ode_head(const Formula&) const noexcept;
        void check_ode_head(const Formula&) const;
        bool valid_ode_body(const Formula&) const noexcept;
        void check_ode_body(const Formula&) const;

        inline bool valid_inv_key(const Key&) const noexcept;
        inline void check_inv_key(const Key&) const;
        inline bool valid_ode_key(const Key&) const noexcept;
        inline void check_ode_key(const Key&) const;

        const Key& get_inv_key(const Formula&) const noexcept;
        const Key& get_inv_key_check(const Formula&) const;
        const Key& get_derived_key(const Formula&) const noexcept;
        const Key& get_derived_key_check(const Formula&) const;
        Key get_ode_key(const Formula&) const noexcept;
        Key get_ode_key_check(const Formula&) const;
        String_view get_ode_key_view(const Formula&) const noexcept;
        String_view get_ode_key_view_check(const Formula&) const;
    protected:
        Keys parse_keys(List&);
        template <typename T> void parse_body(T&& odes, T&& invs);
        void parse_body(List&&);

        const auto& ckeys() const noexcept                   { return _keys; }
        auto& keys() noexcept                                { return _keys; }

        const auto& call_odes_formulas() const noexcept
                                                { return _all_odes_formulas; }
        auto& all_odes_formulas() noexcept      { return _all_odes_formulas; }
        inline const auto& codes_formulas(const Id&) const noexcept;
        inline auto& odes_formulas(const Id&) noexcept;
        const auto& call_invs_formulas_map() const noexcept
                                            { return _all_invs_formulas_map; }
        auto& all_invs_formulas_map() noexcept
                                            { return _all_invs_formulas_map; }

        const auto& cinvs() const noexcept                   { return _invs; }
        auto& invs() noexcept                                { return _invs; }
        const auto& codes() const noexcept                   { return _odes; }
        auto& odes() noexcept                                { return _odes; }

        static Formula to_ode_formula(Formula);
        static Formula to_inv_formula(Formula);
    private:
        Keys _keys{};

        All_formulas _all_odes_formulas{};
        All_formulas_map _all_invs_formulas_map{};

        Flow::Invariants _invs{};
        Flow::Odes _odes{};
    };
}

#include "ode/flow/parse.inl"
