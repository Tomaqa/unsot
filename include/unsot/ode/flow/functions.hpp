#pragma once

namespace unsot::ode::flow::aux {
    template <typename FunT>
    class Flow::Functions : public Dynamic<Flow::Functions<FunT>> {
    public:
        using typename Dynamic<Flow::Functions<FunT>>::This;
        using typename Dynamic<Flow::Functions<FunT>>::Base;

        friend Parse;

        using Fun = FunT;
        using Funs = Vector<Fun>;

        static constexpr bool has_preds_v = Fun::is_pred_v;

        Functions()                                                 = default;
        virtual ~Functions()                                        = default;
        Functions(const Functions&)                                 = default;
        Functions& operator =(const Functions&)                     = default;
        Functions(Functions&&)                                      = default;
        Functions& operator =(Functions&&)                          = default;
        explicit Functions(const Keys&);
        Functions(const Keys&, Formula);
        Functions(const Keys&, Formulas, Formula def_phi);

        const auto& ckeys() const noexcept              { return *ckeys_l(); }
        const auto& keys() const noexcept                  { return ckeys(); }

        const auto& creals_ptr() const noexcept         { return _reals_ptr; }
        auto& reals_ptr() noexcept                      { return _reals_ptr; }
        const auto& creals() const& noexcept         { return *creals_ptr(); }
        auto& reals()& noexcept                       { return *reals_ptr(); }
        auto&& reals()&& noexcept                    { return move(reals()); }

        const auto& cfuns() const noexcept                   { return _funs; }
        const auto& funs() const noexcept                  { return cfuns(); }
        const auto& cfun(const Id& id = def_id) const  { return cfuns()[id]; }
        const auto& fun(const Id& id = def_id) const      { return cfun(id); }

        size_t size() const noexcept            { return std::size(cfuns()); }
        bool empty() const noexcept            { return std::empty(cfuns()); }

        void add_formula(Formula);

        virtual bool valid_id(const Id&) const noexcept;
        virtual void check_id(const Id&) const;

        Real operator ()(const Reals& state, const Id& = def_id) const;

        String to_string() const& override;
        virtual String head_to_string() const                   { return {}; }
        virtual String body_to_string() const;
    protected:
        void linit();
        bool lvalid_post_init() const noexcept;
        void lcheck_post_init() const;

        virtual void init_funs();
        virtual void init_fun(Fun&);

        virtual bool valid_funs() const noexcept;
        virtual void check_funs() const;

        virtual bool valid_fun(const Fun&) const noexcept;
        virtual void check_fun(const Fun&) const;
        virtual bool valid_fun_keys(const Fun&) const noexcept;
        virtual void check_fun_keys(const Fun&) const;
        bool valid_formula(const Formula&) const noexcept;
        virtual void check_formula(const Formula&) const;

        bool valid_formula_head(const Formula&) const noexcept;
        virtual void check_formula_head(const Formula&) const;
        /// `valid_formula_head' is supposed to be true
        bool valid_formula_body(const Formula&) const noexcept;
        virtual void check_formula_body(const Formula&) const;
        virtual bool valid_formula_head_impl(const Formula&) const;
        virtual bool valid_formula_body_impl(const Formula&) const;

        virtual bool is_atom_formula(const Formula&) const noexcept
                                                              { return true; }

        virtual bool valid_fun_key(const Key&) const noexcept;
        virtual void check_fun_key(const Key&) const;

        virtual bool
            contains_any_valid_fun_key(const Formula&) const noexcept;
        bool contains_any_valid_fun_key(const expr::Ptr&) const noexcept;

        /// `valid_formula_head' is supposed to be true
        virtual const Key& get_fun_key(const Formula&) const noexcept;
        /// `valid_formula_head' is explicitly checked here
        const Key& get_fun_key_check(const Formula&) const;

        virtual bool all_get_fun_keys_equal(const Formula&) const noexcept;

        const Keys_link& ckeys_l() const noexcept          { return _keys_l; }

        auto& funs() noexcept                                { return _funs; }
        auto& fun(const Id& id = def_id)                { return funs()[id]; }

        virtual String fun_to_string(const Id&) const;

        bool lequals(const This&) const noexcept;
        virtual bool equals_keys(const Keys&) const noexcept;
        virtual bool equals_funs(const Funs&) const noexcept;
    private:
        Functions(const Keys&, Formulas);

        Keys_link _keys_l{};
        Reals_ptr _reals_ptr{};

        Funs _funs{};
    };
    extern template class Flow::Functions<Fun>;
    extern template class Flow::Functions<Pred>;

    template <typename B>
    class Flow::Local_mixin : public Inherit<B, Local_mixin<B>> {
    public:
        using Inherit = unsot::Inherit<B, Local_mixin<B>>;
        using typename Inherit::This;
        using typename Inherit::Base;

        Local_mixin()                                               = default;
        virtual ~Local_mixin()                                      = default;
        Local_mixin(const Local_mixin&)                             = default;
        Local_mixin& operator =(const Local_mixin&)                 = default;
        Local_mixin(Local_mixin&&)                                  = default;
        Local_mixin& operator =(Local_mixin&&)                      = default;
        explicit Local_mixin(const Keys&, Key = {});
        Local_mixin(const Keys&, Key, Formula);
        Local_mixin(const Keys&, Key, Formulas, Formula def_phi);

        const auto& ckey() const noexcept                     { return _key; }
        const auto& key() const& noexcept                   { return ckey(); }
        auto& key()& noexcept                                 { return _key; }
        auto&& key()&& noexcept                        { return move(key()); }

        String head_to_string() const override;
    protected:
        bool lvalid_pre_init() const noexcept;
        void lcheck_pre_init() const;

        virtual bool valid_key() const noexcept;
        virtual void check_key() const;

        bool valid_fun_key(const Key&) const noexcept override;
        void check_fun_key(const Key&) const override;

        bool lequals(const This&) const noexcept;
        virtual bool equals_key(const Key&) const noexcept;
    private:
        Key _key{};
    };
}

#include "ode/flow/functions.inl"
