#pragma once

namespace unsot::ode::flow::aux {
    bool Flow::Invariants::masked(const Id& id) const
    {
        return util::masked(cmask(), id);
    }
}
