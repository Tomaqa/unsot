#pragma once

namespace unsot::ode::flow::aux {
    class Flow::Odes : public Inherit<Local_functions<Fun>, Odes> {
    public:
        friend Parse;

        static inline const Formula def_formula = "+ 0";

        using Inherit::Inherit;
        virtual ~Odes()                                             = default;
        Odes(const Keys&, Key, Formulas, Formula def_phi = def_formula);
    protected:
        explicit inline Odes(const Keys&, Key = {});

        bool valid_funs() const noexcept override;
        void check_funs() const override;
        bool valid_funs_nonempty() const noexcept;
        void check_funs_nonempty() const;

        bool valid_fun_key(const Key&) const noexcept override;
        void check_fun_key(const Key&) const override;
    };
}

#include "ode/flow/odes.inl"
