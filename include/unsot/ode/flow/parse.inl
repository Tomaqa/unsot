#pragma once

namespace unsot::ode::flow {
    Parse::Parse(List keys_ls, List odes_ls, List invs_ls)
        : Parse(Keys::parse(move(keys_ls)), move(odes_ls), move(invs_ls))
    { }

    Parse::Parse(List keys_ls, List ls)
        : Parse(Keys::parse(move(keys_ls)), move(ls))
    { }

    bool Parse::valid_inv_key(const Key& key_) const noexcept
    {
        return cinvs().valid_fun_key(key_);
    }

    void Parse::check_inv_key(const Key& key_) const
    {
        cinvs().check_fun_key(key_);
    }

    bool Parse::valid_ode_key(const Key& key_) const noexcept
    {
        return codes().valid_fun_key(key_);
    }

    void Parse::check_ode_key(const Key& key_) const
    {
        codes().check_fun_key(key_);
    }

    template <typename T>
    void Parse::parse_body(T&& odes, T&& invs)
    {
        all_odes_formulas() = parse_odes(move(odes));
        all_invs_formulas_map() = parse_invs(move(invs));
    }

    const auto& Parse::codes_formulas(const Id& id) const noexcept
    {
        return call_odes_formulas()[id];
    }

    auto& Parse::odes_formulas(const Id& id) noexcept
    {
        return all_odes_formulas()[id];
    }
}
