#pragma once

namespace unsot::ode::flow::aux {
    Flow::Keys_ptr Flow::new_keys(Keys&& keys_)
    {
        return MAKE_SHARED(move(keys_));
    }

    Flow::Invariants_ptr Flow::new_invs(Invariants&& invs)
    {
        return MAKE_SHARED(Invariants::cons(move(invs)));
    }

    Flow::Timer_ptr Flow::new_timer(Timer&& timer_)
    {
        return MAKE_SHARED(move(timer_));
    }

    size_t Flow::size() const noexcept
    {
        return ckeys().ode_size();
    }

    /// Checks presence of all keys and their order
    template <typename AllFuns>
    bool Flow::valid_keys_tp(const AllFuns& all_funs) const noexcept
    {
        if (std::size(all_funs) != ckeys().ode_size()) return false;
        return all_of(all_funs, ckeys().code_begin(),
                      [](auto& funs, auto& key_){
            return funs.ckey() == key_;
        });
    }

    template <typename AllFuns>
    void Flow::check_keys_tp(const AllFuns& all_funs) const
    {
        using unsot::to_string;
        expect(valid_keys_tp(all_funs),
               "Function variants mismatch with ODE Keys: "s
               + to_string(all_funs) + "\n!~\n"
               + to_string(ckeys().code_keys_slice()));
    }

    template <typename AllFuns>
    bool Flow::valid_ids(const AllFuns& all_funs,
                         const Ids& ids) const noexcept
    {
        using Funs_ = typename AllFuns::value_type;
        if (std::empty(ids)) return true;
        if (std::size(ids) != size()) return false;
        return all_of(all_funs, cbegin(ids), mem_fn(&Funs_::valid_id));
    }

    template <typename AllFuns>
    void Flow::check_ids(const AllFuns& all_funs, const Ids& ids) const
    {
        using unsot::to_string;
        expect(valid_ids(all_funs, ids),
               "Invalid ids for function variants "s
               + to_string(all_funs) + ": " + to_string(ids));
    }
}

////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////

namespace unsot::ode::flow {
    Config::Config(Ids odes_ids_, Id tau_id_, Id t_id_,
                   Ids linvs_ids_, Id ginv_id_)
        : odes_ids(move(odes_ids_)),
          tau_id(move(tau_id_)), t_id(move(t_id_)),
          linvs_ids(move(linvs_ids_)), ginv_id(move(ginv_id_))
    { }
}

////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////

namespace unsot::ode::flow {
    bool is_special_key(const Key& key) noexcept
    {
        return key == tau_key || key == t_key;
    }

    bool is_global_key(const Key& key) noexcept
    {
        return key == global_key;
    }

    Idx to_idx(const Id& id) noexcept
    {
        return (id == def_id) ? id : id-1;
    }
}

////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////

namespace unsot::ode {
    flow::Ptr new_flow(Flow&& flow)
    {
        return MAKE_SHARED(move(flow));
    }
}
