#pragma once

namespace unsot::smt::solver {
    template <typename S>
    template <typename StrategyT>
    auto Crtp<S>::new_strategy()
    {
        return StrategyT::That::new_me(this->that());
    }

    template <typename S>
    template <typename T>
    void Crtp<S>::set_check_sat_strategy_tp(bool force)
    {
        Check_sat::template set_tp<T>(check_sat_ptr(), force);
    }

    template <typename S>
    auto Crtp<S>::make_parser() noexcept
    {
        return Access::make_parser(this->that());
    }

    template <typename S>
    const auto& Crtp<S>::coption(const Key& key) const
    {
        return coptions_map()[key];
    }

    template <typename S>
    auto& Crtp<S>::option(const Key& key) noexcept
    {
        return options_map()[key];
    }

    template <typename S>
    const auto& Crtp<S>::ccheck_sat_ref() const noexcept
    {
        return Check_sat::cref(ccheck_sat_ptr());
    }

    template <typename S>
    auto& Crtp<S>::check_sat_ref() noexcept
    {
        return Check_sat::ref(check_sat_ptr());
    }

    template <typename S>
    auto Crtp<S>::run_l() const noexcept
    {
        return static_cast<typename That::Run*>(_run_l);
    }

    template <typename S>
    void Crtp<S>::set_run(Run& run) noexcept
    {
        rrun_l() = &run;
    }

    template <typename S>
    auto& Crtp<S>::run_ref()
    {
        auto l = run_l();
        expect(l, "The SMT solver run object is not set.");
        return *l;
    }

    template <typename S>
    void Crtp<S>::set_all_strategies(const String& name, bool force)
    try {
        set_all_strategies_impl(name, force);
    }
    catch (Dummy) {
        THROW("Unknown strategy: "s + name);
    }

    template <typename S>
    void Crtp<S>::set_check_sat_strategy(const String& name, bool force)
    {
        Check_sat::set_tp(BIND_THIS(set_check_sat_strategy_impl), name, force);
    }
}

////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////

namespace unsot::smt::solver {
    template <typename S>
    auto Crtp<S>::Access::make_parser(That& that_) noexcept
    {
        return typename Access::Parser(that_);
    }
}
