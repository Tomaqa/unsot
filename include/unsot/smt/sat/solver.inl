#pragma once

namespace unsot::smt::sat::solver {
    template <typename S, typename Conf>
    void Crtp<S, Conf>::neg(Lit& p) const noexcept
    {
        p = to_neg(p);
    }

    template <typename S, typename Conf>
    typename Crtp<S, Conf>::Lit Crtp<S, Conf>::to_neg(Lit p) const noexcept
    {
        neg(p);
        return p;
    }

    template <typename S, typename Conf>
    ostream& Crtp<S, Conf>::to_dimacs(ostream&, Cnf) const
    {
        THROW("Not supported!");
    }

    template <typename S, typename Conf>
    ostream& Crtp<S, Conf>::to_dimacs(ostream& os) const
    {
        return to_dimacs(os, to_cnf());
    }

    template <typename S, typename Conf>
    Value Crtp<S, Conf>::lit_value(const Lit& p) const
    {
        return value(p.var_id()) ^ p.is_neg();
    }

    template <typename S, typename Conf>
    Value Crtp<S, Conf>::lit_model_value(const Lit& p) const
    {
        return model_value(p.var_id()) ^ p.is_neg();
    }

    ////////////////////////////////////////////////////////////////

    template <typename B, typename SolverT>
    typename Solver_mixin<B, SolverT>::Solver_ptr
    Solver_mixin<B, SolverT>::new_solver()
    {
        return Access::new_solver();
    }

    template <typename B, typename SolverT>
    const auto& Solver_mixin<B, SolverT>::csolver() const noexcept
    {
        return Access::csolver(this->cthat());
    }

    template <typename B, typename SolverT>
    auto& Solver_mixin<B, SolverT>::solver() noexcept
    {
        return Access::solver(this->that());
    }
}

////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////

namespace unsot::smt::sat::solver {
    template <typename B, typename SolverT>
    typename Solver_mixin<B, SolverT>::Solver_ptr
    Solver_mixin<B, SolverT>::Access::new_solver()
    {
        return MAKE_UNIQUE(typename Access::Solver());
    }

    template <typename B, typename SolverT>
    const auto& Solver_mixin<B, SolverT>::Access::csolver(const That& that_) noexcept
    {
        using _Solver = typename Access::Solver;
        assert(dynamic_cast<const _Solver*>(get(that_.csolver_ptr())));
        return static_cast<const _Solver&>(*that_.csolver_ptr());
    }

    template <typename B, typename SolverT>
    auto& Solver_mixin<B, SolverT>::Access::solver(That& that_) noexcept
    {
        using _Solver = typename Access::Solver;
        assert(dynamic_cast<_Solver*>(get(that_.solver_ptr())));
        return static_cast<_Solver&>(*that_.solver_ptr());
    }
}
