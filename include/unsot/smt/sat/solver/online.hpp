#pragma once

#include "smt/sat/solver.hpp"

#include "util/optional.hpp"
#include "util/flag.hpp"
#include "smt/solver.hpp"

namespace unsot::smt::sat::solver::online {
    template <typename BaseConf> struct Conf;

    template <typename ConfT> class Smt_check_sat_base;

    template <typename B, template<typename> typename ConfT = Conf,
              typename SmtCheckSat = Smt_check_sat_base<ConfT<typename B::Base_conf>>>
        class Mixin;

    template <typename S, typename BaseConf, template<typename> typename ConfT = Conf,
              typename SmtCheckSat = Smt_check_sat_base<ConfT<BaseConf>>>
        using Crtp = Mixin<solver::Crtp<S, BaseConf>, ConfT, SmtCheckSat>;
}

namespace unsot::smt::sat::solver::online {
    template <typename BaseConf>
    struct Conf {
        using Base_conf = BaseConf;

        using Sat_var_id = typename Base_conf::Var_id;
        using Sat_lit = typename Base_conf::Lit;

        using T_propagate_result = Sat;
        using T_explain_result = void;
        struct T_decision_suggestion {
            Sat_var_id var_id;
            Flag flag{};
        };
    };

    template <typename ConfT>
    class Smt_check_sat_base {
    public:
        using Online_conf = ConfT;

        Smt_check_sat_base()                                                              = default;
        virtual ~Smt_check_sat_base()                                                     = default;
        Smt_check_sat_base(Smt_check_sat_base&&)                                          = default;

        virtual int decision_level() const noexcept                                             = 0;

        virtual void notice(typename Online_conf::Sat_lit)                                      = 0;
        virtual typename Online_conf::T_propagate_result t_propagate_exec()                     = 0;

        virtual void notice_new_decision_level()                                                 { }
        virtual Optional<typename Online_conf::T_decision_suggestion> t_suggest_decision()
                                                                                      { return {}; }

        virtual typename Online_conf::T_explain_result t_explain(typename Online_conf::Sat_lit);

        virtual void notice_backtrack()                                                          { }
        virtual void notice_backtrack_to(int level);
        virtual void notice_restart();
    };

    template <typename B, template<typename> typename ConfT, typename SmtCheckSat>
    class Mixin : public Inherit<B, Mixin<B, ConfT, SmtCheckSat>> {
    public:
        using Inherit = unsot::Inherit<B, Mixin<B, ConfT, SmtCheckSat>>;
        using typename Inherit::This;
        using typename Inherit::That;
        using Online_t = This;

        using Online_conf = ConfT<typename Inherit::Base_conf>;

        using Smt_check_sat = SmtCheckSat;
        using Smt_check_sat_link = Smt_check_sat*;

        using typename Inherit::Var_id;
        using typename Inherit::Lit;

        static_assert(is_same_v<Var_id, typename Online_conf::Sat_var_id>);
        static_assert(is_same_v<Lit, typename Online_conf::Sat_lit>);

        using typename Inherit::Clause;

        using Inherit::Inherit;
        Mixin()                                                                           = default;
        virtual ~Mixin()                                                                  = default;
        Mixin(const Mixin&)                                                                = delete;
        Mixin& operator =(const Mixin&)                                                    = delete;
        Mixin(Mixin&&)                                                                    = default;
        Mixin& operator =(Mixin&&)                                                        = default;
        Mixin(Smt_check_sat&);

        void connect(Smt_check_sat&);

        virtual int decision_level() const                                                      = 0;
        virtual int decision_level_of(const Var_id&) const                                      = 0;

        /// Called from SMT - this should *not* trigger `t_propagate`
        /// The variable is supposed to be undefined yet!
        virtual void sat_propagate(Lit)                                                         = 0;
        void sat_propagate(const Var_id&, bool val);

        virtual void sat_learn(Clause)                                                          = 0;

        virtual void sat_backtrack()                                                            = 0;
        virtual void sat_backtrack_to(int level)                                                = 0;
    protected:
        const auto& csmt_check_sat_ref() const noexcept;
        auto& smt_check_sat_ref() noexcept;

        virtual void notify(Lit);
        virtual typename Online_conf::T_propagate_result t_propagate_exec();

        virtual void notify_new_decision_level();
        virtual Optional<typename Online_conf::T_decision_suggestion> t_suggest_decision();

        virtual typename Online_conf::T_explain_result t_explain(Lit);

        virtual void notify_backtrack();
        virtual void notify_backtrack_to(int level);
        virtual void notify_restart();
    private:
        Smt_check_sat_link _smt_check_sat_l{};
    };
}

#include "smt/sat/solver/online.inl"
