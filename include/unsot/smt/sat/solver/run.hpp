#pragma once

#include "util/run.hpp"
#include "smt/sat/solver.hpp"

namespace unsot::smt::sat::solver {
    template <typename S, typename ConfT>
    class Crtp<S, ConfT>::Run
        : public virtual unsot::Inherit<util::Run>,
          public unsot::Crtp<typename Crtp<S, ConfT>::That::Run> {
    public:
        using typename unsot::Crtp<typename Crtp<S, ConfT>::That::Run>::That;
        using Solver = Crtp::That;

        using Inherit::Inherit;
        virtual ~Run()                                              = default;

        void init() override;
        void do_stuff() override;

        String usage() const override;
    protected:
        virtual const Solver& csolver() const noexcept     { return _solver; }
        virtual Solver& solver() noexcept                  { return _solver; }

        String lusage() const;

        void linit()                                                       { }

        String getopt_str() const noexcept override;
        String lgetopt_str() const noexcept;
        bool process_opt(char) override;
        bool lprocess_opt(char);
    private:
        Solver _solver{};
    };
}
