#pragma once

#include "smt/sat/solver.hpp"
#include "minisat/core/Solver.h"

namespace unsot::smt::sat::solver::minisat {
    struct Lit;

    struct Conf;

    template <typename B> class Mixin;

    using Orig_solver = ::Minisat::Solver;

    template <typename S, typename ConfT = Conf>
        using Crtp = Mixin<Solver_mixin<solver::Crtp<S, ConfT>, Orig_solver>>;

    using Orig_var = ::Minisat::Var;
    using Orig_lit = ::Minisat::Lit;
    using Orig_lbool = ::Minisat::lbool;

    using Orig_clause = ::Minisat::Clause;

    template <typename T> using Orig_vec = ::Minisat::vec<T>;
    using Orig_lits = Orig_vec<Orig_lit>;
    using Orig_lbools = Orig_vec<Orig_lbool>;

    using Var_id = Orig_var;

    Value to_value(const Orig_lbool&) noexcept;
    Values to_values(const Orig_lbools&);
}

namespace unsot::smt::sat::solver::minisat {
    struct Lit : Inherit<Orig_lit, Lit> {
        using Inherit::Inherit;
        inline Lit(Var_id, bool sign_);

        /// `Minisat` understands `sign` (confusingly) as negation!
        bool is_neg() const noexcept        { return ::Minisat::sign(*this); }
        bool sign() const noexcept                       { return !is_neg(); }
        Var_id var_id() const noexcept       { return ::Minisat::var(*this); }

        String to_string() const&;
    };

    struct Conf {
        using Var_id = minisat::Var_id;
        using Lit = minisat::Lit;
    };

    template <typename B>
    class Mixin : public Inherit<B, Mixin<B>> {
    public:
        using Inherit = unsot::Inherit<B, Mixin<B>>;
        using typename Inherit::This;
        using typename Inherit::That;
        using Minisat_t = This;

        using typename Inherit::Var_id;
        using typename Inherit::Lit;

        using typename Inherit::Clause;
        using typename Inherit::Cnf;

        static_assert(is_convertible_v<Var_id, minisat::Var_id>);
        static_assert(is_derived_from_v<Lit, minisat::Lit>);

        using Inherit::Inherit;
        virtual ~Mixin()                                            = default;
        Mixin(const Mixin&)                                          = delete;
        Mixin& operator =(const Mixin&)                              = delete;
        Mixin(Mixin&&)                                              = default;
        Mixin& operator =(Mixin&&)                                  = default;

        Var_id new_var() override;

        size_t vars_size() const noexcept override;
        size_t clauses_size() const noexcept override;

        Lit to_neg(Lit) const noexcept override;

        using Inherit::contains;
        bool contains(const Var_id&) const noexcept override;

        using Inherit::to_cnf;

        bool add_clause(Clause) override;
        bool add_clause() override;
        bool add_clause(Lit) override;
        bool add_clause(Lit, Lit) override;
        bool add_clause(Lit, Lit, Lit) override;

        Cnf to_cnf() const override;

        using Inherit::to_dimacs;
        ostream& to_dimacs(ostream&) const override;

        bool simplify() override;

        bool solve(const Clause& assumps) override;
        Sat solve_limited(const Clause& assumps) override;
        bool solve() override;
        bool solve(Lit) override;
        bool solve(Lit, Lit) override;
        bool solve(Lit, Lit, Lit) override;

        bool okay() const override;

        Value value(const Var_id&) const override;
        Value model_value(const Var_id&) const override;

        const Values& cmodel() const override;
        const Clause& cconflict() const override;
    protected:
        static Clause make_clause(const Orig_clause&);
        static Clause make_clause(const Orig_vec<Lit>&);

        /// to allow efficient simple casting
        static_assert(sizeof(Var_id) == sizeof(Orig_var));
        static_assert(sizeof(Lit) == sizeof(Orig_lit));

        static Orig_lits& cast_orig_lits(Orig_vec<Lit>&) noexcept;
        static const Orig_vec<Lit>& ccast_orig_vec_of_lits(const Orig_lits&) noexcept;

        template <typename ContT>
            static void to_orig_vec_of_lits(Orig_vec<Lit>&, ContT&&);
    private:
        mutable Values _model{};
        mutable Clause _conflict{};
    };
}

#include "smt/sat/solver/minisat.inl"
