#pragma once

#include "smt/sat/solver.hpp"

#include "util/hash.hpp"
#include "util/bimap.hpp"
#include "expr/bools.hpp"

namespace unsot::smt::sat::solver::expr {
    template <typename B> class Mixin;

    template <typename S, typename ConfT>
        using Crtp = Mixin<solver::Crtp<S, ConfT>>;
}

namespace unsot::smt::sat::solver::expr {
    /// SAT solver extended of `unsot::expr::bools::Clause` etc. interface
    template <typename B>
    class Mixin : public Inherit<B, Mixin<B>> {
    public:
        using Inherit = unsot::Inherit<B, Mixin<B>>;
        using typename Inherit::This;
        using typename Inherit::That;
        using Expr_t = This;

        using typename Inherit::Var_id;
        using typename Inherit::Lit;

        static_assert(is_same_v<bools::Var_id, unsot::expr::bools::Var_id>);
        static_assert(!is_convertible_v<Var_id, bools::Var_id>);
        static_assert(!is_convertible_v<Lit, bools::Lit>);

        using typename Inherit::Clause;
        using typename Inherit::Cnf;

        using Var_ids_bimap = Bimap<bools::Var_id, Var_id, Hash>;

        using Inherit::Inherit;
        virtual ~Mixin()                                            = default;
        Mixin(const Mixin&)                                          = delete;
        Mixin& operator =(const Mixin&)                              = delete;
        Mixin(Mixin&&)                                              = default;
        Mixin& operator =(Mixin&&)                                  = default;

        const auto& cvar_ids_bimap() const noexcept { return _var_ids_bimap; }
        const auto& cvar_id(const bools::Var_id&) const;
        const auto& cevar_id(const Var_id&) const;

        using Inherit::check_contains;
        bool contains(const Var_id&) const noexcept override;
        bool contains(const bools::Var_id&) const noexcept;
        void check_contains(const bools::Var_id&) const;

        virtual Cnf to_cnf(bools::Cnf) const;
        virtual Clause to_clause(bools::Clause) const;
        virtual Lit to_lit(bools::Lit) const;

        virtual bools::Cnf to_ecnf(Cnf) const;
        virtual bools::Clause to_eclause(Clause) const;
        virtual bools::Lit to_elit(Lit) const;

        virtual Var_id add_new_var(const bools::Var_id&);

        virtual bool add_ecnf(bools::Cnf);
        virtual bool add_eclause(bools::Clause);
        virtual bool add_eclause(bools::Lit);
        virtual bool add_eclause(bools::Lit, bools::Lit);
        virtual bool add_eclause(bools::Lit, bools::Lit, bools::Lit);

        using Inherit::to_cnf;
        virtual bools::Cnf to_ecnf() const;
    protected:
        auto& var_ids_bimap() noexcept              { return _var_ids_bimap; }
    private:
        Var_ids_bimap _var_ids_bimap{};
    };
}

#include "smt/sat/solver/expr.inl"
