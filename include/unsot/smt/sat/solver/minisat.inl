#pragma once

namespace unsot::smt::sat::solver::minisat {
    Lit::Lit(Var_id var, bool sign_)
        : Lit(::Minisat::mkLit(move(var), !sign_))
    { }

    ////////////////////////////////////////////////////////////////

    template <typename B>
    typename Mixin<B>::Var_id Mixin<B>::new_var()
    {
        return this->solver().newVar();
    }

    template <typename B>
    size_t Mixin<B>::vars_size() const noexcept
    {
        return this->csolver().nVars();
    }

    template <typename B>
    size_t Mixin<B>::clauses_size() const noexcept
    {
        return this->csolver().nClauses();
    }

    template <typename B>
    typename Mixin<B>::Lit Mixin<B>::to_neg(Lit p) const noexcept
    {
        return ~p;
    }

    template <typename B>
    Orig_lits& Mixin<B>::cast_orig_lits(Orig_vec<Lit>& lits) noexcept
    {
        return reinterpret_cast<Orig_lits&>(lits);
    }

    template <typename B>
    const Orig_vec<typename Mixin<B>::Lit>&
    Mixin<B>::ccast_orig_vec_of_lits(const Orig_lits& lits) noexcept
    {
        return reinterpret_cast<const Orig_vec<Lit>&>(lits);
    }

    template <typename B>
    bool Mixin<B>::add_clause()
    {
        return this->solver().addEmptyClause();
    }

    template <typename B>
    bool Mixin<B>::add_clause(Lit p)
    {
        return this->solver().addClause(move(p));
    }

    template <typename B>
    bool Mixin<B>::add_clause(Lit p, Lit q)
    {
        return this->solver().addClause(move(p), move(q));
    }

    template <typename B>
    bool Mixin<B>::add_clause(Lit p, Lit q, Lit r)
    {
        return this->solver().addClause(move(p), move(q), move(r));
    }

    template <typename B>
    bool Mixin<B>::simplify()
    {
        return this->solver().simplify();
    }

    template <typename B>
    bool Mixin<B>::solve()
    {
        return this->solver().solve();
    }

    template <typename B>
    bool Mixin<B>::solve(Lit p)
    {
        return this->solver().solve(move(p));
    }

    template <typename B>
    bool Mixin<B>::solve(Lit p, Lit q)
    {
        return this->solver().solve(move(p), move(q));
    }

    template <typename B>
    bool Mixin<B>::solve(Lit p, Lit q, Lit r)
    {
        return this->solver().solve(move(p), move(q), move(r));
    }

    template <typename B>
    bool Mixin<B>::okay() const
    {
        return this->csolver().okay();
    }
}
