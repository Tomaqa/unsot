#pragma once

#include "smt/sat/solver/minisat.hpp"

#ifdef NO_EXPLICIT_TP_INST
#include "smt/sat/solver.tpp"
#include "smt/sat/solver/minisat.tpp"

#include "smt/sat/solver/run.tpp"
#endif

namespace unsot::smt::sat::solver::offline::minisat {
    using namespace solver::minisat;

    template <typename S, typename ConfT = Conf>
        using Crtp = solver::minisat::Crtp<S, ConfT>;
}

namespace unsot::smt::sat::solver::offline {
    //! `cons` does not return `Minisat`, because proper `Inherit` not used
    //! but its internals cannot inherit from final class
    struct Minisat final : Inherit<minisat::Crtp<Minisat>> {
        using Inherit::Inherit;
    };
}

#ifndef NO_EXPLICIT_TP_INST
namespace unsot::smt::sat::solver {
    extern template class offline::Minisat::Base::Crtp;
    extern template class offline::Minisat::Solver_mixin;
    extern template class offline::Minisat::Minisat_t::Mixin;
}
#endif  /// NO_EXPLICIT_TP_INST
