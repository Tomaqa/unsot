#pragma once

namespace unsot::smt::sat::solver::online::minisat {
    template <typename B>
    int Mixin<B>::decision_level() const
    {
        return this->csolver().decisionLevel();
    }

    template <typename B>
    int Mixin<B>::decision_level_of(const Var_id& x) const
    {
        return this->csolver().level(x);
    }
}
