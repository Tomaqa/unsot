#pragma once

#include "smt/sat/solver/online.hpp"
#include "smt/sat/solver/expr.hpp"
#include "smt/sat/solver/online/minisat.hpp"

namespace unsot::smt::sat::solver::online::expr::minisat {
    using namespace solver::expr;
    using namespace solver::minisat;

    template <typename S, typename BaseConf = minisat::Conf,
              template<typename> typename OnlineConf = online::Conf,
              typename SmtCheckSat = Smt_check_sat_base<OnlineConf<BaseConf>>>
        using Crtp =
            solver::expr::Mixin<online::minisat::Crtp<S, BaseConf, OnlineConf, SmtCheckSat>>;
}

namespace unsot::smt::sat::solver::online::expr {
    template <template<typename> typename OnlineConf = online::Conf,
              typename SmtCheckSat = Smt_check_sat_base<OnlineConf<minisat::Conf>>>
    struct Minisat
        : Inherit<minisat::Crtp<Minisat<OnlineConf, SmtCheckSat>, minisat::Conf,
                                OnlineConf, SmtCheckSat>,
                  Minisat<OnlineConf, SmtCheckSat>> {
        using Inherit =
            unsot::Inherit<minisat::Crtp<Minisat<OnlineConf, SmtCheckSat>, minisat::Conf,
                                         OnlineConf, SmtCheckSat>,
                           Minisat<OnlineConf, SmtCheckSat>>;

        using Inherit::Inherit;
    };
}
