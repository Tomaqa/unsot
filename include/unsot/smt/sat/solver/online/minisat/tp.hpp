#ifdef NO_EXPLICIT_TP_INST

#include "smt/sat/solver.tpp"
#include "smt/sat/solver/expr.tpp"
#include "smt/sat/solver/minisat.tpp"
#include "smt/sat/solver/online/minisat.tpp"

#include "smt/sat/solver/run.tpp"

#else

namespace unsot::smt::sat::solver {
    namespace aux {
        static_assert(is_same_v<_Minisat, typename TP_INST_SOLVER_NAMESPACE::aux::_S::Sat_solver::Minisat>);
    }

    extern template class aux::_Minisat::Base::Crtp;
    extern template class aux::_Minisat::Expr_t::Mixin;
    extern template class aux::_Minisat::Solver_mixin;
    extern template class aux::_Minisat::Minisat_t::Mixin;
    extern template class aux::_Minisat::Online_t::Mixin;
    extern template class aux::_Minisat::Online_minisat_t::Mixin;
}

#endif  /// NO_EXPLICIT_TP_INST
