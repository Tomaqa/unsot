#pragma once

namespace unsot::smt::sat::solver::online {
    template <typename ConfT>
    typename Smt_check_sat_base<ConfT>::Online_conf::T_explain_result
    Smt_check_sat_base<ConfT>::t_explain(typename Online_conf::Sat_lit)
    {
        if constexpr (!is_void_v<typename Online_conf::T_explain_result>) return {};
    }

    template <typename ConfT>
    void Smt_check_sat_base<ConfT>::notice_backtrack_to(int level)
    {
        assert(decision_level() >= level);

        const int diff = decision_level() - level;
        for (int i = 0; i < diff; ++i) notice_backtrack();
    }

    template <typename ConfT>
    void Smt_check_sat_base<ConfT>::notice_restart()
    {
        notice_backtrack_to(0);
    }

    ////////////////////////////////////////////////////////////////

    template <typename B, template<typename> typename ConfT, typename SmtCheckSat>
    Mixin<B, ConfT, SmtCheckSat>::Mixin(Smt_check_sat& smt_check_sat_)
    {
        connect(smt_check_sat_);
    }

    template <typename B, template<typename> typename ConfT, typename SmtCheckSat>
    void Mixin<B, ConfT, SmtCheckSat>::connect(Smt_check_sat& smt_check_sat_)
    {
        _smt_check_sat_l = &smt_check_sat_;
    }

    template <typename B, template<typename> typename ConfT, typename SmtCheckSat>
    const auto& Mixin<B, ConfT, SmtCheckSat>::csmt_check_sat_ref() const noexcept
    {
        assert(_smt_check_sat_l);
        return *_smt_check_sat_l;
    }

    template <typename B, template<typename> typename ConfT, typename SmtCheckSat>
    auto& Mixin<B, ConfT, SmtCheckSat>::smt_check_sat_ref() noexcept
    {
        assert(_smt_check_sat_l);
        return *_smt_check_sat_l;
    }

    template <typename B, template<typename> typename ConfT, typename SmtCheckSat>
    void Mixin<B, ConfT, SmtCheckSat>::sat_propagate(const Var_id& x, bool val)
    {
        sat_propagate(Lit(x, val));
    }

    template <typename B, template<typename> typename ConfT, typename SmtCheckSat>
    void Mixin<B, ConfT, SmtCheckSat>::notify(Lit p)
    {
        smt_check_sat_ref().notice(move(p));
    }

    template <typename B, template<typename> typename ConfT, typename SmtCheckSat>
    typename Mixin<B, ConfT, SmtCheckSat>::Online_conf::T_propagate_result
    Mixin<B, ConfT, SmtCheckSat>::t_propagate_exec()
    {
        return smt_check_sat_ref().t_propagate_exec();
    }

    template <typename B, template<typename> typename ConfT, typename SmtCheckSat>
    void Mixin<B, ConfT, SmtCheckSat>::notify_new_decision_level()
    {
        smt_check_sat_ref().notice_new_decision_level();
    }

    template <typename B, template<typename> typename ConfT, typename SmtCheckSat>
    Optional<typename Mixin<B, ConfT, SmtCheckSat>::Online_conf::T_decision_suggestion>
    Mixin<B, ConfT, SmtCheckSat>::t_suggest_decision()
    {
        return smt_check_sat_ref().t_suggest_decision();
    }

    template <typename B, template<typename> typename ConfT, typename SmtCheckSat>
    typename Mixin<B, ConfT, SmtCheckSat>::Online_conf::T_explain_result
    Mixin<B, ConfT, SmtCheckSat>::t_explain(Lit p)
    {
        return smt_check_sat_ref().t_explain(move(p));
    }

    template <typename B, template<typename> typename ConfT, typename SmtCheckSat>
    void Mixin<B, ConfT, SmtCheckSat>::notify_backtrack()
    {
        smt_check_sat_ref().notice_backtrack();
    }

    template <typename B, template<typename> typename ConfT, typename SmtCheckSat>
    void Mixin<B, ConfT, SmtCheckSat>::notify_backtrack_to(int level)
    {
        smt_check_sat_ref().notice_backtrack_to(level);
    }

    template <typename B, template<typename> typename ConfT, typename SmtCheckSat>
    void Mixin<B, ConfT, SmtCheckSat>::notify_restart()
    {
        smt_check_sat_ref().notice_restart();
    }
}
