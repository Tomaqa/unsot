#pragma once

#include "smt.hpp"

namespace unsot::smt::sat {
    using Value = Flag;
    using Values = expr::Values<Value>;
}
