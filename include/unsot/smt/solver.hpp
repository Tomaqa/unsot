#pragma once

#include "smt.hpp"

#include "util/hash.hpp"
#include "util/run.hpp"
#include "expr/formula.hpp"
#include "expr/preprocess.hpp"

#if VERBOSE >= 1
#define _SMT_VERBOSITY1(obj) true
#define _SMT_VERBOSITY2(obj) true
#define _SMT_VERBOSITY3(obj) true
#define _SMT_VERBOSITY4(obj) (_verbose_ >= 2 || (obj).cverbosity() >= 4)
#define _SMT_VERBOSITY5(obj) (_verbose_ >= 3 || (obj).cverbosity() >= 5)
#else
#define _SMT_VERBOSITY1(obj) ((obj).cverbosity() >= 1)
#define _SMT_VERBOSITY2(obj) ((obj).cverbosity() >= 2)
#define _SMT_VERBOSITY3(obj) ((obj).cverbosity() >= 3)
#ifdef DEBUG
#define _SMT_VERBOSITY4(obj) ((obj).cverbosity() >= 4)
#define _SMT_VERBOSITY5(obj) ((obj).cverbosity() >= 5)
#else
#define _SMT_VERBOSITY4(obj) false
#define _SMT_VERBOSITY5(obj) false
#endif
#endif

#define _SMT_CVERB(x) CSTREAM(std::clog, x)
#define _SMT_CVERBLN(x) CSTREAMLN(std::clog, x)
#if VERBOSE >= 1
#define _SMT_CVERB1(obj, x) \
                { if ((obj).cverbosity() >= 1) _SMT_CVERB(x); else CVERB(x); }
#define _SMT_CVERB2(obj, x) \
                { if ((obj).cverbosity() >= 2) _SMT_CVERB(x); else CVERB(x); }
#define _SMT_CVERB3(obj, x) \
                { if ((obj).cverbosity() >= 3) _SMT_CVERB(x); else CVERB(x); }
#define _SMT_CVERB1LN(obj, x) \
            { if ((obj).cverbosity() >= 1) _SMT_CVERBLN(x); else CVERBLN(x); }
#define _SMT_CVERB2LN(obj, x) \
            { if ((obj).cverbosity() >= 2) _SMT_CVERBLN(x); else CVERBLN(x); }
#define _SMT_CVERB3LN(obj, x) \
            { if ((obj).cverbosity() >= 3) _SMT_CVERBLN(x); else CVERBLN(x); }
#else
#define _SMT_CVERB1(obj, x)       { if (_SMT_VERBOSITY1(obj)) _SMT_CVERB(x); }
#define _SMT_CVERB2(obj, x)       { if (_SMT_VERBOSITY2(obj)) _SMT_CVERB(x); }
#define _SMT_CVERB3(obj, x)       { if (_SMT_VERBOSITY3(obj)) _SMT_CVERB(x); }
#define _SMT_CVERB1LN(obj, x)   { if (_SMT_VERBOSITY1(obj)) _SMT_CVERBLN(x); }
#define _SMT_CVERB2LN(obj, x)   { if (_SMT_VERBOSITY2(obj)) _SMT_CVERBLN(x); }
#define _SMT_CVERB3LN(obj, x)   { if (_SMT_VERBOSITY3(obj)) _SMT_CVERBLN(x); }
#endif
#if VERBOSE >= 2
#define _SMT_CVERB4(obj, x) \
               { if ((obj).cverbosity() >= 4) _SMT_CVERB(x); else CVERB2(x); }
#define _SMT_CVERB4LN(obj, x) \
           { if ((obj).cverbosity() >= 4) _SMT_CVERBLN(x); else CVERB2LN(x); }
#else
#define _SMT_CVERB4(obj, x)       { if (_SMT_VERBOSITY4(obj)) _SMT_CVERB(x); }
#define _SMT_CVERB4LN(obj, x)   { if (_SMT_VERBOSITY4(obj)) _SMT_CVERBLN(x); }
#endif
#if VERBOSE >= 3
#define _SMT_CVERB5(obj, x) \
               { if ((obj).cverbosity() >= 5) _SMT_CVERB(x); else CVERB3(x); }
#define _SMT_CVERB5LN(obj, x) \
           { if ((obj).cverbosity() >= 5) _SMT_CVERBLN(x); else CVERB3LN(x); }
#else
#define _SMT_CVERB5(obj, x)       { if (_SMT_VERBOSITY5(obj)) _SMT_CVERB(x); }
#define _SMT_CVERB5LN(obj, x)   { if (_SMT_VERBOSITY5(obj)) _SMT_CVERBLN(x); }
#endif

#define SMT_VERBOSITY1 _SMT_VERBOSITY1(this->csolver())
#define SMT_VERBOSITY2 _SMT_VERBOSITY2(this->csolver())
#define SMT_VERBOSITY3 _SMT_VERBOSITY3(this->csolver())
#define SMT_VERBOSITY4 _SMT_VERBOSITY4(this->csolver())
#define SMT_VERBOSITY5 _SMT_VERBOSITY5(this->csolver())

#define SMT_CVERB1(x) _SMT_CVERB1(this->csolver(), x)
#define SMT_CVERB2(x) _SMT_CVERB2(this->csolver(), x)
#define SMT_CVERB3(x) _SMT_CVERB3(this->csolver(), x)
#define SMT_CVERB4(x) _SMT_CVERB4(this->csolver(), x)
#define SMT_CVERB5(x) _SMT_CVERB5(this->csolver(), x)
#define SMT_CVERB1LN(x) _SMT_CVERB1LN(this->csolver(), x)
#define SMT_CVERB2LN(x) _SMT_CVERB2LN(this->csolver(), x)
#define SMT_CVERB3LN(x) _SMT_CVERB3LN(this->csolver(), x)
#define SMT_CVERB4LN(x) _SMT_CVERB4LN(this->csolver(), x)
#define SMT_CVERB5LN(x) _SMT_CVERB5LN(this->csolver(), x)

namespace unsot::smt::solver {
    enum class Mode { assert = 0, check, sat, unsat };

    template <typename S> class Crtp;

    using Option = Key;
    using Option_value = String;
    using Options_map = Hash<Option, Option_value>;

    using Verbosity = short;

    using Duration = double;

    constexpr bool no_force = false;
    constexpr bool do_force = true;
}

namespace unsot::smt::solver {
    template <typename S>
    class Crtp : public Object<Crtp<S>>, public unsot::Crtp<S> {
    public:
        using typename Object<Crtp<S>>::This;
        using typename unsot::Crtp<S>::That;
        using Base = This;

        class Check_sat;

        class Run;

        Crtp();
        virtual ~Crtp()                                                                   = default;
        Crtp(const Crtp&)                                                                 = default;
        Crtp& operator =(const Crtp&)                                                     = default;
        Crtp(Crtp&&)                                                                      = default;
        Crtp& operator =(Crtp&&)                                                          = default;

        template <typename T = void> void set_check_sat_strategy_tp(bool force = no_force);

        [[nodiscard]] List preprocess(String);
        [[nodiscard]] List preprocess(istream&);
        /// including `preprocess`
        void parse(String);
        void parse(istream&);

        /// discluding `Preprocess`
        [[nodiscard]] Preprocess preprocess_lines_only(String);
        [[nodiscard]] Preprocess preprocess_lines_only(istream&);
        [[nodiscard]] List preprocess_list_only(List);
        /// discluding `preprocess`
        void parse_only(List);

        const Mode& cmode() const noexcept                                         { return _mode; }
        const Mode& mode() const noexcept                                        { return cmode(); }

        const auto& cparsed_list() const noexcept                           { return _parsed_list; }
        const auto& parsed_list() const& noexcept                         { return cparsed_list(); }
        auto&& parsed_list()&& noexcept                              { return move(parsed_list()); }

        const auto& coptions_map() const noexcept                           { return _options_map; }
        const auto& options_map() const& noexcept                         { return coptions_map(); }
        auto&& options_map()&& noexcept                              { return move(options_map()); }
        inline const auto& coption(const Key&) const;
        inline const auto& option(const Key& key_) const                   { return coption(key_); }

        bool is_dry_run() const noexcept                                     { return _is_dry_run; }
        void set_dry_run() noexcept                                          { _is_dry_run = true; }
        void unset_dry_run() noexcept                                       { _is_dry_run = false; }

        bool cproducing_models() const noexcept                        { return _producing_models; }
        bool& producing_models() noexcept                              { return _producing_models; }

        const auto& cverbosity() const noexcept                               { return _verbosity; }
        auto& verbosity() noexcept                                            { return _verbosity; }
        void be_verbose() noexcept;
        void be_quiet() noexcept;

        const auto& ccheck_sat_ref() const noexcept;

        [[nodiscard]] auto run_l() const noexcept;
        void set_run(Run&) noexcept;
        auto& run_ref();
        Istream& is() const noexcept;
        Ostream& os() const noexcept;

        bool valid_option(const Option&, const Option_value&) const noexcept;
        void check_option(const Option&, const Option_value&) const;
        bool valid_option(const Option&) const noexcept;
        void check_option(const Option&) const;
        void set_option(const Option&, Option_value);
        Option_value get_option(const Option&) const;

        /// Set all strategies that correspond to the `name`
        void set_all_strategies(const String& name, bool force = no_force);

        void set_check_sat_strategy(const String& name, bool force = no_force);

        virtual bool contains(const Key&) const noexcept;
        void check_contains(const Key&) const;
        void check_not_contains(const Key&) const;
        virtual bool contains_var(const Key&) const noexcept                       { return false; }
        void check_contains_var(const Key&) const;
        void check_not_contains_var(const Key&) const;

        virtual void assert_elem(Elem)                                                          = 0;
        /// Assert 'arbitrary' expression
        /// (Id. `assert` is not used due to collision with the std. macro)
        virtual void assert_expr(Formula)                                                       = 0;
        void assert_expr(expr::Ptr);
        /// Assert expression with solely boolean arguments
        virtual void assert_bool(Formula)                                                       = 0;

        virtual Sat solve();

        String var_to_string(const Key&) const;
        void print_var(const Key&) const;
        void print_vars() const;
    protected:
        class Parser;

        struct Check_sat_conf;

        class Strategy_base;
        template <typename ConfT, typename... Args> class Strategy;

        using Strategy_ptr = Unique_ptr<Strategy_base>;
        using Check_sat_ptr = Strategy_ptr;

        static inline const String aux_var_key_prefix = "_";

        template <typename StrategyT> auto new_strategy();

        [[nodiscard]] auto make_parser() noexcept;

        /// Auxiliary getters for compatibility with nested classes
        const auto& csolver() const noexcept                                       { return *this; }
        auto& solver() noexcept                                                    { return *this; }

        Mode& mode() noexcept                                                      { return _mode; }

        auto& parsed_list()& noexcept                                       { return _parsed_list; }

        auto& options_map()& noexcept                                       { return _options_map; }
        inline auto& option(const Key&) noexcept;

        const auto& ccheck_sat_ptr() const noexcept                       { return _check_sat_ptr; }
        auto& check_sat_ptr() noexcept                                    { return _check_sat_ptr; }
        auto& check_sat_ref() noexcept;

        auto& rrun_l() noexcept                                                   { return _run_l; }

        long cauto_var_counter() const noexcept                        { return _auto_var_counter; }
        long& auto_var_counter() noexcept                              { return _auto_var_counter; }

        virtual bool valid_option_value(const Option&, const Option_value&) const;
        void check_option_value(const Option&, const Option_value&) const;
        virtual void apply_option(const Option&);

        virtual void set_all_strategies_impl(const String& name, bool force = no_force);

        virtual void set_check_sat_strategy_impl(const String& name, bool force = no_force);

        Key new_var_key(Key = "");

        virtual bool is_aux_var(const Key&) const noexcept;

        virtual void init_solve();
        virtual void finish_solve(const Sat&);

        virtual void print_var_impl(const Key&) const;

        bool allowed_get_value() const noexcept;

        virtual String stats_to_string() const;
        virtual String var_stats_to_string() const                                              = 0;
    private:
        struct Access;

        virtual String var_to_string_impl(const Key&) const                                     = 0;
        virtual void print_vars_impl() const                                                    = 0;

        Mode _mode{};

        List _parsed_list{};

        Options_map _options_map{};
        bool _is_dry_run{};
        bool _producing_models{true};
        Verbosity _verbosity{};

        Check_sat_ptr _check_sat_ptr{new_strategy<Check_sat>()};

        Run* _run_l{};

        long _auto_var_counter{};
    };
}

namespace unsot::smt::solver {
    template <typename S>
    struct Crtp<S>::Check_sat_conf {
        template <typename T> using Type_tp = typename T::Check_sat;

        using Ret = Sat;

        using Print_perform_parts = true_type;
    };

    /// Auxiliary class to allow accessing certain protected members of derived classes
    /// Avoid inheriting from the literally `final` class
    template <typename S>
    struct Crtp<S>::Access : That::Inherit {
        [[nodiscard]] static auto make_parser(That&) noexcept;
    };
}

#include "smt/solver/strategy.hpp"
#include "smt/solver/check_sat.hpp"
#include "smt/solver/parser.hpp"

#include "smt/solver.inl"
