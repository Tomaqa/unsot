#pragma once

#include "util/alg.hpp"
#include "expr/alg.hpp"

namespace unsot::smt::solver::bools {
    template <typename B, typename SatSolver>
    const var::Id& Mixin<B, SatSolver>::cbool_id(const typename Sat_solver::Var_id& sat_vid) const
    {
        return cbool_ids_map()[sat_vid];
    }

    template <typename B, typename SatSolver>
    const typename Mixin<B, SatSolver>::Sat_solver::Var_id&
    Mixin<B, SatSolver>::csat_var_id(const var::Id& vid) const
    {
        auto& sat_vid = csat_var_ids_map()[vid];
        assert(sat_vid != invalid_idx);
        return sat_vid;
    }

    template <typename B, typename SatSolver>
    const auto& Mixin<B, SatSolver>::cfun_def(const Key& key_) const
    {
        return cfun_defs_map()[key_];
    }

    template <typename B, typename SatSolver>
    auto& Mixin<B, SatSolver>::fun_def(const Key& key_)
    {
        return fun_defs_map()[key_];
    }

    template <typename B, typename SatSolver>
    const Key& Mixin<B, SatSolver>::cformula_key(const Formula& phi) const
    {
        return cformula_keys_map()[phi];
    }

    template <typename B, typename SatSolver>
    Key& Mixin<B, SatSolver>::formula_key(const Formula& phi)
    {
        return formula_keys_map()[phi];
    }

    template <typename B, typename SatSolver>
    var::Ptr* Mixin<B, SatSolver>::find_bool_ptr(const typename Sat_solver::Var_id& sat_vid) noexcept
    {
        return const_cast<var::Ptr*>(cfind_bool_ptr(sat_vid));
    }

    template <typename B, typename SatSolver>
    const var::Ptr* Mixin<B, SatSolver>::cfind_bool_ptr(const typename Sat_solver::Lit& lit) const noexcept
    {
        return cfind_bool_ptr(lit.var_id());
    }

    template <typename B, typename SatSolver>
    var::Ptr* Mixin<B, SatSolver>::find_bool_ptr(const typename Sat_solver::Lit& lit) noexcept
    {
        return const_cast<var::Ptr*>(cfind_bool_ptr(lit));
    }

    template <typename B, typename SatSolver>
    template <typename Sort, typename ValT>
    void Mixin<B, SatSolver>::define_var_tp(Key&& key_, ValT&& val)
    {
        static constexpr bool is_declare = is_same_v<ValT, Dummy>;

        if (SMT_VERBOSITY2) {
            if constexpr (is_declare) _SMT_CVERB("declare_var");
            else _SMT_CVERB("define_var");
            _SMT_CVERBLN(": " << key_);
        }

        auto& vars_ = vars<Sort>(this->that());
        vars_.add_var(move(key_), move(val));

        if constexpr (!is_declare) {
            auto& vptr = vars_.ptrs().back();
            vptr->lock();
        }
    }

    template <typename B, typename SatSolver>
    template <typename ArgSort, bool checkArgs, bool allowDups, typename UnF>
    Key Mixin<B, SatSolver>::instantiate_fun_inst_tp(Fun_inst&& fun, UnF f)
    {
        auto [key_, phi] = fun;

        if constexpr (!allowDups) if (contains_formula(phi)) {
            return cformula_key(phi);
        }

        SMT_CVERB2LN("instantiate_fun: " << fun << "\n");

        if constexpr (checkArgs) check_contains_free_arg_keys<ArgSort>(this->cthat(), phi);

        invoke(move(f), this->that_l(), move(fun));
        if constexpr (!allowDups) formula_key(phi) = key_;
        return key_;
    }

    template <typename B, typename SatSolver>
    template <typename Sort, typename ArgSort>
    void Mixin<B, SatSolver>::define_fun_tp(Key&& key_, Formula&& phi, Keys&& arg_keys)
    {
        phi.maybe_virtual_init();
        check_not_contains_fun_def(key_);
        check_contains_free_arg_keys<ArgSort>(*this, phi, arg_keys);
        auto& fdef = fun_def(key_);
        fdef = {this->that_l(), move(key_), move(phi), move(arg_keys)};

        SMT_CVERB1LN("define_fun: " << fdef << "\n");
    }

    template <typename B, typename SatSolver>
    template <typename InstF>
    Key Mixin<B, SatSolver>::
        instantiate_def_fun_tp(const Key& key_, const Keys& arg_keys, InstF inst_f)
    {
        return invoke(move(inst_f), this->that_l(), make_fun_inst(key_, arg_keys));
    }
}

////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////

namespace unsot::smt::solver::bools {
    template <typename M>
    auto&& view(M&& map, const var::Id& vid)
    {
        return FORWARD(map)[vid];
    }

    namespace aux {
        template <typename V, typename SrcVars, typename VarsT>
        Views_map<V> make_default_views_map(const SrcVars& from, VarsT& to)
        {
            const size_t size_ = from.size();
            Views_map<V> map;
            map.reserve(size_);
            for (auto& vptr : from) {
                view(map, vptr->cid()) = V(to);
            }
            return map;
        }
    }

    template <typename V, typename SrcSort, typename SolverT>
    Views_map<V> make_default_views_map(SolverT& s)
    {
        using Sort = typename V::Container::Value;
        return aux::make_default_views_map<V>(vars<SrcSort>(s), vars<Sort>(s));
    }

    template <typename V>
    void shrink_views_map(Views_map<V>& map)
    {
        erase_if(map, [](auto& p){ return p.second.empty(); });
    }
}

////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////

namespace unsot::smt::solver {
    template <typename Sort, typename ArgSort, typename SolverT,
              Req<is_bool_v<Sort> && is_bool_v<ArgSort>>>
    auto&& vars(SolverT&& solver) noexcept
    {
        return FORWARD(solver).bools();
    }

    template <typename Sort, typename SolverT, Req<is_void_v<Sort> || is_bool_v<Sort>>>
    bool contains(const SolverT& solver, const Key& key) noexcept
    {
        if constexpr (is_bool_v<Sort>) return solver.contains_bool(key);
        else return solver.contains(key);
    }

    template <typename Sort, typename SolverT, Req<is_void_v<Sort> || is_bool_v<Sort>>>
    void check_contains(const SolverT& solver, const Key& key)
    {
        if constexpr (is_bool_v<Sort>) solver.check_contains_bool(key);
        else solver.check_contains(key);
    }

    template <typename Phi, typename UnF>
    void for_each_free_arg_key(Phi&& phi, const Keys& keys, UnF f)
    {
        for_each_if_key_rec(FORWARD(phi), [&keys, &f](auto&& eptr){
            if (util::contains(keys, cast_key(eptr))) return;
            f(FORWARD(eptr));
        });
    }

    template <typename Sort, typename SolverT>
    void check_contains_free_arg_keys(const SolverT& solver,
                                      const Formula& phi, const Keys& keys)
    {
        for_each_free_arg_key(phi, keys, as_ckeys([&solver](auto& key){
            check_contains<Sort>(solver.cthat(), key);
        }));
    }
}
