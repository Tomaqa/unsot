#ifdef NO_EXPLICIT_TP_INST

#include "smt/solver.tpp"

#include "smt/solver/run.tpp"

#else

namespace TP_INST_SOLVER_NAMESPACE {
    extern template class aux::_S::Base::Crtp;
}

#endif  /// NO_EXPLICIT_TP_INST
