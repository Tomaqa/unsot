#ifdef NO_EXPLICIT_TP_INST

#include "smt/solver/online/reals.tpp"

#include "smt/solver/online/reals/run.tpp"

#else

namespace TP_INST_SOLVER_NAMESPACE {
    extern template class aux::_S::Online_reals_t::Mixin;
}

#endif  /// NO_EXPLICIT_TP_INST
