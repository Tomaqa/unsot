#pragma once

namespace unsot::smt::solver::online::reals {
    template <typename B>
    const auto& Mixin<B>::Check_sat::creal_preds_view(const var::Id& vid) const
    {
        return view(creal_preds_views_map(), vid);
    }

    template <typename B>
    auto& Mixin<B>::Check_sat::real_preds_view(const var::Id& vid) noexcept
    {
        return view(real_preds_views_map(), vid);
    }

    template <typename B>
    const auto& Mixin<B>::Check_sat::carg_real_to_preds_view(const var::Id& vid) const
    {
        return view(carg_real_to_preds_views_map(), vid);
    }

    template <typename B>
    auto& Mixin<B>::Check_sat::arg_real_to_preds_view(const var::Id& vid) noexcept
    {
        return view(arg_real_to_preds_views_map(), vid);
    }

    template <typename B>
    const auto& Mixin<B>::Check_sat::creal_to_assigners_view(const var::Id& vid) const
    {
        return view(creal_to_assigners_views_map(), vid);
    }

    template <typename B>
    auto& Mixin<B>::Check_sat::real_to_assigners_view(const var::Id& vid) noexcept
    {
        return view(real_to_assigners_views_map(), vid);
    }
}
