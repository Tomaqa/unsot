#pragma once

namespace unsot::smt::solver::online::reals::rail {
/*
    template <typename B>
    template <typename B2>
    const auto& Mixin<B>::Check_sat::Rail_mixin<B2>::ctiming_to_visits_map_entry(const var::Id& vid) const
    {
        return ctiming_to_visits_map()[vid];
    }

    template <typename B>
    template <typename B2>
    auto& Mixin<B>::Check_sat::Rail_mixin<B2>::timing_to_visits_map_entry(const var::Id& vid) noexcept
    {
        return timing_to_visits_map()[vid];
    }
*/
}

////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////

namespace unsot::smt::solver::online::reals::rail {
    template <typename B>
    template <typename B2>
    template <typename B3>
    const auto& Mixin<B>::Check_sat::Rail_mixin<B2>::Strategy_mixin<B3>::ccheck_sat_ref() const noexcept
    {
        return Rail_mixin::cast(this->ccheck_sat_ptr());
    }

    template <typename B>
    template <typename B2>
    template <typename B3>
    auto& Mixin<B>::Check_sat::Rail_mixin<B2>::Strategy_mixin<B3>::check_sat_ref() noexcept
    {
        return Rail_mixin::cast(this->check_sat_ptr());
    }
}
