#pragma once

#include "util/hash.hpp"

namespace unsot::smt::solver::online::reals::rail {
    template <typename B>
    class Mixin<B>::Check_sat : public unsot::Inherit<typename B::Check_sat, Check_sat> {
    public:
        using Inherit = unsot::Inherit<typename B::Check_sat, Check_sat>;
        using typename Inherit::This;
        using typename Inherit::Solver;
        using typename Inherit::That;

        /// `Check_sat` itself would not be compatible with other strategies like `Initial`
        template <typename B2> class Rail_mixin;

        using Rail = Rail_mixin<That>;

        using Inherit::Inherit;
        virtual ~Check_sat()                                                              = default;
        Check_sat(Check_sat&&)                                                            = default;
    };
}

namespace unsot::smt::solver::online::reals::rail {
    template <typename B>
    template <typename B2>
    class Mixin<B>::Check_sat::Rail_mixin
        : public unsot::Inherit<typename Check_sat::template Init_map_mixin<B2>, Rail_mixin<B2>> {
    public:
        using Inherit = unsot::Inherit<typename Check_sat::template Init_map_mixin<B2>, Rail_mixin<B2>>;
        using typename Inherit::This;
        using typename Inherit::Solver;
        using typename Inherit::That;

        class T_propagate;
        class T_suggest_decision;
        class Analyze_t_inconsistency;

        using Inherit::Inherit;
        Rail_mixin();
        virtual ~Rail_mixin()                                                             = default;
        Rail_mixin(Rail_mixin&&);
        Rail_mixin& operator =(Rail_mixin&&);

        void static_init();

        static String svariant_name()                                             { return "rail"; }
        String variant_name() const override                             { return svariant_name(); }
    protected:
        template <typename B3> class Strategy_mixin;

        template <typename B3> class T_suggest_decision_bmc_mixin;
/*
        struct Timing_to_visits_map_entry;
        using Timing_to_visits_map = Hash<var::Id, Timing_to_visits_map_entry>;

        const auto& ctiming_to_visits_map() const noexcept         { return _timing_to_visits_map; }
        auto& timing_to_visits_map() noexcept                      { return _timing_to_visits_map; }
        const auto& ctiming_to_visits_map_entry(const var::Id&) const;
        auto& timing_to_visits_map_entry(const var::Id&) noexcept;

        virtual bool is_timing(const var::Ptr&) const;
        virtual bool compute_is_timing(const var::Ptr&) const;

        void perform_init_impl() override;

        virtual void set_timing_to_visits_map();
    private:
        Timing_to_visits_map _timing_to_visits_map{};
*/
    };
}

namespace unsot::smt::solver::online::reals::rail {
    template <typename B>
    template <typename B2>
    template <typename B3>
    class Mixin<B>::Check_sat::Rail_mixin<B2>::Strategy_mixin
        : public unsot::Inherit<B3, Strategy_mixin<B3>> {
    public:
        using Inherit = unsot::Inherit<B3, Strategy_mixin<B3>>;
        using typename Inherit::This;
        using typename Inherit::Solver;

        using Inherit::Inherit;
        virtual ~Strategy_mixin()                                                         = default;
        Strategy_mixin(Strategy_mixin&&)                                                  = default;

        const auto& ccheck_sat_ref() const noexcept;

        static String svariant_name()                        { return Rail_mixin::svariant_name(); }
        String variant_name() const override                             { return svariant_name(); }
    protected:
        auto& check_sat_ref() noexcept;
    };

/*
    template <typename B>
    template <typename B2>
    struct Mixin<B>::Check_sat::Rail_mixin<B2>::Timing_to_visits_map_entry {
        struct Visit;
        using Visits = Vector<Visit>;

        Visits visits{};
        bool learned{};
    };

    template <typename B>
    template <typename B2>
    struct Mixin<B>::Check_sat::Rail_mixin<B2>::Timing_to_visits_map_entry::Visit {
        String to_string() const                                               { return *key_link; }

        const Key* key_link;
    };
*/
}

#include "smt/solver/online/reals/rail/check_sat/t_prop.hpp"
#include "smt/solver/online/reals/rail/check_sat/t_sugg.hpp"
#include "smt/solver/online/reals/rail/check_sat/t_learn.hpp"

#include "smt/solver/online/reals/rail/check_sat.inl"
