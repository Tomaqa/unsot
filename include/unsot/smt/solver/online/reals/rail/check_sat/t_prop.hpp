#pragma once

namespace unsot::smt::solver::online::reals::rail {
    template <typename B>
    template <typename B2>
    class Mixin<B>::Check_sat::Rail_mixin<B2>::T_propagate
        : public unsot::Inherit<Strategy_mixin<typename B2::T_propagate>, T_propagate> {
    public:
        using Inherit = unsot::Inherit<Strategy_mixin<typename B2::T_propagate>, T_propagate>;
        using typename Inherit::This;
        using typename Inherit::Solver;

        using Inherit::Inherit;
        virtual ~T_propagate()                                                            = default;
        T_propagate(T_propagate&&)                                                        = default;
    protected:
        using typename Inherit::Perform_with_result;

        Optional<Perform_with_result> try_perform_with_not_inferenced(const var::Ptr&) override;
        //+ virtual Perform_with_result perform_with_timing(const var::Ptr&);
    };
}

#include "smt/solver/online/reals/rail/check_sat/t_prop.inl"
