#pragma once

namespace unsot::smt::solver::online::reals::rail {
    template <typename B>
    template <typename B2>
    class Mixin<B>::Check_sat::Rail_mixin<B2>::Analyze_t_inconsistency
        : public unsot::Inherit<Strategy_mixin<typename B2::Analyze_t_inconsistency>, Analyze_t_inconsistency> {
    public:
        using Inherit = unsot::Inherit<Strategy_mixin<typename B2::Analyze_t_inconsistency>, Analyze_t_inconsistency>;
        using typename Inherit::This;
        using typename Inherit::Solver;

        using Inherit::Inherit;
        virtual ~Analyze_t_inconsistency()                                                = default;
        Analyze_t_inconsistency(Analyze_t_inconsistency&&)                                = default;
    protected:
        void perform_body(var::Ptr&) override;

        //+ virtual void learn_from_timing(const var::Ptr&);
    };
}

#include "smt/solver/online/reals/rail/check_sat/t_learn.inl"
