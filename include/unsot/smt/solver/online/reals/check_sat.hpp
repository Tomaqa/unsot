#pragma once

namespace unsot::smt::solver::online::reals {
    template <typename B>
    class Mixin<B>::Check_sat : public unsot::Inherit<typename B::Check_sat, Check_sat> {
    public:
        using Inherit = unsot::Inherit<typename B::Check_sat, Check_sat>;
        using typename Inherit::This;
        using typename Inherit::Solver;
        using typename Inherit::That;

        using typename Inherit::Online_conf;

        template <typename B2> class Init_map_mixin;

        using With_init_map = Init_map_mixin<That>;

        class T_propagate;
        class T_suggest_decision;
        class T_explain;
        class Analyze_t_inconsistency;

        using Inherit::Inherit;
        virtual ~Check_sat()                                                              = default;
        Check_sat(Check_sat&&)                                                            = default;
    protected:
        using Reals_view = typename Solver::Reals_view;
        using Real_preds_view = Preds_uniq_view;
        using Real_preds_views_map = Views_map<Preds_uniq_view>;
        using Real_to_preds_view = Preds_uniq_view;
        using Real_to_preds_views_map = Views_map<Preds_uniq_view>;

        const auto& creal_preds_views_map() const noexcept         { return _real_preds_views_map; }
        auto& real_preds_views_map() noexcept                      { return _real_preds_views_map; }
        const auto& creal_preds_view(const var::Id&) const;
        auto& real_preds_view(const var::Id&) noexcept;
        const auto& carg_real_to_preds_views_map() const noexcept
                                                            { return _arg_real_to_preds_views_map; }
        auto& arg_real_to_preds_views_map() noexcept        { return _arg_real_to_preds_views_map; }
        const auto& carg_real_to_preds_view(const var::Id&) const;
        auto& arg_real_to_preds_view(const var::Id&) noexcept;
        const auto& creal_to_assigners_views_map() const noexcept
                                                            { return _real_to_assigners_views_map; }
        auto& real_to_assigners_views_map() noexcept        { return _real_to_assigners_views_map; }
        const auto& creal_to_assigners_view(const var::Id&) const;
        auto& real_to_assigners_view(const var::Id&) noexcept;

        void perform_init_impl() override;
        void perform_finish_profiled() override;

        virtual void post_compute_real_pred(var::Ptr&, const var::Ptr& from = nullptr);

        typename Sat_solver::Clause
            make_depend_real_preds_clause_of(const var::Id&,
                                             var::Distance max_len = var::inf_dist) override;

        void t_backtrack_bool_impl(var::Ptr&) override;

        virtual void reset_real_pred(var::Ptr&);
        virtual void pre_reset_real_pred(var::Ptr&);
        virtual void reset_real_pred_body(var::Ptr&);
        virtual void post_reset_real_pred(var::Ptr&);

        virtual void reset_real_pred_with_assigned(var::Ptr&);
        /// Must not be called after `reset_real_pred` - the assigned would be already forgotten
        virtual void reset_assigned_of_real_pred(var::Ptr&);
    private:
        Real_preds_views_map _real_preds_views_map{};
        Real_to_preds_views_map _arg_real_to_preds_views_map{};
        Real_to_preds_views_map _real_to_assigners_views_map{};
    };
}

#include "smt/solver/online/reals/check_sat/init_map.hpp"
#include "smt/solver/online/reals/check_sat/t_prop.hpp"
#include "smt/solver/online/reals/check_sat/t_sugg.hpp"
#include "smt/solver/online/reals/check_sat/t_learn.hpp"

#include "smt/solver/online/reals/check_sat.inl"
