#pragma once

namespace unsot::smt::solver::online::reals {
    template <typename B>
    class Mixin<B>::Check_sat::T_explain
        : public unsot::Inherit<typename B::Check_sat::T_explain, T_explain> {
    public:
        using Inherit = unsot::Inherit<typename B::Check_sat::T_explain, T_explain>;
        using typename Inherit::This;
        using typename Inherit::Solver;

        using Inherit::Inherit;
        virtual ~T_explain()                                                              = default;
        T_explain(T_explain&&)                                                            = default;
    protected:
        typename Sat_solver::Clause explain_impl(const var::Ptr&, var::Distance max_len) override;
    };

    template <typename B>
    class Mixin<B>::Check_sat::Analyze_t_inconsistency
        : public unsot::Inherit<typename B::Check_sat::Analyze_t_inconsistency, Analyze_t_inconsistency> {
    public:
        using Inherit = unsot::Inherit<typename B::Check_sat::Analyze_t_inconsistency, Analyze_t_inconsistency>;
        using typename Inherit::This;
        using typename Inherit::Solver;
        using typename Inherit::That;

        template <typename B2> class Car_mixin;

        using Car = Car_mixin<That>;

        using Inherit::Inherit;
        virtual ~Analyze_t_inconsistency()                                                = default;
        Analyze_t_inconsistency(Analyze_t_inconsistency&&)                                = default;
    protected:
        void backtrack(var::Ptr&, typename Sat_solver::Clause& confl) override;

        typename Sat_solver::Clause explain_impl(const var::Ptr&, var::Distance max_len) override;
    };
}

#include "smt/solver/online/reals/check_sat/t_learn/car.hpp"

#include "smt/solver/online/reals/check_sat/t_learn.inl"
