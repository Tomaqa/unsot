#pragma once

namespace unsot::smt::solver::online::reals {
    template <typename B>
    template <typename B2>
    const auto& Mixin<B>::Check_sat::Init_map_mixin<B2>::cinitial_real_to_assigners_view(const var::Id& vid) const
    {
        return view(cinitial_real_to_assigners_views_map(), vid);
    }

    template <typename B>
    template <typename B2>
    auto& Mixin<B>::Check_sat::Init_map_mixin<B2>::initial_real_to_assigners_view(const var::Id& vid) noexcept
    {
        return view(initial_real_to_assigners_views_map(), vid);
    }
}
