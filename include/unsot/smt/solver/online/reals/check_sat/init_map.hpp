#pragma once

#include "util/hash.hpp"

namespace unsot::smt::solver::online::reals {
    template <typename B>
    template <typename B2>
    class Mixin<B>::Check_sat::Init_map_mixin : public unsot::Inherit<B2, Init_map_mixin<B2>> {
    public:
        using Inherit = unsot::Inherit<B2, Init_map_mixin<B2>>;
        using typename Inherit::This;
        using typename Inherit::Solver;

        class T_suggest_decision;

        using Inherit::Inherit;
        virtual ~Init_map_mixin()                                                         = default;
        Init_map_mixin(Init_map_mixin&&)                                                  = default;
    protected:
        using typename Inherit::Real_to_preds_view;
        using typename Inherit::Real_to_preds_views_map;

        const auto& cinitial_real_to_assigners_views_map() const noexcept
                                                    { return _initial_real_to_assigners_views_map; }
        auto& initial_real_to_assigners_views_map() noexcept
                                                    { return _initial_real_to_assigners_views_map; }
        const auto& cinitial_real_to_assigners_view(const var::Id&) const;
        auto& initial_real_to_assigners_view(const var::Id&) noexcept;

        virtual bool is_initial_real_var(const var::Id&) const noexcept;

        void perform_init_impl() override;
    private:
        Real_to_preds_views_map _initial_real_to_assigners_views_map{};
    };
}

#include "smt/solver/online/reals/check_sat/init_map/t_sugg.hpp"

#include "smt/solver/online/reals/check_sat/init_map.inl"
