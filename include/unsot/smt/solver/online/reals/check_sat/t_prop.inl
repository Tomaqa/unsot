#pragma once

namespace unsot::smt::solver::online::reals {
    template <typename B>
    constexpr typename Mixin<B>::Check_sat::T_propagate::Perform_with_result&
    Mixin<B>::Check_sat::T_propagate::Perform_with_result::operator +=(Perform_with_result rhs) noexcept
    {
        inconsistent_count += rhs.inconsistent_count;
        propagation_count += rhs.propagation_count;
        return *this;
    }
}
