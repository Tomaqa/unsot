#pragma once

namespace unsot::smt::solver::online::reals {
    template <typename B>
    template <typename B2>
    const auto& Mixin<B>::Check_sat::T_suggest_decision::Bmc_mixin<B2>::cvar_info(const var::Id& vid) const
    {
        return cvar_info_map()[vid];
    }

    template <typename B>
    template <typename B2>
    auto& Mixin<B>::Check_sat::T_suggest_decision::Bmc_mixin<B2>::var_info(const var::Id& vid) noexcept
    {
        return var_info_map()[vid];
    }

    template <typename B>
    template <typename B2>
    const var::Id& Mixin<B>::Check_sat::T_suggest_decision::Bmc_mixin<B2>::ccurrent_bool_id() const
    {
        return this->ccurrent_state().var_id;
    }
}
