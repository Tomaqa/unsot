#pragma once

#include "smt/solver/online/check_sat.hpp"

#include "util/hash.hpp"

namespace unsot::smt::solver::online::reals {
    template <typename B>
    template <typename B2>
    class Mixin<B>::Check_sat::T_suggest_decision::Bmc_mixin :
        public unsot::Inherit<typename Mixin::Online_bools_t::Check_sat::T_suggest_decision::template Stack_mixin<typename Mixin::Online_t::Check_sat::T_suggest_decision::template Stack_mixin<B2, Bmc_state>>, Bmc_mixin<B2>> {
    public:
        using Inherit =
            unsot::Inherit<typename Mixin::Online_bools_t::Check_sat::T_suggest_decision::template Stack_mixin<typename Mixin::Online_t::Check_sat::T_suggest_decision::template Stack_mixin<B2, Bmc_state>>, Bmc_mixin<B2>>;
        using typename Inherit::This;
        using typename Inherit::Solver;

        using typename Inherit::Ret;

        template <typename B3> class Sort_mixin;

        using typename Inherit::Bmc;
        using Sorted = Sort_mixin<Bmc>;

        using Inherit::Inherit;
        virtual ~Bmc_mixin()                                                              = default;
        Bmc_mixin(Bmc_mixin&&)                                                            = default;

        void post_check_sat_init() override;

        static String svariant_name()                                              { return "bmc"; }
        String variant_name() const override                             { return svariant_name(); }
    protected:
        using Bool_view = bools::Uniq_view;
        using Bool_views = Vector<Bool_view>;

        struct Var_info;
        using Var_info_map = Hash<var::Id, Var_info>;

        using typename Inherit::State;

        const auto& cbool_views() const noexcept                             { return _bool_views; }
        auto& bool_views() noexcept                                          { return _bool_views; }
        const auto& cbool_view(Idx idx) const noexcept                { return cbool_views()[idx]; }
        auto& bool_view(Idx idx) noexcept                              { return bool_views()[idx]; }

        const auto& cvar_info_map() const noexcept                         { return _var_info_map; }
        auto& var_info_map() noexcept                                      { return _var_info_map; }
        const auto& cvar_info(const var::Id&) const;
        auto& var_info(const var::Id&) noexcept;
        const auto& cmax_bmc_step() const noexcept                         { return _max_bmc_step; }
        auto& max_bmc_step() noexcept                                      { return _max_bmc_step; }

        const var::Id& ccurrent_bool_id() const override;

        virtual void init_var_info_map();
        virtual Var_info compute_var_info(const var::Ptr&);

        virtual Idx compute_bmc_step_of(const var::Ptr&);

        virtual void set_bool_views();
        virtual void sort_bool_views();
        virtual void sort_bool_view(Bool_view&);
        virtual Flag sort_bool_view_compare(const var::Id&, const var::Id&) const;
        virtual Flag sort_bool_view_compare_prior(const var::Id&, const var::Id&) const;
        virtual Flag sort_bool_view_compare_rest(const var::Ptr&, const var::Ptr&) const;

        Optional<Ret> perform_body() override;

        Flag perform_value_with(const var::Ptr&) const override;

        void backtrack_impl() override;

        bool maybe_backtrack_from_bool_head(const var::Ptr&) const override;

        bool is_ok(const State&) const noexcept override;

        String state_to_string(const State&) const override;
    private:
        Bool_views _bool_views{};

        Var_info_map _var_info_map{};
        Idx _max_bmc_step{};
    };

    template <typename B>
    struct Mixin<B>::Check_sat::T_suggest_decision::Bmc_state {
        Idx bmc_step;
        Idx var_idx;
        var::Id var_id;
    };
}

namespace unsot::smt::solver::online::reals {
    template <typename B>
    template <typename B2>
    struct Mixin<B>::Check_sat::T_suggest_decision::Bmc_mixin<B2>::Var_info {
        Idx bmc_step;
        Flag suggest_value{};
    };
}

#include "smt/solver/online/reals/check_sat/t_sugg/bmc/sort.hpp"

#include "smt/solver/online/reals/check_sat/t_sugg/bmc.inl"
