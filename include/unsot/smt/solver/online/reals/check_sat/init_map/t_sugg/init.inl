#pragma once

namespace unsot::smt::solver::online::reals {
    template <typename B>
    template <typename B2>
    template <typename B3>
    const var::Id& Mixin<B>::Check_sat::Init_map_mixin<B2>::T_suggest_decision::Initial_mixin<B3>::ccurrent_bool_id() const
    {
        return this->ccurrent_state().assigner_id;
    }

    template <typename B>
    template <typename B2>
    template <typename B3>
    const auto& Mixin<B>::Check_sat::Init_map_mixin<B2>::T_suggest_decision::Initial_mixin<B3>::ccurrent_initial_real_var_ptr() const noexcept
    {
        return cinitial_reals_view()[this->ccurrent_state().init_real_var_idx];
    }
}
