#pragma once

#include "util/string/alg.hpp"

namespace unsot::smt::solver::online::reals {
    template <typename B>
    template <typename B2>
    const auto& Mixin<B>::Check_sat::Init_map_mixin<B2>::T_suggest_decision::ccheck_sat_ref() const noexcept
    {
        return Init_map_mixin::cast(this->ccheck_sat_ptr());
    }

    template <typename B>
    template <typename B2>
    auto& Mixin<B>::Check_sat::Init_map_mixin<B2>::T_suggest_decision::check_sat_ref() noexcept
    {
        return Init_map_mixin::cast(this->check_sat_ptr());
    }
}

////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////

namespace unsot::smt::solver::online::reals {
    template <typename B>
    template <typename B2>
    template <typename B3>
    String Mixin<B>::Check_sat::Init_map_mixin<B2>::T_suggest_decision::Bmc_sort_mixin<B3>::svariant_name()
    {
        String str = Inherit::svariant_name();
        if (starts_with(str, Inherit::Bmc_mixin::svariant_name())) str += "_init";
        return str;
    }
}
