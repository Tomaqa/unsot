#pragma once

namespace unsot::smt::solver::online::reals {
    template <typename B>
    template <typename B2>
    template <typename B3>
    class Mixin<B>::Check_sat::Init_map_mixin<B2>::T_suggest_decision::Initial_mixin
        : public unsot::Inherit<typename Mixin::Online_bools_t::Check_sat::T_suggest_decision::template Stack_mixin<typename Mixin::Online_t::Check_sat::T_suggest_decision::template Stack_mixin<B3, Initial_state>>, Initial_mixin<B3>> {
    public:
        using Inherit = unsot::Inherit<typename Mixin::Online_bools_t::Check_sat::T_suggest_decision::template Stack_mixin<typename Mixin::Online_t::Check_sat::T_suggest_decision::template Stack_mixin<B3, Initial_state>>, Initial_mixin<B3>>;
        using typename Inherit::This;
        using typename Inherit::Solver;

        using typename Inherit::Ret;

        using Inherit::Inherit;
        virtual ~Initial_mixin()                                                          = default;
        Initial_mixin(Initial_mixin&&)                                                    = default;

        void post_check_sat_init() override;

        void post_check_sat_restart() override;

        void restart() override;

        static String svariant_name()                                             { return "init"; }
        String variant_name() const override                             { return svariant_name(); }
    protected:
        using Reals_view = typename Check_sat::Reals_view;
        using Real_to_preds_view = typename Check_sat::Real_to_preds_view;

        using typename Inherit::State;

        const auto& cinitial_real_assigners_view() const noexcept
                                                            { return _initial_real_assigners_view; }
        auto& initial_real_assigners_view() noexcept        { return _initial_real_assigners_view; }
        const auto& cinitial_reals_view() const noexcept             { return _initial_reals_view; }
        auto& initial_reals_view() noexcept                          { return _initial_reals_view; }

        const auto& cnext_state() const noexcept                             { return _next_state; }
        auto& next_state() noexcept                                          { return _next_state; }

        virtual bool is_initial_real_assigner(const var::Id&) const noexcept;

        bool invalid_current_bool() const noexcept override;
        const var::Id& ccurrent_bool_id() const override;
        const auto& ccurrent_initial_real_var_ptr() const noexcept;

        void rm_current_state() noexcept override;

        virtual void order_initial_real_views();

        Optional<Ret> perform_body() override;
        virtual Optional<Ret>
            try_perform_with_initial_real_var(const var::Id&, const Idx, Idx ass_idx);

        void backtrack() override;
        void backtrack_impl() override;

        bool maybe_backtrack_from_bool_head(const var::Ptr&) const override;
        bool maybe_backtrack_from_bool_body(const var::Ptr&) override;

        bool is_ok(const State&) const noexcept override;

        String state_to_string(const State&) const override;
        String current_state_to_string_extra() const override;
    private:
        Real_to_preds_view _initial_real_assigners_view{this->solver().bools()};
        Reals_view _initial_reals_view{};

        Optional<State> _next_state{};
    };

    template <typename B>
    template <typename B2>
    struct Mixin<B>::Check_sat::Init_map_mixin<B2>::T_suggest_decision::Initial_state {
        Idx init_real_var_idx;
        Idx assigner_idx;
        var::Id init_real_var_id;
        var::Id assigner_id;
        //+ int counter{};
    };
}

#include "smt/solver/online/reals/check_sat/init_map/t_sugg/init.inl"
