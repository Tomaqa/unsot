#pragma once

#include "smt/solver/online/reals.hpp"

namespace unsot::smt::solver::online::reals::rail {
    template <typename B> class Mixin;

    template <typename S, typename SatSolver, var::Type typeV = var::Type::assignee>
        using Crtp = Mixin<reals::Crtp<S, SatSolver, typeV>>;
}

namespace unsot::smt::solver::online::reals::rail {
    template <typename B>
    class Mixin : public Inherit<B, Mixin<B>> {
    public:
        using Inherit = unsot::Inherit<B, Mixin<B>>;
        using typename Inherit::This;
        using typename Inherit::That;
        using Rail_t = This;

        class Check_sat;

        using Inherit::Inherit;
        Mixin();
        virtual ~Mixin()                                                                  = default;
        Mixin(const Mixin&)                                                                = delete;
        Mixin& operator =(const Mixin&)                                                    = delete;
        Mixin(Mixin&&)                                                                    = default;
        Mixin& operator =(Mixin&&)                                                        = default;

        void static_init();

        template <typename T = void> void set_check_sat_strategy_tp(bool force = no_force);
    protected:
        void set_check_sat_strategy_impl(const String& name, bool force = no_force) override;
    };
}

#include "smt/solver/online/reals/rail/check_sat.hpp"

#include "smt/solver/online/reals/rail.inl"
