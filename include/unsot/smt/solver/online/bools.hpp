#pragma once

#include "smt/solver/bools.hpp"
#include "smt/solver/online.hpp"

namespace unsot::smt::solver::online::bools {
    using namespace solver::bools;

    template <typename B> class Mixin;

    template <typename S, typename SatSolver>
        using Crtp = Mixin<online::Mixin<solver::bools::Crtp<S, SatSolver>>>;
}

namespace unsot::smt::solver::online::bools {
    template <typename B>
    class Mixin : public Inherit<B, Mixin<B>> {
    public:
        using Inherit = unsot::Inherit<B, Mixin<B>>;
        using typename Inherit::This;
        using typename Inherit::That;
        using Online_bools_t = This;

        class Check_sat;

        class Run;

        using typename Inherit::Sat_solver;

        using Inherit::Inherit;
        Mixin();
        virtual ~Mixin()                                                                  = default;
        Mixin(const Mixin&)                                                                = delete;
        Mixin& operator =(const Mixin&)                                                    = delete;
        Mixin(Mixin&&)                                                                    = default;
        Mixin& operator =(Mixin&&)                                                        = default;

        template <typename T = void> void set_check_sat_strategy_tp(bool force = no_force);

        void set_t_learn_strategy(const String& name, bool force = no_force);
        void set_t_explain_strategy(const String& name, bool force = no_force);
        void set_analyze_t_inconsistency_strategy(const String& name, bool force = no_force);
    protected:
        void lset_check_sat_strategy();

        bool valid_option_value(const Option&, const Option_value&) const override;
        void apply_option(const Option&) override;

        void set_all_strategies_impl(const String& name, bool force = no_force) override;

        void set_t_suggest_decision_strategy_impl(const String& name, bool force = no_force) override;
        virtual void set_t_learn_strategy_impl(const String& name, bool force = no_force);
        virtual void set_t_explain_strategy_impl(const String& name, bool force = no_force);
        virtual void set_analyze_t_inconsistency_strategy_impl(const String& name, bool force = no_force);
    };
}

#include "smt/solver/online/bools/check_sat.hpp"

#include "smt/solver/online/bools.inl"
