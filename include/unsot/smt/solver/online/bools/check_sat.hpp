#pragma once

#include "util/hash.hpp"

namespace unsot::smt::solver::online::bools {
    template <typename B>
    class Mixin<B>::Check_sat : public unsot::Inherit<typename B::Check_sat, Check_sat> {
    public:
        using Inherit = unsot::Inherit<typename B::Check_sat, Check_sat>;
        using typename Inherit::This;
        using typename Inherit::Solver;

        using typename Inherit::Online_conf;

        class T_propagate;
        class T_suggest_decision;
        class T_explain;
        class Analyze_t_inconsistency;

        using Inherit::Inherit;
        // Necessary to avoid "incomplete type" error in GCC
        Check_sat()                                                                        = delete;
        virtual ~Check_sat()                                                              = default;
        Check_sat(Check_sat&&)                                                            = default;
        Check_sat(Solver&);

        template <typename T = void> void set_t_explain_strategy_tp(bool force = no_force);
        template <typename T = void> void set_analyze_t_inconsistency_strategy_tp(bool force = no_force);

        const auto& ct_explain_ref() const noexcept;
        auto& t_explain_ref() noexcept;

        int decision_level() const noexcept override;
        int cdecision_level_of(const var::Id&) const;

        bool tracked(const var::Id&) const noexcept;
        virtual bool tracked(const var::Ptr&) const noexcept;
        virtual bool decided(const var::Id&) const noexcept;
        bool decided(const var::Ptr&) const noexcept;
        bool decided_at(const var::Id&, int level) const noexcept;
        bool decided_at(const var::Ptr&, int level) const noexcept;

        virtual bool tracked_before(const var::Id&, const var::Id&) const;
        virtual bool tracked_at_same_level_before(const var::Id&, int level, const var::Id&) const;
        bool tracked_at_same_level_before(const var::Id&, const var::Id&) const;

        const var::Id& decision_bool_id_at(int level) const;
        const var::Ptr& cdecision_bool_ptr_at(int level) const;
        const var::Id& current_decision_bool_id() const;
        const var::Ptr& ccurrent_decision_bool_ptr() const;

        void notice(typename Sat_solver::Lit) override;

        void notice_new_decision_level() override;

        typename Online_conf::T_explain_result t_explain(typename Sat_solver::Lit) override;

        void notice_backtrack() override;
        void notice_restart() override;

        String profiling_to_string(Duration total) const override;
    protected:
        //+ only because of `T_learn::unknown_strategy_msg`
        friend Mixin;

        struct T_explain_conf;
        struct Analyze_t_inconsistency_conf;

        template <typename B2> class Strategy_mixin;

        class T_learn_base;
        /// Base class for `T_explain` and `Analyze_t_inconsistency`
        //+ since it is templated, later it cannot be "specialized" for both explain and analyze
        template <typename ConfT> class T_learn;

        using typename Inherit::Strategy_ptr;
        using T_explain_ptr = Strategy_ptr;
        using Analyze_t_inconsistency_ptr = Strategy_ptr;

        using Tracked_bools_view = View;
        using Tracked_bool_views = Vector<Tracked_bools_view>;

        using Decision_levels_map = Hash<var::Id, int>;

        const auto& ct_explain_ptr() const noexcept                       { return _t_explain_ptr; }
        auto& t_explain_ptr() noexcept                                    { return _t_explain_ptr; }
        const auto& canalyze_t_inconsistency_ptr() const noexcept
                                                            { return _analyze_t_inconsistency_ptr; }
        auto& analyze_t_inconsistency_ptr() noexcept        { return _analyze_t_inconsistency_ptr; }
        const auto& canalyze_t_inconsistency_ref() const noexcept;
        auto& analyze_t_inconsistency_ref() noexcept;

        const auto& ctracked_bool_views() const noexcept             { return _tracked_bool_views; }
        auto& tracked_bool_views() noexcept                          { return _tracked_bool_views; }
        const auto& ccurrent_tracked_bools_view() const noexcept;
        auto& current_tracked_bools_view() noexcept;

        const auto& cdecision_levels_map() const noexcept           { return _decision_levels_map; }
        auto& decision_levels_map() noexcept                        { return _decision_levels_map; }
        int& decision_level_of(const var::Id&);
        int cdecision_level_of(const typename Sat_solver::Lit&) const;

        var::Ptr& decision_bool_ptr_at(int level);
        var::Ptr& current_decision_bool_ptr();

        virtual void notice(var::Ptr&, Bool);
        virtual void notice_impl(var::Ptr&, Bool);

        virtual void track(const var::Ptr&);

        void perform_init_profiled() override;
        void perform_init_impl() override;
        Sat perform_body() override;

        template <typename ItT> int max_decision_level_of(ItT first, ItT last) const;
        int max_decision_level_of(const typename Sat_solver::Clause&) const;

        virtual void t_backtrack_to(int level);

        virtual void t_backtrack_bool(var::Ptr&);
        virtual void t_backtrack_bool_impl(var::Ptr&);
    private:
        /// Accessing a view other than the top one can cause inconsistency
        /// between SAT and SMT solver ...
        const auto& ctracked_bools_view(int level) const noexcept;
        auto& tracked_bools_view(int level) noexcept;

        virtual void track_at(const var::Ptr&, int level);

        T_explain_ptr _t_explain_ptr{this->solver().template new_strategy<T_explain>()};
        Analyze_t_inconsistency_ptr
            _analyze_t_inconsistency_ptr{this->solver().template new_strategy<Analyze_t_inconsistency>()};

        Tracked_bool_views _tracked_bool_views{};

        Decision_levels_map _decision_levels_map{};
    };
}

namespace unsot::smt::solver::online::bools {
    template <typename B>
    struct Mixin<B>::Check_sat::T_explain_conf {
        template <typename T> using Type_tp = typename T::Check_sat::T_explain;

        using Ret = typename Online_conf::T_explain_result;
    };

    template <typename B>
    struct Mixin<B>::Check_sat::Analyze_t_inconsistency_conf {
        template <typename T> using Type_tp = typename T::Check_sat::Analyze_t_inconsistency;

        using Ret = void;
    };

    template <typename B>
    template <typename B2>
    class Mixin<B>::Check_sat::Strategy_mixin : public unsot::Inherit<B2, Strategy_mixin<B2>> {
    public:
        using Inherit = unsot::Inherit<B2, Strategy_mixin<B2>>;
        using typename Inherit::This;
        using typename Inherit::Solver;

        using Inherit::Inherit;
        virtual ~Strategy_mixin()                                                         = default;
        Strategy_mixin(Strategy_mixin&&)                                                  = default;

        virtual void pre_check_sat_notice(var::Ptr&)                                             { }
        virtual void post_check_sat_notice(var::Ptr&)                                            { }

        virtual void pre_check_sat_t_backtrack_bool(var::Ptr&)                                   { }
        virtual void post_check_sat_t_backtrack_bool(var::Ptr&)                                  { }
    };
}

#include "smt/solver/online/bools/check_sat/t_prop.hpp"
#include "smt/solver/online/bools/check_sat/t_sugg.hpp"
#include "smt/solver/online/bools/check_sat/t_learn.hpp"

#include "smt/solver/online/bools/check_sat.inl"
