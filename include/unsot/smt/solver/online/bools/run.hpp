#pragma once

#include "smt/solver/online/run.hpp"
#include "smt/solver/online/bools.hpp"

namespace unsot::smt::solver::online::bools {
    template <typename B>
    class Mixin<B>::Run : public unsot::Inherit<typename B::Run> {
    public:
        using Inherit = unsot::Inherit<typename B::Run>;
        using typename Inherit::Solver;
        using typename Inherit::That;

        using Inherit::Inherit;
        virtual ~Run()                                              = default;
    };
}
