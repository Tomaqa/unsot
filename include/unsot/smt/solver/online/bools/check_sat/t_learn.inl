#pragma once

#include "util/alg.hpp"

namespace unsot::smt::solver::online::bools {
    template <typename B>
    constexpr bool Mixin<B>::Check_sat::T_learn_base::sorting_decision_levels() noexcept
    {
        return order_decision_levels_v == Order_decision_levels::asc
            || order_decision_levels_v == Order_decision_levels::desc;
    }
}
