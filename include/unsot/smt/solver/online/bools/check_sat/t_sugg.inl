#pragma once

namespace unsot::smt::solver::online::bools {
    template <typename B>
    const var::Ptr& Mixin<B>::Check_sat::T_suggest_decision::ccurrent_bool_ptr() const
    {
        return this->csolver().cbools().cptr(ccurrent_bool_id());
    }

    template <typename B>
    const var::Id& Mixin<B>::Check_sat::T_suggest_decision::ccurrent_bool_id() const
    {
        return ccurrent_bool_ptr()->cid();
    }
}
