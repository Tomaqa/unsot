#pragma once

namespace unsot::smt::solver::online::bools {
    template <typename B>
    template <typename B2>
    class Mixin<B>::Check_sat::T_suggest_decision::Order_mixin
        : public unsot::Inherit<B2, Order_mixin<B2>> {
    public:
        using Inherit = unsot::Inherit<B2, Order_mixin<B2>>;
        using typename Inherit::This;
        using typename Inherit::Solver;

        using typename Inherit::Ret;

        using Inherit::Inherit;
        virtual ~Order_mixin()                                        = default;
        Order_mixin(Order_mixin&&)                                    = default;

        void post_check_sat_init() override;

        void restart() override;

        static String svariant_name()                                            { return "order"; }
        String variant_name() const override                             { return svariant_name(); }
    protected:
        const var::Id& ccurrent_bool_id() const override    { return _current_bool_id; }
        auto& current_bool_id() noexcept                    { return _current_bool_id; }

        bool invalid_current_state() const noexcept override;

        Optional<Ret> perform_body() override;

        void perform_print_with(const var::Ptr&, const Flag& val) const override;

        void backtrack_impl() override;
    private:
        var::Id _current_bool_id{var::invalid_id};
    };
}

#include "smt/solver/online/bools/check_sat/t_sugg/order.inl"
