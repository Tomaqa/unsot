#pragma once

namespace unsot::smt::solver::online {
    template <typename B>
    class Mixin<B>::Check_sat::T_propagate
        : public unsot::Inherit<Strategy_mixin<typename Mixin::template Strategy<T_propagate_conf>>,
                                T_propagate> {
    public:
        using Inherit =
            unsot::Inherit<Strategy_mixin<typename Mixin::template Strategy<T_propagate_conf>>,
                           T_propagate>;
        using typename Inherit::This;
        using typename Inherit::Solver;
        using typename Inherit::That;

        static_assert(is_same_v<That, typename Check_sat::That::T_propagate>);

        using typename Inherit::Strategy_conf;

        static_assert(is_same_v<typename Strategy_conf::Ret,
                      typename Online_conf::T_propagate_result>);

        using Inherit::Inherit;
        virtual ~T_propagate()                                                            = default;
        T_propagate(T_propagate&&)                                                        = default;

        virtual bool empty() const noexcept                                                     = 0;

        typename Strategy_conf::Ret perform() override;

        void post_check_sat_restart() override;
    protected:
        static String name_impl()                                          { return "t-propagate"; }
    };
}

#include "smt/solver/online/check_sat/t_prop.inl"
