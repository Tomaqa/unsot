#pragma once

namespace unsot::smt::solver::online {
    template <typename B>
    template <typename B2, typename StateT>
    const auto& Mixin<B>::Check_sat::T_suggest_decision::Stack_mixin<B2, StateT>::ccurrent_state() const noexcept
    {
        assert(this->valid_current_state());
        return cstate_stack().back();
    }

    template <typename B>
    template <typename B2, typename StateT>
    auto& Mixin<B>::Check_sat::T_suggest_decision::Stack_mixin<B2, StateT>::current_state() noexcept
    {
        assert(this->valid_current_state());
        return state_stack().back();
    }

    template <typename B>
    template <typename B2, typename StateT>
    bool Mixin<B>::Check_sat::T_suggest_decision::Stack_mixin<B2, StateT>::invalid_current_state() const noexcept
    {
        return empty(cstate_stack());
    }

    template <typename B>
    template <typename B2, typename StateT>
    void Mixin<B>::Check_sat::T_suggest_decision::Stack_mixin<B2, StateT>::add_state(State state) noexcept
    {
        state_stack().push_back(move(state));
    }

    template <typename B>
    template <typename B2, typename StateT>
    void Mixin<B>::Check_sat::T_suggest_decision::Stack_mixin<B2, StateT>::rm_current_state() noexcept
    {
        assert(this->valid_current_state());
        return state_stack().pop_back();
    }
}
