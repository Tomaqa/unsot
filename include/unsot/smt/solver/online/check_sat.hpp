#pragma once

#include "smt/solver.hpp"
#include "smt/sat/solver/online.hpp"

#include "util/optional.hpp"

namespace unsot::smt::solver::online {
    template <typename B>
    class Mixin<B>::Check_sat
        : public unsot::Inherit<typename B::Check_sat, Check_sat>,
          public sat::solver::online::Smt_check_sat_base<typename Mixin::Sat_solver::Online_conf> {
    public:
        using Inherit = unsot::Inherit<typename B::Check_sat, Check_sat>;
        using Online_check_sat_base =
            sat::solver::online::Smt_check_sat_base<typename Mixin::Sat_solver::Online_conf>;
        using typename Inherit::This;
        using typename Inherit::Solver;

        using Sat_solver = typename Solver::Sat_solver;
        using Online_conf = typename Sat_solver::Online_conf;

        static_assert(is_same_v<typename Sat_solver::Online_conf,
                                typename Online_check_sat_base::Online_conf>);
        static_assert(is_same_v<typename Sat_solver::Var_id, typename Online_conf::Sat_var_id>);
        static_assert(is_same_v<typename Sat_solver::Lit, typename Online_conf::Sat_lit>);

        class T_propagate;
        class T_suggest_decision;

        using Inherit::Inherit;
        // Necessary to avoid "incomplete type" error in GCC
        Check_sat()                                                                        = delete;
        virtual ~Check_sat()                                                              = default;
        Check_sat(Check_sat&&)                                                            = default;

        template <typename T = void> void set_t_propagate_strategy_tp(bool force = no_force);
        template <typename T = void> void set_t_suggest_decision_strategy_tp(bool force = no_force);

        const auto& ct_propagate_ref() const noexcept;
        auto& t_propagate_ref() noexcept;
        const auto& ct_suggest_decision_ref() const noexcept;
        auto& t_suggest_decision_ref() noexcept;

        const auto& ct_backtrack_duration() const noexcept         { return _t_backtrack_duration; }
        const auto& ct_backtrack_count() const noexcept               { return _t_backtrack_count; }

        typename Online_conf::T_propagate_result t_propagate_exec() override;

        Optional<typename Online_conf::T_decision_suggestion> t_suggest_decision() override;

        void notice_backtrack_to(int level) override;
        void notice_restart() override;

        String profiling_to_string(Duration total) const override;
    protected:
        struct T_propagate_conf;
        struct T_suggest_decision_conf;

        template <typename B2, typename... Args> class Strategy_mixin;

        using Strategy_ptr = typename Solver::Strategy_ptr;
        using T_propagate_ptr = Strategy_ptr;
        using T_suggest_decision_ptr = Strategy_ptr;

        const auto& ct_propagate_ptr() const noexcept                   { return _t_propagate_ptr; }
        auto& t_propagate_ptr() noexcept                                { return _t_propagate_ptr; }
        const auto& ct_suggest_decision_ptr() const noexcept     { return _t_suggest_decision_ptr; }
        auto& t_suggest_decision_ptr() noexcept                  { return _t_suggest_decision_ptr; }

        bool crestarting() const noexcept                                    { return _restarting; }
        bool& restarting() noexcept                                          { return _restarting; }

        auto& t_backtrack_duration() noexcept                      { return _t_backtrack_duration; }
        auto& t_backtrack_count() noexcept                            { return _t_backtrack_count; }

        void perform_init_profiled() override;

        virtual void notice_restart_impl();
    private:
        T_propagate_ptr _t_propagate_ptr{this->solver().template new_strategy<T_propagate>()};
        T_suggest_decision_ptr
            _t_suggest_decision_ptr{this->solver().template new_strategy<T_suggest_decision>()};

        bool _restarting{};

        Duration _t_backtrack_duration{};
        long _t_backtrack_count{};
    };
}

namespace unsot::smt::solver::online {
    template <typename B>
    struct Mixin<B>::Check_sat::T_propagate_conf {
        template <typename T> using Type_tp = typename T::Check_sat::T_propagate;

        using Ret = typename Online_conf::T_propagate_result;
    };

    template <typename B>
    struct Mixin<B>::Check_sat::T_suggest_decision_conf {
        template <typename T> using Type_tp = typename T::Check_sat::T_suggest_decision;

        using Ret = Optional<typename Online_conf::T_decision_suggestion>;
    };

    template <typename B>
    template <typename B2, typename... Args>
    class Mixin<B>::Check_sat::Strategy_mixin
        : public unsot::Inherit<B2, Mixin::Check_sat::Strategy_mixin<B2, Args...>> {
    public:
        using Inherit = unsot::Inherit<B2, Mixin::Check_sat::Strategy_mixin<B2, Args...>>;
        using typename Inherit::This;
        using typename Inherit::Solver;

        using typename Inherit::Strategy_conf;

        static_assert(is_same_v<typename Solver::Check_sat, typename Check_sat::That>);

        using Inherit::Inherit;
        virtual ~Strategy_mixin()                                                         = default;
        Strategy_mixin(Strategy_mixin&&)                                                  = default;

        const auto& ccheck_sat_ptr() const noexcept     { return this->csolver().ccheck_sat_ptr(); }
        const auto& ccheck_sat_ref() const noexcept     { return this->csolver().ccheck_sat_ref(); }

        bool cprogressed() const noexcept                                    { return _progressed; }
        const auto& cprogressed_count() const noexcept                 { return _progressed_count; }

        virtual void pre_check_sat_init();
        virtual void post_check_sat_init();

        virtual void pre_check_sat_restart()                                                     { }
        virtual void post_check_sat_restart();

        virtual void restart();

        String profiling_to_string(Duration total) const override;
    protected:
        auto& check_sat_ptr() noexcept                    { return this->solver().check_sat_ptr(); }
        auto& check_sat_ref() noexcept                    { return this->solver().check_sat_ref(); }

        bool& progressed() noexcept                                          { return _progressed; }
        auto& progressed_count() noexcept                              { return _progressed_count; }

        void perform_init(Args&...) override;
        void perform_finish() override;

        static String profiling_total_name_impl()                      { return Check_sat::name(); }
    private:
        bool _progressed{};
        long _progressed_count{};
    };
}

#include "smt/solver/online/check_sat/t_prop.hpp"
#include "smt/solver/online/check_sat/t_sugg.hpp"

#include "smt/solver/online/check_sat.inl"
