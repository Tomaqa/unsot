#pragma once

#include "smt/solver.hpp"

namespace unsot::smt::solver::online {
    template <typename B> class Mixin;

    template <typename S> using Crtp = Mixin<solver::Crtp<S>>;
}

namespace unsot::smt::solver::online {
    template <typename B>
    class Mixin : public Inherit<B, Mixin<B>> {
    public:
        using Inherit = unsot::Inherit<B, Mixin<B>>;
        using typename Inherit::This;
        using typename Inherit::That;
        using Online_t = This;

        class Check_sat;

        class Run;

        using Inherit::Inherit;
        Mixin();
        virtual ~Mixin()                                                                  = default;
        Mixin(const Mixin&)                                                                = delete;
        Mixin& operator =(const Mixin&)                                                    = delete;
        Mixin(Mixin&&)                                                                    = default;
        Mixin& operator =(Mixin&&)                                                        = default;

        /// It might require to also change the check-sat strategy, hence here in the solver
        void set_t_propagate_strategy(const String& name, bool force = no_force);
        void set_t_suggest_decision_strategy(const String& name, bool force = no_force);
    protected:
        bool valid_option_value(const Option&, const Option_value&) const override;
        void apply_option(const Option&) override;

        void set_all_strategies_impl(const String& name, bool force = no_force) override;

        virtual void set_t_propagate_strategy_impl(const String& name, bool force = no_force);
        virtual void set_t_suggest_decision_strategy_impl(const String& name, bool force = no_force);
    };
}

#include "smt/solver/online/check_sat.hpp"

#include "smt/solver/online.inl"
