#pragma once

namespace unsot::smt::solver::reals {
    template <typename B, var::Type typeV>
    typename Mixin<B, typeV>::Sat_solver::Clause
    Mixin<B, typeV>::Check_sat::
        make_depend_real_preds_clause_impl_of(const var::Id& vid, var::Distance max_len)
    {
        return Make_depend_preds_clause<>(this->solver()).perform(vid, max_len);
    }
}

////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////

namespace unsot::smt::solver::reals {
    template <typename B, var::Type typeV>
    template <typename IdSort>
    Mixin<B, typeV>::Check_sat::Make_depend_preds_clause<IdSort>::Make_depend_preds_clause(Solver& s)
        : solver(s), view(s.bools())
    { }

    template <typename B, var::Type typeV>
    template <typename IdSort>
    typename Mixin<B, typeV>::Sat_solver::Clause
    Mixin<B, typeV>::Check_sat::Make_depend_preds_clause<IdSort>::
        perform(const var::Id& vid, var::Distance max_len_)
    {
        Make_depend_preds_clause<Bool>::visited_ids.clear();
        Make_depend_preds_clause<Real>::visited_ids.clear();
        view.clear();
        max_len = move(max_len_);
        if (!visit<IdSort>(vid)) return {};
        return solver.ccheck_sat_ref().make_sat_clause(view);
    }

    template <typename B, var::Type typeV>
    template <typename IdSort>
    template <typename Sort>
    bool Mixin<B, typeV>::Check_sat::Make_depend_preds_clause<IdSort>::visit(const var::Id& vid)
    {
        auto& vids = Make_depend_preds_clause<Sort>::visited_ids;
        if (auto [_, inserted] = vids.insert(vid); !inserted) return true;

        auto& vptr = vars<Sort>(solver).cptr(vid);
        static constexpr bool is_bool = is_bool_v<Sort>;
        if constexpr (is_bool) {
            if (!view.insert(vid)) return true;
            if (view.size() > max_len) return false;
        }
        else {
            if (!vptr->computed()) return true;
        }

        using Dep_sort = Cond_t<is_bool, Real, Bool>;
        for (auto& did : vptr->cdepend_ids()) {
            if (!visit<Dep_sort>(did)) return false;
        }
        return true;
    }
}