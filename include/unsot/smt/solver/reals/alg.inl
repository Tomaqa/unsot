#pragma once

namespace unsot::smt::solver::reals {
    namespace aux {
        template <bool dependV, typename M>
            M make_real_to_preds_views_map(M&&, Preds&);
        template <bool dependV, typename M, typename RealsMap, typename RealsT>
            M make_pred_to_preds_views_map(M&&, const RealsMap&, const RealsT&, Preds&);

        template <bool dependV, typename V, typename SolverT>
        Views_map<V> make_real_to_preds_views_map(SolverT& s)
        {
            static_assert(is_bool_v<typename V::Container::Value>);
            Views_map<V> map = make_default_views_map<V, Real>(s);
            return make_real_to_preds_views_map<dependV>(move(map), s.real_preds());
        }

        template <bool dependV, typename V, typename SolverT>
        Views_map<V> make_pred_to_preds_views_map(SolverT& s)
        {
            static_assert(is_bool_v<typename V::Container::Value>);
            const Views_map<V> reals_map = make_real_to_preds_views_map<dependV, V>(s);
            Views_map<V> map = make_default_views_map<V, Bool>(s);
            return make_pred_to_preds_views_map<dependV>(move(map), reals_map,
                                                         s.creals(), s.real_preds());
        }

        extern template Views_map<Preds_view>
            make_real_to_preds_views_map<true>(Views_map<Preds_view>&&, Preds&);
        extern template Views_map<Preds_view>
            make_real_to_preds_views_map<false>(Views_map<Preds_view>&&, Preds&);
        extern template Views_map<Preds_uniq_view>
            make_real_to_preds_views_map<true>(Views_map<Preds_uniq_view>&&, Preds&);
        extern template Views_map<Preds_uniq_view>
            make_real_to_preds_views_map<false>(Views_map<Preds_uniq_view>&&, Preds&);
        extern template Views_map<Preds_uniq_view>
            make_pred_to_preds_views_map<true>(Views_map<Preds_uniq_view>&&,
                                               const Views_map<Preds_uniq_view>&,
                                               const Reals<>&, Preds&);
        extern template Views_map<Preds_uniq_view>
            make_pred_to_preds_views_map<false>(Views_map<Preds_uniq_view>&&,
                                                const Views_map<Preds_uniq_view>&,
                                                const Reals<>&, Preds&);
        extern template Views_map<Preds_uniq_view>
            make_pred_to_preds_views_map<true>(Views_map<Preds_uniq_view>&&,
                                               const Views_map<Preds_uniq_view>&,
                                               const Reals<var::Type::auto_assignee>&, Preds&);
        extern template Views_map<Preds_uniq_view>
            make_pred_to_preds_views_map<false>(Views_map<Preds_uniq_view>&&,
                                                const Views_map<Preds_uniq_view>&,
                                                const Reals<var::Type::auto_assignee>&, Preds&);
    }

    template <typename V, typename SolverT>
    Views_map<V> make_depend_real_to_preds_views_map(SolverT& s)
    {
        return aux::make_real_to_preds_views_map<true, V>(s);
    }

    template <typename V, typename SolverT>
    Views_map<V> make_arg_real_to_preds_views_map(SolverT& s)
    {
        return aux::make_real_to_preds_views_map<false, V>(s);
    }

    template <typename V, typename SolverT>
    Views_map<V> make_depend_pred_to_preds_views_map(SolverT& s)
    {
        return aux::make_pred_to_preds_views_map<true, V>(s);
    }

    template <typename V, typename SolverT>
    Views_map<V> make_arg_pred_to_preds_views_map(SolverT& s)
    {
        return aux::make_pred_to_preds_views_map<false, V>(s);
    }

    namespace aux {
        template <typename M, typename RealsMap, typename RealsT>
            M make_real_to_assigners_views_map(M&&, const RealsMap& arg_reals_map, const RealsT&);
        template <typename M, typename RealsMap, typename RealsT>
            M make_initial_real_to_assigners_views_map(M&&, const RealsMap& arg_reals_map,
                                                       const RealsT&);

        template <typename V, typename InitAssignMap, typename RealsMap, typename RealsT>
            V make_sorted_initial_real_vars_view(const InitAssignMap&,
                                                 const RealsMap& arg_reals_map,
                                                 RealsT&, const Preds&, Verbosity);

        extern template Views_map<Preds_uniq_view>
            make_real_to_assigners_views_map(Views_map<Preds_uniq_view>&&,
                                             const Views_map<Preds_uniq_view>&,
                                             const Reals<>&);
        extern template Views_map<Preds_uniq_view>
            make_real_to_assigners_views_map(Views_map<Preds_uniq_view>&&,
                                             const Views_map<Preds_uniq_view>&,
                                             const Reals<var::Type::auto_assignee>&);
        extern template Views_map<Preds_uniq_view>
            make_initial_real_to_assigners_views_map(Views_map<Preds_uniq_view>&&,
                                                     const Views_map<Preds_uniq_view>&,
                                                     const Reals<>&);
        extern template Views_map<Preds_uniq_view>
            make_initial_real_to_assigners_views_map(Views_map<Preds_uniq_view>&&,
                                                     const Views_map<Preds_uniq_view>&,
                                                     const Reals<var::Type::auto_assignee>&);

        extern template View<>
            make_sorted_initial_real_vars_view(const Views_map<Preds_uniq_view>&,
                                               const Views_map<Preds_uniq_view>&,
                                               Reals<>&, const Preds&, Verbosity);
        extern template View<var::Type::auto_assignee>
            make_sorted_initial_real_vars_view(const Views_map<Preds_uniq_view>&,
                                               const Views_map<Preds_uniq_view>&,
                                               Reals<var::Type::auto_assignee>&,
                                               const Preds&, Verbosity);
    }

    template <typename V, typename SolverT, typename RealsMap>
    Views_map<V> make_real_to_assigners_views_map(SolverT& s, const RealsMap& arg_reals_map)
    {
        static_assert(is_bool_v<typename V::Container::Value>);
        Views_map<V> map = make_default_views_map<V, Real>(s);
        return aux::make_real_to_assigners_views_map(move(map), arg_reals_map, s.creals());
    }

    template <typename V, typename SolverT, typename RealsMap>
    Views_map<V> make_initial_real_to_assigners_views_map(SolverT& s, const RealsMap& arg_reals_map)
    {
        static_assert(is_bool_v<typename V::Container::Value>);
        Views_map<V> map = make_default_views_map<V, Real>(s);
        return aux::make_initial_real_to_assigners_views_map(move(map), arg_reals_map, s.creals());
    }

    template <typename V, typename SolverT, typename RealsMap, typename InitAssignMap>
    V make_sorted_initial_real_vars_view(SolverT& s, const RealsMap& arg_reals_map,
                                         const InitAssignMap& init_map)
    {
        return aux::make_sorted_initial_real_vars_view<V>(init_map, arg_reals_map,
                                                          s.reals(), s.creal_preds(),
                                                          s.cverbosity());
    }
}
