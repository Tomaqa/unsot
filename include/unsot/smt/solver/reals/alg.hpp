#pragma once

#include "smt/solver/reals.hpp"

namespace unsot::smt::solver::reals {
    bool maybe_initial_assigner(const var::Ptr&) noexcept;
    bool maybe_initial(const Preds::Types::Assigner&) noexcept;

    template <typename V, typename SolverT>
        Views_map<V> make_depend_real_to_preds_views_map(SolverT&);
    template <typename V, typename SolverT>
        Views_map<V> make_arg_real_to_preds_views_map(SolverT&);
    template <typename V, typename SolverT>
        Views_map<V> make_depend_pred_to_preds_views_map(SolverT&);
    template <typename V, typename SolverT>
        Views_map<V> make_arg_pred_to_preds_views_map(SolverT&);

    template <typename V, typename SolverT, typename RealsMap>
        Views_map<V> make_real_to_assigners_views_map(SolverT&, const RealsMap& arg_reals_map);
    template <typename V, typename SolverT, typename RealsMap>
        Views_map<V> make_initial_real_to_assigners_views_map(SolverT&,
                                                              const RealsMap& arg_reals_map);
    template <typename V, typename SolverT, typename RealsMap, typename InitAssignMap>
        V make_sorted_initial_real_vars_view(SolverT&, const RealsMap& arg_reals_map,
                                             const InitAssignMap&);
}

#include "smt/solver/reals/alg.inl"
