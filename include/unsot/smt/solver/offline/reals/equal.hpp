#pragma once

#include "smt/solver/offline/reals.hpp"

#include "util/view.hpp"

namespace unsot::smt::solver::offline::reals::equal {
    template <typename B> class Mixin;

    template <typename S, typename SatSolver, var::Type typeV = var::Type::assignee>
        using Crtp = Mixin<reals::Crtp<S, SatSolver, typeV>>;
}

namespace unsot::smt::solver::offline::reals::equal {
    template <typename B>
    class Mixin : public Inherit<B, Mixin<B>> {
    public:
        using Inherit = unsot::Inherit<B, Mixin<B>>;
        using typename Inherit::This;
        using typename Inherit::That;
        using Offline_equal_t = This;

        class Check_sat;

        using typename Inherit::Reals;

        using Inherit::Inherit;
        virtual ~Mixin()                                            = default;
        Mixin(const Mixin&)                                          = delete;
        Mixin& operator =(const Mixin&)                              = delete;
        Mixin(Mixin&&)                                              = default;
        Mixin& operator =(Mixin&&)                                  = default;
    protected:
        /*struct Var_equality;*/

        struct Set_reals_view;
        struct Set_real_preds_view;

        using typename Inherit::Reals_view;
        /// Using `Preds` instead of `Bools` is appropriate here
        using Real_preds_view = util::View<Preds>;

        /*
        using Var_equalities = Vector<Var_equality>;
        using Var_equalities_map = Vector<Var_equalities>;
        */

        const auto& creals_view() const noexcept       { return _reals_view; }
        auto& reals_view() noexcept                    { return _reals_view; }
        const auto& creal_preds_view() const noexcept
                                                  { return _real_preds_view; }
        auto& real_preds_view() noexcept          { return _real_preds_view; }

        /*
        const auto& cvar_equalities_map() const noexcept
                                               { return _var_equalities_map; }
        auto& var_equalities_map() noexcept
                                               { return _var_equalities_map; }
        const auto& cvar_equalities(const var::Id& vid) const noexcept;
        auto& var_equalities(const var::Id& vid) noexcept;
        */

        void init_solve() override;

        /*
        virtual void preprocess_equalities();
        virtual void set_var_equalities();
        */
    private:
        Reals_view _reals_view{this->reals()};
        Real_preds_view _real_preds_view{this->real_preds()};

        /*Var_equalities_map _var_equalities_map{};*/
    };
}

namespace unsot::smt::solver::offline::reals::equal {
    /*
    template <typename B>
    struct Mixin<B>::Var_equality {
        var::Id pred_id{};
    };
    */

    template <typename B>
    struct Mixin<B>::Set_reals_view {
        using Solver = Mixin::That;

        virtual ~Set_reals_view()                                   = default;
        Set_reals_view(Solver& solver_)                  : solver(solver_) { }

        virtual Set_reals_view& perform();

        Solver& solver;
    };

    template <typename B>
    struct Mixin<B>::Set_real_preds_view {
        using Solver = Mixin::That;

        virtual ~Set_real_preds_view()                              = default;
        Set_real_preds_view(Solver& solver_)             : solver(solver_) { }

        virtual Set_real_preds_view& perform();

        Solver& solver;
    };
}

#include "smt/solver/offline/reals/equal/check_sat.hpp"

#include "smt/solver/offline/reals/equal.inl"
