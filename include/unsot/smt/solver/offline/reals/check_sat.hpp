#pragma once

namespace unsot::smt::solver::offline::reals {
    template <typename B>
    class Mixin<B>::Check_sat : public unsot::Inherit<typename B::Check_sat, Mixin::Check_sat> {
    public:
        using Inherit = unsot::Inherit<typename B::Check_sat, Mixin::Check_sat>;
        using typename Inherit::This;
        using typename Inherit::Solver;

        using Inherit::Inherit;
        virtual ~Check_sat()                                        = default;
        Check_sat(const Check_sat&)                                  = delete;
        Check_sat& operator =(const Check_sat&)                      = delete;
        Check_sat(Check_sat&&)                                      = default;
        Check_sat& operator =(Check_sat&&)                           = delete;

        const auto& ctry_eval_reals_duration() const noexcept
                                                    { return _try_eval_reals_duration; }
        const auto& cupdate_reals_duration() const noexcept
                                                      { return _update_reals_duration; }
        auto ceval_reals_count() const noexcept            { return _eval_reals_count; }

        void restart() override;

        String profiling_to_string(Duration total) const override;
    protected:
        const auto& cconflicts() const noexcept         { return _conflicts; }
        auto& conflicts() noexcept                      { return _conflicts; }
        const auto& clast_conflict_l() const noexcept
                                                  { return _last_conflict_l; }
        auto& last_conflict_l() noexcept          { return _last_conflict_l; }
        const auto& clast_conflict() const noexcept
                                               { return *clast_conflict_l(); }
        auto& last_conflict() noexcept          { return *last_conflict_l(); }

        const auto& clast_depend_id() const noexcept
                                                   { return _last_depend_id; }
        bool is_last_depend_bool() const noexcept
                                              { return _is_last_depend_bool; }
        void set_last_depend_id(var::Id, bool is_bool = true) noexcept;

        Flag cprogress() const noexcept                  { return _progress; }
        [[nodiscard]] Flag& progress() noexcept          { return _progress; }

        auto& try_eval_reals_duration() noexcept    { return _try_eval_reals_duration; }
        auto& update_reals_duration() noexcept        { return _update_reals_duration; }
        auto& eval_reals_count() noexcept                  { return _eval_reals_count; }

        Sat perform_body() override;

        virtual Sat eval_reals();
        virtual void try_eval_reals();
        /// Update reals wrt. assigned real predicates
        virtual Flag update_reals()                                       = 0;

        virtual typename Sat_solver::Clause make_depend_real_preds_clause();
        virtual typename Sat_solver::Clause make_depend_real_preds_conflict();

        /// Assert all conflict clauses and clear them
        virtual bool assert_conflicts();

        /// Add new standalone conflict clause
        virtual void add_conflict(typename Sat_solver::Clause);
        /// Append clause into last conflict clause
        virtual void append_conflict(typename Sat_solver::Clause);

        void add_real_preds_conflict();
        void append_real_preds_conflict();
    private:
        typename Sat_solver::Cnf _conflicts{};
        typename Sat_solver::Clause* _last_conflict_l{};

        var::Id _last_depend_id{};
        bool _is_last_depend_bool{};

        Flag _progress{};

        Duration _try_eval_reals_duration{};
        Duration _update_reals_duration{};
        long _eval_reals_count{};
    };
}
