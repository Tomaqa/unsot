#pragma once

#include "smt/solver/offline/reals/equal.hpp"
#include "smt/sat/solver/offline/expr/minisat.hpp"

#ifdef NO_EXPLICIT_TP_INST
#include "smt/solver.tpp"
#include "smt/solver/bools.tpp"
#include "smt/solver/reals.tpp"

#include "smt/solver/offline/bools.tpp"
#include "smt/solver/offline/reals.tpp"
#include "smt/solver/offline/reals/equal.tpp"

#include "smt/solver/run.tpp"
#include "smt/solver/offline/bools/run.tpp"
#endif

namespace unsot::smt::solver::offline::reals::equal::minisat {
    template <typename S>
        using Crtp = equal::Crtp<S, sat::solver::offline::expr::Minisat>;

    struct Solver final : Inherit<Crtp<Solver>> {
        using Inherit::Inherit;
    };
}

#ifndef NO_EXPLICIT_TP_INST
namespace unsot::smt::solver {
    namespace aux {
        using _S = offline::reals::equal::minisat::Solver;
    }

    extern template class aux::_S::Base::Crtp;
    extern template class aux::_S::Bools_t::Mixin;
    extern template class aux::_S::Reals_t::Mixin;
}

namespace unsot::smt::solver::offline {
    extern template class solver::aux::_S::Offline_bools_t::Mixin;
    extern template class solver::aux::_S::Offline_reals_t::Mixin;
    extern template class solver::aux::_S::Offline_equal_t::Mixin;
}
#endif  /// NO_EXPLICIT_TP_INST
