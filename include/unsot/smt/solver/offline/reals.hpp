#pragma once

#include "smt/solver/reals.hpp"
#include "smt/solver/offline/bools.hpp"

namespace unsot::smt::solver::offline::reals {
    using namespace solver::reals;

    template <typename B> class Mixin;

    template <typename S, typename SatSolver, var::Type typeV = var::Type::assignee>
        using Crtp = Mixin<solver::reals::Mixin<bools::Crtp<S, SatSolver>, typeV>>;
}

namespace unsot::smt::solver::offline::reals {
    template <typename B>
    class Mixin : public Inherit<B, Mixin<B>> {
    public:
        using Inherit = unsot::Inherit<B, Mixin<B>>;
        using typename Inherit::This;
        using typename Inherit::That;
        using Offline_reals_t = This;

        class Check_sat;

        using typename Inherit::Sat_solver;

        using Inherit::Inherit;
        virtual ~Mixin()                                            = default;
        Mixin(const Mixin&)                                          = delete;
        Mixin& operator =(const Mixin&)                              = delete;
        Mixin(Mixin&&)                                              = default;
        Mixin& operator =(Mixin&&)                                  = default;
    };
}

#include "smt/solver/offline/reals/check_sat.hpp"
