#pragma once

namespace unsot::smt::solver::offline::bools {
    template <typename B>
    class Mixin<B>::Check_sat : public unsot::Inherit<typename B::Check_sat, Mixin::Check_sat> {
    public:
        using Inherit = unsot::Inherit<typename B::Check_sat, Mixin::Check_sat>;
        using typename Inherit::This;
        using typename Inherit::Solver;

        using Inherit::Inherit;
        virtual ~Check_sat()                                        = default;
        Check_sat(Check_sat&&)                                      = default;

        const auto& ceval_bools_duration() const noexcept    { return _eval_bools_duration; }
        auto ceval_bools_count() const noexcept                 { return _eval_bools_count; }

        String profiling_to_string(Duration total) const override;
    protected:
        auto& eval_bools_duration() noexcept                 { return _eval_bools_duration; }
        auto& eval_bools_count() noexcept                       { return _eval_bools_count; }

        Sat perform_body() override;

        virtual bool eval_bools();
        /// Assigns all `bools` according to SAT model
        virtual bool update_bools();
    private:
        Duration _eval_bools_duration{};
        long _eval_bools_count{};
    };
}
