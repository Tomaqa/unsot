#pragma once

#include "smt/solver/run.hpp"
#include "smt/solver/offline/bools.hpp"

namespace unsot::smt::solver::offline::bools {
    template <typename B>
    class Mixin<B>::Run : public unsot::Inherit<typename B::Run> {
    public:
        using Inherit = unsot::Inherit<typename B::Run>;
        using typename Inherit::Solver;
        using typename Inherit::That;

        using Inherit::Inherit;
        virtual ~Run()                                                                    = default;

        List preprocess() override;
    };
}
