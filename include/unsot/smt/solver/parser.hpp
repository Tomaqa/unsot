#pragma once

#include "expr/pos.hpp"

namespace unsot::smt::solver {
    template <typename S>
    class Crtp<S>::Parser : public Object<Parser> {
    public:
        using typename Object<Parser>::This;
        using Solver = Crtp::That;

        Parser()                                                                           = delete;
        virtual ~Parser()                                                                 = default;
        Parser(const Parser&)                                                             = default;
        Parser& operator =(const Parser&)                                                 = default;
        Parser(Parser&&)                                                                  = default;
        Parser& operator =(Parser&&)                                                      = default;
        Parser(Solver& solver_);

        static bool is_plain_cmd(const List&) noexcept;
        static void check_is_plain_cmd(const List&);

        [[nodiscard]] virtual Preprocess preprocess_lines(istream&);
        virtual void preprocess_list(List&);
        [[nodiscard]] virtual List preprocess(istream&);

        virtual void parse(List&);

        const auto& csolver() const noexcept                                     { return _solver; }
        const auto& solver() const noexcept                                    { return csolver(); }
        auto& solver() noexcept                                                  { return _solver; }
    protected:
        virtual void preprocess_sublist(expr::Ptr&);
        /// Throws `ignore` if list becomes a key
        virtual void post_preprocess_list(expr::Ptr&);
        /// Position of `pos` is already *after* `cmd`
        virtual void post_preprocess_cmd(expr::Ptr&, Key& /*cmd*/, Pos&)                         { }
        virtual void post_preprocess_elem(expr::Ptr&)                                            { }

        virtual void parse_sublist(expr::Ptr&);
        virtual void parse_expand_sublist(expr::Ptr&);
        /// Position of `pos` is already *after* `cmd`
        virtual void parse_expand_cmd(expr::Ptr&, Key& /*cmd*/, Pos&)                            { }
        virtual void parse_expand_elem(expr::Ptr&)                                               { }

        /// Position of `pos` is already *after* `cmd`
        virtual void parse_cmd(const Key& cmd, Pos&);
        virtual void parse_cmd_set_option(Pos&&);
        virtual void parse_cmd_get_option(Pos&&);
        virtual void parse_cmd_assert(Pos&&);
        virtual void parse_cmd_check_sat(Pos&&);
        virtual void parse_cmd_get_value(Pos&&);
        virtual void parse_cmd_get_model(Pos&&);
        virtual void parse_cmd_echo(Pos&&);
        virtual void parse_cmd_exit(Pos&&);
    private:
        Solver& _solver;
    };
}
