#ifdef NO_EXPLICIT_TP_INST

#include "smt/solver/bools.tpp"

#else

namespace TP_INST_SOLVER_NAMESPACE {
    extern template class aux::_S::Bools_t::Mixin;
}

#endif  /// NO_EXPLICIT_TP_INST
