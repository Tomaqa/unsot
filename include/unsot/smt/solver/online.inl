#pragma once

namespace unsot::smt::solver::online {
    template <typename B>
    void Mixin<B>::set_t_propagate_strategy(const String& name, bool force)
    {
        Check_sat::T_propagate::set_tp(BIND_THIS(set_t_propagate_strategy_impl), name, force);
    }

    template <typename B>
    void Mixin<B>::set_t_suggest_decision_strategy(const String& name, bool force)
    {
        Check_sat::T_suggest_decision::set_tp(BIND_THIS(set_t_suggest_decision_strategy_impl), name, force);
    }
}
