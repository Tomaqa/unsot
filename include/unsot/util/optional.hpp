#pragma once

#include "util.hpp"

#include <optional>

namespace unsot::util {
    using std::make_optional;
}

namespace unsot::util {
    template <typename T>
    class Optional : public Inherit<std::optional<T>, Optional<T>> {
    public:
        using Inherit = unsot::Inherit<std::optional<T>, Optional<T>>;
        using typename Inherit::This;
        using Base = typename Inherit::Parent;

        using Value = typename Inherit::value_type;

        using Inherit::Inherit;
        using Base::operator =;
        Optional()                                                                        = default;
        template <typename U, typename T_ = This, Req<is_derived_from_v<Decay<U>, T_>> = 0>
            constexpr Optional(U&&);
        template <typename U, typename T_ = This, Req<is_derived_from_v<Decay<U>, T_>> = 0>
            constexpr Optional& operator =(U&&);
        template <typename U = Value, typename T_ = This, Req<!is_derived_from_v<Decay<U>, T_>> = 0>
            constexpr Optional(U&&);
        template <typename U = Value, typename T_ = This, Req<!is_derived_from_v<Decay<U>, T_>> = 0>
            constexpr Optional& operator =(U&&);

        using Inherit::operator *;
        using Inherit::operator ->;
        constexpr const auto& cvalue() const noexcept;
        constexpr const auto& value() const& noexcept                           { return cvalue(); }
        constexpr auto& value()& noexcept;
        constexpr auto&& value()&& noexcept;
        constexpr const auto& cvalue_check() const;
        constexpr const auto& value_check() const&                        { return cvalue_check(); }
        constexpr auto& value_check()&;
        constexpr auto&& value_check()&&;

        using Inherit::has_value;
        constexpr bool valid() const noexcept                                { return has_value(); }
        constexpr bool invalid() const noexcept                                 { return !valid(); }
        constexpr bool unknown() const noexcept                                { return invalid(); }
        explicit constexpr operator bool() const noexcept                        { return valid(); }
        constexpr bool operator !() const noexcept                             { return invalid(); }

        using Inherit::value_or;
        constexpr auto value_or_default() const&;
        constexpr auto value_or_default()&&;

        using Inherit::reset;
        void invalidate() noexcept                                                      { reset(); }
        void forget() noexcept                                                          { reset(); }

        String to_string() const;

        constexpr bool equals(const This&) const noexcept;
        constexpr bool equals(const Value&) const noexcept;
    };
}

#include "util/optional.inl"
