#pragma once

#include "util.hpp"

#include "util/flag.hpp"

namespace unsot::util {
    namespace aux::bimap {
        template <typename Key1T, typename Key2T,
                  template<typename...> typename Map1T,
                  template<typename...> typename Map2T>
        class Base : public Static<Base<Key1T, Key2T, Map1T, Map2T>> {
        public:
            using typename Static<Base<Key1T, Key2T, Map1T, Map2T>>::This;

            using Key1 = Key1T;
            using Key2 = Key2T;

            using Key1_link = const Key1*;
            using Key2_link = const Key2*;

            using Map1 = Map1T<Key1, Key2_link>;
            using Map2 = Map2T<Key2, Key1_link>;

            using key_type = typename Map1::key_type;
            using mapped_type = typename Map1::mapped_type;
            using value_type = typename Map1::value_type;
            using reference = typename Map1::reference;
            using const_reference = typename Map1::const_reference;
            using pointer = typename Map1::pointer;
            using const_pointer = typename Map1::const_pointer;
            using size_type = typename Map1::size_type;
            using const_iterator = typename Map1::const_iterator;
            using iterator = typename Map1::iterator;

            const_iterator cbegin() const noexcept                      { return cmap1().cbegin(); }
            const_iterator cend() const noexcept                          { return cmap1().cend(); }
            const_iterator begin() const noexcept                               { return cbegin(); }
            const_iterator end() const noexcept                                   { return cend(); }
            iterator begin() noexcept                                     { return map1().begin(); }
            iterator end() noexcept                                         { return map1().end(); }

            bool contains_key1(const Key1& key1_) const noexcept
                                           { return cmap1().contains(key1_); }
            bool contains_key2(const Key2& key2_) const noexcept
                                           { return cmap2().contains(key2_); }

            const auto& ckey2(const Key1& key1_) const
                                                   { return *ckey2_l(key1_); }
            auto& key2(const Key1& key1_) noexcept  { return *key2_l(key1_); }
            const auto& ckey1(const Key2& key2_) const
                                                   { return *ckey1_l(key2_); }
            auto& key1(const Key2& key2_) noexcept  { return *key1_l(key2_); }

            Key2_link find_key2(const Key1& key1_) const noexcept;
            Key1_link find_key1(const Key2& key2_) const noexcept;

            bool insert(const Key1&, const Key2&);

            String to_string() const&;
        protected:
            const auto& cmap1() const                        { return _map1; }
            auto& map1()                                     { return _map1; }
            const auto& cmap2() const                        { return _map2; }
            auto& map2()                                     { return _map2; }

            const auto& ckey2_l(const Key1&) const;
            auto& key2_l(const Key1&) noexcept;
            const auto& ckey1_l(const Key2&) const;
            auto& key1_l(const Key2&) noexcept;
        private:
            Map1 _map1{};
            Map2 _map2{};
        };

        template <typename B>
        class Operator_mixin : public Inherit<B, Operator_mixin<B>> {
        public:
            using Inherit = unsot::Inherit<B, Operator_mixin<B>>;
            using typename Inherit::This;

            using typename Inherit::Key1;
            using typename Inherit::Key2;

            static_assert(!is_convertible_v<Key1, Key2>);
            static_assert(!is_convertible_v<Key2, Key1>);

            using typename Inherit::Key1_link;
            using typename Inherit::Key2_link;

            using Inherit::Inherit;

            bool contains(const Key1& key1_) const noexcept
                                        { return this->contains_key1(key1_); }
            bool contains(const Key2& key2_) const noexcept
                                        { return this->contains_key2(key2_); }

            const Key2& operator [](const Key1& key1_) const
                                                { return this->ckey2(key1_); }
            const Key1& operator [](const Key2& key2_) const
                                                { return this->ckey1(key2_); }
            Operator_mixin& operator [](const Key1&);
            Operator_mixin& operator [](const Key2&);
            Operator_mixin& operator =(const Key2&);
            Operator_mixin& operator =(const Key1&);

            Key2_link find(const Key1& key1_) noexcept
                                            { return this->find_key2(key1_); }
            Key1_link find(const Key2& key2_) noexcept
                                            { return this->find_key1(key2_); }
        private:
            void assign_key2(const Key2&);
            void assign_key1(const Key1&);

            Key1_link _key1_l{};
            Key2_link _key2_l{};
            Key1_link* _key1_ll{};
            Key2_link* _key2_ll{};
            Flag _ref1{};
        };
    }

    namespace aux {
        template <typename Key1T, typename Key2T,
                  template<typename...> typename Map1T,
                  template<typename...> typename Map2T,
                  bool conv1 = true, bool conv2 = true>
        struct Bimap : Inherit<bimap::Base<Key1T, Key2T, Map1T, Map2T>> {
            using Inherit = unsot::Inherit<bimap::Base<Key1T, Key2T,
                                                       Map1T, Map2T>>;

            using Inherit::Inherit;
        };

        template <typename Key1T, typename Key2T,
                  template<typename...> typename Map1T,
                  template<typename...> typename Map2T>
        struct Bimap<Key1T, Key2T, Map1T, Map2T, false, false>
            : Inherit<bimap::Operator_mixin<bimap::Base<Key1T, Key2T,
                                                        Map1T, Map2T>>> {
            using Inherit = unsot::Inherit<bimap::Operator_mixin<
                bimap::Base<Key1T, Key2T, Map1T, Map2T>
            >>;

            using Inherit::Inherit;
        };
    }

    template <typename Key1T, typename Key2T,
              template<typename...> typename Map1T,
              template<typename...> typename Map2T>
    class Bimap : public Inherit<aux::Bimap<Key1T, Key2T, Map1T, Map2T,
                                            is_convertible_v<Key1T, Key2T>,
                                            is_convertible_v<Key2T, Key1T>>,
                                 Bimap<Key1T, Key2T, Map1T, Map2T>> {
    public:
        using Inherit =
            unsot::Inherit<aux::Bimap<Key1T, Key2T, Map1T, Map2T,
                                      is_convertible_v<Key1T, Key2T>,
                                      is_convertible_v<Key2T, Key1T>>,
                           Bimap<Key1T, Key2T, Map1T, Map2T>>;
        using typename Inherit::This;

        using Inherit::Inherit;
    };
}

#include "util/bimap.inl"
