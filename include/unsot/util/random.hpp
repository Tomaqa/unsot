#pragma once

#include "util.hpp"

#include <random>

namespace unsot::util {
    using Pseudo_random = std::default_random_engine;
    using True_random = std::random_device;

    inline Pseudo_random pseudo_random{};
    inline True_random true_random{};

    template <typename ItT, typename RandomT = Pseudo_random&>
        void shuffle(ItT first, ItT last, RandomT&& = pseudo_random);
    template <typename ContT, typename RandomT = Pseudo_random&>
        void shuffle(ContT&, RandomT&& = pseudo_random);
}

#include "util/random.inl"
