#pragma once

#include "util/fun.hpp"

namespace unsot::util {
    template <typename ContT, typename BinF> struct Container_f;
}

namespace unsot::util {
    template <typename ContT,
              typename BinF = Bin_f<typename ContT::value_type>>
    struct Container_f : Inherit<BinF> {
        using Inherit = unsot::Inherit<BinF>;

        using Ret = typename Inherit::result_type;

        using Inherit::Inherit;

        using Inherit::operator ();
        Ret operator ()(const ContT&) const;
        template <typename UnConvF>
            Ret operator ()(const ContT&, UnConvF) const;
    };
}

#include "util/fun/alg.inl"
