#pragma once

#include "util/numeric/alg.hpp"

namespace unsot::util {
    template <typename ContT, typename BinF>
    typename Container_f<ContT, BinF>::Ret
    Container_f<ContT, BinF>::operator ()(const ContT& cont) const
    {
        using numeric::accumulate;
        return accumulate(++cbegin(cont), cend(cont), cont.front(), *this);
    }

    template <typename ContT, typename BinF>
    template <typename UnConvF>
    typename Container_f<ContT, BinF>::Ret
    Container_f<ContT, BinF>::
        operator ()(const ContT& cont, UnConvF conv) const
    {
        using numeric::accumulate;
        return accumulate(++cbegin(cont), cend(cont), conv(cont.front()),
                          [this, &conv](auto&& lhs, auto&& rhs) -> Ret {
            return (*this)(FORWARD(lhs), conv(FORWARD(rhs)));
        });
    }
}
