#pragma once

#include "util.hpp"

#include <variant>

namespace unsot::util {
    template <typename T, typename... Ts>
        constexpr bool valid(const Union<Ts...>&) noexcept;

    template <typename T, typename... Ts>
        constexpr const T& get(const Union<Ts...>&);
    template <typename T, typename... Ts> constexpr T& get(Union<Ts...>&);
    template <typename T, typename... Ts> constexpr T&& get(Union<Ts...>&&);
    template <typename T, typename... Ts>
        constexpr const T* get_l(const Union<Ts...>&) noexcept;
    template <typename T, typename... Ts>
        constexpr T* get_l(Union<Ts...>&) noexcept;

    template <typename T, typename... Ts>
        Optional<T> to(const Union<Ts...>&) noexcept;
    template <typename T, typename... Ts>
        Optional<T> to(Union<Ts...>&&) noexcept;
    template <typename T, typename... Ts> T to_check(const Union<Ts...>&);
    template <typename T, typename... Ts> T to_check(Union<Ts...>&&);

    template <typename F, typename... Unions>
        constexpr decltype(auto) visit(F&&, Unions&&...);
}

namespace unsot::util {
    /// Composition is used instead of inheritance,
    /// because `std::visit' works reliably only directly with `std::variant'
    template <typename... Ts>
    class Union : public Static<Union<Ts...>> {
    public:
        using typename Static<Union<Ts...>>::This;
        using Base = std::variant<Ts...>;

        Union()                                                                           = default;
        ~Union()                                                                          = default;
        Union(const Union&)                                                               = default;
        Union& operator =(const Union&)                                                   = default;
        Union(Union&&)                                                                    = default;
        Union& operator =(Union&&)                                                        = default;
        template <typename... Args> constexpr Union(Args&&...);
        constexpr Union& operator =(const Base&);
        constexpr Union& operator =(Base&&);
        template <typename T, typename = If<!is_derived_from_v<T, This>>> Union& operator =(T&&);
        void swap(Union&) noexcept;

        constexpr size_t index() const noexcept;
        constexpr bool valid() const noexcept                                 { return !invalid(); }
        constexpr bool invalid() const noexcept;
        template <typename T> constexpr bool valid() const noexcept;

        template <typename T> constexpr const T& cget() const;
        template <typename T> constexpr const T& get() const&                  { return cget<T>(); }
        template <typename T> constexpr T& get()&;
        template <typename T> constexpr T&& get()&&                  { return std::move(get<T>()); }
        template <size_t idx> constexpr const auto& cget() const;
        template <size_t idx> constexpr const auto& get() const&             { return cget<idx>(); }
        template <size_t idx> constexpr auto& get()&;
        template <size_t idx> constexpr auto&& get()&&             { return std::move(get<idx>()); }
        template <typename T> constexpr const T* cget_l() const noexcept;
        template <typename T> constexpr const T* get_l() const noexcept      { return cget_l<T>(); }
        template <typename T> constexpr T* get_l() noexcept;
        template <size_t idx> constexpr auto cget_l() const noexcept;
        template <size_t idx> constexpr auto get_l() const noexcept        { return cget_l<idx>(); }
        template <size_t idx> constexpr auto get_l() noexcept;

        template <typename T> Optional<T> to() const& noexcept;
        template <typename T> Optional<T> to()&& noexcept;
        template <typename T> T to_check() const&;
        template <typename T> T to_check()&&;

        template <typename T, typename... Args> T& emplace(Args&&...);

        template <typename F, typename... Unions>
            constexpr decltype(auto) cvisit(F&&, Unions&&... others) const;
        template <typename F, typename... Unions>
            constexpr decltype(auto) visit(F&& f, Unions&&... others) const
                                                  { return cvisit(FORWARD(f), FORWARD(others)...); }
        template <typename F, typename... Unions>
            constexpr decltype(auto) visit(F&&, Unions&&... others);

        String to_string() const&;
        String to_string()&&;

        constexpr bool equals(const This&) const noexcept;
    protected:
        const auto& cvar() const noexcept                                           { return _var; }
        const auto& var() const& noexcept                                         { return cvar(); }
        auto& var()& noexcept                                                       { return _var; }
        auto&& var()&& noexcept                                         { return std::move(var()); }
    private:
        template <typename T, typename U> static auto&& get_impl(U&&);
        template <size_t idx, typename U> static auto&& get_impl(U&&);

        template <typename T, bool check, typename U>
            static auto to_impl(U&&) noexcept(!check);

        template <typename F, typename... Unions>
            static constexpr decltype(auto) visit_impl(F&&, Unions&&...);

        template <typename F, typename U>
            static String to_string_impl(F&&, U&&);

        Base _var{};
    };
}

#include "util/union.inl"
