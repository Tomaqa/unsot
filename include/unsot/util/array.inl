#pragma once

namespace unsot::util {
    namespace aux {
        template <typename T, size_t n, size_t... is>
        constexpr array<std::remove_cv_t<T>, n>
        to_array_impl(T(&&a)[n], std::index_sequence<is...>)
        {
            return { {move(a[is])...} };
        }

        template <typename T, size_t n, size_t... is>
        constexpr array<std::remove_cv_t<T>, n>
        to_array_impl(T(&a)[n], std::index_sequence<is...>)
        {
            return { {a[is]...} };
        }
    }

    template <typename T, size_t n>
    constexpr array<std::remove_cv_t<T>, n> to_array(T(&&a)[n])
    {
        return aux::to_array_impl(move(a), std::make_index_sequence<n>{});
    }

    template <typename T, size_t n>
    constexpr array<std::remove_cv_t<T>, n> to_array(T(&a)[n])
    {
        return aux::to_array_impl(a, std::make_index_sequence<n>{});
    }

    template <typename T, size_t n1, size_t n2>
    constexpr array<T, n1+n2> array_cat(const array<T, n1>& lhs, const array<T, n2>& rhs)
    {
        array<T, n1+n2> res{};
        int j = 0;
        for (Idx lsize = lhs.size(), i = 0; i < lsize; ++i, ++j)
            res[j] = lhs[i];
        for (Idx rsize = rhs.size(), i = 0; i < rsize; ++i, ++j)
            res[j] = rhs[i];
        return res;
    }
}
