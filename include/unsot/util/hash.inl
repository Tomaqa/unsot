#pragma once

namespace unsot::util {
    template <typename KeyT, typename ValueT, typename HashT>
    String Hash<KeyT, ValueT, HashT>::to_string() const
    {
        using unsot::to_string;
        String str;
        for (const auto& e : *this) {
            str += to_string(e) + ", ";
        }
        return str;
    }

    ////////////////////////////////////////////////////////////////

    template <typename KeyT>
    String Hash<KeyT>::to_string() const
    {
        using unsot::to_string;
        String str;
        for (const auto& e : *this) {
            str += to_string(e) + " ";
        }
        return str;
    }

    ////////////////////////////////////////////////////////////////

    //+ template <typename T>
    //+ size_t Hash_object<T>::hash() const
    //+ {
    //+     return this->ccast().hash();
    //+ }
}

//+ namespace std {
//+     template <typename T>
//+     struct hash<unsot::util::Hash_object<T>>
//+     {
//+         size_t operator ()(const unsot::util::Hash_object<T>& object) const
//+         {
//+             return object.hash();
//+         }
//+     };
//+ }
