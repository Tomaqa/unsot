#pragma once

namespace unsot::util {
    template <typename B, typename ValueT>
    bool Associative_mixin<B, ValueT>::contains(const Key& key) const noexcept
    {
        return this->count(key) == 1;
    }

    //+ template <typename B, typename ValueT>
    //+ template <typename K>
    //+ bool Associative_mixin<B, ValueT>::contains(const K& k) const noexcept
    //+ {
    //+     if constexpr (is_convertible_v<K, Key>)
    //+         return this->count(static_cast<const Key&>(k)) == 1;
    //+     else return this->count(k) == 1;
    //+ }

    template <typename B, typename ValueT>
    void Associative_mixin<B, ValueT>::insert(const Base& rhs)
    {
        insert(rhs.cbegin(), rhs.cend());
    }

    template <typename B, typename ValueT>
    void Associative_mixin<B, ValueT>::insert(Base&& rhs)
    {
        insert(make_move_iterator(rhs.begin()), make_move_iterator(rhs.end()));
    }

    ////////////////////////////////////////////////////////////////

    template <typename B>
    const typename Map_mixin<B>::Value& Map_mixin<B>::cat(const Key& key) const
    try {
        return Inherit::at(key);
    }
    catch (const std::out_of_range&) {
        THROW("Key not found: ") + to_string(key);
    }

    template <typename B>
    typename Map_mixin<B>::Value& Map_mixin<B>::at(const Key& key)
    {
        return const_cast<Value&>(cat(key));
    }

    template <typename B>
    const typename Map_mixin<B>::Value&
    Map_mixin<B>::operator [](const Key& key) const
    {
        assert(!_prefer_assert_over_throw_ || this->contains(key));
        return cat(key);
    }

    template <typename B>
    Optional<typename Map_mixin<B>::Value>
    Map_mixin<B>::extract_value(const Key& key) noexcept
    {
        auto node_handle = extract(key);
        if (empty(node_handle)) return {};
        return move(node_handle.mapped());
    }
}

////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////

namespace unsot::util {
    template <typename M, typename UnPred>
    typename M::size_type erase_if(M& map, UnPred pred)
    {
        const auto size_ = map.size();
        for (auto it = map.begin(), eit = map.end(); it != eit;) {
            if (pred(*it)) it = map.erase(it);
            else ++it;
        }
        return size_ - map.size();
    }
}
