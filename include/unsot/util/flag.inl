#pragma once

namespace unsot::util {
    constexpr Flag::Flag(int val) noexcept
        : Flag((val == unknown_value) ? Flag() : Flag(bool(val)))
    { }

    Flag::operator int() const noexcept
    {
        return (unknown()) ? unknown_value : int(cvalue());
    }

    Flag Flag::operator &(Flag rhs) const noexcept
    {
        return rhs &= *this;
    }

    Flag Flag::operator |(Flag rhs) const noexcept
    {
        return rhs |= *this;
    }

    Flag Flag::operator ^(Flag rhs) const noexcept
    {
        return rhs ^= *this;
    }

    constexpr bool Flag::equals(const This& rhs) const noexcept
    {
        return Inherit::equals(rhs);
    }

    constexpr bool Flag::equals(int val) const noexcept
    {
        return equals(Flag(val));
    }
}

////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////

namespace unsot::util {
    template <typename T, Req<is_integral_v<T>>>
    Flag operator &(Flag flag, T x) noexcept
    {
        return flag & Flag(x);
    }

    template <typename T, Req<is_integral_v<T>>>
    Flag operator |(Flag flag, T x) noexcept
    {
        return flag | Flag(x);
    }

    template <typename T, Req<is_integral_v<T>>>
    Flag operator ^(Flag flag, T x) noexcept
    {
        return flag ^ Flag(x);
    }

    template <typename T, Req<is_integral_v<T>>>
    Flag operator &(T x, Flag flag) noexcept
    {
        return move(flag) & x;
    }

    template <typename T, Req<is_integral_v<T>>>
    Flag operator |(T x, Flag flag) noexcept
    {
        return move(flag) | x;
    }

    template <typename T, Req<is_integral_v<T>>>
    Flag operator ^(T x, Flag flag) noexcept
    {
        return move(flag) ^ x;
    }
}
