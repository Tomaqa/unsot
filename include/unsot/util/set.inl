#pragma once

namespace unsot::util {
    template <typename KeyT>
    String Set<KeyT>::to_string() const
    {
        using unsot::to_string;
        String str;
        for (const auto& e : *this) {
            str += to_string(e) + " ";
        }
        return str;
    }

    ////////////////////////////////////////////////////////////////

    template <typename KeyT>
    String Multiset<KeyT>::to_string() const
    {
        using unsot::to_string;
        String str;
        for (const auto& e : *this) {
            str += to_string(e) + " ";
        }
        return str;
    }
}
