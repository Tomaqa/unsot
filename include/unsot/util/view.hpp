#pragma once

#include "util.hpp"

#include "util/hash.hpp"

namespace unsot::util::view {
    template <typename V, typename T, template<typename...> typename AccCont = Vector>
        class Crtp;
    template <typename B> class Unique_mixin;
}

namespace unsot::util::view {
    namespace aux {
        template <typename V, typename PtrT,
                  template<typename...> typename AccCont,
                  typename CPtrT = const PtrT, typename AccT = PtrT, bool randomAccess = false>
        class Crtp : public Static<Crtp<V, PtrT, AccCont, CPtrT, AccT, randomAccess>>,
                     public unsot::Crtp<V>,
                     public Container_types<Rm_ptr<PtrT>> {
        public:
            using typename Static<Crtp<V, PtrT, AccCont, CPtrT, AccT, randomAccess>>::This;
            using typename unsot::Crtp<V>::That;
            using Base = This;

            struct const_iterator;
            struct iterator;

            using Const_ptr = CPtrT;
            using Ptr = PtrT;

            using Accessor = AccT;
            template <typename AccT_> using Accessors_tp = AccCont<AccT_>;
            using Accessors = Accessors_tp<Accessor>;

            using Const_access_iterator = typename Accessors::const_iterator;
            using Access_iterator = typename Accessors::iterator;

            static constexpr bool random_access_v = randomAccess;

            Crtp()                                                                        = default;
            ~Crtp()                                                                       = default;
            Crtp(const Crtp&)                                                             = default;
            Crtp& operator =(const Crtp&)                                                 = default;
            Crtp(Crtp&&)                                                                  = default;
            Crtp& operator =(Crtp&&)                                                      = default;
            void swap(That&) noexcept;

            const_iterator make_const_iterator(Const_access_iterator) const noexcept;
            iterator make_iterator(Access_iterator) noexcept;

            const auto& caccessors() const noexcept                           { return _accessors; }
            auto& accessors() noexcept                  { return _accessors; }
            const auto& caccessor(Idx idx) const noexcept              { return caccessors()[idx]; }
            auto& accessor(Idx idx) noexcept                            { return accessors()[idx]; }

            size_t size() const noexcept                         { return std::size(caccessors()); }
            bool empty() const noexcept                         { return std::empty(caccessors()); }

            Const_access_iterator cacc_begin() const noexcept  { return std::cbegin(caccessors()); }
            Const_access_iterator cacc_end() const noexcept      { return std::cend(caccessors()); }
            Const_access_iterator acc_begin() const noexcept                { return cacc_begin(); }
            Const_access_iterator acc_end() const noexcept                    { return cacc_end(); }
            Access_iterator acc_begin() noexcept                 { return std::begin(accessors()); }
            Access_iterator acc_end() noexcept                     { return std::end(accessors()); }

            const_iterator cbegin() const noexcept;
            const_iterator cend() const noexcept;
            const_iterator begin() const noexcept                               { return cbegin(); }
            const_iterator end() const noexcept                                   { return cend(); }
            iterator begin() noexcept;
            iterator end() noexcept;
            const auto& cfront() const noexcept                                { return *cbegin(); }
            const auto& cback() const noexcept                                 { return *--cend(); }
            const auto& front() const noexcept                                  { return cfront(); }
            const auto& back() const noexcept                                    { return cback(); }
            auto& front() noexcept                                              { return *begin(); }
            auto& back() noexcept                                               { return *--end(); }

            const auto& caccess(const Accessor&) const noexcept;
            auto& access(const Accessor&) noexcept;

            const auto& operator [](Idx) const noexcept;
            auto& operator [](Idx) noexcept;

            void reserve(size_t size_)                               { accessors().reserve(size_); }
            void resize(size_t size_)                                 { accessors().resize(size_); }

            void clear()                                                    { accessors().clear(); }

            void push_back(Accessor);
            void pop_back();

            String to_string() const&;
        protected:
            template <typename AccIt> class Iterator_crtp;

            void swap_impl(That&) noexcept;

            void push_back_impl(Accessor);
            void pop_back_impl();
        private:
            Accessors _accessors{};
        };

        template <typename B, typename ContT>
        class Cont_mixin : public Inherit<B, Cont_mixin<B, ContT>> {
        public:
            using Inherit = unsot::Inherit<B, Cont_mixin<B, ContT>>;
            using typename Inherit::This;
            using typename Inherit::Base;
            using typename Inherit::That;

            using Container = Decay<ContT>;

            using Const_it = typename Container::const_iterator;
            using It = Cond_t<is_const_v<Container>, Const_it, typename Container::iterator>;

            using Inherit::Inherit;

            using Inherit::push_back;
            template <typename ItT = It, Req<is_iterator_v<ItT> && !is_ptr_v<Decay<ItT>>> = 0>
                void push_back(ItT);

            template <typename ItT = It> void assign(ItT first, ItT last, size_t);
            template <typename ItT = It> void assign(ItT first, ItT last);
            template <typename C = Container, Req<is_container_v<C>> = 0> void assign(C&);
            template <typename C = Container&&, Req<is_container_v<C>> = 0> void assign(C&&);
        protected:
            using Inherit::push_back_impl;
            template <typename ItT = It> void push_back_impl(ItT);
        };

        template <typename B, bool randomAccess = false>
        class Mixin : public Inherit<B> {
        public:
            using Inherit = unsot::Inherit<B>;
            using typename Inherit::That;
            using typename Inherit::Base;

            using typename Inherit::Ptr;

            static_assert(!Inherit::random_access_v);

            using typename Inherit::Accessor;

            static_assert(is_same_v<Accessor, Ptr>);

            using typename Inherit::Const_access_iterator;
            using typename Inherit::Access_iterator;

            using typename Inherit::const_iterator;
            using typename Inherit::iterator;

            using Inherit::Inherit;
        protected:
            friend Base;

            template <typename AccIt> class Iterator_base;

            const_iterator make_const_iterator_impl(Const_access_iterator it) const noexcept
                                                                                      { return it; }
            iterator make_iterator_impl(Access_iterator it) noexcept                  { return it; }

            const auto& caccess_impl(const Accessor& acc) const noexcept            { return *acc; }
            auto& access_impl(const Accessor& acc) noexcept                         { return *acc; }
        };

        template <typename B>
        class Mixin<B, true> : public Inherit<B> {
        public:
            using Inherit = unsot::Inherit<B>;
            using typename Inherit::That;
            using typename Inherit::Base;

            using typename Inherit::Container;

            using typename Inherit::It;

            static_assert(Inherit::random_access_v);

            using typename Inherit::Ptr;

            using typename Inherit::Accessor;

            using typename Inherit::Const_access_iterator;
            using typename Inherit::Access_iterator;

            using typename Inherit::const_iterator;
            using typename Inherit::iterator;

            using Inherit::Inherit;
            Mixin()                                                                       = default;
            Mixin(const Mixin&)                                                            = delete;
            Mixin& operator =(const Mixin&)                                                = delete;
            Mixin(Mixin&&)                                                                = default;
            Mixin& operator =(Mixin&&)                                                    = default;
            explicit Mixin(Container&);

            void connect(Container&);

            using Inherit::push_back;
            void push_back(Ptr);
        protected:
            friend Base;
            friend typename Inherit::Cont_mixin;

            template <typename AccIt> class Iterator_base;

            void swap_impl(That&) noexcept;

            const_iterator make_const_iterator_impl(Const_access_iterator) const noexcept;
            iterator make_iterator_impl(Access_iterator) noexcept;

            const auto& ccont_l() const noexcept                                 { return _cont_l; }
            auto& cont_l() noexcept                                              { return _cont_l; }
            const auto& ccont() const noexcept                                { return *ccont_l(); }
            const auto& cont() const noexcept                                    { return ccont(); }
            auto& cont() noexcept                                              { return *cont_l(); }

            const auto& caccess_impl(const Accessor&) const noexcept;
            auto& access_impl(const Accessor& acc) noexcept;

            //+ not implemented for iterators, need to distinguish const/non-const
            Accessor to_accessor(Ptr) const noexcept;

            using Inherit::push_back_impl;
            template <typename ItT = It> void push_back_impl(ItT);
        private:
            /// (zero-initialization)
            Container* _cont_l{};
        };

        template <typename B>
        class Front_mixin : public Inherit<B> {
        public:
            using Inherit = unsot::Inherit<B>;
            using typename Inherit::That;
            using typename Inherit::Base;

            using typename Inherit::Accessor;

            using Inherit::Inherit;

            void push_front(Accessor);
            template <typename PtrT, typename AccT = Accessor,
                      Req<is_ptr_v<Decay<PtrT>> && !is_iterator_v<PtrT> && !is_same_v<Decay<PtrT>, AccT>> = 0>
                void push_front(PtrT);
            template <typename ItT, Req<is_iterator_v<ItT> && !is_ptr_v<Decay<ItT>>> = 0>
                void push_front(ItT);
            void pop_front();
        protected:
            void push_front_impl(Accessor);
            template <typename ItT> void push_front_impl(ItT);
            void pop_front_impl();
        };

        template <typename T> using Enabled_front = Void<decltype(&T::pop_front)>;

        template <typename V, typename ContT, template<typename...> typename AccCont,
                  typename C = Decay<ContT>,
                  bool randomAccess = is_random_access_iterator_v<typename C::iterator>,
                  typename PtrT = typename C::pointer,
                  typename AccT = Cond_t<!randomAccess, PtrT, typename C::size_type>>
            using Cont_crtp =
                Cond_mixin<enabled_v<Enabled_front, AccCont<AccT>>, Front_mixin,
                Mixin<Cont_mixin<Crtp<V, PtrT, AccCont, typename C::const_pointer, AccT,
                                      randomAccess>, ContT>, randomAccess>
                >;
    }

    template <typename V, typename ContT, template<typename...> typename AccCont>
    class Crtp : public Inherit<aux::Cont_crtp<V, ContT, AccCont>> {
    public:
        using Inherit = unsot::Inherit<aux::Cont_crtp<V, ContT, AccCont>>;

        using Inherit::Inherit;
    };

    template <typename V, typename T, template<typename...> typename AccCont>
    class Crtp<V, T*, AccCont>
        : public Inherit<Cond_mixin<enabled_v<aux::Enabled_front, AccCont<T*>>, aux::Front_mixin,
                         aux::Mixin<aux::Crtp<V, T*, AccCont>>
    >> {
    public:
        using Inherit = unsot::Inherit<Cond_mixin<enabled_v<aux::Enabled_front, AccCont<T*>>,
                                       aux::Front_mixin,
                                       aux::Mixin<aux::Crtp<V, T*, AccCont>>>>;

        using Inherit::Inherit;
    };

    template <typename B>
    class Unique_mixin : public Inherit<B, Unique_mixin<B>> {
    public:
        using Inherit = unsot::Inherit<B, Unique_mixin<B>>;
        using typename Inherit::This;
        using typename Inherit::Base;
        using typename Inherit::That;

        using typename Inherit::Accessor;
        using typename Inherit::Accessors;

        using Inherit::Inherit;

        bool contains(const Accessor&) const noexcept;
        template <typename T = This, typename P, Req<is_same_v<P, typename T::Ptr>> = 0>
            bool contains(P) const noexcept;

        void reserve(size_t size_);

        void clear();

        bool insert(Accessor);

        template <typename T = This> void merge(T&&);
    protected:
        friend Base;
        friend typename Inherit::Cont_mixin;
        template <typename> friend class aux::Front_mixin;

        using Accessors_set = Hash<Accessor>;

        void swap_impl(That&) noexcept;

        const auto& caccessors_set() const noexcept                       { return _accessors_set; }
        auto& accessors_set() noexcept                                    { return _accessors_set; }

        /// Cannot use for `push_front_impl` (it may not be available)
        using Inherit::push_back_impl;
        void push_back_impl(Accessor);
        void pop_back_impl();
        template <typename T = This, typename Arg> void push_front_impl(Arg&&);
        template <typename T = This> void pop_front_impl();
    private:
        Accessors_set _accessors_set{};
    };
}

namespace unsot::util {
    template <typename T, template<typename...> typename AccCont>
    class View final : public Inherit<view::Crtp<View<T, AccCont>, T, AccCont>> {
    public:
        using Inherit = unsot::Inherit<view::Crtp<View<T, AccCont>, T, AccCont>>;
        using typename Inherit::This;

        using Inherit::Inherit;
    };

    template <typename T, template<typename...> typename AccCont>
    class Unique_view final
        : public Inherit<view::Unique_mixin<view::Crtp<Unique_view<T, AccCont>, T, AccCont>>> {
    public:
        using Inherit =
            unsot::Inherit<view::Unique_mixin<view::Crtp<Unique_view<T, AccCont>, T, AccCont>>>;
        using typename Inherit::This;

        using Inherit::Inherit;
    };
}

namespace unsot::util::view::aux {
    template <typename V, typename PtrT, template<typename...> typename AccCont,
              typename CPtrT, typename AccT, bool randomAccess>
    template <typename AccIt>
    class Crtp<V, PtrT, AccCont, CPtrT, AccT, randomAccess>::Iterator_crtp
        : public Inherit<AccIt, Iterator_crtp<AccIt>>,
          public unsot::Crtp<typename V::template Iterator_base<AccIt>> {
    public:
        using Inherit = unsot::Inherit<AccIt, Iterator_crtp<AccIt>>;

        using value_type = typename Crtp::value_type;
        using reference = typename Crtp::reference;
        using pointer = typename Crtp::pointer;
        using size_type = typename Crtp::size_type;

        using Inherit::Inherit;

        String to_string() const&;
    protected:
        const auto& cderef() const noexcept;
        auto& deref() noexcept;
    };

    template <typename V, typename PtrT, template<typename...> typename AccCont,
              typename CPtrT, typename AccT, bool randomAccess>
    struct Crtp<V, PtrT, AccCont, CPtrT, AccT, randomAccess>::const_iterator
        : public Inherit<typename V::template Iterator_base<Const_access_iterator>> {
        using Inherit = unsot::Inherit<typename V::template Iterator_base<Const_access_iterator>>;

        using Inherit::Inherit;

        const auto& operator *() const noexcept                        { return Inherit::cderef(); }

        const_iterator& operator ++() noexcept;
        const_iterator operator ++(int) noexcept;
        const_iterator operator +(int) const noexcept;
        const_iterator& operator --() noexcept;
        const_iterator operator --(int) noexcept;
        const_iterator operator -(int) const noexcept;
    };

    template <typename V, typename PtrT, template<typename...> typename AccCont,
              typename CPtrT, typename AccT, bool randomAccess>
    struct Crtp<V, PtrT, AccCont, CPtrT, AccT, randomAccess>::iterator
        : public Inherit<typename V::template Iterator_base<Access_iterator>> {
        using Inherit = unsot::Inherit<typename V::template Iterator_base<Access_iterator>>;

        using Inherit::Inherit;

        const auto& operator *() const noexcept                        { return Inherit::cderef(); }
        auto& operator *() noexcept                                     { return Inherit::deref(); }

        iterator& operator ++() noexcept;
        iterator operator ++(int) noexcept;
        iterator operator +(int) const noexcept;
        iterator& operator --() noexcept;
        iterator operator --(int) noexcept;
        iterator operator -(int) const noexcept;
    };

    template <typename B, bool randomAccess>
    template <typename AccIt>
    class Mixin<B, randomAccess>::Iterator_base
        : public unsot::Inherit<typename B::template Iterator_crtp<AccIt>> {
    public:
        using Inherit = unsot::Inherit<typename B::template Iterator_crtp<AccIt>>;

        using Inherit::Inherit;
    protected:
        friend typename Inherit::Iterator_crtp;

        const auto& cderef() const noexcept;
        auto& deref() noexcept;
    };

    template <typename B>
    template <typename AccIt>
    class Mixin<B, true>::Iterator_base
        : public unsot::Inherit<typename B::template Iterator_crtp<AccIt>> {
    public:
        using Inherit = unsot::Inherit<typename B::template Iterator_crtp<AccIt>>;
        using typename Inherit::Parent;

        using Container_link = Cond_t<is_same_v<AccIt, typename Mixin::Access_iterator>,
                                      Container*, const Container*>;

        Iterator_base()                                                                   = default;
        ~Iterator_base()                                                                  = default;
        Iterator_base(const Iterator_base&)                                               = default;
        Iterator_base& operator =(const Iterator_base&)                                   = default;
        Iterator_base(Iterator_base&&)                                                    = default;
        Iterator_base& operator =(Iterator_base&&)                                        = default;
        Iterator_base(Parent, Container_link);
    protected:
        friend typename Inherit::Iterator_crtp;

        const auto& ccont_l() const noexcept                                     { return _cont_l; }
        auto& cont_l() noexcept                                                  { return _cont_l; }

        const auto& cderef() const noexcept;
        auto& deref() noexcept;
    private:
        /// (zero-initialization)
        Container_link _cont_l{};
    };
}

#include "util/view.inl"
