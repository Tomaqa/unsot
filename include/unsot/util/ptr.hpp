#pragma once

#include <memory>

#define MAKE_UNIQUE(obj) make_unique<DECAY(obj)>(obj)
#define MAKE_SHARED(obj) make_shared<DECAY(obj)>(obj)

namespace unsot::util {
    template <typename B> class Smart_ptr_mixin;

    using std::make_unique;
    using std::make_shared;

    /// Copies the pointer if it is not unique
    template <typename P> void maybe_shallow_copy(P&);
    template <typename P> void maybe_deep_copy(P&);
}

namespace unsot::util {
    template <typename B>
    class Smart_ptr_mixin : public Inherit<B, Smart_ptr_mixin<B>> {
    public:
        using Inherit = unsot::Inherit<B, Smart_ptr_mixin<B>>;
        using typename Inherit::This;
        using Base = typename Inherit::Parent;

        using Inherit::Inherit;

        String to_string() const;

        bool equals(const This&) const noexcept;
    };

    template <typename T>
    struct Unique_ptr : Inherit<Smart_ptr_mixin<std::unique_ptr<T>>> {
        using Inherit = unsot::Inherit<Smart_ptr_mixin<std::unique_ptr<T>>>;

        using Inherit::Inherit;

        bool unique() const noexcept                            { return true; }
    };

    template <typename T>
    struct Shared_ptr : Inherit<Smart_ptr_mixin<std::shared_ptr<T>>> {
        using Inherit = unsot::Inherit<Smart_ptr_mixin<std::shared_ptr<T>>>;

        using Inherit::Inherit;

        bool unique() const noexcept;
    };
}

#include "util/ptr.inl"
