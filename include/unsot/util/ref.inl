#pragma once

namespace unsot::util {
    template <typename T>
    template <typename T_>
    constexpr Ref<T>::Ref(T_&& i)
        : Inherit(&i)
    { }

    template <typename T>
    constexpr Ref<T>::Ref(Item&& i)
        : Inherit(std::move(i))
    { }

    template <typename T>
    constexpr Ref<T>::Ref(const Item&& i)
        : Inherit(std::move(i))
    { }

    template <typename T>
    Ref<T>::Ref(Tag<Item>)
        : Inherit(Item())
    { }

    template <typename T>
    template <typename T_, typename>
    constexpr Ref<T>& Ref<T>::operator =(T_&& i)
    {
        if constexpr (is_same_v<decltype(i), Item&&> || is_same_v<decltype(i), const Item&&>)
            Inherit::Parent::operator =(std::move(i));
        else Inherit::Parent::operator =(&i);
        return *this;
    }

    template <typename T>
    template <typename R>
    constexpr auto& Ref<T>::item_impl(R&& r)
    {
        return r.visit([](auto& arg) -> Forward_as_ref<T, decltype(arg)> {
            if constexpr (is_ptr_v<Rm_ref<decltype(arg)>>) return *arg;
            else return arg;
        });
    }

    template <typename T>
    constexpr bool Ref<T>::is_owner() const noexcept
    {
        return this->template valid<Item>();
    }

    template <typename T>
    String Ref<T>::to_string() const&
    {
        return to_string_impl(*this);
    }

    template <typename T>
    String Ref<T>::to_string()&&
    {
        return to_string_impl(std::move(*this));
    }

    template <typename T>
    template <typename R>
    String Ref<T>::to_string_impl(R&& r)
    {
        using unsot::to_string;
        return to_string(item_impl(FORWARD(r)));
    }
}
