#pragma once

namespace unsot::util {
    template <typename KeyT, typename ValueT>
    String Map<KeyT, ValueT>::to_string() const
    {
        using unsot::to_string;
        String str;
        for (const auto& e : *this) {
            str += to_string(e) + ", ";
        }
        return str;
    }
}
