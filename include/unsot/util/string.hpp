#pragma once

#include "util.hpp"

#include <string_view>

namespace unsot::util {
    template <typename Arg, typename Str, Req<is_string_v<Str>> = 0>
        Optional<Arg> to_value(Str&&) noexcept;
    template <typename Arg, typename Str, Req<is_string_v<Str>> = 0>
        Arg to_value_check(Str&&);
}

namespace unsot::util {
    //+ extend of SSO (if necessary?)
    class C_str : public Static<C_str> {
    public:
        using Data = char*;
        using Const_data = const char*;

        C_str()                                                     = default;
        ~C_str();
        C_str(const C_str&);
        C_str& operator =(const C_str&);
        C_str(C_str&&);
        C_str& operator =(C_str&&);
        C_str(Const_data);
        /// The data is retaken and must be safely freeable with `free'
        C_str(Data);
        C_str& operator =(Const_data);
        C_str& operator =(Data);
        C_str(const String&);
        C_str& operator =(const String&);
        /// Only const version available,
        /// as it is impossible to safely move `std::string's internal buffer,
        /// even with non-const `string::data' since C++17

        Const_data cdata() const noexcept                    { return _data; }
        Data data() noexcept                                 { return _data; }

        operator Const_data() const noexcept               { return cdata(); }
        operator Data() noexcept                            { return data(); }

        operator String() const noexcept                   { return cdata(); }

        void clear() noexcept;

        String to_string() const;
    private:
        Data _data{};
    };

    struct String_view : Inherit<std::string_view, String_view> {
        using typename Inherit::This;

        using Inherit::Inherit;
        String_view()                                               = default;
        String_view(const String&);

        String to_string() const;
        operator String() const                        { return to_string(); }

        bool equals(const This&) const;
    };
}

#include "util/string.inl"
