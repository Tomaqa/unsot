#pragma once

namespace unsot::util {
    template <typename F>
    Scope_guard<F>::~Scope_guard()
    {
        if constexpr (enabled_bool_cast_v<F>) {
            if (_guard) _guard();
        }
        else _guard();
    }

    template <typename F>
    Scope_guard<F>::Scope_guard(F guard_)
        : _guard(std::move(guard_))
    { }

    template <typename F>
    void Scope_guard<F>::set_guard(F guard_)
    {
        _guard = std::move(guard_);
    }

    template <typename F>
    void Scope_guard<F>::unset_guard()
    {
        set_guard(nullptr);
    }
}
