#pragma once

namespace unsot::util {
    template <typename... Ts>
    template <typename... Args>
    constexpr Union<Ts...>::Union(Args&&... args)
        : _var(FORWARD(args)...)
    { }

    template <typename... Ts>
    constexpr Union<Ts...>& Union<Ts...>::operator =(const Base& var_)
    {
        _var = var_;
        return *this;
    }

    template <typename... Ts>
    constexpr Union<Ts...>& Union<Ts...>::operator =(Base&& var_)
    {
        _var = std::move(var_);
        return *this;
    }

    template <typename... Ts>
    template <typename T, typename>
    Union<Ts...>& Union<Ts...>::operator =(T&& arg)
    {
        _var = FORWARD(arg);
        return *this;
    }

    template <typename... Ts>
    void Union<Ts...>::swap(Union& rhs) noexcept
    {
        _var.swap(rhs._var);
    }

    template <typename... Ts>
    constexpr size_t Union<Ts...>::index() const noexcept
    {
        return cvar().index();
    }

    template <typename... Ts>
    constexpr bool Union<Ts...>::invalid() const noexcept
    {
        return cvar().valueless_by_exception();
    }

    template <typename... Ts>
    template <typename T>
    constexpr bool Union<Ts...>::valid() const noexcept
    {
        return std::holds_alternative<T>(cvar());
    }

    template <typename... Ts>
    template <typename T>
    constexpr const T& Union<Ts...>::cget() const
    {
        assert(valid());
        return get_impl<T>(cvar());
    }

    template <typename... Ts>
    template <typename T>
    constexpr T& Union<Ts...>::get()&
    {
        assert(valid());
        return get_impl<T>(var());
    }

    template <typename... Ts>
    template <size_t idx>
    constexpr const auto& Union<Ts...>::cget() const
    {
        assert(valid());
        return get_impl<idx>(cvar());
    }

    template <typename... Ts>
    template <size_t idx>
    constexpr auto& Union<Ts...>::get()&
    {
        assert(valid());
        return get_impl<idx>(var());
    }

    template <typename... Ts>
    template <typename T, typename U>
    auto&& Union<Ts...>::get_impl(U&& u)
    try {
        return std::get<T>(FORWARD(u));
    }
    catch (const std::bad_variant_access&) {
        THROW("Attempt to get invalid type of Union: ")
            + typeid(Decay<T>).name();
    }

    template <typename... Ts>
    template <size_t idx, typename U>
    auto&& Union<Ts...>::get_impl(U&& u)
    {
        return get_impl<std::variant_alternative_t<idx, U>>(FORWARD(u));
    }

    template <typename... Ts>
    template <typename T>
    constexpr const T* Union<Ts...>::cget_l() const noexcept
    {
        return std::get_if<T>(&cvar());
    }

    template <typename... Ts>
    template <typename T>
    constexpr T* Union<Ts...>::get_l() noexcept
    {
        return std::get_if<T>(&var());
    }

    template <typename... Ts>
    template <size_t idx>
    constexpr auto Union<Ts...>::cget_l() const noexcept
    {
        return std::get_if<idx>(&cvar());
    }

    template <typename... Ts>
    template <size_t idx>
    constexpr auto Union<Ts...>::get_l() noexcept
    {
        return std::get_if<idx>(&var());
    }

    template <typename... Ts>
    template <typename T>
    Optional<T> Union<Ts...>::to() const& noexcept
    {
        return to_impl<T, false>(*this);
    }

    template <typename... Ts>
    template <typename T>
    Optional<T> Union<Ts...>::to()&& noexcept
    {
        return to_impl<T, false>(std::move(*this));
    }

    template <typename... Ts>
    template <typename T>
    T Union<Ts...>::to_check() const&
    {
        return to_impl<T, true>(*this);
    }

    template <typename... Ts>
    template <typename T>
    T Union<Ts...>::to_check()&&
    {
        return to_impl<T, true>(std::move(*this));
    }

    template <typename... Ts>
    template <typename T, bool check, typename U>
    auto Union<Ts...>::to_impl(U&& u) noexcept(!check)
    {
        using RetT = Cond_t<check, T, Optional<T>>;
        static const auto f = [](auto&& arg) -> RetT {
            using Arg = decltype(arg);
            if constexpr (is_constructible_v<T, Arg>)
                return FORWARD(arg);
            else if constexpr (!check) return {};
            else THROW("Conversion of Union value<")
                 + typeid(Arg).name() + "> to <"
                 + typeid(T).name() + "> failed";
        };

        if constexpr (!check) return visit_impl(f, FORWARD(u));
        else try {
            return visit_impl(f, FORWARD(u));
        }
        catch (const Error& err) {
            if constexpr (check) throw err + ": "s + u.to_string();
        }
    }

    template <typename... Ts>
    template <typename T, typename... Args>
    T& Union<Ts...>::emplace(Args&&... args)
    {
        return var().template emplace<T>(FORWARD(args)...);
    }

    template <typename... Args>
    template <typename F, typename... Unions>
    constexpr decltype(auto)
    Union<Args...>::cvisit(F&& f, Unions&&... others) const
    {
        assert(valid());
        return visit_impl(FORWARD(f), *this, FORWARD(others)...);
    }

    template <typename... Args>
    template <typename F, typename... Unions>
    constexpr decltype(auto) Union<Args...>::visit(F&& f, Unions&&... others)
    {
        assert(valid());
        return visit_impl(FORWARD(f), *this, FORWARD(others)...);
    }

    template <typename... Args>
    template <typename F, typename... Unions>
    constexpr decltype(auto)
    Union<Args...>::visit_impl(F&& f, Unions&&... unions)
    {
        constexpr auto var_f = [](auto&& u) -> auto& { return u.var(); };

        return std::visit(FORWARD(f), var_f(FORWARD(unions)...));
    }

    template <typename... Ts>
    String Union<Ts...>::to_string() const&
    {
        using unsot::to_string;
        static const auto f = [](const auto& arg){ return to_string(arg); };
        return to_string_impl(f, *this);
    }

    template <typename... Ts>
    String Union<Ts...>::to_string()&&
    {
        using unsot::to_string;
        static const auto f = [](auto&& arg){
            return to_string(std::move(arg));
        };
        return to_string_impl(f, std::move(*this));
    }

    template <typename... Ts>
    template <typename F, typename U>
    String Union<Ts...>::to_string_impl(F&& f, U&& u)
    {
        if (u.invalid()) return "<invalid>";
        return visit_impl(FORWARD(f), FORWARD(u));
    }

    template <typename... Ts>
    constexpr bool Union<Ts...>::equals(const This& rhs) const noexcept
    {
        return cvar() == rhs.cvar();
    }
}

////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////

namespace unsot::util {
    template <typename T, typename... Ts>
    constexpr bool valid(const Union<Ts...>& u) noexcept
    {
        return u.template valid<T>();
    }

    template <typename T, typename... Ts>
    constexpr const T& get(const Union<Ts...>& u)
    {
        return u.template get<T>();
    }

    template <typename T, typename... Ts>
    constexpr T& get(Union<Ts...>& u)
    {
        return u.template get<T>();
    }

    template <typename T, typename... Ts>
    constexpr T&& get(Union<Ts...>&& u)
    {
        return std::move(u).template get<T>();
    }

    template <typename T, typename... Ts>
    constexpr const T* get_l(const Union<Ts...>& u) noexcept
    {
        return u.template get_l<T>();
    }

    template <typename T, typename... Ts>
    constexpr T* get_l(Union<Ts...>& u) noexcept
    {
        return u.template get_l<T>();
    }

    template <typename T, typename... Ts>
    Optional<T> to(const Union<Ts...>& u) noexcept
    {
        return u.template to<T>();
    }

    template <typename T, typename... Ts>
    Optional<T> to(Union<Ts...>&& u) noexcept
    {
        return std::move(u).template to<T>();
    }

    template <typename T, typename... Ts>
    T to_check(const Union<Ts...>& u)
    {
        return u.template to_check<T>();
    }

    template <typename T, typename... Ts>
    T to_check(Union<Ts...>&& u)
    {
        return std::move(u).template to_check<T>();
    }
}
