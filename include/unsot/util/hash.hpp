#pragma once

#include "util/associative.hpp"

#include <unordered_set>
#include <unordered_map>

namespace unsot::util {
    template <typename KeyT, typename ValueT, typename HashT>
    class Hash
        : public Inherit<Map_mixin<std::unordered_map<KeyT, ValueT, HashT>>,
                         Hash<KeyT, ValueT, HashT>> {
        //+ : public Inherit<Map_mixin<std::unordered_map<KeyT, ValueT, HashT,
        //+                                               equal_to<>>>,
        //+                  Hash<KeyT, ValueT, HashT>> {
    public:
        using Inherit =
            unsot::Inherit<Map_mixin<std::unordered_map<KeyT, ValueT, HashT>>,
                           Hash<KeyT, ValueT, HashT>>;
        //+ using Inherit = unsot::Inherit<
        //+     Map_mixin<std::unordered_map<KeyT, ValueT, HashT, equal_to<>>>,
        //+     Hash<KeyT, ValueT, HashT>>;
        using typename Inherit::This;
        using typename Inherit::Base;

        using Inherit::Inherit;
        using Inherit::Parent::operator =;

        String to_string() const;
    };

    template <typename KeyT>
    class Hash<KeyT>
        : public Inherit<Associative_mixin<std::unordered_set<KeyT>>, Hash<KeyT>> {
        //+ : public Inherit<Associative_mixin<
        //+     std::unordered_set<KeyT, std::hash<KeyT>, equal_to<>>>, Hash<KeyT>> {
    public:
        using Inherit = unsot::Inherit<Associative_mixin<std::unordered_set<KeyT>>, Hash<KeyT>>;
        //+ using Inherit = unsot::Inherit<Associative_mixin<
        //+     std::unordered_set<KeyT, std::hash<KeyT>, equal_to<>>>, Hash<KeyT>>;
        using typename Inherit::This;
        using typename Inherit::Base;

        using Inherit::Inherit;
        using Inherit::Parent::operator =;

        String to_string() const;
    };

    //+ template <typename T>
    //+ class Hash_object : public Object<T> {
    //+ public:
    //+     size_t hash() const;
    //+ protected:
    //+     Hash_object()                                               = default;
    //+     ~Hash_object()                                              = default;
    //+     Hash_object(const Hash_object&)                             = default;
    //+     Hash_object& operator =(const Hash_object&)                 = default;
    //+     Hash_object(Hash_object&&)                                  = default;
    //+     Hash_object& operator =(Hash_object&&)                      = default;
    //+ };
}

//+ namespace std {
//+     template <typename T> struct hash<unsot::util::Hash_object<T>>;
//+ }

#include "util/hash.inl"
