#pragma once

#include <cmath>

namespace unsot::util {
    using std::abs;
    using std::ceil;
    using std::floor;
    using std::round;
    using std::sqrt;
    using std::cbrt;
    using std::sin;
    using std::cos;
    using std::tan;
    using std::exp;
    using std::log;
    using std::pow;
}

namespace unsot::util::numeric {
    constexpr double def_rel_eps = 1e-6;
    constexpr double def_abs_eps = 1e-6;
    constexpr double half_def_rel_eps = def_rel_eps/2.;
    constexpr double half_def_abs_eps = def_abs_eps/2.;
}
