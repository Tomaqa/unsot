#pragma once

#include "util/run.hpp"

namespace unsot::util {
    Path common_dirname(const Path&, const Path& = pwd());
}
