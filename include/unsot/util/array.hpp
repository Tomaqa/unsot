#pragma once

#include "util.hpp"

#include <array>

namespace unsot::util {
    using std::array;

    template <typename T, size_t n>
        constexpr array<std::remove_cv_t<T>, n> to_array(T(&&)[n]);
    template <typename T, size_t n>
        constexpr array<std::remove_cv_t<T>, n> to_array(T(&)[n]);

    template <typename T, size_t n1, size_t n2>
        constexpr array<T, n1+n2> array_cat(const array<T, n1>&, const array<T, n2>&);
}

#include "util/array.inl"
