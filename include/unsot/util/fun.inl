#pragma once

namespace unsot::util {
    template <typename F, typename... Args>
    decltype(auto) bind_front(F&& f, Args&&... args)
    {
        return [CAPTURE_RVAL(f), targs{MAKE_TUPLE(args)}]
               (auto&&... rest) mutable -> decltype(auto) {
            return apply(FORWARD(f), tuple_cat(FORWARD(targs), FORWARD_AS_TUPLE(rest)));
        };
    }

    template <typename RetT, typename U, typename T, typename... Args>
    decltype(auto) bind_this(RetT U::* f, T&& this_, Args&&... args)
    {
        return bind_front(f, FORWARD(this_), FORWARD(args)...);
    }
}
