#pragma once

namespace unsot::util {
    template <typename F, typename WrapF>
    auto wrap(F f, WrapF wrap_f)
    {
        return [CAPTURE_RVAL2(f, wrap_f)](auto&&... args) mutable -> decltype(auto) {
            if constexpr (is_member_function_pointer_v<decltype(f)>) {
                auto tup = MAKE_TUPLE(args);
                auto car = std::move(tuple_car(tup));
                auto cdr = tuple_cdr(std::move(tup));
                return invoke(std::move(f), std::move(car),
                              apply(std::move(wrap_f), std::move(cdr)));
            }
            else return invoke(std::move(f), invoke(std::move(wrap_f), FORWARD(args))...);
        };
    }

    template <typename BinF, typename InputIt2>
    auto as_unary(BinF f, InputIt2 first2)
    {
        //+ generalize to n-ary functions, not only binary?
        return [CAPTURE_RVAL(f), it2{first2}](auto&& elem1) mutable {
            return f(FORWARD(elem1), *it2++);
        };
    }

    template <typename ContT, typename UnPred, typename>
    bool all_of(const ContT& cont, UnPred pred)
    {
        return std::all_of(cbegin(cont), cend(cont), std::move(pred));
    }

    template <typename Cont1, typename InputIt2, typename BinPred,
              typename, typename>
    bool all_of(const Cont1& cont1, InputIt2 first2, BinPred pred)
    {
        return equal(cont1, first2, std::move(pred));
    }

    template <typename ContT, typename UnPred, typename>
    bool any_of(const ContT& cont, UnPred pred)
    {
        return std::any_of(cbegin(cont), cend(cont), std::move(pred));
    }

    template <typename Cont1, typename InputIt2, typename BinPred,
              typename, typename>
    bool any_of(const Cont1& cont1, InputIt2 first2, BinPred pred)
    {
        /// \exists . x <=> \not \forall . \not x
        return !none_of(cont1, first2, std::move(pred));
    }

    template <typename ContT, typename UnPred, typename>
    bool none_of(const ContT& cont, UnPred pred)
    {
        return std::none_of(cbegin(cont), cend(cont), std::move(pred));
    }

    template <typename Cont1, typename InputIt2, typename BinPred,
              typename, typename>
    bool none_of(const Cont1& cont1, InputIt2 first2, BinPred pred)
    {
        /// \forall . \not x
        return all_of(cont1, first2,
                      [&pred](const auto& arg1, const auto& arg2){
            return !pred(arg1, arg2);
        });
    }

    template <typename InputIt, typename BinPred, typename>
    bool all_equal(InputIt first, InputIt last, BinPred pred)
    {
        if (first == last) return true;
        const auto& e0 = *first;
        return std::all_of(++first, last, [&e0, &pred](const auto& e){
            return pred(e0, e);
        });
    }

    template <typename ContT, typename BinPred, typename>
    bool all_equal(const ContT& cont, BinPred pred)
    {
        return all_equal(cbegin(cont), cend(cont), std::move(pred));
    }

    template <typename InputIt, typename BinPred, typename>
    bool any_equal(InputIt first, InputIt last, BinPred pred)
    {
        for (auto it = first; it != last; ) {
            const auto& e0 = *it;
            if (std::any_of(++it, last, [&e0, &pred](const auto& e){
                return pred(e0, e);
            })) return true;
        }
        return false;
    }

    template <typename ContT, typename BinPred, typename>
    bool any_equal(const ContT& cont, BinPred pred)
    {
        return any_equal(cbegin(cont), cend(cont), std::move(pred));
    }

    template <typename InputIt, typename BinPred, typename>
    bool none_equal(InputIt first, InputIt last, BinPred pred)
    {
        return !any_equal(first, last, std::move(pred));
    }

    template <typename ContT, typename BinPred, typename>
    bool none_equal(const ContT& cont, BinPred pred)
    {
        return none_equal(cbegin(cont), cend(cont), std::move(pred));
    }

    template <typename Cont1, typename T2, typename BinPred, typename, typename>
    bool equal(const Cont1& cont1, const T2& t2, BinPred pred)
    {
        if constexpr (is_iterator_v<T2>)
            return std::equal(cbegin(cont1), cend(cont1), t2, std::move(pred));
        else {
            auto& cont2 = t2;
            if (size(cont1) != size(cont2)) return false;
            return equal(cont1, cbegin(cont2), std::move(pred));
        }
    }

    template <typename ContT, typename T, typename>
    auto find(const ContT& cont, const T& val)
    {
        return std::find(cbegin(cont), cend(cont), val);
    }

    template <typename ContT, typename T, typename>
    auto find(ContT& cont, const T& val)
    {
        return std::find(begin(cont), end(cont), val);
    }

    template <typename ContT, typename UnPred>
    auto find_if(const ContT& cont, UnPred pred)
    {
        return std::find_if(cbegin(cont), cend(cont), std::move(pred));
    }

    template <typename ContT, typename UnPred>
    auto find_if(ContT& cont, UnPred pred)
    {
        return std::find_if(begin(cont), end(cont), std::move(pred));
    }

    template <typename ContT, typename UnPred>
    auto find_if_not(const ContT& cont, UnPred pred)
    {
        return std::find_if_not(cbegin(cont), cend(cont), std::move(pred));
    }

    template <typename ContT, typename UnPred>
    auto find_if_not(ContT& cont, UnPred pred)
    {
        return std::find_if_not(begin(cont), end(cont), std::move(pred));
    }

    template <typename InputIt, typename T, typename>
    bool contains(InputIt first, InputIt last, const T& val)
    {
        return std::find(first, last, val) != last;
    }

    template <typename ContT, typename T, typename>
    bool contains(const ContT& cont, const T& val)
    {
        if constexpr (enabled_contains_v<ContT>)
            return cont.contains(val);
        else return find(cont, val) != end(cont);
    }

    template <typename T, typename U>
    bool contains(initializer_list<T> ils, const U& value)
    {
        return contains(ils.begin(), ils.end(), value);
    }

    template <typename ContT, typename T, typename>
    auto count(const ContT& cont, const T& val)
    {
        return std::count(cbegin(cont), cend(cont), val);
    }

    template <typename ContT, typename UnPred, typename>
    auto count_if(const ContT& cont, UnPred pred)
    {
        return std::count_if(cbegin(cont), cend(cont), std::move(pred));
    }

    template <typename ContT, typename OutputIt>
    OutputIt copy(const ContT& cont, OutputIt d_first)
    {
        return std::copy(cbegin(cont), cend(cont), d_first);
    }

    template <typename ContT, typename OutputIt>
    OutputIt move(ContT&& cont, OutputIt d_first)
    {
        return move(cont, d_first);
    }

    template <typename ContT, typename OutputIt>
    OutputIt move(ContT& cont, OutputIt d_first)
    {
        return std::move(begin(cont), end(cont), d_first);
    }

    template <typename InputIt, typename SizeT, typename OutputIt>
    OutputIt move_n(InputIt first1, SizeT n, OutputIt d_first)
    {
        return std::copy_n(make_move_iterator(first1), n, d_first);
    }

    template <typename ContT, typename UnF>
    UnF for_each(const ContT& cont, UnF f)
    {
        return std::for_each(cbegin(cont), cend(cont), std::move(f));
    }

    template <typename ContT, typename UnF>
    UnF for_each(ContT&& cont, UnF f)
    {
        return std::for_each(make_move_iterator(begin(cont)),
                             make_move_iterator(end(cont)), std::move(f));
    }

    template <typename ContT, typename UnF>
    UnF for_each(ContT& cont, UnF f)
    {
        return std::for_each(begin(cont), end(cont), std::move(f));
    }

    template <typename InputIt1, typename InputIt2, typename BinF>
    BinF for_each(InputIt1 first1, InputIt1 last1, InputIt2 first2, BinF f)
    {
        std::for_each(first1, last1, as_unary(f, first2));
        return f;
    }

    template <typename Cont1, typename InputIt2, typename BinF, typename>
    BinF for_each(const Cont1& cont1, InputIt2 first2, BinF f)
    {
        return for_each(cbegin(cont1), cend(cont1), first2, std::move(f));
    }

    template <typename Cont1, typename InputIt2, typename BinF, typename>
    BinF for_each(Cont1&& cont1, InputIt2 first2, BinF f)
    {
        return for_each(make_move_iterator(begin(cont1)),
                        make_move_iterator(end(cont1)), first2, std::move(f));
    }

    template <typename Cont1, typename InputIt2, typename BinF, typename>
    BinF for_each(Cont1& cont1, InputIt2 first2, BinF f)
    {
        return for_each(begin(cont1), end(cont1), first2, std::move(f));
    }

    template <typename InputIt1, typename SizeT, typename InputIt2, typename BinF>
    InputIt1 for_each_n(InputIt1 first1, SizeT n, InputIt2 first2, BinF f)
    {
        return std::for_each_n(first1, n, as_unary(std::move(f), first2));
    }

    template <typename ContT, typename OutputIt, typename UnF>
    OutputIt transform(const ContT& cont, OutputIt d_first, UnF f)
    {
        return std::transform(cbegin(cont), cend(cont), d_first, std::move(f));
    }

    template <typename ContT, typename OutputIt, typename UnF>
    OutputIt transform(ContT&& cont, OutputIt d_first, UnF f)
    {
        return std::transform(make_move_iterator(begin(cont)),
                              make_move_iterator(end(cont)), d_first,
                              std::move(f));
    }

    template <typename ContT, typename OutputIt, typename UnF>
    OutputIt transform(ContT& cont, OutputIt d_first, UnF f)
    {
        return std::transform(begin(cont), end(cont), d_first, std::move(f));
    }

    template <typename Cont1, typename InputIt2, typename OutputIt, typename BinF, typename>
    OutputIt transform(const Cont1& cont1, InputIt2 first2, OutputIt d_first, BinF f)
    {
        return std::transform(cbegin(cont1), cend(cont1), first2, d_first, std::move(f));
    }

    template <typename Cont1, typename InputIt2, typename OutputIt, typename BinF, typename>
    OutputIt transform(Cont1&& cont1, InputIt2 first2, OutputIt d_first, BinF f)
    {
        return std::transform(make_move_iterator(begin(cont1)),
                              make_move_iterator(end(cont1)), first2, d_first,
                              std::move(f));
    }

    template <typename Cont1, typename InputIt2, typename OutputIt, typename BinF, typename>
    OutputIt transform(Cont1& cont1, InputIt2 first2, OutputIt d_first, BinF f)
    {
        return std::transform(begin(cont1), end(cont1), first2, d_first, std::move(f));
    }

    template <typename ContT, typename UnF>
    UnF for_each_cdr(const ContT& cont, UnF f)
    {
        if (empty(cont)) return f;
        return std::for_each(++cbegin(cont), cend(cont), std::move(f));
    }

    template <typename ContT, typename UnF>
    UnF for_each_cdr(ContT&& cont, UnF f)
    {
        return std::for_each(make_move_iterator(++begin(cont)),
                             make_move_iterator(end(cont)), std::move(f));
    }

    template <typename ContT, typename UnF>
    UnF for_each_cdr(ContT& cont, UnF f)
    {
        if (empty(cont)) return f;
        return std::for_each(++begin(cont), end(cont), std::move(f));
    }

    template <typename InputIt, typename UnPred>
    UnPred for_each_while(InputIt first, InputIt last, UnPred pred)
    {
        std::find_if_not(first, last, pred);
        return pred;
    }

    template <typename ContT, typename UnPred>
    UnPred for_each_while(const ContT& cont, UnPred pred)
    {
        find_if_not(cont, pred);
        return pred;
    }

    template <typename ContT, typename UnPred>
    UnPred for_each_while(ContT&& cont, UnPred pred)
    {
        return for_each_while(cont, std::move(pred));
    }

    template <typename ContT, typename UnPred>
    UnPred for_each_while(ContT& cont, UnPred pred)
    {
        find_if_not(cont, pred);
        return pred;
    }

    template <typename InputIt1, typename InputIt2, typename BinPred>
    BinPred for_each_while(InputIt1 first1, InputIt1 last1, InputIt2 first2, BinPred pred)
    {
        for_each_while(first1, last1, as_unary(pred, first2));
        return pred;
    }

    template <typename Cont1, typename InputIt2, typename BinPred, typename>
    BinPred for_each_while(const Cont1& cont1, InputIt2 first2, BinPred pred)
    {
        return for_each_while(cbegin(cont1), cend(cont1), first2,
                              std::move(pred));
    }

    template <typename Cont1, typename InputIt2, typename BinPred, typename>
    BinPred for_each_while(Cont1&& cont1, InputIt2 first2, BinPred pred)
    {
        return for_each_while(make_move_iterator(begin(cont1)),
                              make_move_iterator(end(cont1)), first2,
                              std::move(pred));
    }

    template <typename Cont1, typename InputIt2, typename BinPred, typename>
    BinPred for_each_while(Cont1& cont1, InputIt2 first2, BinPred pred)
    {
        return for_each_while(begin(cont1), end(cont1), first2,
                              std::move(pred));
    }

    template <typename InputIt, typename UnPred, typename UnF>
    UnF for_each_if(InputIt first, InputIt last, UnPred pred, UnF f)
    {
        for (; first != last; ++first) {
            if (auto& e = *first; pred(e)) f(e);
        }
        return f;
    }

    template <typename ContT, typename UnPred, typename UnF>
    UnF for_each_if(const ContT& cont, UnPred pred, UnF f)
    {
        return for_each_if(cbegin(cont), cend(cont),
                           std::move(pred), std::move(f));
    }

    template <typename ContT, typename UnPred, typename UnF>
    UnF for_each_if(ContT&& cont, UnPred pred, UnF f)
    {
        return for_each_if(make_move_iterator(begin(cont)),
                           make_move_iterator(end(cont)),
                           std::move(pred), std::move(f));
    }

    template <typename ContT, typename UnPred, typename UnF>
    UnF for_each_if(ContT& cont, UnPred pred, UnF f)
    {
        return for_each_if(begin(cont), end(cont),
                           std::move(pred), std::move(f));
    }

    template <typename InputIt1, typename InputIt2, typename BinPred, typename BinF>
    BinF for_each_if(InputIt1 first1, InputIt1 last1, InputIt2 first2, BinPred pred, BinF f)
    {
        for (; first1 != last1; ++first1, ++first2) {
            if (auto &e1 = *first1, &e2 = *first2; pred(e1, e2)) f(e1, e2);
        }
        return f;

    }

    template <typename Cont1, typename InputIt2, typename BinPred, typename BinF>
    BinF for_each_if(const Cont1& cont1, InputIt2 first2, BinPred pred, BinF f)
    {
        return for_each_if(cbegin(cont1), cend(cont1), first2,
                           std::move(pred), std::move(f));
    }

    template <typename Cont1, typename InputIt2, typename BinPred, typename BinF>
    BinF for_each_if(Cont1&& cont1, InputIt2 first2, BinPred pred, BinF f)
    {
        return for_each_if(make_move_iterator(begin(cont1)),
                           make_move_iterator(end(cont1)), first2,
                           std::move(pred), std::move(f));
    }

    template <typename Cont1, typename InputIt2, typename BinPred, typename BinF>
    BinF for_each_if(Cont1& cont1, InputIt2 first2, BinPred pred, BinF f)
    {
        return for_each_if(begin(cont1), end(cont1), first2,
                           std::move(pred), std::move(f));
    }

    template <typename InputIt, typename UnPred, typename UnThenF, typename UnElseF>
    UnThenF for_each_ite(InputIt first, InputIt last, UnPred pred, UnThenF then_f, UnElseF else_f)
    {
        for (; first != last; ++first) {
            if (auto& e = *first; pred(e)) then_f(e);
            else else_f(e);
        }
        return then_f;
    }

    template <typename ContT, typename UnPred, typename UnThenF, typename UnElseF>
    UnThenF for_each_ite(const ContT& cont, UnPred pred, UnThenF then_f, UnElseF else_f)
    {
        return for_each_ite(cbegin(cont), cend(cont), std::move(pred),
                            std::move(then_f), std::move(else_f));
    }

    template <typename ContT, typename UnPred, typename UnThenF, typename UnElseF>
    UnThenF for_each_ite(ContT&& cont, UnPred pred, UnThenF then_f, UnElseF else_f)
    {
        return for_each_ite(make_move_iterator(begin(cont)),
                            make_move_iterator(end(cont)), std::move(pred),
                            std::move(then_f), std::move(else_f));
    }

    template <typename ContT, typename UnPred, typename UnThenF, typename UnElseF>
    UnThenF for_each_ite(ContT& cont, UnPred pred, UnThenF then_f, UnElseF else_f)
    {
        return for_each_ite(begin(cont), end(cont), std::move(pred),
                            std::move(then_f), std::move(else_f));
    }

    template <typename InputIt1, typename InputIt2, typename BinPred,
              typename BinThenF, typename BinElseF>
    BinThenF for_each_ite(InputIt1 first1, InputIt1 last1, InputIt2 first2, BinPred pred,
                          BinThenF then_f, BinElseF else_f)
    {
        for (; first1 != last1; ++first1, ++first2) {
            if (auto &e1 = *first1, &e2 = *first2; pred(e1, e2))
                then_f(e1, e2);
            else else_f(e1, e2);
        }
        return then_f;
    }

    template <typename Cont1, typename InputIt2, typename BinPred,
              typename BinThenF, typename BinElseF>
    BinThenF for_each_ite(const Cont1& cont1, InputIt2 first2, BinPred pred,
                          BinThenF then_f, BinElseF else_f)
    {
        return for_each_ite(cbegin(cont1), cend(cont1), first2, std::move(pred),
                            std::move(then_f), std::move(else_f));
    }

    template <typename Cont1, typename InputIt2, typename BinPred,
              typename BinThenF, typename BinElseF>
    BinThenF for_each_ite(Cont1&& cont1, InputIt2 first2, BinPred pred,
                          BinThenF then_f, BinElseF else_f)
    {
        return for_each_ite(make_move_iterator(begin(cont1)),
                            make_move_iterator(end(cont1)), first2,
                            std::move(pred),
                            std::move(then_f), std::move(else_f));
    }

    template <typename Cont1, typename InputIt2, typename BinPred,
              typename BinThenF, typename BinElseF>
    BinThenF for_each_ite(Cont1& cont1, InputIt2 first2, BinPred pred,
                          BinThenF then_f, BinElseF else_f)
    {
        return for_each_ite(begin(cont1), end(cont1), first2, std::move(pred),
                            std::move(then_f), std::move(else_f));
    }

    template <typename T, typename FromIt, typename SizeT, typename UnConvF,
              Req<is_integral_v<SizeT>>, typename E, Req<is_unary_conv_f_v<UnConvF, E>>>
    T to(FromIt first, FromIt last, SizeT size_, UnConvF conv)
    {
        T t;
        to(t, first, last, size_, std::move(conv));
        return t;
    }

    template <typename T, typename FromIt, typename UnConvF,
              typename E, Req<is_unary_conv_f_v<UnConvF, E>>>
    T to(FromIt first, FromIt last, UnConvF conv)
    {
        T t;
        to(t, first, last, std::move(conv));
        return t;
    }

    template <typename T, typename From, typename SizeT, typename UnConvF,
              Req<is_container_v<From>>, Req<is_integral_v<SizeT>>,
              typename E, Req<is_unary_conv_f_v<UnConvF, E>>>
    T to(From&& from, SizeT size_, UnConvF conv)
    {
        T t;
        to(t, FORWARD(from), size_, std::move(conv));
        return t;
    }

    template <typename T, typename From, typename UnConvF,
              Req<is_container_v<From>>, typename E, Req<is_unary_conv_f_v<UnConvF, E>>>
    T to(From&& from, UnConvF conv)
    {
        T t;
        to(t, FORWARD(from), std::move(conv));
        return t;
    }

    namespace aux {
        template <typename T, typename FromIt, typename UnConvF>
        void to_impl(T& t, FromIt first, FromIt last, UnConvF conv)
        {
            std::transform(first, last, back_inserter(t), [&conv](auto&& e){
                return conv(FORWARD(e));
            });
        }
    }

    template <typename T, typename FromIt, typename SizeT, typename UnConvF,
              Req<is_integral_v<SizeT>>, typename E, Req<is_unary_conv_f_v<UnConvF, E>>>
    void to(T& t, FromIt first, FromIt last, SizeT size_, UnConvF conv)
    {
        if (size_ == 0) return;
        t.reserve(size_);
        aux::to_impl(t, first, last, std::move(conv));
    }

    template <typename T, typename FromIt, typename UnConvF,
              typename E, Req<is_unary_conv_f_v<UnConvF, E>>>
    void to(T& t, FromIt first, FromIt last, UnConvF conv)
    {
        if constexpr (enabled_reserve_v<T>) {
            const long size_ = last - first;
            return to(t, first, last, size_, std::move(conv));
        }
        aux::to_impl(t, first, last, std::move(conv));
    }

    namespace aux {
        template <typename T, typename From, typename UnConvF>
        void to_impl(T& t, From&& from, UnConvF conv)
        {
            transform(FORWARD(from), back_inserter(t), [&conv](auto&& e){
                return conv(FORWARD(e));
            });
        }
    }

    template <typename T, typename From, typename SizeT, typename UnConvF,
              Req<is_container_v<From>>, Req<is_integral_v<SizeT>>,
              typename E, Req<is_unary_conv_f_v<UnConvF, E>>>
    void to(T& t, From&& from, SizeT size_, UnConvF conv)
    {
        if (size_ == 0) return;
        t.reserve(size_);
        aux::to_impl(t, FORWARD(from), std::move(conv));
    }

    template <typename T, typename From, typename UnConvF,
              Req<is_container_v<From>>, typename E, Req<is_unary_conv_f_v<UnConvF, E>>>
    void to(T& t, From&& from, UnConvF conv)
    {
        if constexpr (enabled_reserve_v<T>) {
            const size_t size_ = size(t) + size(from);
            return to(t, FORWARD(from), size_, std::move(conv));
        }
        aux::to_impl(t, FORWARD(from), std::move(conv));
    }

    template <typename T, typename FromIt1, typename FromIt2, typename SizeT, typename BinConvF,
              Req<is_integral_v<SizeT>>,
              typename E1, typename E2, Req<is_binary_conv_f_v<BinConvF, E1, E2>>>
    T to(FromIt1 first1, FromIt1 last1, FromIt2 first2, SizeT size_, BinConvF conv)
    {
        T t;
        to(t, first1, last1, first2, size_, std::move(conv));
        return t;
    }

    template <typename T, typename FromIt1, typename FromIt2, typename BinConvF,
              typename E1, typename E2, Req<is_binary_conv_f_v<BinConvF, E1, E2>>>
    T to(FromIt1 first1, FromIt1 last1, FromIt2 first2, BinConvF conv)
    {
        T t;
        to(t, first1, last1, first2, std::move(conv));
        return t;
    }

    template <typename T, typename From1, typename FromIt2, typename SizeT, typename BinConvF,
              Req<is_container_v<From1>>, Req<is_integral_v<SizeT>>,
              typename E1, typename E2, Req<is_binary_conv_f_v<BinConvF, E1, E2>>>
    T to(From1&& from1, FromIt2 first2, SizeT size_, BinConvF conv)
    {
        T t;
        to(t, FORWARD(from1), first2, size_, std::move(conv));
        return t;
    }

    template <typename T, typename From1, typename FromIt2, typename BinConvF,
              Req<is_container_v<From1>>,
              typename E1, typename E2, Req<is_binary_conv_f_v<BinConvF, E1, E2>>>
    T to(From1&& from1, FromIt2 first2, BinConvF conv)
    {
        T t;
        to(t, FORWARD(from1), first2, std::move(conv));
        return t;
    }

    namespace aux {
        template <typename T, typename FromIt1, typename FromIt2, typename BinConvF>
        void to_impl(T& t, FromIt1 first1, FromIt1 last1, FromIt2 first2, BinConvF conv)
        {
            std::transform(first1, last1, first2, back_inserter(t),
                      [&conv](auto&& e1, auto&& e2){
                return conv(FORWARD(e1), FORWARD(e2));
            });
        }
    }

    template <typename T, typename FromIt1, typename FromIt2, typename SizeT, typename BinConvF,
              Req<is_integral_v<SizeT>>,
              typename E1, typename E2, Req<is_binary_conv_f_v<BinConvF, E1, E2>>>
    void to(T& t, FromIt1 first1, FromIt1 last1, FromIt2 first2, SizeT size_, BinConvF conv)
    {
        if (size_ == 0) return;
        t.reserve(size_);
        aux::to_impl(t, first1, last1, first2, std::move(conv));
    }

    template <typename T, typename FromIt1, typename FromIt2, typename BinConvF,
              typename E1, typename E2, Req<is_binary_conv_f_v<BinConvF, E1, E2>>>
    void to(T& t, FromIt1 first1, FromIt1 last1, FromIt2 first2, BinConvF conv)
    {
        if constexpr (enabled_reserve_v<T>) {
            const long size_ = last1 - first1;
            return to(t, first1, last1, first2, size_, std::move(conv));
        }
        aux::to_impl(t, first1, last1, first2, std::move(conv));
    }

    namespace aux {
        template <typename T, typename From1, typename FromIt2, typename BinConvF>
        void to_impl(T& t, From1&& from1, FromIt2 first2, BinConvF conv)
        {
            transform(FORWARD(from1), first2, back_inserter(t),
                      [&conv](auto&& e1, auto&& e2){
                return conv(FORWARD(e1), FORWARD(e2));
            });
        }
    }

    template <typename T, typename From1, typename FromIt2, typename SizeT, typename BinConvF,
              Req<is_container_v<From1>>, Req<is_integral_v<SizeT>>,
              typename E1, typename E2, Req<is_binary_conv_f_v<BinConvF, E1, E2>>>
    void to(T& t, From1&& from1, FromIt2 first2, SizeT size_, BinConvF conv)
    {
        if (size_ == 0) return;
        t.reserve(size_);
        aux::to_impl(t, FORWARD(from1), first2, std::move(conv));
    }

    template <typename T, typename From1, typename FromIt2, typename BinConvF,
              Req<is_container_v<From1>>,
              typename E1, typename E2, Req<is_binary_conv_f_v<BinConvF, E1, E2>>>
    void to(T& t, From1&& from1, FromIt2 first2, BinConvF conv)
    {
        if constexpr (enabled_reserve_v<T>) {
            const size_t size_ = size(t) + size(from1);
            return to(t, FORWARD(from1), first2, size_, std::move(conv));
        }
        aux::to_impl(t, FORWARD(from1), first2, std::move(conv));
    }

    template <typename OutputCont, typename InputIt, typename SizeT>
    OutputCont& append(OutputCont& cont1, InputIt first2, InputIt last2,
                       [[maybe_unused]] SizeT size_)
    {
        if constexpr (is_constructible_v<OutputCont, InputIt, InputIt>) {
            if (empty(cont1)) {
                cont1 = OutputCont(first2, last2);
                return cont1;
            }
        }

        if constexpr (enabled_reserve_v<OutputCont>)
            cont1.reserve(size(cont1) + size_);
        std::copy(first2, last2, back_inserter(cont1));
        return cont1;
    }

    template <typename OutputCont, typename InputIt>
    OutputCont& append(OutputCont& cont1, InputIt first2, InputIt last2)
    {
        const long size_ = last2 - first2;
        return append(cont1, first2, last2, size_);
    }

    template <typename OutputCont, typename InputCont>
    OutputCont& append(OutputCont& cont1, const InputCont& cont2)
    {
        return append(cont1, cbegin(cont2), cend(cont2), size(cont2));
    }

    template <typename OutputCont, typename InputCont>
    OutputCont& append(OutputCont& cont1, InputCont&& cont2)
    {
        return append(cont1, make_move_iterator(begin(cont2)),
                      make_move_iterator(end(cont2)), size(cont2));
    }

    template <typename OutputCont, typename InputIt, typename SizeT>
    OutputCont cat(OutputCont cont1, InputIt first2, InputIt last2, SizeT size_)
    {
        return append(cont1, first2, last2, size_);
    }

    template <typename OutputCont, typename InputIt>
    OutputCont cat(OutputCont cont1, InputIt first2, InputIt last2)
    {
        return append(cont1, first2, last2);
    }

    template <typename OutputCont, typename InputCont>
    OutputCont cat(OutputCont cont1, InputCont&& cont2)
    {
        return append(cont1, FORWARD(cont2));
    }

    template <typename InputIt, typename SizeT, typename InputCont>
    auto cat(InputIt first1, InputIt last1, SizeT size_, InputCont&& cont2)
    {
        using OutputCont = DECAY(cont2);
        OutputCont out;
        if constexpr (enabled_reserve_v<OutputCont>)
            out.reserve(size_ + size(cont2));
        out.assign(first1, last1);
        return append(out, FORWARD(cont2));
    }

    template <typename InputIt, typename InputCont>
    auto cat(InputIt first1, InputIt last1, InputCont&& cont2)
    {
        const long size_ = last1 - first1;
        return cat(first1, last1, size_, FORWARD(cont2));
    }

    template <typename OutputCont, typename InputIt, typename UnPred>
    OutputCont split_if(InputIt first, InputIt last, UnPred pred)
    {
        OutputCont d_cont;
        if (first == last) return d_cont;
        auto first2 = first;
        for (++first2; first2 != last; ++first2) {
            if (!pred(*first2)) continue;
            d_cont.emplace_back(make_move_iterator(first),
                                make_move_iterator(first2));
            first = first2;
        }
        if (first != first2) {
            d_cont.emplace_back(make_move_iterator(first),
                                make_move_iterator(first2));
        }
        return d_cont;
    }

    template <typename OutputCont, typename InputCont, typename UnPred>
    OutputCont split_if(InputCont&& cont, UnPred pred)
    {
        return split_if<OutputCont>(begin(cont), end(cont), std::move(pred));
    }

    template <typename OutputCont, typename InputCont, typename UnPred>
    OutputCont inplace_split_if(InputCont& cont, UnPred pred)
    {
        if (empty(cont)) return {};
        auto last = end(cont);
        auto first = std::find_if(++begin(cont), last, std::move(pred));
        OutputCont d_cont =
            split_if<OutputCont>(first, last, std::move(pred));
        cont.erase(first, last);
        return d_cont;
    }

    template <typename ContT, typename>
    ContT reverse(ContT&& cont)
    {
        std::reverse(begin(cont), end(cont));
        return FORWARD(cont);
    }

    template <typename ContT>
    ContT reverse_copy(const ContT& cont)
    {
        ContT d_cont;
        if constexpr (enabled_reserve_v<ContT>) d_cont.reserve(size(cont));
        std::reverse_copy(cbegin(cont), cend(cont), back_inserter(d_cont));
        return d_cont;
    }

    template <typename ItT, typename, typename BinPred, typename, typename>
    bool is_sorted(ItT first, ItT last, BinPred pred)
    {
        return std::is_sorted(first, last, std::move(pred));
    }

    template <typename ItT, typename, typename UnConvF, typename BinPred,
              typename, typename>
    bool is_sorted(ItT first, ItT last, UnConvF conv, BinPred pred)
    {
        return util::is_sorted(first, last,
                               [&conv, &pred](const auto& e1, const auto& e2){
            return pred(conv(e1), conv(e2));
        });
    }

    template <typename ContT, typename, typename BinPred, typename, typename>
    bool is_sorted(const ContT& cont, BinPred pred)
    {
        return util::is_sorted(cbegin(cont), cend(cont), std::move(pred));
    }

    template <typename ContT, typename, typename UnConvF, typename BinPred,
              typename, typename>
    bool is_sorted(const ContT& cont, UnConvF conv, BinPred pred)
    {
        return is_sorted(cbegin(cont), cend(cont),
                         std::move(conv), std::move(pred));
    }

    template <typename ItT, typename, typename BinPred, typename, typename>
    void sort(ItT first, ItT last, BinPred pred)
    {
        std::sort(first, last, std::move(pred));
    }

    template <typename ItT, typename, typename UnConvF, typename BinPred,
              typename, typename>
    void sort(ItT first, ItT last, UnConvF conv, BinPred pred)
    {
        util::sort(first, last,
                   [&conv, &pred](const auto& e1, const auto& e2){
            return pred(conv(e1), conv(e2));
        });
    }

    template <typename ContT, typename, typename BinPred,
              typename, typename, typename>
    ContT sort(ContT&& cont, BinPred pred)
    {
        util::sort(begin(cont), end(cont), std::move(pred));
        return FORWARD(cont);
    }

    template <typename ContT, typename, typename UnConvF, typename BinPred,
              typename, typename, typename>
    ContT sort(ContT&& cont, UnConvF conv, BinPred pred)
    {
        sort(begin(cont), end(cont), std::move(conv), std::move(pred));
        return FORWARD(cont);
    }

    template <typename ItT, typename, typename BinPred, typename, typename>
    void stable_sort(ItT first, ItT last, BinPred pred)
    {
        std::stable_sort(first, last, std::move(pred));
    }

    template <typename ItT, typename, typename UnConvF, typename BinPred,
              typename, typename>
    void stable_sort(ItT first, ItT last, UnConvF conv, BinPred pred)
    {
        util::stable_sort(first, last,
                          [&conv, &pred](const auto& e1, const auto& e2){
            return pred(conv(e1), conv(e2));
        });
    }

    template <typename ContT, typename, typename BinPred,
              typename, typename, typename>
    ContT stable_sort(ContT&& cont, BinPred pred)
    {
        util::stable_sort(begin(cont), end(cont), std::move(pred));
        return FORWARD(cont);
    }

    template <typename ContT, typename, typename UnConvF, typename BinPred,
              typename, typename, typename>
    ContT stable_sort(ContT&& cont, UnConvF conv, BinPred pred)
    {
        stable_sort(begin(cont), end(cont), std::move(conv), std::move(pred));
        return FORWARD(cont);
    }

    template <typename ContT, typename, typename BinPred, typename, typename>
    ContT sort_copy(const ContT& cont, BinPred pred)
    {
        ContT d_cont(cont);
        sort(d_cont, std::move(pred));
        return d_cont;
    }

    template <typename ContT, typename, typename UnConvF, typename BinPred,
              typename, typename>
    ContT sort_copy(const ContT& cont, UnConvF conv, BinPred pred)
    {
        ContT d_cont(cont);
        sort(d_cont, std::move(conv), std::move(pred));
        return d_cont;
    }
}
