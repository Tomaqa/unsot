#pragma once

namespace unsot::util {
    template <typename ItT, typename RandomT>
    void shuffle(ItT first, ItT last, RandomT&& rnd)
    {
        std::shuffle(first, last, FORWARD(rnd));
    }

    template <typename ContT, typename RandomT>
    void shuffle(ContT& cont, RandomT&& rnd)
    {
        util::shuffle(begin(cont), end(cont), FORWARD(rnd));
    }
}
