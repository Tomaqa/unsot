#pragma once

#include "util.hpp"

#include "util/optional.hpp"

namespace unsot::util {
    template <typename M, typename UnPred>
        typename M::size_type erase_if(M&, UnPred);
}

namespace unsot::util {
    template <typename B, typename ValueT = typename B::value_type>
    class Associative_mixin : public Inherit<B> {
    public:
        using Inherit = unsot::Inherit<B>;
        using Base = typename Inherit::Parent;

        using Key = typename Inherit::key_type;
        using Value = ValueT;

        using Inherit::Inherit;
        using Base::operator =;

        bool contains(const Key&) const noexcept;
        //+ only since C++20; heterogenous lookup not usable until then
        //+ (which would prevent constructing `Key' instance on each lookup)
        //+ template <typename K> bool contains(const K&) const noexcept;

        using Base::insert;
        void insert(const Base&);
        void insert(Base&&);
    };

    template <typename B>
    class Map_mixin : public Inherit<Associative_mixin<B, typename B::mapped_type>> {
    public:
        using Inherit = unsot::Inherit<Associative_mixin<B, typename B::mapped_type>>;
        using typename Inherit::Base;

        using typename Inherit::Key;
        using typename Inherit::Value;
        using Pair = typename Inherit::value_type;

        using Inherit::Inherit;
        using Inherit::Parent::operator =;

        const Value& cat(const Key&) const;
        const Value& at(const Key& key) const                                   { return cat(key); }
        Value& at(const Key&);

        using Inherit::operator [];
        const Value& operator [](const Key&) const;

        using Inherit::extract;
        Optional<Value> extract_value(const Key&) noexcept;
    };
}

#include "util/associative.inl"
