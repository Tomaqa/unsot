#pragma once

#include "util.hpp"

#include "util/fun.hpp"

namespace unsot::util {
    template <typename F = Lazy<void>>
    class Scope_guard {
    public:
        Scope_guard()                                               = default;
        ~Scope_guard();
        Scope_guard(const Scope_guard&)                              = delete;
        Scope_guard& operator =(const Scope_guard&)                  = delete;
        Scope_guard(Scope_guard&&)                                  = default;
        Scope_guard& operator =(Scope_guard&&)                      = default;
        Scope_guard(F guard_);

        //! it does not work if the lambda has captures ???
        void set_guard(F guard_);
        void unset_guard();
    private:
        F _guard{};
    };
}

#include "util/scope_guard.inl"
