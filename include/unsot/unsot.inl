#pragma once

namespace unsot {
    template <typename T, typename MemFn, typename... Args>
    decltype(auto) call(T&& t, MemFn f, Args&&... args)
    {
        return (FORWARD(t).*move(f))(FORWARD(args)...);
    }

    template <typename T, typename U, typename>
    bool operator ==(const T& lhs, const U& rhs)
    {
        return lhs.equals(rhs);
    }

    //+ template <typename T, typename U, typename>
    //+ bool operator ==(const U& lhs, const T& rhs)
    //+ {
    //+     return rhs.equals(lhs);
    //+ }

    template <typename T, typename U, typename>
    bool operator <(const T& lhs, const U& rhs)
    {
        return lhs.less(rhs);
    }

    template <typename T, typename U, typename>
    void swap(T& lhs, U& rhs)
    {
        lhs.swap(rhs);
    }

    template <typename P, typename T, Req<is_ptr_v<T>>>
    auto get(P&& ptr)
    {
        return FORWARD(ptr).get();
    }

    template <typename T>
    T deep_copy(const T& t)
    {
        if constexpr (enabled_member_deep_copy_v<T>) return t.deep_copy();
        else return t;
    }

    template <typename T>
    decltype(auto) tuple_car(T&& t)
    {
       return get<0>(FORWARD(t));
    }

    namespace aux {
        template <size_t... is, typename T>
        auto tuple_cdr_impl(std::index_sequence<is...>, T&& t)
        {
           return make_tuple(get<is+1>(FORWARD(t))...);
        }
    }

    template <typename T>
    decltype(auto) tuple_cdr(T&& t)
    {
       auto index_seq = std::make_index_sequence<tuple_size_v<T>-1>();
       return aux::tuple_cdr_impl(move(index_seq), FORWARD(t));
    }
}
