#pragma once

namespace unsot::util {
    template <typename T, template<typename...> typename ConstraintT>
    constexpr bool Wrap<T, ConstraintT>::equals(const Wrap& rhs) const noexcept
    {
        return cbase() == rhs.cbase();
    }

    template <typename T, template<typename...> typename ConstraintT>
    constexpr bool Wrap<T, ConstraintT>::less(const Wrap& rhs) const noexcept
    {
        return cbase() < rhs.cbase();
    }

    ////////////////////////////////////////////////////////////////

    template <typename T, Req<is_integral_v<T>>>
    constexpr Idx operator +(Idx idx, T inc)
    {
        return idx += inc;
    }

    template <typename T, Req<is_integral_v<T>>>
    constexpr Idx operator -(Idx idx, T dec)
    {
        return idx -= dec;
    }

    template <typename T, Req<is_integral_v<T>>>
    constexpr Idx operator *(Idx idx, T mul)
    {
        return idx *= mul;
    }

    template <typename T, Req<is_integral_v<T>>>
    constexpr Idx operator /(Idx idx, T div)
    {
        return idx /= div;
    }

    template <typename T, Req<is_integral_v<T>>>
    constexpr Idx operator %(Idx idx, T mod)
    {
        return idx %= mod;
    }

    template <typename T, Req<is_integral_v<T>>>
    constexpr Idx operator &(Idx idx, T x)
    {
        return idx &= x;
    }

    template <typename T, Req<is_integral_v<T>>>
    constexpr Idx operator |(Idx idx, T x)
    {
        return idx |= x;
    }

    template <typename T, Req<is_integral_v<T>>>
    constexpr Idx operator ^(Idx idx, T x)
    {
        return idx ^= x;
    }

    template <typename T, Req<is_integral_v<T>>>
    constexpr Idx operator <<(Idx idx, T x)
    {
        return idx <<= x;
    }

    template <typename T, Req<is_integral_v<T>>>
    constexpr Idx operator >>(Idx idx, T x)
    {
        return idx >>= x;
    }

    template <typename T, Req<unsot::is_integral_v<T>>>
    constexpr Idx operator +(T inc, Idx idx)
    {
        return move(idx) + inc;
    }

    template <typename T, Req<unsot::is_integral_v<T>>>
    constexpr Idx operator -(T min, Idx idx)
    {
        return min - idx.base();
    }

    template <typename T, Req<unsot::is_integral_v<T>>>
    constexpr Idx operator *(T mul, Idx idx)
    {
        return move(idx) * mul;
    }

    template <typename T, Req<unsot::is_integral_v<T>>>
    constexpr Idx operator ^(T x, Idx idx)
    {
        return move(idx) ^ x;
    }

    template <typename T, Req<is_integral_v<T>>>
    constexpr Idx& operator +=(Idx& idx, T inc)
    {
        idx.base() += inc;
        return idx;
    }

    template <typename T, Req<is_integral_v<T>>>
    constexpr Idx& operator -=(Idx& idx, T dec)
    {
        idx.base() -= dec;
        return idx;
    }

    template <typename T, Req<is_integral_v<T>>>
    constexpr Idx& operator *=(Idx& idx, T mul)
    {
        idx.base() *= mul;
        return idx;
    }

    template <typename T, Req<is_integral_v<T>>>
    constexpr Idx& operator /=(Idx& idx, T div)
    {
        idx.base() /= div;
        return idx;
    }

    template <typename T, Req<is_integral_v<T>>>
    constexpr Idx& operator %=(Idx& idx, T mod)
    {
        idx.base() %= mod;
        return idx;
    }

    template <typename T, Req<is_integral_v<T>>>
    constexpr Idx& operator &=(Idx& idx, T x)
    {
        idx.base() &= x;
        return idx;
    }

    template <typename T, Req<is_integral_v<T>>>
    constexpr Idx& operator |=(Idx& idx, T x)
    {
        idx.base() |= x;
        return idx;
    }

    template <typename T, Req<is_integral_v<T>>>
    constexpr Idx& operator ^=(Idx& idx, T x)
    {
        idx.base() ^= x;
        return idx;
    }

    template <typename T, Req<is_integral_v<T>>>
    constexpr Idx& operator <<=(Idx& idx, T x)
    {
        idx.base() <<= x;
        return idx;
    }

    template <typename T, Req<is_integral_v<T>>>
    constexpr Idx& operator >>=(Idx& idx, T x)
    {
        idx.base() >>= x;
        return idx;
    }

    constexpr Idx& operator ++(Idx& idx)
    {
        ++idx.base();
        return idx;
    }

    constexpr Idx operator ++(Idx& idx, int)
    {
        Idx tmp = idx;
        ++idx;
        return tmp;
    }

    constexpr Idx& operator --(Idx& idx)
    {
        --idx.base();
        return idx;
    }

    constexpr Idx operator --(Idx& idx, int)
    {
        Idx tmp = idx;
        --idx;
        return tmp;
    }
}

////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////

namespace unsot::util {
    template <typename ItT>
    auto to_pointer(ItT it) noexcept
    {
        return &*it;
    }

    template <typename ContT>
    typename ContT::iterator to_iterator(typename ContT::const_iterator it) noexcept
    {
        ContT c;
        return c.erase(it, it);
    }

    bool masked(const Mask& mask, Idx idx)
    {
        return mask[idx];
    }
}
