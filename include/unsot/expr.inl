#pragma once

namespace unsot::expr {
    template <typename T>
    Ptr Place::make_ptr(T&& t)
    {
        return MAKE_SHARED(FORWARD(t));
    }
}

////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////

namespace unsot::expr {
    template <typename Arg, typename V, Req<is_values_v<V>>>
    Values_ptr<Arg> new_values(V&& vals)
    {
        return MAKE_SHARED(FORWARD(vals));
    }

    template <typename Arg>
    Values<Arg> cons_values(const Keys& keys)
    {
        return Values<Arg>(keys.size());
    }

    template <typename Arg>
    Values_ptr<Arg> new_values(const Keys& keys)
    {
        return new_values<Arg>(cons_values<Arg>(keys));
    }

    constexpr bool valid_key(const Key& key) noexcept
    {
        if (empty(key)) return true;
        return !isdigit(key[0]) && none_of(key, isspace);
    }

    constexpr void check_key(const Key& key)
    {
        using unsot::to_string;
        expect(valid_key(key), "Invalid expr key: "s + to_string(key));
    }

    template <typename K, Req<is_keys_v<K>>>
    Forward_as<Key, K> key(K&& keys, const Key_id& id) noexcept
    {
        return FORWARD(FORWARD(keys)[id]);
    }

    template <typename Arg, typename V, Req<is_values_v<V>>>
    Forward_as<Arg, V> value(V&& vals, const Key_id& id) noexcept
    {
        return FORWARD(FORWARD(vals)[id]);
    }

    template <typename K, Req<is_ids_map_v<K>>>
    Forward_as<Key_id, K> key_id(K&& ids_map, const Key& key_)
    {
        return FORWARD(FORWARD(ids_map)[key_]);
    }

    template <typename Arg>
    bool valid_values(const Values<Arg>& vals, const Keys& keys) noexcept
    {
        return size(vals) == size(keys);
    }

    template <typename Arg>
    void check_values(const Values<Arg>& vals, const Keys& keys)
    {
        expect(valid_values(vals, keys),
               "Count of argument values does not correspond to keys: "s
               + "expected " + to_string(size(keys))
               + ", got " + to_string(size(vals)));
    }

    template <typename Arg, typename V>
    Key_id add_key_value(Keys& keys, Values<Arg>& vals,
                         Ids_map& imap, Key key_, V val)
    {
        if constexpr (is_same_v<V, Dummy>) vals.resize(size(keys)+1);
        else vals.emplace_back(move(val));
        return add_key(keys, imap, move(key_));
    }
}
