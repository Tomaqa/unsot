#pragma once

#include "util.hpp"

namespace unsot::expr {
    using namespace util;
    using util::is_integral_v;

    class Place;
    class Elem_base;
    template <typename... Args> class Elem_tp;
    class List;
    class Formula;

    /// To allow easy sharing and pointing to others, e.g. in evaluation of such functions
    using Ptr = Shared_ptr<Place>;

    template <typename Arg> using Values = Vector<Arg>;
    template <typename Arg> using Values_ptr = Shared_ptr<Values<Arg>>;

    using Key = String;
    using Keys = Values<Key>;
    using Keys_ptr = Values_ptr<Key>;

    using Int = int;
    using Real = double;
    using Ints = Values<Int>;
    using Reals = Values<Real>;

    using Elem = Elem_tp<Int, Real>;

    using Bool = Int;
    using Bools = Values<Bool>;

    using Lists = Vector<List>;

    using Key_id = Idx;
    using Ids_map = Hash<Key, Key_id>;
    using Ids_map_ptr = Shared_ptr<Ids_map>;

    template <typename Sort> constexpr bool is_int_v = is_same_v<Sort, Int>;
    template <typename Sort> constexpr bool is_real_v = is_same_v<Sort, Real>;
    template <typename Sort> constexpr bool is_bool_v = is_same_v<Sort, Bool>;

    template <typename T> constexpr bool is_key_v = is_same_v<Decay<T>, Key>;
    template <typename T> constexpr bool is_ptr_v = is_same_v<Decay<T>, Ptr>;
    template <typename T> constexpr bool is_expr_v = is_derived_from_v<Decay<T>, Place>;
    template <typename T, typename E = Elem>
        constexpr bool is_elem_v = is_derived_from_v<Decay<T>, E>;
    template <typename T, typename E = Elem>
        constexpr bool is_elem_base_v = is_elem_v<T, Elem_base>;
    template <typename T> constexpr bool is_list_v = is_derived_from_v<Decay<T>, List>;
    template <typename T> constexpr bool is_formula_v = is_derived_from_v<Decay<T>, Formula>;

    template <typename T> constexpr bool is_keys_v = is_same_v<Decay<T>, Keys>;
    template <typename T, typename ContT = Decay<T>, typename Arg = typename ContT::value_type>
        constexpr bool is_values_v = is_same_v<ContT, Values<Arg>>;
    template <typename T> constexpr bool is_ids_map_v = is_same_v<Decay<T>, Ids_map>;

    template <typename K = const Keys&, Req<is_keys_v<K>> = 0>
        Keys_ptr new_keys(K&& = {});
    template <typename Arg, typename V = const Values<Arg>&, Req<is_values_v<V>> = 0>
        Values_ptr<Arg> new_values(V&& = {});
    template <typename K = const Ids_map&, Req<is_ids_map_v<K>> = 0>
        Ids_map_ptr new_ids_map(K&& = {});

    extern template Keys_ptr new_keys(const Keys&);
    extern template Keys_ptr new_keys(Keys&&);
    extern template Ids_map_ptr new_ids_map(const Ids_map&);
    extern template Ids_map_ptr new_ids_map(Ids_map&&);

    template <typename Arg> Values<Arg> cons_values(const Keys&);
    Ids_map cons_ids_map(const Keys&);
    template <typename Arg> Values_ptr<Arg> new_values(const Keys&);
    Ids_map_ptr new_ids_map(const Keys&);

    bool is_seq(const Ptr&) noexcept;
    void check_is_seq(const Ptr&);

    constexpr bool valid_key(const Key&) noexcept;
    constexpr void check_key(const Key&);

    bool valid_keys(const Keys&) noexcept;
    void check_keys(const Keys&);
    template <typename Arg> bool valid_values(const Values<Arg>&, const Keys&) noexcept;
    template <typename Arg> void check_values(const Values<Arg>&, const Keys&);
    bool valid_ids_map(const Ids_map&, const Keys&) noexcept;
    void check_ids_map(const Ids_map&, const Keys&);

    template <typename K, Req<is_keys_v<K>> = 0>
        Forward_as<Key, K> key(K&&, const Key_id&) noexcept;
    template <typename Arg, typename V, Req<is_values_v<V>> = 0>
        Forward_as<Arg, V> value(V&&, const Key_id&) noexcept;
    template <typename K, Req<is_ids_map_v<K>> = 0>
        Forward_as<Key_id, K> key_id(K&&, const Key&);

    Key_id add_key(Keys&, Ids_map&, Key);
    template <typename Arg, typename V = Dummy>
        Key_id add_key_value(Keys&, Values<Arg>&, Ids_map&, Key, [[maybe_unused]] V = {});
}

namespace unsot::expr {
    class Place : public Dynamic<Place, Ptr> {
    //+ class Place : public Hash_object<Place> {
    public:
        virtual ~Place()                                            = default;

        template <typename T> static Ptr make_ptr(T&&);

        virtual bool is_elem() const noexcept                             = 0;
        virtual bool is_list() const noexcept                             = 0;
        static void check_is_elem(const Ptr&);
        static void check_is_list(const Ptr&);

        String to_string() const& override                                = 0;
        virtual String to_string()&&                   { return to_string(); }

        //+ virtual size_t hash() const                                   = 0;
    };
}

#include "expr/elem.hpp"
#include "expr/list.hpp"

#include "expr.inl"
