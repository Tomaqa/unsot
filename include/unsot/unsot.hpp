#pragma once

#include <string>
#include <utility>
#include <tuple>
#include <type_traits>

#define EMPTY do {} while(0)
#define UNUSED(x) do {(void)(x);} while(0)

#ifdef NDEBUG
#undef DEBUG
#endif

#ifdef DEBUG
#include <cxxabi.h>

constexpr bool _debug_ = true;

inline bool _prefer_assert_over_throw_ = true;
#else
#define NDEBUG

constexpr bool _debug_ = false;
#endif

#include <cassert>

#define FORWARD(...) forward<decltype(__VA_ARGS__)>(__VA_ARGS__)
#define MAKE_TUPLE(...) make_tuple(FORWARD(__VA_ARGS__)...)
#define FORWARD_AS_TUPLE(...) forward_as_tuple(FORWARD(__VA_ARGS__)...)

#define TUPLE_WITH_SEQ(tup) FORWARD(tup), std::make_index_sequence<tuple_size_v<DECAY(tup)>>{}
#define FORWARD_AS_TUPLE_WITH_SEQ(args) TUPLE_WITH_SEQ(FORWARD_AS_TUPLE(args))

#define TUPLE_WITH_SEQ_TYPENAMES typename TArgs, size_t... I
#define TUPLE_WITH_SEQ_PARAMS(tup) TArgs&& tup, std::index_sequence<I...>
#define UNPACK_TUPLE(tup) get<I>(FORWARD(tup))...

#define NOEXCEPT(expr) noexcept(noexcept(expr))

#define DECAY(obj) Decay<decltype(obj)>

#ifdef __clang__
#ifndef NO_EXPLICIT_TP_INST
#define NO_EXPLICIT_TP_INST
#endif
#endif

namespace unsot {
    /// Curiously recurring template pattern - for static polymorphism
    template <typename T> class Crtp;

    template <typename T> struct Object;
    /// Object with support of static functions
    template <typename T> struct Static;
    /// Object with support of virtual functions
    template <typename T, typename P = T*> struct Dynamic;
    template <typename B, typename T = void> struct Inherit;

    struct Dummy final {};
    struct Ignore {};
    struct Absorb;

    struct Empty {};

    class [[nodiscard]] Error;

    template <typename T> class Vector;

    template <typename T = void> struct Tag {};

    template <template<typename...> typename X, typename T, typename = void>
        struct Enabled;
    template <template<typename...> typename X, typename T, typename = void>
        struct Enabled_bool;

    template <template<typename...> typename T, template<typename...> typename U,
              typename... Args>
        class Connect;

    using std::size_t;

    using std::nullptr_t;

    using std::move;
    using std::forward;
    using std::swap;
    using std::exchange;

    using std::as_const;

    using String = std::string;

    template <typename T, typename U = T> using Pair = std::pair<T, U>;
    using std::tuple;
    using std::get;
    using std::make_pair;
    using std::make_tuple;
    using std::make_from_tuple;
    using std::forward_as_tuple;
    using std::tie;
    using std::tuple_cat;
    using std::tuple_size_v;

    using std::initializer_list;

    using std::declval;

    constexpr Dummy dummy;
    constexpr Ignore ignore;

    template <typename T> constexpr Tag<T> tag;

    using std::integral_constant;
    using std::bool_constant;
    using std::true_type;
    using std::false_type;
    template <typename> constexpr bool true_tp = true;
    template <typename> constexpr bool false_tp = false;

    constexpr bool do_assert = true;
    constexpr bool no_assert = false;

    namespace aux {
        template <typename T, typename... Args>
        struct Void final {
            using Type = T;
        };
    }

    template <typename... Args> using Void = std::void_t<Args...>;
    template <typename T, typename... Args> using Fwd_t = typename aux::Void<T, Args...>::Type;

    template <bool cond, typename O = void> using If_t = std::enable_if_t<cond, O>;
    template <bool cond, typename T, typename U> using Cond_t = std::conditional_t<cond, T, U>;
    template <bool cond> using If = If_t<cond>;

    template <bool cond> using Req = If_t<cond, int>;
    template <typename T, bool cond> using Ret_if = If_t<cond, T>;

    template <typename T> using Decay = std::decay_t<T>;
    template <typename T> using Rm_ref = std::remove_reference_t<T>;
    template <typename T> using Rm_ptr = std::remove_pointer_t<T>;
    template <typename T> using Rm_const = std::remove_const_t<T>;
    template <typename T> using Add_lref = std::add_lvalue_reference_t<T>;
    template <typename T> using Add_rref = std::add_rvalue_reference_t<T>;
    template <typename T> using Add_ptr = std::add_pointer_t<T>;
    template <typename T> using Add_const = std::add_const_t<T>;
    template <typename T> using Add_cref = Add_lref<Add_const<T>>;
    template <typename T> using Add_cptr = Add_ptr<Add_const<T>>;

    using std::is_same_v;
    using std::is_convertible_v;
    using std::is_constructible_v;
    using std::is_default_constructible_v;
    using std::is_copy_constructible_v;
    using std::is_move_constructible_v;
    using std::is_base_of_v;
    template <typename T, typename B> constexpr bool is_derived_from_v = is_base_of_v<B, T>;
    template <typename T> constexpr bool is_void_v = is_same_v<T, void>;

    using std::is_const_v;
    template <typename T> constexpr bool is_ptr_v = std::is_pointer_v<T>;
    template <typename T> constexpr bool is_ref_v = std::is_reference_v<T>;
    template <typename T> constexpr bool is_lref_v = std::is_lvalue_reference_v<T>;
    template <typename T> constexpr bool is_rref_v = std::is_rvalue_reference_v<T>;
    template <typename T> constexpr bool is_cref_v = is_lref_v<T> && is_const_v<Rm_ref<T>>;
    template <typename T> constexpr bool is_cptr_v = is_ptr_v<T> && is_const_v<Rm_ptr<T>>;

    template <typename T, typename U>
        using Const_as = Cond_t<is_const_v<U>, Add_const<T>,
                         Cond_t<is_cref_v<U> && is_ref_v<T>,
                                Add_cref<Rm_ref<T>>,
                         Cond_t<is_cref_v<U> && is_ptr_v<T>,
                                Add_cptr<Rm_ptr<T>>,
                         Cond_t<is_cptr_v<U> && is_ref_v<T>,
                                Add_cref<Rm_ref<T>>,
                         Cond_t<is_cptr_v<U> && is_ptr_v<T>,
                                Add_cptr<Rm_ptr<T>>,
        T>>>>>;
    /// `Forward_as<T, T>` should be equivalent with `T`
    template <typename T, typename U>
        using Forward_as = Cond_t<is_cref_v<U>, Add_cref<T>,
                           Cond_t<is_cptr_v<U>, Add_cptr<T>,
                           Cond_t<is_lref_v<U>, Add_lref<T>,
                           Cond_t<is_rref_v<U>, Add_rref<T>,
                           Cond_t<is_ptr_v<U>, Add_ptr<T>,
        T>>>>>;
    template <typename T, typename U = T>
        using Forward_as_ref = Cond_t<is_cref_v<U>, Add_cref<T>,
                               Cond_t<is_lref_v<U>, Add_lref<T>,
        Add_rref<T>>>;
    template <typename T, typename U = T>
        using Forward_as_ptr = Cond_t<is_cptr_v<U>, Add_cptr<T>, Add_ptr<T>>;

    using std::is_invocable_v;
    using std::is_invocable_r_v;

    using std::is_member_pointer_v;
    using std::is_member_object_pointer_v;
    using std::is_member_function_pointer_v;

    template <template<typename...> typename X, typename T>
        constexpr bool enabled_v = Enabled<X, T>::value;
    template <template<typename...> typename X, typename T>
        constexpr bool enabled_bool_v = Enabled_bool<X, T>::value;

    using std::negation;
    using std::conjunction;
    using std::disjunction;
    using std::negation_v;
    using std::conjunction_v;
    using std::disjunction_v;

    //+ constexpr int float_precision = 6;
    //+ constexpr int double_precision = 10;

    namespace aux {
        template <typename T, typename Arg>
            using Enabled_cast = decltype(static_cast<T>(declval<Arg>()));
        template <typename Arg> using Enabled_bool_cast = Enabled_cast<bool, Arg>;

        template <typename T>
            using Is_cont = Void<typename Decay<T>::iterator>;
        template <typename T>
            using Is_it = Fwd_t<typename std::iterator_traits<T>::iterator_category>;

        template <typename T, typename U = T>
            using Equals = decltype(declval<T>() == declval<U>());
        template <typename T, typename U = T>
            using Member_equals = decltype(declval<T>().equals(declval<U>()));
        template <typename T, typename U = T>
            using Less = decltype(declval<T>() < declval<U>());
        template <typename T, typename U = T>
            using Member_less = decltype(declval<T>().less(declval<U>()));

        template <typename T> using To_string = decltype(to_string(declval<T>()));
        template <typename T> using Member_to_string = decltype(declval<T>().to_string());

        template <typename T, typename U = T>
            using Swap = decltype(swap(declval<T>(), declval<U>()));
        template <typename T, typename U = T>
            using Member_swap = decltype(declval<T>().swap(declval<U>()));

        template <typename T> using Deep_copy = decltype(deep_copy(declval<T>()));
        template <typename T> using Member_shallow_copy = decltype(declval<T>().shallow_copy());
        template <typename T> using Member_deep_copy = decltype(declval<T>().deep_copy());
    }

    using std::is_class_v;
    using std::is_polymorphic_v;
    using std::is_abstract_v;

    using std::is_integral_v;
    using std::is_floating_point_v;
    using std::is_arithmetic_v;

    template <typename T> constexpr bool enabled_bool_cast_v = enabled_v<aux::Enabled_bool_cast, T>;

    template <typename T> constexpr bool is_container_v = enabled_v<aux::Is_cont, T>;
    template <typename T> constexpr bool is_iterator_v = enabled_v<aux::Is_it, T>;
    template <typename T>
        constexpr bool is_random_access_iterator_v =
            is_iterator_v<T> && is_same_v<aux::Is_it<T>, std::random_access_iterator_tag>;

    template <typename T>
        constexpr bool is_string_v = is_same_v<Decay<T>, char*>
                                  || is_same_v<Decay<T>, const char*>
                                  || is_same_v<Decay<T>, String>;

    //+ missing second arg. type
    template <typename T> constexpr bool enabled_equals_v = enabled_v<aux::Equals, T>;
    template <typename T> constexpr bool enabled_member_equals_v = enabled_v<aux::Member_equals, T>;
    template <typename T> constexpr bool enabled_less_v = enabled_v<aux::Less, T>;
    template <typename T> constexpr bool enabled_member_less_v = enabled_v<aux::Member_less, T>;

    template <typename T>
        constexpr bool enabled_to_string_v = is_string_v<T> || enabled_v<aux::To_string, T>;
    template <typename T>
        constexpr bool enabled_member_to_string_v = enabled_v<aux::Member_to_string, T>;

    template <typename T> constexpr bool enabled_swap_v = enabled_v<aux::Swap, T>;
    template <typename T> constexpr bool enabled_member_swap_v = enabled_v<aux::Member_swap, T>;

    template <typename T> constexpr bool enabled_deep_copy_v = enabled_v<aux::Deep_copy, T>;
    template <typename T>
        constexpr bool enabled_member_shallow_copy_v = enabled_v<aux::Member_shallow_copy, T>;
    template <typename T>
        constexpr bool enabled_member_deep_copy_v = enabled_v<aux::Member_deep_copy, T>;

    struct Copy_tag : Tag<> {};
    struct Shallow_copy_tag : Copy_tag {};
    struct Deep_copy_tag : Copy_tag {};
    constexpr Copy_tag copy_tag;
    constexpr Shallow_copy_tag shallow_copy_tag;
    constexpr Deep_copy_tag deep_copy_tag;

    template <typename T, typename MemFn, typename... Args>
        decltype(auto) call(T&&, MemFn, Args&&...);

    //+ missing second arg. type in enable
    template <typename T, typename U = T, typename = If<enabled_member_equals_v<T>>>
        bool operator ==(const T& lhs, const U& rhs);
    //+ template <typename T, typename U,
    //+           typename = If<enabled_member_equals<T>>>
    //+     bool operator ==(const U& lhs, const T& rhs);
    //+ missing second arg. type in enable
    template <typename T, typename U = T, typename = If<enabled_member_equals_v<T>>>
        constexpr bool operator !=(const T& lhs, const U& rhs)
                                                     { return !(lhs == rhs); }

    //+ missing second arg. type in enable
    template <typename T, typename U = T, typename = If<enabled_member_less_v<T>>>
        bool operator <(const T& lhs, const U& rhs);

    //+ missing second arg. type in enable
    template <typename T, typename U = T, typename = If<enabled_member_swap_v<T>>>
        void swap(T& lhs, U& rhs);

    /// generic function to get raw pointer
    template <typename T> T* get(T* ptr)                       { return ptr; }
    template <typename P, typename T = decltype(declval<P>().get()), Req<is_ptr_v<T>> = 0>
        auto get(P&& ptr);

    /// Uses `deep_copy` if present and copy constructor otherwise
    template <typename T> T deep_copy(const T&);

    template <typename T> decltype(auto) tuple_car(T&&);
    template <typename T> decltype(auto) tuple_cdr(T&&);
}

namespace unsot {
    struct Absorb {
        template <typename... Args> Absorb(Args&&...)                                            { }
    };

    template <template<typename...> typename X, typename T, typename>
    struct Enabled final : false_type {};

    template <template<typename...> typename X, typename T>
    struct Enabled<X, T, Void<X<T>>> final : true_type {};

    template <template<typename...> typename X, typename T, typename>
    struct Enabled_bool final : false_type {};

    template <template<typename...> typename X, typename T>
    struct Enabled_bool<X, T, Void<X<T>>> final : X<T> {};

    template <template<typename...> typename T, template<typename...> typename U, typename... Args>
    class Connect final {
    private:
        template <typename C, bool first>
        struct Other {
            using Type = Cond_t<first, typename C::T1, typename C::T2>;
        };
    public:
        using T1 = T<Args..., Other<Connect, false>>;
        using T2 = U<Args..., Other<Connect, true>>;
    };
}

#include "unsot/object.hpp"
#include "unsot/string.hpp"
#include "unsot/vector.hpp"
#include "unsot/error.hpp"

#include "unsot.inl"
