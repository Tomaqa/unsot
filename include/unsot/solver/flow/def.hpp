#pragma once

#include "ode/flow/parse.hpp"

namespace unsot::solver::flow {
    class Def : public Object<Def> {
    public:
        using typename Object<Def>::This;

        template <typename S> class Define;

        Def()                                                       = default;
        ~Def()                                                      = default;
        Def(const Def&)                                             = default;
        Def& operator =(const Def&)                                 = default;
        Def(Def&&)                                                  = default;
        Def& operator =(Def&&)                                      = default;
        Def(Key, expr::Keys real_arg_keys_, expr::Keys bool_arg_keys_);

        const auto& ckey() const noexcept                     { return _key; }
        const auto& key() const noexcept                    { return ckey(); }
        const auto& cid() const noexcept                       { return _id; }
        const auto& id() const noexcept                      { return cid(); }

        const auto& cflow_ptr() const noexcept           { return _flow_ptr; }
        const auto& cflow() const noexcept            { return *cflow_ptr(); }
        const auto& flow() const noexcept                  { return cflow(); }

        const auto& cflow_parse() const noexcept       { return _flow_parse; }
        const auto& flow_parse() const noexcept      { return cflow_parse(); }

        const auto& cinit_keys() const noexcept         { return _init_keys; }
        const auto& init_keys() const noexcept        { return cinit_keys(); }
        const auto& creal_arg_keys() const noexcept { return _real_arg_keys; }
        const auto& real_arg_keys() const noexcept{ return creal_arg_keys(); }
        const auto& cbool_arg_keys() const noexcept { return _bool_arg_keys; }
        const auto& bool_arg_keys() const noexcept{ return cbool_arg_keys(); }
        const auto& cfinal_keys() const noexcept       { return _final_keys; }
        const auto& final_keys() const noexcept      { return cfinal_keys(); }

        const auto& carg_keys_mask() const noexcept { return _arg_keys_mask; }

        const auto& cvariant_arg_keys() const noexcept
                                                 { return _variant_arg_keys; }
        const auto& variant_arg_keys() const noexcept
                                               { return cvariant_arg_keys(); }
        const auto& cvariant_depend_keys() const noexcept
                                              { return _variant_depend_keys; }
        const auto& variant_depend_keys() const noexcept
                                            { return cvariant_depend_keys(); }

        const All_variants_map& call_odes_map() const noexcept
                                                     { return _all_odes_map; }
        const All_variants_map& all_odes_map() const noexcept
                                                   { return call_odes_map(); }
        inline const Variants& codes(const Key& ode_key) const;
        const Variants& odes(const Key& ode_key) const
                                                    { return codes(ode_key); }
        inline Variants& odes(const Key& ode_key) noexcept;
        const All_variants_map& call_invariants_map() const noexcept
                                               { return _all_invariants_map; }
        const All_variants_map& all_invariants_map() const noexcept
                                             { return call_invariants_map(); }
        inline const Variants& cinvariants(const Key& ode_key) const;
        const Variants& invariants(const Key& ode_key) const
                                              { return cinvariants(ode_key); }
        inline Variants& invariants(const Key& ode_key) noexcept;
        const Key& ct_ode_key() const noexcept          { return _t_ode_key; }
        const Key& t_ode_key() const noexcept         { return ct_ode_key(); }
        Key& t_ode_key() noexcept                       { return _t_ode_key; }
    protected:
        auto& key() noexcept                                  { return _key; }
        auto& id() noexcept                                    { return _id; }

        auto& flow_ptr() noexcept                        { return _flow_ptr; }
        auto& flow() noexcept                          { return *flow_ptr(); }
        auto& flow_parse() noexcept                    { return _flow_parse; }

        auto& init_keys() noexcept                      { return _init_keys; }
        auto& real_arg_keys() noexcept              { return _real_arg_keys; }
        auto& bool_arg_keys() noexcept              { return _bool_arg_keys; }
        auto& final_keys() noexcept                    { return _final_keys; }

        auto& arg_keys_mask() noexcept              { return _arg_keys_mask; }

        auto& variant_arg_keys() noexcept        { return _variant_arg_keys; }
        auto& variant_depend_keys() noexcept  { return _variant_depend_keys; }

        All_variants_map& all_odes_map() noexcept    { return _all_odes_map; }
        All_variants_map& all_invariants_map() noexcept
                                               { return _all_invariants_map; }
    private:
        Key _key{};
        Id _id{};

        Ptr _flow_ptr{};
        Parse _flow_parse{};

        expr::Keys _init_keys{};
        expr::Keys _real_arg_keys{};
        expr::Keys _bool_arg_keys{};
        expr::Keys _final_keys{};

        Mask _arg_keys_mask{};

        expr::Keys _variant_arg_keys{};
        expr::Keys _variant_depend_keys{};

        All_variants_map _all_odes_map{};
        All_variants_map _all_invariants_map{};
        Key _t_ode_key{};
    };
}

namespace unsot::solver::flow {
    template <typename S>
    class Def::Define : public Object<Define<S>> {
    public:
        using typename Object<Define<S>>::This;

        using Solver = S;

        ~Define()                                                   = default;
        Define(Solver&, Key, Formula, expr::Keys ode_keys_,
               expr::Keys real_arg_keys_, expr::Keys bool_arg_keys_);

        Define& perform();
    protected:
        class Parse_formula;

        using Def_ptr = Unique_ptr<Def>;

        static Def_ptr new_def(Def&&);

        const Solver& csolver() const noexcept             { return _solver; }
        Solver& solver() noexcept                          { return _solver; }

        const auto& cdef() const noexcept                { return *_def_ptr; }
        auto& def() noexcept                             { return *_def_ptr; }

        void set_init_and_final_keys();
        void set_fun();
        void set_flow(Formula);

        void define_variants(Def& solver_def);
    private:
        void set_fun_arg_keys();

        void set_keys();

        void set_arg_keys_mask();

        Solver& _solver;

        Def_ptr _def_ptr{};

        Keys _keys;

        Formula _fun_formula{expr::new_key("and")};
        expr::Keys _fun_arg_keys{};

        List _parse_list{};
        Flow _flow{};
        Parse _flow_parse{};
    };
}

namespace unsot::solver::flow {
    template <typename S>
    class Def::Define<S>::Parse_formula : public Object<Parse_formula> {
    public:
        using typename Object<Parse_formula>::This;

        ~Parse_formula()                                            = default;
        Parse_formula(Define&, Formula);

        Parse_formula& perform();
    protected:
        const auto& cdefine() const noexcept               { return _define; }
        auto& define() noexcept                            { return _define; }

        auto& eptr() noexcept                                { return _eptr; }

        const auto& cflow_parse() const noexcept       { return _flow_parse; }
        auto& flow_parse() noexcept                    { return _flow_parse; }

        bool valid_init_key(const Key&) const noexcept;
        bool valid_final_key(const Key&) const noexcept;

        void preprocess()                                                  { }
        void parse();
        void parse_subformula(expr::Ptr&);

        void add_to_fun_formula();
    private:
        Define& _define;

        expr::Ptr _eptr;

        Parse _flow_parse;
    };
}

#include "solver/flow/def.inl"
