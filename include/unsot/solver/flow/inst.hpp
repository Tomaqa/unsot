#pragma once

namespace unsot::solver::flow {
    template <typename S>
    class Inst : public Object<Inst<S>> {
    public:
        using typename Object<Inst<S>>::This;

        class Instantiate;

        using Solver = S;

        using Variant_var_ids = var::Base::Depend_ids;

        using Def_to_inst_variant_keys_map = Hash<Key, Key>;

        Inst()                                                                            = default;
        virtual ~Inst()                                                                   = default;
        Inst(const Inst&)                                                                 = default;
        Inst& operator =(const Inst&)                                                     = default;
        Inst(Inst&&)                                                                      = default;
        Inst& operator =(Inst&&)                                                          = default;
        Inst(Solver*, flow::Id, Inst_id, const expr::Keys& init_vals,
             const expr::Keys& real_arg_vals, const expr::Keys& bool_arg_vals);

        const auto& csolver_l() const noexcept                                 { return _solver_l; }
        const auto& csolver() const noexcept                                { return *csolver_l(); }
        const auto& solver() const noexcept                                    { return csolver(); }
        auto& solver() noexcept                                              { return *solver_l(); }

        const auto& cflow_id() const noexcept                                   { return _flow_id; }
        const auto& flow_id() const noexcept                                  { return cflow_id(); }
        const auto& cdef() const noexcept;
        const auto& def() const noexcept                                          { return cdef(); }
        const auto& cflow() const noexcept;
        const auto& flow() const noexcept                                        { return cflow(); }

        const auto& cid() const noexcept                                             { return _id; }
        const auto& id() const noexcept                                            { return cid(); }

        const auto& cinit_values() const noexcept                           { return _init_values; }
        [[nodiscard]] const auto& init_values() const noexcept            { return cinit_values(); }
        const auto& creal_arg_values() const noexcept                   { return _real_arg_values; }
        const auto& real_arg_values() const noexcept                  { return creal_arg_values(); }
        const auto& cbool_arg_values() const noexcept                   { return _bool_arg_values; }
        const auto& bool_arg_values() const noexcept                  { return cbool_arg_values(); }
        const auto& cfinal_keys() const noexcept                             { return _final_keys; }
        const auto& final_keys() const noexcept                            { return cfinal_keys(); }

        /// Reduced args that only participate inside ODEs and invariants
        const auto& cflow_arg_values() const noexcept                   { return _flow_arg_values; }
        const auto& flow_arg_values() const noexcept                  { return cflow_arg_values(); }

        /// Reduced args of predicate (expr) variables
        /// (Init. values are never reduced)
        const auto& cvariant_arg_values() const noexcept             { return _variant_arg_values; }
        const auto& variant_arg_values() const noexcept            { return cvariant_arg_values(); }
        const auto& cvariant_depend_values() const noexcept       { return _variant_depend_values; }
        const auto& variant_depend_values() const noexcept      { return cvariant_depend_values(); }

        const Variant_var_ids& cvariant_arg_ids() const noexcept        { return _variant_arg_ids; }
        const Variant_var_ids& variant_arg_ids() const noexcept       { return cvariant_arg_ids(); }
        const Variant_var_ids& cvariant_depend_ids() const noexcept  { return _variant_depend_ids; }
        const Variant_var_ids& variant_depend_ids() const noexcept { return cvariant_depend_ids(); }
        const Variant_var_ids& cfinal_var_ids() const noexcept            { return _final_var_ids; }
        const Variant_var_ids& final_var_ids() const noexcept           { return cfinal_var_ids(); }

        const auto& cdef_to_inst_variant_keys_map() const noexcept
                                                           { return _def_to_inst_variant_keys_map; }
        const auto& def_to_inst_variant_keys_map() const noexcept
                                                         { return cdef_to_inst_variant_keys_map(); }
        const auto& cinst_variant_key_of(const Key& def_vrt_key) const;
        const auto& inst_variant_key_of(const Key& def_vrt_key) const
                                                       { return cinst_variant_key_of(def_vrt_key); }

        const Variant_var_ids& code_var_ids() const noexcept                { return _ode_var_ids; }
        const Variant_var_ids& ode_var_ids() const noexcept               { return code_var_ids(); }
        const Variant_var_ids& cinvariant_var_ids() const noexcept    { return _invariant_var_ids; }
        const Variant_var_ids& invariant_var_ids() const noexcept   { return cinvariant_var_ids(); }
        const var::Id& ct_ode_id() const noexcept                              { return _t_ode_id; }
        const auto& ct_ode_ptr() const;
        auto& t_ode_ptr();
        const auto& ct_ode() const;
        auto& t_ode();

        size_t cn_variants() const noexcept;
        size_t max_set_variants() const noexcept;
        int cn_set_variants() const noexcept                             { return _n_set_variants; }

        bool cvalue() const noexcept                                              { return _value; }
        bool value() const noexcept                                             { return cvalue(); }
        bool ccomputed() const noexcept                                        { return _computed; }
        bool& computed() noexcept                                              { return _computed; }

        const auto& ctrigger_ode_id() const noexcept                     { return _trigger_ode_id; }
        const auto& ctrigger_ode_ptr() const;
        auto& trigger_ode_ptr();
        const auto& ctrigger_ode() const;
        auto& trigger_ode();

        const reals::Preds_view& ccurrent_odes_view() const noexcept  { return _current_odes_view; }
        reals::Preds_view& current_odes_view() noexcept               { return _current_odes_view; }
        const reals::Preds_view& ccurrent_invariants_view() const noexcept
                                                                { return _current_invariants_view; }
        reals::Preds_view& current_invariants_view() noexcept   { return _current_invariants_view; }

        virtual void notice_set_variant(const var::Id&) noexcept;
        virtual void notice_unset_variant(const var::Id&) noexcept;

        virtual bool all_variants_set() const noexcept;

        virtual Flag surely_evaluable() const noexcept;

        bool eval(const var::Id& trigger_ode_id_);

        virtual void reset();
    protected:
        auto& solver_l() noexcept                                              { return _solver_l; }

        [[nodiscard]] auto& init_values() noexcept                          { return _init_values; }
        auto& real_arg_values() noexcept                                { return _real_arg_values; }
        auto& bool_arg_values() noexcept                                { return _bool_arg_values; }
        auto& final_keys() noexcept                                          { return _final_keys; }

        auto& flow_arg_values() noexcept                                { return _flow_arg_values; }

        auto& variant_arg_values() noexcept                          { return _variant_arg_values; }
        auto& variant_depend_values() noexcept                    { return _variant_depend_values; }

        Variant_var_ids& variant_arg_ids() noexcept                     { return _variant_arg_ids; }
        Variant_var_ids& variant_depend_ids() noexcept               { return _variant_depend_ids; }
        Variant_var_ids& final_var_ids() noexcept                         { return _final_var_ids; }

        auto& def_to_inst_variant_keys_map() noexcept      { return _def_to_inst_variant_keys_map; }
        auto& inst_variant_key_of(const Key& def_vrt_key) noexcept;

        Variant_var_ids& ode_var_ids() noexcept                             { return _ode_var_ids; }
        Variant_var_ids& invariant_var_ids() noexcept                 { return _invariant_var_ids; }
        var::Id& t_ode_id() noexcept                                           { return _t_ode_id; }

        int& n_set_variants() noexcept                                   { return _n_set_variants; }

        auto& value() noexcept                                                    { return _value; }

        auto& trigger_ode_id() noexcept                                  { return _trigger_ode_id; }

        virtual bool compute();

        virtual State make_init_state();
        virtual Config make_config();
    private:
        Solver* _solver_l{};
        flow::Id _flow_id{};

        Inst_id _id{};

        //+ Elems
        expr::Keys _init_values{};
        expr::Keys _real_arg_values{};
        expr::Keys _bool_arg_values{};
        expr::Keys _final_keys{};

        expr::Keys _flow_arg_values{};

        //+ Elems
        expr::Keys _variant_arg_values{};
        expr::Keys _variant_depend_values{};
        Variant_var_ids _variant_arg_ids{};
        Variant_var_ids _variant_depend_ids{};
        Variant_var_ids _final_var_ids{};

        Def_to_inst_variant_keys_map _def_to_inst_variant_keys_map{};

        Variant_var_ids _ode_var_ids{};
        Variant_var_ids _invariant_var_ids{};
        var::Id _t_ode_id{};

        int _n_set_variants{};

        bool _value{};
        bool _computed{};

        var::Id _trigger_ode_id{var::invalid_id};

        reals::Preds_view _current_odes_view{};
        reals::Preds_view _current_invariants_view{};
    };
}

namespace unsot::solver::flow {
    template <typename S>
    class Inst<S>::Instantiate : public Object<Instantiate> {
    public:
        using typename Object<Instantiate>::This;

        ~Instantiate()                                                                    = default;
        Instantiate(Solver&, const flow::Id&, const expr::Keys& init_vals,
                    const expr::Keys& real_arg_vals, const expr::Keys& bool_arg_vals);

        Instantiate& perform();
        Formula expand();

        Key to_inst_key(Key) noexcept;
    protected:
        const auto& csolver() const noexcept                                     { return _solver; }
        auto& solver() noexcept                                                  { return _solver; }

        const auto& cdef() const noexcept                                           { return _def; }

        const auto& cid() const noexcept                                             { return _id; }
        auto& id() noexcept                                                          { return _id; }
        const auto& cinst() const noexcept;
        auto& inst() noexcept;

        Inst make_inst(const expr::Keys& init_vals,
                       const expr::Keys& real_arg_vals, const expr::Keys& bool_arg_vals);

        void set_inst(const expr::Keys& init_vals,
                      const expr::Keys& real_arg_vals, const expr::Keys& bool_arg_vals);
        void set_final_keys();
        void instantiate_variants();
        void set_inst_arg_values();
    private:
        Solver& _solver;

        const Def& _def;

        expr::Keys _defun_arg_values{};

        Inst_id _id{};
    };
}

#include "solver/flow/inst.inl"
