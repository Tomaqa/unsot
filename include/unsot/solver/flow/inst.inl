#pragma once

namespace unsot::solver::flow {
    template <typename S>
    const auto& Inst<S>::cdef() const noexcept
    {
        return csolver().cflow_def(cflow_id());
    }

    template <typename S>
    const auto& Inst<S>::cflow() const noexcept
    {
        return csolver().code_solver().cflow(cflow_id());
    }

    template <typename S>
    const auto& Inst<S>::cinst_variant_key_of(const Key& def_vrt_key) const
    {
        return cdef_to_inst_variant_keys_map()[def_vrt_key];
    }

    template <typename S>
    auto& Inst<S>::inst_variant_key_of(const Key& def_vrt_key) noexcept
    {
        return def_to_inst_variant_keys_map()[def_vrt_key];
    }

    template <typename S>
    size_t Inst<S>::cn_variants() const noexcept
    {
        return size(code_var_ids()) + size(cinvariant_var_ids());
    }

    template <typename S>
    size_t Inst<S>::max_set_variants() const noexcept
    {
        return size(cfinal_keys()) + size(cinvariant_var_ids());
    }

    template <typename S>
    const auto& Inst<S>::ct_ode_ptr() const
    {
        auto& tid = ct_ode_id();
        return csolver().creal_preds().cptr(tid);
    }

    template <typename S>
    auto& Inst<S>::t_ode_ptr()
    {
        auto& tid = ct_ode_id();
        return solver().real_preds().ptr(tid);
    }

    template <typename S>
    const auto& Inst<S>::ct_ode() const
    {
        return Solver::Ode::cast(ct_ode_ptr());
    }

    template <typename S>
    auto& Inst<S>::t_ode()
    {
        return Solver::Ode::cast(t_ode_ptr());
    }

    template <typename S>
    const auto& Inst<S>::ctrigger_ode_ptr() const
    {
        auto& trid = ctrigger_ode_id();
        assert(trid != var::invalid_id);
        return csolver().creal_preds().cptr(trid);
    }

    template <typename S>
    auto& Inst<S>::trigger_ode_ptr()
    {
        auto& trid = ctrigger_ode_id();
        assert(trid != var::invalid_id);
        return solver().real_preds().ptr(trid);
    }

    template <typename S>
    const auto& Inst<S>::ctrigger_ode() const
    {
        return Solver::Ode::cast(ctrigger_ode_ptr());
    }

    template <typename S>
    auto& Inst<S>::trigger_ode()
    {
        return Solver::Ode::cast(trigger_ode_ptr());
    }
}

////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////

namespace unsot::solver::flow {
    template <typename S>
    const auto& Inst<S>::Instantiate::cinst() const noexcept
    {
        return csolver().cflow_inst(cid());
    }

    template <typename S>
    auto& Inst<S>::Instantiate::inst() noexcept
    {
        return solver().flow_inst(cid());
    }
}
