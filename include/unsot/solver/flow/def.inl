#pragma once

namespace unsot::solver::flow {
    const Variants& Def::codes(const Key& ode_key) const
    {
        return variants(call_odes_map(), ode_key);
    }

    Variants& Def::odes(const Key& ode_key) noexcept
    {
        return variants(all_odes_map(), ode_key);
    }

    const Variants& Def::cinvariants(const Key& ode_key) const
    {
        return variants(call_invariants_map(), ode_key);
    }

    Variants& Def::invariants(const Key& ode_key) noexcept
    {
        return variants(all_invariants_map(), ode_key);
    }
}

////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////

namespace unsot::solver::flow {
    template <typename S>
    typename Def::Define<S>::Def_ptr Def::Define<S>::new_def(Def&& fdef)
    {
        return MAKE_UNIQUE(move(fdef));
    }
}
