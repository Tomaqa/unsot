#pragma once

#include "util/alg.hpp"

namespace unsot::solver::flow {
    expr::Keys make_arg_keys(const Def& fdef)
    {
        return make_arg_keys(fdef.cinit_keys(), fdef.creal_arg_keys());
    }

    expr::Keys make_masked_arg_keys(const Def& fdef)
    {
        return make_masked_arg_keys(fdef.cinit_keys(), fdef.creal_arg_keys(),
                                    fdef.carg_keys_mask());
    }

    expr::Keys make_masked_arg_keys_keep_init(const Def& fdef)
    {
        return make_masked_arg_keys_keep_init(fdef.cinit_keys(),
                                              fdef.creal_arg_keys(),
                                              fdef.carg_keys_mask());
    }

    namespace aux {
        template <bool odesV, typename DefT, typename BinF, typename DefUnF>
        void for_each_variant(DefT&& fdef, const Keys& keys, BinF f,
                              DefUnF def_f)
        {
            auto body_f = [&fdef, &f, &def_f](const auto& okey){
                auto& variants = invoke([&]() -> auto& {
                    if constexpr (odesV) return fdef.odes(okey);
                    else return fdef.invariants(okey);
                });
                if (empty(variants)) def_f(okey);
                else try { f(variants, okey); }
                catch (Ignore) { }
                catch (Dummy) { def_f(okey); }
            };

            if constexpr (odesV) for_each_ode_key(keys, move(body_f));
            else for_each_inv_key(keys, move(body_f));
        }
    }

    template <typename DefT, typename BinF, typename DefUnF>
    void for_each_ode(DefT&& fdef, const Keys& keys, BinF f, DefUnF def_f)
    {
        aux::for_each_variant<true>(fdef, keys, move(f), move(def_f));
    }

    template <typename DefT, typename BinF, typename DefUnF>
    void for_each_invariant(DefT&& fdef, const Keys& keys, BinF f,
                            DefUnF def_f)
    {
        aux::for_each_variant<false>(fdef, keys, move(f), move(def_f));
    }

    template <typename DefT, typename BinF, typename DefUnF>
    void for_each_variant(DefT&& fdef, const Keys& keys, BinF f, DefUnF def_f)
    {
        for_each_ode(fdef, keys, f, def_f);
        for_each_invariant(fdef, keys, move(f), move(def_f));
    }
}
