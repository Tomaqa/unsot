#pragma once

#include "solver/flow.hpp"

namespace unsot::solver::var {
    using namespace expr;
    using namespace expr::var;
    using expr::var::Ptr;

    template <typename B, typename InstT> class Variant_mixin;
    template <typename B> class Ode_mixin;

    template <typename B> class Final_mixin;

    template <typename InstT> using Invariant = Variant_mixin<bools::Var, InstT>;
    template <typename InstT>
        using Ode = Ode_mixin<Variant_mixin<reals::Preds::Types::Assigner, InstT>>;

    template <var::Type typeV = var::Type::assignee>
        using Final = Final_mixin<reals::Var<typeV>>;
}

namespace unsot::solver::var {
    template <typename B, typename InstT>
    class Variant_mixin : public Inherit<B, Variant_mixin<B, InstT>> {
    public:
        using Inherit = unsot::Inherit<B, Variant_mixin<B, InstT>>;
        using typename Inherit::This;
        using typename Inherit::Base;
        using Variant_t = This;

        using typename Inherit::Value;
        using typename Inherit::Values_ptr;

        using Flow_inst = InstT;
        using Solver = typename Flow_inst::Solver;
        using Flow_insts = flow::Insts<Solver>;
        using Flow_insts_ptr = flow::Insts_ptr<Solver>;

        using Inherit::Parent::operator =;
        Variant_mixin()                                             = default;
        virtual ~Variant_mixin()                                    = default;
        Variant_mixin(const Variant_mixin&)                          = delete;
        Variant_mixin& operator =(const Variant_mixin&)              = delete;
        Variant_mixin(Variant_mixin&&)                              = default;
        Variant_mixin& operator =(Variant_mixin&&)                  = default;

        using Inherit::cons_tp;
        template <typename T>
            static T cons_tp(Keys_ptr&, Values_ptr&, Ids_map_ptr&, Key ode_key_, Id final_vid,
                             Flow_insts_ptr, flow::Inst_id, Key);

        const auto& code_key() const noexcept             { return _ode_key; }
        const auto& ode_key() const& noexcept           { return code_key(); }
        auto&& ode_key()&& noexcept                { return move(ode_key()); }

        const auto& cfinal_var_id() const noexcept   { return _final_var_id; }

        const auto& cflow_insts_ptr() const noexcept
                                                   { return _flow_insts_ptr; }
        const auto& cflow_insts() const noexcept{ return *cflow_insts_ptr(); }
        const auto& flow_insts() const noexcept      { return cflow_insts(); }
        const auto& cflow_inst_id() const noexcept   { return _flow_inst_id; }
        const auto& flow_inst_id() const noexcept  { return cflow_inst_id(); }
        const auto& cflow_inst() const noexcept;
        const auto& flow_inst() const noexcept        { return cflow_inst(); }
        auto& flow_inst() noexcept;

        const auto& cfinal_var() const;
        const auto& final_var() const                 { return cfinal_var(); }
        auto& final_var();
    protected:
        explicit Variant_mixin(Keys_ptr, Values_ptr, Ids_map_ptr, Id, Key ode_key_, Id final_vid,
                               Flow_insts_ptr, flow::Inst_id);
        template <TUPLE_WITH_SEQ_TYPENAMES>
            explicit Variant_mixin(Keys_ptr, Values_ptr, Ids_map_ptr, Id,
                                   TUPLE_WITH_SEQ_PARAMS(targs),
                                   Key ode_key_, Id final_vid,
                                   Flow_insts_ptr, flow::Inst_id);

        auto& ode_key()& noexcept                         { return _ode_key; }

        auto& flow_insts_ptr() noexcept            { return _flow_insts_ptr; }
        auto& flow_insts() noexcept              { return *flow_insts_ptr(); }
        auto& flow_inst_id() noexcept                { return _flow_inst_id; }

        virtual void notify_set_variant() noexcept;
        virtual void notify_unset_variant() noexcept;

        void set_only_impl() noexcept override;
        void unset_only_impl() noexcept override;
        void reset_impl() noexcept override;
    private:
        Key _ode_key{};

        Id _final_var_id{};

        Flow_insts_ptr _flow_insts_ptr{};
        flow::Inst_id _flow_inst_id{};
    };

    template <typename B>
    class Ode_mixin : public Inherit<B, Ode_mixin<B>> {
    public:
        using Inherit = unsot::Inherit<B, Ode_mixin<B>>;
        using typename Inherit::This;
        using typename Inherit::Base;
        using Ode_t = This;

        using typename Inherit::Value;
        using typename Inherit::Values_ptr;

        using typename Inherit::Args_base_link_ptr;

        using typename Inherit::Fun;

        using typename Inherit::Arg_ids;
        using typename Inherit::Depend_ids;

        using typename Inherit::Flow_inst;
        using typename Inherit::Flow_insts_ptr;

        static_assert(is_same_v<Arg_ids, typename Flow_inst::Variant_var_ids>);
        static_assert(is_same_v<Depend_ids, typename Flow_inst::Variant_var_ids>);

        using Inherit::Parent::operator =;
        Ode_mixin()                                                 = default;
        virtual ~Ode_mixin()                                        = default;
        Ode_mixin(const Ode_mixin&)                                  = delete;
        Ode_mixin& operator =(const Ode_mixin&)                      = delete;
        Ode_mixin(Ode_mixin&&)                                      = default;
        Ode_mixin& operator =(Ode_mixin&&)                          = default;

        using Inherit::cons_tp;
        template <typename T>
            static T cons_tp(Keys_ptr&, Values_ptr&, Ids_map_ptr&, Fun, Args_base_link_ptr,
                             Key ode_key_, Id final_vid, Flow_insts_ptr, flow::Inst_id, Key);

        const Arg_ids& carg_ids() const override;
        const Depend_ids& cdepend_ids() const override;

        Flag surely_computable() const noexcept override;

        Flag surely_any_assignable() const noexcept override;
    protected:
        using Inherit::Inherit;

        void add_assignees() override;

        void notify_set_variant() noexcept override;
        void notify_unset_variant() noexcept override;

        Flag computable_body() const noexcept override;

        Flag any_assignable_body() const noexcept override;

        bool compute_body_prepare_not_assign() override;
        bool compute_body_prepare_assign() override;
        Value compute_body_value_not_assign(bool success) override;
    };

    template <typename B>
    class Final_mixin : public Inherit<B, Final_mixin<B>> {
    public:
        using Inherit = unsot::Inherit<B, Final_mixin<B>>;
        using typename Inherit::This;
        using typename Inherit::Base;
        using Final_t = This;

        using Inherit::Parent::operator =;
        Final_mixin()                                               = default;
        virtual ~Final_mixin()                                      = default;
        Final_mixin(const Final_mixin&)                              = delete;
        Final_mixin& operator =(const Final_mixin&)                  = delete;
        Final_mixin(Final_mixin&&)                                  = default;
        Final_mixin& operator =(Final_mixin&&)                      = default;

        bool allowed_assigner(const Id&) const noexcept override
                                                             { return false; }
    protected:
        using Inherit::Inherit;
    };
}

#include "solver/var.inl"
