#pragma once

namespace unsot::solver::flow {
    template <typename Is, typename I, typename PtrT>
    PtrT new_insts(Is&& insts)
    {
        return MAKE_SHARED(move(insts));
    }

    template <typename PtrT>
    PtrT new_insts()
    {
        return new_insts(typename PtrT::element_type{});
    }

    template <typename Ds>
    Forward_as<Def, Ds> def(Ds&& defs, const Id& fid) noexcept
    {
        return FORWARD(FORWARD(defs)[to_idx(fid)]);
    }

    template <typename Ds>
    Forward_as<Def, Ds> def(Ds&& defs, const Ids_map& dmap, const Key& dkey)
    {
        return FORWARD(def(FORWARD(defs), id(dmap, dkey)));
    }

    template <typename M>
    Forward_as<Id, M> id(M&& dmap, const Key& dkey)
    {
        return FORWARD(FORWARD(dmap)[dkey]);
    }

    template <typename Is, typename I>
    Forward_as<I, Is> inst(Is&& insts, const Inst_id& iid) noexcept
    {
        return FORWARD(FORWARD(insts)[iid]);
    }

    template <typename M>
    Forward_as<Variants, M> variants(M&& vmap, const Key& ode_key)
    {
        return FORWARD(FORWARD(vmap)[ode_key]);
    }
}
