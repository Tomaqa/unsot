#pragma once

namespace unsot::solver {
    template <typename B, typename OdeSolver>
    class Mixin<B, OdeSolver>::Check_sat : public unsot::Inherit<typename B::Check_sat, Check_sat> {
    public:
        using Inherit = unsot::Inherit<typename B::Check_sat, Check_sat>;
        using typename Inherit::This;
        using typename Inherit::Solver;

        using Flow_inst = typename Solver::Flow_inst;

        using Invariant = typename Solver::Invariant;
        using Ode = typename Solver::Ode;
        using Final_var = typename Solver::Final_var;

        using Inherit::Inherit;

        const auto& ccompute_flows_duration() const noexcept     { return _compute_flows_duration; }
        const auto& ccompute_flows_count() const noexcept           { return _compute_flows_count; }
    protected:
        auto& compute_flows_duration() noexcept                  { return _compute_flows_duration; }
        auto& compute_flows_count() noexcept                        { return _compute_flows_count; }

        virtual void pre_compute_of(Flow_inst&);
        virtual void post_compute_of(Flow_inst&);

        virtual void reset_of(Flow_inst&)                                                        { }
    private:
        Duration _compute_flows_duration{};
        long _compute_flows_count{};
    };
}
