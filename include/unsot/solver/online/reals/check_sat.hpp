#pragma once

namespace unsot::solver::online::reals {
    template <typename B>
    class Mixin<B>::Check_sat : public unsot::Inherit<typename B::Check_sat, Check_sat> {
    public:
        using Inherit = unsot::Inherit<typename B::Check_sat, Check_sat>;
        using typename Inherit::This;
        using typename Inherit::Solver;

        class T_propagate;

        using typename Inherit::Flow_inst;

        using typename Inherit::Invariant;
        using typename Inherit::Ode;
        using typename Inherit::Final_var;

        using Inherit::Inherit;
        virtual ~Check_sat()                                                              = default;
        Check_sat(Check_sat&&)                                                            = default;

        String profiling_to_string(Duration total) const override;
    protected:
        friend Flow_inst;

        void reset_of(Flow_inst&) override;

        virtual void reset_flow_variant(var::Ptr&);
        virtual void reset_invariant(var::Ptr&);
        virtual void reset_ode(var::Ptr&);
    };
}

#include "solver/online/reals/check_sat/t_prop.hpp"
