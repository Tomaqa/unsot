#pragma once

#include "solver/online/reals/rail.hpp"
#include "smt/sat/solver/online/expr/minisat.hpp"

#define TP_INST_SOLVER_NAMESPACE unsot

namespace unsot::solver::online::reals::rail::minisat {
    template <typename S, typename OdeSolver>
        using Crtp = rail::Crtp<S, smt::sat::solver::online::expr::Minisat<>, OdeSolver>;

    template <typename OdeSolver>
    struct Solver final : Inherit<Crtp<Solver<OdeSolver>, OdeSolver>> {
        using Inherit = unsot::Inherit<Crtp<Solver<OdeSolver>, OdeSolver>>;

        using Inherit::Inherit;
    };
}

namespace unsot::smt::sat::solver {
    namespace aux {
        using _Minisat = online::expr::Minisat<>;
    }
}
