#ifdef NO_EXPLICIT_TP_INST

#include "solver/online/reals.tpp"

#else

namespace TP_INST_SOLVER_NAMESPACE {
    extern template class aux::_S::Unsot_online_t::Mixin;
}

#endif  /// NO_EXPLICIT_TP_INST
