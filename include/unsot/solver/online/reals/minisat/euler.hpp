#pragma once

#include "solver/online/reals/minisat.hpp"
#include "ode/solver/euler.hpp"

namespace unsot {
    namespace aux {
        using _S = solver::online::reals::minisat::Solver<ode::solver::Euler>;
    }
}
#include "smt/solver/tp.hpp"
#include "smt/solver/bools/tp.hpp"
#include "smt/solver/reals/tp.hpp"
#include "smt/solver/online/tp.hpp"
#include "smt/solver/online/bools/tp.hpp"
#include "smt/solver/online/reals/tp.hpp"
#include "solver/tp.hpp"
#include "solver/online/reals/tp.hpp"

#include "smt/sat/solver/online/minisat/tp.hpp"
