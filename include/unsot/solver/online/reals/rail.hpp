#pragma once

#include "solver/online/reals.hpp"
#include "smt/solver/online/reals/rail.hpp"

namespace unsot::solver::online::reals::rail {
    template <typename S, typename SatSolver, typename OdeSolver,
              var::Type typeV = var::Type::assignee>
        using Crtp = reals::Mixin<solver::Mixin<smt::solver::online::reals::rail::Crtp<S, SatSolver, typeV>,
                                                OdeSolver>>;
}
