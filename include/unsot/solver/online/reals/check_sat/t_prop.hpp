#pragma once

namespace unsot::solver::online::reals {
    template <typename B>
    class Mixin<B>::Check_sat::T_propagate
        : public unsot::Inherit<typename B::Check_sat::T_propagate, T_propagate> {
    public:
        using Inherit = unsot::Inherit<typename B::Check_sat::T_propagate, T_propagate>;
        using typename Inherit::This;
        using typename Inherit::Solver;

        using Inherit::Inherit;
        virtual ~T_propagate()                                                            = default;
        T_propagate(T_propagate&&)                                                        = default;

        void post_check_sat_notice(var::Ptr&) override;
    protected:
        virtual void notice_invariant(var::Ptr&);
    };
}
