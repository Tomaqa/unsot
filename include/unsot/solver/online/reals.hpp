#pragma once

#include "solver.hpp"
#include "smt/solver/online/reals.hpp"

namespace unsot::solver::online::reals {
    template <typename B> class Mixin;

    template <typename S, typename SatSolver, typename OdeSolver,
              var::Type typeV = var::Type::assignee>
        using Crtp = Mixin<solver::Mixin<smt::solver::online::reals::Crtp<S, SatSolver, typeV>,
                                         OdeSolver>>;
}

namespace unsot::solver::online::reals {
    template <typename B>
    class Mixin : public Inherit<B, Mixin<B>> {
    public:
        using Inherit = unsot::Inherit<B, Mixin<B>>;
        using typename Inherit::This;
        using typename Inherit::That;
        using Unsot_online_t = This;

        class Check_sat;

        using Inherit::Inherit;
        virtual ~Mixin()                                            = default;
        Mixin(const Mixin&)                                          = delete;
        Mixin& operator =(const Mixin&)                              = delete;
        Mixin(Mixin&&)                                              = default;
        Mixin& operator =(Mixin&&)                                  = default;
    };
}

#include "solver/online/reals/check_sat.hpp"
