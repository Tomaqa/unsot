#pragma once

#include "ode/flow.hpp"

#include "util/hash.hpp"
#include "expr/var.hpp"

namespace unsot::solver::flow {
    using namespace ode::flow;
    namespace var = expr::var;
    using flow::Ptr;
    using flow::Keys;

    class Def;
    template <typename S> class Inst;

    using Defs = Vector<Def>;

    using Ids_map = Hash<Key, Id>;

    using Inst_id = Idx;
    template <typename S> using Insts = Vector<Inst<S>>;
    template <typename S> using Insts_ptr = Shared_ptr<Insts<S>>;

    using All_variants_map = Hash<Key, Variants>;

    inline const String param_key_prefix = "&";
    inline const String final_key_suffix = "*";

    template <typename Is, typename I = typename Decay<Is>::value_type,
              typename PtrT = Insts_ptr<typename I::Solver>>
        inline PtrT new_insts(Is&& insts);
    template <typename PtrT> inline PtrT new_insts();

    template <typename Ds> Forward_as<Def, Ds> def(Ds&&, const Id&) noexcept;
    template <typename Ds> Forward_as<Def, Ds> def(Ds&&, const Ids_map&, const Key&);
    template <typename M> Forward_as<Id, M> id(M&&, const Key&);

    template <typename Is, typename I = typename Decay<Is>::value_type>
        Forward_as<I, Is> inst(Is&&, const Inst_id&) noexcept;

    template <typename M> Forward_as<Variants, M> variants(M&&, const Key& ode_key);

    bool is_param_key(const Key&) noexcept;
    bool is_init_key(const Key&) noexcept;
    bool is_final_key(const Key&) noexcept;

    Key to_param_key(Key) noexcept;
    Key to_init_key(Key ode_key) noexcept;
    Key to_final_key(Key ode_key) noexcept;
    Key param_to_ode_key(Key) noexcept;
    Key init_to_ode_key(Key) noexcept;
    Key final_to_ode_key(Key) noexcept;

    Key to_ode_variant_key(Key ode_key) noexcept;
    Key to_inv_variant_key(Key ode_key) noexcept;
    Key ode_variant_to_ode_key(Key) noexcept;
    Key inv_variant_to_ode_key(Key) noexcept;
}

#include "solver/flow/def.hpp"
#include "solver/flow/inst.hpp"

#include "solver/flow.inl"
