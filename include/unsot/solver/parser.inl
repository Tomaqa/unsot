#pragma once

namespace unsot::solver {
    template <typename B, typename OdeSolver>
    const auto& Mixin<B, OdeSolver>::Parser::
        cflow_def_arg_sorts_mask(const Key& fkey) const
    {
        return cflow_def_arg_sort_masks()[fkey];
    }

    template <typename B, typename OdeSolver>
    auto& Mixin<B, OdeSolver>::Parser::
        flow_def_arg_sorts_mask(const Key& fkey) noexcept
    {
        return flow_def_arg_sort_masks()[fkey];
    }
}
