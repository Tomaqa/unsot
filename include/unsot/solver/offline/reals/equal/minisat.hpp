#pragma once

#include "solver/offline/reals/equal.hpp"
#include "smt/solver/offline/reals/equal/minisat.hpp"

#ifdef NO_EXPLICIT_TP_INST
#include "solver.tpp"
#include "solver/offline/reals/equal.tpp"

#include "smt/solver.tpp"
#include "smt/solver/bools.tpp"
#include "smt/solver/reals.tpp"

#include "smt/solver/offline/bools.tpp"
#include "smt/solver/offline/reals.tpp"
#include "smt/solver/offline/reals/equal.tpp"

#include "solver/run.tpp"
#include "smt/solver/run.tpp"
#include "smt/solver/offline/bools/run.tpp"
#endif

namespace unsot::solver::offline::reals::equal::minisat {
    template <typename S, typename OdeSolver>
        using Crtp = equal::Crtp<S, smt::sat::solver::offline::expr::Minisat, OdeSolver>;

    template <typename OdeSolver>
    struct Solver final : Inherit<Crtp<Solver<OdeSolver>, OdeSolver>> {
        using Inherit = unsot::Inherit<Crtp<Solver<OdeSolver>, OdeSolver>>;

        using Inherit::Inherit;
    };
}
