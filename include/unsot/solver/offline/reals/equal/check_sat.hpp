#pragma once

namespace unsot::solver::offline::reals::equal {
    template <typename B>
    class Mixin<B>::Check_sat : public unsot::Inherit<typename B::Check_sat, Mixin::Check_sat> {
    public:
        using Inherit = unsot::Inherit<typename B::Check_sat, Mixin::Check_sat>;
        using typename Inherit::This;
        using typename Inherit::Solver;

        using typename Inherit::Flow_inst;

        using Inherit::Inherit;

        String profiling_to_string(Duration total) const override;
    protected:
        friend Flow_inst;

        void pre_compute_of(Flow_inst&) override;
    };
}
