#pragma once

#include "smt/solver/run.hpp"
#include "ode/solver/run.hpp"
#include "solver.hpp"

namespace unsot::solver {
    namespace aux {
        template <typename S>
        class Ode_run : public Inherit<ode::solver::Base::Run<S>> {
        public:
            using Inherit = unsot::Inherit<ode::solver::Base::Run<S>>;

            using typename Inherit::Solver;
            using Ode_solver = Solver;

            using Inherit::Inherit;
        protected:
            virtual const Ode_solver& code_solver() const noexcept        = 0;
            virtual Ode_solver& ode_solver() noexcept                     = 0;
            const Solver& csolver() const noexcept override
                                                     { return code_solver(); }
            Solver& solver() noexcept override        { return ode_solver(); }
        };
    }

    template <typename B, typename OdeSolver>
    class Mixin<B, OdeSolver>::Run : public unsot::Inherit<typename B::Run>,
                                     public aux::Ode_run<OdeSolver> {
    public:
        using Inherit = unsot::Inherit<typename B::Run>;
        using Ode_run = aux::Ode_run<OdeSolver>;
        using typename Inherit::That;
        using typename Inherit::Solver;
        using typename Ode_run::Ode_solver;

        using Inherit::Inherit;
        virtual ~Run()                                              = default;

        void init() override;
        void do_stuff() override;
        void finish() override;

        String usage() const override;

        void plot_trajects() override;
    protected:
        using Inherit::csolver;
        using Inherit::solver;
        const Ode_solver& code_solver() const noexcept override
                                           { return csolver().code_solver(); }
        Ode_solver& ode_solver() noexcept override
                                             { return solver().ode_solver(); }

        Path plot_path_suffix(const flow::Id&) const override;

        String getopt_str() const noexcept override;
        bool process_opt(char) override;

        void plot_traject(const flow::Id&) override;
    };
}
