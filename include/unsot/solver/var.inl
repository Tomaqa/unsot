#pragma once

namespace unsot::solver::var {
    template <typename B, typename InstT>
    Variant_mixin<B, InstT>::Variant_mixin(Keys_ptr kptr, Values_ptr vptr,
                                           Ids_map_ptr imap_ptr, Id id_,
                                           Key ode_key_, Id final_vid,
                                           Flow_insts_ptr finsts_ptr,
                                           flow::Inst_id finst_id)
        : Variant_mixin(move(kptr), move(vptr), move(imap_ptr), move(id_),
                        TUPLE_WITH_SEQ(make_tuple()),
                        move(ode_key_), move(final_vid),
                        move(finsts_ptr), move(finst_id))
    { }

    template <typename B, typename InstT>
    template <typename T>
    T Variant_mixin<B, InstT>::
        cons_tp(Keys_ptr& kptr, Values_ptr& vptr, Ids_map_ptr& imap_ptr,
                Key ode_key_, Id final_vid,
                Flow_insts_ptr finsts_ptr, flow::Inst_id finst_id, Key key_)
    {
        Id id_ = add_key_value(*kptr, *vptr, *imap_ptr, move(key_));
        return cons_tp<T>(kptr, vptr, imap_ptr, move(id_),
                          move(ode_key_), move(final_vid),
                          move(finsts_ptr), move(finst_id));
    }

    template <typename B, typename InstT>
    const auto& Variant_mixin<B, InstT>::cflow_inst() const noexcept
    {
        return flow::inst(cflow_insts(), cflow_inst_id());
    }

    template <typename B, typename InstT>
    auto& Variant_mixin<B, InstT>::flow_inst() noexcept
    {
        return flow::inst(flow_insts(), cflow_inst_id());
    }

    template <typename B, typename InstT>
    const auto& Variant_mixin<B, InstT>::cfinal_var() const
    {
        auto& vptr = cflow_inst().csolver().creals().cptr(cfinal_var_id());
        return Solver::Final_var::cast(vptr);
    }

    template <typename B, typename InstT>
    auto& Variant_mixin<B, InstT>::final_var()
    {
        auto& vptr = flow_inst().solver().reals().ptr(cfinal_var_id());
        return Solver::Final_var::cast(vptr);
    }

    ////////////////////////////////////////////////////////////////

    template <typename B>
    template <typename T>
    T Ode_mixin<B>::
        cons_tp(Keys_ptr& kptr, Values_ptr& vptr, Ids_map_ptr& imap_ptr,
                Fun fun_, Args_base_link_ptr args_l_ptr,
                Key ode_key_, Id final_vid,
                Flow_insts_ptr finsts_ptr, flow::Inst_id finst_id, Key key_)
    {
        Id id_ = add_key_value(*kptr, *vptr, *imap_ptr, move(key_));
        return cons_tp<T>(kptr, vptr, imap_ptr, move(id_),
                          TUPLE_WITH_SEQ(make_tuple(move(fun_),
                                                    move(args_l_ptr))),
                          move(ode_key_), move(final_vid),
                          move(finsts_ptr), move(finst_id));
    }

    template <typename B>
    const typename Ode_mixin<B>::Arg_ids& Ode_mixin<B>::carg_ids() const
    {
        return this->cflow_inst().cvariant_arg_ids();
    }

    template <typename B>
    const typename Ode_mixin<B>::Depend_ids& Ode_mixin<B>::cdepend_ids() const
    {
        return this->cflow_inst().cvariant_depend_ids();
    }
}
