#pragma once

#include "test.hpp"
#include "smt/solver_test.hpp"

#include "solver.hpp"

#include "expr/preprocess.hpp"

namespace unsot::solver::test {
    using namespace unsot::test;
    using namespace smt::solver::test;
    using namespace unsot::solver;
    using unsot::to_string;

    ////////////////////////////////////////////////////////////////

    struct Ball {
        static inline const String name = "BALL";

        static inline const String input =
            "(echo \"<" + name + ">\")"
            " (set-option :verbosity 3)"
            " #define N 5\n"
            " #assert (> #N (xor #N 1))\n"
            " #define N-1 $d(- #N 1)\n"
            " #define g 9.8\n"
            " #define D 0.9\n"
            " (define-fun K () Real 0.95)"
            " (define-flow flow1 (x v) ((&up Bool) (&up* Bool)"
            "                           (&t* Real) (&x* Real) (&v* Real))"
            "   (and  (= x' v)"
            "         (ite &up (= v' (- (- #g) (* #D (^ v 2)) ))"
            "                  (= v' (+ (- #g) (* #D (^ v 2)) ))"
            "         )"
            "         (>= x     0)"
            "         (ite &up (>= v 0)"
            "                  (>= 0 v)"
            "         )"
            "         (= &t* _t*)"
            "         (ite &up"
            "           (and (not &up*) (= &x* x*) (= &v* -1e-6) )"
            "           (and      &up*  (= &x* 1e-6) (= &v* (* (- K) v* )) )"
            "         )"
            " ))"
            " #for (i 0 #N)"
            "     (declare-fun up#i () Bool)"
            "     (declare-fun t#i () Real)"
            "     (declare-fun x#i () Real)"
            "     (declare-fun v#i () Real)"
            " #endfor"
            " (assert (and  (not up0) (= t0 0) (= x0 5) (= v0 0) ))"
            " (assert (and 1 #for (i 0 #N-1)"
            "   #let j $d(+ #i 1)"
            "     (flow flow1 (t#i x#i v#i) (up#i up#j t#j x#j v#j))"
            "   #endlet j"
            " #endfor))"
            " (assert (and up#N (> t#N 0) (= x#N 1e-6) (> v#N 0) ))"
            ;

        static inline const Keys expected_bkeys = {
            "up0",  "up1",  "up2",  "up3",  "up4",  "up5",
        };
        static inline const expr::Bools expected_bvals = {
            false,   true,  false,   true,  false,   true,
        };

        static inline const Keys expected_rkeys = {
            "t0",  "t1",  "t2",  "t3",  "t4",  "t5",
            "x0",  "x1",  "x2",  "x3",  "x4",  "x5",
            "v0",  "v1",  "v2",  "v3",  "v4",  "v5",
        };
        static inline const expr::Reals expected_rvals = {
              0., 1.749588, 2.003780, 2.292533, 2.488659, 2.702497,
              5.,     1e-6, 0.361897,     1e-6, 0.206379,     1e-6,
              0., 3.134672,    -1e-6, 2.186528,    -1e-6, 1.766204,
        };
    };

    ////////////////////////////////////////////////////////////////

    template <const char* fn>
    struct File {
        static inline const String name = "FILE "s + fn;

        template <typename S>
        static void parse(S& s)
        {
            typename S::Run run_obj;
            run_obj.ipath() = fn;
            run_obj.init();
            s.set_run(run_obj);

            Preprocess preprocess_obj(s.preprocess_lines_only(run_obj.is()));
            preprocess_obj.set_run(run_obj);
            List ls = s.preprocess_list_only(move(preprocess_obj).perform());

            cerr << endl << "<" << name << ">" << endl;
            s.be_verbose();
            s.parse_only(move(ls));
        }
    };

    inline char ball_fn[] = "data/smto/ball/ball.smto";
    struct Ball_file : File<ball_fn> {
        static inline const Keys expected_bkeys = {
            "up<0>",  "up<1>",  "up<2>",  "up<3>",  "up<4>",  "up<5>",
        };
        static inline const expr::Bools expected_bvals = {
            false,   true,  false,   true,  false,   true,
        };

        static inline const Keys expected_rkeys = {
            "x<0>",  "x<1>",  "x<2>",  "x<3>",  "x<4>",  "x<5>",  "x<6>",
            "v<0>",  "v<1>",  "v<2>",  "v<3>",  "v<4>",  "v<5>",  "v<6>",
        };
        static inline const expr::Reals expected_rvals = {
              4.,       0., 2.252167,       0., 1.435724,       0., 1.034071,
             -4., 7.432534,       0., 5.677186,       0., 4.714587,       0.,
        };
    };

    inline char prostate_fn[] = "data/smto/prostate/prostate.smto";
    struct Prostate_file : File<prostate_fn> {
        static inline const Keys expected_bkeys = {
            "on<0>",  "on<1>",  "on<2>",  "on<3>",  "on<4>",  "on<5>",  "on<6>",
        };
        static inline const expr::Bools expected_bvals = {
               true,  false,   true,  false,   true,  false,  false,
        };

        static inline const Keys expected_rkeys = {
            "x<0>",  "x<1>",  "x<2>",  "x<3>",  "x<4>",  "x<5>",  "x<6>",
            "y<0>",  "y<1>",  "y<2>",  "y<3>",  "y<4>",  "y<5>",  "y<6>",
            "z<0>",  "z<1>",  "z<2>",  "z<3>",  "z<4>",  "z<5>",  "z<6>",
            "r0",  "r1",
        };
        static inline const expr::Reals expected_rvals = {
             15., 3.619738, 9.886848, 3.691549, 9.903838, 3.713417, 4.650731,
             0.1, 0.380262, 0.113152, 0.308451, 0.096162, 0.286583, 0.250697,
             12., 0.517430, 18.377180, 0.581129, 18.308584, 0.583277, 13.374554,
             4., 10.
        };
    };

    inline char glucose_fn[] = "data/smto/glucose/glucose.smto";
    struct Glucose_file : File<glucose_fn> {
        static inline const Keys expected_bkeys = {
            "mode1<0>",  "mode5<1>",  "mode9<2>",  "mode9<3>",
        };
        static inline const expr::Bools expected_bvals = {
            true,  true,  true,  true,
        };

        static inline const Keys expected_rkeys = {
            "Ip<0>",  "Ip<1>",  "Ip<2>",
             "X<0>",   "X<1>",   "X<2>",
            "I1<0>",  "I1<1>",  "I1<2>",
            "Id<0>",  "Id<1>",  "Id<2>",
            "Il<0>",  "Il<1>",  "Il<2>",
            "Gp<0>",  "Gp<1>",  "Gp<2>",
            "Gt<0>",  "Gt<1>",  "Gt<2>",
        };
        static inline const expr::Reals expected_rvals = {
            5.000000, 5.000000, 0.012532,
            30.000000, 30.000000, -40.167299,
            120.000000, 120.000000, 92.678627,
            120.000000, 120.000000, 116.528480,
            3.000000, 3.000000, 0.012532,
            200.000000, 200.000000, 245.762454,
            150.000000, 150.000000, 108.941678,
        };
    };

    inline char fun_fn[] = "data/smto/fun/fun.smto";
    struct Fun_file : Inherit<File<fun_fn>> {
        static inline const Keys expected_bkeys = {};
        static inline const expr::Bools expected_bvals = {};

        static inline const Keys expected_rkeys = {/*
            "low<0>", "low<1>", "low<2>", "low<3>", "low<4>", "low<5>", "low<6>",
            "low<7>", "low<8>", "low<9>", "low<10>", "low<11>", "low<12>", "low<13>",
            "low<14>", "low<15>", "low<16>", "low<17>", "low<18>", "low<19>", "low<20>",
            "high<0>", "high<1>", "high<2>", "high<3>", "high<4>", "high<5>", "high<6>",
            "high<7>", "high<8>", "high<9>", "high<10>", "high<11>", "high<12>", "high<13>",
            "high<14>", "high<15>", "high<16>", "high<17>", "high<18>", "high<19>", "high<20>",
            "y1<0>", "y1<1>", "y1<2>", "y1<3>", "y1<4>", "y1<5>", "y1<6>",
            "y1<7>", "y1<8>", "y1<9>", "y1<10>", "y1<11>", "y1<12>", "y1<13>",
            "y1<14>", "y1<15>", "y1<16>", "y1<17>", "y1<18>", "y1<19>", "y1<20>"
        */};
        static inline const expr::Reals expected_rvals = {/*
            -0.750000, -1.466110, -1.576427, -1.097915, 0.737726, 3.509097, 4.810660,
            4.951991, 5.141316, 5.606483, 8.931458, 9.002760, 9.082766, 9.178366,
            9.308479, 9.610923, 13.189808, 13.245251, 13.307805, 13.382071, 13.480305,
            0.750000, 0.787952, 1.361781, 2.193142, 6.251842, 7.038950, 7.218377,
            7.427999, 7.711974, 8.409725, 13.397187, 13.504140, 13.624148, 13.767548,
            13.962718, 14.416384, 19.784713, 19.867877, 19.961708, 20.073107, 20.220458,
            0.500000, -0.214000, -1.000000, -0.286000, 1.928000, 4.214000, 5.000000,
            5.000000, 5.714000, 7.928000, 10.214000, 11.000000, 11.714000, 12.500000,
            13.214000, 14.000000, 14.714000, 15.500000, 16.214000, 17.000000, 16.286000,
        */};
    };
}
